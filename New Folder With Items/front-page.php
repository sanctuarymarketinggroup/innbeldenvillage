<?php
/**
  * The template for displaying the front page
  *
  * This is the template that displays all pages by default.
  * Please note that this is the WordPress construct of pages and that
  * other "pages" on your WordPress site will use a different template.
  *
*/
 
get_header(); ?>

<main id="front-page" class="front-page container" role="main">
<?php while ( have_posts() ) : the_post(); ?>

  <section class="front-hero">
    <div class="row">
      <h1>Celebrate your life in style.</h1>
      <p>Serving residents as if we’re caring for our own mothers and fathers.</p>
    </div>
  </section>

  <section class="tour">
    <div class="row">
      <h2>
        <i><?php echo get_svg('chat'); ?></i>
        Schdule a Personal Tour
      </h2>
      <p>[Gravity Form Here]</p>
    </div>
  </section>

  <section class="discover">
    <div class="row full">
      <div class="left-block half">
        <div class="inner half-row">
          <h2>Discover the Difference</h2>
          <p>Entering The Inn at Belden Village, you’ll see and feel the difference — nurturing, love, caring, family and faith. See the transformation in your loved one.</p>
          <ul>
            <li>Nurse on duty 24 hours a day</li>
            <li>Fun-loving caregivers help with everyday needs</li>
            <li>Activities offered throughout the day</li>
            <li>Chauffeured transportation to doctors, shopping, dining and recreation</li>
            <li>Private suites and areas to visit that feel like home</li>
            <li>Caring staff provides meals, housekeeping, laundry and personal care</li>
            <li>Amenities include a beauty shop, spa, library, chapel, theatre, courtyard, health & wellness room and much more</li>
          </ul>
          <a href="#" class="btn outline">Learn More</a>
        </div>
      </div>
      <div class="right-block half">
        <div class="inner half-row">
          <h2>Discover</h2>
        </div>
      </div>
    </div>
  </section>

  <div class="explore">
    <div class="row full">
      <div class="left-block half">
        <div class="inner half-row">
          <div class="play-video">
            <div class="play"></div>
            <p>View Videos</p>
          </div>
          <h2>Explore</h2>
        </div>
      </div>
      <div class="right-block half">
        <div class="inner half-row">
          <h2>Explore The Inn At Belden Village</h2>
          <p>Tour our private suites, beautiful patio, great room and fireplace, dining room and more as you discover the splendor and warmth of The Inn at Belden Village.</p>
          <div class="icon-group">
            <i class="icon"><?php echo get_svg('photo'); ?></i>
            <a href="#" class="btn outline">Photo Gallery</a>
          </div>
          <div class="icon-group">
            <i class="icon"><?php echo get_svg('brochure'); ?></i>
            <a href="#" class="btn outline">Online Brochure</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="meet">
    <div class="row full">
      <div class="left-block half">
        <div class="inner half-row">
          <h2>What Families Say</h2>
          <div class="entries slider meet-slider">
            <div class="entry testimonial">
              <div class="entry-content">[MAKE DYNAMIC]</div>
              <div class="entry-resident">[MAKE DYNAMIC]</div>
            </div>
            <div class="entry testimonial">
              <div class="entry-content">[MAKE DYNAMIC]</div>
              <div class="entry-resident">[MAKE DYNAMIC]</div>
            </div>
          </div>
          <div class="actions">
            <div class="entries-nav">
              <div class="arrow arrow-left"><</div>
              <div class="arrow arrow-right">></div>
            </div>
            <a href="#" class="btn outline">Hear more from families</a>
          </div>
        </div>
      </div>
      <div class="right-block half">
        <div class="residents section-slider">
          <div class="resident">
            <h2>Meet Our Residents</h2>
            <p>Senior Spotlight: <span>[MAKE DYNAMIC]</span></p>
          </div>
          <div class="resident">
            <h2>Meet Our Residents</h2>
            <p>Senior Spotlight: <span>[MAKE DYNAMIC]</span></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="questions">
    <?php
      $qs = array(
        array(
          'title' => 'How do I start the conversation about assisted living with my loved one?',
          'body'  => 'Lorem Ipsum'
        ),
        array(
          'title' => 'How do I choose the best community for my loved one?',
          'body'  => 'Lorem Ipsum'
        ),
        array(
          'title' => 'How do we afford assisted living?',
          'body'  => 'Lorem Ipsum'
        ),
        array(
          'title' => 'What is assisted living and how do I know it\'s right for my loved one?',
          'body'  => 'Lorem Ipsum'
        )
      );
    ?>
    <div class="row">
      <h2>Answers to Important Questions</h2>
      <div class="toggles">
        <?php foreach($qs as $q) { ?>
          <div class="toggle">
            <h4>
              <?php echo $q['title']; ?>
            </h4>
            <div class="body">
              <?php echo $q['body']; ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>

  <div class="gallery">
    <div class="row">
      <!-- Static Images -->
      <?php echo get_img_lazy( get_image_placeholder_url() ); ?>
      <?php echo get_img_lazy( get_image_placeholder_url() ); ?>
      <?php echo get_img_lazy( get_image_placeholder_url() ); ?>
      <?php echo get_img_lazy( get_image_placeholder_url() ); ?>
    </div>
  </div>

  <div class="articles">
    <?php
      $articles = new WP_Query( array(
        'post_type' => 'post',
        'showposts' => 3,
        'posts_per_page' => 3
      ) );
    ?>
    <div class="row">
      <h2>What's going on at The Inn?</h2>
      <div class="entries">
        <?php
          if ( $articles->have_posts() ) {
            while ( $articles->have_posts() ) { $articles->the_post(); ?>
              <div class="entry">
                <a href="<?php echo get_permalink(get_the_id()); ?>" class="img">
                  [Image]
                </a>
                <h3>
                  <a href="<?php echo get_permalink(get_the_id()); ?>"><?php the_title(); ?></a>
                </h3>
                <a href="<?php echo get_permalink(get_the_id()); ?>" class="readmore">Read More</a>
              </div>
            <?php }
          }
          wp_reset_postdata();
        ?>
      </div>
    </div>
  </div>

  <section class="associatations">
    <div class="row">
      <div class="logo"></div>
      <div class="logo"></div>
      <div class="logo"></div>
      <div class="logo"></div>
      <div class="logo"></div>
      <div class="logo"></div>
      <div class="logo"></div>
    </div>
  </section>
  
  <section class="get-in-touch">
    <div class="row">
      <h2>
        <i><?php echo get_svg('chat'); ?></i>
        Get In Touch with Us
      </h2>
      <p>[Gravity Form Here]</p>
    </div>
  </section>

<?php endwhile;?>
</main>

<?php get_footer(); ?>