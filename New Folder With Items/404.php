<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header(); ?>

<div id="not-found-page" class="template default-template" role="main">
	<article class="entry" id="post-not-found">
		<header class="page-header entry-header">
			<?php echo show_template('components/page-title', array('class' => 'entry-title', 'title' => 'Oops, that page went off course')); ?>
		</header>
		<main class="page-body entry-body">
			<?php do_action('before_entry_body'); ?>
			<?php echo show_template('components/page-content', array(
				'class' => 'entry-content',
				'customContent' => '<p>Don\'t worry, we\'d love to help you get back on track and find what you were looking for. And if you still have questions, please <a href="' . HOME_URL . '/contact/">contact us</a> for personal assistance.</p><a class="btn" href="/">Head Back Home</a>')); ?>
			<?php do_action('after_entry_body'); ?>
		</main>
		<footer class="page-footer entry-footer"></footer>
	</article>
</div>

<?php get_footer();
