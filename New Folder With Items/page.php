<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

$class = 'default';
if ( FLBuilderModel::is_builder_enabled() ) {
	$class = 'full-width';
}

get_header(); ?>

<div id="page" class="template <?php echo $class . '-template'; ?>" role="main">
	<?php while ( have_posts() ) { the_post(); ?>
		<article class="entry" id="post-<?php the_ID(); ?>">
			<header class="page-header entry-header">
				<?php echo show_template('components/page-title', array('class' => 'entry-title')); ?>
			</header>
			<?php echo show_template('components/entry-image'); ?>
			<main class="page-body entry-body">
				<?php do_action('before_entry_body'); ?>
				<?php echo show_template('components/page-content', array('class' => 'entry-content')); ?>
				<?php do_action('after_entry_body'); ?>
			</main>
			<footer class="page-footer entry-footer">
				<?php // if(is_single()) comments_template(); ?>
			</footer>
		</article>
		<aside class="sidebar">
			<?php get_sidebar(); ?>
		</aside>
	<?php } ?>
</div>

<?php get_footer();