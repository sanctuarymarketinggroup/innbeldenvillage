<?php $settings = get_setting( array('header-logo') ); ?>
<?php // $logo = acf_image( $settings['header-logo'] ); ?>
<?php // $logo = get_svg( $settings['header-logo'] ); ?>
<?php $logo = get_bloginfo( 'name' ); ?>
<?php $phone = get_setting('phone'); ?>

<header id="header" class="container">
  <div class="row">
    <div class="logo">
      <a href="<?php echo HOME_URL; ?>"><?php echo $logo; ?></a>
    </div>
    <div class="notice">
      <i class="icon"></i>
      <p>Come Tour and Enjoy Lunch on us!</p>
    </div>
    <div class="info">
      <div class="phone">Call Us: <a href="<?php echo $phone['primary']['link']; ?>"><?php echo $phone['primary']['text']; ?></a></div>
      <div class="actions">
        <div class="facebook"></div>
        <div class="directions">
          <i class="icon"></i>
          <span>Directions</span>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="mainmenu">
  <?php echo smg_menu(); ?>
</div>

<div class="header-form">
  <div class="row">
    <h6>Schedule a Personal Tour:</h6>
    <p>[Gravity Form]</p>
  </div>
</div>