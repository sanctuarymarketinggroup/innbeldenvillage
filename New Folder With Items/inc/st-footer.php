<?php $logo = get_setting('header-logo'); ?>
<?php // $logo = acf_image( $settings['header-logo'] ); ?>
<?php // $logo = get_svg( $settings['header-logo'] ); ?>
<?php $logo = get_setting( 'sitename' ); ?>
<?php $sitename = get_setting('sitename'); ?>
<?php $address = get_setting('address'); ?>
<?php $phone = get_setting('phone'); ?>

<footer id="footer" class="container">
  <div class="row">
    <div class="logo">
      <a href="<?php echo HOME_URL; ?>"><?php echo $logo; ?></a>
    </div>
    <div class="info">
      <h4><?php echo $sitename; ?></h4>
      <div class="address">
        <div>
          <p><?php echo $address['street']; ?><br/>
          <?php echo $address['city']; ?>, <?php echo $address['state']; ?> <?php echo $address['zip']; ?><br/>
          Ph: <a href="<?php echo $phone['primary']['link']; ?>"><?php echo $phone['primary']['text']; ?></a><br/>
        </div>
      </div>
    </div>
    <nav class="footer-menu">
      <?php smg_footer_menu(); ?>
    </nav>
  </div>
</footer>

<div class="copyright">
  <div class="row">
    <div class="social">
      <div class="facebook"></div>
    </div>
    <p>&copy;<?php echo date('Y'); ?> <?php echo $sitename; ?></p>
  </div>
</div>