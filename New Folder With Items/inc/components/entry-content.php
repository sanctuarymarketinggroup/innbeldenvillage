<div class="entry-content">
  <?php do_action('before_entry_content'); ?>
  <?php 
    if( !isset($customContent) || empty($customContent) ){ 
      the_content();
    } else {
      echo $customContent;  
    }
  ?> 
  <?php do_action('after_entry_content'); ?>
</div>