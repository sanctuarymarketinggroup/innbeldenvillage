<h1 class="entry-title">
  <?php
  if (isset($title) && ! empty($title)) {
    echo $title;
  } elseif (isset($customid) && ! empty($customid)) {
    echo get_the_title( $customid );
  } else {
    the_title(); 
  }
  ?>
</h1>