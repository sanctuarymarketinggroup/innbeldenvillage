<div class="<?php echo get_incclass($class, 'page-content'); ?>">
  <?php
    if (incvar($customContent)) {
      echo $customContent;
    } else {
      the_content();
    }
  ?> 
</div>