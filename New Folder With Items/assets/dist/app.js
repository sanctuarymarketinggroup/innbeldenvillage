"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function ($) {
  /*
    Options:
  
  #############################
    Required
  #############################
  
    -- Slide in Side = default left
      -- Possible options "top", "left", "right", "bottom"
  
    -- Slide in Speed = default 400
      -- Options any miliseconds
  
    -- Optional Open/Close Button = default Create on for them
      -- Any object with a div or class
  
    -- Width of Menu = default 100%
      -- Any number
  
  #############################
    Non Essentails
  #############################
  
    -- Fixed or Static = Default Fixed
      -- Non essential for now
  
    -- Animation
      -- Non essential for now
  
    -- Optional Button Styling
      -- Non essential for now
  
  
  
  */
  $.fn.mobile_menu = function (options) {
    // Define Default settings:
    var defaults = {
      side: 'left',
      speed: '0.5s',
      toggler: '.menu-toggle',
      width: '100%',
      height: '100%',
      buttonStyles: true
    };
    var initals = {
      toggleButton: '<div class="menu-toggle"></div>',
      menuWrap: '<div class="offcanvas-menu-wrap"></div>'
    };
    var $toggler;
    var offcanvas;
    var $this = $(this);
    var settings = $.extend({}, initals, defaults, options);

    if (settings.toggler == defaults.toggler) {
      $this.before(settings.toggleButton);
    }
    /* Determine if you want toggler Styles */


    $toggler = $(settings.toggler);

    if (settings.buttonStyles) {
      $toggler.addClass('offcanvas-toggle');
    }

    switch (settings.side) {
      case "left":
        offcanvas = 'translateX(-100%)';
        break;

      case "top":
        offcanvas = 'translateY(-100%)';
        break;

      case "right":
        offcanvas = 'translateX(100%)';
        break;

      case "bottom":
        offcanvas = 'translateY(100%)';
        break;
    }

    var styles = {
      'transform': offcanvas,
      'transition': settings.speed,
      height: settings.height,
      width: settings.width
    };
    return this.each(function () {
      $this.addClass('offcanvas-menu').wrap(settings.menuWrap);
      $this.parent().css(styles);
      console.log($toggler);
      $toggler.on('click', function () {
        console.log('open/close');
        $(this).toggleClass('offcanvas-active');
        console.log($this);
        $this.closest('.offcanvas-menu-wrap').toggleClass('offcanvas-open');
      });
    });
  };

  $('#mobile_menu').mobile_menu({
    side: 'bottom'
  });
})(jQuery);

;
/*
    _ _      _       _
___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                  |__/
 Version: 1.6.0
 Author: Ken Wheeler
Website: http://kenwheeler.github.io
   Docs: http://kenwheeler.github.io/slick
   Repo: http://github.com/kenwheeler/slick
 Issues: http://github.com/kenwheeler/slick/issues
 */

/* global window, document, define, jQuery, setInterval, clearInterval */

(function (factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }
})(function ($) {
  'use strict';

  var Slick = window.Slick || {};

  Slick = function () {
    var instanceUid = 0;

    function Slick(element, settings) {
      var _ = this,
          dataSettings;

      _.defaults = {
        accessibility: true,
        adaptiveHeight: false,
        appendArrows: $(element),
        appendDots: $(element),
        arrows: true,
        asNavFor: null,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        customPaging: function customPaging(slider, i) {
          return $('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1);
        },
        dots: false,
        dotsClass: 'slick-dots',
        draggable: true,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: true,
        swipeToSlide: false,
        touchMove: true,
        touchThreshold: 5,
        useCSS: true,
        useTransform: true,
        variableWidth: false,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      };
      _.initials = {
        animating: false,
        dragging: false,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: false,
        slideOffset: 0,
        swipeLeft: null,
        $list: null,
        touchObject: {},
        transformsEnabled: false,
        unslicked: false
      };
      $.extend(_, _.initials);
      _.activeBreakpoint = null;
      _.animType = null;
      _.animProp = null;
      _.breakpoints = [];
      _.breakpointSettings = [];
      _.cssTransitions = false;
      _.focussed = false;
      _.interrupted = false;
      _.hidden = 'hidden';
      _.paused = true;
      _.positionProp = null;
      _.respondTo = null;
      _.rowCount = 1;
      _.shouldClick = true;
      _.$slider = $(element);
      _.$slidesCache = null;
      _.transformType = null;
      _.transitionType = null;
      _.visibilityChange = 'visibilitychange';
      _.windowWidth = 0;
      _.windowTimer = null;
      dataSettings = $(element).data('slick') || {};
      _.options = $.extend({}, _.defaults, settings, dataSettings);
      _.currentSlide = _.options.initialSlide;
      _.originalSettings = _.options;

      if (typeof document.mozHidden !== 'undefined') {
        _.hidden = 'mozHidden';
        _.visibilityChange = 'mozvisibilitychange';
      } else if (typeof document.webkitHidden !== 'undefined') {
        _.hidden = 'webkitHidden';
        _.visibilityChange = 'webkitvisibilitychange';
      }

      _.autoPlay = $.proxy(_.autoPlay, _);
      _.autoPlayClear = $.proxy(_.autoPlayClear, _);
      _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
      _.changeSlide = $.proxy(_.changeSlide, _);
      _.clickHandler = $.proxy(_.clickHandler, _);
      _.selectHandler = $.proxy(_.selectHandler, _);
      _.setPosition = $.proxy(_.setPosition, _);
      _.swipeHandler = $.proxy(_.swipeHandler, _);
      _.dragHandler = $.proxy(_.dragHandler, _);
      _.keyHandler = $.proxy(_.keyHandler, _);
      _.instanceUid = instanceUid++; // A simple way to check for HTML strings
      // Strict HTML recognition (must start with <)
      // Extracted from jQuery v1.11 source

      _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;

      _.registerBreakpoints();

      _.init(true);
    }

    return Slick;
  }();

  Slick.prototype.activateADA = function () {
    var _ = this;

    _.$slideTrack.find('.slick-active').attr({
      'aria-hidden': 'false'
    }).find('a, input, button, select').attr({
      'tabindex': '0'
    });
  };

  Slick.prototype.addSlide = Slick.prototype.slickAdd = function (markup, index, addBefore) {
    var _ = this;

    if (typeof index === 'boolean') {
      addBefore = index;
      index = null;
    } else if (index < 0 || index >= _.slideCount) {
      return false;
    }

    _.unload();

    if (typeof index === 'number') {
      if (index === 0 && _.$slides.length === 0) {
        $(markup).appendTo(_.$slideTrack);
      } else if (addBefore) {
        $(markup).insertBefore(_.$slides.eq(index));
      } else {
        $(markup).insertAfter(_.$slides.eq(index));
      }
    } else {
      if (addBefore === true) {
        $(markup).prependTo(_.$slideTrack);
      } else {
        $(markup).appendTo(_.$slideTrack);
      }
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slides.each(function (index, element) {
      $(element).attr('data-slick-index', index);
    });

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.animateHeight = function () {
    var _ = this;

    if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);

      _.$list.animate({
        height: targetHeight
      }, _.options.speed);
    }
  };

  Slick.prototype.animateSlide = function (targetLeft, callback) {
    var animProps = {},
        _ = this;

    _.animateHeight();

    if (_.options.rtl === true && _.options.vertical === false) {
      targetLeft = -targetLeft;
    }

    if (_.transformsEnabled === false) {
      if (_.options.vertical === false) {
        _.$slideTrack.animate({
          left: targetLeft
        }, _.options.speed, _.options.easing, callback);
      } else {
        _.$slideTrack.animate({
          top: targetLeft
        }, _.options.speed, _.options.easing, callback);
      }
    } else {
      if (_.cssTransitions === false) {
        if (_.options.rtl === true) {
          _.currentLeft = -_.currentLeft;
        }

        $({
          animStart: _.currentLeft
        }).animate({
          animStart: targetLeft
        }, {
          duration: _.options.speed,
          easing: _.options.easing,
          step: function step(now) {
            now = Math.ceil(now);

            if (_.options.vertical === false) {
              animProps[_.animType] = 'translate(' + now + 'px, 0px)';

              _.$slideTrack.css(animProps);
            } else {
              animProps[_.animType] = 'translate(0px,' + now + 'px)';

              _.$slideTrack.css(animProps);
            }
          },
          complete: function complete() {
            if (callback) {
              callback.call();
            }
          }
        });
      } else {
        _.applyTransition();

        targetLeft = Math.ceil(targetLeft);

        if (_.options.vertical === false) {
          animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
        } else {
          animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
        }

        _.$slideTrack.css(animProps);

        if (callback) {
          setTimeout(function () {
            _.disableTransition();

            callback.call();
          }, _.options.speed);
        }
      }
    }
  };

  Slick.prototype.getNavTarget = function () {
    var _ = this,
        asNavFor = _.options.asNavFor;

    if (asNavFor && asNavFor !== null) {
      asNavFor = $(asNavFor).not(_.$slider);
    }

    return asNavFor;
  };

  Slick.prototype.asNavFor = function (index) {
    var _ = this,
        asNavFor = _.getNavTarget();

    if (asNavFor !== null && _typeof(asNavFor) === 'object') {
      asNavFor.each(function () {
        var target = $(this).slick('getSlick');

        if (!target.unslicked) {
          target.slideHandler(index, true);
        }
      });
    }
  };

  Slick.prototype.applyTransition = function (slide) {
    var _ = this,
        transition = {};

    if (_.options.fade === false) {
      transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
    } else {
      transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
    }

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.autoPlay = function () {
    var _ = this;

    _.autoPlayClear();

    if (_.slideCount > _.options.slidesToShow) {
      _.autoPlayTimer = setInterval(_.autoPlayIterator, _.options.autoplaySpeed);
    }
  };

  Slick.prototype.autoPlayClear = function () {
    var _ = this;

    if (_.autoPlayTimer) {
      clearInterval(_.autoPlayTimer);
    }
  };

  Slick.prototype.autoPlayIterator = function () {
    var _ = this,
        slideTo = _.currentSlide + _.options.slidesToScroll;

    if (!_.paused && !_.interrupted && !_.focussed) {
      if (_.options.infinite === false) {
        if (_.direction === 1 && _.currentSlide + 1 === _.slideCount - 1) {
          _.direction = 0;
        } else if (_.direction === 0) {
          slideTo = _.currentSlide - _.options.slidesToScroll;

          if (_.currentSlide - 1 === 0) {
            _.direction = 1;
          }
        }
      }

      _.slideHandler(slideTo);
    }
  };

  Slick.prototype.buildArrows = function () {
    var _ = this;

    if (_.options.arrows === true) {
      _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
      _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

      if (_.slideCount > _.options.slidesToShow) {
        _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

        _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

        if (_.htmlExpr.test(_.options.prevArrow)) {
          _.$prevArrow.prependTo(_.options.appendArrows);
        }

        if (_.htmlExpr.test(_.options.nextArrow)) {
          _.$nextArrow.appendTo(_.options.appendArrows);
        }

        if (_.options.infinite !== true) {
          _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        }
      } else {
        _.$prevArrow.add(_.$nextArrow).addClass('slick-hidden').attr({
          'aria-disabled': 'true',
          'tabindex': '-1'
        });
      }
    }
  };

  Slick.prototype.buildDots = function () {
    var _ = this,
        i,
        dot;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$slider.addClass('slick-dotted');

      dot = $('<ul />').addClass(_.options.dotsClass);

      for (i = 0; i <= _.getDotCount(); i += 1) {
        dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
      }

      _.$dots = dot.appendTo(_.options.appendDots);

      _.$dots.find('li').first().addClass('slick-active').attr('aria-hidden', 'false');
    }
  };

  Slick.prototype.buildOut = function () {
    var _ = this;

    _.$slides = _.$slider.children(_.options.slide + ':not(.slick-cloned)').addClass('slick-slide');
    _.slideCount = _.$slides.length;

    _.$slides.each(function (index, element) {
      $(element).attr('data-slick-index', index).data('originalStyling', $(element).attr('style') || '');
    });

    _.$slider.addClass('slick-slider');

    _.$slideTrack = _.slideCount === 0 ? $('<div class="slick-track"/>').appendTo(_.$slider) : _.$slides.wrapAll('<div class="slick-track"/>').parent();
    _.$list = _.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent();

    _.$slideTrack.css('opacity', 0);

    if (_.options.centerMode === true || _.options.swipeToSlide === true) {
      _.options.slidesToScroll = 1;
    }

    $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

    _.setupInfinite();

    _.buildArrows();

    _.buildDots();

    _.updateDots();

    _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

    if (_.options.draggable === true) {
      _.$list.addClass('draggable');
    }
  };

  Slick.prototype.buildRows = function () {
    var _ = this,
        a,
        b,
        c,
        newSlides,
        numOfSlides,
        originalSlides,
        slidesPerSection;

    newSlides = document.createDocumentFragment();
    originalSlides = _.$slider.children();

    if (_.options.rows > 1) {
      slidesPerSection = _.options.slidesPerRow * _.options.rows;
      numOfSlides = Math.ceil(originalSlides.length / slidesPerSection);

      for (a = 0; a < numOfSlides; a++) {
        var slide = document.createElement('div');

        for (b = 0; b < _.options.rows; b++) {
          var row = document.createElement('div');

          for (c = 0; c < _.options.slidesPerRow; c++) {
            var target = a * slidesPerSection + (b * _.options.slidesPerRow + c);

            if (originalSlides.get(target)) {
              row.appendChild(originalSlides.get(target));
            }
          }

          slide.appendChild(row);
        }

        newSlides.appendChild(slide);
      }

      _.$slider.empty().append(newSlides);

      _.$slider.children().children().children().css({
        'width': 100 / _.options.slidesPerRow + '%',
        'display': 'inline-block'
      });
    }
  };

  Slick.prototype.checkResponsive = function (initial, forceUpdate) {
    var _ = this,
        breakpoint,
        targetBreakpoint,
        respondToWidth,
        triggerBreakpoint = false;

    var sliderWidth = _.$slider.width();

    var windowWidth = window.innerWidth || $(window).width();

    if (_.respondTo === 'window') {
      respondToWidth = windowWidth;
    } else if (_.respondTo === 'slider') {
      respondToWidth = sliderWidth;
    } else if (_.respondTo === 'min') {
      respondToWidth = Math.min(windowWidth, sliderWidth);
    }

    if (_.options.responsive && _.options.responsive.length && _.options.responsive !== null) {
      targetBreakpoint = null;

      for (breakpoint in _.breakpoints) {
        if (_.breakpoints.hasOwnProperty(breakpoint)) {
          if (_.originalSettings.mobileFirst === false) {
            if (respondToWidth < _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          } else {
            if (respondToWidth > _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          }
        }
      }

      if (targetBreakpoint !== null) {
        if (_.activeBreakpoint !== null) {
          if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
            _.activeBreakpoint = targetBreakpoint;

            if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
              _.unslick(targetBreakpoint);
            } else {
              _.options = $.extend({}, _.originalSettings, _.breakpointSettings[targetBreakpoint]);

              if (initial === true) {
                _.currentSlide = _.options.initialSlide;
              }

              _.refresh(initial);
            }

            triggerBreakpoint = targetBreakpoint;
          }
        } else {
          _.activeBreakpoint = targetBreakpoint;

          if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
            _.unslick(targetBreakpoint);
          } else {
            _.options = $.extend({}, _.originalSettings, _.breakpointSettings[targetBreakpoint]);

            if (initial === true) {
              _.currentSlide = _.options.initialSlide;
            }

            _.refresh(initial);
          }

          triggerBreakpoint = targetBreakpoint;
        }
      } else {
        if (_.activeBreakpoint !== null) {
          _.activeBreakpoint = null;
          _.options = _.originalSettings;

          if (initial === true) {
            _.currentSlide = _.options.initialSlide;
          }

          _.refresh(initial);

          triggerBreakpoint = targetBreakpoint;
        }
      } // only trigger breakpoints during an actual break. not on initialize.


      if (!initial && triggerBreakpoint !== false) {
        _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
      }
    }
  };

  Slick.prototype.changeSlide = function (event, dontAnimate) {
    var _ = this,
        $target = $(event.currentTarget),
        indexOffset,
        slideOffset,
        unevenOffset; // If target is a link, prevent default action.


    if ($target.is('a')) {
      event.preventDefault();
    } // If target is not the <li> element (ie: a child), find the <li>.


    if (!$target.is('li')) {
      $target = $target.closest('li');
    }

    unevenOffset = _.slideCount % _.options.slidesToScroll !== 0;
    indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

    switch (event.data.message) {
      case 'previous':
        slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;

        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
        }

        break;

      case 'next':
        slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;

        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
        }

        break;

      case 'index':
        var index = event.data.index === 0 ? 0 : event.data.index || $target.index() * _.options.slidesToScroll;

        _.slideHandler(_.checkNavigable(index), false, dontAnimate);

        $target.children().trigger('focus');
        break;

      default:
        return;
    }
  };

  Slick.prototype.checkNavigable = function (index) {
    var _ = this,
        navigables,
        prevNavigable;

    navigables = _.getNavigableIndexes();
    prevNavigable = 0;

    if (index > navigables[navigables.length - 1]) {
      index = navigables[navigables.length - 1];
    } else {
      for (var n in navigables) {
        if (index < navigables[n]) {
          index = prevNavigable;
          break;
        }

        prevNavigable = navigables[n];
      }
    }

    return index;
  };

  Slick.prototype.cleanUpEvents = function () {
    var _ = this;

    if (_.options.dots && _.$dots !== null) {
      $('li', _.$dots).off('click.slick', _.changeSlide).off('mouseenter.slick', $.proxy(_.interrupt, _, true)).off('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }

    _.$slider.off('focus.slick blur.slick');

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
      _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);
    }

    _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);

    _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);

    _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);

    _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

    _.$list.off('click.slick', _.clickHandler);

    $(document).off(_.visibilityChange, _.visibility);

    _.cleanUpSlideEvents();

    if (_.options.accessibility === true) {
      _.$list.off('keydown.slick', _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().off('click.slick', _.selectHandler);
    }

    $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);
    $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);
    $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);
    $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);
    $(document).off('ready.slick.slick-' + _.instanceUid, _.setPosition);
  };

  Slick.prototype.cleanUpSlideEvents = function () {
    var _ = this;

    _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));

    _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
  };

  Slick.prototype.cleanUpRows = function () {
    var _ = this,
        originalSlides;

    if (_.options.rows > 1) {
      originalSlides = _.$slides.children().children();
      originalSlides.removeAttr('style');

      _.$slider.empty().append(originalSlides);
    }
  };

  Slick.prototype.clickHandler = function (event) {
    var _ = this;

    if (_.shouldClick === false) {
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  };

  Slick.prototype.destroy = function (refresh) {
    var _ = this;

    _.autoPlayClear();

    _.touchObject = {};

    _.cleanUpEvents();

    $('.slick-cloned', _.$slider).detach();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.$prevArrow.length) {
      _.$prevArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');

      if (_.htmlExpr.test(_.options.prevArrow)) {
        _.$prevArrow.remove();
      }
    }

    if (_.$nextArrow && _.$nextArrow.length) {
      _.$nextArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');

      if (_.htmlExpr.test(_.options.nextArrow)) {
        _.$nextArrow.remove();
      }
    }

    if (_.$slides) {
      _.$slides.removeClass('slick-slide slick-active slick-center slick-visible slick-current').removeAttr('aria-hidden').removeAttr('data-slick-index').each(function () {
        $(this).attr('style', $(this).data('originalStyling'));
      });

      _.$slideTrack.children(this.options.slide).detach();

      _.$slideTrack.detach();

      _.$list.detach();

      _.$slider.append(_.$slides);
    }

    _.cleanUpRows();

    _.$slider.removeClass('slick-slider');

    _.$slider.removeClass('slick-initialized');

    _.$slider.removeClass('slick-dotted');

    _.unslicked = true;

    if (!refresh) {
      _.$slider.trigger('destroy', [_]);
    }
  };

  Slick.prototype.disableTransition = function (slide) {
    var _ = this,
        transition = {};

    transition[_.transitionType] = '';

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.fadeSlide = function (slideIndex, callback) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).css({
        zIndex: _.options.zIndex
      });

      _.$slides.eq(slideIndex).animate({
        opacity: 1
      }, _.options.speed, _.options.easing, callback);
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 1,
        zIndex: _.options.zIndex
      });

      if (callback) {
        setTimeout(function () {
          _.disableTransition(slideIndex);

          callback.call();
        }, _.options.speed);
      }
    }
  };

  Slick.prototype.fadeSlideOut = function (slideIndex) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).animate({
        opacity: 0,
        zIndex: _.options.zIndex - 2
      }, _.options.speed, _.options.easing);
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 0,
        zIndex: _.options.zIndex - 2
      });
    }
  };

  Slick.prototype.filterSlides = Slick.prototype.slickFilter = function (filter) {
    var _ = this;

    if (filter !== null) {
      _.$slidesCache = _.$slides;

      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.focusHandler = function () {
    var _ = this;

    _.$slider.off('focus.slick blur.slick').on('focus.slick blur.slick', '*:not(.slick-arrow)', function (event) {
      event.stopImmediatePropagation();
      var $sf = $(this);
      setTimeout(function () {
        if (_.options.pauseOnFocus) {
          _.focussed = $sf.is(':focus');

          _.autoPlay();
        }
      }, 0);
    });
  };

  Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function () {
    var _ = this;

    return _.currentSlide;
  };

  Slick.prototype.getDotCount = function () {
    var _ = this;

    var breakPoint = 0;
    var counter = 0;
    var pagerQty = 0;

    if (_.options.infinite === true) {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
      }
    } else if (_.options.centerMode === true) {
      pagerQty = _.slideCount;
    } else if (!_.options.asNavFor) {
      pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
    } else {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
      }
    }

    return pagerQty - 1;
  };

  Slick.prototype.getLeft = function (slideIndex) {
    var _ = this,
        targetLeft,
        verticalHeight,
        verticalOffset = 0,
        targetSlide;

    _.slideOffset = 0;
    verticalHeight = _.$slides.first().outerHeight(true);

    if (_.options.infinite === true) {
      if (_.slideCount > _.options.slidesToShow) {
        _.slideOffset = _.slideWidth * _.options.slidesToShow * -1;
        verticalOffset = verticalHeight * _.options.slidesToShow * -1;
      }

      if (_.slideCount % _.options.slidesToScroll !== 0) {
        if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
          if (slideIndex > _.slideCount) {
            _.slideOffset = (_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth * -1;
            verticalOffset = (_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight * -1;
          } else {
            _.slideOffset = _.slideCount % _.options.slidesToScroll * _.slideWidth * -1;
            verticalOffset = _.slideCount % _.options.slidesToScroll * verticalHeight * -1;
          }
        }
      }
    } else {
      if (slideIndex + _.options.slidesToShow > _.slideCount) {
        _.slideOffset = (slideIndex + _.options.slidesToShow - _.slideCount) * _.slideWidth;
        verticalOffset = (slideIndex + _.options.slidesToShow - _.slideCount) * verticalHeight;
      }
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.slideOffset = 0;
      verticalOffset = 0;
    }

    if (_.options.centerMode === true && _.options.infinite === true) {
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
    } else if (_.options.centerMode === true) {
      _.slideOffset = 0;
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
    }

    if (_.options.vertical === false) {
      targetLeft = slideIndex * _.slideWidth * -1 + _.slideOffset;
    } else {
      targetLeft = slideIndex * verticalHeight * -1 + verticalOffset;
    }

    if (_.options.variableWidth === true) {
      if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
        targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
      } else {
        targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
      }

      if (_.options.rtl === true) {
        if (targetSlide[0]) {
          targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
        } else {
          targetLeft = 0;
        }
      } else {
        targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
      }

      if (_.options.centerMode === true) {
        if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
          targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
        } else {
          targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
        }

        if (_.options.rtl === true) {
          if (targetSlide[0]) {
            targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
          } else {
            targetLeft = 0;
          }
        } else {
          targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
        }

        targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
      }
    }

    return targetLeft;
  };

  Slick.prototype.getOption = Slick.prototype.slickGetOption = function (option) {
    var _ = this;

    return _.options[option];
  };

  Slick.prototype.getNavigableIndexes = function () {
    var _ = this,
        breakPoint = 0,
        counter = 0,
        indexes = [],
        max;

    if (_.options.infinite === false) {
      max = _.slideCount;
    } else {
      breakPoint = _.options.slidesToScroll * -1;
      counter = _.options.slidesToScroll * -1;
      max = _.slideCount * 2;
    }

    while (breakPoint < max) {
      indexes.push(breakPoint);
      breakPoint = counter + _.options.slidesToScroll;
      counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
    }

    return indexes;
  };

  Slick.prototype.getSlick = function () {
    return this;
  };

  Slick.prototype.getSlideCount = function () {
    var _ = this,
        slidesTraversed,
        swipedSlide,
        centerOffset;

    centerOffset = _.options.centerMode === true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0;

    if (_.options.swipeToSlide === true) {
      _.$slideTrack.find('.slick-slide').each(function (index, slide) {
        if (slide.offsetLeft - centerOffset + $(slide).outerWidth() / 2 > _.swipeLeft * -1) {
          swipedSlide = slide;
          return false;
        }
      });

      slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;
      return slidesTraversed;
    } else {
      return _.options.slidesToScroll;
    }
  };

  Slick.prototype.goTo = Slick.prototype.slickGoTo = function (slide, dontAnimate) {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'index',
        index: parseInt(slide)
      }
    }, dontAnimate);
  };

  Slick.prototype.init = function (creation) {
    var _ = this;

    if (!$(_.$slider).hasClass('slick-initialized')) {
      $(_.$slider).addClass('slick-initialized');

      _.buildRows();

      _.buildOut();

      _.setProps();

      _.startLoad();

      _.loadSlider();

      _.initializeEvents();

      _.updateArrows();

      _.updateDots();

      _.checkResponsive(true);

      _.focusHandler();
    }

    if (creation) {
      _.$slider.trigger('init', [_]);
    }

    if (_.options.accessibility === true) {
      _.initADA();
    }

    if (_.options.autoplay) {
      _.paused = false;

      _.autoPlay();
    }
  };

  Slick.prototype.initADA = function () {
    var _ = this;

    _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
      'aria-hidden': 'true',
      'tabindex': '-1'
    }).find('a, input, button, select').attr({
      'tabindex': '-1'
    });

    _.$slideTrack.attr('role', 'listbox');

    _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function (i) {
      $(this).attr({
        'role': 'option',
        'aria-describedby': 'slick-slide' + _.instanceUid + i + ''
      });
    });

    if (_.$dots !== null) {
      _.$dots.attr('role', 'tablist').find('li').each(function (i) {
        $(this).attr({
          'role': 'presentation',
          'aria-selected': 'false',
          'aria-controls': 'navigation' + _.instanceUid + i + '',
          'id': 'slick-slide' + _.instanceUid + i + ''
        });
      }).first().attr('aria-selected', 'true').end().find('button').attr('role', 'button').end().closest('div').attr('role', 'toolbar');
    }

    _.activateADA();
  };

  Slick.prototype.initArrowEvents = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.off('click.slick').on('click.slick', {
        message: 'previous'
      }, _.changeSlide);

      _.$nextArrow.off('click.slick').on('click.slick', {
        message: 'next'
      }, _.changeSlide);
    }
  };

  Slick.prototype.initDotEvents = function () {
    var _ = this;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      $('li', _.$dots).on('click.slick', {
        message: 'index'
      }, _.changeSlide);
    }

    if (_.options.dots === true && _.options.pauseOnDotsHover === true) {
      $('li', _.$dots).on('mouseenter.slick', $.proxy(_.interrupt, _, true)).on('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initSlideEvents = function () {
    var _ = this;

    if (_.options.pauseOnHover) {
      _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));

      _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initializeEvents = function () {
    var _ = this;

    _.initArrowEvents();

    _.initDotEvents();

    _.initSlideEvents();

    _.$list.on('touchstart.slick mousedown.slick', {
      action: 'start'
    }, _.swipeHandler);

    _.$list.on('touchmove.slick mousemove.slick', {
      action: 'move'
    }, _.swipeHandler);

    _.$list.on('touchend.slick mouseup.slick', {
      action: 'end'
    }, _.swipeHandler);

    _.$list.on('touchcancel.slick mouseleave.slick', {
      action: 'end'
    }, _.swipeHandler);

    _.$list.on('click.slick', _.clickHandler);

    $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

    if (_.options.accessibility === true) {
      _.$list.on('keydown.slick', _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on('click.slick', _.selectHandler);
    }

    $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));
    $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));
    $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);
    $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
    $(document).on('ready.slick.slick-' + _.instanceUid, _.setPosition);
  };

  Slick.prototype.initUI = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.show();

      _.$nextArrow.show();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.show();
    }
  };

  Slick.prototype.keyHandler = function (event) {
    var _ = this; //Dont slide if the cursor is inside the form fields and arrow keys are pressed


    if (!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
      if (event.keyCode === 37 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? 'next' : 'previous'
          }
        });
      } else if (event.keyCode === 39 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? 'previous' : 'next'
          }
        });
      }
    }
  };

  Slick.prototype.lazyLoad = function () {
    var _ = this,
        loadRange,
        cloneRange,
        rangeStart,
        rangeEnd;

    function loadImages(imagesScope) {
      $('img[data-lazy]', imagesScope).each(function () {
        var image = $(this),
            imageSource = $(this).attr('data-lazy'),
            imageToLoad = document.createElement('img');

        imageToLoad.onload = function () {
          image.animate({
            opacity: 0
          }, 100, function () {
            image.attr('src', imageSource).animate({
              opacity: 1
            }, 200, function () {
              image.removeAttr('data-lazy').removeClass('slick-loading');
            });

            _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
          });
        };

        imageToLoad.onerror = function () {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');

          _.$slider.trigger('lazyLoadError', [_, image, imageSource]);
        };

        imageToLoad.src = imageSource;
      });
    }

    if (_.options.centerMode === true) {
      if (_.options.infinite === true) {
        rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
        rangeEnd = rangeStart + _.options.slidesToShow + 2;
      } else {
        rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
        rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
      }
    } else {
      rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
      rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);

      if (_.options.fade === true) {
        if (rangeStart > 0) rangeStart--;
        if (rangeEnd <= _.slideCount) rangeEnd++;
      }
    }

    loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
    loadImages(loadRange);

    if (_.slideCount <= _.options.slidesToShow) {
      cloneRange = _.$slider.find('.slick-slide');
      loadImages(cloneRange);
    } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
      cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
      loadImages(cloneRange);
    } else if (_.currentSlide === 0) {
      cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
      loadImages(cloneRange);
    }
  };

  Slick.prototype.loadSlider = function () {
    var _ = this;

    _.setPosition();

    _.$slideTrack.css({
      opacity: 1
    });

    _.$slider.removeClass('slick-loading');

    _.initUI();

    if (_.options.lazyLoad === 'progressive') {
      _.progressiveLazyLoad();
    }
  };

  Slick.prototype.next = Slick.prototype.slickNext = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'next'
      }
    });
  };

  Slick.prototype.orientationChange = function () {
    var _ = this;

    _.checkResponsive();

    _.setPosition();
  };

  Slick.prototype.pause = Slick.prototype.slickPause = function () {
    var _ = this;

    _.autoPlayClear();

    _.paused = true;
  };

  Slick.prototype.play = Slick.prototype.slickPlay = function () {
    var _ = this;

    _.autoPlay();

    _.options.autoplay = true;
    _.paused = false;
    _.focussed = false;
    _.interrupted = false;
  };

  Slick.prototype.postSlide = function (index) {
    var _ = this;

    if (!_.unslicked) {
      _.$slider.trigger('afterChange', [_, index]);

      _.animating = false;

      _.setPosition();

      _.swipeLeft = null;

      if (_.options.autoplay) {
        _.autoPlay();
      }

      if (_.options.accessibility === true) {
        _.initADA();
      }
    }
  };

  Slick.prototype.prev = Slick.prototype.slickPrev = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'previous'
      }
    });
  };

  Slick.prototype.preventDefault = function (event) {
    event.preventDefault();
  };

  Slick.prototype.progressiveLazyLoad = function (tryCount) {
    tryCount = tryCount || 1;

    var _ = this,
        $imgsToLoad = $('img[data-lazy]', _.$slider),
        image,
        imageSource,
        imageToLoad;

    if ($imgsToLoad.length) {
      image = $imgsToLoad.first();
      imageSource = image.attr('data-lazy');
      imageToLoad = document.createElement('img');

      imageToLoad.onload = function () {
        image.attr('src', imageSource).removeAttr('data-lazy').removeClass('slick-loading');

        if (_.options.adaptiveHeight === true) {
          _.setPosition();
        }

        _.$slider.trigger('lazyLoaded', [_, image, imageSource]);

        _.progressiveLazyLoad();
      };

      imageToLoad.onerror = function () {
        if (tryCount < 3) {
          /**
           * try to load the image 3 times,
           * leave a slight delay so we don't get
           * servers blocking the request.
           */
          setTimeout(function () {
            _.progressiveLazyLoad(tryCount + 1);
          }, 500);
        } else {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');

          _.$slider.trigger('lazyLoadError', [_, image, imageSource]);

          _.progressiveLazyLoad();
        }
      };

      imageToLoad.src = imageSource;
    } else {
      _.$slider.trigger('allImagesLoaded', [_]);
    }
  };

  Slick.prototype.refresh = function (initializing) {
    var _ = this,
        currentSlide,
        lastVisibleIndex;

    lastVisibleIndex = _.slideCount - _.options.slidesToShow; // in non-infinite sliders, we don't want to go past the
    // last visible index.

    if (!_.options.infinite && _.currentSlide > lastVisibleIndex) {
      _.currentSlide = lastVisibleIndex;
    } // if less slides than to show, go to start.


    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    currentSlide = _.currentSlide;

    _.destroy(true);

    $.extend(_, _.initials, {
      currentSlide: currentSlide
    });

    _.init();

    if (!initializing) {
      _.changeSlide({
        data: {
          message: 'index',
          index: currentSlide
        }
      }, false);
    }
  };

  Slick.prototype.registerBreakpoints = function () {
    var _ = this,
        breakpoint,
        currentBreakpoint,
        l,
        responsiveSettings = _.options.responsive || null;

    if ($.type(responsiveSettings) === 'array' && responsiveSettings.length) {
      _.respondTo = _.options.respondTo || 'window';

      for (breakpoint in responsiveSettings) {
        l = _.breakpoints.length - 1;
        currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

        if (responsiveSettings.hasOwnProperty(breakpoint)) {
          // loop through the breakpoints and cut out any existing
          // ones with the same breakpoint number, we don't want dupes.
          while (l >= 0) {
            if (_.breakpoints[l] && _.breakpoints[l] === currentBreakpoint) {
              _.breakpoints.splice(l, 1);
            }

            l--;
          }

          _.breakpoints.push(currentBreakpoint);

          _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;
        }
      }

      _.breakpoints.sort(function (a, b) {
        return _.options.mobileFirst ? a - b : b - a;
      });
    }
  };

  Slick.prototype.reinit = function () {
    var _ = this;

    _.$slides = _.$slideTrack.children(_.options.slide).addClass('slick-slide');
    _.slideCount = _.$slides.length;

    if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
      _.currentSlide = _.currentSlide - _.options.slidesToScroll;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    _.registerBreakpoints();

    _.setProps();

    _.setupInfinite();

    _.buildArrows();

    _.updateArrows();

    _.initArrowEvents();

    _.buildDots();

    _.updateDots();

    _.initDotEvents();

    _.cleanUpSlideEvents();

    _.initSlideEvents();

    _.checkResponsive(false, true);

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on('click.slick', _.selectHandler);
    }

    _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

    _.setPosition();

    _.focusHandler();

    _.paused = !_.options.autoplay;

    _.autoPlay();

    _.$slider.trigger('reInit', [_]);
  };

  Slick.prototype.resize = function () {
    var _ = this;

    if ($(window).width() !== _.windowWidth) {
      clearTimeout(_.windowDelay);
      _.windowDelay = window.setTimeout(function () {
        _.windowWidth = $(window).width();

        _.checkResponsive();

        if (!_.unslicked) {
          _.setPosition();
        }
      }, 50);
    }
  };

  Slick.prototype.removeSlide = Slick.prototype.slickRemove = function (index, removeBefore, removeAll) {
    var _ = this;

    if (typeof index === 'boolean') {
      removeBefore = index;
      index = removeBefore === true ? 0 : _.slideCount - 1;
    } else {
      index = removeBefore === true ? --index : index;
    }

    if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
      return false;
    }

    _.unload();

    if (removeAll === true) {
      _.$slideTrack.children().remove();
    } else {
      _.$slideTrack.children(this.options.slide).eq(index).remove();
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.setCSS = function (position) {
    var _ = this,
        positionProps = {},
        x,
        y;

    if (_.options.rtl === true) {
      position = -position;
    }

    x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
    y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';
    positionProps[_.positionProp] = position;

    if (_.transformsEnabled === false) {
      _.$slideTrack.css(positionProps);
    } else {
      positionProps = {};

      if (_.cssTransitions === false) {
        positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';

        _.$slideTrack.css(positionProps);
      } else {
        positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';

        _.$slideTrack.css(positionProps);
      }
    }
  };

  Slick.prototype.setDimensions = function () {
    var _ = this;

    if (_.options.vertical === false) {
      if (_.options.centerMode === true) {
        _.$list.css({
          padding: '0px ' + _.options.centerPadding
        });
      }
    } else {
      _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);

      if (_.options.centerMode === true) {
        _.$list.css({
          padding: _.options.centerPadding + ' 0px'
        });
      }
    }

    _.listWidth = _.$list.width();
    _.listHeight = _.$list.height();

    if (_.options.vertical === false && _.options.variableWidth === false) {
      _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);

      _.$slideTrack.width(Math.ceil(_.slideWidth * _.$slideTrack.children('.slick-slide').length));
    } else if (_.options.variableWidth === true) {
      _.$slideTrack.width(5000 * _.slideCount);
    } else {
      _.slideWidth = Math.ceil(_.listWidth);

      _.$slideTrack.height(Math.ceil(_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length));
    }

    var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();

    if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);
  };

  Slick.prototype.setFade = function () {
    var _ = this,
        targetLeft;

    _.$slides.each(function (index, element) {
      targetLeft = _.slideWidth * index * -1;

      if (_.options.rtl === true) {
        $(element).css({
          position: 'relative',
          right: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0
        });
      } else {
        $(element).css({
          position: 'relative',
          left: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0
        });
      }
    });

    _.$slides.eq(_.currentSlide).css({
      zIndex: _.options.zIndex - 1,
      opacity: 1
    });
  };

  Slick.prototype.setHeight = function () {
    var _ = this;

    if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);

      _.$list.css('height', targetHeight);
    }
  };

  Slick.prototype.setOption = Slick.prototype.slickSetOption = function () {
    /**
     * accepts arguments in format of:
     *
     *  - for changing a single option's value:
     *     .slick("setOption", option, value, refresh )
     *
     *  - for changing a set of responsive options:
     *     .slick("setOption", 'responsive', [{}, ...], refresh )
     *
     *  - for updating multiple values at once (not responsive)
     *     .slick("setOption", { 'option': value, ... }, refresh )
     */
    var _ = this,
        l,
        item,
        option,
        value,
        refresh = false,
        type;

    if ($.type(arguments[0]) === 'object') {
      option = arguments[0];
      refresh = arguments[1];
      type = 'multiple';
    } else if ($.type(arguments[0]) === 'string') {
      option = arguments[0];
      value = arguments[1];
      refresh = arguments[2];

      if (arguments[0] === 'responsive' && $.type(arguments[1]) === 'array') {
        type = 'responsive';
      } else if (typeof arguments[1] !== 'undefined') {
        type = 'single';
      }
    }

    if (type === 'single') {
      _.options[option] = value;
    } else if (type === 'multiple') {
      $.each(option, function (opt, val) {
        _.options[opt] = val;
      });
    } else if (type === 'responsive') {
      for (item in value) {
        if ($.type(_.options.responsive) !== 'array') {
          _.options.responsive = [value[item]];
        } else {
          l = _.options.responsive.length - 1; // loop through the responsive object and splice out duplicates.

          while (l >= 0) {
            if (_.options.responsive[l].breakpoint === value[item].breakpoint) {
              _.options.responsive.splice(l, 1);
            }

            l--;
          }

          _.options.responsive.push(value[item]);
        }
      }
    }

    if (refresh) {
      _.unload();

      _.reinit();
    }
  };

  Slick.prototype.setPosition = function () {
    var _ = this;

    _.setDimensions();

    _.setHeight();

    if (_.options.fade === false) {
      _.setCSS(_.getLeft(_.currentSlide));
    } else {
      _.setFade();
    }

    _.$slider.trigger('setPosition', [_]);
  };

  Slick.prototype.setProps = function () {
    var _ = this,
        bodyStyle = document.body.style;

    _.positionProp = _.options.vertical === true ? 'top' : 'left';

    if (_.positionProp === 'top') {
      _.$slider.addClass('slick-vertical');
    } else {
      _.$slider.removeClass('slick-vertical');
    }

    if (bodyStyle.WebkitTransition !== undefined || bodyStyle.MozTransition !== undefined || bodyStyle.msTransition !== undefined) {
      if (_.options.useCSS === true) {
        _.cssTransitions = true;
      }
    }

    if (_.options.fade) {
      if (typeof _.options.zIndex === 'number') {
        if (_.options.zIndex < 3) {
          _.options.zIndex = 3;
        }
      } else {
        _.options.zIndex = _.defaults.zIndex;
      }
    }

    if (bodyStyle.OTransform !== undefined) {
      _.animType = 'OTransform';
      _.transformType = '-o-transform';
      _.transitionType = 'OTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.MozTransform !== undefined) {
      _.animType = 'MozTransform';
      _.transformType = '-moz-transform';
      _.transitionType = 'MozTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.webkitTransform !== undefined) {
      _.animType = 'webkitTransform';
      _.transformType = '-webkit-transform';
      _.transitionType = 'webkitTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.msTransform !== undefined) {
      _.animType = 'msTransform';
      _.transformType = '-ms-transform';
      _.transitionType = 'msTransition';
      if (bodyStyle.msTransform === undefined) _.animType = false;
    }

    if (bodyStyle.transform !== undefined && _.animType !== false) {
      _.animType = 'transform';
      _.transformType = 'transform';
      _.transitionType = 'transition';
    }

    _.transformsEnabled = _.options.useTransform && _.animType !== null && _.animType !== false;
  };

  Slick.prototype.setSlideClasses = function (index) {
    var _ = this,
        centerOffset,
        allSlides,
        indexOffset,
        remainder;

    allSlides = _.$slider.find('.slick-slide').removeClass('slick-active slick-center slick-current').attr('aria-hidden', 'true');

    _.$slides.eq(index).addClass('slick-current');

    if (_.options.centerMode === true) {
      centerOffset = Math.floor(_.options.slidesToShow / 2);

      if (_.options.infinite === true) {
        if (index >= centerOffset && index <= _.slideCount - 1 - centerOffset) {
          _.$slides.slice(index - centerOffset, index + centerOffset + 1).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          indexOffset = _.options.slidesToShow + index;
          allSlides.slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2).addClass('slick-active').attr('aria-hidden', 'false');
        }

        if (index === 0) {
          allSlides.eq(allSlides.length - 1 - _.options.slidesToShow).addClass('slick-center');
        } else if (index === _.slideCount - 1) {
          allSlides.eq(_.options.slidesToShow).addClass('slick-center');
        }
      }

      _.$slides.eq(index).addClass('slick-center');
    } else {
      if (index >= 0 && index <= _.slideCount - _.options.slidesToShow) {
        _.$slides.slice(index, index + _.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
      } else if (allSlides.length <= _.options.slidesToShow) {
        allSlides.addClass('slick-active').attr('aria-hidden', 'false');
      } else {
        remainder = _.slideCount % _.options.slidesToShow;
        indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

        if (_.options.slidesToShow == _.options.slidesToScroll && _.slideCount - index < _.options.slidesToShow) {
          allSlides.slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          allSlides.slice(indexOffset, indexOffset + _.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
        }
      }
    }

    if (_.options.lazyLoad === 'ondemand') {
      _.lazyLoad();
    }
  };

  Slick.prototype.setupInfinite = function () {
    var _ = this,
        i,
        slideIndex,
        infiniteCount;

    if (_.options.fade === true) {
      _.options.centerMode = false;
    }

    if (_.options.infinite === true && _.options.fade === false) {
      slideIndex = null;

      if (_.slideCount > _.options.slidesToShow) {
        if (_.options.centerMode === true) {
          infiniteCount = _.options.slidesToShow + 1;
        } else {
          infiniteCount = _.options.slidesToShow;
        }

        for (i = _.slideCount; i > _.slideCount - infiniteCount; i -= 1) {
          slideIndex = i - 1;
          $(_.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex - _.slideCount).prependTo(_.$slideTrack).addClass('slick-cloned');
        }

        for (i = 0; i < infiniteCount; i += 1) {
          slideIndex = i;
          $(_.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex + _.slideCount).appendTo(_.$slideTrack).addClass('slick-cloned');
        }

        _.$slideTrack.find('.slick-cloned').find('[id]').each(function () {
          $(this).attr('id', '');
        });
      }
    }
  };

  Slick.prototype.interrupt = function (toggle) {
    var _ = this;

    if (!toggle) {
      _.autoPlay();
    }

    _.interrupted = toggle;
  };

  Slick.prototype.selectHandler = function (event) {
    var _ = this;

    var targetElement = $(event.target).is('.slick-slide') ? $(event.target) : $(event.target).parents('.slick-slide');
    var index = parseInt(targetElement.attr('data-slick-index'));
    if (!index) index = 0;

    if (_.slideCount <= _.options.slidesToShow) {
      _.setSlideClasses(index);

      _.asNavFor(index);

      return;
    }

    _.slideHandler(index);
  };

  Slick.prototype.slideHandler = function (index, sync, dontAnimate) {
    var targetSlide,
        animSlide,
        oldSlide,
        slideLeft,
        targetLeft = null,
        _ = this,
        navTarget;

    sync = sync || false;

    if (_.animating === true && _.options.waitForAnimate === true) {
      return;
    }

    if (_.options.fade === true && _.currentSlide === index) {
      return;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      return;
    }

    if (sync === false) {
      _.asNavFor(index);
    }

    targetSlide = index;
    targetLeft = _.getLeft(targetSlide);
    slideLeft = _.getLeft(_.currentSlide);
    _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

    if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;

        if (dontAnimate !== true) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }

      return;
    } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > _.slideCount - _.options.slidesToScroll)) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;

        if (dontAnimate !== true) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }

      return;
    }

    if (_.options.autoplay) {
      clearInterval(_.autoPlayTimer);
    }

    if (targetSlide < 0) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = _.slideCount - _.slideCount % _.options.slidesToScroll;
      } else {
        animSlide = _.slideCount + targetSlide;
      }
    } else if (targetSlide >= _.slideCount) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = 0;
      } else {
        animSlide = targetSlide - _.slideCount;
      }
    } else {
      animSlide = targetSlide;
    }

    _.animating = true;

    _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

    oldSlide = _.currentSlide;
    _.currentSlide = animSlide;

    _.setSlideClasses(_.currentSlide);

    if (_.options.asNavFor) {
      navTarget = _.getNavTarget();
      navTarget = navTarget.slick('getSlick');

      if (navTarget.slideCount <= navTarget.options.slidesToShow) {
        navTarget.setSlideClasses(_.currentSlide);
      }
    }

    _.updateDots();

    _.updateArrows();

    if (_.options.fade === true) {
      if (dontAnimate !== true) {
        _.fadeSlideOut(oldSlide);

        _.fadeSlide(animSlide, function () {
          _.postSlide(animSlide);
        });
      } else {
        _.postSlide(animSlide);
      }

      _.animateHeight();

      return;
    }

    if (dontAnimate !== true) {
      _.animateSlide(targetLeft, function () {
        _.postSlide(animSlide);
      });
    } else {
      _.postSlide(animSlide);
    }
  };

  Slick.prototype.startLoad = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.hide();

      _.$nextArrow.hide();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.hide();
    }

    _.$slider.addClass('slick-loading');
  };

  Slick.prototype.swipeDirection = function () {
    var xDist,
        yDist,
        r,
        swipeAngle,
        _ = this;

    xDist = _.touchObject.startX - _.touchObject.curX;
    yDist = _.touchObject.startY - _.touchObject.curY;
    r = Math.atan2(yDist, xDist);
    swipeAngle = Math.round(r * 180 / Math.PI);

    if (swipeAngle < 0) {
      swipeAngle = 360 - Math.abs(swipeAngle);
    }

    if (swipeAngle <= 45 && swipeAngle >= 0) {
      return _.options.rtl === false ? 'left' : 'right';
    }

    if (swipeAngle <= 360 && swipeAngle >= 315) {
      return _.options.rtl === false ? 'left' : 'right';
    }

    if (swipeAngle >= 135 && swipeAngle <= 225) {
      return _.options.rtl === false ? 'right' : 'left';
    }

    if (_.options.verticalSwiping === true) {
      if (swipeAngle >= 35 && swipeAngle <= 135) {
        return 'down';
      } else {
        return 'up';
      }
    }

    return 'vertical';
  };

  Slick.prototype.swipeEnd = function (event) {
    var _ = this,
        slideCount,
        direction;

    _.dragging = false;
    _.interrupted = false;
    _.shouldClick = _.touchObject.swipeLength > 10 ? false : true;

    if (_.touchObject.curX === undefined) {
      return false;
    }

    if (_.touchObject.edgeHit === true) {
      _.$slider.trigger('edge', [_, _.swipeDirection()]);
    }

    if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {
      direction = _.swipeDirection();

      switch (direction) {
        case 'left':
        case 'down':
          slideCount = _.options.swipeToSlide ? _.checkNavigable(_.currentSlide + _.getSlideCount()) : _.currentSlide + _.getSlideCount();
          _.currentDirection = 0;
          break;

        case 'right':
        case 'up':
          slideCount = _.options.swipeToSlide ? _.checkNavigable(_.currentSlide - _.getSlideCount()) : _.currentSlide - _.getSlideCount();
          _.currentDirection = 1;
          break;

        default:
      }

      if (direction != 'vertical') {
        _.slideHandler(slideCount);

        _.touchObject = {};

        _.$slider.trigger('swipe', [_, direction]);
      }
    } else {
      if (_.touchObject.startX !== _.touchObject.curX) {
        _.slideHandler(_.currentSlide);

        _.touchObject = {};
      }
    }
  };

  Slick.prototype.swipeHandler = function (event) {
    var _ = this;

    if (_.options.swipe === false || 'ontouchend' in document && _.options.swipe === false) {
      return;
    } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
      return;
    }

    _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ? event.originalEvent.touches.length : 1;
    _.touchObject.minSwipe = _.listWidth / _.options.touchThreshold;

    if (_.options.verticalSwiping === true) {
      _.touchObject.minSwipe = _.listHeight / _.options.touchThreshold;
    }

    switch (event.data.action) {
      case 'start':
        _.swipeStart(event);

        break;

      case 'move':
        _.swipeMove(event);

        break;

      case 'end':
        _.swipeEnd(event);

        break;
    }
  };

  Slick.prototype.swipeMove = function (event) {
    var _ = this,
        edgeWasHit = false,
        curLeft,
        swipeDirection,
        swipeLength,
        positionOffset,
        touches;

    touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

    if (!_.dragging || touches && touches.length !== 1) {
      return false;
    }

    curLeft = _.getLeft(_.currentSlide);
    _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
    _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;
    _.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

    if (_.options.verticalSwiping === true) {
      _.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));
    }

    swipeDirection = _.swipeDirection();

    if (swipeDirection === 'vertical') {
      return;
    }

    if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
      event.preventDefault();
    }

    positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);

    if (_.options.verticalSwiping === true) {
      positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
    }

    swipeLength = _.touchObject.swipeLength;
    _.touchObject.edgeHit = false;

    if (_.options.infinite === false) {
      if (_.currentSlide === 0 && swipeDirection === 'right' || _.currentSlide >= _.getDotCount() && swipeDirection === 'left') {
        swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
        _.touchObject.edgeHit = true;
      }
    }

    if (_.options.vertical === false) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    } else {
      _.swipeLeft = curLeft + swipeLength * (_.$list.height() / _.listWidth) * positionOffset;
    }

    if (_.options.verticalSwiping === true) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    }

    if (_.options.fade === true || _.options.touchMove === false) {
      return false;
    }

    if (_.animating === true) {
      _.swipeLeft = null;
      return false;
    }

    _.setCSS(_.swipeLeft);
  };

  Slick.prototype.swipeStart = function (event) {
    var _ = this,
        touches;

    _.interrupted = true;

    if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
      _.touchObject = {};
      return false;
    }

    if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
      touches = event.originalEvent.touches[0];
    }

    _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
    _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;
    _.dragging = true;
  };

  Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function () {
    var _ = this;

    if (_.$slidesCache !== null) {
      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.unload = function () {
    var _ = this;

    $('.slick-cloned', _.$slider).remove();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
      _.$prevArrow.remove();
    }

    if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
      _.$nextArrow.remove();
    }

    _.$slides.removeClass('slick-slide slick-active slick-visible slick-current').attr('aria-hidden', 'true').css('width', '');
  };

  Slick.prototype.unslick = function (fromBreakpoint) {
    var _ = this;

    _.$slider.trigger('unslick', [_, fromBreakpoint]);

    _.destroy();
  };

  Slick.prototype.updateArrows = function () {
    var _ = this,
        centerOffset;

    centerOffset = Math.floor(_.options.slidesToShow / 2);

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow && !_.options.infinite) {
      _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

      _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

      if (_.currentSlide === 0) {
        _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {
        _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {
        _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      }
    }
  };

  Slick.prototype.updateDots = function () {
    var _ = this;

    if (_.$dots !== null) {
      _.$dots.find('li').removeClass('slick-active').attr('aria-hidden', 'true');

      _.$dots.find('li').eq(Math.floor(_.currentSlide / _.options.slidesToScroll)).addClass('slick-active').attr('aria-hidden', 'false');
    }
  };

  Slick.prototype.visibility = function () {
    var _ = this;

    if (_.options.autoplay) {
      if (document[_.hidden]) {
        _.interrupted = true;
      } else {
        _.interrupted = false;
      }
    }
  };

  $.fn.slick = function () {
    var _ = this,
        opt = arguments[0],
        args = Array.prototype.slice.call(arguments, 1),
        l = _.length,
        i,
        ret;

    for (i = 0; i < l; i++) {
      if (_typeof(opt) == 'object' || typeof opt == 'undefined') _[i].slick = new Slick(_[i], opt);else ret = _[i].slick[opt].apply(_[i].slick, args);
      if (typeof ret != 'undefined') return ret;
    }

    return _;
  };
});

;
;
;
;
;
"use strict";

(function ($) {})(jQuery);
"use strict";

(function ($) {
  var _toggleHeaderForm = function _toggleHeaderForm() {
    $(".header-form").toggleClass("active");
  };

  $(".notice").on("click", _toggleHeaderForm);
})(jQuery);
"use strict";

/**
 * Basic Slider
 */
(function ($) {
  if ($(".slider").length < 1) {
    return;
  }

  $(".slider").slick({
    slidesToShow: 1,
    arrows: false
  });
})(jQuery);
/**
 * Section Slider
 */


(function ($) {
  if ($(".section-slider").length < 1) {
    return;
  }

  $(".section-slider").slick({
    slidesToShow: 1,
    prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="33" viewBox="0 0 22 33"><path fill="#E1E3DF" d="M22.2388393,306.761161 L7.33258929,321.667411 C7.07812373,321.921876 6.77678746,322.049107 6.42857143,322.049107 C6.0803554,322.049107 5.77901913,321.921876 5.52455357,321.667411 L2.18973214,318.332589 C1.93526658,318.078124 1.80803571,317.776787 1.80803571,317.428571 C1.80803571,317.080355 1.93526658,316.779019 2.18973214,316.524554 L12.8571429,305.857143 L2.18973214,295.189732 C1.93526658,294.935267 1.80803571,294.63393 1.80803571,294.285714 C1.80803571,293.937498 1.93526658,293.636162 2.18973214,293.381696 L5.52455357,290.046875 C5.77901913,289.792409 6.0803554,289.665179 6.42857143,289.665179 C6.77678746,289.665179 7.07812373,289.792409 7.33258929,290.046875 L22.2388393,304.953125 C22.4933048,305.207591 22.6205357,305.508927 22.6205357,305.857143 C22.6205357,306.205359 22.4933048,306.506695 22.2388393,306.761161 Z" transform="rotate(-180 11.714 161.357)"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="34" viewBox="0 0 22 34"><path fill="#E1E3DF" d="M1172.23884,305.761161 L1157.33259,320.667411 C1157.07812,320.921876 1156.77679,321.049107 1156.42857,321.049107 C1156.08036,321.049107 1155.77902,320.921876 1155.52455,320.667411 L1152.18973,317.332589 C1151.93527,317.078124 1151.80804,316.776787 1151.80804,316.428571 C1151.80804,316.080355 1151.93527,315.779019 1152.18973,315.524554 L1162.85714,304.857143 L1152.18973,294.189732 C1151.93527,293.935267 1151.80804,293.63393 1151.80804,293.285714 C1151.80804,292.937498 1151.93527,292.636162 1152.18973,292.381696 L1155.52455,289.046875 C1155.77902,288.792409 1156.08036,288.665179 1156.42857,288.665179 C1156.77679,288.665179 1157.07812,288.792409 1157.33259,289.046875 L1172.23884,303.953125 C1172.4933,304.207591 1172.62054,304.508927 1172.62054,304.857143 C1172.62054,305.205359 1172.4933,305.506695 1172.23884,305.761161 Z" transform="translate(-1151 -288)"/></svg></button>'
  });
})(jQuery);
/**
 * Product Slider
 */


(function ($) {
  if ($(".product-slider").length < 1) {
    return;
  }

  $(".product-slider").slick({
    slidesToShow: 1,
    prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="33" viewBox="0 0 22 33"><path fill="#E1E3DF" d="M22.2388393,306.761161 L7.33258929,321.667411 C7.07812373,321.921876 6.77678746,322.049107 6.42857143,322.049107 C6.0803554,322.049107 5.77901913,321.921876 5.52455357,321.667411 L2.18973214,318.332589 C1.93526658,318.078124 1.80803571,317.776787 1.80803571,317.428571 C1.80803571,317.080355 1.93526658,316.779019 2.18973214,316.524554 L12.8571429,305.857143 L2.18973214,295.189732 C1.93526658,294.935267 1.80803571,294.63393 1.80803571,294.285714 C1.80803571,293.937498 1.93526658,293.636162 2.18973214,293.381696 L5.52455357,290.046875 C5.77901913,289.792409 6.0803554,289.665179 6.42857143,289.665179 C6.77678746,289.665179 7.07812373,289.792409 7.33258929,290.046875 L22.2388393,304.953125 C22.4933048,305.207591 22.6205357,305.508927 22.6205357,305.857143 C22.6205357,306.205359 22.4933048,306.506695 22.2388393,306.761161 Z" transform="rotate(-180 11.714 161.357)"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="34" viewBox="0 0 22 34"><path fill="#E1E3DF" d="M1172.23884,305.761161 L1157.33259,320.667411 C1157.07812,320.921876 1156.77679,321.049107 1156.42857,321.049107 C1156.08036,321.049107 1155.77902,320.921876 1155.52455,320.667411 L1152.18973,317.332589 C1151.93527,317.078124 1151.80804,316.776787 1151.80804,316.428571 C1151.80804,316.080355 1151.93527,315.779019 1152.18973,315.524554 L1162.85714,304.857143 L1152.18973,294.189732 C1151.93527,293.935267 1151.80804,293.63393 1151.80804,293.285714 C1151.80804,292.937498 1151.93527,292.636162 1152.18973,292.381696 L1155.52455,289.046875 C1155.77902,288.792409 1156.08036,288.665179 1156.42857,288.665179 C1156.77679,288.665179 1157.07812,288.792409 1157.33259,289.046875 L1172.23884,303.953125 C1172.4933,304.207591 1172.62054,304.508927 1172.62054,304.857143 C1172.62054,305.205359 1172.4933,305.506695 1172.23884,305.761161 Z" transform="translate(-1151 -288)"/></svg></button>'
  });
})(jQuery);
"use strict";

var lazyImageLoad = function lazyImageLoad() {
  var lazyImages = document.querySelectorAll(".lazyload");
  var inAdvance = 300;
  lazyImages.forEach(function (image) {
    if (image.offsetParent == null) {
      return;
    }

    if (image.classList.contains("loaded")) {
      return;
    }

    if (image.offsetTop < window.innerHeight + window.pageYOffset + inAdvance) {
      if (image.dataset.src != undefined) {
        image.src = image.dataset.src;
      }

      if (image.dataset.bg != undefined) {
        image.style.backgroundImage = "url(" + image.dataset.bg + ")";
      }

      if (image.dataset.iframe != undefined) {
        image.src = image.dataset.iframe;
      }

      image.onload = function () {
        image.classList.add("loaded");
      };
    }
  });
};

(function ($) {
  $(document).on("ready", lazyImageLoad);
  window.addEventListener("scroll", lazyImageLoad);
  window.addEventListener("resize", lazyImageLoad);
})(jQuery);
"use strict";

/*! jQuery & Zepto Lazy v1.7.10 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2018 Daniel 'Eisbehr' Kern */
!function (t, e) {
  "use strict";

  function r(r, a, i, u, l) {
    function f() {
      L = t.devicePixelRatio > 1, i = c(i), a.delay >= 0 && setTimeout(function () {
        s(!0);
      }, a.delay), (a.delay < 0 || a.combined) && (u.e = v(a.throttle, function (t) {
        "resize" === t.type && (w = B = -1), s(t.all);
      }), u.a = function (t) {
        t = c(t), i.push.apply(i, t);
      }, u.g = function () {
        return i = n(i).filter(function () {
          return !n(this).data(a.loadedName);
        });
      }, u.f = function (t) {
        for (var e = 0; e < t.length; e++) {
          var r = i.filter(function () {
            return this === t[e];
          });
          r.length && s(!1, r);
        }
      }, s(), n(a.appendScroll).on("scroll." + l + " resize." + l, u.e));
    }

    function c(t) {
      var i = a.defaultImage,
          o = a.placeholder,
          u = a.imageBase,
          l = a.srcsetAttribute,
          f = a.loaderAttribute,
          c = a._f || {};
      t = n(t).filter(function () {
        var t = n(this),
            r = m(this);
        return !t.data(a.handledName) && (t.attr(a.attribute) || t.attr(l) || t.attr(f) || c[r] !== e);
      }).data("plugin_" + a.name, r);

      for (var s = 0, d = t.length; s < d; s++) {
        var A = n(t[s]),
            g = m(t[s]),
            h = A.attr(a.imageBaseAttribute) || u;
        g === N && h && A.attr(l) && A.attr(l, b(A.attr(l), h)), c[g] === e || A.attr(f) || A.attr(f, c[g]), g === N && i && !A.attr(E) ? A.attr(E, i) : g === N || !o || A.css(O) && "none" !== A.css(O) || A.css(O, "url('" + o + "')");
      }

      return t;
    }

    function s(t, e) {
      if (!i.length) return void (a.autoDestroy && r.destroy());

      for (var o = e || i, u = !1, l = a.imageBase || "", f = a.srcsetAttribute, c = a.handledName, s = 0; s < o.length; s++) {
        if (t || e || A(o[s])) {
          var g = n(o[s]),
              h = m(o[s]),
              b = g.attr(a.attribute),
              v = g.attr(a.imageBaseAttribute) || l,
              p = g.attr(a.loaderAttribute);
          g.data(c) || a.visibleOnly && !g.is(":visible") || !((b || g.attr(f)) && (h === N && (v + b !== g.attr(E) || g.attr(f) !== g.attr(F)) || h !== N && v + b !== g.css(O)) || p) || (u = !0, g.data(c, !0), d(g, h, v, p));
        }
      }

      u && (i = n(i).filter(function () {
        return !n(this).data(c);
      }));
    }

    function d(t, e, r, i) {
      ++z;

      var _o = function o() {
        y("onError", t), p(), _o = n.noop;
      };

      y("beforeLoad", t);
      var u = a.attribute,
          l = a.srcsetAttribute,
          f = a.sizesAttribute,
          c = a.retinaAttribute,
          s = a.removeAttribute,
          d = a.loadedName,
          A = t.attr(c);

      if (i) {
        var _g = function g() {
          s && t.removeAttr(a.loaderAttribute), t.data(d, !0), y(T, t), setTimeout(p, 1), _g = n.noop;
        };

        t.off(I).one(I, _o).one(D, _g), y(i, t, function (e) {
          e ? (t.off(D), _g()) : (t.off(I), _o());
        }) || t.trigger(I);
      } else {
        var h = n(new Image());
        h.one(I, _o).one(D, function () {
          t.hide(), e === N ? t.attr(C, h.attr(C)).attr(F, h.attr(F)).attr(E, h.attr(E)) : t.css(O, "url('" + h.attr(E) + "')"), t[a.effect](a.effectTime), s && (t.removeAttr(u + " " + l + " " + c + " " + a.imageBaseAttribute), f !== C && t.removeAttr(f)), t.data(d, !0), y(T, t), h.remove(), p();
        });
        var m = (L && A ? A : t.attr(u)) || "";
        h.attr(C, t.attr(f)).attr(F, t.attr(l)).attr(E, m ? r + m : null), h.complete && h.trigger(D);
      }
    }

    function A(t) {
      var e = t.getBoundingClientRect(),
          r = a.scrollDirection,
          n = a.threshold,
          i = h() + n > e.top && -n < e.bottom,
          o = g() + n > e.left && -n < e.right;
      return "vertical" === r ? i : "horizontal" === r ? o : i && o;
    }

    function g() {
      return w >= 0 ? w : w = n(t).width();
    }

    function h() {
      return B >= 0 ? B : B = n(t).height();
    }

    function m(t) {
      return t.tagName.toLowerCase();
    }

    function b(t, e) {
      if (e) {
        var r = t.split(",");
        t = "";

        for (var a = 0, n = r.length; a < n; a++) {
          t += e + r[a].trim() + (a !== n - 1 ? "," : "");
        }
      }

      return t;
    }

    function v(t, e) {
      var n,
          i = 0;
      return function (o, u) {
        function l() {
          i = +new Date(), e.call(r, o);
        }

        var f = +new Date() - i;
        n && clearTimeout(n), f > t || !a.enableThrottle || u ? l() : n = setTimeout(l, t - f);
      };
    }

    function p() {
      --z, i.length || z || y("onFinishedAll");
    }

    function y(t, e, n) {
      return !!(t = a[t]) && (t.apply(r, [].slice.call(arguments, 1)), !0);
    }

    var z = 0,
        w = -1,
        B = -1,
        L = !1,
        T = "afterLoad",
        D = "load",
        I = "error",
        N = "img",
        E = "src",
        F = "srcset",
        C = "sizes",
        O = "background-image";
    "event" === a.bind || o ? f() : n(t).on(D + "." + l, f);
  }

  function a(a, o) {
    var u = this,
        l = n.extend({}, u.config, o),
        f = {},
        c = l.name + "-" + ++i;
    return u.config = function (t, r) {
      return r === e ? l[t] : (l[t] = r, u);
    }, u.addItems = function (t) {
      return f.a && f.a("string" === n.type(t) ? n(t) : t), u;
    }, u.getItems = function () {
      return f.g ? f.g() : {};
    }, u.update = function (t) {
      return f.e && f.e({}, !t), u;
    }, u.force = function (t) {
      return f.f && f.f("string" === n.type(t) ? n(t) : t), u;
    }, u.loadAll = function () {
      return f.e && f.e({
        all: !0
      }, !0), u;
    }, u.destroy = function () {
      return n(l.appendScroll).off("." + c, f.e), n(t).off("." + c), f = {}, e;
    }, r(u, l, a, f, c), l.chainable ? a : u;
  }

  var n = t.jQuery || t.Zepto,
      i = 0,
      o = !1;
  n.fn.Lazy = n.fn.lazy = function (t) {
    return new a(this, t);
  }, n.Lazy = n.lazy = function (t, r, i) {
    if (n.isFunction(r) && (i = r, r = []), n.isFunction(i)) {
      t = n.isArray(t) ? t : [t], r = n.isArray(r) ? r : [r];

      for (var o = a.prototype.config, u = o._f || (o._f = {}), l = 0, f = t.length; l < f; l++) {
        (o[t[l]] === e || n.isFunction(o[t[l]])) && (o[t[l]] = i);
      }

      for (var c = 0, s = r.length; c < s; c++) {
        u[r[c]] = t[0];
      }
    }
  }, a.prototype.config = {
    name: "lazy",
    chainable: !0,
    autoDestroy: !0,
    bind: "load",
    threshold: 500,
    visibleOnly: !1,
    appendScroll: t,
    scrollDirection: "both",
    imageBase: null,
    defaultImage: "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
    placeholder: null,
    delay: -1,
    combined: !1,
    attribute: "data-src",
    srcsetAttribute: "data-srcset",
    sizesAttribute: "data-sizes",
    retinaAttribute: "data-retina",
    loaderAttribute: "data-loader",
    imageBaseAttribute: "data-imagebase",
    removeAttribute: !0,
    handledName: "handled",
    loadedName: "loaded",
    effect: "show",
    effectTime: 0,
    enableThrottle: !0,
    throttle: 250,
    beforeLoad: e,
    afterLoad: e,
    onError: e,
    onFinishedAll: e
  }, n(t).on("load", function () {
    o = !0;
  });
}(window);
"use strict";

(function ($) {
  var $header = $("#header");
  var $menu = $header.find("#menu");

  var toggleActive = function toggleActive(event) {
    event.preventDefault();
    $menu.toggleClass("change");
    $header.toggleClass("change");
    $header.find(".bars").toggleClass("change");
    $("body, html").toggleClass("no-scroll");
  };

  var mobile_open_menu = function mobile_open_menu() {
    $(this).next().toggleClass("mobile-drop-open");
    $(this).toggleClass("rotate-dropdown-button");
  };

  $menu.on("click", ".bars", toggleActive);
  $header.on("click", ".dropdown-button", mobile_open_menu);
})(jQuery);
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.6.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */

/* global window, document, define, jQuery, setInterval, clearInterval */
(function (factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }
})(function ($) {
  'use strict';

  var Slick = window.Slick || {};

  Slick = function () {
    var instanceUid = 0;

    function Slick(element, settings) {
      var _ = this,
          dataSettings;

      _.defaults = {
        accessibility: true,
        adaptiveHeight: false,
        appendArrows: $(element),
        appendDots: $(element),
        arrows: true,
        asNavFor: null,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        customPaging: function customPaging(slider, i) {
          return $('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1);
        },
        dots: false,
        dotsClass: 'slick-dots',
        draggable: true,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: true,
        swipeToSlide: false,
        touchMove: true,
        touchThreshold: 5,
        useCSS: true,
        useTransform: true,
        variableWidth: false,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      };
      _.initials = {
        animating: false,
        dragging: false,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: false,
        slideOffset: 0,
        swipeLeft: null,
        $list: null,
        touchObject: {},
        transformsEnabled: false,
        unslicked: false
      };
      $.extend(_, _.initials);
      _.activeBreakpoint = null;
      _.animType = null;
      _.animProp = null;
      _.breakpoints = [];
      _.breakpointSettings = [];
      _.cssTransitions = false;
      _.focussed = false;
      _.interrupted = false;
      _.hidden = 'hidden';
      _.paused = true;
      _.positionProp = null;
      _.respondTo = null;
      _.rowCount = 1;
      _.shouldClick = true;
      _.$slider = $(element);
      _.$slidesCache = null;
      _.transformType = null;
      _.transitionType = null;
      _.visibilityChange = 'visibilitychange';
      _.windowWidth = 0;
      _.windowTimer = null;
      dataSettings = $(element).data('slick') || {};
      _.options = $.extend({}, _.defaults, settings, dataSettings);
      _.currentSlide = _.options.initialSlide;
      _.originalSettings = _.options;

      if (typeof document.mozHidden !== 'undefined') {
        _.hidden = 'mozHidden';
        _.visibilityChange = 'mozvisibilitychange';
      } else if (typeof document.webkitHidden !== 'undefined') {
        _.hidden = 'webkitHidden';
        _.visibilityChange = 'webkitvisibilitychange';
      }

      _.autoPlay = $.proxy(_.autoPlay, _);
      _.autoPlayClear = $.proxy(_.autoPlayClear, _);
      _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
      _.changeSlide = $.proxy(_.changeSlide, _);
      _.clickHandler = $.proxy(_.clickHandler, _);
      _.selectHandler = $.proxy(_.selectHandler, _);
      _.setPosition = $.proxy(_.setPosition, _);
      _.swipeHandler = $.proxy(_.swipeHandler, _);
      _.dragHandler = $.proxy(_.dragHandler, _);
      _.keyHandler = $.proxy(_.keyHandler, _);
      _.instanceUid = instanceUid++; // A simple way to check for HTML strings
      // Strict HTML recognition (must start with <)
      // Extracted from jQuery v1.11 source

      _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;

      _.registerBreakpoints();

      _.init(true);
    }

    return Slick;
  }();

  Slick.prototype.activateADA = function () {
    var _ = this;

    _.$slideTrack.find('.slick-active').attr({
      'aria-hidden': 'false'
    }).find('a, input, button, select').attr({
      'tabindex': '0'
    });
  };

  Slick.prototype.addSlide = Slick.prototype.slickAdd = function (markup, index, addBefore) {
    var _ = this;

    if (typeof index === 'boolean') {
      addBefore = index;
      index = null;
    } else if (index < 0 || index >= _.slideCount) {
      return false;
    }

    _.unload();

    if (typeof index === 'number') {
      if (index === 0 && _.$slides.length === 0) {
        $(markup).appendTo(_.$slideTrack);
      } else if (addBefore) {
        $(markup).insertBefore(_.$slides.eq(index));
      } else {
        $(markup).insertAfter(_.$slides.eq(index));
      }
    } else {
      if (addBefore === true) {
        $(markup).prependTo(_.$slideTrack);
      } else {
        $(markup).appendTo(_.$slideTrack);
      }
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slides.each(function (index, element) {
      $(element).attr('data-slick-index', index);
    });

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.animateHeight = function () {
    var _ = this;

    if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);

      _.$list.animate({
        height: targetHeight
      }, _.options.speed);
    }
  };

  Slick.prototype.animateSlide = function (targetLeft, callback) {
    var animProps = {},
        _ = this;

    _.animateHeight();

    if (_.options.rtl === true && _.options.vertical === false) {
      targetLeft = -targetLeft;
    }

    if (_.transformsEnabled === false) {
      if (_.options.vertical === false) {
        _.$slideTrack.animate({
          left: targetLeft
        }, _.options.speed, _.options.easing, callback);
      } else {
        _.$slideTrack.animate({
          top: targetLeft
        }, _.options.speed, _.options.easing, callback);
      }
    } else {
      if (_.cssTransitions === false) {
        if (_.options.rtl === true) {
          _.currentLeft = -_.currentLeft;
        }

        $({
          animStart: _.currentLeft
        }).animate({
          animStart: targetLeft
        }, {
          duration: _.options.speed,
          easing: _.options.easing,
          step: function step(now) {
            now = Math.ceil(now);

            if (_.options.vertical === false) {
              animProps[_.animType] = 'translate(' + now + 'px, 0px)';

              _.$slideTrack.css(animProps);
            } else {
              animProps[_.animType] = 'translate(0px,' + now + 'px)';

              _.$slideTrack.css(animProps);
            }
          },
          complete: function complete() {
            if (callback) {
              callback.call();
            }
          }
        });
      } else {
        _.applyTransition();

        targetLeft = Math.ceil(targetLeft);

        if (_.options.vertical === false) {
          animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
        } else {
          animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
        }

        _.$slideTrack.css(animProps);

        if (callback) {
          setTimeout(function () {
            _.disableTransition();

            callback.call();
          }, _.options.speed);
        }
      }
    }
  };

  Slick.prototype.getNavTarget = function () {
    var _ = this,
        asNavFor = _.options.asNavFor;

    if (asNavFor && asNavFor !== null) {
      asNavFor = $(asNavFor).not(_.$slider);
    }

    return asNavFor;
  };

  Slick.prototype.asNavFor = function (index) {
    var _ = this,
        asNavFor = _.getNavTarget();

    if (asNavFor !== null && _typeof(asNavFor) === 'object') {
      asNavFor.each(function () {
        var target = $(this).slick('getSlick');

        if (!target.unslicked) {
          target.slideHandler(index, true);
        }
      });
    }
  };

  Slick.prototype.applyTransition = function (slide) {
    var _ = this,
        transition = {};

    if (_.options.fade === false) {
      transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
    } else {
      transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
    }

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.autoPlay = function () {
    var _ = this;

    _.autoPlayClear();

    if (_.slideCount > _.options.slidesToShow) {
      _.autoPlayTimer = setInterval(_.autoPlayIterator, _.options.autoplaySpeed);
    }
  };

  Slick.prototype.autoPlayClear = function () {
    var _ = this;

    if (_.autoPlayTimer) {
      clearInterval(_.autoPlayTimer);
    }
  };

  Slick.prototype.autoPlayIterator = function () {
    var _ = this,
        slideTo = _.currentSlide + _.options.slidesToScroll;

    if (!_.paused && !_.interrupted && !_.focussed) {
      if (_.options.infinite === false) {
        if (_.direction === 1 && _.currentSlide + 1 === _.slideCount - 1) {
          _.direction = 0;
        } else if (_.direction === 0) {
          slideTo = _.currentSlide - _.options.slidesToScroll;

          if (_.currentSlide - 1 === 0) {
            _.direction = 1;
          }
        }
      }

      _.slideHandler(slideTo);
    }
  };

  Slick.prototype.buildArrows = function () {
    var _ = this;

    if (_.options.arrows === true) {
      _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
      _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

      if (_.slideCount > _.options.slidesToShow) {
        _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

        _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

        if (_.htmlExpr.test(_.options.prevArrow)) {
          _.$prevArrow.prependTo(_.options.appendArrows);
        }

        if (_.htmlExpr.test(_.options.nextArrow)) {
          _.$nextArrow.appendTo(_.options.appendArrows);
        }

        if (_.options.infinite !== true) {
          _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        }
      } else {
        _.$prevArrow.add(_.$nextArrow).addClass('slick-hidden').attr({
          'aria-disabled': 'true',
          'tabindex': '-1'
        });
      }
    }
  };

  Slick.prototype.buildDots = function () {
    var _ = this,
        i,
        dot;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$slider.addClass('slick-dotted');

      dot = $('<ul />').addClass(_.options.dotsClass);

      for (i = 0; i <= _.getDotCount(); i += 1) {
        dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
      }

      _.$dots = dot.appendTo(_.options.appendDots);

      _.$dots.find('li').first().addClass('slick-active').attr('aria-hidden', 'false');
    }
  };

  Slick.prototype.buildOut = function () {
    var _ = this;

    _.$slides = _.$slider.children(_.options.slide + ':not(.slick-cloned)').addClass('slick-slide');
    _.slideCount = _.$slides.length;

    _.$slides.each(function (index, element) {
      $(element).attr('data-slick-index', index).data('originalStyling', $(element).attr('style') || '');
    });

    _.$slider.addClass('slick-slider');

    _.$slideTrack = _.slideCount === 0 ? $('<div class="slick-track"/>').appendTo(_.$slider) : _.$slides.wrapAll('<div class="slick-track"/>').parent();
    _.$list = _.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent();

    _.$slideTrack.css('opacity', 0);

    if (_.options.centerMode === true || _.options.swipeToSlide === true) {
      _.options.slidesToScroll = 1;
    }

    $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

    _.setupInfinite();

    _.buildArrows();

    _.buildDots();

    _.updateDots();

    _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

    if (_.options.draggable === true) {
      _.$list.addClass('draggable');
    }
  };

  Slick.prototype.buildRows = function () {
    var _ = this,
        a,
        b,
        c,
        newSlides,
        numOfSlides,
        originalSlides,
        slidesPerSection;

    newSlides = document.createDocumentFragment();
    originalSlides = _.$slider.children();

    if (_.options.rows > 1) {
      slidesPerSection = _.options.slidesPerRow * _.options.rows;
      numOfSlides = Math.ceil(originalSlides.length / slidesPerSection);

      for (a = 0; a < numOfSlides; a++) {
        var slide = document.createElement('div');

        for (b = 0; b < _.options.rows; b++) {
          var row = document.createElement('div');

          for (c = 0; c < _.options.slidesPerRow; c++) {
            var target = a * slidesPerSection + (b * _.options.slidesPerRow + c);

            if (originalSlides.get(target)) {
              row.appendChild(originalSlides.get(target));
            }
          }

          slide.appendChild(row);
        }

        newSlides.appendChild(slide);
      }

      _.$slider.empty().append(newSlides);

      _.$slider.children().children().children().css({
        'width': 100 / _.options.slidesPerRow + '%',
        'display': 'inline-block'
      });
    }
  };

  Slick.prototype.checkResponsive = function (initial, forceUpdate) {
    var _ = this,
        breakpoint,
        targetBreakpoint,
        respondToWidth,
        triggerBreakpoint = false;

    var sliderWidth = _.$slider.width();

    var windowWidth = window.innerWidth || $(window).width();

    if (_.respondTo === 'window') {
      respondToWidth = windowWidth;
    } else if (_.respondTo === 'slider') {
      respondToWidth = sliderWidth;
    } else if (_.respondTo === 'min') {
      respondToWidth = Math.min(windowWidth, sliderWidth);
    }

    if (_.options.responsive && _.options.responsive.length && _.options.responsive !== null) {
      targetBreakpoint = null;

      for (breakpoint in _.breakpoints) {
        if (_.breakpoints.hasOwnProperty(breakpoint)) {
          if (_.originalSettings.mobileFirst === false) {
            if (respondToWidth < _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          } else {
            if (respondToWidth > _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          }
        }
      }

      if (targetBreakpoint !== null) {
        if (_.activeBreakpoint !== null) {
          if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
            _.activeBreakpoint = targetBreakpoint;

            if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
              _.unslick(targetBreakpoint);
            } else {
              _.options = $.extend({}, _.originalSettings, _.breakpointSettings[targetBreakpoint]);

              if (initial === true) {
                _.currentSlide = _.options.initialSlide;
              }

              _.refresh(initial);
            }

            triggerBreakpoint = targetBreakpoint;
          }
        } else {
          _.activeBreakpoint = targetBreakpoint;

          if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
            _.unslick(targetBreakpoint);
          } else {
            _.options = $.extend({}, _.originalSettings, _.breakpointSettings[targetBreakpoint]);

            if (initial === true) {
              _.currentSlide = _.options.initialSlide;
            }

            _.refresh(initial);
          }

          triggerBreakpoint = targetBreakpoint;
        }
      } else {
        if (_.activeBreakpoint !== null) {
          _.activeBreakpoint = null;
          _.options = _.originalSettings;

          if (initial === true) {
            _.currentSlide = _.options.initialSlide;
          }

          _.refresh(initial);

          triggerBreakpoint = targetBreakpoint;
        }
      } // only trigger breakpoints during an actual break. not on initialize.


      if (!initial && triggerBreakpoint !== false) {
        _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
      }
    }
  };

  Slick.prototype.changeSlide = function (event, dontAnimate) {
    var _ = this,
        $target = $(event.currentTarget),
        indexOffset,
        slideOffset,
        unevenOffset; // If target is a link, prevent default action.


    if ($target.is('a')) {
      event.preventDefault();
    } // If target is not the <li> element (ie: a child), find the <li>.


    if (!$target.is('li')) {
      $target = $target.closest('li');
    }

    unevenOffset = _.slideCount % _.options.slidesToScroll !== 0;
    indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

    switch (event.data.message) {
      case 'previous':
        slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;

        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
        }

        break;

      case 'next':
        slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;

        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
        }

        break;

      case 'index':
        var index = event.data.index === 0 ? 0 : event.data.index || $target.index() * _.options.slidesToScroll;

        _.slideHandler(_.checkNavigable(index), false, dontAnimate);

        $target.children().trigger('focus');
        break;

      default:
        return;
    }
  };

  Slick.prototype.checkNavigable = function (index) {
    var _ = this,
        navigables,
        prevNavigable;

    navigables = _.getNavigableIndexes();
    prevNavigable = 0;

    if (index > navigables[navigables.length - 1]) {
      index = navigables[navigables.length - 1];
    } else {
      for (var n in navigables) {
        if (index < navigables[n]) {
          index = prevNavigable;
          break;
        }

        prevNavigable = navigables[n];
      }
    }

    return index;
  };

  Slick.prototype.cleanUpEvents = function () {
    var _ = this;

    if (_.options.dots && _.$dots !== null) {
      $('li', _.$dots).off('click.slick', _.changeSlide).off('mouseenter.slick', $.proxy(_.interrupt, _, true)).off('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }

    _.$slider.off('focus.slick blur.slick');

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
      _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);
    }

    _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);

    _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);

    _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);

    _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

    _.$list.off('click.slick', _.clickHandler);

    $(document).off(_.visibilityChange, _.visibility);

    _.cleanUpSlideEvents();

    if (_.options.accessibility === true) {
      _.$list.off('keydown.slick', _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().off('click.slick', _.selectHandler);
    }

    $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);
    $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);
    $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);
    $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);
    $(document).off('ready.slick.slick-' + _.instanceUid, _.setPosition);
  };

  Slick.prototype.cleanUpSlideEvents = function () {
    var _ = this;

    _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));

    _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
  };

  Slick.prototype.cleanUpRows = function () {
    var _ = this,
        originalSlides;

    if (_.options.rows > 1) {
      originalSlides = _.$slides.children().children();
      originalSlides.removeAttr('style');

      _.$slider.empty().append(originalSlides);
    }
  };

  Slick.prototype.clickHandler = function (event) {
    var _ = this;

    if (_.shouldClick === false) {
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  };

  Slick.prototype.destroy = function (refresh) {
    var _ = this;

    _.autoPlayClear();

    _.touchObject = {};

    _.cleanUpEvents();

    $('.slick-cloned', _.$slider).detach();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.$prevArrow.length) {
      _.$prevArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');

      if (_.htmlExpr.test(_.options.prevArrow)) {
        _.$prevArrow.remove();
      }
    }

    if (_.$nextArrow && _.$nextArrow.length) {
      _.$nextArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');

      if (_.htmlExpr.test(_.options.nextArrow)) {
        _.$nextArrow.remove();
      }
    }

    if (_.$slides) {
      _.$slides.removeClass('slick-slide slick-active slick-center slick-visible slick-current').removeAttr('aria-hidden').removeAttr('data-slick-index').each(function () {
        $(this).attr('style', $(this).data('originalStyling'));
      });

      _.$slideTrack.children(this.options.slide).detach();

      _.$slideTrack.detach();

      _.$list.detach();

      _.$slider.append(_.$slides);
    }

    _.cleanUpRows();

    _.$slider.removeClass('slick-slider');

    _.$slider.removeClass('slick-initialized');

    _.$slider.removeClass('slick-dotted');

    _.unslicked = true;

    if (!refresh) {
      _.$slider.trigger('destroy', [_]);
    }
  };

  Slick.prototype.disableTransition = function (slide) {
    var _ = this,
        transition = {};

    transition[_.transitionType] = '';

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.fadeSlide = function (slideIndex, callback) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).css({
        zIndex: _.options.zIndex
      });

      _.$slides.eq(slideIndex).animate({
        opacity: 1
      }, _.options.speed, _.options.easing, callback);
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 1,
        zIndex: _.options.zIndex
      });

      if (callback) {
        setTimeout(function () {
          _.disableTransition(slideIndex);

          callback.call();
        }, _.options.speed);
      }
    }
  };

  Slick.prototype.fadeSlideOut = function (slideIndex) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).animate({
        opacity: 0,
        zIndex: _.options.zIndex - 2
      }, _.options.speed, _.options.easing);
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 0,
        zIndex: _.options.zIndex - 2
      });
    }
  };

  Slick.prototype.filterSlides = Slick.prototype.slickFilter = function (filter) {
    var _ = this;

    if (filter !== null) {
      _.$slidesCache = _.$slides;

      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.focusHandler = function () {
    var _ = this;

    _.$slider.off('focus.slick blur.slick').on('focus.slick blur.slick', '*:not(.slick-arrow)', function (event) {
      event.stopImmediatePropagation();
      var $sf = $(this);
      setTimeout(function () {
        if (_.options.pauseOnFocus) {
          _.focussed = $sf.is(':focus');

          _.autoPlay();
        }
      }, 0);
    });
  };

  Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function () {
    var _ = this;

    return _.currentSlide;
  };

  Slick.prototype.getDotCount = function () {
    var _ = this;

    var breakPoint = 0;
    var counter = 0;
    var pagerQty = 0;

    if (_.options.infinite === true) {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
      }
    } else if (_.options.centerMode === true) {
      pagerQty = _.slideCount;
    } else if (!_.options.asNavFor) {
      pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
    } else {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
      }
    }

    return pagerQty - 1;
  };

  Slick.prototype.getLeft = function (slideIndex) {
    var _ = this,
        targetLeft,
        verticalHeight,
        verticalOffset = 0,
        targetSlide;

    _.slideOffset = 0;
    verticalHeight = _.$slides.first().outerHeight(true);

    if (_.options.infinite === true) {
      if (_.slideCount > _.options.slidesToShow) {
        _.slideOffset = _.slideWidth * _.options.slidesToShow * -1;
        verticalOffset = verticalHeight * _.options.slidesToShow * -1;
      }

      if (_.slideCount % _.options.slidesToScroll !== 0) {
        if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
          if (slideIndex > _.slideCount) {
            _.slideOffset = (_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth * -1;
            verticalOffset = (_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight * -1;
          } else {
            _.slideOffset = _.slideCount % _.options.slidesToScroll * _.slideWidth * -1;
            verticalOffset = _.slideCount % _.options.slidesToScroll * verticalHeight * -1;
          }
        }
      }
    } else {
      if (slideIndex + _.options.slidesToShow > _.slideCount) {
        _.slideOffset = (slideIndex + _.options.slidesToShow - _.slideCount) * _.slideWidth;
        verticalOffset = (slideIndex + _.options.slidesToShow - _.slideCount) * verticalHeight;
      }
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.slideOffset = 0;
      verticalOffset = 0;
    }

    if (_.options.centerMode === true && _.options.infinite === true) {
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
    } else if (_.options.centerMode === true) {
      _.slideOffset = 0;
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
    }

    if (_.options.vertical === false) {
      targetLeft = slideIndex * _.slideWidth * -1 + _.slideOffset;
    } else {
      targetLeft = slideIndex * verticalHeight * -1 + verticalOffset;
    }

    if (_.options.variableWidth === true) {
      if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
        targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
      } else {
        targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
      }

      if (_.options.rtl === true) {
        if (targetSlide[0]) {
          targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
        } else {
          targetLeft = 0;
        }
      } else {
        targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
      }

      if (_.options.centerMode === true) {
        if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
          targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
        } else {
          targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
        }

        if (_.options.rtl === true) {
          if (targetSlide[0]) {
            targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
          } else {
            targetLeft = 0;
          }
        } else {
          targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
        }

        targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
      }
    }

    return targetLeft;
  };

  Slick.prototype.getOption = Slick.prototype.slickGetOption = function (option) {
    var _ = this;

    return _.options[option];
  };

  Slick.prototype.getNavigableIndexes = function () {
    var _ = this,
        breakPoint = 0,
        counter = 0,
        indexes = [],
        max;

    if (_.options.infinite === false) {
      max = _.slideCount;
    } else {
      breakPoint = _.options.slidesToScroll * -1;
      counter = _.options.slidesToScroll * -1;
      max = _.slideCount * 2;
    }

    while (breakPoint < max) {
      indexes.push(breakPoint);
      breakPoint = counter + _.options.slidesToScroll;
      counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
    }

    return indexes;
  };

  Slick.prototype.getSlick = function () {
    return this;
  };

  Slick.prototype.getSlideCount = function () {
    var _ = this,
        slidesTraversed,
        swipedSlide,
        centerOffset;

    centerOffset = _.options.centerMode === true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0;

    if (_.options.swipeToSlide === true) {
      _.$slideTrack.find('.slick-slide').each(function (index, slide) {
        if (slide.offsetLeft - centerOffset + $(slide).outerWidth() / 2 > _.swipeLeft * -1) {
          swipedSlide = slide;
          return false;
        }
      });

      slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;
      return slidesTraversed;
    } else {
      return _.options.slidesToScroll;
    }
  };

  Slick.prototype.goTo = Slick.prototype.slickGoTo = function (slide, dontAnimate) {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'index',
        index: parseInt(slide)
      }
    }, dontAnimate);
  };

  Slick.prototype.init = function (creation) {
    var _ = this;

    if (!$(_.$slider).hasClass('slick-initialized')) {
      $(_.$slider).addClass('slick-initialized');

      _.buildRows();

      _.buildOut();

      _.setProps();

      _.startLoad();

      _.loadSlider();

      _.initializeEvents();

      _.updateArrows();

      _.updateDots();

      _.checkResponsive(true);

      _.focusHandler();
    }

    if (creation) {
      _.$slider.trigger('init', [_]);
    }

    if (_.options.accessibility === true) {
      _.initADA();
    }

    if (_.options.autoplay) {
      _.paused = false;

      _.autoPlay();
    }
  };

  Slick.prototype.initADA = function () {
    var _ = this;

    _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
      'aria-hidden': 'true',
      'tabindex': '-1'
    }).find('a, input, button, select').attr({
      'tabindex': '-1'
    });

    _.$slideTrack.attr('role', 'listbox');

    _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function (i) {
      $(this).attr({
        'role': 'option',
        'aria-describedby': 'slick-slide' + _.instanceUid + i + ''
      });
    });

    if (_.$dots !== null) {
      _.$dots.attr('role', 'tablist').find('li').each(function (i) {
        $(this).attr({
          'role': 'presentation',
          'aria-selected': 'false',
          'aria-controls': 'navigation' + _.instanceUid + i + '',
          'id': 'slick-slide' + _.instanceUid + i + ''
        });
      }).first().attr('aria-selected', 'true').end().find('button').attr('role', 'button').end().closest('div').attr('role', 'toolbar');
    }

    _.activateADA();
  };

  Slick.prototype.initArrowEvents = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.off('click.slick').on('click.slick', {
        message: 'previous'
      }, _.changeSlide);

      _.$nextArrow.off('click.slick').on('click.slick', {
        message: 'next'
      }, _.changeSlide);
    }
  };

  Slick.prototype.initDotEvents = function () {
    var _ = this;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      $('li', _.$dots).on('click.slick', {
        message: 'index'
      }, _.changeSlide);
    }

    if (_.options.dots === true && _.options.pauseOnDotsHover === true) {
      $('li', _.$dots).on('mouseenter.slick', $.proxy(_.interrupt, _, true)).on('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initSlideEvents = function () {
    var _ = this;

    if (_.options.pauseOnHover) {
      _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));

      _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initializeEvents = function () {
    var _ = this;

    _.initArrowEvents();

    _.initDotEvents();

    _.initSlideEvents();

    _.$list.on('touchstart.slick mousedown.slick', {
      action: 'start'
    }, _.swipeHandler);

    _.$list.on('touchmove.slick mousemove.slick', {
      action: 'move'
    }, _.swipeHandler);

    _.$list.on('touchend.slick mouseup.slick', {
      action: 'end'
    }, _.swipeHandler);

    _.$list.on('touchcancel.slick mouseleave.slick', {
      action: 'end'
    }, _.swipeHandler);

    _.$list.on('click.slick', _.clickHandler);

    $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

    if (_.options.accessibility === true) {
      _.$list.on('keydown.slick', _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on('click.slick', _.selectHandler);
    }

    $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));
    $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));
    $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);
    $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
    $(document).on('ready.slick.slick-' + _.instanceUid, _.setPosition);
  };

  Slick.prototype.initUI = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.show();

      _.$nextArrow.show();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.show();
    }
  };

  Slick.prototype.keyHandler = function (event) {
    var _ = this; //Dont slide if the cursor is inside the form fields and arrow keys are pressed


    if (!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
      if (event.keyCode === 37 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? 'next' : 'previous'
          }
        });
      } else if (event.keyCode === 39 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? 'previous' : 'next'
          }
        });
      }
    }
  };

  Slick.prototype.lazyLoad = function () {
    var _ = this,
        loadRange,
        cloneRange,
        rangeStart,
        rangeEnd;

    function loadImages(imagesScope) {
      $('img[data-lazy]', imagesScope).each(function () {
        var image = $(this),
            imageSource = $(this).attr('data-lazy'),
            imageToLoad = document.createElement('img');

        imageToLoad.onload = function () {
          image.animate({
            opacity: 0
          }, 100, function () {
            image.attr('src', imageSource).animate({
              opacity: 1
            }, 200, function () {
              image.removeAttr('data-lazy').removeClass('slick-loading');
            });

            _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
          });
        };

        imageToLoad.onerror = function () {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');

          _.$slider.trigger('lazyLoadError', [_, image, imageSource]);
        };

        imageToLoad.src = imageSource;
      });
    }

    if (_.options.centerMode === true) {
      if (_.options.infinite === true) {
        rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
        rangeEnd = rangeStart + _.options.slidesToShow + 2;
      } else {
        rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
        rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
      }
    } else {
      rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
      rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);

      if (_.options.fade === true) {
        if (rangeStart > 0) rangeStart--;
        if (rangeEnd <= _.slideCount) rangeEnd++;
      }
    }

    loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
    loadImages(loadRange);

    if (_.slideCount <= _.options.slidesToShow) {
      cloneRange = _.$slider.find('.slick-slide');
      loadImages(cloneRange);
    } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
      cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
      loadImages(cloneRange);
    } else if (_.currentSlide === 0) {
      cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
      loadImages(cloneRange);
    }
  };

  Slick.prototype.loadSlider = function () {
    var _ = this;

    _.setPosition();

    _.$slideTrack.css({
      opacity: 1
    });

    _.$slider.removeClass('slick-loading');

    _.initUI();

    if (_.options.lazyLoad === 'progressive') {
      _.progressiveLazyLoad();
    }
  };

  Slick.prototype.next = Slick.prototype.slickNext = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'next'
      }
    });
  };

  Slick.prototype.orientationChange = function () {
    var _ = this;

    _.checkResponsive();

    _.setPosition();
  };

  Slick.prototype.pause = Slick.prototype.slickPause = function () {
    var _ = this;

    _.autoPlayClear();

    _.paused = true;
  };

  Slick.prototype.play = Slick.prototype.slickPlay = function () {
    var _ = this;

    _.autoPlay();

    _.options.autoplay = true;
    _.paused = false;
    _.focussed = false;
    _.interrupted = false;
  };

  Slick.prototype.postSlide = function (index) {
    var _ = this;

    if (!_.unslicked) {
      _.$slider.trigger('afterChange', [_, index]);

      _.animating = false;

      _.setPosition();

      _.swipeLeft = null;

      if (_.options.autoplay) {
        _.autoPlay();
      }

      if (_.options.accessibility === true) {
        _.initADA();
      }
    }
  };

  Slick.prototype.prev = Slick.prototype.slickPrev = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'previous'
      }
    });
  };

  Slick.prototype.preventDefault = function (event) {
    event.preventDefault();
  };

  Slick.prototype.progressiveLazyLoad = function (tryCount) {
    tryCount = tryCount || 1;

    var _ = this,
        $imgsToLoad = $('img[data-lazy]', _.$slider),
        image,
        imageSource,
        imageToLoad;

    if ($imgsToLoad.length) {
      image = $imgsToLoad.first();
      imageSource = image.attr('data-lazy');
      imageToLoad = document.createElement('img');

      imageToLoad.onload = function () {
        image.attr('src', imageSource).removeAttr('data-lazy').removeClass('slick-loading');

        if (_.options.adaptiveHeight === true) {
          _.setPosition();
        }

        _.$slider.trigger('lazyLoaded', [_, image, imageSource]);

        _.progressiveLazyLoad();
      };

      imageToLoad.onerror = function () {
        if (tryCount < 3) {
          /**
           * try to load the image 3 times,
           * leave a slight delay so we don't get
           * servers blocking the request.
           */
          setTimeout(function () {
            _.progressiveLazyLoad(tryCount + 1);
          }, 500);
        } else {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');

          _.$slider.trigger('lazyLoadError', [_, image, imageSource]);

          _.progressiveLazyLoad();
        }
      };

      imageToLoad.src = imageSource;
    } else {
      _.$slider.trigger('allImagesLoaded', [_]);
    }
  };

  Slick.prototype.refresh = function (initializing) {
    var _ = this,
        currentSlide,
        lastVisibleIndex;

    lastVisibleIndex = _.slideCount - _.options.slidesToShow; // in non-infinite sliders, we don't want to go past the
    // last visible index.

    if (!_.options.infinite && _.currentSlide > lastVisibleIndex) {
      _.currentSlide = lastVisibleIndex;
    } // if less slides than to show, go to start.


    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    currentSlide = _.currentSlide;

    _.destroy(true);

    $.extend(_, _.initials, {
      currentSlide: currentSlide
    });

    _.init();

    if (!initializing) {
      _.changeSlide({
        data: {
          message: 'index',
          index: currentSlide
        }
      }, false);
    }
  };

  Slick.prototype.registerBreakpoints = function () {
    var _ = this,
        breakpoint,
        currentBreakpoint,
        l,
        responsiveSettings = _.options.responsive || null;

    if ($.type(responsiveSettings) === 'array' && responsiveSettings.length) {
      _.respondTo = _.options.respondTo || 'window';

      for (breakpoint in responsiveSettings) {
        l = _.breakpoints.length - 1;
        currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

        if (responsiveSettings.hasOwnProperty(breakpoint)) {
          // loop through the breakpoints and cut out any existing
          // ones with the same breakpoint number, we don't want dupes.
          while (l >= 0) {
            if (_.breakpoints[l] && _.breakpoints[l] === currentBreakpoint) {
              _.breakpoints.splice(l, 1);
            }

            l--;
          }

          _.breakpoints.push(currentBreakpoint);

          _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;
        }
      }

      _.breakpoints.sort(function (a, b) {
        return _.options.mobileFirst ? a - b : b - a;
      });
    }
  };

  Slick.prototype.reinit = function () {
    var _ = this;

    _.$slides = _.$slideTrack.children(_.options.slide).addClass('slick-slide');
    _.slideCount = _.$slides.length;

    if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
      _.currentSlide = _.currentSlide - _.options.slidesToScroll;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    _.registerBreakpoints();

    _.setProps();

    _.setupInfinite();

    _.buildArrows();

    _.updateArrows();

    _.initArrowEvents();

    _.buildDots();

    _.updateDots();

    _.initDotEvents();

    _.cleanUpSlideEvents();

    _.initSlideEvents();

    _.checkResponsive(false, true);

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on('click.slick', _.selectHandler);
    }

    _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

    _.setPosition();

    _.focusHandler();

    _.paused = !_.options.autoplay;

    _.autoPlay();

    _.$slider.trigger('reInit', [_]);
  };

  Slick.prototype.resize = function () {
    var _ = this;

    if ($(window).width() !== _.windowWidth) {
      clearTimeout(_.windowDelay);
      _.windowDelay = window.setTimeout(function () {
        _.windowWidth = $(window).width();

        _.checkResponsive();

        if (!_.unslicked) {
          _.setPosition();
        }
      }, 50);
    }
  };

  Slick.prototype.removeSlide = Slick.prototype.slickRemove = function (index, removeBefore, removeAll) {
    var _ = this;

    if (typeof index === 'boolean') {
      removeBefore = index;
      index = removeBefore === true ? 0 : _.slideCount - 1;
    } else {
      index = removeBefore === true ? --index : index;
    }

    if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
      return false;
    }

    _.unload();

    if (removeAll === true) {
      _.$slideTrack.children().remove();
    } else {
      _.$slideTrack.children(this.options.slide).eq(index).remove();
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.setCSS = function (position) {
    var _ = this,
        positionProps = {},
        x,
        y;

    if (_.options.rtl === true) {
      position = -position;
    }

    x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
    y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';
    positionProps[_.positionProp] = position;

    if (_.transformsEnabled === false) {
      _.$slideTrack.css(positionProps);
    } else {
      positionProps = {};

      if (_.cssTransitions === false) {
        positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';

        _.$slideTrack.css(positionProps);
      } else {
        positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';

        _.$slideTrack.css(positionProps);
      }
    }
  };

  Slick.prototype.setDimensions = function () {
    var _ = this;

    if (_.options.vertical === false) {
      if (_.options.centerMode === true) {
        _.$list.css({
          padding: '0px ' + _.options.centerPadding
        });
      }
    } else {
      _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);

      if (_.options.centerMode === true) {
        _.$list.css({
          padding: _.options.centerPadding + ' 0px'
        });
      }
    }

    _.listWidth = _.$list.width();
    _.listHeight = _.$list.height();

    if (_.options.vertical === false && _.options.variableWidth === false) {
      _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);

      _.$slideTrack.width(Math.ceil(_.slideWidth * _.$slideTrack.children('.slick-slide').length));
    } else if (_.options.variableWidth === true) {
      _.$slideTrack.width(5000 * _.slideCount);
    } else {
      _.slideWidth = Math.ceil(_.listWidth);

      _.$slideTrack.height(Math.ceil(_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length));
    }

    var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();

    if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);
  };

  Slick.prototype.setFade = function () {
    var _ = this,
        targetLeft;

    _.$slides.each(function (index, element) {
      targetLeft = _.slideWidth * index * -1;

      if (_.options.rtl === true) {
        $(element).css({
          position: 'relative',
          right: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0
        });
      } else {
        $(element).css({
          position: 'relative',
          left: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0
        });
      }
    });

    _.$slides.eq(_.currentSlide).css({
      zIndex: _.options.zIndex - 1,
      opacity: 1
    });
  };

  Slick.prototype.setHeight = function () {
    var _ = this;

    if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);

      _.$list.css('height', targetHeight);
    }
  };

  Slick.prototype.setOption = Slick.prototype.slickSetOption = function () {
    /**
     * accepts arguments in format of:
     *
     *  - for changing a single option's value:
     *     .slick("setOption", option, value, refresh )
     *
     *  - for changing a set of responsive options:
     *     .slick("setOption", 'responsive', [{}, ...], refresh )
     *
     *  - for updating multiple values at once (not responsive)
     *     .slick("setOption", { 'option': value, ... }, refresh )
     */
    var _ = this,
        l,
        item,
        option,
        value,
        refresh = false,
        type;

    if ($.type(arguments[0]) === 'object') {
      option = arguments[0];
      refresh = arguments[1];
      type = 'multiple';
    } else if ($.type(arguments[0]) === 'string') {
      option = arguments[0];
      value = arguments[1];
      refresh = arguments[2];

      if (arguments[0] === 'responsive' && $.type(arguments[1]) === 'array') {
        type = 'responsive';
      } else if (typeof arguments[1] !== 'undefined') {
        type = 'single';
      }
    }

    if (type === 'single') {
      _.options[option] = value;
    } else if (type === 'multiple') {
      $.each(option, function (opt, val) {
        _.options[opt] = val;
      });
    } else if (type === 'responsive') {
      for (item in value) {
        if ($.type(_.options.responsive) !== 'array') {
          _.options.responsive = [value[item]];
        } else {
          l = _.options.responsive.length - 1; // loop through the responsive object and splice out duplicates.

          while (l >= 0) {
            if (_.options.responsive[l].breakpoint === value[item].breakpoint) {
              _.options.responsive.splice(l, 1);
            }

            l--;
          }

          _.options.responsive.push(value[item]);
        }
      }
    }

    if (refresh) {
      _.unload();

      _.reinit();
    }
  };

  Slick.prototype.setPosition = function () {
    var _ = this;

    _.setDimensions();

    _.setHeight();

    if (_.options.fade === false) {
      _.setCSS(_.getLeft(_.currentSlide));
    } else {
      _.setFade();
    }

    _.$slider.trigger('setPosition', [_]);
  };

  Slick.prototype.setProps = function () {
    var _ = this,
        bodyStyle = document.body.style;

    _.positionProp = _.options.vertical === true ? 'top' : 'left';

    if (_.positionProp === 'top') {
      _.$slider.addClass('slick-vertical');
    } else {
      _.$slider.removeClass('slick-vertical');
    }

    if (bodyStyle.WebkitTransition !== undefined || bodyStyle.MozTransition !== undefined || bodyStyle.msTransition !== undefined) {
      if (_.options.useCSS === true) {
        _.cssTransitions = true;
      }
    }

    if (_.options.fade) {
      if (typeof _.options.zIndex === 'number') {
        if (_.options.zIndex < 3) {
          _.options.zIndex = 3;
        }
      } else {
        _.options.zIndex = _.defaults.zIndex;
      }
    }

    if (bodyStyle.OTransform !== undefined) {
      _.animType = 'OTransform';
      _.transformType = '-o-transform';
      _.transitionType = 'OTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.MozTransform !== undefined) {
      _.animType = 'MozTransform';
      _.transformType = '-moz-transform';
      _.transitionType = 'MozTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.webkitTransform !== undefined) {
      _.animType = 'webkitTransform';
      _.transformType = '-webkit-transform';
      _.transitionType = 'webkitTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.msTransform !== undefined) {
      _.animType = 'msTransform';
      _.transformType = '-ms-transform';
      _.transitionType = 'msTransition';
      if (bodyStyle.msTransform === undefined) _.animType = false;
    }

    if (bodyStyle.transform !== undefined && _.animType !== false) {
      _.animType = 'transform';
      _.transformType = 'transform';
      _.transitionType = 'transition';
    }

    _.transformsEnabled = _.options.useTransform && _.animType !== null && _.animType !== false;
  };

  Slick.prototype.setSlideClasses = function (index) {
    var _ = this,
        centerOffset,
        allSlides,
        indexOffset,
        remainder;

    allSlides = _.$slider.find('.slick-slide').removeClass('slick-active slick-center slick-current').attr('aria-hidden', 'true');

    _.$slides.eq(index).addClass('slick-current');

    if (_.options.centerMode === true) {
      centerOffset = Math.floor(_.options.slidesToShow / 2);

      if (_.options.infinite === true) {
        if (index >= centerOffset && index <= _.slideCount - 1 - centerOffset) {
          _.$slides.slice(index - centerOffset, index + centerOffset + 1).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          indexOffset = _.options.slidesToShow + index;
          allSlides.slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2).addClass('slick-active').attr('aria-hidden', 'false');
        }

        if (index === 0) {
          allSlides.eq(allSlides.length - 1 - _.options.slidesToShow).addClass('slick-center');
        } else if (index === _.slideCount - 1) {
          allSlides.eq(_.options.slidesToShow).addClass('slick-center');
        }
      }

      _.$slides.eq(index).addClass('slick-center');
    } else {
      if (index >= 0 && index <= _.slideCount - _.options.slidesToShow) {
        _.$slides.slice(index, index + _.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
      } else if (allSlides.length <= _.options.slidesToShow) {
        allSlides.addClass('slick-active').attr('aria-hidden', 'false');
      } else {
        remainder = _.slideCount % _.options.slidesToShow;
        indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

        if (_.options.slidesToShow == _.options.slidesToScroll && _.slideCount - index < _.options.slidesToShow) {
          allSlides.slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          allSlides.slice(indexOffset, indexOffset + _.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
        }
      }
    }

    if (_.options.lazyLoad === 'ondemand') {
      _.lazyLoad();
    }
  };

  Slick.prototype.setupInfinite = function () {
    var _ = this,
        i,
        slideIndex,
        infiniteCount;

    if (_.options.fade === true) {
      _.options.centerMode = false;
    }

    if (_.options.infinite === true && _.options.fade === false) {
      slideIndex = null;

      if (_.slideCount > _.options.slidesToShow) {
        if (_.options.centerMode === true) {
          infiniteCount = _.options.slidesToShow + 1;
        } else {
          infiniteCount = _.options.slidesToShow;
        }

        for (i = _.slideCount; i > _.slideCount - infiniteCount; i -= 1) {
          slideIndex = i - 1;
          $(_.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex - _.slideCount).prependTo(_.$slideTrack).addClass('slick-cloned');
        }

        for (i = 0; i < infiniteCount; i += 1) {
          slideIndex = i;
          $(_.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex + _.slideCount).appendTo(_.$slideTrack).addClass('slick-cloned');
        }

        _.$slideTrack.find('.slick-cloned').find('[id]').each(function () {
          $(this).attr('id', '');
        });
      }
    }
  };

  Slick.prototype.interrupt = function (toggle) {
    var _ = this;

    if (!toggle) {
      _.autoPlay();
    }

    _.interrupted = toggle;
  };

  Slick.prototype.selectHandler = function (event) {
    var _ = this;

    var targetElement = $(event.target).is('.slick-slide') ? $(event.target) : $(event.target).parents('.slick-slide');
    var index = parseInt(targetElement.attr('data-slick-index'));
    if (!index) index = 0;

    if (_.slideCount <= _.options.slidesToShow) {
      _.setSlideClasses(index);

      _.asNavFor(index);

      return;
    }

    _.slideHandler(index);
  };

  Slick.prototype.slideHandler = function (index, sync, dontAnimate) {
    var targetSlide,
        animSlide,
        oldSlide,
        slideLeft,
        targetLeft = null,
        _ = this,
        navTarget;

    sync = sync || false;

    if (_.animating === true && _.options.waitForAnimate === true) {
      return;
    }

    if (_.options.fade === true && _.currentSlide === index) {
      return;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      return;
    }

    if (sync === false) {
      _.asNavFor(index);
    }

    targetSlide = index;
    targetLeft = _.getLeft(targetSlide);
    slideLeft = _.getLeft(_.currentSlide);
    _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

    if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;

        if (dontAnimate !== true) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }

      return;
    } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > _.slideCount - _.options.slidesToScroll)) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;

        if (dontAnimate !== true) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }

      return;
    }

    if (_.options.autoplay) {
      clearInterval(_.autoPlayTimer);
    }

    if (targetSlide < 0) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = _.slideCount - _.slideCount % _.options.slidesToScroll;
      } else {
        animSlide = _.slideCount + targetSlide;
      }
    } else if (targetSlide >= _.slideCount) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = 0;
      } else {
        animSlide = targetSlide - _.slideCount;
      }
    } else {
      animSlide = targetSlide;
    }

    _.animating = true;

    _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

    oldSlide = _.currentSlide;
    _.currentSlide = animSlide;

    _.setSlideClasses(_.currentSlide);

    if (_.options.asNavFor) {
      navTarget = _.getNavTarget();
      navTarget = navTarget.slick('getSlick');

      if (navTarget.slideCount <= navTarget.options.slidesToShow) {
        navTarget.setSlideClasses(_.currentSlide);
      }
    }

    _.updateDots();

    _.updateArrows();

    if (_.options.fade === true) {
      if (dontAnimate !== true) {
        _.fadeSlideOut(oldSlide);

        _.fadeSlide(animSlide, function () {
          _.postSlide(animSlide);
        });
      } else {
        _.postSlide(animSlide);
      }

      _.animateHeight();

      return;
    }

    if (dontAnimate !== true) {
      _.animateSlide(targetLeft, function () {
        _.postSlide(animSlide);
      });
    } else {
      _.postSlide(animSlide);
    }
  };

  Slick.prototype.startLoad = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.hide();

      _.$nextArrow.hide();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.hide();
    }

    _.$slider.addClass('slick-loading');
  };

  Slick.prototype.swipeDirection = function () {
    var xDist,
        yDist,
        r,
        swipeAngle,
        _ = this;

    xDist = _.touchObject.startX - _.touchObject.curX;
    yDist = _.touchObject.startY - _.touchObject.curY;
    r = Math.atan2(yDist, xDist);
    swipeAngle = Math.round(r * 180 / Math.PI);

    if (swipeAngle < 0) {
      swipeAngle = 360 - Math.abs(swipeAngle);
    }

    if (swipeAngle <= 45 && swipeAngle >= 0) {
      return _.options.rtl === false ? 'left' : 'right';
    }

    if (swipeAngle <= 360 && swipeAngle >= 315) {
      return _.options.rtl === false ? 'left' : 'right';
    }

    if (swipeAngle >= 135 && swipeAngle <= 225) {
      return _.options.rtl === false ? 'right' : 'left';
    }

    if (_.options.verticalSwiping === true) {
      if (swipeAngle >= 35 && swipeAngle <= 135) {
        return 'down';
      } else {
        return 'up';
      }
    }

    return 'vertical';
  };

  Slick.prototype.swipeEnd = function (event) {
    var _ = this,
        slideCount,
        direction;

    _.dragging = false;
    _.interrupted = false;
    _.shouldClick = _.touchObject.swipeLength > 10 ? false : true;

    if (_.touchObject.curX === undefined) {
      return false;
    }

    if (_.touchObject.edgeHit === true) {
      _.$slider.trigger('edge', [_, _.swipeDirection()]);
    }

    if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {
      direction = _.swipeDirection();

      switch (direction) {
        case 'left':
        case 'down':
          slideCount = _.options.swipeToSlide ? _.checkNavigable(_.currentSlide + _.getSlideCount()) : _.currentSlide + _.getSlideCount();
          _.currentDirection = 0;
          break;

        case 'right':
        case 'up':
          slideCount = _.options.swipeToSlide ? _.checkNavigable(_.currentSlide - _.getSlideCount()) : _.currentSlide - _.getSlideCount();
          _.currentDirection = 1;
          break;

        default:
      }

      if (direction != 'vertical') {
        _.slideHandler(slideCount);

        _.touchObject = {};

        _.$slider.trigger('swipe', [_, direction]);
      }
    } else {
      if (_.touchObject.startX !== _.touchObject.curX) {
        _.slideHandler(_.currentSlide);

        _.touchObject = {};
      }
    }
  };

  Slick.prototype.swipeHandler = function (event) {
    var _ = this;

    if (_.options.swipe === false || 'ontouchend' in document && _.options.swipe === false) {
      return;
    } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
      return;
    }

    _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ? event.originalEvent.touches.length : 1;
    _.touchObject.minSwipe = _.listWidth / _.options.touchThreshold;

    if (_.options.verticalSwiping === true) {
      _.touchObject.minSwipe = _.listHeight / _.options.touchThreshold;
    }

    switch (event.data.action) {
      case 'start':
        _.swipeStart(event);

        break;

      case 'move':
        _.swipeMove(event);

        break;

      case 'end':
        _.swipeEnd(event);

        break;
    }
  };

  Slick.prototype.swipeMove = function (event) {
    var _ = this,
        edgeWasHit = false,
        curLeft,
        swipeDirection,
        swipeLength,
        positionOffset,
        touches;

    touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

    if (!_.dragging || touches && touches.length !== 1) {
      return false;
    }

    curLeft = _.getLeft(_.currentSlide);
    _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
    _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;
    _.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

    if (_.options.verticalSwiping === true) {
      _.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));
    }

    swipeDirection = _.swipeDirection();

    if (swipeDirection === 'vertical') {
      return;
    }

    if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
      event.preventDefault();
    }

    positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);

    if (_.options.verticalSwiping === true) {
      positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
    }

    swipeLength = _.touchObject.swipeLength;
    _.touchObject.edgeHit = false;

    if (_.options.infinite === false) {
      if (_.currentSlide === 0 && swipeDirection === 'right' || _.currentSlide >= _.getDotCount() && swipeDirection === 'left') {
        swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
        _.touchObject.edgeHit = true;
      }
    }

    if (_.options.vertical === false) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    } else {
      _.swipeLeft = curLeft + swipeLength * (_.$list.height() / _.listWidth) * positionOffset;
    }

    if (_.options.verticalSwiping === true) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    }

    if (_.options.fade === true || _.options.touchMove === false) {
      return false;
    }

    if (_.animating === true) {
      _.swipeLeft = null;
      return false;
    }

    _.setCSS(_.swipeLeft);
  };

  Slick.prototype.swipeStart = function (event) {
    var _ = this,
        touches;

    _.interrupted = true;

    if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
      _.touchObject = {};
      return false;
    }

    if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
      touches = event.originalEvent.touches[0];
    }

    _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
    _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;
    _.dragging = true;
  };

  Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function () {
    var _ = this;

    if (_.$slidesCache !== null) {
      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.unload = function () {
    var _ = this;

    $('.slick-cloned', _.$slider).remove();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
      _.$prevArrow.remove();
    }

    if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
      _.$nextArrow.remove();
    }

    _.$slides.removeClass('slick-slide slick-active slick-visible slick-current').attr('aria-hidden', 'true').css('width', '');
  };

  Slick.prototype.unslick = function (fromBreakpoint) {
    var _ = this;

    _.$slider.trigger('unslick', [_, fromBreakpoint]);

    _.destroy();
  };

  Slick.prototype.updateArrows = function () {
    var _ = this,
        centerOffset;

    centerOffset = Math.floor(_.options.slidesToShow / 2);

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow && !_.options.infinite) {
      _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

      _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

      if (_.currentSlide === 0) {
        _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {
        _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {
        _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      }
    }
  };

  Slick.prototype.updateDots = function () {
    var _ = this;

    if (_.$dots !== null) {
      _.$dots.find('li').removeClass('slick-active').attr('aria-hidden', 'true');

      _.$dots.find('li').eq(Math.floor(_.currentSlide / _.options.slidesToScroll)).addClass('slick-active').attr('aria-hidden', 'false');
    }
  };

  Slick.prototype.visibility = function () {
    var _ = this;

    if (_.options.autoplay) {
      if (document[_.hidden]) {
        _.interrupted = true;
      } else {
        _.interrupted = false;
      }
    }
  };

  $.fn.slick = function () {
    var _ = this,
        opt = arguments[0],
        args = Array.prototype.slice.call(arguments, 1),
        l = _.length,
        i,
        ret;

    for (i = 0; i < l; i++) {
      if (_typeof(opt) == 'object' || typeof opt == 'undefined') _[i].slick = new Slick(_[i], opt);else ret = _[i].slick[opt].apply(_[i].slick, args);
      if (typeof ret != 'undefined') return ret;
    }

    return _;
  };
});
"use strict";

// Meet Slider
(function ($) {
  if ($(".front-page").length < 1) {
    return;
  }

  var $wrap = $(".meet");
  var $slider = $wrap.find(".meet-slider");

  var _prev = function _prev() {
    $slider.slick("slickPrev");
  };

  var _next = function _next() {
    $slider.slick("slickNext");
  };

  $wrap.on("click", ".arrow-left", _prev);
  $wrap.on("click", ".arrow-right", _next);
})(jQuery); // Important Questions


(function ($) {
  if ($(".front-page").length < 1) {
    return;
  }

  var $wrap = $(".toggles");

  var _toggle = function _toggle() {
    $(this).parent().toggleClass("active");
  };

  $wrap.on("click", "h4", _toggle);
})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vYmlsZS1tZW51LmpzIiwiYXBwLmpzIiwic2xpY2suanMiLCJzY3JpcHRzL2dlbmVyYWwuanMiLCJzY3JpcHRzL2hlYWRlci5qcyIsInNjcmlwdHMvc2xpY2suanMiLCJwbHVnaW5zL2pxdWVyeS5sYXp5LmpzIiwicGx1Z2lucy9qcXVlcnkubGF6eS5taW4uanMiLCJwbHVnaW5zL21vYmlsZS1tZW51LmpzIiwicGx1Z2lucy9zbGljay5qcyIsInNwZWNpZmljL2Zyb250LXBhZ2UuanMiXSwibmFtZXMiOlsiJCIsImZuIiwibW9iaWxlX21lbnUiLCJvcHRpb25zIiwiZGVmYXVsdHMiLCJzaWRlIiwic3BlZWQiLCJ0b2dnbGVyIiwid2lkdGgiLCJoZWlnaHQiLCJidXR0b25TdHlsZXMiLCJpbml0YWxzIiwidG9nZ2xlQnV0dG9uIiwibWVudVdyYXAiLCIkdG9nZ2xlciIsIm9mZmNhbnZhcyIsIiR0aGlzIiwic2V0dGluZ3MiLCJleHRlbmQiLCJiZWZvcmUiLCJhZGRDbGFzcyIsInN0eWxlcyIsImVhY2giLCJ3cmFwIiwicGFyZW50IiwiY3NzIiwiY29uc29sZSIsImxvZyIsIm9uIiwidG9nZ2xlQ2xhc3MiLCJjbG9zZXN0IiwialF1ZXJ5IiwiZmFjdG9yeSIsImRlZmluZSIsImFtZCIsImV4cG9ydHMiLCJtb2R1bGUiLCJyZXF1aXJlIiwiU2xpY2siLCJ3aW5kb3ciLCJpbnN0YW5jZVVpZCIsImVsZW1lbnQiLCJfIiwiZGF0YVNldHRpbmdzIiwiYWNjZXNzaWJpbGl0eSIsImFkYXB0aXZlSGVpZ2h0IiwiYXBwZW5kQXJyb3dzIiwiYXBwZW5kRG90cyIsImFycm93cyIsImFzTmF2Rm9yIiwicHJldkFycm93IiwibmV4dEFycm93IiwiYXV0b3BsYXkiLCJhdXRvcGxheVNwZWVkIiwiY2VudGVyTW9kZSIsImNlbnRlclBhZGRpbmciLCJjc3NFYXNlIiwiY3VzdG9tUGFnaW5nIiwic2xpZGVyIiwiaSIsInRleHQiLCJkb3RzIiwiZG90c0NsYXNzIiwiZHJhZ2dhYmxlIiwiZWFzaW5nIiwiZWRnZUZyaWN0aW9uIiwiZmFkZSIsImZvY3VzT25TZWxlY3QiLCJpbmZpbml0ZSIsImluaXRpYWxTbGlkZSIsImxhenlMb2FkIiwibW9iaWxlRmlyc3QiLCJwYXVzZU9uSG92ZXIiLCJwYXVzZU9uRm9jdXMiLCJwYXVzZU9uRG90c0hvdmVyIiwicmVzcG9uZFRvIiwicmVzcG9uc2l2ZSIsInJvd3MiLCJydGwiLCJzbGlkZSIsInNsaWRlc1BlclJvdyIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwic3dpcGUiLCJzd2lwZVRvU2xpZGUiLCJ0b3VjaE1vdmUiLCJ0b3VjaFRocmVzaG9sZCIsInVzZUNTUyIsInVzZVRyYW5zZm9ybSIsInZhcmlhYmxlV2lkdGgiLCJ2ZXJ0aWNhbCIsInZlcnRpY2FsU3dpcGluZyIsIndhaXRGb3JBbmltYXRlIiwiekluZGV4IiwiaW5pdGlhbHMiLCJhbmltYXRpbmciLCJkcmFnZ2luZyIsImF1dG9QbGF5VGltZXIiLCJjdXJyZW50RGlyZWN0aW9uIiwiY3VycmVudExlZnQiLCJjdXJyZW50U2xpZGUiLCJkaXJlY3Rpb24iLCIkZG90cyIsImxpc3RXaWR0aCIsImxpc3RIZWlnaHQiLCJsb2FkSW5kZXgiLCIkbmV4dEFycm93IiwiJHByZXZBcnJvdyIsInNsaWRlQ291bnQiLCJzbGlkZVdpZHRoIiwiJHNsaWRlVHJhY2siLCIkc2xpZGVzIiwic2xpZGluZyIsInNsaWRlT2Zmc2V0Iiwic3dpcGVMZWZ0IiwiJGxpc3QiLCJ0b3VjaE9iamVjdCIsInRyYW5zZm9ybXNFbmFibGVkIiwidW5zbGlja2VkIiwiYWN0aXZlQnJlYWtwb2ludCIsImFuaW1UeXBlIiwiYW5pbVByb3AiLCJicmVha3BvaW50cyIsImJyZWFrcG9pbnRTZXR0aW5ncyIsImNzc1RyYW5zaXRpb25zIiwiZm9jdXNzZWQiLCJpbnRlcnJ1cHRlZCIsImhpZGRlbiIsInBhdXNlZCIsInBvc2l0aW9uUHJvcCIsInJvd0NvdW50Iiwic2hvdWxkQ2xpY2siLCIkc2xpZGVyIiwiJHNsaWRlc0NhY2hlIiwidHJhbnNmb3JtVHlwZSIsInRyYW5zaXRpb25UeXBlIiwidmlzaWJpbGl0eUNoYW5nZSIsIndpbmRvd1dpZHRoIiwid2luZG93VGltZXIiLCJkYXRhIiwib3JpZ2luYWxTZXR0aW5ncyIsImRvY3VtZW50IiwibW96SGlkZGVuIiwid2Via2l0SGlkZGVuIiwiYXV0b1BsYXkiLCJwcm94eSIsImF1dG9QbGF5Q2xlYXIiLCJhdXRvUGxheUl0ZXJhdG9yIiwiY2hhbmdlU2xpZGUiLCJjbGlja0hhbmRsZXIiLCJzZWxlY3RIYW5kbGVyIiwic2V0UG9zaXRpb24iLCJzd2lwZUhhbmRsZXIiLCJkcmFnSGFuZGxlciIsImtleUhhbmRsZXIiLCJodG1sRXhwciIsInJlZ2lzdGVyQnJlYWtwb2ludHMiLCJpbml0IiwicHJvdG90eXBlIiwiYWN0aXZhdGVBREEiLCJmaW5kIiwiYXR0ciIsImFkZFNsaWRlIiwic2xpY2tBZGQiLCJtYXJrdXAiLCJpbmRleCIsImFkZEJlZm9yZSIsInVubG9hZCIsImxlbmd0aCIsImFwcGVuZFRvIiwiaW5zZXJ0QmVmb3JlIiwiZXEiLCJpbnNlcnRBZnRlciIsInByZXBlbmRUbyIsImNoaWxkcmVuIiwiZGV0YWNoIiwiYXBwZW5kIiwicmVpbml0IiwiYW5pbWF0ZUhlaWdodCIsInRhcmdldEhlaWdodCIsIm91dGVySGVpZ2h0IiwiYW5pbWF0ZSIsImFuaW1hdGVTbGlkZSIsInRhcmdldExlZnQiLCJjYWxsYmFjayIsImFuaW1Qcm9wcyIsImxlZnQiLCJ0b3AiLCJhbmltU3RhcnQiLCJkdXJhdGlvbiIsInN0ZXAiLCJub3ciLCJNYXRoIiwiY2VpbCIsImNvbXBsZXRlIiwiY2FsbCIsImFwcGx5VHJhbnNpdGlvbiIsInNldFRpbWVvdXQiLCJkaXNhYmxlVHJhbnNpdGlvbiIsImdldE5hdlRhcmdldCIsIm5vdCIsInRhcmdldCIsInNsaWNrIiwic2xpZGVIYW5kbGVyIiwidHJhbnNpdGlvbiIsInNldEludGVydmFsIiwiY2xlYXJJbnRlcnZhbCIsInNsaWRlVG8iLCJidWlsZEFycm93cyIsInJlbW92ZUNsYXNzIiwicmVtb3ZlQXR0ciIsInRlc3QiLCJhZGQiLCJidWlsZERvdHMiLCJkb3QiLCJnZXREb3RDb3VudCIsImZpcnN0IiwiYnVpbGRPdXQiLCJ3cmFwQWxsIiwic2V0dXBJbmZpbml0ZSIsInVwZGF0ZURvdHMiLCJzZXRTbGlkZUNsYXNzZXMiLCJidWlsZFJvd3MiLCJhIiwiYiIsImMiLCJuZXdTbGlkZXMiLCJudW1PZlNsaWRlcyIsIm9yaWdpbmFsU2xpZGVzIiwic2xpZGVzUGVyU2VjdGlvbiIsImNyZWF0ZURvY3VtZW50RnJhZ21lbnQiLCJjcmVhdGVFbGVtZW50Iiwicm93IiwiZ2V0IiwiYXBwZW5kQ2hpbGQiLCJlbXB0eSIsImNoZWNrUmVzcG9uc2l2ZSIsImluaXRpYWwiLCJmb3JjZVVwZGF0ZSIsImJyZWFrcG9pbnQiLCJ0YXJnZXRCcmVha3BvaW50IiwicmVzcG9uZFRvV2lkdGgiLCJ0cmlnZ2VyQnJlYWtwb2ludCIsInNsaWRlcldpZHRoIiwiaW5uZXJXaWR0aCIsIm1pbiIsImhhc093blByb3BlcnR5IiwidW5zbGljayIsInJlZnJlc2giLCJ0cmlnZ2VyIiwiZXZlbnQiLCJkb250QW5pbWF0ZSIsIiR0YXJnZXQiLCJjdXJyZW50VGFyZ2V0IiwiaW5kZXhPZmZzZXQiLCJ1bmV2ZW5PZmZzZXQiLCJpcyIsInByZXZlbnREZWZhdWx0IiwibWVzc2FnZSIsImNoZWNrTmF2aWdhYmxlIiwibmF2aWdhYmxlcyIsInByZXZOYXZpZ2FibGUiLCJnZXROYXZpZ2FibGVJbmRleGVzIiwibiIsImNsZWFuVXBFdmVudHMiLCJvZmYiLCJpbnRlcnJ1cHQiLCJ2aXNpYmlsaXR5IiwiY2xlYW5VcFNsaWRlRXZlbnRzIiwib3JpZW50YXRpb25DaGFuZ2UiLCJyZXNpemUiLCJjbGVhblVwUm93cyIsInN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbiIsInN0b3BQcm9wYWdhdGlvbiIsImRlc3Ryb3kiLCJyZW1vdmUiLCJmYWRlU2xpZGUiLCJzbGlkZUluZGV4Iiwib3BhY2l0eSIsImZhZGVTbGlkZU91dCIsImZpbHRlclNsaWRlcyIsInNsaWNrRmlsdGVyIiwiZmlsdGVyIiwiZm9jdXNIYW5kbGVyIiwiJHNmIiwiZ2V0Q3VycmVudCIsInNsaWNrQ3VycmVudFNsaWRlIiwiYnJlYWtQb2ludCIsImNvdW50ZXIiLCJwYWdlclF0eSIsImdldExlZnQiLCJ2ZXJ0aWNhbEhlaWdodCIsInZlcnRpY2FsT2Zmc2V0IiwidGFyZ2V0U2xpZGUiLCJmbG9vciIsIm9mZnNldExlZnQiLCJvdXRlcldpZHRoIiwiZ2V0T3B0aW9uIiwic2xpY2tHZXRPcHRpb24iLCJvcHRpb24iLCJpbmRleGVzIiwibWF4IiwicHVzaCIsImdldFNsaWNrIiwiZ2V0U2xpZGVDb3VudCIsInNsaWRlc1RyYXZlcnNlZCIsInN3aXBlZFNsaWRlIiwiY2VudGVyT2Zmc2V0IiwiYWJzIiwiZ29UbyIsInNsaWNrR29UbyIsInBhcnNlSW50IiwiY3JlYXRpb24iLCJoYXNDbGFzcyIsInNldFByb3BzIiwic3RhcnRMb2FkIiwibG9hZFNsaWRlciIsImluaXRpYWxpemVFdmVudHMiLCJ1cGRhdGVBcnJvd3MiLCJpbml0QURBIiwiZW5kIiwiaW5pdEFycm93RXZlbnRzIiwiaW5pdERvdEV2ZW50cyIsImluaXRTbGlkZUV2ZW50cyIsImFjdGlvbiIsImluaXRVSSIsInNob3ciLCJ0YWdOYW1lIiwibWF0Y2giLCJrZXlDb2RlIiwibG9hZFJhbmdlIiwiY2xvbmVSYW5nZSIsInJhbmdlU3RhcnQiLCJyYW5nZUVuZCIsImxvYWRJbWFnZXMiLCJpbWFnZXNTY29wZSIsImltYWdlIiwiaW1hZ2VTb3VyY2UiLCJpbWFnZVRvTG9hZCIsIm9ubG9hZCIsIm9uZXJyb3IiLCJzcmMiLCJzbGljZSIsInByb2dyZXNzaXZlTGF6eUxvYWQiLCJuZXh0Iiwic2xpY2tOZXh0IiwicGF1c2UiLCJzbGlja1BhdXNlIiwicGxheSIsInNsaWNrUGxheSIsInBvc3RTbGlkZSIsInByZXYiLCJzbGlja1ByZXYiLCJ0cnlDb3VudCIsIiRpbWdzVG9Mb2FkIiwiaW5pdGlhbGl6aW5nIiwibGFzdFZpc2libGVJbmRleCIsImN1cnJlbnRCcmVha3BvaW50IiwibCIsInJlc3BvbnNpdmVTZXR0aW5ncyIsInR5cGUiLCJzcGxpY2UiLCJzb3J0IiwiY2xlYXJUaW1lb3V0Iiwid2luZG93RGVsYXkiLCJyZW1vdmVTbGlkZSIsInNsaWNrUmVtb3ZlIiwicmVtb3ZlQmVmb3JlIiwicmVtb3ZlQWxsIiwic2V0Q1NTIiwicG9zaXRpb24iLCJwb3NpdGlvblByb3BzIiwieCIsInkiLCJzZXREaW1lbnNpb25zIiwicGFkZGluZyIsIm9mZnNldCIsInNldEZhZGUiLCJyaWdodCIsInNldEhlaWdodCIsInNldE9wdGlvbiIsInNsaWNrU2V0T3B0aW9uIiwiaXRlbSIsInZhbHVlIiwiYXJndW1lbnRzIiwib3B0IiwidmFsIiwiYm9keVN0eWxlIiwiYm9keSIsInN0eWxlIiwiV2Via2l0VHJhbnNpdGlvbiIsInVuZGVmaW5lZCIsIk1velRyYW5zaXRpb24iLCJtc1RyYW5zaXRpb24iLCJPVHJhbnNmb3JtIiwicGVyc3BlY3RpdmVQcm9wZXJ0eSIsIndlYmtpdFBlcnNwZWN0aXZlIiwiTW96VHJhbnNmb3JtIiwiTW96UGVyc3BlY3RpdmUiLCJ3ZWJraXRUcmFuc2Zvcm0iLCJtc1RyYW5zZm9ybSIsInRyYW5zZm9ybSIsImFsbFNsaWRlcyIsInJlbWFpbmRlciIsImluZmluaXRlQ291bnQiLCJjbG9uZSIsInRvZ2dsZSIsInRhcmdldEVsZW1lbnQiLCJwYXJlbnRzIiwic3luYyIsImFuaW1TbGlkZSIsIm9sZFNsaWRlIiwic2xpZGVMZWZ0IiwibmF2VGFyZ2V0IiwiaGlkZSIsInN3aXBlRGlyZWN0aW9uIiwieERpc3QiLCJ5RGlzdCIsInIiLCJzd2lwZUFuZ2xlIiwic3RhcnRYIiwiY3VyWCIsInN0YXJ0WSIsImN1clkiLCJhdGFuMiIsInJvdW5kIiwiUEkiLCJzd2lwZUVuZCIsInN3aXBlTGVuZ3RoIiwiZWRnZUhpdCIsIm1pblN3aXBlIiwiaW5kZXhPZiIsImZpbmdlckNvdW50Iiwib3JpZ2luYWxFdmVudCIsInRvdWNoZXMiLCJzd2lwZVN0YXJ0Iiwic3dpcGVNb3ZlIiwiZWRnZVdhc0hpdCIsImN1ckxlZnQiLCJwb3NpdGlvbk9mZnNldCIsInBhZ2VYIiwiY2xpZW50WCIsInBhZ2VZIiwiY2xpZW50WSIsInNxcnQiLCJwb3ciLCJ1bmZpbHRlclNsaWRlcyIsInNsaWNrVW5maWx0ZXIiLCJmcm9tQnJlYWtwb2ludCIsImFyZ3MiLCJBcnJheSIsInJldCIsImFwcGx5IiwiX3RvZ2dsZUhlYWRlckZvcm0iLCJsYXp5SW1hZ2VMb2FkIiwibGF6eUltYWdlcyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJpbkFkdmFuY2UiLCJmb3JFYWNoIiwib2Zmc2V0UGFyZW50IiwiY2xhc3NMaXN0IiwiY29udGFpbnMiLCJvZmZzZXRUb3AiLCJpbm5lckhlaWdodCIsInBhZ2VZT2Zmc2V0IiwiZGF0YXNldCIsImJnIiwiYmFja2dyb3VuZEltYWdlIiwiaWZyYW1lIiwiYWRkRXZlbnRMaXN0ZW5lciIsInQiLCJlIiwidSIsImYiLCJMIiwiZGV2aWNlUGl4ZWxSYXRpbyIsImRlbGF5IiwicyIsImNvbWJpbmVkIiwidiIsInRocm90dGxlIiwidyIsIkIiLCJhbGwiLCJnIiwibG9hZGVkTmFtZSIsImFwcGVuZFNjcm9sbCIsImRlZmF1bHRJbWFnZSIsIm8iLCJwbGFjZWhvbGRlciIsImltYWdlQmFzZSIsInNyY3NldEF0dHJpYnV0ZSIsImxvYWRlckF0dHJpYnV0ZSIsIl9mIiwibSIsImhhbmRsZWROYW1lIiwiYXR0cmlidXRlIiwibmFtZSIsImQiLCJBIiwiaCIsImltYWdlQmFzZUF0dHJpYnV0ZSIsIk4iLCJFIiwiTyIsImF1dG9EZXN0cm95IiwicCIsInZpc2libGVPbmx5IiwiRiIsInoiLCJub29wIiwic2l6ZXNBdHRyaWJ1dGUiLCJyZXRpbmFBdHRyaWJ1dGUiLCJyZW1vdmVBdHRyaWJ1dGUiLCJUIiwiSSIsIm9uZSIsIkQiLCJJbWFnZSIsIkMiLCJlZmZlY3QiLCJlZmZlY3RUaW1lIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0Iiwic2Nyb2xsRGlyZWN0aW9uIiwidGhyZXNob2xkIiwiYm90dG9tIiwidG9Mb3dlckNhc2UiLCJzcGxpdCIsInRyaW0iLCJEYXRlIiwiZW5hYmxlVGhyb3R0bGUiLCJiaW5kIiwiY29uZmlnIiwiYWRkSXRlbXMiLCJnZXRJdGVtcyIsInVwZGF0ZSIsImZvcmNlIiwibG9hZEFsbCIsImNoYWluYWJsZSIsIlplcHRvIiwiTGF6eSIsImxhenkiLCJpc0Z1bmN0aW9uIiwiaXNBcnJheSIsImJlZm9yZUxvYWQiLCJhZnRlckxvYWQiLCJvbkVycm9yIiwib25GaW5pc2hlZEFsbCIsIiRoZWFkZXIiLCIkbWVudSIsInRvZ2dsZUFjdGl2ZSIsIm1vYmlsZV9vcGVuX21lbnUiLCIkd3JhcCIsIl9wcmV2IiwiX25leHQiLCJfdG9nZ2xlIl0sIm1hcHBpbmdzIjoiOzs7O0FBQUEsQ0FBQyxVQUFTQSxDQUFULEVBQVc7QUFFWjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQ0VBLEVBQUFBLENBQUMsQ0FBQ0MsRUFBRkQsQ0FBS0UsV0FBTEYsR0FBbUIsVUFBV0csT0FBWCxFQUFxQjtBQUV0QztBQUNBLFFBQUlDLFFBQVEsR0FBRztBQUNiQyxNQUFBQSxJQUFJLEVBQUUsTUFETztBQUViQyxNQUFBQSxLQUFLLEVBQUUsTUFGTTtBQUdiQyxNQUFBQSxPQUFPLEVBQUUsY0FISTtBQUliQyxNQUFBQSxLQUFLLEVBQUUsTUFKTTtBQUtiQyxNQUFBQSxNQUFNLEVBQUUsTUFMSztBQU1iQyxNQUFBQSxZQUFZLEVBQUU7QUFORCxLQUFmO0FBU0EsUUFBSUMsT0FBTyxHQUFHO0FBQ1pDLE1BQUFBLFlBQVksRUFBRSxpQ0FERjtBQUVaQyxNQUFBQSxRQUFRLEVBQUU7QUFGRSxLQUFkO0FBS0EsUUFBSUMsUUFBSjtBQUNBLFFBQUlDLFNBQUo7QUFFQSxRQUFJQyxLQUFLLEdBQUdoQixDQUFDLENBQUMsSUFBRCxDQUFiO0FBRUEsUUFBSWlCLFFBQVEsR0FBR2pCLENBQUMsQ0FBQ2tCLE1BQUZsQixDQUFVLEVBQVZBLEVBQWNXLE9BQWRYLEVBQXVCSSxRQUF2QkosRUFBaUNHLE9BQWpDSCxDQUFmOztBQUVBLFFBQUdpQixRQUFRLENBQUNWLE9BQVRVLElBQW9CYixRQUFRLENBQUNHLE9BQWhDLEVBQXdDO0FBQ3RDUyxNQUFBQSxLQUFLLENBQUNHLE1BQU5ILENBQWFDLFFBQVEsQ0FBQ0wsWUFBdEJJO0FBQ0Q7QUFFRDs7O0FBQ0FGLElBQUFBLFFBQVEsR0FBR2QsQ0FBQyxDQUFDaUIsUUFBUSxDQUFDVixPQUFWLENBQVpPOztBQUNBLFFBQUdHLFFBQVEsQ0FBQ1AsWUFBWixFQUF5QjtBQUN2QkksTUFBQUEsUUFBUSxDQUFDTSxRQUFUTixDQUFrQixrQkFBbEJBO0FBQ0Q7O0FBRUQsWUFBU0csUUFBUSxDQUFDWixJQUFsQjtBQUNFLFdBQUssTUFBTDtBQUNFVSxRQUFBQSxTQUFTLEdBQUcsbUJBQVpBO0FBQ0E7O0FBQ0YsV0FBSyxLQUFMO0FBQ0VBLFFBQUFBLFNBQVMsR0FBRyxtQkFBWkE7QUFDQTs7QUFDRixXQUFLLE9BQUw7QUFDRUEsUUFBQUEsU0FBUyxHQUFHLGtCQUFaQTtBQUNBOztBQUNGLFdBQUssUUFBTDtBQUNFQSxRQUFBQSxTQUFTLEdBQUcsa0JBQVpBO0FBQ0E7QUFaSjs7QUFlQSxRQUFJTSxNQUFNLEdBQUc7QUFDWCxtQkFBY04sU0FESDtBQUVYLG9CQUFlRSxRQUFRLENBQUNYLEtBRmI7QUFHWEcsTUFBQUEsTUFBTSxFQUFHUSxRQUFRLENBQUNSLE1BSFA7QUFJWEQsTUFBQUEsS0FBSyxFQUFHUyxRQUFRLENBQUNUO0FBSk4sS0FBYjtBQU9BLFdBQU8sS0FBS2MsSUFBTCxDQUFVLFlBQVc7QUFFeEJOLE1BQUFBLEtBQUssQ0FBQ0ksUUFBTkosQ0FBZSxnQkFBZkEsRUFBaUNPLElBQWpDUCxDQUFzQ0MsUUFBUSxDQUFDSixRQUEvQ0c7QUFDQUEsTUFBQUEsS0FBSyxDQUFDUSxNQUFOUixHQUFlUyxHQUFmVCxDQUFvQkssTUFBcEJMO0FBQ0FVLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUkQsQ0FBWVosUUFBWlk7QUFDQVosTUFBQUEsUUFBUSxDQUFDYyxFQUFUZCxDQUFZLE9BQVpBLEVBQXFCLFlBQVU7QUFDN0JZLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUkQsQ0FBWSxZQUFaQTtBQUVBMUIsUUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBREEsQ0FBUTZCLFdBQVI3QixDQUFvQixrQkFBcEJBO0FBQ0EwQixRQUFBQSxPQUFPLENBQUNDLEdBQVJELENBQVlWLEtBQVpVO0FBQ0FWLFFBQUFBLEtBQUssQ0FBQ2MsT0FBTmQsQ0FBYyxzQkFBZEEsRUFBc0NhLFdBQXRDYixDQUFrRCxnQkFBbERBO0FBTEYsT0FBQUY7QUFMRyxLQUFBLENBQVA7QUF4REYsR0FBQWQ7O0FBMkVBQSxFQUFBQSxDQUFDLENBQUMsY0FBRCxDQUFEQSxDQUFrQkUsV0FBbEJGLENBQThCO0FBQzVCSyxJQUFBQSxJQUFJLEVBQUU7QUFEc0IsR0FBOUJMO0FBakhGLENBQUEsRUFzSEcrQixNQXRISDs7QUM0R0E7QUM1R0E7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQyxDQUFBLFVBQVNDLE9BQVQsRUFBa0I7QUFDZjs7QUFDQSxNQUFJLE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0NBLE1BQU0sQ0FBQ0MsR0FBM0MsRUFBZ0Q7QUFDNUNELElBQUFBLE1BQU0sQ0FBQyxDQUFDLFFBQUQsQ0FBRCxFQUFhRCxPQUFiLENBQU5DO0FBREosR0FBQSxNQUVPLElBQUksT0FBT0UsT0FBUCxLQUFtQixXQUF2QixFQUFvQztBQUN2Q0MsSUFBQUEsTUFBTSxDQUFDRCxPQUFQQyxHQUFpQkosT0FBTyxDQUFDSyxPQUFPLENBQUMsUUFBRCxDQUFSLENBQXhCRDtBQURHLEdBQUEsTUFFQTtBQUNISixJQUFBQSxPQUFPLENBQUNELE1BQUQsQ0FBUEM7QUFDSDtBQVJKLENBQUEsRUFVQyxVQUFTaEMsQ0FBVCxFQUFZO0FBQ1Y7O0FBQ0EsTUFBSXNDLEtBQUssR0FBR0MsTUFBTSxDQUFDRCxLQUFQQyxJQUFnQixFQUE1Qjs7QUFFQUQsRUFBQUEsS0FBSyxHQUFJLFlBQVc7QUFFaEIsUUFBSUUsV0FBVyxHQUFHLENBQWxCOztBQUVBLGFBQVNGLEtBQVQsQ0FBZUcsT0FBZixFQUF3QnhCLFFBQXhCLEVBQWtDO0FBRTlCLFVBQUl5QixDQUFDLEdBQUcsSUFBUjtBQUFBLFVBQWNDLFlBQWQ7O0FBRUFELE1BQUFBLENBQUMsQ0FBQ3RDLFFBQUZzQyxHQUFhO0FBQ1RFLFFBQUFBLGFBQWEsRUFBRSxJQUROO0FBRVRDLFFBQUFBLGNBQWMsRUFBRSxLQUZQO0FBR1RDLFFBQUFBLFlBQVksRUFBRTlDLENBQUMsQ0FBQ3lDLE9BQUQsQ0FITjtBQUlUTSxRQUFBQSxVQUFVLEVBQUUvQyxDQUFDLENBQUN5QyxPQUFELENBSko7QUFLVE8sUUFBQUEsTUFBTSxFQUFFLElBTEM7QUFNVEMsUUFBQUEsUUFBUSxFQUFFLElBTkQ7QUFPVEMsUUFBQUEsU0FBUyxFQUFFLDhIQVBGO0FBUVRDLFFBQUFBLFNBQVMsRUFBRSxzSEFSRjtBQVNUQyxRQUFBQSxRQUFRLEVBQUUsS0FURDtBQVVUQyxRQUFBQSxhQUFhLEVBQUUsSUFWTjtBQVdUQyxRQUFBQSxVQUFVLEVBQUUsS0FYSDtBQVlUQyxRQUFBQSxhQUFhLEVBQUUsTUFaTjtBQWFUQyxRQUFBQSxPQUFPLEVBQUUsTUFiQTtBQWNUQyxRQUFBQSxZQUFZLEVBQUUsc0JBQVNDLE1BQVQsRUFBaUJDLENBQWpCLEVBQW9CO0FBQzlCLGlCQUFPM0QsQ0FBQyxDQUFDLHNFQUFELENBQURBLENBQTBFNEQsSUFBMUU1RCxDQUErRTJELENBQUMsR0FBRyxDQUFuRjNELENBQVA7QUFmSyxTQUFBO0FBaUJUNkQsUUFBQUEsSUFBSSxFQUFFLEtBakJHO0FBa0JUQyxRQUFBQSxTQUFTLEVBQUUsWUFsQkY7QUFtQlRDLFFBQUFBLFNBQVMsRUFBRSxJQW5CRjtBQW9CVEMsUUFBQUEsTUFBTSxFQUFFLFFBcEJDO0FBcUJUQyxRQUFBQSxZQUFZLEVBQUUsSUFyQkw7QUFzQlRDLFFBQUFBLElBQUksRUFBRSxLQXRCRztBQXVCVEMsUUFBQUEsYUFBYSxFQUFFLEtBdkJOO0FBd0JUQyxRQUFBQSxRQUFRLEVBQUUsSUF4QkQ7QUF5QlRDLFFBQUFBLFlBQVksRUFBRSxDQXpCTDtBQTBCVEMsUUFBQUEsUUFBUSxFQUFFLFVBMUJEO0FBMkJUQyxRQUFBQSxXQUFXLEVBQUUsS0EzQko7QUE0QlRDLFFBQUFBLFlBQVksRUFBRSxJQTVCTDtBQTZCVEMsUUFBQUEsWUFBWSxFQUFFLElBN0JMO0FBOEJUQyxRQUFBQSxnQkFBZ0IsRUFBRSxLQTlCVDtBQStCVEMsUUFBQUEsU0FBUyxFQUFFLFFBL0JGO0FBZ0NUQyxRQUFBQSxVQUFVLEVBQUUsSUFoQ0g7QUFpQ1RDLFFBQUFBLElBQUksRUFBRSxDQWpDRztBQWtDVEMsUUFBQUEsR0FBRyxFQUFFLEtBbENJO0FBbUNUQyxRQUFBQSxLQUFLLEVBQUUsRUFuQ0U7QUFvQ1RDLFFBQUFBLFlBQVksRUFBRSxDQXBDTDtBQXFDVEMsUUFBQUEsWUFBWSxFQUFFLENBckNMO0FBc0NUQyxRQUFBQSxjQUFjLEVBQUUsQ0F0Q1A7QUF1Q1Q1RSxRQUFBQSxLQUFLLEVBQUUsR0F2Q0U7QUF3Q1Q2RSxRQUFBQSxLQUFLLEVBQUUsSUF4Q0U7QUF5Q1RDLFFBQUFBLFlBQVksRUFBRSxLQXpDTDtBQTBDVEMsUUFBQUEsU0FBUyxFQUFFLElBMUNGO0FBMkNUQyxRQUFBQSxjQUFjLEVBQUUsQ0EzQ1A7QUE0Q1RDLFFBQUFBLE1BQU0sRUFBRSxJQTVDQztBQTZDVEMsUUFBQUEsWUFBWSxFQUFFLElBN0NMO0FBOENUQyxRQUFBQSxhQUFhLEVBQUUsS0E5Q047QUErQ1RDLFFBQUFBLFFBQVEsRUFBRSxLQS9DRDtBQWdEVEMsUUFBQUEsZUFBZSxFQUFFLEtBaERSO0FBaURUQyxRQUFBQSxjQUFjLEVBQUUsSUFqRFA7QUFrRFRDLFFBQUFBLE1BQU0sRUFBRTtBQWxEQyxPQUFibkQ7QUFxREFBLE1BQUFBLENBQUMsQ0FBQ29ELFFBQUZwRCxHQUFhO0FBQ1RxRCxRQUFBQSxTQUFTLEVBQUUsS0FERjtBQUVUQyxRQUFBQSxRQUFRLEVBQUUsS0FGRDtBQUdUQyxRQUFBQSxhQUFhLEVBQUUsSUFITjtBQUlUQyxRQUFBQSxnQkFBZ0IsRUFBRSxDQUpUO0FBS1RDLFFBQUFBLFdBQVcsRUFBRSxJQUxKO0FBTVRDLFFBQUFBLFlBQVksRUFBRSxDQU5MO0FBT1RDLFFBQUFBLFNBQVMsRUFBRSxDQVBGO0FBUVRDLFFBQUFBLEtBQUssRUFBRSxJQVJFO0FBU1RDLFFBQUFBLFNBQVMsRUFBRSxJQVRGO0FBVVRDLFFBQUFBLFVBQVUsRUFBRSxJQVZIO0FBV1RDLFFBQUFBLFNBQVMsRUFBRSxDQVhGO0FBWVRDLFFBQUFBLFVBQVUsRUFBRSxJQVpIO0FBYVRDLFFBQUFBLFVBQVUsRUFBRSxJQWJIO0FBY1RDLFFBQUFBLFVBQVUsRUFBRSxJQWRIO0FBZVRDLFFBQUFBLFVBQVUsRUFBRSxJQWZIO0FBZ0JUQyxRQUFBQSxXQUFXLEVBQUUsSUFoQko7QUFpQlRDLFFBQUFBLE9BQU8sRUFBRSxJQWpCQTtBQWtCVEMsUUFBQUEsT0FBTyxFQUFFLEtBbEJBO0FBbUJUQyxRQUFBQSxXQUFXLEVBQUUsQ0FuQko7QUFvQlRDLFFBQUFBLFNBQVMsRUFBRSxJQXBCRjtBQXFCVEMsUUFBQUEsS0FBSyxFQUFFLElBckJFO0FBc0JUQyxRQUFBQSxXQUFXLEVBQUUsRUF0Qko7QUF1QlRDLFFBQUFBLGlCQUFpQixFQUFFLEtBdkJWO0FBd0JUQyxRQUFBQSxTQUFTLEVBQUU7QUF4QkYsT0FBYjVFO0FBMkJBMUMsTUFBQUEsQ0FBQyxDQUFDa0IsTUFBRmxCLENBQVMwQyxDQUFUMUMsRUFBWTBDLENBQUMsQ0FBQ29ELFFBQWQ5RjtBQUVBMEMsTUFBQUEsQ0FBQyxDQUFDNkUsZ0JBQUY3RSxHQUFxQixJQUFyQkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDOEUsUUFBRjlFLEdBQWEsSUFBYkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDK0UsUUFBRi9FLEdBQWEsSUFBYkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDZ0YsV0FBRmhGLEdBQWdCLEVBQWhCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUNpRixrQkFBRmpGLEdBQXVCLEVBQXZCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUNrRixjQUFGbEYsR0FBbUIsS0FBbkJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ21GLFFBQUZuRixHQUFhLEtBQWJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ29GLFdBQUZwRixHQUFnQixLQUFoQkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDcUYsTUFBRnJGLEdBQVcsUUFBWEE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDc0YsTUFBRnRGLEdBQVcsSUFBWEE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDdUYsWUFBRnZGLEdBQWlCLElBQWpCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUNpQyxTQUFGakMsR0FBYyxJQUFkQTtBQUNBQSxNQUFBQSxDQUFDLENBQUN3RixRQUFGeEYsR0FBYSxDQUFiQTtBQUNBQSxNQUFBQSxDQUFDLENBQUN5RixXQUFGekYsR0FBZ0IsSUFBaEJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixHQUFZMUMsQ0FBQyxDQUFDeUMsT0FBRCxDQUFiQztBQUNBQSxNQUFBQSxDQUFDLENBQUMyRixZQUFGM0YsR0FBaUIsSUFBakJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzRGLGFBQUY1RixHQUFrQixJQUFsQkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRjdGLEdBQW1CLElBQW5CQTtBQUNBQSxNQUFBQSxDQUFDLENBQUM4RixnQkFBRjlGLEdBQXFCLGtCQUFyQkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDK0YsV0FBRi9GLEdBQWdCLENBQWhCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUNnRyxXQUFGaEcsR0FBZ0IsSUFBaEJBO0FBRUFDLE1BQUFBLFlBQVksR0FBRzNDLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRHpDLENBQVcySSxJQUFYM0ksQ0FBZ0IsT0FBaEJBLEtBQTRCLEVBQTNDMkM7QUFFQUQsTUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLEdBQVkxQyxDQUFDLENBQUNrQixNQUFGbEIsQ0FBUyxFQUFUQSxFQUFhMEMsQ0FBQyxDQUFDdEMsUUFBZkosRUFBeUJpQixRQUF6QmpCLEVBQW1DMkMsWUFBbkMzQyxDQUFaMEM7QUFFQUEsTUFBQUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTJCLFlBQTNCM0I7QUFFQUEsTUFBQUEsQ0FBQyxDQUFDa0csZ0JBQUZsRyxHQUFxQkEsQ0FBQyxDQUFDdkMsT0FBdkJ1Qzs7QUFFQSxVQUFJLE9BQU9tRyxRQUFRLENBQUNDLFNBQWhCLEtBQThCLFdBQWxDLEVBQStDO0FBQzNDcEcsUUFBQUEsQ0FBQyxDQUFDcUYsTUFBRnJGLEdBQVcsV0FBWEE7QUFDQUEsUUFBQUEsQ0FBQyxDQUFDOEYsZ0JBQUY5RixHQUFxQixxQkFBckJBO0FBRkosT0FBQSxNQUdPLElBQUksT0FBT21HLFFBQVEsQ0FBQ0UsWUFBaEIsS0FBaUMsV0FBckMsRUFBa0Q7QUFDckRyRyxRQUFBQSxDQUFDLENBQUNxRixNQUFGckYsR0FBVyxjQUFYQTtBQUNBQSxRQUFBQSxDQUFDLENBQUM4RixnQkFBRjlGLEdBQXFCLHdCQUFyQkE7QUFDSDs7QUFFREEsTUFBQUEsQ0FBQyxDQUFDc0csUUFBRnRHLEdBQWExQyxDQUFDLENBQUNpSixLQUFGakosQ0FBUTBDLENBQUMsQ0FBQ3NHLFFBQVZoSixFQUFvQjBDLENBQXBCMUMsQ0FBYjBDO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3dHLGFBQUZ4RyxHQUFrQjFDLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDd0csYUFBVmxKLEVBQXlCMEMsQ0FBekIxQyxDQUFsQjBDO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3lHLGdCQUFGekcsR0FBcUIxQyxDQUFDLENBQUNpSixLQUFGakosQ0FBUTBDLENBQUMsQ0FBQ3lHLGdCQUFWbkosRUFBNEIwQyxDQUE1QjFDLENBQXJCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDMEcsV0FBRjFHLEdBQWdCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUMwRyxXQUFWcEosRUFBdUIwQyxDQUF2QjFDLENBQWhCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDMkcsWUFBRjNHLEdBQWlCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUMyRyxZQUFWckosRUFBd0IwQyxDQUF4QjFDLENBQWpCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNEcsYUFBRjVHLEdBQWtCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUM0RyxhQUFWdEosRUFBeUIwQyxDQUF6QjFDLENBQWxCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNkcsV0FBRjdHLEdBQWdCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUM2RyxXQUFWdkosRUFBdUIwQyxDQUF2QjFDLENBQWhCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDOEcsWUFBRjlHLEdBQWlCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUM4RyxZQUFWeEosRUFBd0IwQyxDQUF4QjFDLENBQWpCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDK0csV0FBRi9HLEdBQWdCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUMrRyxXQUFWekosRUFBdUIwQyxDQUF2QjFDLENBQWhCMEM7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDZ0gsVUFBRmhILEdBQWUxQyxDQUFDLENBQUNpSixLQUFGakosQ0FBUTBDLENBQUMsQ0FBQ2dILFVBQVYxSixFQUFzQjBDLENBQXRCMUMsQ0FBZjBDO0FBRUFBLE1BQUFBLENBQUMsQ0FBQ0YsV0FBRkUsR0FBZ0JGLFdBQVcsRUFBM0JFLENBdkk4QixDQXlJOUI7QUFDQTtBQUNBOztBQUNBQSxNQUFBQSxDQUFDLENBQUNpSCxRQUFGakgsR0FBYSwyQkFBYkE7O0FBR0FBLE1BQUFBLENBQUMsQ0FBQ2tILG1CQUFGbEg7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ21ILElBQUZuSCxDQUFPLElBQVBBO0FBRUg7O0FBRUQsV0FBT0osS0FBUDtBQXhKSyxHQUFBLEVBQVRBOztBQTRKQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCeUgsV0FBaEJ6SCxHQUE4QixZQUFXO0FBQ3JDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY3NILElBQWR0SCxDQUFtQixlQUFuQkEsRUFBb0N1SCxJQUFwQ3ZILENBQXlDO0FBQ3JDLHFCQUFlO0FBRHNCLEtBQXpDQSxFQUVHc0gsSUFGSHRILENBRVEsMEJBRlJBLEVBRW9DdUgsSUFGcEN2SCxDQUV5QztBQUNyQyxrQkFBWTtBQUR5QixLQUZ6Q0E7QUFISixHQUFBSjs7QUFXQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCNEgsUUFBaEI1SCxHQUEyQkEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCNkgsUUFBaEI3SCxHQUEyQixVQUFTOEgsTUFBVCxFQUFpQkMsS0FBakIsRUFBd0JDLFNBQXhCLEVBQW1DO0FBRXJGLFFBQUk1SCxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJLE9BQU8ySCxLQUFQLEtBQWtCLFNBQXRCLEVBQWlDO0FBQzdCQyxNQUFBQSxTQUFTLEdBQUdELEtBQVpDO0FBQ0FELE1BQUFBLEtBQUssR0FBRyxJQUFSQTtBQUZKLEtBQUEsTUFHTyxJQUFJQSxLQUFLLEdBQUcsQ0FBUkEsSUFBY0EsS0FBSyxJQUFJM0gsQ0FBQyxDQUFDa0UsVUFBN0IsRUFBMEM7QUFDN0MsYUFBTyxLQUFQO0FBQ0g7O0FBRURsRSxJQUFBQSxDQUFDLENBQUM2SCxNQUFGN0g7O0FBRUEsUUFBSSxPQUFPMkgsS0FBUCxLQUFrQixRQUF0QixFQUFnQztBQUM1QixVQUFJQSxLQUFLLEtBQUssQ0FBVkEsSUFBZTNILENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVOEgsTUFBVjlILEtBQXFCLENBQXhDLEVBQTJDO0FBQ3ZDMUMsUUFBQUEsQ0FBQyxDQUFDb0ssTUFBRCxDQUFEcEssQ0FBVXlLLFFBQVZ6SyxDQUFtQjBDLENBQUMsQ0FBQ29FLFdBQXJCOUc7QUFESixPQUFBLE1BRU8sSUFBSXNLLFNBQUosRUFBZTtBQUNsQnRLLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRHBLLENBQVUwSyxZQUFWMUssQ0FBdUIwQyxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVWlJLEVBQVZqSSxDQUFhMkgsS0FBYjNILENBQXZCMUM7QUFERyxPQUFBLE1BRUE7QUFDSEEsUUFBQUEsQ0FBQyxDQUFDb0ssTUFBRCxDQUFEcEssQ0FBVTRLLFdBQVY1SyxDQUFzQjBDLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVaUksRUFBVmpJLENBQWEySCxLQUFiM0gsQ0FBdEIxQztBQUNIO0FBUEwsS0FBQSxNQVFPO0FBQ0gsVUFBSXNLLFNBQVMsS0FBSyxJQUFsQixFQUF3QjtBQUNwQnRLLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRHBLLENBQVU2SyxTQUFWN0ssQ0FBb0IwQyxDQUFDLENBQUNvRSxXQUF0QjlHO0FBREosT0FBQSxNQUVPO0FBQ0hBLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRHBLLENBQVV5SyxRQUFWekssQ0FBbUIwQyxDQUFDLENBQUNvRSxXQUFyQjlHO0FBQ0g7QUFDSjs7QUFFRDBDLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxHQUFZQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixLQUFLdkMsT0FBTCxDQUFhNEUsS0FBcENyQyxDQUFaQTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNvSSxRQUFkcEksQ0FBdUIsS0FBS3ZDLE9BQUwsQ0FBYTRFLEtBQXBDckMsRUFBMkNxSSxNQUEzQ3JJOztBQUVBQSxJQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY3NJLE1BQWR0SSxDQUFxQkEsQ0FBQyxDQUFDcUUsT0FBdkJyRTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVVwQixJQUFWb0IsQ0FBZSxVQUFTMkgsS0FBVCxFQUFnQjVILE9BQWhCLEVBQXlCO0FBQ3BDekMsTUFBQUEsQ0FBQyxDQUFDeUMsT0FBRCxDQUFEekMsQ0FBV2lLLElBQVhqSyxDQUFnQixrQkFBaEJBLEVBQW9DcUssS0FBcENySztBQURKLEtBQUEwQzs7QUFJQUEsSUFBQUEsQ0FBQyxDQUFDMkYsWUFBRjNGLEdBQWlCQSxDQUFDLENBQUNxRSxPQUFuQnJFOztBQUVBQSxJQUFBQSxDQUFDLENBQUN1SSxNQUFGdkk7QUF6Q0osR0FBQUo7O0FBNkNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0I0SSxhQUFoQjVJLEdBQWdDLFlBQVc7QUFDdkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBQ0EsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsS0FBMkIsQ0FBM0JBLElBQWdDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUcsY0FBVkgsS0FBNkIsSUFBN0RBLElBQXFFQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWdELFFBQVZoRCxLQUF1QixLQUFoRyxFQUF1RztBQUNuRyxVQUFJeUksWUFBWSxHQUFHekksQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVVpSSxFQUFWakksQ0FBYUEsQ0FBQyxDQUFDMEQsWUFBZjFELEVBQTZCMEksV0FBN0IxSSxDQUF5QyxJQUF6Q0EsQ0FBbkI7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRMkksT0FBUjNJLENBQWdCO0FBQ1pqQyxRQUFBQSxNQUFNLEVBQUUwSztBQURJLE9BQWhCekksRUFFR0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVwQyxLQUZib0M7QUFHSDtBQVBMLEdBQUFKOztBQVVBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JnSixZQUFoQmhKLEdBQStCLFVBQVNpSixVQUFULEVBQXFCQyxRQUFyQixFQUErQjtBQUUxRCxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFBQSxRQUNJL0ksQ0FBQyxHQUFHLElBRFI7O0FBR0FBLElBQUFBLENBQUMsQ0FBQ3dJLGFBQUZ4STs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW9DLEdBQVZwQyxLQUFrQixJQUFsQkEsSUFBMEJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVZ0QsUUFBVmhELEtBQXVCLEtBQXJELEVBQTREO0FBQ3hENkksTUFBQUEsVUFBVSxHQUFHLENBQUNBLFVBQWRBO0FBQ0g7O0FBQ0QsUUFBSTdJLENBQUMsQ0FBQzJFLGlCQUFGM0UsS0FBd0IsS0FBNUIsRUFBbUM7QUFDL0IsVUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVnRCxRQUFWaEQsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUJBLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjMkksT0FBZDNJLENBQXNCO0FBQ2xCZ0osVUFBQUEsSUFBSSxFQUFFSDtBQURZLFNBQXRCN0ksRUFFR0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVwQyxLQUZib0MsRUFFb0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVc0IsTUFGOUJ0QixFQUVzQzhJLFFBRnRDOUk7QUFESixPQUFBLE1BSU87QUFDSEEsUUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWMySSxPQUFkM0ksQ0FBc0I7QUFDbEJpSixVQUFBQSxHQUFHLEVBQUVKO0FBRGEsU0FBdEI3SSxFQUVHQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXBDLEtBRmJvQyxFQUVvQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVzQixNQUY5QnRCLEVBRXNDOEksUUFGdEM5STtBQUdIO0FBVEwsS0FBQSxNQVdPO0FBRUgsVUFBSUEsQ0FBQyxDQUFDa0YsY0FBRmxGLEtBQXFCLEtBQXpCLEVBQWdDO0FBQzVCLFlBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVb0MsR0FBVnBDLEtBQWtCLElBQXRCLEVBQTRCO0FBQ3hCQSxVQUFBQSxDQUFDLENBQUN5RCxXQUFGekQsR0FBZ0IsQ0FBRUEsQ0FBQyxDQUFDeUQsV0FBcEJ6RDtBQUNIOztBQUNEMUMsUUFBQUEsQ0FBQyxDQUFDO0FBQ0U0TCxVQUFBQSxTQUFTLEVBQUVsSixDQUFDLENBQUN5RDtBQURmLFNBQUQsQ0FBRG5HLENBRUdxTCxPQUZIckwsQ0FFVztBQUNQNEwsVUFBQUEsU0FBUyxFQUFFTDtBQURKLFNBRlh2TCxFQUlHO0FBQ0M2TCxVQUFBQSxRQUFRLEVBQUVuSixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXBDLEtBRHJCO0FBRUMwRCxVQUFBQSxNQUFNLEVBQUV0QixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXNCLE1BRm5CO0FBR0M4SCxVQUFBQSxJQUFJLEVBQUUsY0FBU0MsR0FBVCxFQUFjO0FBQ2hCQSxZQUFBQSxHQUFHLEdBQUdDLElBQUksQ0FBQ0MsSUFBTEQsQ0FBVUQsR0FBVkMsQ0FBTkQ7O0FBQ0EsZ0JBQUlySixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWdELFFBQVZoRCxLQUF1QixLQUEzQixFQUFrQztBQUM5QitJLGNBQUFBLFNBQVMsQ0FBQy9JLENBQUMsQ0FBQzhFLFFBQUgsQ0FBVGlFLEdBQXdCLGVBQ3BCTSxHQURvQixHQUNkLFVBRFZOOztBQUVBL0ksY0FBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQixHQUFkaUIsQ0FBa0IrSSxTQUFsQi9JO0FBSEosYUFBQSxNQUlPO0FBQ0grSSxjQUFBQSxTQUFTLENBQUMvSSxDQUFDLENBQUM4RSxRQUFILENBQVRpRSxHQUF3QixtQkFDcEJNLEdBRG9CLEdBQ2QsS0FEVk47O0FBRUEvSSxjQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY2pCLEdBQWRpQixDQUFrQitJLFNBQWxCL0k7QUFDSDtBQWJOLFdBQUE7QUFlQ3dKLFVBQUFBLFFBQVEsRUFBRSxvQkFBVztBQUNqQixnQkFBSVYsUUFBSixFQUFjO0FBQ1ZBLGNBQUFBLFFBQVEsQ0FBQ1csSUFBVFg7QUFDSDtBQUNKO0FBbkJGLFNBSkh4TDtBQUpKLE9BQUEsTUE4Qk87QUFFSDBDLFFBQUFBLENBQUMsQ0FBQzBKLGVBQUYxSjs7QUFDQTZJLFFBQUFBLFVBQVUsR0FBR1MsSUFBSSxDQUFDQyxJQUFMRCxDQUFVVCxVQUFWUyxDQUFiVDs7QUFFQSxZQUFJN0ksQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVnRCxRQUFWaEQsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUIrSSxVQUFBQSxTQUFTLENBQUMvSSxDQUFDLENBQUM4RSxRQUFILENBQVRpRSxHQUF3QixpQkFBaUJGLFVBQWpCLEdBQThCLGVBQXRERTtBQURKLFNBQUEsTUFFTztBQUNIQSxVQUFBQSxTQUFTLENBQUMvSSxDQUFDLENBQUM4RSxRQUFILENBQVRpRSxHQUF3QixxQkFBcUJGLFVBQXJCLEdBQWtDLFVBQTFERTtBQUNIOztBQUNEL0ksUUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQixHQUFkaUIsQ0FBa0IrSSxTQUFsQi9JOztBQUVBLFlBQUk4SSxRQUFKLEVBQWM7QUFDVmEsVUFBQUEsVUFBVSxDQUFDLFlBQVc7QUFFbEIzSixZQUFBQSxDQUFDLENBQUM0SixpQkFBRjVKOztBQUVBOEksWUFBQUEsUUFBUSxDQUFDVyxJQUFUWDtBQUpNLFdBQUEsRUFLUDlJLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVcEMsS0FMSCxDQUFWK0w7QUFNSDtBQUVKO0FBRUo7QUE1RUwsR0FBQS9KOztBQWdGQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCaUssWUFBaEJqSyxHQUErQixZQUFXO0FBRXRDLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSU8sUUFBUSxHQUFHUCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVU8sUUFEekI7O0FBR0EsUUFBS0EsUUFBUSxJQUFJQSxRQUFRLEtBQUssSUFBOUIsRUFBcUM7QUFDakNBLE1BQUFBLFFBQVEsR0FBR2pELENBQUMsQ0FBQ2lELFFBQUQsQ0FBRGpELENBQVl3TSxHQUFaeE0sQ0FBZ0IwQyxDQUFDLENBQUMwRixPQUFsQnBJLENBQVhpRDtBQUNIOztBQUVELFdBQU9BLFFBQVA7QUFUSixHQUFBWDs7QUFhQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCVyxRQUFoQlgsR0FBMkIsVUFBUytILEtBQVQsRUFBZ0I7QUFFdkMsUUFBSTNILENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSU8sUUFBUSxHQUFHUCxDQUFDLENBQUM2SixZQUFGN0osRUFEZjs7QUFHQSxRQUFLTyxRQUFRLEtBQUssSUFBYkEsSUFBcUIsUUFBT0EsUUFBUCxNQUFvQixRQUE5QyxFQUF5RDtBQUNyREEsTUFBQUEsUUFBUSxDQUFDM0IsSUFBVDJCLENBQWMsWUFBVztBQUNyQixZQUFJd0osTUFBTSxHQUFHek0sQ0FBQyxDQUFDLElBQUQsQ0FBREEsQ0FBUTBNLEtBQVIxTSxDQUFjLFVBQWRBLENBQWI7O0FBQ0EsWUFBRyxDQUFDeU0sTUFBTSxDQUFDbkYsU0FBWCxFQUFzQjtBQUNsQm1GLFVBQUFBLE1BQU0sQ0FBQ0UsWUFBUEYsQ0FBb0JwQyxLQUFwQm9DLEVBQTJCLElBQTNCQTtBQUNIO0FBSkwsT0FBQXhKO0FBTUg7QUFaTCxHQUFBWDs7QUFnQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjhKLGVBQWhCOUosR0FBa0MsVUFBU3lDLEtBQVQsRUFBZ0I7QUFFOUMsUUFBSXJDLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWtLLFVBQVUsR0FBRyxFQURqQjs7QUFHQSxRQUFJbEssQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QixJQUFWeEIsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJrSyxNQUFBQSxVQUFVLENBQUNsSyxDQUFDLENBQUM2RixjQUFILENBQVZxRSxHQUErQmxLLENBQUMsQ0FBQzRGLGFBQUY1RixHQUFrQixHQUFsQkEsR0FBd0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVcEMsS0FBbENvQyxHQUEwQyxLQUExQ0EsR0FBa0RBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVYyxPQUEzRm9KO0FBREosS0FBQSxNQUVPO0FBQ0hBLE1BQUFBLFVBQVUsQ0FBQ2xLLENBQUMsQ0FBQzZGLGNBQUgsQ0FBVnFFLEdBQStCLGFBQWFsSyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXBDLEtBQXZCLEdBQStCLEtBQS9CLEdBQXVDb0MsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVjLE9BQWhGb0o7QUFDSDs7QUFFRCxRQUFJbEssQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QixJQUFWeEIsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJBLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjakIsR0FBZGlCLENBQWtCa0ssVUFBbEJsSztBQURKLEtBQUEsTUFFTztBQUNIQSxNQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVWlJLEVBQVZqSSxDQUFhcUMsS0FBYnJDLEVBQW9CakIsR0FBcEJpQixDQUF3QmtLLFVBQXhCbEs7QUFDSDtBQWZMLEdBQUFKOztBQW1CQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCMEcsUUFBaEIxRyxHQUEyQixZQUFXO0FBRWxDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUN3RyxhQUFGeEc7O0FBRUEsUUFBS0EsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBOUIsRUFBNkM7QUFDekN2QyxNQUFBQSxDQUFDLENBQUN1RCxhQUFGdkQsR0FBa0JtSyxXQUFXLENBQUVuSyxDQUFDLENBQUN5RyxnQkFBSixFQUFzQnpHLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVVyxhQUFoQyxDQUE3Qlg7QUFDSDtBQVJMLEdBQUFKOztBQVlBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0I0RyxhQUFoQjVHLEdBQWdDLFlBQVc7QUFFdkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdUQsYUFBTixFQUFxQjtBQUNqQjZHLE1BQUFBLGFBQWEsQ0FBQ3BLLENBQUMsQ0FBQ3VELGFBQUgsQ0FBYjZHO0FBQ0g7QUFOTCxHQUFBeEs7O0FBVUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjZHLGdCQUFoQjdHLEdBQW1DLFlBQVc7QUFFMUMsUUFBSUksQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJcUssT0FBTyxHQUFHckssQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBRHpDOztBQUdBLFFBQUssQ0FBQ3hDLENBQUMsQ0FBQ3NGLE1BQUgsSUFBYSxDQUFDdEYsQ0FBQyxDQUFDb0YsV0FBaEIsSUFBK0IsQ0FBQ3BGLENBQUMsQ0FBQ21GLFFBQXZDLEVBQWtEO0FBRTlDLFVBQUtuRixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBCLFFBQVYxQixLQUF1QixLQUE1QixFQUFvQztBQUVoQyxZQUFLQSxDQUFDLENBQUMyRCxTQUFGM0QsS0FBZ0IsQ0FBaEJBLElBQXVCQSxDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUIsQ0FBakJBLEtBQTJCQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZSxDQUF0RSxFQUEyRTtBQUN2RUEsVUFBQUEsQ0FBQyxDQUFDMkQsU0FBRjNELEdBQWMsQ0FBZEE7QUFESixTQUFBLE1BSUssSUFBS0EsQ0FBQyxDQUFDMkQsU0FBRjNELEtBQWdCLENBQXJCLEVBQXlCO0FBRTFCcUssVUFBQUEsT0FBTyxHQUFHckssQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQXJDNkg7O0FBRUEsY0FBS3JLLENBQUMsQ0FBQzBELFlBQUYxRCxHQUFpQixDQUFqQkEsS0FBdUIsQ0FBNUIsRUFBZ0M7QUFDNUJBLFlBQUFBLENBQUMsQ0FBQzJELFNBQUYzRCxHQUFjLENBQWRBO0FBQ0g7QUFFSjtBQUVKOztBQUVEQSxNQUFBQSxDQUFDLENBQUNpSyxZQUFGakssQ0FBZ0JxSyxPQUFoQnJLO0FBRUg7QUEzQkwsR0FBQUo7O0FBK0JBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0IwSyxXQUFoQjFLLEdBQThCLFlBQVc7QUFFckMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVNLE1BQVZOLEtBQXFCLElBQXpCLEVBQWdDO0FBRTVCQSxNQUFBQSxDQUFDLENBQUNpRSxVQUFGakUsR0FBZTFDLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVUSxTQUFYLENBQURsRCxDQUF1Qm9CLFFBQXZCcEIsQ0FBZ0MsYUFBaENBLENBQWYwQztBQUNBQSxNQUFBQSxDQUFDLENBQUNnRSxVQUFGaEUsR0FBZTFDLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVUyxTQUFYLENBQURuRCxDQUF1Qm9CLFFBQXZCcEIsQ0FBZ0MsYUFBaENBLENBQWYwQzs7QUFFQSxVQUFJQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUE3QixFQUE0QztBQUV4Q3ZDLFFBQUFBLENBQUMsQ0FBQ2lFLFVBQUZqRSxDQUFhdUssV0FBYnZLLENBQXlCLGNBQXpCQSxFQUF5Q3dLLFVBQXpDeEssQ0FBb0Qsc0JBQXBEQTs7QUFDQUEsUUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRmhFLENBQWF1SyxXQUFidkssQ0FBeUIsY0FBekJBLEVBQXlDd0ssVUFBekN4SyxDQUFvRCxzQkFBcERBOztBQUVBLFlBQUlBLENBQUMsQ0FBQ2lILFFBQUZqSCxDQUFXeUssSUFBWHpLLENBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVEsU0FBMUJSLENBQUosRUFBMEM7QUFDdENBLFVBQUFBLENBQUMsQ0FBQ2lFLFVBQUZqRSxDQUFhbUksU0FBYm5JLENBQXVCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUksWUFBakNKO0FBQ0g7O0FBRUQsWUFBSUEsQ0FBQyxDQUFDaUgsUUFBRmpILENBQVd5SyxJQUFYekssQ0FBZ0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVUyxTQUExQlQsQ0FBSixFQUEwQztBQUN0Q0EsVUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRmhFLENBQWErSCxRQUFiL0gsQ0FBc0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVSSxZQUFoQ0o7QUFDSDs7QUFFRCxZQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBCLFFBQVYxQixLQUF1QixJQUEzQixFQUFpQztBQUM3QkEsVUFBQUEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQ0t0QixRQURMc0IsQ0FDYyxnQkFEZEEsRUFFS3VILElBRkx2SCxDQUVVLGVBRlZBLEVBRTJCLE1BRjNCQTtBQUdIO0FBakJMLE9BQUEsTUFtQk87QUFFSEEsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQWEwSyxHQUFiMUssQ0FBa0JBLENBQUMsQ0FBQ2dFLFVBQXBCaEUsRUFFS3RCLFFBRkxzQixDQUVjLGNBRmRBLEVBR0t1SCxJQUhMdkgsQ0FHVTtBQUNGLDJCQUFpQixNQURmO0FBRUYsc0JBQVk7QUFGVixTQUhWQTtBQVFIO0FBRUo7QUF4Q0wsR0FBQUo7O0FBNENBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0IrSyxTQUFoQi9LLEdBQTRCLFlBQVc7QUFFbkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJaUIsQ0FESjtBQUFBLFFBQ08ySixHQURQOztBQUdBLFFBQUk1SyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1CLElBQVZuQixLQUFtQixJQUFuQkEsSUFBMkJBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXhELEVBQXNFO0FBRWxFdkMsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVV0QixRQUFWc0IsQ0FBbUIsY0FBbkJBOztBQUVBNEssTUFBQUEsR0FBRyxHQUFHdE4sQ0FBQyxDQUFDLFFBQUQsQ0FBREEsQ0FBWW9CLFFBQVpwQixDQUFxQjBDLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVb0IsU0FBL0I5RCxDQUFOc047O0FBRUEsV0FBSzNKLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsSUFBSWpCLENBQUMsQ0FBQzZLLFdBQUY3SyxFQUFqQixFQUFrQ2lCLENBQUMsSUFBSSxDQUF2QyxFQUEwQztBQUN0QzJKLFFBQUFBLEdBQUcsQ0FBQ3RDLE1BQUpzQyxDQUFXdE4sQ0FBQyxDQUFDLFFBQUQsQ0FBREEsQ0FBWWdMLE1BQVpoTCxDQUFtQjBDLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVZSxZQUFWZixDQUF1QnlKLElBQXZCekosQ0FBNEIsSUFBNUJBLEVBQWtDQSxDQUFsQ0EsRUFBcUNpQixDQUFyQ2pCLENBQW5CMUMsQ0FBWHNOO0FBQ0g7O0FBRUQ1SyxNQUFBQSxDQUFDLENBQUM0RCxLQUFGNUQsR0FBVTRLLEdBQUcsQ0FBQzdDLFFBQUo2QyxDQUFhNUssQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVLLFVBQXZCdUssQ0FBVjVLOztBQUVBQSxNQUFBQSxDQUFDLENBQUM0RCxLQUFGNUQsQ0FBUXNILElBQVJ0SCxDQUFhLElBQWJBLEVBQW1COEssS0FBbkI5SyxHQUEyQnRCLFFBQTNCc0IsQ0FBb0MsY0FBcENBLEVBQW9EdUgsSUFBcER2SCxDQUF5RCxhQUF6REEsRUFBd0UsT0FBeEVBO0FBRUg7QUFuQkwsR0FBQUo7O0FBdUJBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JtTCxRQUFoQm5MLEdBQTJCLFlBQVc7QUFFbEMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxHQUNJQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FDS29JLFFBRExwSSxDQUNlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXFDLEtBQVZyQyxHQUFrQixxQkFEakNBLEVBRUt0QixRQUZMc0IsQ0FFYyxhQUZkQSxDQURKQTtBQUtBQSxJQUFBQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVU4SCxNQUF6QjlIOztBQUVBQSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVXBCLElBQVZvQixDQUFlLFVBQVMySCxLQUFULEVBQWdCNUgsT0FBaEIsRUFBeUI7QUFDcEN6QyxNQUFBQSxDQUFDLENBQUN5QyxPQUFELENBQUR6QyxDQUNLaUssSUFETGpLLENBQ1Usa0JBRFZBLEVBQzhCcUssS0FEOUJySyxFQUVLMkksSUFGTDNJLENBRVUsaUJBRlZBLEVBRTZCQSxDQUFDLENBQUN5QyxPQUFELENBQUR6QyxDQUFXaUssSUFBWGpLLENBQWdCLE9BQWhCQSxLQUE0QixFQUZ6REE7QUFESixLQUFBMEM7O0FBTUFBLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVdEIsUUFBVnNCLENBQW1CLGNBQW5CQTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLEdBQWlCQSxDQUFDLENBQUNrRSxVQUFGbEUsS0FBaUIsQ0FBakJBLEdBQ2IxQyxDQUFDLENBQUMsNEJBQUQsQ0FBREEsQ0FBZ0N5SyxRQUFoQ3pLLENBQXlDMEMsQ0FBQyxDQUFDMEYsT0FBM0NwSSxDQURhMEMsR0FFYkEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVVnTCxPQUFWaEwsQ0FBa0IsNEJBQWxCQSxFQUFnRGxCLE1BQWhEa0IsRUFGSkE7QUFJQUEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLEdBQVVBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjbkIsSUFBZG1CLENBQ04sOENBRE1BLEVBQzBDbEIsTUFEMUNrQixFQUFWQTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQixHQUFkaUIsQ0FBa0IsU0FBbEJBLEVBQTZCLENBQTdCQTs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVksVUFBVlosS0FBeUIsSUFBekJBLElBQWlDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBDLFlBQVYxQyxLQUEyQixJQUFoRSxFQUFzRTtBQUNsRUEsTUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUFWeEMsR0FBMkIsQ0FBM0JBO0FBQ0g7O0FBRUQxQyxJQUFBQSxDQUFDLENBQUMsZ0JBQUQsRUFBbUIwQyxDQUFDLENBQUMwRixPQUFyQixDQUFEcEksQ0FBK0J3TSxHQUEvQnhNLENBQW1DLE9BQW5DQSxFQUE0Q29CLFFBQTVDcEIsQ0FBcUQsZUFBckRBOztBQUVBMEMsSUFBQUEsQ0FBQyxDQUFDaUwsYUFBRmpMOztBQUVBQSxJQUFBQSxDQUFDLENBQUNzSyxXQUFGdEs7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzJLLFNBQUYzSzs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDa0wsVUFBRmxMOztBQUdBQSxJQUFBQSxDQUFDLENBQUNtTCxlQUFGbkwsQ0FBa0IsT0FBT0EsQ0FBQyxDQUFDMEQsWUFBVCxLQUEwQixRQUExQixHQUFxQzFELENBQUMsQ0FBQzBELFlBQXZDLEdBQXNELENBQXhFMUQ7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVxQixTQUFWckIsS0FBd0IsSUFBNUIsRUFBa0M7QUFDOUJBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRdEIsUUFBUnNCLENBQWlCLFdBQWpCQTtBQUNIO0FBOUNMLEdBQUFKOztBQWtEQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCd0wsU0FBaEJ4TCxHQUE0QixZQUFXO0FBRW5DLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBY3FMLENBQWQ7QUFBQSxRQUFpQkMsQ0FBakI7QUFBQSxRQUFvQkMsQ0FBcEI7QUFBQSxRQUF1QkMsU0FBdkI7QUFBQSxRQUFrQ0MsV0FBbEM7QUFBQSxRQUErQ0MsY0FBL0M7QUFBQSxRQUE4REMsZ0JBQTlEOztBQUVBSCxJQUFBQSxTQUFTLEdBQUdyRixRQUFRLENBQUN5RixzQkFBVHpGLEVBQVpxRjtBQUNBRSxJQUFBQSxjQUFjLEdBQUcxTCxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVW9JLFFBQVZwSSxFQUFqQjBMOztBQUVBLFFBQUcxTCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1DLElBQVZuQyxHQUFpQixDQUFwQixFQUF1QjtBQUVuQjJMLE1BQUFBLGdCQUFnQixHQUFHM0wsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVzQyxZQUFWdEMsR0FBeUJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVbUMsSUFBdER3SjtBQUNBRixNQUFBQSxXQUFXLEdBQUduQyxJQUFJLENBQUNDLElBQUxELENBQ1ZvQyxjQUFjLENBQUM1RCxNQUFmNEQsR0FBd0JDLGdCQURkckMsQ0FBZG1DOztBQUlBLFdBQUlKLENBQUMsR0FBRyxDQUFSLEVBQVdBLENBQUMsR0FBR0ksV0FBZixFQUE0QkosQ0FBQyxFQUE3QixFQUFnQztBQUM1QixZQUFJaEosS0FBSyxHQUFHOEQsUUFBUSxDQUFDMEYsYUFBVDFGLENBQXVCLEtBQXZCQSxDQUFaOztBQUNBLGFBQUltRixDQUFDLEdBQUcsQ0FBUixFQUFXQSxDQUFDLEdBQUd0TCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1DLElBQXpCLEVBQStCbUosQ0FBQyxFQUFoQyxFQUFvQztBQUNoQyxjQUFJUSxHQUFHLEdBQUczRixRQUFRLENBQUMwRixhQUFUMUYsQ0FBdUIsS0FBdkJBLENBQVY7O0FBQ0EsZUFBSW9GLENBQUMsR0FBRyxDQUFSLEVBQVdBLENBQUMsR0FBR3ZMLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVc0MsWUFBekIsRUFBdUNpSixDQUFDLEVBQXhDLEVBQTRDO0FBQ3hDLGdCQUFJeEIsTUFBTSxHQUFJc0IsQ0FBQyxHQUFHTSxnQkFBSk4sSUFBeUJDLENBQUMsR0FBR3RMLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVc0MsWUFBZGdKLEdBQThCQyxDQUF2REYsQ0FBZDs7QUFDQSxnQkFBSUssY0FBYyxDQUFDSyxHQUFmTCxDQUFtQjNCLE1BQW5CMkIsQ0FBSixFQUFnQztBQUM1QkksY0FBQUEsR0FBRyxDQUFDRSxXQUFKRixDQUFnQkosY0FBYyxDQUFDSyxHQUFmTCxDQUFtQjNCLE1BQW5CMkIsQ0FBaEJJO0FBQ0g7QUFDSjs7QUFDRHpKLFVBQUFBLEtBQUssQ0FBQzJKLFdBQU4zSixDQUFrQnlKLEdBQWxCeko7QUFDSDs7QUFDRG1KLFFBQUFBLFNBQVMsQ0FBQ1EsV0FBVlIsQ0FBc0JuSixLQUF0Qm1KO0FBQ0g7O0FBRUR4TCxNQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVWlNLEtBQVZqTSxHQUFrQnNJLE1BQWxCdEksQ0FBeUJ3TCxTQUF6QnhMOztBQUNBQSxNQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVW9JLFFBQVZwSSxHQUFxQm9JLFFBQXJCcEksR0FBZ0NvSSxRQUFoQ3BJLEdBQ0tqQixHQURMaUIsQ0FDUztBQUNELGlCQUFTLE1BQU1BLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVc0MsWUFBaEIsR0FBZ0MsR0FEeEM7QUFFRCxtQkFBVztBQUZWLE9BRFR0QztBQU1IO0FBcENMLEdBQUFKOztBQXdDQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCc00sZUFBaEJ0TSxHQUFrQyxVQUFTdU0sT0FBVCxFQUFrQkMsV0FBbEIsRUFBK0I7QUFFN0QsUUFBSXBNLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXFNLFVBREo7QUFBQSxRQUNnQkMsZ0JBRGhCO0FBQUEsUUFDa0NDLGNBRGxDO0FBQUEsUUFDa0RDLGlCQUFpQixHQUFHLEtBRHRFOztBQUVBLFFBQUlDLFdBQVcsR0FBR3pNLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVbEMsS0FBVmtDLEVBQWxCOztBQUNBLFFBQUkrRixXQUFXLEdBQUdsRyxNQUFNLENBQUM2TSxVQUFQN00sSUFBcUJ2QyxDQUFDLENBQUN1QyxNQUFELENBQUR2QyxDQUFVUSxLQUFWUixFQUF2Qzs7QUFFQSxRQUFJMEMsQ0FBQyxDQUFDaUMsU0FBRmpDLEtBQWdCLFFBQXBCLEVBQThCO0FBQzFCdU0sTUFBQUEsY0FBYyxHQUFHeEcsV0FBakJ3RztBQURKLEtBQUEsTUFFTyxJQUFJdk0sQ0FBQyxDQUFDaUMsU0FBRmpDLEtBQWdCLFFBQXBCLEVBQThCO0FBQ2pDdU0sTUFBQUEsY0FBYyxHQUFHRSxXQUFqQkY7QUFERyxLQUFBLE1BRUEsSUFBSXZNLENBQUMsQ0FBQ2lDLFNBQUZqQyxLQUFnQixLQUFwQixFQUEyQjtBQUM5QnVNLE1BQUFBLGNBQWMsR0FBR2pELElBQUksQ0FBQ3FELEdBQUxyRCxDQUFTdkQsV0FBVHVELEVBQXNCbUQsV0FBdEJuRCxDQUFqQmlEO0FBQ0g7O0FBRUQsUUFBS3ZNLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVa0MsVUFBVmxDLElBQ0RBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVa0MsVUFBVmxDLENBQXFCOEgsTUFEcEI5SCxJQUVEQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWtDLFVBQVZsQyxLQUF5QixJQUY3QixFQUVtQztBQUUvQnNNLE1BQUFBLGdCQUFnQixHQUFHLElBQW5CQTs7QUFFQSxXQUFLRCxVQUFMLElBQW1Cck0sQ0FBQyxDQUFDZ0YsV0FBckIsRUFBa0M7QUFDOUIsWUFBSWhGLENBQUMsQ0FBQ2dGLFdBQUZoRixDQUFjNE0sY0FBZDVNLENBQTZCcU0sVUFBN0JyTSxDQUFKLEVBQThDO0FBQzFDLGNBQUlBLENBQUMsQ0FBQ2tHLGdCQUFGbEcsQ0FBbUI2QixXQUFuQjdCLEtBQW1DLEtBQXZDLEVBQThDO0FBQzFDLGdCQUFJdU0sY0FBYyxHQUFHdk0sQ0FBQyxDQUFDZ0YsV0FBRmhGLENBQWNxTSxVQUFkck0sQ0FBckIsRUFBZ0Q7QUFDNUNzTSxjQUFBQSxnQkFBZ0IsR0FBR3RNLENBQUMsQ0FBQ2dGLFdBQUZoRixDQUFjcU0sVUFBZHJNLENBQW5Cc007QUFDSDtBQUhMLFdBQUEsTUFJTztBQUNILGdCQUFJQyxjQUFjLEdBQUd2TSxDQUFDLENBQUNnRixXQUFGaEYsQ0FBY3FNLFVBQWRyTSxDQUFyQixFQUFnRDtBQUM1Q3NNLGNBQUFBLGdCQUFnQixHQUFHdE0sQ0FBQyxDQUFDZ0YsV0FBRmhGLENBQWNxTSxVQUFkck0sQ0FBbkJzTTtBQUNIO0FBQ0o7QUFDSjtBQUNKOztBQUVELFVBQUlBLGdCQUFnQixLQUFLLElBQXpCLEVBQStCO0FBQzNCLFlBQUl0TSxDQUFDLENBQUM2RSxnQkFBRjdFLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCLGNBQUlzTSxnQkFBZ0IsS0FBS3RNLENBQUMsQ0FBQzZFLGdCQUF2QnlILElBQTJDRixXQUEvQyxFQUE0RDtBQUN4RHBNLFlBQUFBLENBQUMsQ0FBQzZFLGdCQUFGN0UsR0FDSXNNLGdCQURKdE07O0FBRUEsZ0JBQUlBLENBQUMsQ0FBQ2lGLGtCQUFGakYsQ0FBcUJzTSxnQkFBckJ0TSxNQUEyQyxTQUEvQyxFQUEwRDtBQUN0REEsY0FBQUEsQ0FBQyxDQUFDNk0sT0FBRjdNLENBQVVzTSxnQkFBVnRNO0FBREosYUFBQSxNQUVPO0FBQ0hBLGNBQUFBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxHQUFZMUMsQ0FBQyxDQUFDa0IsTUFBRmxCLENBQVMsRUFBVEEsRUFBYTBDLENBQUMsQ0FBQ2tHLGdCQUFmNUksRUFDUjBDLENBQUMsQ0FBQ2lGLGtCQUFGakYsQ0FDSXNNLGdCQURKdE0sQ0FEUTFDLENBQVowQzs7QUFHQSxrQkFBSW1NLE9BQU8sS0FBSyxJQUFoQixFQUFzQjtBQUNsQm5NLGdCQUFBQSxDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMkIsWUFBM0IzQjtBQUNIOztBQUNEQSxjQUFBQSxDQUFDLENBQUM4TSxPQUFGOU0sQ0FBVW1NLE9BQVZuTTtBQUNIOztBQUNEd00sWUFBQUEsaUJBQWlCLEdBQUdGLGdCQUFwQkU7QUFDSDtBQWhCTCxTQUFBLE1BaUJPO0FBQ0h4TSxVQUFBQSxDQUFDLENBQUM2RSxnQkFBRjdFLEdBQXFCc00sZ0JBQXJCdE07O0FBQ0EsY0FBSUEsQ0FBQyxDQUFDaUYsa0JBQUZqRixDQUFxQnNNLGdCQUFyQnRNLE1BQTJDLFNBQS9DLEVBQTBEO0FBQ3REQSxZQUFBQSxDQUFDLENBQUM2TSxPQUFGN00sQ0FBVXNNLGdCQUFWdE07QUFESixXQUFBLE1BRU87QUFDSEEsWUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLEdBQVkxQyxDQUFDLENBQUNrQixNQUFGbEIsQ0FBUyxFQUFUQSxFQUFhMEMsQ0FBQyxDQUFDa0csZ0JBQWY1SSxFQUNSMEMsQ0FBQyxDQUFDaUYsa0JBQUZqRixDQUNJc00sZ0JBREp0TSxDQURRMUMsQ0FBWjBDOztBQUdBLGdCQUFJbU0sT0FBTyxLQUFLLElBQWhCLEVBQXNCO0FBQ2xCbk0sY0FBQUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTJCLFlBQTNCM0I7QUFDSDs7QUFDREEsWUFBQUEsQ0FBQyxDQUFDOE0sT0FBRjlNLENBQVVtTSxPQUFWbk07QUFDSDs7QUFDRHdNLFVBQUFBLGlCQUFpQixHQUFHRixnQkFBcEJFO0FBQ0g7QUFoQ0wsT0FBQSxNQWlDTztBQUNILFlBQUl4TSxDQUFDLENBQUM2RSxnQkFBRjdFLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCQSxVQUFBQSxDQUFDLENBQUM2RSxnQkFBRjdFLEdBQXFCLElBQXJCQTtBQUNBQSxVQUFBQSxDQUFDLENBQUN2QyxPQUFGdUMsR0FBWUEsQ0FBQyxDQUFDa0csZ0JBQWRsRzs7QUFDQSxjQUFJbU0sT0FBTyxLQUFLLElBQWhCLEVBQXNCO0FBQ2xCbk0sWUFBQUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTJCLFlBQTNCM0I7QUFDSDs7QUFDREEsVUFBQUEsQ0FBQyxDQUFDOE0sT0FBRjlNLENBQVVtTSxPQUFWbk07O0FBQ0F3TSxVQUFBQSxpQkFBaUIsR0FBR0YsZ0JBQXBCRTtBQUNIO0FBNUQwQixPQUFBLENBK0QvQjs7O0FBQ0EsVUFBSSxDQUFDTCxPQUFELElBQVlLLGlCQUFpQixLQUFLLEtBQXRDLEVBQThDO0FBQzFDeE0sUUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVUrTSxPQUFWL00sQ0FBa0IsWUFBbEJBLEVBQWdDLENBQUNBLENBQUQsRUFBSXdNLGlCQUFKLENBQWhDeE07QUFDSDtBQUNKO0FBcEZMLEdBQUFKOztBQXdGQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCOEcsV0FBaEI5RyxHQUE4QixVQUFTb04sS0FBVCxFQUFnQkMsV0FBaEIsRUFBNkI7QUFFdkQsUUFBSWpOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWtOLE9BQU8sR0FBRzVQLENBQUMsQ0FBQzBQLEtBQUssQ0FBQ0csYUFBUCxDQURmO0FBQUEsUUFFSUMsV0FGSjtBQUFBLFFBRWlCN0ksV0FGakI7QUFBQSxRQUU4QjhJLFlBRjlCLENBRnVELENBTXZEOzs7QUFDQSxRQUFHSCxPQUFPLENBQUNJLEVBQVJKLENBQVcsR0FBWEEsQ0FBSCxFQUFvQjtBQUNoQkYsTUFBQUEsS0FBSyxDQUFDTyxjQUFOUDtBQVJtRCxLQUFBLENBV3ZEOzs7QUFDQSxRQUFHLENBQUNFLE9BQU8sQ0FBQ0ksRUFBUkosQ0FBVyxJQUFYQSxDQUFKLEVBQXNCO0FBQ2xCQSxNQUFBQSxPQUFPLEdBQUdBLE9BQU8sQ0FBQzlOLE9BQVI4TixDQUFnQixJQUFoQkEsQ0FBVkE7QUFDSDs7QUFFREcsSUFBQUEsWUFBWSxHQUFJck4sQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBekJ4QyxLQUE0QyxDQUE1RHFOO0FBQ0FELElBQUFBLFdBQVcsR0FBR0MsWUFBWSxHQUFHLENBQUgsR0FBTyxDQUFDck4sQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQzBELFlBQWxCLElBQWtDMUQsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUE3RTRLOztBQUVBLFlBQVFKLEtBQUssQ0FBQy9HLElBQU4rRyxDQUFXUSxPQUFuQjtBQUVJLFdBQUssVUFBTDtBQUNJakosUUFBQUEsV0FBVyxHQUFHNkksV0FBVyxLQUFLLENBQWhCQSxHQUFvQnBOLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBOUI0SyxHQUErQ3BOLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBVnZDLEdBQXlCb04sV0FBdEY3STs7QUFDQSxZQUFJdkUsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBN0IsRUFBMkM7QUFDdkN2QyxVQUFBQSxDQUFDLENBQUNpSyxZQUFGakssQ0FBZUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCdUUsV0FBaEN2RSxFQUE2QyxLQUE3Q0EsRUFBb0RpTixXQUFwRGpOO0FBQ0g7O0FBQ0Q7O0FBRUosV0FBSyxNQUFMO0FBQ0l1RSxRQUFBQSxXQUFXLEdBQUc2SSxXQUFXLEtBQUssQ0FBaEJBLEdBQW9CcE4sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUE5QjRLLEdBQStDQSxXQUE3RDdJOztBQUNBLFlBQUl2RSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUE3QixFQUEyQztBQUN2Q3ZDLFVBQUFBLENBQUMsQ0FBQ2lLLFlBQUZqSyxDQUFlQSxDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUJ1RSxXQUFoQ3ZFLEVBQTZDLEtBQTdDQSxFQUFvRGlOLFdBQXBEak47QUFDSDs7QUFDRDs7QUFFSixXQUFLLE9BQUw7QUFDSSxZQUFJMkgsS0FBSyxHQUFHcUYsS0FBSyxDQUFDL0csSUFBTitHLENBQVdyRixLQUFYcUYsS0FBcUIsQ0FBckJBLEdBQXlCLENBQXpCQSxHQUNSQSxLQUFLLENBQUMvRyxJQUFOK0csQ0FBV3JGLEtBQVhxRixJQUFvQkUsT0FBTyxDQUFDdkYsS0FBUnVGLEtBQWtCbE4sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQURwRDs7QUFHQXhDLFFBQUFBLENBQUMsQ0FBQ2lLLFlBQUZqSyxDQUFlQSxDQUFDLENBQUN5TixjQUFGek4sQ0FBaUIySCxLQUFqQjNILENBQWZBLEVBQXdDLEtBQXhDQSxFQUErQ2lOLFdBQS9Dak47O0FBQ0FrTixRQUFBQSxPQUFPLENBQUM5RSxRQUFSOEUsR0FBbUJILE9BQW5CRyxDQUEyQixPQUEzQkE7QUFDQTs7QUFFSjtBQUNJO0FBekJSO0FBbkJKLEdBQUF0Tjs7QUFpREFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjZOLGNBQWhCN04sR0FBaUMsVUFBUytILEtBQVQsRUFBZ0I7QUFFN0MsUUFBSTNILENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSTBOLFVBREo7QUFBQSxRQUNnQkMsYUFEaEI7O0FBR0FELElBQUFBLFVBQVUsR0FBRzFOLENBQUMsQ0FBQzROLG1CQUFGNU4sRUFBYjBOO0FBQ0FDLElBQUFBLGFBQWEsR0FBRyxDQUFoQkE7O0FBQ0EsUUFBSWhHLEtBQUssR0FBRytGLFVBQVUsQ0FBQ0EsVUFBVSxDQUFDNUYsTUFBWDRGLEdBQW9CLENBQXJCLENBQXRCLEVBQStDO0FBQzNDL0YsTUFBQUEsS0FBSyxHQUFHK0YsVUFBVSxDQUFDQSxVQUFVLENBQUM1RixNQUFYNEYsR0FBb0IsQ0FBckIsQ0FBbEIvRjtBQURKLEtBQUEsTUFFTztBQUNILFdBQUssSUFBSWtHLENBQVQsSUFBY0gsVUFBZCxFQUEwQjtBQUN0QixZQUFJL0YsS0FBSyxHQUFHK0YsVUFBVSxDQUFDRyxDQUFELENBQXRCLEVBQTJCO0FBQ3ZCbEcsVUFBQUEsS0FBSyxHQUFHZ0csYUFBUmhHO0FBQ0E7QUFDSDs7QUFDRGdHLFFBQUFBLGFBQWEsR0FBR0QsVUFBVSxDQUFDRyxDQUFELENBQTFCRjtBQUNIO0FBQ0o7O0FBRUQsV0FBT2hHLEtBQVA7QUFuQkosR0FBQS9IOztBQXNCQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCa08sYUFBaEJsTyxHQUFnQyxZQUFXO0FBRXZDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVbUIsSUFBVm5CLElBQWtCQSxDQUFDLENBQUM0RCxLQUFGNUQsS0FBWSxJQUFsQyxFQUF3QztBQUVwQzFDLE1BQUFBLENBQUMsQ0FBQyxJQUFELEVBQU8wQyxDQUFDLENBQUM0RCxLQUFULENBQUR0RyxDQUNLeVEsR0FETHpRLENBQ1MsYUFEVEEsRUFDd0IwQyxDQUFDLENBQUMwRyxXQUQxQnBKLEVBRUt5USxHQUZMelEsQ0FFUyxrQkFGVEEsRUFFNkJBLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDZ08sU0FBVjFRLEVBQXFCMEMsQ0FBckIxQyxFQUF3QixJQUF4QkEsQ0FGN0JBLEVBR0t5USxHQUhMelEsQ0FHUyxrQkFIVEEsRUFHNkJBLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDZ08sU0FBVjFRLEVBQXFCMEMsQ0FBckIxQyxFQUF3QixLQUF4QkEsQ0FIN0JBO0FBS0g7O0FBRUQwQyxJQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStOLEdBQVYvTixDQUFjLHdCQUFkQTs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVU0sTUFBVk4sS0FBcUIsSUFBckJBLElBQTZCQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUExRCxFQUF3RTtBQUNwRXZDLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUZqRSxJQUFnQkEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQWErTixHQUFiL04sQ0FBaUIsYUFBakJBLEVBQWdDQSxDQUFDLENBQUMwRyxXQUFsQzFHLENBQWhCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUNnRSxVQUFGaEUsSUFBZ0JBLENBQUMsQ0FBQ2dFLFVBQUZoRSxDQUFhK04sR0FBYi9OLENBQWlCLGFBQWpCQSxFQUFnQ0EsQ0FBQyxDQUFDMEcsV0FBbEMxRyxDQUFoQkE7QUFDSDs7QUFFREEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVErTixHQUFSL04sQ0FBWSxrQ0FBWkEsRUFBZ0RBLENBQUMsQ0FBQzhHLFlBQWxEOUc7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRK04sR0FBUi9OLENBQVksaUNBQVpBLEVBQStDQSxDQUFDLENBQUM4RyxZQUFqRDlHOztBQUNBQSxJQUFBQSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUStOLEdBQVIvTixDQUFZLDhCQUFaQSxFQUE0Q0EsQ0FBQyxDQUFDOEcsWUFBOUM5Rzs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVErTixHQUFSL04sQ0FBWSxvQ0FBWkEsRUFBa0RBLENBQUMsQ0FBQzhHLFlBQXBEOUc7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRK04sR0FBUi9OLENBQVksYUFBWkEsRUFBMkJBLENBQUMsQ0FBQzJHLFlBQTdCM0c7O0FBRUExQyxJQUFBQSxDQUFDLENBQUM2SSxRQUFELENBQUQ3SSxDQUFZeVEsR0FBWnpRLENBQWdCMEMsQ0FBQyxDQUFDOEYsZ0JBQWxCeEksRUFBb0MwQyxDQUFDLENBQUNpTyxVQUF0QzNROztBQUVBMEMsSUFBQUEsQ0FBQyxDQUFDa08sa0JBQUZsTzs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUUsYUFBVkYsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbENBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRK04sR0FBUi9OLENBQVksZUFBWkEsRUFBNkJBLENBQUMsQ0FBQ2dILFVBQS9CaEg7QUFDSDs7QUFFRCxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXlCLGFBQVZ6QixLQUE0QixJQUFoQyxFQUFzQztBQUNsQzFDLE1BQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ29FLFdBQUgsQ0FBRDlHLENBQWlCOEssUUFBakI5SyxHQUE0QnlRLEdBQTVCelEsQ0FBZ0MsYUFBaENBLEVBQStDMEMsQ0FBQyxDQUFDNEcsYUFBakR0SjtBQUNIOztBQUVEQSxJQUFBQSxDQUFDLENBQUN1QyxNQUFELENBQUR2QyxDQUFVeVEsR0FBVnpRLENBQWMsbUNBQW1DMEMsQ0FBQyxDQUFDRixXQUFuRHhDLEVBQWdFMEMsQ0FBQyxDQUFDbU8saUJBQWxFN1E7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFEdkMsQ0FBVXlRLEdBQVZ6USxDQUFjLHdCQUF3QjBDLENBQUMsQ0FBQ0YsV0FBeEN4QyxFQUFxRDBDLENBQUMsQ0FBQ29PLE1BQXZEOVE7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDLG1CQUFELEVBQXNCMEMsQ0FBQyxDQUFDb0UsV0FBeEIsQ0FBRDlHLENBQXNDeVEsR0FBdEN6USxDQUEwQyxXQUExQ0EsRUFBdUQwQyxDQUFDLENBQUN1TixjQUF6RGpRO0FBRUFBLElBQUFBLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRHZDLENBQVV5USxHQUFWelEsQ0FBYyxzQkFBc0IwQyxDQUFDLENBQUNGLFdBQXRDeEMsRUFBbUQwQyxDQUFDLENBQUM2RyxXQUFyRHZKO0FBQ0FBLElBQUFBLENBQUMsQ0FBQzZJLFFBQUQsQ0FBRDdJLENBQVl5USxHQUFaelEsQ0FBZ0IsdUJBQXVCMEMsQ0FBQyxDQUFDRixXQUF6Q3hDLEVBQXNEMEMsQ0FBQyxDQUFDNkcsV0FBeER2SjtBQTlDSixHQUFBc0M7O0FBa0RBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JzTyxrQkFBaEJ0TyxHQUFxQyxZQUFXO0FBRTVDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUStOLEdBQVIvTixDQUFZLGtCQUFaQSxFQUFnQzFDLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDZ08sU0FBVjFRLEVBQXFCMEMsQ0FBckIxQyxFQUF3QixJQUF4QkEsQ0FBaEMwQzs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVErTixHQUFSL04sQ0FBWSxrQkFBWkEsRUFBZ0MxQyxDQUFDLENBQUNpSixLQUFGakosQ0FBUTBDLENBQUMsQ0FBQ2dPLFNBQVYxUSxFQUFxQjBDLENBQXJCMUMsRUFBd0IsS0FBeEJBLENBQWhDMEM7QUFMSixHQUFBSjs7QUFTQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCeU8sV0FBaEJ6TyxHQUE4QixZQUFXO0FBRXJDLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBYzBMLGNBQWQ7O0FBRUEsUUFBRzFMLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVbUMsSUFBVm5DLEdBQWlCLENBQXBCLEVBQXVCO0FBQ25CMEwsTUFBQUEsY0FBYyxHQUFHMUwsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVVvSSxRQUFWcEksR0FBcUJvSSxRQUFyQnBJLEVBQWpCMEw7QUFDQUEsTUFBQUEsY0FBYyxDQUFDbEIsVUFBZmtCLENBQTBCLE9BQTFCQTs7QUFDQTFMLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVaU0sS0FBVmpNLEdBQWtCc0ksTUFBbEJ0SSxDQUF5QjBMLGNBQXpCMUw7QUFDSDtBQVJMLEdBQUFKOztBQVlBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0IrRyxZQUFoQi9HLEdBQStCLFVBQVNvTixLQUFULEVBQWdCO0FBRTNDLFFBQUloTixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN5RixXQUFGekYsS0FBa0IsS0FBdEIsRUFBNkI7QUFDekJnTixNQUFBQSxLQUFLLENBQUNzQix3QkFBTnRCO0FBQ0FBLE1BQUFBLEtBQUssQ0FBQ3VCLGVBQU52QjtBQUNBQSxNQUFBQSxLQUFLLENBQUNPLGNBQU5QO0FBQ0g7QUFSTCxHQUFBcE47O0FBWUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjRPLE9BQWhCNU8sR0FBMEIsVUFBU2tOLE9BQVQsRUFBa0I7QUFFeEMsUUFBSTlNLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUN3RyxhQUFGeEc7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxHQUFnQixFQUFoQkE7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzhOLGFBQUY5Tjs7QUFFQTFDLElBQUFBLENBQUMsQ0FBQyxlQUFELEVBQWtCMEMsQ0FBQyxDQUFDMEYsT0FBcEIsQ0FBRHBJLENBQThCK0ssTUFBOUIvSzs7QUFFQSxRQUFJMEMsQ0FBQyxDQUFDNEQsS0FBTixFQUFhO0FBQ1Q1RCxNQUFBQSxDQUFDLENBQUM0RCxLQUFGNUQsQ0FBUXlPLE1BQVJ6TztBQUNIOztBQUdELFFBQUtBLENBQUMsQ0FBQ2lFLFVBQUZqRSxJQUFnQkEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQWE4SCxNQUFsQyxFQUEyQztBQUV2QzlILE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUZqRSxDQUNLdUssV0FETHZLLENBQ2lCLHlDQURqQkEsRUFFS3dLLFVBRkx4SyxDQUVnQixvQ0FGaEJBLEVBR0tqQixHQUhMaUIsQ0FHUyxTQUhUQSxFQUdtQixFQUhuQkE7O0FBS0EsVUFBS0EsQ0FBQyxDQUFDaUgsUUFBRmpILENBQVd5SyxJQUFYekssQ0FBaUJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVUSxTQUEzQlIsQ0FBTCxFQUE2QztBQUN6Q0EsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQWF5TyxNQUFiek87QUFDSDtBQUNKOztBQUVELFFBQUtBLENBQUMsQ0FBQ2dFLFVBQUZoRSxJQUFnQkEsQ0FBQyxDQUFDZ0UsVUFBRmhFLENBQWE4SCxNQUFsQyxFQUEyQztBQUV2QzlILE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUZoRSxDQUNLdUssV0FETHZLLENBQ2lCLHlDQURqQkEsRUFFS3dLLFVBRkx4SyxDQUVnQixvQ0FGaEJBLEVBR0tqQixHQUhMaUIsQ0FHUyxTQUhUQSxFQUdtQixFQUhuQkE7O0FBS0EsVUFBS0EsQ0FBQyxDQUFDaUgsUUFBRmpILENBQVd5SyxJQUFYekssQ0FBaUJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVUyxTQUEzQlQsQ0FBTCxFQUE2QztBQUN6Q0EsUUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRmhFLENBQWF5TyxNQUFiek87QUFDSDtBQUVKOztBQUdELFFBQUlBLENBQUMsQ0FBQ3FFLE9BQU4sRUFBZTtBQUVYckUsTUFBQUEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQ0t1SyxXQURMdkssQ0FDaUIsbUVBRGpCQSxFQUVLd0ssVUFGTHhLLENBRWdCLGFBRmhCQSxFQUdLd0ssVUFITHhLLENBR2dCLGtCQUhoQkEsRUFJS3BCLElBSkxvQixDQUlVLFlBQVU7QUFDWjFDLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQURBLENBQVFpSyxJQUFSakssQ0FBYSxPQUFiQSxFQUFzQkEsQ0FBQyxDQUFDLElBQUQsQ0FBREEsQ0FBUTJJLElBQVIzSSxDQUFhLGlCQUFiQSxDQUF0QkE7QUFMUixPQUFBMEM7O0FBUUFBLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjb0ksUUFBZHBJLENBQXVCLEtBQUt2QyxPQUFMLENBQWE0RSxLQUFwQ3JDLEVBQTJDcUksTUFBM0NySTs7QUFFQUEsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNxSSxNQUFkckk7O0FBRUFBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRcUksTUFBUnJJOztBQUVBQSxNQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVXNJLE1BQVZ0SSxDQUFpQkEsQ0FBQyxDQUFDcUUsT0FBbkJyRTtBQUNIOztBQUVEQSxJQUFBQSxDQUFDLENBQUNxTyxXQUFGck87O0FBRUFBLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVdUssV0FBVnZLLENBQXNCLGNBQXRCQTs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVV1SyxXQUFWdkssQ0FBc0IsbUJBQXRCQTs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVV1SyxXQUFWdkssQ0FBc0IsY0FBdEJBOztBQUVBQSxJQUFBQSxDQUFDLENBQUM0RSxTQUFGNUUsR0FBYyxJQUFkQTs7QUFFQSxRQUFHLENBQUM4TSxPQUFKLEVBQWE7QUFDVDlNLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVK00sT0FBVi9NLENBQWtCLFNBQWxCQSxFQUE2QixDQUFDQSxDQUFELENBQTdCQTtBQUNIO0FBeEVMLEdBQUFKOztBQTRFQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCZ0ssaUJBQWhCaEssR0FBb0MsVUFBU3lDLEtBQVQsRUFBZ0I7QUFFaEQsUUFBSXJDLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWtLLFVBQVUsR0FBRyxFQURqQjs7QUFHQUEsSUFBQUEsVUFBVSxDQUFDbEssQ0FBQyxDQUFDNkYsY0FBSCxDQUFWcUUsR0FBK0IsRUFBL0JBOztBQUVBLFFBQUlsSyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdCLElBQVZ4QixLQUFtQixLQUF2QixFQUE4QjtBQUMxQkEsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQixHQUFkaUIsQ0FBa0JrSyxVQUFsQmxLO0FBREosS0FBQSxNQUVPO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVaUksRUFBVmpJLENBQWFxQyxLQUFickMsRUFBb0JqQixHQUFwQmlCLENBQXdCa0ssVUFBeEJsSztBQUNIO0FBWEwsR0FBQUo7O0FBZUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjhPLFNBQWhCOU8sR0FBNEIsVUFBUytPLFVBQVQsRUFBcUI3RixRQUFyQixFQUErQjtBQUV2RCxRQUFJOUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDa0YsY0FBRmxGLEtBQXFCLEtBQXpCLEVBQWdDO0FBRTVCQSxNQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVWlJLEVBQVZqSSxDQUFhMk8sVUFBYjNPLEVBQXlCakIsR0FBekJpQixDQUE2QjtBQUN6Qm1ELFFBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVbUQ7QUFETyxPQUE3Qm5EOztBQUlBQSxNQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVWlJLEVBQVZqSSxDQUFhMk8sVUFBYjNPLEVBQXlCMkksT0FBekIzSSxDQUFpQztBQUM3QjRPLFFBQUFBLE9BQU8sRUFBRTtBQURvQixPQUFqQzVPLEVBRUdBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVcEMsS0FGYm9DLEVBRW9CQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXNCLE1BRjlCdEIsRUFFc0M4SSxRQUZ0QzlJO0FBTkosS0FBQSxNQVVPO0FBRUhBLE1BQUFBLENBQUMsQ0FBQzBKLGVBQUYxSixDQUFrQjJPLFVBQWxCM087O0FBRUFBLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVaUksRUFBVmpJLENBQWEyTyxVQUFiM08sRUFBeUJqQixHQUF6QmlCLENBQTZCO0FBQ3pCNE8sUUFBQUEsT0FBTyxFQUFFLENBRGdCO0FBRXpCekwsUUFBQUEsTUFBTSxFQUFFbkQsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVtRDtBQUZPLE9BQTdCbkQ7O0FBS0EsVUFBSThJLFFBQUosRUFBYztBQUNWYSxRQUFBQSxVQUFVLENBQUMsWUFBVztBQUVsQjNKLFVBQUFBLENBQUMsQ0FBQzRKLGlCQUFGNUosQ0FBb0IyTyxVQUFwQjNPOztBQUVBOEksVUFBQUEsUUFBUSxDQUFDVyxJQUFUWDtBQUpNLFNBQUEsRUFLUDlJLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVcEMsS0FMSCxDQUFWK0w7QUFNSDtBQUVKO0FBaENMLEdBQUEvSjs7QUFvQ0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmlQLFlBQWhCalAsR0FBK0IsVUFBUytPLFVBQVQsRUFBcUI7QUFFaEQsUUFBSTNPLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ2tGLGNBQUZsRixLQUFxQixLQUF6QixFQUFnQztBQUU1QkEsTUFBQUEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVVpSSxFQUFWakksQ0FBYTJPLFVBQWIzTyxFQUF5QjJJLE9BQXpCM0ksQ0FBaUM7QUFDN0I0TyxRQUFBQSxPQUFPLEVBQUUsQ0FEb0I7QUFFN0J6TCxRQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1ELE1BQVZuRCxHQUFtQjtBQUZFLE9BQWpDQSxFQUdHQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXBDLEtBSGJvQyxFQUdvQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVzQixNQUg5QnRCO0FBRkosS0FBQSxNQU9PO0FBRUhBLE1BQUFBLENBQUMsQ0FBQzBKLGVBQUYxSixDQUFrQjJPLFVBQWxCM087O0FBRUFBLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVaUksRUFBVmpJLENBQWEyTyxVQUFiM08sRUFBeUJqQixHQUF6QmlCLENBQTZCO0FBQ3pCNE8sUUFBQUEsT0FBTyxFQUFFLENBRGdCO0FBRXpCekwsUUFBQUEsTUFBTSxFQUFFbkQsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVtRCxNQUFWbkQsR0FBbUI7QUFGRixPQUE3QkE7QUFLSDtBQXBCTCxHQUFBSjs7QUF3QkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmtQLFlBQWhCbFAsR0FBK0JBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQm1QLFdBQWhCblAsR0FBOEIsVUFBU29QLE1BQVQsRUFBaUI7QUFFMUUsUUFBSWhQLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlnUCxNQUFNLEtBQUssSUFBZixFQUFxQjtBQUVqQmhQLE1BQUFBLENBQUMsQ0FBQzJGLFlBQUYzRixHQUFpQkEsQ0FBQyxDQUFDcUUsT0FBbkJyRTs7QUFFQUEsTUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjdIOztBQUVBQSxNQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixLQUFLdkMsT0FBTCxDQUFhNEUsS0FBcENyQyxFQUEyQ3FJLE1BQTNDckk7O0FBRUFBLE1BQUFBLENBQUMsQ0FBQzJGLFlBQUYzRixDQUFlZ1AsTUFBZmhQLENBQXNCZ1AsTUFBdEJoUCxFQUE4QitILFFBQTlCL0gsQ0FBdUNBLENBQUMsQ0FBQ29FLFdBQXpDcEU7O0FBRUFBLE1BQUFBLENBQUMsQ0FBQ3VJLE1BQUZ2STtBQUVIO0FBaEJMLEdBQUFKOztBQW9CQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCcVAsWUFBaEJyUCxHQUErQixZQUFXO0FBRXRDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FDSytOLEdBREwvTixDQUNTLHdCQURUQSxFQUVLZCxFQUZMYyxDQUVRLHdCQUZSQSxFQUdRLHFCQUhSQSxFQUcrQixVQUFTZ04sS0FBVCxFQUFnQjtBQUUzQ0EsTUFBQUEsS0FBSyxDQUFDc0Isd0JBQU50QjtBQUNBLFVBQUlrQyxHQUFHLEdBQUc1UixDQUFDLENBQUMsSUFBRCxDQUFYO0FBRUFxTSxNQUFBQSxVQUFVLENBQUMsWUFBVztBQUVsQixZQUFJM0osQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVUrQixZQUFkLEVBQTZCO0FBQ3pCL0IsVUFBQUEsQ0FBQyxDQUFDbUYsUUFBRm5GLEdBQWFrUCxHQUFHLENBQUM1QixFQUFKNEIsQ0FBTyxRQUFQQSxDQUFibFA7O0FBQ0FBLFVBQUFBLENBQUMsQ0FBQ3NHLFFBQUZ0RztBQUNIO0FBTEssT0FBQSxFQU9QLENBUE8sQ0FBVjJKO0FBUkosS0FBQTNKO0FBSkosR0FBQUo7O0FBd0JBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J1UCxVQUFoQnZQLEdBQTZCQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J3UCxpQkFBaEJ4UCxHQUFvQyxZQUFXO0FBRXhFLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUNBLFdBQU9BLENBQUMsQ0FBQzBELFlBQVQ7QUFISixHQUFBOUQ7O0FBT0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmlMLFdBQWhCakwsR0FBOEIsWUFBVztBQUVyQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJcVAsVUFBVSxHQUFHLENBQWpCO0FBQ0EsUUFBSUMsT0FBTyxHQUFHLENBQWQ7QUFDQSxRQUFJQyxRQUFRLEdBQUcsQ0FBZjs7QUFFQSxRQUFJdlAsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVUwQixRQUFWMUIsS0FBdUIsSUFBM0IsRUFBaUM7QUFDN0IsYUFBT3FQLFVBQVUsR0FBR3JQLENBQUMsQ0FBQ2tFLFVBQXRCLEVBQWtDO0FBQzlCLFVBQUVxTCxRQUFGO0FBQ0FGLFFBQUFBLFVBQVUsR0FBR0MsT0FBTyxHQUFHdFAsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUFqQzZNO0FBQ0FDLFFBQUFBLE9BQU8sSUFBSXRQLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBVnhDLElBQTRCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXRDdkMsR0FBcURBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBL0R4QyxHQUFnRkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFyRytNO0FBQ0g7QUFMTCxLQUFBLE1BTU8sSUFBSXRQLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVWSxVQUFWWixLQUF5QixJQUE3QixFQUFtQztBQUN0Q3VQLE1BQUFBLFFBQVEsR0FBR3ZQLENBQUMsQ0FBQ2tFLFVBQWJxTDtBQURHLEtBQUEsTUFFQSxJQUFHLENBQUN2UCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVU8sUUFBZCxFQUF3QjtBQUMzQmdQLE1BQUFBLFFBQVEsR0FBRyxJQUFJakcsSUFBSSxDQUFDQyxJQUFMRCxDQUFVLENBQUN0SixDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUExQixJQUEwQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBOUQ4RyxDQUFmaUc7QUFERyxLQUFBLE1BRUQ7QUFDRixhQUFPRixVQUFVLEdBQUdyUCxDQUFDLENBQUNrRSxVQUF0QixFQUFrQztBQUM5QixVQUFFcUwsUUFBRjtBQUNBRixRQUFBQSxVQUFVLEdBQUdDLE9BQU8sR0FBR3RQLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBakM2TTtBQUNBQyxRQUFBQSxPQUFPLElBQUl0UCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQVZ4QyxJQUE0QkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUF0Q3ZDLEdBQXFEQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQS9EeEMsR0FBZ0ZBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBckcrTTtBQUNIO0FBQ0o7O0FBRUQsV0FBT0MsUUFBUSxHQUFHLENBQWxCO0FBMUJKLEdBQUEzUDs7QUE4QkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjRQLE9BQWhCNVAsR0FBMEIsVUFBUytPLFVBQVQsRUFBcUI7QUFFM0MsUUFBSTNPLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSTZJLFVBREo7QUFBQSxRQUVJNEcsY0FGSjtBQUFBLFFBR0lDLGNBQWMsR0FBRyxDQUhyQjtBQUFBLFFBSUlDLFdBSko7O0FBTUEzUCxJQUFBQSxDQUFDLENBQUN1RSxXQUFGdkUsR0FBZ0IsQ0FBaEJBO0FBQ0F5UCxJQUFBQSxjQUFjLEdBQUd6UCxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVThLLEtBQVY5SyxHQUFrQjBJLFdBQWxCMUksQ0FBOEIsSUFBOUJBLENBQWpCeVA7O0FBRUEsUUFBSXpQLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCLFVBQUlBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTdCLEVBQTJDO0FBQ3ZDdkMsUUFBQUEsQ0FBQyxDQUFDdUUsV0FBRnZFLEdBQWlCQSxDQUFDLENBQUNtRSxVQUFGbkUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUF6QnZDLEdBQXlDLENBQUMsQ0FBM0RBO0FBQ0EwUCxRQUFBQSxjQUFjLEdBQUlELGNBQWMsR0FBR3pQLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBM0JrTixHQUEyQyxDQUFDLENBQTlEQztBQUNIOztBQUNELFVBQUkxUCxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUF6QnhDLEtBQTRDLENBQWhELEVBQW1EO0FBQy9DLFlBQUkyTyxVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQXZCbU0sR0FBd0MzTyxDQUFDLENBQUNrRSxVQUExQ3lLLElBQXdEM08sQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBckYsRUFBbUc7QUFDL0YsY0FBSW9NLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBQW5CLEVBQStCO0FBQzNCbEUsWUFBQUEsQ0FBQyxDQUFDdUUsV0FBRnZFLEdBQWlCLENBQUNBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBVnZDLElBQTBCMk8sVUFBVSxHQUFHM08sQ0FBQyxDQUFDa0UsVUFBekNsRSxDQUFELElBQXlEQSxDQUFDLENBQUNtRSxVQUEzRCxHQUF5RSxDQUFDLENBQTNGbkU7QUFDQTBQLFlBQUFBLGNBQWMsR0FBSSxDQUFDMVAsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsSUFBMEIyTyxVQUFVLEdBQUczTyxDQUFDLENBQUNrRSxVQUF6Q2xFLENBQUQsSUFBeUR5UCxjQUF6RCxHQUEyRSxDQUFDLENBQTlGQztBQUZKLFdBQUEsTUFHTztBQUNIMVAsWUFBQUEsQ0FBQyxDQUFDdUUsV0FBRnZFLEdBQWtCQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUF6QnhDLEdBQTJDQSxDQUFDLENBQUNtRSxVQUE3Q25FLEdBQTJELENBQUMsQ0FBOUVBO0FBQ0EwUCxZQUFBQSxjQUFjLEdBQUsxUCxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUF6QnhDLEdBQTJDeVAsY0FBM0N6UCxHQUE2RCxDQUFDLENBQWpGMFA7QUFDSDtBQUNKO0FBQ0o7QUFmTCxLQUFBLE1BZ0JPO0FBQ0gsVUFBSWYsVUFBVSxHQUFHM08sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUF2Qm9NLEdBQXNDM08sQ0FBQyxDQUFDa0UsVUFBNUMsRUFBd0Q7QUFDcERsRSxRQUFBQSxDQUFDLENBQUN1RSxXQUFGdkUsR0FBZ0IsQ0FBRTJPLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBdkJvTSxHQUF1QzNPLENBQUMsQ0FBQ2tFLFVBQTNDLElBQXlEbEUsQ0FBQyxDQUFDbUUsVUFBM0VuRTtBQUNBMFAsUUFBQUEsY0FBYyxHQUFHLENBQUVmLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBdkJvTSxHQUF1QzNPLENBQUMsQ0FBQ2tFLFVBQTNDLElBQXlEdUwsY0FBMUVDO0FBQ0g7QUFDSjs7QUFFRCxRQUFJMVAsQ0FBQyxDQUFDa0UsVUFBRmxFLElBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTlCLEVBQTRDO0FBQ3hDdkMsTUFBQUEsQ0FBQyxDQUFDdUUsV0FBRnZFLEdBQWdCLENBQWhCQTtBQUNBMFAsTUFBQUEsY0FBYyxHQUFHLENBQWpCQTtBQUNIOztBQUVELFFBQUkxUCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVksVUFBVlosS0FBeUIsSUFBekJBLElBQWlDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBCLFFBQVYxQixLQUF1QixJQUE1RCxFQUFrRTtBQUM5REEsTUFBQUEsQ0FBQyxDQUFDdUUsV0FBRnZFLElBQWlCQSxDQUFDLENBQUNtRSxVQUFGbkUsR0FBZXNKLElBQUksQ0FBQ3NHLEtBQUx0RyxDQUFXdEosQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIsQ0FBcENzSixDQUFmdEosR0FBd0RBLENBQUMsQ0FBQ21FLFVBQTNFbkU7QUFESixLQUFBLE1BRU8sSUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVZLFVBQVZaLEtBQXlCLElBQTdCLEVBQW1DO0FBQ3RDQSxNQUFBQSxDQUFDLENBQUN1RSxXQUFGdkUsR0FBZ0IsQ0FBaEJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3VFLFdBQUZ2RSxJQUFpQkEsQ0FBQyxDQUFDbUUsVUFBRm5FLEdBQWVzSixJQUFJLENBQUNzRyxLQUFMdEcsQ0FBV3RKLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBVnZDLEdBQXlCLENBQXBDc0osQ0FBaEN0SjtBQUNIOztBQUVELFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVZ0QsUUFBVmhELEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCNkksTUFBQUEsVUFBVSxHQUFLOEYsVUFBVSxHQUFHM08sQ0FBQyxDQUFDbUUsVUFBZndLLEdBQTZCLENBQUMsQ0FBOUJBLEdBQW1DM08sQ0FBQyxDQUFDdUUsV0FBcERzRTtBQURKLEtBQUEsTUFFTztBQUNIQSxNQUFBQSxVQUFVLEdBQUs4RixVQUFVLEdBQUdjLGNBQWJkLEdBQStCLENBQUMsQ0FBaENBLEdBQXFDZSxjQUFwRDdHO0FBQ0g7O0FBRUQsUUFBSTdJLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVK0MsYUFBVi9DLEtBQTRCLElBQWhDLEVBQXNDO0FBRWxDLFVBQUlBLENBQUMsQ0FBQ2tFLFVBQUZsRSxJQUFnQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUExQnZDLElBQTBDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBCLFFBQVYxQixLQUF1QixLQUFyRSxFQUE0RTtBQUN4RTJQLFFBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjb0ksUUFBZHBJLENBQXVCLGNBQXZCQSxFQUF1Q2lJLEVBQXZDakksQ0FBMEMyTyxVQUExQzNPLENBQWQyUDtBQURKLE9BQUEsTUFFTztBQUNIQSxRQUFBQSxXQUFXLEdBQUczUCxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixjQUF2QkEsRUFBdUNpSSxFQUF2Q2pJLENBQTBDMk8sVUFBVSxHQUFHM08sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFqRXZDLENBQWQyUDtBQUNIOztBQUVELFVBQUkzUCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW9DLEdBQVZwQyxLQUFrQixJQUF0QixFQUE0QjtBQUN4QixZQUFJMlAsV0FBVyxDQUFDLENBQUQsQ0FBZixFQUFvQjtBQUNoQjlHLFVBQUFBLFVBQVUsR0FBRyxDQUFDN0ksQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNsQyxLQUFka0MsS0FBd0IyUCxXQUFXLENBQUMsQ0FBRCxDQUFYQSxDQUFlRSxVQUF2QzdQLEdBQW9EMlAsV0FBVyxDQUFDN1IsS0FBWjZSLEVBQXJELElBQTRFLENBQUMsQ0FBMUY5RztBQURKLFNBQUEsTUFFTztBQUNIQSxVQUFBQSxVQUFVLEdBQUksQ0FBZEE7QUFDSDtBQUxMLE9BQUEsTUFNTztBQUNIQSxRQUFBQSxVQUFVLEdBQUc4RyxXQUFXLENBQUMsQ0FBRCxDQUFYQSxHQUFpQkEsV0FBVyxDQUFDLENBQUQsQ0FBWEEsQ0FBZUUsVUFBZkYsR0FBNEIsQ0FBQyxDQUE5Q0EsR0FBa0QsQ0FBL0Q5RztBQUNIOztBQUVELFVBQUk3SSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVksVUFBVlosS0FBeUIsSUFBN0IsRUFBbUM7QUFDL0IsWUFBSUEsQ0FBQyxDQUFDa0UsVUFBRmxFLElBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTFCdkMsSUFBMENBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLEtBQXJFLEVBQTRFO0FBQ3hFMlAsVUFBQUEsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNvSSxRQUFkcEksQ0FBdUIsY0FBdkJBLEVBQXVDaUksRUFBdkNqSSxDQUEwQzJPLFVBQTFDM08sQ0FBZDJQO0FBREosU0FBQSxNQUVPO0FBQ0hBLFVBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjb0ksUUFBZHBJLENBQXVCLGNBQXZCQSxFQUF1Q2lJLEVBQXZDakksQ0FBMEMyTyxVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXZCb00sR0FBc0MsQ0FBaEYzTyxDQUFkMlA7QUFDSDs7QUFFRCxZQUFJM1AsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVvQyxHQUFWcEMsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEIsY0FBSTJQLFdBQVcsQ0FBQyxDQUFELENBQWYsRUFBb0I7QUFDaEI5RyxZQUFBQSxVQUFVLEdBQUcsQ0FBQzdJLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjbEMsS0FBZGtDLEtBQXdCMlAsV0FBVyxDQUFDLENBQUQsQ0FBWEEsQ0FBZUUsVUFBdkM3UCxHQUFvRDJQLFdBQVcsQ0FBQzdSLEtBQVo2UixFQUFyRCxJQUE0RSxDQUFDLENBQTFGOUc7QUFESixXQUFBLE1BRU87QUFDSEEsWUFBQUEsVUFBVSxHQUFJLENBQWRBO0FBQ0g7QUFMTCxTQUFBLE1BTU87QUFDSEEsVUFBQUEsVUFBVSxHQUFHOEcsV0FBVyxDQUFDLENBQUQsQ0FBWEEsR0FBaUJBLFdBQVcsQ0FBQyxDQUFELENBQVhBLENBQWVFLFVBQWZGLEdBQTRCLENBQUMsQ0FBOUNBLEdBQWtELENBQS9EOUc7QUFDSDs7QUFFREEsUUFBQUEsVUFBVSxJQUFJLENBQUM3SSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUWxDLEtBQVJrQyxLQUFrQjJQLFdBQVcsQ0FBQ0csVUFBWkgsRUFBbkIsSUFBK0MsQ0FBN0Q5RztBQUNIO0FBQ0o7O0FBRUQsV0FBT0EsVUFBUDtBQTNGSixHQUFBako7O0FBK0ZBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JtUSxTQUFoQm5RLEdBQTRCQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JvUSxjQUFoQnBRLEdBQWlDLFVBQVNxUSxNQUFULEVBQWlCO0FBRTFFLFFBQUlqUSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxXQUFPQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWlRLE1BQVZqUSxDQUFQO0FBSkosR0FBQUo7O0FBUUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmdPLG1CQUFoQmhPLEdBQXNDLFlBQVc7QUFFN0MsUUFBSUksQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJcVAsVUFBVSxHQUFHLENBRGpCO0FBQUEsUUFFSUMsT0FBTyxHQUFHLENBRmQ7QUFBQSxRQUdJWSxPQUFPLEdBQUcsRUFIZDtBQUFBLFFBSUlDLEdBSko7O0FBTUEsUUFBSW5RLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCbVEsTUFBQUEsR0FBRyxHQUFHblEsQ0FBQyxDQUFDa0UsVUFBUmlNO0FBREosS0FBQSxNQUVPO0FBQ0hkLE1BQUFBLFVBQVUsR0FBR3JQLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBVnhDLEdBQTJCLENBQUMsQ0FBekNxUDtBQUNBQyxNQUFBQSxPQUFPLEdBQUd0UCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQVZ4QyxHQUEyQixDQUFDLENBQXRDc1A7QUFDQWEsTUFBQUEsR0FBRyxHQUFHblEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWUsQ0FBckJtUTtBQUNIOztBQUVELFdBQU9kLFVBQVUsR0FBR2MsR0FBcEIsRUFBeUI7QUFDckJELE1BQUFBLE9BQU8sQ0FBQ0UsSUFBUkYsQ0FBYWIsVUFBYmE7QUFDQWIsTUFBQUEsVUFBVSxHQUFHQyxPQUFPLEdBQUd0UCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQWpDNk07QUFDQUMsTUFBQUEsT0FBTyxJQUFJdFAsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUFWeEMsSUFBNEJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBdEN2QyxHQUFxREEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUEvRHhDLEdBQWdGQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXJHK007QUFDSDs7QUFFRCxXQUFPWSxPQUFQO0FBdEJKLEdBQUF0UTs7QUEwQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnlRLFFBQWhCelEsR0FBMkIsWUFBVztBQUVsQyxXQUFPLElBQVA7QUFGSixHQUFBQTs7QUFNQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCMFEsYUFBaEIxUSxHQUFnQyxZQUFXO0FBRXZDLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXVRLGVBREo7QUFBQSxRQUNxQkMsV0FEckI7QUFBQSxRQUNrQ0MsWUFEbEM7O0FBR0FBLElBQUFBLFlBQVksR0FBR3pRLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVWSxVQUFWWixLQUF5QixJQUF6QkEsR0FBZ0NBLENBQUMsQ0FBQ21FLFVBQUZuRSxHQUFlc0osSUFBSSxDQUFDc0csS0FBTHRHLENBQVd0SixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQVZ2QyxHQUF5QixDQUFwQ3NKLENBQS9DdEosR0FBd0YsQ0FBdkd5UTs7QUFFQSxRQUFJelEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVUwQyxZQUFWMUMsS0FBMkIsSUFBL0IsRUFBcUM7QUFDakNBLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjc0gsSUFBZHRILENBQW1CLGNBQW5CQSxFQUFtQ3BCLElBQW5Db0IsQ0FBd0MsVUFBUzJILEtBQVQsRUFBZ0J0RixLQUFoQixFQUF1QjtBQUMzRCxZQUFJQSxLQUFLLENBQUN3TixVQUFOeE4sR0FBbUJvTyxZQUFuQnBPLEdBQW1DL0UsQ0FBQyxDQUFDK0UsS0FBRCxDQUFEL0UsQ0FBU3dTLFVBQVR4UyxLQUF3QixDQUEzRCtFLEdBQWlFckMsQ0FBQyxDQUFDd0UsU0FBRnhFLEdBQWMsQ0FBQyxDQUFwRixFQUF3RjtBQUNwRndRLFVBQUFBLFdBQVcsR0FBR25PLEtBQWRtTztBQUNBLGlCQUFPLEtBQVA7QUFDSDtBQUpMLE9BQUF4UTs7QUFPQXVRLE1BQUFBLGVBQWUsR0FBR2pILElBQUksQ0FBQ29ILEdBQUxwSCxDQUFTaE0sQ0FBQyxDQUFDa1QsV0FBRCxDQUFEbFQsQ0FBZWlLLElBQWZqSyxDQUFvQixrQkFBcEJBLElBQTBDMEMsQ0FBQyxDQUFDMEQsWUFBckQ0RixLQUFzRSxDQUF4RmlIO0FBRUEsYUFBT0EsZUFBUDtBQVZKLEtBQUEsTUFZTztBQUNILGFBQU92USxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQWpCO0FBQ0g7QUFyQkwsR0FBQTVDOztBQXlCQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCK1EsSUFBaEIvUSxHQUF1QkEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCZ1IsU0FBaEJoUixHQUE0QixVQUFTeUMsS0FBVCxFQUFnQjRLLFdBQWhCLEVBQTZCO0FBRTVFLFFBQUlqTixDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMEcsV0FBRjFHLENBQWM7QUFDVmlHLE1BQUFBLElBQUksRUFBRTtBQUNGdUgsUUFBQUEsT0FBTyxFQUFFLE9BRFA7QUFFRjdGLFFBQUFBLEtBQUssRUFBRWtKLFFBQVEsQ0FBQ3hPLEtBQUQ7QUFGYjtBQURJLEtBQWRyQyxFQUtHaU4sV0FMSGpOO0FBSkosR0FBQUo7O0FBYUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnVILElBQWhCdkgsR0FBdUIsVUFBU2tSLFFBQVQsRUFBbUI7QUFFdEMsUUFBSTlRLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUksQ0FBQzFDLENBQUMsQ0FBQzBDLENBQUMsQ0FBQzBGLE9BQUgsQ0FBRHBJLENBQWF5VCxRQUFielQsQ0FBc0IsbUJBQXRCQSxDQUFMLEVBQWlEO0FBRTdDQSxNQUFBQSxDQUFDLENBQUMwQyxDQUFDLENBQUMwRixPQUFILENBQURwSSxDQUFhb0IsUUFBYnBCLENBQXNCLG1CQUF0QkE7O0FBRUEwQyxNQUFBQSxDQUFDLENBQUNvTCxTQUFGcEw7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQytLLFFBQUYvSzs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDZ1IsUUFBRmhSOztBQUNBQSxNQUFBQSxDQUFDLENBQUNpUixTQUFGalI7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ2tSLFVBQUZsUjs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDbVIsZ0JBQUZuUjs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDb1IsWUFBRnBSOztBQUNBQSxNQUFBQSxDQUFDLENBQUNrTCxVQUFGbEw7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ2tNLGVBQUZsTSxDQUFrQixJQUFsQkE7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ2lQLFlBQUZqUDtBQUVIOztBQUVELFFBQUk4USxRQUFKLEVBQWM7QUFDVjlRLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVK00sT0FBVi9NLENBQWtCLE1BQWxCQSxFQUEwQixDQUFDQSxDQUFELENBQTFCQTtBQUNIOztBQUVELFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVRSxhQUFWRixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ0EsTUFBQUEsQ0FBQyxDQUFDcVIsT0FBRnJSO0FBQ0g7O0FBRUQsUUFBS0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVVLFFBQWYsRUFBMEI7QUFFdEJWLE1BQUFBLENBQUMsQ0FBQ3NGLE1BQUZ0RixHQUFXLEtBQVhBOztBQUNBQSxNQUFBQSxDQUFDLENBQUNzRyxRQUFGdEc7QUFFSDtBQWxDTCxHQUFBSjs7QUFzQ0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnlSLE9BQWhCelIsR0FBMEIsWUFBVztBQUNqQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVUwSyxHQUFWMUssQ0FBY0EsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNzSCxJQUFkdEgsQ0FBbUIsZUFBbkJBLENBQWRBLEVBQW1EdUgsSUFBbkR2SCxDQUF3RDtBQUNwRCxxQkFBZSxNQURxQztBQUVwRCxrQkFBWTtBQUZ3QyxLQUF4REEsRUFHR3NILElBSEh0SCxDQUdRLDBCQUhSQSxFQUdvQ3VILElBSHBDdkgsQ0FHeUM7QUFDckMsa0JBQVk7QUFEeUIsS0FIekNBOztBQU9BQSxJQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY3VILElBQWR2SCxDQUFtQixNQUFuQkEsRUFBMkIsU0FBM0JBOztBQUVBQSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVThKLEdBQVY5SixDQUFjQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY3NILElBQWR0SCxDQUFtQixlQUFuQkEsQ0FBZEEsRUFBbURwQixJQUFuRG9CLENBQXdELFVBQVNpQixDQUFULEVBQVk7QUFDaEUzRCxNQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFEQSxDQUFRaUssSUFBUmpLLENBQWE7QUFDVCxnQkFBUSxRQURDO0FBRVQsNEJBQW9CLGdCQUFnQjBDLENBQUMsQ0FBQ0YsV0FBbEIsR0FBZ0NtQixDQUFoQyxHQUFvQztBQUYvQyxPQUFiM0Q7QUFESixLQUFBMEM7O0FBT0EsUUFBSUEsQ0FBQyxDQUFDNEQsS0FBRjVELEtBQVksSUFBaEIsRUFBc0I7QUFDbEJBLE1BQUFBLENBQUMsQ0FBQzRELEtBQUY1RCxDQUFRdUgsSUFBUnZILENBQWEsTUFBYkEsRUFBcUIsU0FBckJBLEVBQWdDc0gsSUFBaEN0SCxDQUFxQyxJQUFyQ0EsRUFBMkNwQixJQUEzQ29CLENBQWdELFVBQVNpQixDQUFULEVBQVk7QUFDeEQzRCxRQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFEQSxDQUFRaUssSUFBUmpLLENBQWE7QUFDVCxrQkFBUSxjQURDO0FBRVQsMkJBQWlCLE9BRlI7QUFHVCwyQkFBaUIsZUFBZTBDLENBQUMsQ0FBQ0YsV0FBakIsR0FBK0JtQixDQUEvQixHQUFtQyxFQUgzQztBQUlULGdCQUFNLGdCQUFnQmpCLENBQUMsQ0FBQ0YsV0FBbEIsR0FBZ0NtQixDQUFoQyxHQUFvQztBQUpqQyxTQUFiM0Q7QUFESixPQUFBMEMsRUFRSzhLLEtBUkw5SyxHQVFhdUgsSUFSYnZILENBUWtCLGVBUmxCQSxFQVFtQyxNQVJuQ0EsRUFRMkNzUixHQVIzQ3RSLEdBU0tzSCxJQVRMdEgsQ0FTVSxRQVRWQSxFQVNvQnVILElBVHBCdkgsQ0FTeUIsTUFUekJBLEVBU2lDLFFBVGpDQSxFQVMyQ3NSLEdBVDNDdFIsR0FVS1osT0FWTFksQ0FVYSxLQVZiQSxFQVVvQnVILElBVnBCdkgsQ0FVeUIsTUFWekJBLEVBVWlDLFNBVmpDQTtBQVdIOztBQUNEQSxJQUFBQSxDQUFDLENBQUNxSCxXQUFGckg7QUEvQkosR0FBQUo7O0FBbUNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0IyUixlQUFoQjNSLEdBQWtDLFlBQVc7QUFFekMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVNLE1BQVZOLEtBQXFCLElBQXJCQSxJQUE2QkEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBMUQsRUFBd0U7QUFDcEV2QyxNQUFBQSxDQUFDLENBQUNpRSxVQUFGakUsQ0FDSStOLEdBREovTixDQUNRLGFBRFJBLEVBRUlkLEVBRkpjLENBRU8sYUFGUEEsRUFFc0I7QUFDZHdOLFFBQUFBLE9BQU8sRUFBRTtBQURLLE9BRnRCeE4sRUFJTUEsQ0FBQyxDQUFDMEcsV0FKUjFHOztBQUtBQSxNQUFBQSxDQUFDLENBQUNnRSxVQUFGaEUsQ0FDSStOLEdBREovTixDQUNRLGFBRFJBLEVBRUlkLEVBRkpjLENBRU8sYUFGUEEsRUFFc0I7QUFDZHdOLFFBQUFBLE9BQU8sRUFBRTtBQURLLE9BRnRCeE4sRUFJTUEsQ0FBQyxDQUFDMEcsV0FKUjFHO0FBS0g7QUFmTCxHQUFBSjs7QUFtQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjRSLGFBQWhCNVIsR0FBZ0MsWUFBVztBQUV2QyxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1CLElBQVZuQixLQUFtQixJQUFuQkEsSUFBMkJBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXhELEVBQXNFO0FBQ2xFakYsTUFBQUEsQ0FBQyxDQUFDLElBQUQsRUFBTzBDLENBQUMsQ0FBQzRELEtBQVQsQ0FBRHRHLENBQWlCNEIsRUFBakI1QixDQUFvQixhQUFwQkEsRUFBbUM7QUFDL0JrUSxRQUFBQSxPQUFPLEVBQUU7QUFEc0IsT0FBbkNsUSxFQUVHMEMsQ0FBQyxDQUFDMEcsV0FGTHBKO0FBR0g7O0FBRUQsUUFBSzBDLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVbUIsSUFBVm5CLEtBQW1CLElBQW5CQSxJQUEyQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVnQyxnQkFBVmhDLEtBQStCLElBQS9ELEVBQXNFO0FBRWxFMUMsTUFBQUEsQ0FBQyxDQUFDLElBQUQsRUFBTzBDLENBQUMsQ0FBQzRELEtBQVQsQ0FBRHRHLENBQ0s0QixFQURMNUIsQ0FDUSxrQkFEUkEsRUFDNEJBLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDZ08sU0FBVjFRLEVBQXFCMEMsQ0FBckIxQyxFQUF3QixJQUF4QkEsQ0FENUJBLEVBRUs0QixFQUZMNUIsQ0FFUSxrQkFGUkEsRUFFNEJBLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDZ08sU0FBVjFRLEVBQXFCMEMsQ0FBckIxQyxFQUF3QixLQUF4QkEsQ0FGNUJBO0FBSUg7QUFoQkwsR0FBQXNDOztBQW9CQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCNlIsZUFBaEI3UixHQUFrQyxZQUFXO0FBRXpDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUtBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVOEIsWUFBZixFQUE4QjtBQUUxQjlCLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRZCxFQUFSYyxDQUFXLGtCQUFYQSxFQUErQjFDLENBQUMsQ0FBQ2lKLEtBQUZqSixDQUFRMEMsQ0FBQyxDQUFDZ08sU0FBVjFRLEVBQXFCMEMsQ0FBckIxQyxFQUF3QixJQUF4QkEsQ0FBL0IwQzs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVFkLEVBQVJjLENBQVcsa0JBQVhBLEVBQStCMUMsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUNnTyxTQUFWMVEsRUFBcUIwQyxDQUFyQjFDLEVBQXdCLEtBQXhCQSxDQUEvQjBDO0FBRUg7QUFUTCxHQUFBSjs7QUFhQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCdVIsZ0JBQWhCdlIsR0FBbUMsWUFBVztBQUUxQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDdVIsZUFBRnZSOztBQUVBQSxJQUFBQSxDQUFDLENBQUN3UixhQUFGeFI7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ3lSLGVBQUZ6Ujs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVFkLEVBQVJjLENBQVcsa0NBQVhBLEVBQStDO0FBQzNDMFIsTUFBQUEsTUFBTSxFQUFFO0FBRG1DLEtBQS9DMVIsRUFFR0EsQ0FBQyxDQUFDOEcsWUFGTDlHOztBQUdBQSxJQUFBQSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUWQsRUFBUmMsQ0FBVyxpQ0FBWEEsRUFBOEM7QUFDMUMwUixNQUFBQSxNQUFNLEVBQUU7QUFEa0MsS0FBOUMxUixFQUVHQSxDQUFDLENBQUM4RyxZQUZMOUc7O0FBR0FBLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRZCxFQUFSYyxDQUFXLDhCQUFYQSxFQUEyQztBQUN2QzBSLE1BQUFBLE1BQU0sRUFBRTtBQUQrQixLQUEzQzFSLEVBRUdBLENBQUMsQ0FBQzhHLFlBRkw5Rzs7QUFHQUEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVFkLEVBQVJjLENBQVcsb0NBQVhBLEVBQWlEO0FBQzdDMFIsTUFBQUEsTUFBTSxFQUFFO0FBRHFDLEtBQWpEMVIsRUFFR0EsQ0FBQyxDQUFDOEcsWUFGTDlHOztBQUlBQSxJQUFBQSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUWQsRUFBUmMsQ0FBVyxhQUFYQSxFQUEwQkEsQ0FBQyxDQUFDMkcsWUFBNUIzRzs7QUFFQTFDLElBQUFBLENBQUMsQ0FBQzZJLFFBQUQsQ0FBRDdJLENBQVk0QixFQUFaNUIsQ0FBZTBDLENBQUMsQ0FBQzhGLGdCQUFqQnhJLEVBQW1DQSxDQUFDLENBQUNpSixLQUFGakosQ0FBUTBDLENBQUMsQ0FBQ2lPLFVBQVYzUSxFQUFzQjBDLENBQXRCMUMsQ0FBbkNBOztBQUVBLFFBQUkwQyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUUsYUFBVkYsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbENBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRZCxFQUFSYyxDQUFXLGVBQVhBLEVBQTRCQSxDQUFDLENBQUNnSCxVQUE5QmhIO0FBQ0g7O0FBRUQsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV5QixhQUFWekIsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbEMxQyxNQUFBQSxDQUFDLENBQUMwQyxDQUFDLENBQUNvRSxXQUFILENBQUQ5RyxDQUFpQjhLLFFBQWpCOUssR0FBNEI0QixFQUE1QjVCLENBQStCLGFBQS9CQSxFQUE4QzBDLENBQUMsQ0FBQzRHLGFBQWhEdEo7QUFDSDs7QUFFREEsSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFEdkMsQ0FBVTRCLEVBQVY1QixDQUFhLG1DQUFtQzBDLENBQUMsQ0FBQ0YsV0FBbER4QyxFQUErREEsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUNtTyxpQkFBVjdRLEVBQTZCMEMsQ0FBN0IxQyxDQUEvREE7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFEdkMsQ0FBVTRCLEVBQVY1QixDQUFhLHdCQUF3QjBDLENBQUMsQ0FBQ0YsV0FBdkN4QyxFQUFvREEsQ0FBQyxDQUFDaUosS0FBRmpKLENBQVEwQyxDQUFDLENBQUNvTyxNQUFWOVEsRUFBa0IwQyxDQUFsQjFDLENBQXBEQTtBQUVBQSxJQUFBQSxDQUFDLENBQUMsbUJBQUQsRUFBc0IwQyxDQUFDLENBQUNvRSxXQUF4QixDQUFEOUcsQ0FBc0M0QixFQUF0QzVCLENBQXlDLFdBQXpDQSxFQUFzRDBDLENBQUMsQ0FBQ3VOLGNBQXhEalE7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFEdkMsQ0FBVTRCLEVBQVY1QixDQUFhLHNCQUFzQjBDLENBQUMsQ0FBQ0YsV0FBckN4QyxFQUFrRDBDLENBQUMsQ0FBQzZHLFdBQXBEdko7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDNkksUUFBRCxDQUFEN0ksQ0FBWTRCLEVBQVo1QixDQUFlLHVCQUF1QjBDLENBQUMsQ0FBQ0YsV0FBeEN4QyxFQUFxRDBDLENBQUMsQ0FBQzZHLFdBQXZEdko7QUF6Q0osR0FBQXNDOztBQTZDQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCK1IsTUFBaEIvUixHQUF5QixZQUFXO0FBRWhDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVTSxNQUFWTixLQUFxQixJQUFyQkEsSUFBNkJBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTFELEVBQXdFO0FBRXBFdkMsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQWE0UixJQUFiNVI7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUZoRSxDQUFhNFIsSUFBYjVSO0FBRUg7O0FBRUQsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVtQixJQUFWbkIsS0FBbUIsSUFBbkJBLElBQTJCQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUF4RCxFQUFzRTtBQUVsRXZDLE1BQUFBLENBQUMsQ0FBQzRELEtBQUY1RCxDQUFRNFIsSUFBUjVSO0FBRUg7QUFmTCxHQUFBSjs7QUFtQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQm9ILFVBQWhCcEgsR0FBNkIsVUFBU29OLEtBQVQsRUFBZ0I7QUFFekMsUUFBSWhOLENBQUMsR0FBRyxJQUFSLENBRnlDLENBR3hDOzs7QUFDRCxRQUFHLENBQUNnTixLQUFLLENBQUNqRCxNQUFOaUQsQ0FBYTZFLE9BQWI3RSxDQUFxQjhFLEtBQXJCOUUsQ0FBMkIsdUJBQTNCQSxDQUFKLEVBQXlEO0FBQ3JELFVBQUlBLEtBQUssQ0FBQytFLE9BQU4vRSxLQUFrQixFQUFsQkEsSUFBd0JoTixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUUsYUFBVkYsS0FBNEIsSUFBeEQsRUFBOEQ7QUFDMURBLFFBQUFBLENBQUMsQ0FBQzBHLFdBQUYxRyxDQUFjO0FBQ1ZpRyxVQUFBQSxJQUFJLEVBQUU7QUFDRnVILFlBQUFBLE9BQU8sRUFBRXhOLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVb0MsR0FBVnBDLEtBQWtCLElBQWxCQSxHQUF5QixNQUF6QkEsR0FBbUM7QUFEMUM7QUFESSxTQUFkQTtBQURKLE9BQUEsTUFNTyxJQUFJZ04sS0FBSyxDQUFDK0UsT0FBTi9FLEtBQWtCLEVBQWxCQSxJQUF3QmhOLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVRSxhQUFWRixLQUE0QixJQUF4RCxFQUE4RDtBQUNqRUEsUUFBQUEsQ0FBQyxDQUFDMEcsV0FBRjFHLENBQWM7QUFDVmlHLFVBQUFBLElBQUksRUFBRTtBQUNGdUgsWUFBQUEsT0FBTyxFQUFFeE4sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVvQyxHQUFWcEMsS0FBa0IsSUFBbEJBLEdBQXlCLFVBQXpCQSxHQUFzQztBQUQ3QztBQURJLFNBQWRBO0FBS0g7QUFDSjtBQWxCTCxHQUFBSjs7QUFzQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmdDLFFBQWhCaEMsR0FBMkIsWUFBVztBQUVsQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lnUyxTQURKO0FBQUEsUUFDZUMsVUFEZjtBQUFBLFFBQzJCQyxVQUQzQjtBQUFBLFFBQ3VDQyxRQUR2Qzs7QUFHQSxhQUFTQyxVQUFULENBQW9CQyxXQUFwQixFQUFpQztBQUU3Qi9VLE1BQUFBLENBQUMsQ0FBQyxnQkFBRCxFQUFtQitVLFdBQW5CLENBQUQvVSxDQUFpQ3NCLElBQWpDdEIsQ0FBc0MsWUFBVztBQUU3QyxZQUFJZ1YsS0FBSyxHQUFHaFYsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUFBLFlBQ0lpVixXQUFXLEdBQUdqVixDQUFDLENBQUMsSUFBRCxDQUFEQSxDQUFRaUssSUFBUmpLLENBQWEsV0FBYkEsQ0FEbEI7QUFBQSxZQUVJa1YsV0FBVyxHQUFHck0sUUFBUSxDQUFDMEYsYUFBVDFGLENBQXVCLEtBQXZCQSxDQUZsQjs7QUFJQXFNLFFBQUFBLFdBQVcsQ0FBQ0MsTUFBWkQsR0FBcUIsWUFBVztBQUU1QkYsVUFBQUEsS0FBSyxDQUNBM0osT0FETDJKLENBQ2E7QUFBRTFELFlBQUFBLE9BQU8sRUFBRTtBQUFYLFdBRGIwRCxFQUM2QixHQUQ3QkEsRUFDa0MsWUFBVztBQUNyQ0EsWUFBQUEsS0FBSyxDQUNBL0ssSUFETCtLLENBQ1UsS0FEVkEsRUFDaUJDLFdBRGpCRCxFQUVLM0osT0FGTDJKLENBRWE7QUFBRTFELGNBQUFBLE9BQU8sRUFBRTtBQUFYLGFBRmIwRCxFQUU2QixHQUY3QkEsRUFFa0MsWUFBVztBQUNyQ0EsY0FBQUEsS0FBSyxDQUNBOUgsVUFETDhILENBQ2dCLFdBRGhCQSxFQUVLL0gsV0FGTCtILENBRWlCLGVBRmpCQTtBQUhSLGFBQUFBOztBQU9BdFMsWUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVUrTSxPQUFWL00sQ0FBa0IsWUFBbEJBLEVBQWdDLENBQUNBLENBQUQsRUFBSXNTLEtBQUosRUFBV0MsV0FBWCxDQUFoQ3ZTO0FBVFIsV0FBQXNTO0FBRkosU0FBQUU7O0FBZ0JBQSxRQUFBQSxXQUFXLENBQUNFLE9BQVpGLEdBQXNCLFlBQVc7QUFFN0JGLFVBQUFBLEtBQUssQ0FDQTlILFVBREw4SCxDQUNpQixXQURqQkEsRUFFSy9ILFdBRkwrSCxDQUVrQixlQUZsQkEsRUFHSzVULFFBSEw0VCxDQUdlLHNCQUhmQTs7QUFLQXRTLFVBQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVK00sT0FBVi9NLENBQWtCLGVBQWxCQSxFQUFtQyxDQUFFQSxDQUFGLEVBQUtzUyxLQUFMLEVBQVlDLFdBQVosQ0FBbkN2UztBQVBKLFNBQUF3Uzs7QUFXQUEsUUFBQUEsV0FBVyxDQUFDRyxHQUFaSCxHQUFrQkQsV0FBbEJDO0FBakNKLE9BQUFsVjtBQXFDSDs7QUFFRCxRQUFJMEMsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVZLFVBQVZaLEtBQXlCLElBQTdCLEVBQW1DO0FBQy9CLFVBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCa1MsUUFBQUEsVUFBVSxHQUFHbFMsQ0FBQyxDQUFDMEQsWUFBRjFELElBQWtCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQVZ2QyxHQUF5QixDQUF6QkEsR0FBNkIsQ0FBL0NBLENBQWJrUztBQUNBQyxRQUFBQSxRQUFRLEdBQUdELFVBQVUsR0FBR2xTLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBdkIyUCxHQUFzQyxDQUFqREM7QUFGSixPQUFBLE1BR087QUFDSEQsUUFBQUEsVUFBVSxHQUFHNUksSUFBSSxDQUFDNkcsR0FBTDdHLENBQVMsQ0FBVEEsRUFBWXRKLENBQUMsQ0FBQzBELFlBQUYxRCxJQUFrQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIsQ0FBekJBLEdBQTZCLENBQS9DQSxDQUFac0osQ0FBYjRJO0FBQ0FDLFFBQUFBLFFBQVEsR0FBRyxLQUFLblMsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIsQ0FBekJBLEdBQTZCLENBQWxDLElBQXVDQSxDQUFDLENBQUMwRCxZQUFwRHlPO0FBQ0g7QUFQTCxLQUFBLE1BUU87QUFDSEQsTUFBQUEsVUFBVSxHQUFHbFMsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVUwQixRQUFWMUIsR0FBcUJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBVnZDLEdBQXlCQSxDQUFDLENBQUMwRCxZQUFoRDFELEdBQStEQSxDQUFDLENBQUMwRCxZQUE5RXdPO0FBQ0FDLE1BQUFBLFFBQVEsR0FBRzdJLElBQUksQ0FBQ0MsSUFBTEQsQ0FBVTRJLFVBQVUsR0FBR2xTLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBakMrRyxDQUFYNkk7O0FBQ0EsVUFBSW5TLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0IsSUFBVnhCLEtBQW1CLElBQXZCLEVBQTZCO0FBQ3pCLFlBQUlrUyxVQUFVLEdBQUcsQ0FBakIsRUFBb0JBLFVBQVU7QUFDOUIsWUFBSUMsUUFBUSxJQUFJblMsQ0FBQyxDQUFDa0UsVUFBbEIsRUFBOEJpTyxRQUFRO0FBQ3pDO0FBQ0o7O0FBRURILElBQUFBLFNBQVMsR0FBR2hTLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVc0gsSUFBVnRILENBQWUsY0FBZkEsRUFBK0I0UyxLQUEvQjVTLENBQXFDa1MsVUFBckNsUyxFQUFpRG1TLFFBQWpEblMsQ0FBWmdTO0FBQ0FJLElBQUFBLFVBQVUsQ0FBQ0osU0FBRCxDQUFWSTs7QUFFQSxRQUFJcFMsQ0FBQyxDQUFDa0UsVUFBRmxFLElBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTlCLEVBQTRDO0FBQ3hDMFAsTUFBQUEsVUFBVSxHQUFHalMsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVVzSCxJQUFWdEgsQ0FBZSxjQUFmQSxDQUFiaVM7QUFDQUcsTUFBQUEsVUFBVSxDQUFDSCxVQUFELENBQVZHO0FBRkosS0FBQSxNQUlBLElBQUlwUyxDQUFDLENBQUMwRCxZQUFGMUQsSUFBa0JBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQS9DLEVBQTZEO0FBQ3pEMFAsTUFBQUEsVUFBVSxHQUFHalMsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVVzSCxJQUFWdEgsQ0FBZSxlQUFmQSxFQUFnQzRTLEtBQWhDNVMsQ0FBc0MsQ0FBdENBLEVBQXlDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQW5EdkMsQ0FBYmlTO0FBQ0FHLE1BQUFBLFVBQVUsQ0FBQ0gsVUFBRCxDQUFWRztBQUZKLEtBQUEsTUFHTyxJQUFJcFMsQ0FBQyxDQUFDMEQsWUFBRjFELEtBQW1CLENBQXZCLEVBQTBCO0FBQzdCaVMsTUFBQUEsVUFBVSxHQUFHalMsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVVzSCxJQUFWdEgsQ0FBZSxlQUFmQSxFQUFnQzRTLEtBQWhDNVMsQ0FBc0NBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBVnZDLEdBQXlCLENBQUMsQ0FBaEVBLENBQWJpUztBQUNBRyxNQUFBQSxVQUFVLENBQUNILFVBQUQsQ0FBVkc7QUFDSDtBQTVFTCxHQUFBeFM7O0FBZ0ZBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JzUixVQUFoQnRSLEdBQTZCLFlBQVc7QUFFcEMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzZHLFdBQUY3Rzs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQixHQUFkaUIsQ0FBa0I7QUFDZDRPLE1BQUFBLE9BQU8sRUFBRTtBQURLLEtBQWxCNU87O0FBSUFBLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVdUssV0FBVnZLLENBQXNCLGVBQXRCQTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMlIsTUFBRjNSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVNEIsUUFBVjVCLEtBQXVCLGFBQTNCLEVBQTBDO0FBQ3RDQSxNQUFBQSxDQUFDLENBQUM2UyxtQkFBRjdTO0FBQ0g7QUFoQkwsR0FBQUo7O0FBb0JBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JrVCxJQUFoQmxULEdBQXVCQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JtVCxTQUFoQm5ULEdBQTRCLFlBQVc7QUFFMUQsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzBHLFdBQUYxRyxDQUFjO0FBQ1ZpRyxNQUFBQSxJQUFJLEVBQUU7QUFDRnVILFFBQUFBLE9BQU8sRUFBRTtBQURQO0FBREksS0FBZHhOO0FBSkosR0FBQUo7O0FBWUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnVPLGlCQUFoQnZPLEdBQW9DLFlBQVc7QUFFM0MsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ2tNLGVBQUZsTTs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDNkcsV0FBRjdHO0FBTEosR0FBQUo7O0FBU0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQm9ULEtBQWhCcFQsR0FBd0JBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnFULFVBQWhCclQsR0FBNkIsWUFBVztBQUU1RCxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDd0csYUFBRnhHOztBQUNBQSxJQUFBQSxDQUFDLENBQUNzRixNQUFGdEYsR0FBVyxJQUFYQTtBQUxKLEdBQUFKOztBQVNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JzVCxJQUFoQnRULEdBQXVCQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J1VCxTQUFoQnZULEdBQTRCLFlBQVc7QUFFMUQsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3NHLFFBQUZ0Rzs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVVLFFBQVZWLEdBQXFCLElBQXJCQTtBQUNBQSxJQUFBQSxDQUFDLENBQUNzRixNQUFGdEYsR0FBVyxLQUFYQTtBQUNBQSxJQUFBQSxDQUFDLENBQUNtRixRQUFGbkYsR0FBYSxLQUFiQTtBQUNBQSxJQUFBQSxDQUFDLENBQUNvRixXQUFGcEYsR0FBZ0IsS0FBaEJBO0FBUkosR0FBQUo7O0FBWUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQndULFNBQWhCeFQsR0FBNEIsVUFBUytILEtBQVQsRUFBZ0I7QUFFeEMsUUFBSTNILENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUksQ0FBQ0EsQ0FBQyxDQUFDNEUsU0FBUCxFQUFtQjtBQUVmNUUsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVUrTSxPQUFWL00sQ0FBa0IsYUFBbEJBLEVBQWlDLENBQUNBLENBQUQsRUFBSTJILEtBQUosQ0FBakMzSDs7QUFFQUEsTUFBQUEsQ0FBQyxDQUFDcUQsU0FBRnJELEdBQWMsS0FBZEE7O0FBRUFBLE1BQUFBLENBQUMsQ0FBQzZHLFdBQUY3Rzs7QUFFQUEsTUFBQUEsQ0FBQyxDQUFDd0UsU0FBRnhFLEdBQWMsSUFBZEE7O0FBRUEsVUFBS0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVVLFFBQWYsRUFBMEI7QUFDdEJWLFFBQUFBLENBQUMsQ0FBQ3NHLFFBQUZ0RztBQUNIOztBQUVELFVBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVRSxhQUFWRixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ0EsUUFBQUEsQ0FBQyxDQUFDcVIsT0FBRnJSO0FBQ0g7QUFFSjtBQXRCTCxHQUFBSjs7QUEwQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnlULElBQWhCelQsR0FBdUJBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjBULFNBQWhCMVQsR0FBNEIsWUFBVztBQUUxRCxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMEcsV0FBRjFHLENBQWM7QUFDVmlHLE1BQUFBLElBQUksRUFBRTtBQUNGdUgsUUFBQUEsT0FBTyxFQUFFO0FBRFA7QUFESSxLQUFkeE47QUFKSixHQUFBSjs7QUFZQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCMk4sY0FBaEIzTixHQUFpQyxVQUFTb04sS0FBVCxFQUFnQjtBQUU3Q0EsSUFBQUEsS0FBSyxDQUFDTyxjQUFOUDtBQUZKLEdBQUFwTjs7QUFNQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCaVQsbUJBQWhCalQsR0FBc0MsVUFBVTJULFFBQVYsRUFBcUI7QUFFdkRBLElBQUFBLFFBQVEsR0FBR0EsUUFBUSxJQUFJLENBQXZCQTs7QUFFQSxRQUFJdlQsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJd1QsV0FBVyxHQUFHbFcsQ0FBQyxDQUFFLGdCQUFGLEVBQW9CMEMsQ0FBQyxDQUFDMEYsT0FBdEIsQ0FEbkI7QUFBQSxRQUVJNE0sS0FGSjtBQUFBLFFBR0lDLFdBSEo7QUFBQSxRQUlJQyxXQUpKOztBQU1BLFFBQUtnQixXQUFXLENBQUMxTCxNQUFqQixFQUEwQjtBQUV0QndLLE1BQUFBLEtBQUssR0FBR2tCLFdBQVcsQ0FBQzFJLEtBQVowSSxFQUFSbEI7QUFDQUMsTUFBQUEsV0FBVyxHQUFHRCxLQUFLLENBQUMvSyxJQUFOK0ssQ0FBVyxXQUFYQSxDQUFkQztBQUNBQyxNQUFBQSxXQUFXLEdBQUdyTSxRQUFRLENBQUMwRixhQUFUMUYsQ0FBdUIsS0FBdkJBLENBQWRxTTs7QUFFQUEsTUFBQUEsV0FBVyxDQUFDQyxNQUFaRCxHQUFxQixZQUFXO0FBRTVCRixRQUFBQSxLQUFLLENBQ0EvSyxJQURMK0ssQ0FDVyxLQURYQSxFQUNrQkMsV0FEbEJELEVBRUs5SCxVQUZMOEgsQ0FFZ0IsV0FGaEJBLEVBR0svSCxXQUhMK0gsQ0FHaUIsZUFIakJBOztBQUtBLFlBQUt0UyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUcsY0FBVkgsS0FBNkIsSUFBbEMsRUFBeUM7QUFDckNBLFVBQUFBLENBQUMsQ0FBQzZHLFdBQUY3RztBQUNIOztBQUVEQSxRQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixZQUFsQkEsRUFBZ0MsQ0FBRUEsQ0FBRixFQUFLc1MsS0FBTCxFQUFZQyxXQUFaLENBQWhDdlM7O0FBQ0FBLFFBQUFBLENBQUMsQ0FBQzZTLG1CQUFGN1M7QUFaSixPQUFBd1M7O0FBZ0JBQSxNQUFBQSxXQUFXLENBQUNFLE9BQVpGLEdBQXNCLFlBQVc7QUFFN0IsWUFBS2UsUUFBUSxHQUFHLENBQWhCLEVBQW9CO0FBRWhCOzs7OztBQUtBNUosVUFBQUEsVUFBVSxDQUFFLFlBQVc7QUFDbkIzSixZQUFBQSxDQUFDLENBQUM2UyxtQkFBRjdTLENBQXVCdVQsUUFBUSxHQUFHLENBQWxDdlQ7QUFETSxXQUFBLEVBRVAsR0FGTyxDQUFWMko7QUFQSixTQUFBLE1BV087QUFFSDJJLFVBQUFBLEtBQUssQ0FDQTlILFVBREw4SCxDQUNpQixXQURqQkEsRUFFSy9ILFdBRkwrSCxDQUVrQixlQUZsQkEsRUFHSzVULFFBSEw0VCxDQUdlLHNCQUhmQTs7QUFLQXRTLFVBQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVK00sT0FBVi9NLENBQWtCLGVBQWxCQSxFQUFtQyxDQUFFQSxDQUFGLEVBQUtzUyxLQUFMLEVBQVlDLFdBQVosQ0FBbkN2Uzs7QUFFQUEsVUFBQUEsQ0FBQyxDQUFDNlMsbUJBQUY3UztBQUVIO0FBeEJMLE9BQUF3Uzs7QUE0QkFBLE1BQUFBLFdBQVcsQ0FBQ0csR0FBWkgsR0FBa0JELFdBQWxCQztBQWxESixLQUFBLE1Bb0RPO0FBRUh4UyxNQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixpQkFBbEJBLEVBQXFDLENBQUVBLENBQUYsQ0FBckNBO0FBRUg7QUFsRUwsR0FBQUo7O0FBc0VBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JrTixPQUFoQmxOLEdBQTBCLFVBQVU2VCxZQUFWLEVBQXlCO0FBRS9DLFFBQUl6VCxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQWMwRCxZQUFkO0FBQUEsUUFBNEJnUSxnQkFBNUI7O0FBRUFBLElBQUFBLGdCQUFnQixHQUFHMVQsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBNUNtUixDQUorQyxDQU0vQztBQUNBOztBQUNBLFFBQUksQ0FBQzFULENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBWCxJQUF5QjFCLENBQUMsQ0FBQzBELFlBQUYxRCxHQUFpQjBULGdCQUE5QyxFQUFrRTtBQUM5RDFULE1BQUFBLENBQUMsQ0FBQzBELFlBQUYxRCxHQUFpQjBULGdCQUFqQjFUO0FBVDJDLEtBQUEsQ0FZL0M7OztBQUNBLFFBQUtBLENBQUMsQ0FBQ2tFLFVBQUZsRSxJQUFnQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUEvQixFQUE4QztBQUMxQ3ZDLE1BQUFBLENBQUMsQ0FBQzBELFlBQUYxRCxHQUFpQixDQUFqQkE7QUFFSDs7QUFFRDBELElBQUFBLFlBQVksR0FBRzFELENBQUMsQ0FBQzBELFlBQWpCQTs7QUFFQTFELElBQUFBLENBQUMsQ0FBQ3dPLE9BQUZ4TyxDQUFVLElBQVZBOztBQUVBMUMsSUFBQUEsQ0FBQyxDQUFDa0IsTUFBRmxCLENBQVMwQyxDQUFUMUMsRUFBWTBDLENBQUMsQ0FBQ29ELFFBQWQ5RixFQUF3QjtBQUFFb0csTUFBQUEsWUFBWSxFQUFFQTtBQUFoQixLQUF4QnBHOztBQUVBMEMsSUFBQUEsQ0FBQyxDQUFDbUgsSUFBRm5IOztBQUVBLFFBQUksQ0FBQ3lULFlBQUwsRUFBb0I7QUFFaEJ6VCxNQUFBQSxDQUFDLENBQUMwRyxXQUFGMUcsQ0FBYztBQUNWaUcsUUFBQUEsSUFBSSxFQUFFO0FBQ0Z1SCxVQUFBQSxPQUFPLEVBQUUsT0FEUDtBQUVGN0YsVUFBQUEsS0FBSyxFQUFFakU7QUFGTDtBQURJLE9BQWQxRCxFQUtHLEtBTEhBO0FBT0g7QUFuQ0wsR0FBQUo7O0FBdUNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JzSCxtQkFBaEJ0SCxHQUFzQyxZQUFXO0FBRTdDLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBY3FNLFVBQWQ7QUFBQSxRQUEwQnNILGlCQUExQjtBQUFBLFFBQTZDQyxDQUE3QztBQUFBLFFBQ0lDLGtCQUFrQixHQUFHN1QsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVrQyxVQUFWbEMsSUFBd0IsSUFEakQ7O0FBR0EsUUFBSzFDLENBQUMsQ0FBQ3dXLElBQUZ4VyxDQUFPdVcsa0JBQVB2VyxNQUErQixPQUEvQkEsSUFBMEN1VyxrQkFBa0IsQ0FBQy9MLE1BQWxFLEVBQTJFO0FBRXZFOUgsTUFBQUEsQ0FBQyxDQUFDaUMsU0FBRmpDLEdBQWNBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVaUMsU0FBVmpDLElBQXVCLFFBQXJDQTs7QUFFQSxXQUFNcU0sVUFBTixJQUFvQndILGtCQUFwQixFQUF5QztBQUVyQ0QsUUFBQUEsQ0FBQyxHQUFHNVQsQ0FBQyxDQUFDZ0YsV0FBRmhGLENBQWM4SCxNQUFkOUgsR0FBcUIsQ0FBekI0VDtBQUNBRCxRQUFBQSxpQkFBaUIsR0FBR0Usa0JBQWtCLENBQUN4SCxVQUFELENBQWxCd0gsQ0FBK0J4SCxVQUFuRHNIOztBQUVBLFlBQUlFLGtCQUFrQixDQUFDakgsY0FBbkJpSCxDQUFrQ3hILFVBQWxDd0gsQ0FBSixFQUFtRDtBQUUvQztBQUNBO0FBQ0EsaUJBQU9ELENBQUMsSUFBSSxDQUFaLEVBQWdCO0FBQ1osZ0JBQUk1VCxDQUFDLENBQUNnRixXQUFGaEYsQ0FBYzRULENBQWQ1VCxLQUFvQkEsQ0FBQyxDQUFDZ0YsV0FBRmhGLENBQWM0VCxDQUFkNVQsTUFBcUIyVCxpQkFBN0MsRUFBaUU7QUFDN0QzVCxjQUFBQSxDQUFDLENBQUNnRixXQUFGaEYsQ0FBYytULE1BQWQvVCxDQUFxQjRULENBQXJCNVQsRUFBdUIsQ0FBdkJBO0FBQ0g7O0FBQ0Q0VCxZQUFBQSxDQUFDO0FBQ0o7O0FBRUQ1VCxVQUFBQSxDQUFDLENBQUNnRixXQUFGaEYsQ0FBY29RLElBQWRwUSxDQUFtQjJULGlCQUFuQjNUOztBQUNBQSxVQUFBQSxDQUFDLENBQUNpRixrQkFBRmpGLENBQXFCMlQsaUJBQXJCM1QsSUFBMEM2VCxrQkFBa0IsQ0FBQ3hILFVBQUQsQ0FBbEJ3SCxDQUErQnRWLFFBQXpFeUI7QUFFSDtBQUVKOztBQUVEQSxNQUFBQSxDQUFDLENBQUNnRixXQUFGaEYsQ0FBY2dVLElBQWRoVSxDQUFtQixVQUFTcUwsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7QUFDOUIsZUFBU3RMLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVNkIsV0FBVjdCLEdBQTBCcUwsQ0FBQyxHQUFDQyxDQUE1QnRMLEdBQWdDc0wsQ0FBQyxHQUFDRCxDQUEzQztBQURKLE9BQUFyTDtBQUlIO0FBcENMLEdBQUFKOztBQXdDQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCMkksTUFBaEIzSSxHQUF5QixZQUFXO0FBRWhDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsR0FDSUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQ0tvSSxRQURMcEksQ0FDY0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVxQyxLQUR4QnJDLEVBRUt0QixRQUZMc0IsQ0FFYyxhQUZkQSxDQURKQTtBQUtBQSxJQUFBQSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVU4SCxNQUF6QjlIOztBQUVBLFFBQUlBLENBQUMsQ0FBQzBELFlBQUYxRCxJQUFrQkEsQ0FBQyxDQUFDa0UsVUFBcEJsRSxJQUFrQ0EsQ0FBQyxDQUFDMEQsWUFBRjFELEtBQW1CLENBQXpELEVBQTREO0FBQ3hEQSxNQUFBQSxDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUJBLENBQUMsQ0FBQzBELFlBQUYxRCxHQUFpQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUE1Q3hDO0FBQ0g7O0FBRUQsUUFBSUEsQ0FBQyxDQUFDa0UsVUFBRmxFLElBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTlCLEVBQTRDO0FBQ3hDdkMsTUFBQUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCLENBQWpCQTtBQUNIOztBQUVEQSxJQUFBQSxDQUFDLENBQUNrSCxtQkFBRmxIOztBQUVBQSxJQUFBQSxDQUFDLENBQUNnUixRQUFGaFI7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ2lMLGFBQUZqTDs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDc0ssV0FBRnRLOztBQUNBQSxJQUFBQSxDQUFDLENBQUNvUixZQUFGcFI7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ3VSLGVBQUZ2Ujs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDMkssU0FBRjNLOztBQUNBQSxJQUFBQSxDQUFDLENBQUNrTCxVQUFGbEw7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ3dSLGFBQUZ4Ujs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDa08sa0JBQUZsTzs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDeVIsZUFBRnpSOztBQUVBQSxJQUFBQSxDQUFDLENBQUNrTSxlQUFGbE0sQ0FBa0IsS0FBbEJBLEVBQXlCLElBQXpCQTs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXlCLGFBQVZ6QixLQUE0QixJQUFoQyxFQUFzQztBQUNsQzFDLE1BQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ29FLFdBQUgsQ0FBRDlHLENBQWlCOEssUUFBakI5SyxHQUE0QjRCLEVBQTVCNUIsQ0FBK0IsYUFBL0JBLEVBQThDMEMsQ0FBQyxDQUFDNEcsYUFBaER0SjtBQUNIOztBQUVEMEMsSUFBQUEsQ0FBQyxDQUFDbUwsZUFBRm5MLENBQWtCLE9BQU9BLENBQUMsQ0FBQzBELFlBQVQsS0FBMEIsUUFBMUIsR0FBcUMxRCxDQUFDLENBQUMwRCxZQUF2QyxHQUFzRCxDQUF4RTFEOztBQUVBQSxJQUFBQSxDQUFDLENBQUM2RyxXQUFGN0c7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ2lQLFlBQUZqUDs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDc0YsTUFBRnRGLEdBQVcsQ0FBQ0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVVLFFBQXRCVjs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDc0csUUFBRnRHOztBQUVBQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixRQUFsQkEsRUFBNEIsQ0FBQ0EsQ0FBRCxDQUE1QkE7QUE5Q0osR0FBQUo7O0FBa0RBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J3TyxNQUFoQnhPLEdBQXlCLFlBQVc7QUFFaEMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSTFDLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRHZDLENBQVVRLEtBQVZSLE9BQXNCMEMsQ0FBQyxDQUFDK0YsV0FBNUIsRUFBeUM7QUFDckNrTyxNQUFBQSxZQUFZLENBQUNqVSxDQUFDLENBQUNrVSxXQUFILENBQVpEO0FBQ0FqVSxNQUFBQSxDQUFDLENBQUNrVSxXQUFGbFUsR0FBZ0JILE1BQU0sQ0FBQzhKLFVBQVA5SixDQUFrQixZQUFXO0FBQ3pDRyxRQUFBQSxDQUFDLENBQUMrRixXQUFGL0YsR0FBZ0IxQyxDQUFDLENBQUN1QyxNQUFELENBQUR2QyxDQUFVUSxLQUFWUixFQUFoQjBDOztBQUNBQSxRQUFBQSxDQUFDLENBQUNrTSxlQUFGbE07O0FBQ0EsWUFBSSxDQUFDQSxDQUFDLENBQUM0RSxTQUFQLEVBQW1CO0FBQUU1RSxVQUFBQSxDQUFDLENBQUM2RyxXQUFGN0c7QUFBa0I7QUFIM0IsT0FBQUgsRUFJYixFQUphQSxDQUFoQkc7QUFLSDtBQVhMLEdBQUFKOztBQWNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J1VSxXQUFoQnZVLEdBQThCQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J3VSxXQUFoQnhVLEdBQThCLFVBQVMrSCxLQUFULEVBQWdCME0sWUFBaEIsRUFBOEJDLFNBQTlCLEVBQXlDO0FBRWpHLFFBQUl0VSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJLE9BQU8ySCxLQUFQLEtBQWtCLFNBQXRCLEVBQWlDO0FBQzdCME0sTUFBQUEsWUFBWSxHQUFHMU0sS0FBZjBNO0FBQ0ExTSxNQUFBQSxLQUFLLEdBQUcwTSxZQUFZLEtBQUssSUFBakJBLEdBQXdCLENBQXhCQSxHQUE0QnJVLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlLENBQW5EMkg7QUFGSixLQUFBLE1BR087QUFDSEEsTUFBQUEsS0FBSyxHQUFHME0sWUFBWSxLQUFLLElBQWpCQSxHQUF3QixFQUFFMU0sS0FBMUIwTSxHQUFrQzFNLEtBQTFDQTtBQUNIOztBQUVELFFBQUkzSCxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZSxDQUFmQSxJQUFvQjJILEtBQUssR0FBRyxDQUE1QjNILElBQWlDMkgsS0FBSyxHQUFHM0gsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWUsQ0FBNUQsRUFBK0Q7QUFDM0QsYUFBTyxLQUFQO0FBQ0g7O0FBRURBLElBQUFBLENBQUMsQ0FBQzZILE1BQUY3SDs7QUFFQSxRQUFJc1UsU0FBUyxLQUFLLElBQWxCLEVBQXdCO0FBQ3BCdFUsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNvSSxRQUFkcEksR0FBeUJ5TyxNQUF6QnpPO0FBREosS0FBQSxNQUVPO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjb0ksUUFBZHBJLENBQXVCLEtBQUt2QyxPQUFMLENBQWE0RSxLQUFwQ3JDLEVBQTJDaUksRUFBM0NqSSxDQUE4QzJILEtBQTlDM0gsRUFBcUR5TyxNQUFyRHpPO0FBQ0g7O0FBRURBLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxHQUFZQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixLQUFLdkMsT0FBTCxDQUFhNEUsS0FBcENyQyxDQUFaQTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNvSSxRQUFkcEksQ0FBdUIsS0FBS3ZDLE9BQUwsQ0FBYTRFLEtBQXBDckMsRUFBMkNxSSxNQUEzQ3JJOztBQUVBQSxJQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY3NJLE1BQWR0SSxDQUFxQkEsQ0FBQyxDQUFDcUUsT0FBdkJyRTs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMkYsWUFBRjNGLEdBQWlCQSxDQUFDLENBQUNxRSxPQUFuQnJFOztBQUVBQSxJQUFBQSxDQUFDLENBQUN1SSxNQUFGdkk7QUEvQkosR0FBQUo7O0FBbUNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0IyVSxNQUFoQjNVLEdBQXlCLFVBQVM0VSxRQUFULEVBQW1CO0FBRXhDLFFBQUl4VSxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l5VSxhQUFhLEdBQUcsRUFEcEI7QUFBQSxRQUVJQyxDQUZKO0FBQUEsUUFFT0MsQ0FGUDs7QUFJQSxRQUFJM1UsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVvQyxHQUFWcEMsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEJ3VSxNQUFBQSxRQUFRLEdBQUcsQ0FBQ0EsUUFBWkE7QUFDSDs7QUFDREUsSUFBQUEsQ0FBQyxHQUFHMVUsQ0FBQyxDQUFDdUYsWUFBRnZGLElBQWtCLE1BQWxCQSxHQUEyQnNKLElBQUksQ0FBQ0MsSUFBTEQsQ0FBVWtMLFFBQVZsTCxJQUFzQixJQUFqRHRKLEdBQXdELEtBQTVEMFU7QUFDQUMsSUFBQUEsQ0FBQyxHQUFHM1UsQ0FBQyxDQUFDdUYsWUFBRnZGLElBQWtCLEtBQWxCQSxHQUEwQnNKLElBQUksQ0FBQ0MsSUFBTEQsQ0FBVWtMLFFBQVZsTCxJQUFzQixJQUFoRHRKLEdBQXVELEtBQTNEMlU7QUFFQUYsSUFBQUEsYUFBYSxDQUFDelUsQ0FBQyxDQUFDdUYsWUFBSCxDQUFia1AsR0FBZ0NELFFBQWhDQzs7QUFFQSxRQUFJelUsQ0FBQyxDQUFDMkUsaUJBQUYzRSxLQUF3QixLQUE1QixFQUFtQztBQUMvQkEsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQixHQUFkaUIsQ0FBa0J5VSxhQUFsQnpVO0FBREosS0FBQSxNQUVPO0FBQ0h5VSxNQUFBQSxhQUFhLEdBQUcsRUFBaEJBOztBQUNBLFVBQUl6VSxDQUFDLENBQUNrRixjQUFGbEYsS0FBcUIsS0FBekIsRUFBZ0M7QUFDNUJ5VSxRQUFBQSxhQUFhLENBQUN6VSxDQUFDLENBQUM4RSxRQUFILENBQWIyUCxHQUE0QixlQUFlQyxDQUFmLEdBQW1CLElBQW5CLEdBQTBCQyxDQUExQixHQUE4QixHQUExREY7O0FBQ0F6VSxRQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY2pCLEdBQWRpQixDQUFrQnlVLGFBQWxCelU7QUFGSixPQUFBLE1BR087QUFDSHlVLFFBQUFBLGFBQWEsQ0FBQ3pVLENBQUMsQ0FBQzhFLFFBQUgsQ0FBYjJQLEdBQTRCLGlCQUFpQkMsQ0FBakIsR0FBcUIsSUFBckIsR0FBNEJDLENBQTVCLEdBQWdDLFFBQTVERjs7QUFDQXpVLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUZwRSxDQUFjakIsR0FBZGlCLENBQWtCeVUsYUFBbEJ6VTtBQUNIO0FBQ0o7QUF6QkwsR0FBQUo7O0FBNkJBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JnVixhQUFoQmhWLEdBQWdDLFlBQVc7QUFFdkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVnRCxRQUFWaEQsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUIsVUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVZLFVBQVZaLEtBQXlCLElBQTdCLEVBQW1DO0FBQy9CQSxRQUFBQSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUWpCLEdBQVJpQixDQUFZO0FBQ1I2VSxVQUFBQSxPQUFPLEVBQUcsU0FBUzdVLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVYTtBQURyQixTQUFaYjtBQUdIO0FBTEwsS0FBQSxNQU1PO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRakMsTUFBUmlDLENBQWVBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVOEssS0FBVjlLLEdBQWtCMEksV0FBbEIxSSxDQUE4QixJQUE5QkEsSUFBc0NBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBL0R2Qzs7QUFDQSxVQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVksVUFBVlosS0FBeUIsSUFBN0IsRUFBbUM7QUFDL0JBLFFBQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRakIsR0FBUmlCLENBQVk7QUFDUjZVLFVBQUFBLE9BQU8sRUFBRzdVLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVYSxhQUFWYixHQUEwQjtBQUQ1QixTQUFaQTtBQUdIO0FBQ0o7O0FBRURBLElBQUFBLENBQUMsQ0FBQzZELFNBQUY3RCxHQUFjQSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUWxDLEtBQVJrQyxFQUFkQTtBQUNBQSxJQUFBQSxDQUFDLENBQUM4RCxVQUFGOUQsR0FBZUEsQ0FBQyxDQUFDeUUsS0FBRnpFLENBQVFqQyxNQUFSaUMsRUFBZkE7O0FBR0EsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVnRCxRQUFWaEQsS0FBdUIsS0FBdkJBLElBQWdDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVStDLGFBQVYvQyxLQUE0QixLQUFoRSxFQUF1RTtBQUNuRUEsTUFBQUEsQ0FBQyxDQUFDbUUsVUFBRm5FLEdBQWVzSixJQUFJLENBQUNDLElBQUxELENBQVV0SixDQUFDLENBQUM2RCxTQUFGN0QsR0FBY0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFsQytHLENBQWZ0Sjs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNsQyxLQUFka0MsQ0FBb0JzSixJQUFJLENBQUNDLElBQUxELENBQVd0SixDQUFDLENBQUNtRSxVQUFGbkUsR0FBZUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNvSSxRQUFkcEksQ0FBdUIsY0FBdkJBLEVBQXVDOEgsTUFBakV3QixDQUFwQnRKO0FBRkosS0FBQSxNQUlPLElBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVK0MsYUFBVi9DLEtBQTRCLElBQWhDLEVBQXNDO0FBQ3pDQSxNQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY2xDLEtBQWRrQyxDQUFvQixPQUFPQSxDQUFDLENBQUNrRSxVQUE3QmxFO0FBREcsS0FBQSxNQUVBO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQ21FLFVBQUZuRSxHQUFlc0osSUFBSSxDQUFDQyxJQUFMRCxDQUFVdEosQ0FBQyxDQUFDNkQsU0FBWnlGLENBQWZ0Sjs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRnBFLENBQWNqQyxNQUFkaUMsQ0FBcUJzSixJQUFJLENBQUNDLElBQUxELENBQVd0SixDQUFDLENBQUNxRSxPQUFGckUsQ0FBVThLLEtBQVY5SyxHQUFrQjBJLFdBQWxCMUksQ0FBOEIsSUFBOUJBLElBQXNDQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixjQUF2QkEsRUFBdUM4SCxNQUF4RndCLENBQXJCdEo7QUFDSDs7QUFFRCxRQUFJOFUsTUFBTSxHQUFHOVUsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVU4SyxLQUFWOUssR0FBa0I4UCxVQUFsQjlQLENBQTZCLElBQTdCQSxJQUFxQ0EsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVU4SyxLQUFWOUssR0FBa0JsQyxLQUFsQmtDLEVBQWxEOztBQUNBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVK0MsYUFBVi9DLEtBQTRCLEtBQWhDLEVBQXVDQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixjQUF2QkEsRUFBdUNsQyxLQUF2Q2tDLENBQTZDQSxDQUFDLENBQUNtRSxVQUFGbkUsR0FBZThVLE1BQTVEOVU7QUFuQzNDLEdBQUFKOztBQXVDQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCbVYsT0FBaEJuVixHQUEwQixZQUFXO0FBRWpDLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSTZJLFVBREo7O0FBR0E3SSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FBVXBCLElBQVZvQixDQUFlLFVBQVMySCxLQUFULEVBQWdCNUgsT0FBaEIsRUFBeUI7QUFDcEM4SSxNQUFBQSxVQUFVLEdBQUk3SSxDQUFDLENBQUNtRSxVQUFGbkUsR0FBZTJILEtBQWYzSCxHQUF3QixDQUFDLENBQXZDNkk7O0FBQ0EsVUFBSTdJLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVb0MsR0FBVnBDLEtBQWtCLElBQXRCLEVBQTRCO0FBQ3hCMUMsUUFBQUEsQ0FBQyxDQUFDeUMsT0FBRCxDQUFEekMsQ0FBV3lCLEdBQVh6QixDQUFlO0FBQ1hrWCxVQUFBQSxRQUFRLEVBQUUsVUFEQztBQUVYUSxVQUFBQSxLQUFLLEVBQUVuTSxVQUZJO0FBR1hJLFVBQUFBLEdBQUcsRUFBRSxDQUhNO0FBSVg5RixVQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1ELE1BQVZuRCxHQUFtQixDQUpoQjtBQUtYNE8sVUFBQUEsT0FBTyxFQUFFO0FBTEUsU0FBZnRSO0FBREosT0FBQSxNQVFPO0FBQ0hBLFFBQUFBLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRHpDLENBQVd5QixHQUFYekIsQ0FBZTtBQUNYa1gsVUFBQUEsUUFBUSxFQUFFLFVBREM7QUFFWHhMLFVBQUFBLElBQUksRUFBRUgsVUFGSztBQUdYSSxVQUFBQSxHQUFHLEVBQUUsQ0FITTtBQUlYOUYsVUFBQUEsTUFBTSxFQUFFbkQsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVtRCxNQUFWbkQsR0FBbUIsQ0FKaEI7QUFLWDRPLFVBQUFBLE9BQU8sRUFBRTtBQUxFLFNBQWZ0UjtBQU9IO0FBbEJMLEtBQUEwQzs7QUFxQkFBLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVaUksRUFBVmpJLENBQWFBLENBQUMsQ0FBQzBELFlBQWYxRCxFQUE2QmpCLEdBQTdCaUIsQ0FBaUM7QUFDN0JtRCxNQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1ELE1BQVZuRCxHQUFtQixDQURFO0FBRTdCNE8sTUFBQUEsT0FBTyxFQUFFO0FBRm9CLEtBQWpDNU87QUExQkosR0FBQUo7O0FBaUNBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JxVixTQUFoQnJWLEdBQTRCLFlBQVc7QUFFbkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsS0FBMkIsQ0FBM0JBLElBQWdDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVUcsY0FBVkgsS0FBNkIsSUFBN0RBLElBQXFFQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWdELFFBQVZoRCxLQUF1QixLQUFoRyxFQUF1RztBQUNuRyxVQUFJeUksWUFBWSxHQUFHekksQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVVpSSxFQUFWakksQ0FBYUEsQ0FBQyxDQUFDMEQsWUFBZjFELEVBQTZCMEksV0FBN0IxSSxDQUF5QyxJQUF6Q0EsQ0FBbkI7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUZ6RSxDQUFRakIsR0FBUmlCLENBQVksUUFBWkEsRUFBc0J5SSxZQUF0QnpJO0FBQ0g7QUFQTCxHQUFBSjs7QUFXQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCc1YsU0FBaEJ0VixHQUNBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0J1VixjQUFoQnZWLEdBQWlDLFlBQVc7QUFFeEM7Ozs7Ozs7Ozs7OztBQWFBLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBYzRULENBQWQ7QUFBQSxRQUFpQndCLElBQWpCO0FBQUEsUUFBdUJuRixNQUF2QjtBQUFBLFFBQStCb0YsS0FBL0I7QUFBQSxRQUFzQ3ZJLE9BQU8sR0FBRyxLQUFoRDtBQUFBLFFBQXVEZ0gsSUFBdkQ7O0FBRUEsUUFBSXhXLENBQUMsQ0FBQ3dXLElBQUZ4VyxDQUFRZ1ksU0FBUyxDQUFDLENBQUQsQ0FBakJoWSxNQUEyQixRQUEvQixFQUEwQztBQUV0QzJTLE1BQUFBLE1BQU0sR0FBSXFGLFNBQVMsQ0FBQyxDQUFELENBQW5CckY7QUFDQW5ELE1BQUFBLE9BQU8sR0FBR3dJLFNBQVMsQ0FBQyxDQUFELENBQW5CeEk7QUFDQWdILE1BQUFBLElBQUksR0FBRyxVQUFQQTtBQUpKLEtBQUEsTUFNTyxJQUFLeFcsQ0FBQyxDQUFDd1csSUFBRnhXLENBQVFnWSxTQUFTLENBQUMsQ0FBRCxDQUFqQmhZLE1BQTJCLFFBQWhDLEVBQTJDO0FBRTlDMlMsTUFBQUEsTUFBTSxHQUFJcUYsU0FBUyxDQUFDLENBQUQsQ0FBbkJyRjtBQUNBb0YsTUFBQUEsS0FBSyxHQUFHQyxTQUFTLENBQUMsQ0FBRCxDQUFqQkQ7QUFDQXZJLE1BQUFBLE9BQU8sR0FBR3dJLFNBQVMsQ0FBQyxDQUFELENBQW5CeEk7O0FBRUEsVUFBS3dJLFNBQVMsQ0FBQyxDQUFELENBQVRBLEtBQWlCLFlBQWpCQSxJQUFpQ2hZLENBQUMsQ0FBQ3dXLElBQUZ4VyxDQUFRZ1ksU0FBUyxDQUFDLENBQUQsQ0FBakJoWSxNQUEyQixPQUFqRSxFQUEyRTtBQUV2RXdXLFFBQUFBLElBQUksR0FBRyxZQUFQQTtBQUZKLE9BQUEsTUFJTyxJQUFLLE9BQU93QixTQUFTLENBQUMsQ0FBRCxDQUFoQixLQUF3QixXQUE3QixFQUEyQztBQUU5Q3hCLFFBQUFBLElBQUksR0FBRyxRQUFQQTtBQUVIO0FBRUo7O0FBRUQsUUFBS0EsSUFBSSxLQUFLLFFBQWQsRUFBeUI7QUFFckI5VCxNQUFBQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWlRLE1BQVZqUSxJQUFvQnFWLEtBQXBCclY7QUFGSixLQUFBLE1BS08sSUFBSzhULElBQUksS0FBSyxVQUFkLEVBQTJCO0FBRTlCeFcsTUFBQUEsQ0FBQyxDQUFDc0IsSUFBRnRCLENBQVEyUyxNQUFSM1MsRUFBaUIsVUFBVWlZLEdBQVYsRUFBZUMsR0FBZixFQUFxQjtBQUVsQ3hWLFFBQUFBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdVYsR0FBVnZWLElBQWlCd1YsR0FBakJ4VjtBQUZKLE9BQUExQztBQUZHLEtBQUEsTUFTQSxJQUFLd1csSUFBSSxLQUFLLFlBQWQsRUFBNkI7QUFFaEMsV0FBTXNCLElBQU4sSUFBY0MsS0FBZCxFQUFzQjtBQUVsQixZQUFJL1gsQ0FBQyxDQUFDd1csSUFBRnhXLENBQVEwQyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWtDLFVBQWxCNUUsTUFBbUMsT0FBdkMsRUFBaUQ7QUFFN0MwQyxVQUFBQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWtDLFVBQVZsQyxHQUF1QixDQUFFcVYsS0FBSyxDQUFDRCxJQUFELENBQVAsQ0FBdkJwVjtBQUZKLFNBQUEsTUFJTztBQUVINFQsVUFBQUEsQ0FBQyxHQUFHNVQsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVrQyxVQUFWbEMsQ0FBcUI4SCxNQUFyQjlILEdBQTRCLENBQWhDNFQsQ0FGRyxDQUlIOztBQUNBLGlCQUFPQSxDQUFDLElBQUksQ0FBWixFQUFnQjtBQUVaLGdCQUFJNVQsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVrQyxVQUFWbEMsQ0FBcUI0VCxDQUFyQjVULEVBQXdCcU0sVUFBeEJyTSxLQUF1Q3FWLEtBQUssQ0FBQ0QsSUFBRCxDQUFMQyxDQUFZaEosVUFBdkQsRUFBb0U7QUFFaEVyTSxjQUFBQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWtDLFVBQVZsQyxDQUFxQitULE1BQXJCL1QsQ0FBNEI0VCxDQUE1QjVULEVBQThCLENBQTlCQTtBQUVIOztBQUVENFQsWUFBQUEsQ0FBQztBQUVKOztBQUVENVQsVUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVrQyxVQUFWbEMsQ0FBcUJvUSxJQUFyQnBRLENBQTJCcVYsS0FBSyxDQUFDRCxJQUFELENBQWhDcFY7QUFFSDtBQUVKO0FBRUo7O0FBRUQsUUFBSzhNLE9BQUwsRUFBZTtBQUVYOU0sTUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjdIOztBQUNBQSxNQUFBQSxDQUFDLENBQUN1SSxNQUFGdkk7QUFFSDtBQTlGTCxHQUFBSjs7QUFrR0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmlILFdBQWhCakgsR0FBOEIsWUFBVztBQUVyQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDNFUsYUFBRjVVOztBQUVBQSxJQUFBQSxDQUFDLENBQUNpVixTQUFGalY7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QixJQUFWeEIsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJBLE1BQUFBLENBQUMsQ0FBQ3VVLE1BQUZ2VSxDQUFTQSxDQUFDLENBQUN3UCxPQUFGeFAsQ0FBVUEsQ0FBQyxDQUFDMEQsWUFBWjFELENBQVRBO0FBREosS0FBQSxNQUVPO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQytVLE9BQUYvVTtBQUNIOztBQUVEQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixhQUFsQkEsRUFBaUMsQ0FBQ0EsQ0FBRCxDQUFqQ0E7QUFkSixHQUFBSjs7QUFrQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQm9SLFFBQWhCcFIsR0FBMkIsWUFBVztBQUVsQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l5VixTQUFTLEdBQUd0UCxRQUFRLENBQUN1UCxJQUFUdlAsQ0FBY3dQLEtBRDlCOztBQUdBM1YsSUFBQUEsQ0FBQyxDQUFDdUYsWUFBRnZGLEdBQWlCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWdELFFBQVZoRCxLQUF1QixJQUF2QkEsR0FBOEIsS0FBOUJBLEdBQXNDLE1BQXZEQTs7QUFFQSxRQUFJQSxDQUFDLENBQUN1RixZQUFGdkYsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJBLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVdEIsUUFBVnNCLENBQW1CLGdCQUFuQkE7QUFESixLQUFBLE1BRU87QUFDSEEsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVV1SyxXQUFWdkssQ0FBc0IsZ0JBQXRCQTtBQUNIOztBQUVELFFBQUl5VixTQUFTLENBQUNHLGdCQUFWSCxLQUErQkksU0FBL0JKLElBQ0FBLFNBQVMsQ0FBQ0ssYUFBVkwsS0FBNEJJLFNBRDVCSixJQUVBQSxTQUFTLENBQUNNLFlBQVZOLEtBQTJCSSxTQUYvQixFQUUwQztBQUN0QyxVQUFJN1YsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVU2QyxNQUFWN0MsS0FBcUIsSUFBekIsRUFBK0I7QUFDM0JBLFFBQUFBLENBQUMsQ0FBQ2tGLGNBQUZsRixHQUFtQixJQUFuQkE7QUFDSDtBQUNKOztBQUVELFFBQUtBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0IsSUFBZixFQUFzQjtBQUNsQixVQUFLLE9BQU94QixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1ELE1BQWpCLEtBQTRCLFFBQWpDLEVBQTRDO0FBQ3hDLFlBQUluRCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1ELE1BQVZuRCxHQUFtQixDQUF2QixFQUEyQjtBQUN2QkEsVUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVtRCxNQUFWbkQsR0FBbUIsQ0FBbkJBO0FBQ0g7QUFITCxPQUFBLE1BSU87QUFDSEEsUUFBQUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVtRCxNQUFWbkQsR0FBbUJBLENBQUMsQ0FBQ3RDLFFBQUZzQyxDQUFXbUQsTUFBOUJuRDtBQUNIO0FBQ0o7O0FBRUQsUUFBSXlWLFNBQVMsQ0FBQ08sVUFBVlAsS0FBeUJJLFNBQTdCLEVBQXdDO0FBQ3BDN1YsTUFBQUEsQ0FBQyxDQUFDOEUsUUFBRjlFLEdBQWEsWUFBYkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNEYsYUFBRjVGLEdBQWtCLGNBQWxCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUM2RixjQUFGN0YsR0FBbUIsYUFBbkJBO0FBQ0EsVUFBSXlWLFNBQVMsQ0FBQ1EsbUJBQVZSLEtBQWtDSSxTQUFsQ0osSUFBK0NBLFNBQVMsQ0FBQ1MsaUJBQVZULEtBQWdDSSxTQUFuRixFQUE4RjdWLENBQUMsQ0FBQzhFLFFBQUY5RSxHQUFhLEtBQWJBO0FBQ2pHOztBQUNELFFBQUl5VixTQUFTLENBQUNVLFlBQVZWLEtBQTJCSSxTQUEvQixFQUEwQztBQUN0QzdWLE1BQUFBLENBQUMsQ0FBQzhFLFFBQUY5RSxHQUFhLGNBQWJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzRGLGFBQUY1RixHQUFrQixnQkFBbEJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzZGLGNBQUY3RixHQUFtQixlQUFuQkE7QUFDQSxVQUFJeVYsU0FBUyxDQUFDUSxtQkFBVlIsS0FBa0NJLFNBQWxDSixJQUErQ0EsU0FBUyxDQUFDVyxjQUFWWCxLQUE2QkksU0FBaEYsRUFBMkY3VixDQUFDLENBQUM4RSxRQUFGOUUsR0FBYSxLQUFiQTtBQUM5Rjs7QUFDRCxRQUFJeVYsU0FBUyxDQUFDWSxlQUFWWixLQUE4QkksU0FBbEMsRUFBNkM7QUFDekM3VixNQUFBQSxDQUFDLENBQUM4RSxRQUFGOUUsR0FBYSxpQkFBYkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNEYsYUFBRjVGLEdBQWtCLG1CQUFsQkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRjdGLEdBQW1CLGtCQUFuQkE7QUFDQSxVQUFJeVYsU0FBUyxDQUFDUSxtQkFBVlIsS0FBa0NJLFNBQWxDSixJQUErQ0EsU0FBUyxDQUFDUyxpQkFBVlQsS0FBZ0NJLFNBQW5GLEVBQThGN1YsQ0FBQyxDQUFDOEUsUUFBRjlFLEdBQWEsS0FBYkE7QUFDakc7O0FBQ0QsUUFBSXlWLFNBQVMsQ0FBQ2EsV0FBVmIsS0FBMEJJLFNBQTlCLEVBQXlDO0FBQ3JDN1YsTUFBQUEsQ0FBQyxDQUFDOEUsUUFBRjlFLEdBQWEsYUFBYkE7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNEYsYUFBRjVGLEdBQWtCLGVBQWxCQTtBQUNBQSxNQUFBQSxDQUFDLENBQUM2RixjQUFGN0YsR0FBbUIsY0FBbkJBO0FBQ0EsVUFBSXlWLFNBQVMsQ0FBQ2EsV0FBVmIsS0FBMEJJLFNBQTlCLEVBQXlDN1YsQ0FBQyxDQUFDOEUsUUFBRjlFLEdBQWEsS0FBYkE7QUFDNUM7O0FBQ0QsUUFBSXlWLFNBQVMsQ0FBQ2MsU0FBVmQsS0FBd0JJLFNBQXhCSixJQUFxQ3pWLENBQUMsQ0FBQzhFLFFBQUY5RSxLQUFlLEtBQXhELEVBQStEO0FBQzNEQSxNQUFBQSxDQUFDLENBQUM4RSxRQUFGOUUsR0FBYSxXQUFiQTtBQUNBQSxNQUFBQSxDQUFDLENBQUM0RixhQUFGNUYsR0FBa0IsV0FBbEJBO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzZGLGNBQUY3RixHQUFtQixZQUFuQkE7QUFDSDs7QUFDREEsSUFBQUEsQ0FBQyxDQUFDMkUsaUJBQUYzRSxHQUFzQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVU4QyxZQUFWOUMsSUFBMkJBLENBQUMsQ0FBQzhFLFFBQUY5RSxLQUFlLElBQTFDQSxJQUFrREEsQ0FBQyxDQUFDOEUsUUFBRjlFLEtBQWUsS0FBdkZBO0FBNURKLEdBQUFKOztBQWdFQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCdUwsZUFBaEJ2TCxHQUFrQyxVQUFTK0gsS0FBVCxFQUFnQjtBQUU5QyxRQUFJM0gsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJeVEsWUFESjtBQUFBLFFBQ2tCK0YsU0FEbEI7QUFBQSxRQUM2QnBKLFdBRDdCO0FBQUEsUUFDMENxSixTQUQxQzs7QUFHQUQsSUFBQUEsU0FBUyxHQUFHeFcsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQ1BzSCxJQURPdEgsQ0FDRixjQURFQSxFQUVQdUssV0FGT3ZLLENBRUsseUNBRkxBLEVBR1B1SCxJQUhPdkgsQ0FHRixhQUhFQSxFQUdhLE1BSGJBLENBQVp3Vzs7QUFLQXhXLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUNLaUksRUFETGpJLENBQ1EySCxLQURSM0gsRUFFS3RCLFFBRkxzQixDQUVjLGVBRmRBOztBQUlBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVWSxVQUFWWixLQUF5QixJQUE3QixFQUFtQztBQUUvQnlRLE1BQUFBLFlBQVksR0FBR25ILElBQUksQ0FBQ3NHLEtBQUx0RyxDQUFXdEosQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIsQ0FBcENzSixDQUFmbUg7O0FBRUEsVUFBSXpRLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLElBQTNCLEVBQWlDO0FBRTdCLFlBQUkySCxLQUFLLElBQUk4SSxZQUFUOUksSUFBeUJBLEtBQUssSUFBSzNILENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlLENBQWZBLEdBQW9CeVEsWUFBM0QsRUFBeUU7QUFFckV6USxVQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FDSzRTLEtBREw1UyxDQUNXMkgsS0FBSyxHQUFHOEksWUFEbkJ6USxFQUNpQzJILEtBQUssR0FBRzhJLFlBQVI5SSxHQUF1QixDQUR4RDNILEVBRUt0QixRQUZMc0IsQ0FFYyxjQUZkQSxFQUdLdUgsSUFITHZILENBR1UsYUFIVkEsRUFHeUIsT0FIekJBO0FBRkosU0FBQSxNQU9PO0FBRUhvTixVQUFBQSxXQUFXLEdBQUdwTixDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQVZ2QyxHQUF5QjJILEtBQXZDeUY7QUFDQW9KLFVBQUFBLFNBQVMsQ0FDSjVELEtBREw0RCxDQUNXcEosV0FBVyxHQUFHcUQsWUFBZHJELEdBQTZCLENBRHhDb0osRUFDMkNwSixXQUFXLEdBQUdxRCxZQUFkckQsR0FBNkIsQ0FEeEVvSixFQUVLOVgsUUFGTDhYLENBRWMsY0FGZEEsRUFHS2pQLElBSExpUCxDQUdVLGFBSFZBLEVBR3lCLE9BSHpCQTtBQUtIOztBQUVELFlBQUk3TyxLQUFLLEtBQUssQ0FBZCxFQUFpQjtBQUViNk8sVUFBQUEsU0FBUyxDQUNKdk8sRUFETHVPLENBQ1FBLFNBQVMsQ0FBQzFPLE1BQVYwTyxHQUFtQixDQUFuQkEsR0FBdUJ4VyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBRHpDaVUsRUFFSzlYLFFBRkw4WCxDQUVjLGNBRmRBO0FBRkosU0FBQSxNQU1PLElBQUk3TyxLQUFLLEtBQUszSCxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZSxDQUE3QixFQUFnQztBQUVuQ3dXLFVBQUFBLFNBQVMsQ0FDSnZPLEVBREx1TyxDQUNReFcsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQURsQmlVLEVBRUs5WCxRQUZMOFgsQ0FFYyxjQUZkQTtBQUlIO0FBRUo7O0FBRUR4VyxNQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FDS2lJLEVBRExqSSxDQUNRMkgsS0FEUjNILEVBRUt0QixRQUZMc0IsQ0FFYyxjQUZkQTtBQXZDSixLQUFBLE1BMkNPO0FBRUgsVUFBSTJILEtBQUssSUFBSSxDQUFUQSxJQUFjQSxLQUFLLElBQUszSCxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFyRCxFQUFvRTtBQUVoRXZDLFFBQUFBLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUNLNFMsS0FETDVTLENBQ1cySCxLQURYM0gsRUFDa0IySCxLQUFLLEdBQUczSCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBRHBDdkMsRUFFS3RCLFFBRkxzQixDQUVjLGNBRmRBLEVBR0t1SCxJQUhMdkgsQ0FHVSxhQUhWQSxFQUd5QixPQUh6QkE7QUFGSixPQUFBLE1BT08sSUFBSXdXLFNBQVMsQ0FBQzFPLE1BQVYwTyxJQUFvQnhXLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBbEMsRUFBZ0Q7QUFFbkRpVSxRQUFBQSxTQUFTLENBQ0o5WCxRQURMOFgsQ0FDYyxjQURkQSxFQUVLalAsSUFGTGlQLENBRVUsYUFGVkEsRUFFeUIsT0FGekJBO0FBRkcsT0FBQSxNQU1BO0FBRUhDLFFBQUFBLFNBQVMsR0FBR3pXLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXJDa1U7QUFDQXJKLFFBQUFBLFdBQVcsR0FBR3BOLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLElBQXZCQSxHQUE4QkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIySCxLQUF2RDNILEdBQStEMkgsS0FBN0V5Rjs7QUFFQSxZQUFJcE4sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsSUFBMEJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBcEN4QyxJQUF1REEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWUySCxLQUFmM0gsR0FBd0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBN0YsRUFBMkc7QUFFdkdpVSxVQUFBQSxTQUFTLENBQ0o1RCxLQURMNEQsQ0FDV3BKLFdBQVcsSUFBSXBOLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBVnZDLEdBQXlCeVcsU0FBN0IsQ0FEdEJELEVBQytEcEosV0FBVyxHQUFHcUosU0FEN0VELEVBRUs5WCxRQUZMOFgsQ0FFYyxjQUZkQSxFQUdLalAsSUFITGlQLENBR1UsYUFIVkEsRUFHeUIsT0FIekJBO0FBRkosU0FBQSxNQU9PO0FBRUhBLFVBQUFBLFNBQVMsQ0FDSjVELEtBREw0RCxDQUNXcEosV0FEWG9KLEVBQ3dCcEosV0FBVyxHQUFHcE4sQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQURoRGlVLEVBRUs5WCxRQUZMOFgsQ0FFYyxjQUZkQSxFQUdLalAsSUFITGlQLENBR1UsYUFIVkEsRUFHeUIsT0FIekJBO0FBS0g7QUFFSjtBQUVKOztBQUVELFFBQUl4VyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTRCLFFBQVY1QixLQUF1QixVQUEzQixFQUF1QztBQUNuQ0EsTUFBQUEsQ0FBQyxDQUFDNEIsUUFBRjVCO0FBQ0g7QUFuR0wsR0FBQUo7O0FBdUdBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JxTCxhQUFoQnJMLEdBQWdDLFlBQVc7QUFFdkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJaUIsQ0FESjtBQUFBLFFBQ08wTixVQURQO0FBQUEsUUFDbUIrSCxhQURuQjs7QUFHQSxRQUFJMVcsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QixJQUFWeEIsS0FBbUIsSUFBdkIsRUFBNkI7QUFDekJBLE1BQUFBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVWSxVQUFWWixHQUF1QixLQUF2QkE7QUFDSDs7QUFFRCxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBCLFFBQVYxQixLQUF1QixJQUF2QkEsSUFBK0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0IsSUFBVnhCLEtBQW1CLEtBQXRELEVBQTZEO0FBRXpEMk8sTUFBQUEsVUFBVSxHQUFHLElBQWJBOztBQUVBLFVBQUkzTyxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUE3QixFQUEyQztBQUV2QyxZQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVZLFVBQVZaLEtBQXlCLElBQTdCLEVBQW1DO0FBQy9CMFcsVUFBQUEsYUFBYSxHQUFHMVcsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIsQ0FBekMwVztBQURKLFNBQUEsTUFFTztBQUNIQSxVQUFBQSxhQUFhLEdBQUcxVyxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQTFCbVU7QUFDSDs7QUFFRCxhQUFLelYsQ0FBQyxHQUFHakIsQ0FBQyxDQUFDa0UsVUFBWCxFQUF1QmpELENBQUMsR0FBSWpCLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUNwQjBXLGFBRFIsRUFDd0J6VixDQUFDLElBQUksQ0FEN0IsRUFDZ0M7QUFDNUIwTixVQUFBQSxVQUFVLEdBQUcxTixDQUFDLEdBQUcsQ0FBakIwTjtBQUNBclIsVUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDcUUsT0FBRnJFLENBQVUyTyxVQUFWM08sQ0FBRCxDQUFEMUMsQ0FBeUJxWixLQUF6QnJaLENBQStCLElBQS9CQSxFQUFxQ2lLLElBQXJDakssQ0FBMEMsSUFBMUNBLEVBQWdELEVBQWhEQSxFQUNLaUssSUFETGpLLENBQ1Usa0JBRFZBLEVBQzhCcVIsVUFBVSxHQUFHM08sQ0FBQyxDQUFDa0UsVUFEN0M1RyxFQUVLNkssU0FGTDdLLENBRWUwQyxDQUFDLENBQUNvRSxXQUZqQjlHLEVBRThCb0IsUUFGOUJwQixDQUV1QyxjQUZ2Q0E7QUFHSDs7QUFDRCxhQUFLMkQsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHeVYsYUFBaEIsRUFBK0J6VixDQUFDLElBQUksQ0FBcEMsRUFBdUM7QUFDbkMwTixVQUFBQSxVQUFVLEdBQUcxTixDQUFiME47QUFDQXJSLFVBQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3FFLE9BQUZyRSxDQUFVMk8sVUFBVjNPLENBQUQsQ0FBRDFDLENBQXlCcVosS0FBekJyWixDQUErQixJQUEvQkEsRUFBcUNpSyxJQUFyQ2pLLENBQTBDLElBQTFDQSxFQUFnRCxFQUFoREEsRUFDS2lLLElBRExqSyxDQUNVLGtCQURWQSxFQUM4QnFSLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBRDdDNUcsRUFFS3lLLFFBRkx6SyxDQUVjMEMsQ0FBQyxDQUFDb0UsV0FGaEI5RyxFQUU2Qm9CLFFBRjdCcEIsQ0FFc0MsY0FGdENBO0FBR0g7O0FBQ0QwQyxRQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY3NILElBQWR0SCxDQUFtQixlQUFuQkEsRUFBb0NzSCxJQUFwQ3RILENBQXlDLE1BQXpDQSxFQUFpRHBCLElBQWpEb0IsQ0FBc0QsWUFBVztBQUM3RDFDLFVBQUFBLENBQUMsQ0FBQyxJQUFELENBQURBLENBQVFpSyxJQUFSakssQ0FBYSxJQUFiQSxFQUFtQixFQUFuQkE7QUFESixTQUFBMEM7QUFJSDtBQUVKO0FBeENMLEdBQUFKOztBQTRDQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCb08sU0FBaEJwTyxHQUE0QixVQUFVZ1gsTUFBVixFQUFtQjtBQUUzQyxRQUFJNVcsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxDQUFDNFcsTUFBTCxFQUFjO0FBQ1Y1VyxNQUFBQSxDQUFDLENBQUNzRyxRQUFGdEc7QUFDSDs7QUFDREEsSUFBQUEsQ0FBQyxDQUFDb0YsV0FBRnBGLEdBQWdCNFcsTUFBaEI1VztBQVBKLEdBQUFKOztBQVdBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JnSCxhQUFoQmhILEdBQWdDLFVBQVNvTixLQUFULEVBQWdCO0FBRTVDLFFBQUloTixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJNlcsYUFBYSxHQUNidlosQ0FBQyxDQUFDMFAsS0FBSyxDQUFDakQsTUFBUCxDQUFEek0sQ0FBZ0JnUSxFQUFoQmhRLENBQW1CLGNBQW5CQSxJQUNJQSxDQUFDLENBQUMwUCxLQUFLLENBQUNqRCxNQUFQLENBREx6TSxHQUVJQSxDQUFDLENBQUMwUCxLQUFLLENBQUNqRCxNQUFQLENBQUR6TSxDQUFnQndaLE9BQWhCeFosQ0FBd0IsY0FBeEJBLENBSFI7QUFLQSxRQUFJcUssS0FBSyxHQUFHa0osUUFBUSxDQUFDZ0csYUFBYSxDQUFDdFAsSUFBZHNQLENBQW1CLGtCQUFuQkEsQ0FBRCxDQUFwQjtBQUVBLFFBQUksQ0FBQ2xQLEtBQUwsRUFBWUEsS0FBSyxHQUFHLENBQVJBOztBQUVaLFFBQUkzSCxDQUFDLENBQUNrRSxVQUFGbEUsSUFBZ0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBOUIsRUFBNEM7QUFFeEN2QyxNQUFBQSxDQUFDLENBQUNtTCxlQUFGbkwsQ0FBa0IySCxLQUFsQjNIOztBQUNBQSxNQUFBQSxDQUFDLENBQUNPLFFBQUZQLENBQVcySCxLQUFYM0g7O0FBQ0E7QUFFSDs7QUFFREEsSUFBQUEsQ0FBQyxDQUFDaUssWUFBRmpLLENBQWUySCxLQUFmM0g7QUFyQkosR0FBQUo7O0FBeUJBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JxSyxZQUFoQnJLLEdBQStCLFVBQVMrSCxLQUFULEVBQWdCb1AsSUFBaEIsRUFBc0I5SixXQUF0QixFQUFtQztBQUU5RCxRQUFJMEMsV0FBSjtBQUFBLFFBQWlCcUgsU0FBakI7QUFBQSxRQUE0QkMsUUFBNUI7QUFBQSxRQUFzQ0MsU0FBdEM7QUFBQSxRQUFpRHJPLFVBQVUsR0FBRyxJQUE5RDtBQUFBLFFBQ0k3SSxDQUFDLEdBQUcsSUFEUjtBQUFBLFFBQ2NtWCxTQURkOztBQUdBSixJQUFBQSxJQUFJLEdBQUdBLElBQUksSUFBSSxLQUFmQTs7QUFFQSxRQUFJL1csQ0FBQyxDQUFDcUQsU0FBRnJELEtBQWdCLElBQWhCQSxJQUF3QkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVrRCxjQUFWbEQsS0FBNkIsSUFBekQsRUFBK0Q7QUFDM0Q7QUFDSDs7QUFFRCxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdCLElBQVZ4QixLQUFtQixJQUFuQkEsSUFBMkJBLENBQUMsQ0FBQzBELFlBQUYxRCxLQUFtQjJILEtBQWxELEVBQXlEO0FBQ3JEO0FBQ0g7O0FBRUQsUUFBSTNILENBQUMsQ0FBQ2tFLFVBQUZsRSxJQUFnQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUE5QixFQUE0QztBQUN4QztBQUNIOztBQUVELFFBQUl3VSxJQUFJLEtBQUssS0FBYixFQUFvQjtBQUNoQi9XLE1BQUFBLENBQUMsQ0FBQ08sUUFBRlAsQ0FBVzJILEtBQVgzSDtBQUNIOztBQUVEMlAsSUFBQUEsV0FBVyxHQUFHaEksS0FBZGdJO0FBQ0E5RyxJQUFBQSxVQUFVLEdBQUc3SSxDQUFDLENBQUN3UCxPQUFGeFAsQ0FBVTJQLFdBQVYzUCxDQUFiNkk7QUFDQXFPLElBQUFBLFNBQVMsR0FBR2xYLENBQUMsQ0FBQ3dQLE9BQUZ4UCxDQUFVQSxDQUFDLENBQUMwRCxZQUFaMUQsQ0FBWmtYO0FBRUFsWCxJQUFBQSxDQUFDLENBQUN5RCxXQUFGekQsR0FBZ0JBLENBQUMsQ0FBQ3dFLFNBQUZ4RSxLQUFnQixJQUFoQkEsR0FBdUJrWCxTQUF2QmxYLEdBQW1DQSxDQUFDLENBQUN3RSxTQUFyRHhFOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLEtBQXZCQSxJQUFnQ0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVZLFVBQVZaLEtBQXlCLEtBQXpEQSxLQUFtRTJILEtBQUssR0FBRyxDQUFSQSxJQUFhQSxLQUFLLEdBQUczSCxDQUFDLENBQUM2SyxXQUFGN0ssS0FBa0JBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBcEh4QyxDQUFKLEVBQXlJO0FBQ3JJLFVBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0IsSUFBVnhCLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCMlAsUUFBQUEsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDMEQsWUFBaEJpTTs7QUFDQSxZQUFJMUMsV0FBVyxLQUFLLElBQXBCLEVBQTBCO0FBQ3RCak4sVUFBQUEsQ0FBQyxDQUFDNEksWUFBRjVJLENBQWVrWCxTQUFmbFgsRUFBMEIsWUFBVztBQUNqQ0EsWUFBQUEsQ0FBQyxDQUFDb1QsU0FBRnBULENBQVkyUCxXQUFaM1A7QUFESixXQUFBQTtBQURKLFNBQUEsTUFJTztBQUNIQSxVQUFBQSxDQUFDLENBQUNvVCxTQUFGcFQsQ0FBWTJQLFdBQVozUDtBQUNIO0FBQ0o7O0FBQ0Q7QUFYSixLQUFBLE1BWU8sSUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVUwQixRQUFWMUIsS0FBdUIsS0FBdkJBLElBQWdDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVksVUFBVlosS0FBeUIsSUFBekRBLEtBQWtFMkgsS0FBSyxHQUFHLENBQVJBLElBQWFBLEtBQUssR0FBSTNILENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQWpIeEMsQ0FBSixFQUF1STtBQUMxSSxVQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdCLElBQVZ4QixLQUFtQixLQUF2QixFQUE4QjtBQUMxQjJQLFFBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQzBELFlBQWhCaU07O0FBQ0EsWUFBSTFDLFdBQVcsS0FBSyxJQUFwQixFQUEwQjtBQUN0QmpOLFVBQUFBLENBQUMsQ0FBQzRJLFlBQUY1SSxDQUFla1gsU0FBZmxYLEVBQTBCLFlBQVc7QUFDakNBLFlBQUFBLENBQUMsQ0FBQ29ULFNBQUZwVCxDQUFZMlAsV0FBWjNQO0FBREosV0FBQUE7QUFESixTQUFBLE1BSU87QUFDSEEsVUFBQUEsQ0FBQyxDQUFDb1QsU0FBRnBULENBQVkyUCxXQUFaM1A7QUFDSDtBQUNKOztBQUNEO0FBQ0g7O0FBRUQsUUFBS0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVVLFFBQWYsRUFBMEI7QUFDdEIwSixNQUFBQSxhQUFhLENBQUNwSyxDQUFDLENBQUN1RCxhQUFILENBQWI2RztBQUNIOztBQUVELFFBQUl1RixXQUFXLEdBQUcsQ0FBbEIsRUFBcUI7QUFDakIsVUFBSTNQLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQXpCeEMsS0FBNEMsQ0FBaEQsRUFBbUQ7QUFDL0NnWCxRQUFBQSxTQUFTLEdBQUdoWCxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZ0JBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdDLGNBQXJEd1U7QUFESixPQUFBLE1BRU87QUFDSEEsUUFBQUEsU0FBUyxHQUFHaFgsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWUyUCxXQUEzQnFIO0FBQ0g7QUFMTCxLQUFBLE1BTU8sSUFBSXJILFdBQVcsSUFBSTNQLENBQUMsQ0FBQ2tFLFVBQXJCLEVBQWlDO0FBQ3BDLFVBQUlsRSxDQUFDLENBQUNrRSxVQUFGbEUsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QyxjQUF6QnhDLEtBQTRDLENBQWhELEVBQW1EO0FBQy9DZ1gsUUFBQUEsU0FBUyxHQUFHLENBQVpBO0FBREosT0FBQSxNQUVPO0FBQ0hBLFFBQUFBLFNBQVMsR0FBR3JILFdBQVcsR0FBRzNQLENBQUMsQ0FBQ2tFLFVBQTVCOFM7QUFDSDtBQUxFLEtBQUEsTUFNQTtBQUNIQSxNQUFBQSxTQUFTLEdBQUdySCxXQUFacUg7QUFDSDs7QUFFRGhYLElBQUFBLENBQUMsQ0FBQ3FELFNBQUZyRCxHQUFjLElBQWRBOztBQUVBQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixjQUFsQkEsRUFBa0MsQ0FBQ0EsQ0FBRCxFQUFJQSxDQUFDLENBQUMwRCxZQUFOLEVBQW9Cc1QsU0FBcEIsQ0FBbENoWDs7QUFFQWlYLElBQUFBLFFBQVEsR0FBR2pYLENBQUMsQ0FBQzBELFlBQWJ1VDtBQUNBalgsSUFBQUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCZ1gsU0FBakJoWDs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDbUwsZUFBRm5MLENBQWtCQSxDQUFDLENBQUMwRCxZQUFwQjFEOztBQUVBLFFBQUtBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVTyxRQUFmLEVBQTBCO0FBRXRCNFcsTUFBQUEsU0FBUyxHQUFHblgsQ0FBQyxDQUFDNkosWUFBRjdKLEVBQVptWDtBQUNBQSxNQUFBQSxTQUFTLEdBQUdBLFNBQVMsQ0FBQ25OLEtBQVZtTixDQUFnQixVQUFoQkEsQ0FBWkE7O0FBRUEsVUFBS0EsU0FBUyxDQUFDalQsVUFBVmlULElBQXdCQSxTQUFTLENBQUMxWixPQUFWMFosQ0FBa0I1VSxZQUEvQyxFQUE4RDtBQUMxRDRVLFFBQUFBLFNBQVMsQ0FBQ2hNLGVBQVZnTSxDQUEwQm5YLENBQUMsQ0FBQzBELFlBQTVCeVQ7QUFDSDtBQUVKOztBQUVEblgsSUFBQUEsQ0FBQyxDQUFDa0wsVUFBRmxMOztBQUNBQSxJQUFBQSxDQUFDLENBQUNvUixZQUFGcFI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV3QixJQUFWeEIsS0FBbUIsSUFBdkIsRUFBNkI7QUFDekIsVUFBSWlOLFdBQVcsS0FBSyxJQUFwQixFQUEwQjtBQUV0QmpOLFFBQUFBLENBQUMsQ0FBQzZPLFlBQUY3TyxDQUFlaVgsUUFBZmpYOztBQUVBQSxRQUFBQSxDQUFDLENBQUMwTyxTQUFGMU8sQ0FBWWdYLFNBQVpoWCxFQUF1QixZQUFXO0FBQzlCQSxVQUFBQSxDQUFDLENBQUNvVCxTQUFGcFQsQ0FBWWdYLFNBQVpoWDtBQURKLFNBQUFBO0FBSkosT0FBQSxNQVFPO0FBQ0hBLFFBQUFBLENBQUMsQ0FBQ29ULFNBQUZwVCxDQUFZZ1gsU0FBWmhYO0FBQ0g7O0FBQ0RBLE1BQUFBLENBQUMsQ0FBQ3dJLGFBQUZ4STs7QUFDQTtBQUNIOztBQUVELFFBQUlpTixXQUFXLEtBQUssSUFBcEIsRUFBMEI7QUFDdEJqTixNQUFBQSxDQUFDLENBQUM0SSxZQUFGNUksQ0FBZTZJLFVBQWY3SSxFQUEyQixZQUFXO0FBQ2xDQSxRQUFBQSxDQUFDLENBQUNvVCxTQUFGcFQsQ0FBWWdYLFNBQVpoWDtBQURKLE9BQUFBO0FBREosS0FBQSxNQUlPO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQ29ULFNBQUZwVCxDQUFZZ1gsU0FBWmhYO0FBQ0g7QUF4SEwsR0FBQUo7O0FBNEhBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOeEgsQ0FBZ0JxUixTQUFoQnJSLEdBQTRCLFlBQVc7QUFFbkMsUUFBSUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVNLE1BQVZOLEtBQXFCLElBQXJCQSxJQUE2QkEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBMUQsRUFBd0U7QUFFcEV2QyxNQUFBQSxDQUFDLENBQUNpRSxVQUFGakUsQ0FBYW9YLElBQWJwWDs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRmhFLENBQWFvWCxJQUFicFg7QUFFSDs7QUFFRCxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW1CLElBQVZuQixLQUFtQixJQUFuQkEsSUFBMkJBLENBQUMsQ0FBQ2tFLFVBQUZsRSxHQUFlQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQXhELEVBQXNFO0FBRWxFdkMsTUFBQUEsQ0FBQyxDQUFDNEQsS0FBRjVELENBQVFvWCxJQUFScFg7QUFFSDs7QUFFREEsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRjFGLENBQVV0QixRQUFWc0IsQ0FBbUIsZUFBbkJBO0FBakJKLEdBQUFKOztBQXFCQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCeVgsY0FBaEJ6WCxHQUFpQyxZQUFXO0FBRXhDLFFBQUkwWCxLQUFKO0FBQUEsUUFBV0MsS0FBWDtBQUFBLFFBQWtCQyxDQUFsQjtBQUFBLFFBQXFCQyxVQUFyQjtBQUFBLFFBQWlDelgsQ0FBQyxHQUFHLElBQXJDOztBQUVBc1gsSUFBQUEsS0FBSyxHQUFHdFgsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWMwWCxNQUFkMVgsR0FBdUJBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjMlgsSUFBN0NMO0FBQ0FDLElBQUFBLEtBQUssR0FBR3ZYLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjNFgsTUFBZDVYLEdBQXVCQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBYzZYLElBQTdDTjtBQUNBQyxJQUFBQSxDQUFDLEdBQUdsTyxJQUFJLENBQUN3TyxLQUFMeE8sQ0FBV2lPLEtBQVhqTyxFQUFrQmdPLEtBQWxCaE8sQ0FBSmtPO0FBRUFDLElBQUFBLFVBQVUsR0FBR25PLElBQUksQ0FBQ3lPLEtBQUx6TyxDQUFXa08sQ0FBQyxHQUFHLEdBQUpBLEdBQVVsTyxJQUFJLENBQUMwTyxFQUExQjFPLENBQWJtTzs7QUFDQSxRQUFJQSxVQUFVLEdBQUcsQ0FBakIsRUFBb0I7QUFDaEJBLE1BQUFBLFVBQVUsR0FBRyxNQUFNbk8sSUFBSSxDQUFDb0gsR0FBTHBILENBQVNtTyxVQUFUbk8sQ0FBbkJtTztBQUNIOztBQUVELFFBQUtBLFVBQVUsSUFBSSxFQUFkQSxJQUFzQkEsVUFBVSxJQUFJLENBQXpDLEVBQTZDO0FBQ3pDLGFBQVF6WCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW9DLEdBQVZwQyxLQUFrQixLQUFsQkEsR0FBMEIsTUFBMUJBLEdBQW1DLE9BQTNDO0FBQ0g7O0FBQ0QsUUFBS3lYLFVBQVUsSUFBSSxHQUFkQSxJQUF1QkEsVUFBVSxJQUFJLEdBQTFDLEVBQWdEO0FBQzVDLGFBQVF6WCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW9DLEdBQVZwQyxLQUFrQixLQUFsQkEsR0FBMEIsTUFBMUJBLEdBQW1DLE9BQTNDO0FBQ0g7O0FBQ0QsUUFBS3lYLFVBQVUsSUFBSSxHQUFkQSxJQUF1QkEsVUFBVSxJQUFJLEdBQTFDLEVBQWdEO0FBQzVDLGFBQVF6WCxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVW9DLEdBQVZwQyxLQUFrQixLQUFsQkEsR0FBMEIsT0FBMUJBLEdBQW9DLE1BQTVDO0FBQ0g7O0FBQ0QsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVpRCxlQUFWakQsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcEMsVUFBS3lYLFVBQVUsSUFBSSxFQUFkQSxJQUFzQkEsVUFBVSxJQUFJLEdBQXpDLEVBQStDO0FBQzNDLGVBQU8sTUFBUDtBQURKLE9BQUEsTUFFTztBQUNILGVBQU8sSUFBUDtBQUNIO0FBQ0o7O0FBRUQsV0FBTyxVQUFQO0FBOUJKLEdBQUE3WDs7QUFrQ0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnFZLFFBQWhCclksR0FBMkIsVUFBU29OLEtBQVQsRUFBZ0I7QUFFdkMsUUFBSWhOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWtFLFVBREo7QUFBQSxRQUVJUCxTQUZKOztBQUlBM0QsSUFBQUEsQ0FBQyxDQUFDc0QsUUFBRnRELEdBQWEsS0FBYkE7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDb0YsV0FBRnBGLEdBQWdCLEtBQWhCQTtBQUNBQSxJQUFBQSxDQUFDLENBQUN5RixXQUFGekYsR0FBa0JBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFja1ksV0FBZGxZLEdBQTRCLEVBQTVCQSxHQUFtQyxLQUFuQ0EsR0FBMkMsSUFBN0RBOztBQUVBLFFBQUtBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjMlgsSUFBZDNYLEtBQXVCNlYsU0FBNUIsRUFBd0M7QUFDcEMsYUFBTyxLQUFQO0FBQ0g7O0FBRUQsUUFBSzdWLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjbVksT0FBZG5ZLEtBQTBCLElBQS9CLEVBQXNDO0FBQ2xDQSxNQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixNQUFsQkEsRUFBMEIsQ0FBQ0EsQ0FBRCxFQUFJQSxDQUFDLENBQUNxWCxjQUFGclgsRUFBSixDQUExQkE7QUFDSDs7QUFFRCxRQUFLQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBY2tZLFdBQWRsWSxJQUE2QkEsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWNvWSxRQUFoRCxFQUEyRDtBQUV2RHpVLE1BQUFBLFNBQVMsR0FBRzNELENBQUMsQ0FBQ3FYLGNBQUZyWCxFQUFaMkQ7O0FBRUEsY0FBU0EsU0FBVDtBQUVJLGFBQUssTUFBTDtBQUNBLGFBQUssTUFBTDtBQUVJTyxVQUFBQSxVQUFVLEdBQ05sRSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVTBDLFlBQVYxQyxHQUNJQSxDQUFDLENBQUN5TixjQUFGek4sQ0FBa0JBLENBQUMsQ0FBQzBELFlBQUYxRCxHQUFpQkEsQ0FBQyxDQUFDc1EsYUFBRnRRLEVBQW5DQSxDQURKQSxHQUVJQSxDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUJBLENBQUMsQ0FBQ3NRLGFBQUZ0USxFQUh6QmtFO0FBS0FsRSxVQUFBQSxDQUFDLENBQUN3RCxnQkFBRnhELEdBQXFCLENBQXJCQTtBQUVBOztBQUVKLGFBQUssT0FBTDtBQUNBLGFBQUssSUFBTDtBQUVJa0UsVUFBQUEsVUFBVSxHQUNObEUsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVUwQyxZQUFWMUMsR0FDSUEsQ0FBQyxDQUFDeU4sY0FBRnpOLENBQWtCQSxDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUJBLENBQUMsQ0FBQ3NRLGFBQUZ0USxFQUFuQ0EsQ0FESkEsR0FFSUEsQ0FBQyxDQUFDMEQsWUFBRjFELEdBQWlCQSxDQUFDLENBQUNzUSxhQUFGdFEsRUFIekJrRTtBQUtBbEUsVUFBQUEsQ0FBQyxDQUFDd0QsZ0JBQUZ4RCxHQUFxQixDQUFyQkE7QUFFQTs7QUFFSjtBQTFCSjs7QUErQkEsVUFBSTJELFNBQVMsSUFBSSxVQUFqQixFQUE4QjtBQUUxQjNELFFBQUFBLENBQUMsQ0FBQ2lLLFlBQUZqSyxDQUFnQmtFLFVBQWhCbEU7O0FBQ0FBLFFBQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxHQUFnQixFQUFoQkE7O0FBQ0FBLFFBQUFBLENBQUMsQ0FBQzBGLE9BQUYxRixDQUFVK00sT0FBVi9NLENBQWtCLE9BQWxCQSxFQUEyQixDQUFDQSxDQUFELEVBQUkyRCxTQUFKLENBQTNCM0Q7QUFFSDtBQXpDTCxLQUFBLE1BMkNPO0FBRUgsVUFBS0EsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWMwWCxNQUFkMVgsS0FBeUJBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjMlgsSUFBNUMsRUFBbUQ7QUFFL0MzWCxRQUFBQSxDQUFDLENBQUNpSyxZQUFGakssQ0FBZ0JBLENBQUMsQ0FBQzBELFlBQWxCMUQ7O0FBQ0FBLFFBQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxHQUFnQixFQUFoQkE7QUFFSDtBQUVKO0FBdEVMLEdBQUFKOztBQTBFQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCa0gsWUFBaEJsSCxHQUErQixVQUFTb04sS0FBVCxFQUFnQjtBQUUzQyxRQUFJaE4sQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBS0EsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV5QyxLQUFWekMsS0FBb0IsS0FBcEJBLElBQStCLGdCQUFnQm1HLFFBQWhCLElBQTRCbkcsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV5QyxLQUFWekMsS0FBb0IsS0FBcEYsRUFBNEY7QUFDeEY7QUFESixLQUFBLE1BRU8sSUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVxQixTQUFWckIsS0FBd0IsS0FBeEJBLElBQWlDZ04sS0FBSyxDQUFDOEcsSUFBTjlHLENBQVdxTCxPQUFYckwsQ0FBbUIsT0FBbkJBLE1BQWdDLENBQUMsQ0FBdEUsRUFBeUU7QUFDNUU7QUFDSDs7QUFFRGhOLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjc1ksV0FBZHRZLEdBQTRCZ04sS0FBSyxDQUFDdUwsYUFBTnZMLElBQXVCQSxLQUFLLENBQUN1TCxhQUFOdkwsQ0FBb0J3TCxPQUFwQnhMLEtBQWdDNkksU0FBdkQ3SSxHQUN4QkEsS0FBSyxDQUFDdUwsYUFBTnZMLENBQW9Cd0wsT0FBcEJ4TCxDQUE0QmxGLE1BREprRixHQUNhLENBRHpDaE47QUFHQUEsSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWNvWSxRQUFkcFksR0FBeUJBLENBQUMsQ0FBQzZELFNBQUY3RCxHQUFjQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FDbEM0QyxjQURMNUM7O0FBR0EsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVpRCxlQUFWakQsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcENBLE1BQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjb1ksUUFBZHBZLEdBQXlCQSxDQUFDLENBQUM4RCxVQUFGOUQsR0FBZUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQ25DNEMsY0FETDVDO0FBRUg7O0FBRUQsWUFBUWdOLEtBQUssQ0FBQy9HLElBQU4rRyxDQUFXMEUsTUFBbkI7QUFFSSxXQUFLLE9BQUw7QUFDSTFSLFFBQUFBLENBQUMsQ0FBQ3lZLFVBQUZ6WSxDQUFhZ04sS0FBYmhOOztBQUNBOztBQUVKLFdBQUssTUFBTDtBQUNJQSxRQUFBQSxDQUFDLENBQUMwWSxTQUFGMVksQ0FBWWdOLEtBQVpoTjs7QUFDQTs7QUFFSixXQUFLLEtBQUw7QUFDSUEsUUFBQUEsQ0FBQyxDQUFDaVksUUFBRmpZLENBQVdnTixLQUFYaE47O0FBQ0E7QUFaUjtBQXJCSixHQUFBSjs7QUF1Q0FBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjhZLFNBQWhCOVksR0FBNEIsVUFBU29OLEtBQVQsRUFBZ0I7QUFFeEMsUUFBSWhOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSTJZLFVBQVUsR0FBRyxLQURqQjtBQUFBLFFBRUlDLE9BRko7QUFBQSxRQUVhdkIsY0FGYjtBQUFBLFFBRTZCYSxXQUY3QjtBQUFBLFFBRTBDVyxjQUYxQztBQUFBLFFBRTBETCxPQUYxRDs7QUFJQUEsSUFBQUEsT0FBTyxHQUFHeEwsS0FBSyxDQUFDdUwsYUFBTnZMLEtBQXdCNkksU0FBeEI3SSxHQUFvQ0EsS0FBSyxDQUFDdUwsYUFBTnZMLENBQW9Cd0wsT0FBeER4TCxHQUFrRSxJQUE1RXdMOztBQUVBLFFBQUksQ0FBQ3hZLENBQUMsQ0FBQ3NELFFBQUgsSUFBZWtWLE9BQU8sSUFBSUEsT0FBTyxDQUFDMVEsTUFBUjBRLEtBQW1CLENBQWpELEVBQW9EO0FBQ2hELGFBQU8sS0FBUDtBQUNIOztBQUVESSxJQUFBQSxPQUFPLEdBQUc1WSxDQUFDLENBQUN3UCxPQUFGeFAsQ0FBVUEsQ0FBQyxDQUFDMEQsWUFBWjFELENBQVY0WTtBQUVBNVksSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWMyWCxJQUFkM1gsR0FBcUJ3WSxPQUFPLEtBQUszQyxTQUFaMkMsR0FBd0JBLE9BQU8sQ0FBQyxDQUFELENBQVBBLENBQVdNLEtBQW5DTixHQUEyQ3hMLEtBQUssQ0FBQytMLE9BQXRFL1k7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWM2WCxJQUFkN1gsR0FBcUJ3WSxPQUFPLEtBQUszQyxTQUFaMkMsR0FBd0JBLE9BQU8sQ0FBQyxDQUFELENBQVBBLENBQVdRLEtBQW5DUixHQUEyQ3hMLEtBQUssQ0FBQ2lNLE9BQXRFalo7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWNrWSxXQUFkbFksR0FBNEJzSixJQUFJLENBQUN5TyxLQUFMek8sQ0FBV0EsSUFBSSxDQUFDNFAsSUFBTDVQLENBQ25DQSxJQUFJLENBQUM2UCxHQUFMN1AsQ0FBU3RKLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjMlgsSUFBZDNYLEdBQXFCQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBYzBYLE1BQTVDcE8sRUFBb0QsQ0FBcERBLENBRG1DQSxDQUFYQSxDQUE1QnRKOztBQUdBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVaUQsZUFBVmpELEtBQThCLElBQWxDLEVBQXdDO0FBQ3BDQSxNQUFBQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBY2tZLFdBQWRsWSxHQUE0QnNKLElBQUksQ0FBQ3lPLEtBQUx6TyxDQUFXQSxJQUFJLENBQUM0UCxJQUFMNVAsQ0FDbkNBLElBQUksQ0FBQzZQLEdBQUw3UCxDQUFTdEosQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWM2WCxJQUFkN1gsR0FBcUJBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjNFgsTUFBNUN0TyxFQUFvRCxDQUFwREEsQ0FEbUNBLENBQVhBLENBQTVCdEo7QUFFSDs7QUFFRHFYLElBQUFBLGNBQWMsR0FBR3JYLENBQUMsQ0FBQ3FYLGNBQUZyWCxFQUFqQnFYOztBQUVBLFFBQUlBLGNBQWMsS0FBSyxVQUF2QixFQUFtQztBQUMvQjtBQUNIOztBQUVELFFBQUlySyxLQUFLLENBQUN1TCxhQUFOdkwsS0FBd0I2SSxTQUF4QjdJLElBQXFDaE4sQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWNrWSxXQUFkbFksR0FBNEIsQ0FBckUsRUFBd0U7QUFDcEVnTixNQUFBQSxLQUFLLENBQUNPLGNBQU5QO0FBQ0g7O0FBRUQ2TCxJQUFBQSxjQUFjLEdBQUcsQ0FBQzdZLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVb0MsR0FBVnBDLEtBQWtCLEtBQWxCQSxHQUEwQixDQUExQkEsR0FBOEIsQ0FBQyxDQUFoQyxLQUFzQ0EsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWMyWCxJQUFkM1gsR0FBcUJBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjMFgsTUFBbkMxWCxHQUE0QyxDQUE1Q0EsR0FBZ0QsQ0FBQyxDQUF2RixDQUFqQjZZOztBQUNBLFFBQUk3WSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVWlELGVBQVZqRCxLQUE4QixJQUFsQyxFQUF3QztBQUNwQzZZLE1BQUFBLGNBQWMsR0FBRzdZLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjNlgsSUFBZDdYLEdBQXFCQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBYzRYLE1BQW5DNVgsR0FBNEMsQ0FBNUNBLEdBQWdELENBQUMsQ0FBbEU2WTtBQUNIOztBQUdEWCxJQUFBQSxXQUFXLEdBQUdsWSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBY2tZLFdBQTVCQTtBQUVBbFksSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRjFFLENBQWNtWSxPQUFkblksR0FBd0IsS0FBeEJBOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFBVjFCLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCLFVBQUtBLENBQUMsQ0FBQzBELFlBQUYxRCxLQUFtQixDQUFuQkEsSUFBd0JxWCxjQUFjLEtBQUssT0FBM0NyWCxJQUF3REEsQ0FBQyxDQUFDMEQsWUFBRjFELElBQWtCQSxDQUFDLENBQUM2SyxXQUFGN0ssRUFBbEJBLElBQXFDcVgsY0FBYyxLQUFLLE1BQXJILEVBQThIO0FBQzFIYSxRQUFBQSxXQUFXLEdBQUdsWSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBY2tZLFdBQWRsWSxHQUE0QkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QixZQUFwRDJXO0FBQ0FsWSxRQUFBQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBY21ZLE9BQWRuWSxHQUF3QixJQUF4QkE7QUFDSDtBQUNKOztBQUVELFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVZ0QsUUFBVmhELEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCQSxNQUFBQSxDQUFDLENBQUN3RSxTQUFGeEUsR0FBYzRZLE9BQU8sR0FBR1YsV0FBVyxHQUFHVyxjQUF0QzdZO0FBREosS0FBQSxNQUVPO0FBQ0hBLE1BQUFBLENBQUMsQ0FBQ3dFLFNBQUZ4RSxHQUFjNFksT0FBTyxHQUFJVixXQUFXLElBQUlsWSxDQUFDLENBQUN5RSxLQUFGekUsQ0FBUWpDLE1BQVJpQyxLQUFtQkEsQ0FBQyxDQUFDNkQsU0FBekIsQ0FBWHFVLEdBQWtEVyxjQUEzRTdZO0FBQ0g7O0FBQ0QsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVpRCxlQUFWakQsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcENBLE1BQUFBLENBQUMsQ0FBQ3dFLFNBQUZ4RSxHQUFjNFksT0FBTyxHQUFHVixXQUFXLEdBQUdXLGNBQXRDN1k7QUFDSDs7QUFFRCxRQUFJQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXdCLElBQVZ4QixLQUFtQixJQUFuQkEsSUFBMkJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMkMsU0FBVjNDLEtBQXdCLEtBQXZELEVBQThEO0FBQzFELGFBQU8sS0FBUDtBQUNIOztBQUVELFFBQUlBLENBQUMsQ0FBQ3FELFNBQUZyRCxLQUFnQixJQUFwQixFQUEwQjtBQUN0QkEsTUFBQUEsQ0FBQyxDQUFDd0UsU0FBRnhFLEdBQWMsSUFBZEE7QUFDQSxhQUFPLEtBQVA7QUFDSDs7QUFFREEsSUFBQUEsQ0FBQyxDQUFDdVUsTUFBRnZVLENBQVNBLENBQUMsQ0FBQ3dFLFNBQVh4RTtBQXRFSixHQUFBSjs7QUEwRUFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQjZZLFVBQWhCN1ksR0FBNkIsVUFBU29OLEtBQVQsRUFBZ0I7QUFFekMsUUFBSWhOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXdZLE9BREo7O0FBR0F4WSxJQUFBQSxDQUFDLENBQUNvRixXQUFGcEYsR0FBZ0IsSUFBaEJBOztBQUVBLFFBQUlBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjc1ksV0FBZHRZLEtBQThCLENBQTlCQSxJQUFtQ0EsQ0FBQyxDQUFDa0UsVUFBRmxFLElBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVXVDLFlBQWpFLEVBQStFO0FBQzNFdkMsTUFBQUEsQ0FBQyxDQUFDMEUsV0FBRjFFLEdBQWdCLEVBQWhCQTtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFFBQUlnTixLQUFLLENBQUN1TCxhQUFOdkwsS0FBd0I2SSxTQUF4QjdJLElBQXFDQSxLQUFLLENBQUN1TCxhQUFOdkwsQ0FBb0J3TCxPQUFwQnhMLEtBQWdDNkksU0FBekUsRUFBb0Y7QUFDaEYyQyxNQUFBQSxPQUFPLEdBQUd4TCxLQUFLLENBQUN1TCxhQUFOdkwsQ0FBb0J3TCxPQUFwQnhMLENBQTRCLENBQTVCQSxDQUFWd0w7QUFDSDs7QUFFRHhZLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjMFgsTUFBZDFYLEdBQXVCQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBYzJYLElBQWQzWCxHQUFxQndZLE9BQU8sS0FBSzNDLFNBQVoyQyxHQUF3QkEsT0FBTyxDQUFDTSxLQUFoQ04sR0FBd0N4TCxLQUFLLENBQUMrTCxPQUExRi9ZO0FBQ0FBLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYxRSxDQUFjNFgsTUFBZDVYLEdBQXVCQSxDQUFDLENBQUMwRSxXQUFGMUUsQ0FBYzZYLElBQWQ3WCxHQUFxQndZLE9BQU8sS0FBSzNDLFNBQVoyQyxHQUF3QkEsT0FBTyxDQUFDUSxLQUFoQ1IsR0FBd0N4TCxLQUFLLENBQUNpTSxPQUExRmpaO0FBRUFBLElBQUFBLENBQUMsQ0FBQ3NELFFBQUZ0RCxHQUFhLElBQWJBO0FBbkJKLEdBQUFKOztBQXVCQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCd1osY0FBaEJ4WixHQUFpQ0EsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCeVosYUFBaEJ6WixHQUFnQyxZQUFXO0FBRXhFLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQzJGLFlBQUYzRixLQUFtQixJQUF2QixFQUE2QjtBQUV6QkEsTUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjdIOztBQUVBQSxNQUFBQSxDQUFDLENBQUNvRSxXQUFGcEUsQ0FBY29JLFFBQWRwSSxDQUF1QixLQUFLdkMsT0FBTCxDQUFhNEUsS0FBcENyQyxFQUEyQ3FJLE1BQTNDckk7O0FBRUFBLE1BQUFBLENBQUMsQ0FBQzJGLFlBQUYzRixDQUFlK0gsUUFBZi9ILENBQXdCQSxDQUFDLENBQUNvRSxXQUExQnBFOztBQUVBQSxNQUFBQSxDQUFDLENBQUN1SSxNQUFGdkk7QUFFSDtBQWRMLEdBQUFKOztBQWtCQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCaUksTUFBaEJqSSxHQUF5QixZQUFXO0FBRWhDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBMUMsSUFBQUEsQ0FBQyxDQUFDLGVBQUQsRUFBa0IwQyxDQUFDLENBQUMwRixPQUFwQixDQUFEcEksQ0FBOEJtUixNQUE5Qm5SOztBQUVBLFFBQUkwQyxDQUFDLENBQUM0RCxLQUFOLEVBQWE7QUFDVDVELE1BQUFBLENBQUMsQ0FBQzRELEtBQUY1RCxDQUFReU8sTUFBUnpPO0FBQ0g7O0FBRUQsUUFBSUEsQ0FBQyxDQUFDaUUsVUFBRmpFLElBQWdCQSxDQUFDLENBQUNpSCxRQUFGakgsQ0FBV3lLLElBQVh6SyxDQUFnQkEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVRLFNBQTFCUixDQUFwQixFQUEwRDtBQUN0REEsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRmpFLENBQWF5TyxNQUFiek87QUFDSDs7QUFFRCxRQUFJQSxDQUFDLENBQUNnRSxVQUFGaEUsSUFBZ0JBLENBQUMsQ0FBQ2lILFFBQUZqSCxDQUFXeUssSUFBWHpLLENBQWdCQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVMsU0FBMUJULENBQXBCLEVBQTBEO0FBQ3REQSxNQUFBQSxDQUFDLENBQUNnRSxVQUFGaEUsQ0FBYXlPLE1BQWJ6TztBQUNIOztBQUVEQSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGckUsQ0FDS3VLLFdBREx2SyxDQUNpQixzREFEakJBLEVBRUt1SCxJQUZMdkgsQ0FFVSxhQUZWQSxFQUV5QixNQUZ6QkEsRUFHS2pCLEdBSExpQixDQUdTLE9BSFRBLEVBR2tCLEVBSGxCQTtBQWxCSixHQUFBSjs7QUF5QkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQmlOLE9BQWhCak4sR0FBMEIsVUFBUzBaLGNBQVQsRUFBeUI7QUFFL0MsUUFBSXRaLENBQUMsR0FBRyxJQUFSOztBQUNBQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGMUYsQ0FBVStNLE9BQVYvTSxDQUFrQixTQUFsQkEsRUFBNkIsQ0FBQ0EsQ0FBRCxFQUFJc1osY0FBSixDQUE3QnRaOztBQUNBQSxJQUFBQSxDQUFDLENBQUN3TyxPQUFGeE87QUFKSixHQUFBSjs7QUFRQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCd1IsWUFBaEJ4UixHQUErQixZQUFXO0FBRXRDLFFBQUlJLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXlRLFlBREo7O0FBR0FBLElBQUFBLFlBQVksR0FBR25ILElBQUksQ0FBQ3NHLEtBQUx0RyxDQUFXdEosQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVV1QyxZQUFWdkMsR0FBeUIsQ0FBcENzSixDQUFmbUg7O0FBRUEsUUFBS3pRLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVTSxNQUFWTixLQUFxQixJQUFyQkEsSUFDREEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFEeEJ2QyxJQUVELENBQUNBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVMEIsUUFGZixFQUUwQjtBQUV0QjFCLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUZqRSxDQUFhdUssV0FBYnZLLENBQXlCLGdCQUF6QkEsRUFBMkN1SCxJQUEzQ3ZILENBQWdELGVBQWhEQSxFQUFpRSxPQUFqRUE7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUZoRSxDQUFhdUssV0FBYnZLLENBQXlCLGdCQUF6QkEsRUFBMkN1SCxJQUEzQ3ZILENBQWdELGVBQWhEQSxFQUFpRSxPQUFqRUE7O0FBRUEsVUFBSUEsQ0FBQyxDQUFDMEQsWUFBRjFELEtBQW1CLENBQXZCLEVBQTBCO0FBRXRCQSxRQUFBQSxDQUFDLENBQUNpRSxVQUFGakUsQ0FBYXRCLFFBQWJzQixDQUFzQixnQkFBdEJBLEVBQXdDdUgsSUFBeEN2SCxDQUE2QyxlQUE3Q0EsRUFBOEQsTUFBOURBOztBQUNBQSxRQUFBQSxDQUFDLENBQUNnRSxVQUFGaEUsQ0FBYXVLLFdBQWJ2SyxDQUF5QixnQkFBekJBLEVBQTJDdUgsSUFBM0N2SCxDQUFnRCxlQUFoREEsRUFBaUUsT0FBakVBO0FBSEosT0FBQSxNQUtPLElBQUlBLENBQUMsQ0FBQzBELFlBQUYxRCxJQUFrQkEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWVBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVdUMsWUFBM0N2QyxJQUEyREEsQ0FBQyxDQUFDdkMsT0FBRnVDLENBQVVZLFVBQVZaLEtBQXlCLEtBQXhGLEVBQStGO0FBRWxHQSxRQUFBQSxDQUFDLENBQUNnRSxVQUFGaEUsQ0FBYXRCLFFBQWJzQixDQUFzQixnQkFBdEJBLEVBQXdDdUgsSUFBeEN2SCxDQUE2QyxlQUE3Q0EsRUFBOEQsTUFBOURBOztBQUNBQSxRQUFBQSxDQUFDLENBQUNpRSxVQUFGakUsQ0FBYXVLLFdBQWJ2SyxDQUF5QixnQkFBekJBLEVBQTJDdUgsSUFBM0N2SCxDQUFnRCxlQUFoREEsRUFBaUUsT0FBakVBO0FBSEcsT0FBQSxNQUtBLElBQUlBLENBQUMsQ0FBQzBELFlBQUYxRCxJQUFrQkEsQ0FBQyxDQUFDa0UsVUFBRmxFLEdBQWUsQ0FBakNBLElBQXNDQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVksVUFBVlosS0FBeUIsSUFBbkUsRUFBeUU7QUFFNUVBLFFBQUFBLENBQUMsQ0FBQ2dFLFVBQUZoRSxDQUFhdEIsUUFBYnNCLENBQXNCLGdCQUF0QkEsRUFBd0N1SCxJQUF4Q3ZILENBQTZDLGVBQTdDQSxFQUE4RCxNQUE5REE7O0FBQ0FBLFFBQUFBLENBQUMsQ0FBQ2lFLFVBQUZqRSxDQUFhdUssV0FBYnZLLENBQXlCLGdCQUF6QkEsRUFBMkN1SCxJQUEzQ3ZILENBQWdELGVBQWhEQSxFQUFpRSxPQUFqRUE7QUFFSDtBQUVKO0FBL0JMLEdBQUFKOztBQW1DQUEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTnhILENBQWdCc0wsVUFBaEJ0TCxHQUE2QixZQUFXO0FBRXBDLFFBQUlJLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQzRELEtBQUY1RCxLQUFZLElBQWhCLEVBQXNCO0FBRWxCQSxNQUFBQSxDQUFDLENBQUM0RCxLQUFGNUQsQ0FDS3NILElBREx0SCxDQUNVLElBRFZBLEVBRUt1SyxXQUZMdkssQ0FFaUIsY0FGakJBLEVBR0t1SCxJQUhMdkgsQ0FHVSxhQUhWQSxFQUd5QixNQUh6QkE7O0FBS0FBLE1BQUFBLENBQUMsQ0FBQzRELEtBQUY1RCxDQUNLc0gsSUFETHRILENBQ1UsSUFEVkEsRUFFS2lJLEVBRkxqSSxDQUVRc0osSUFBSSxDQUFDc0csS0FBTHRHLENBQVd0SixDQUFDLENBQUMwRCxZQUFGMUQsR0FBaUJBLENBQUMsQ0FBQ3ZDLE9BQUZ1QyxDQUFVd0MsY0FBdEM4RyxDQUZSdEosRUFHS3RCLFFBSExzQixDQUdjLGNBSGRBLEVBSUt1SCxJQUpMdkgsQ0FJVSxhQUpWQSxFQUl5QixPQUp6QkE7QUFNSDtBQWpCTCxHQUFBSjs7QUFxQkFBLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU54SCxDQUFnQnFPLFVBQWhCck8sR0FBNkIsWUFBVztBQUVwQyxRQUFJSSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFLQSxDQUFDLENBQUN2QyxPQUFGdUMsQ0FBVVUsUUFBZixFQUEwQjtBQUV0QixVQUFLeUYsUUFBUSxDQUFDbkcsQ0FBQyxDQUFDcUYsTUFBSCxDQUFiLEVBQTBCO0FBRXRCckYsUUFBQUEsQ0FBQyxDQUFDb0YsV0FBRnBGLEdBQWdCLElBQWhCQTtBQUZKLE9BQUEsTUFJTztBQUVIQSxRQUFBQSxDQUFDLENBQUNvRixXQUFGcEYsR0FBZ0IsS0FBaEJBO0FBRUg7QUFFSjtBQWhCTCxHQUFBSjs7QUFvQkF0QyxFQUFBQSxDQUFDLENBQUNDLEVBQUZELENBQUswTSxLQUFMMU0sR0FBYSxZQUFXO0FBQ3BCLFFBQUkwQyxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l1VixHQUFHLEdBQUdELFNBQVMsQ0FBQyxDQUFELENBRG5CO0FBQUEsUUFFSWlFLElBQUksR0FBR0MsS0FBSyxDQUFDcFMsU0FBTm9TLENBQWdCNUcsS0FBaEI0RyxDQUFzQi9QLElBQXRCK1AsQ0FBMkJsRSxTQUEzQmtFLEVBQXNDLENBQXRDQSxDQUZYO0FBQUEsUUFHSTVGLENBQUMsR0FBRzVULENBQUMsQ0FBQzhILE1BSFY7QUFBQSxRQUlJN0csQ0FKSjtBQUFBLFFBS0l3WSxHQUxKOztBQU1BLFNBQUt4WSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUcyUyxDQUFoQixFQUFtQjNTLENBQUMsRUFBcEIsRUFBd0I7QUFDcEIsVUFBSSxRQUFPc1UsR0FBUCxLQUFjLFFBQWQsSUFBMEIsT0FBT0EsR0FBUCxJQUFjLFdBQTVDLEVBQ0l2VixDQUFDLENBQUNpQixDQUFELENBQURqQixDQUFLZ0ssS0FBTGhLLEdBQWEsSUFBSUosS0FBSixDQUFVSSxDQUFDLENBQUNpQixDQUFELENBQVgsRUFBZ0JzVSxHQUFoQixDQUFidlYsQ0FESixLQUdJeVosR0FBRyxHQUFHelosQ0FBQyxDQUFDaUIsQ0FBRCxDQUFEakIsQ0FBS2dLLEtBQUxoSyxDQUFXdVYsR0FBWHZWLEVBQWdCMFosS0FBaEIxWixDQUFzQkEsQ0FBQyxDQUFDaUIsQ0FBRCxDQUFEakIsQ0FBS2dLLEtBQTNCaEssRUFBa0N1WixJQUFsQ3ZaLENBQU55WjtBQUNKLFVBQUksT0FBT0EsR0FBUCxJQUFjLFdBQWxCLEVBQStCLE9BQU9BLEdBQVA7QUFDbEM7O0FBQ0QsV0FBT3paLENBQVA7QUFkSixHQUFBMUM7QUF6eUZILENBQUE7O0FEeWxGRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUU5bUZBLENBQUMsVUFBU0EsQ0FBVCxFQUFZLENBQUUsQ0FBZixFQUFpQitCLE1BQWpCOzs7QUNBQSxDQUFDLFVBQVMvQixDQUFULEVBQVk7QUFDWCxNQUFNcWMsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixHQUFXO0FBQ25DcmMsSUFBQUEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQjZCLFdBQWxCLENBQThCLFFBQTlCO0FBQ0QsR0FGRDs7QUFJQTdCLEVBQUFBLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYTRCLEVBQWIsQ0FBZ0IsT0FBaEIsRUFBeUJ5YSxpQkFBekI7QUFDRCxDQU5ELEVBTUd0YSxNQU5IOzs7QUNBQTs7O0FBR0EsQ0FBQyxVQUFTL0IsQ0FBVCxFQUFZO0FBQ1gsTUFBSUEsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhd0ssTUFBYixHQUFzQixDQUExQixFQUE2QjtBQUMzQjtBQUNEOztBQUNEeEssRUFBQUEsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhME0sS0FBYixDQUFtQjtBQUNqQnpILElBQUFBLFlBQVksRUFBRSxDQURHO0FBRWpCakMsSUFBQUEsTUFBTSxFQUFFO0FBRlMsR0FBbkI7QUFJRCxDQVJELEVBUUdqQixNQVJIO0FBVUE7Ozs7O0FBR0EsQ0FBQyxVQUFTL0IsQ0FBVCxFQUFZO0FBQ1gsTUFBSUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ3SyxNQUFyQixHQUE4QixDQUFsQyxFQUFxQztBQUNuQztBQUNEOztBQUNEeEssRUFBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUIwTSxLQUFyQixDQUEyQjtBQUN6QnpILElBQUFBLFlBQVksRUFBRSxDQURXO0FBRXpCL0IsSUFBQUEsU0FBUyxFQUNQLDZnQ0FIdUI7QUFJekJDLElBQUFBLFNBQVMsRUFDUDtBQUx1QixHQUEzQjtBQU9ELENBWEQsRUFXR3BCLE1BWEg7QUFhQTs7Ozs7QUFHQSxDQUFDLFVBQVMvQixDQUFULEVBQVk7QUFDWCxNQUFJQSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQndLLE1BQXJCLEdBQThCLENBQWxDLEVBQXFDO0FBQ25DO0FBQ0Q7O0FBQ0R4SyxFQUFBQSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQjBNLEtBQXJCLENBQTJCO0FBQ3pCekgsSUFBQUEsWUFBWSxFQUFFLENBRFc7QUFFekIvQixJQUFBQSxTQUFTLEVBQ1AsNmdDQUh1QjtBQUl6QkMsSUFBQUEsU0FBUyxFQUNQO0FBTHVCLEdBQTNCO0FBT0QsQ0FYRCxFQVdHcEIsTUFYSDs7O0FDaENBLElBQUl1YSxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBQVc7QUFDN0IsTUFBSUMsVUFBVSxHQUFHMVQsUUFBUSxDQUFDMlQsZ0JBQVQsQ0FBMEIsV0FBMUIsQ0FBakI7QUFDQSxNQUFJQyxTQUFTLEdBQUcsR0FBaEI7QUFDQUYsRUFBQUEsVUFBVSxDQUFDRyxPQUFYLENBQW1CLFVBQVMxSCxLQUFULEVBQWdCO0FBQ2pDLFFBQUlBLEtBQUssQ0FBQzJILFlBQU4sSUFBc0IsSUFBMUIsRUFBZ0M7QUFDOUI7QUFDRDs7QUFDRCxRQUFJM0gsS0FBSyxDQUFDNEgsU0FBTixDQUFnQkMsUUFBaEIsQ0FBeUIsUUFBekIsQ0FBSixFQUF3QztBQUN0QztBQUNEOztBQUNELFFBQUk3SCxLQUFLLENBQUM4SCxTQUFOLEdBQWtCdmEsTUFBTSxDQUFDd2EsV0FBUCxHQUFxQnhhLE1BQU0sQ0FBQ3lhLFdBQTVCLEdBQTBDUCxTQUFoRSxFQUEyRTtBQUN6RSxVQUFJekgsS0FBSyxDQUFDaUksT0FBTixDQUFjNUgsR0FBZCxJQUFxQmtELFNBQXpCLEVBQW9DO0FBQ2xDdkQsUUFBQUEsS0FBSyxDQUFDSyxHQUFOLEdBQVlMLEtBQUssQ0FBQ2lJLE9BQU4sQ0FBYzVILEdBQTFCO0FBQ0Q7O0FBQ0QsVUFBSUwsS0FBSyxDQUFDaUksT0FBTixDQUFjQyxFQUFkLElBQW9CM0UsU0FBeEIsRUFBbUM7QUFDakN2RCxRQUFBQSxLQUFLLENBQUNxRCxLQUFOLENBQVk4RSxlQUFaLEdBQThCLFNBQVNuSSxLQUFLLENBQUNpSSxPQUFOLENBQWNDLEVBQXZCLEdBQTRCLEdBQTFEO0FBQ0Q7O0FBQ0QsVUFBSWxJLEtBQUssQ0FBQ2lJLE9BQU4sQ0FBY0csTUFBZCxJQUF3QjdFLFNBQTVCLEVBQXVDO0FBQ3JDdkQsUUFBQUEsS0FBSyxDQUFDSyxHQUFOLEdBQVlMLEtBQUssQ0FBQ2lJLE9BQU4sQ0FBY0csTUFBMUI7QUFDRDs7QUFDRHBJLE1BQUFBLEtBQUssQ0FBQ0csTUFBTixHQUFlLFlBQVc7QUFDeEJILFFBQUFBLEtBQUssQ0FBQzRILFNBQU4sQ0FBZ0J4UCxHQUFoQixDQUFvQixRQUFwQjtBQUNELE9BRkQ7QUFHRDtBQUNGLEdBckJEO0FBc0JELENBekJEOztBQTJCQSxDQUFDLFVBQVNwTixDQUFULEVBQVk7QUFDWEEsRUFBQUEsQ0FBQyxDQUFDNkksUUFBRCxDQUFELENBQVlqSCxFQUFaLENBQWUsT0FBZixFQUF3QjBhLGFBQXhCO0FBQ0EvWixFQUFBQSxNQUFNLENBQUM4YSxnQkFBUCxDQUF3QixRQUF4QixFQUFrQ2YsYUFBbEM7QUFDQS9aLEVBQUFBLE1BQU0sQ0FBQzhhLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDZixhQUFsQztBQUNELENBSkQsRUFJR3ZhLE1BSkg7OztBQzNCQTtBQUNBLENBQUMsVUFBU3ViLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUM7O0FBQWEsV0FBU3JELENBQVQsQ0FBV0EsQ0FBWCxFQUFhbk0sQ0FBYixFQUFlcEssQ0FBZixFQUFpQjZaLENBQWpCLEVBQW1CbEgsQ0FBbkIsRUFBcUI7QUFBQyxhQUFTbUgsQ0FBVCxHQUFZO0FBQUNDLE1BQUFBLENBQUMsR0FBQ0osQ0FBQyxDQUFDSyxnQkFBRixHQUFtQixDQUFyQixFQUF1QmhhLENBQUMsR0FBQ3NLLENBQUMsQ0FBQ3RLLENBQUQsQ0FBMUIsRUFBOEJvSyxDQUFDLENBQUM2UCxLQUFGLElBQVMsQ0FBVCxJQUFZdlIsVUFBVSxDQUFDLFlBQVU7QUFBQ3dSLFFBQUFBLENBQUMsQ0FBQyxDQUFDLENBQUYsQ0FBRDtBQUFNLE9BQWxCLEVBQW1COVAsQ0FBQyxDQUFDNlAsS0FBckIsQ0FBcEQsRUFBZ0YsQ0FBQzdQLENBQUMsQ0FBQzZQLEtBQUYsR0FBUSxDQUFSLElBQVc3UCxDQUFDLENBQUMrUCxRQUFkLE1BQTBCTixDQUFDLENBQUNELENBQUYsR0FBSVEsQ0FBQyxDQUFDaFEsQ0FBQyxDQUFDaVEsUUFBSCxFQUFZLFVBQVNWLENBQVQsRUFBVztBQUFDLHFCQUFXQSxDQUFDLENBQUM5RyxJQUFiLEtBQW9CeUgsQ0FBQyxHQUFDQyxDQUFDLEdBQUMsQ0FBQyxDQUF6QixHQUE0QkwsQ0FBQyxDQUFDUCxDQUFDLENBQUNhLEdBQUgsQ0FBN0I7QUFBcUMsT0FBN0QsQ0FBTCxFQUFvRVgsQ0FBQyxDQUFDelAsQ0FBRixHQUFJLFVBQVN1UCxDQUFULEVBQVc7QUFBQ0EsUUFBQUEsQ0FBQyxHQUFDclAsQ0FBQyxDQUFDcVAsQ0FBRCxDQUFILEVBQU8zWixDQUFDLENBQUNtUCxJQUFGLENBQU9zSixLQUFQLENBQWF6WSxDQUFiLEVBQWUyWixDQUFmLENBQVA7QUFBeUIsT0FBN0csRUFBOEdFLENBQUMsQ0FBQ1ksQ0FBRixHQUFJLFlBQVU7QUFBQyxlQUFPemEsQ0FBQyxHQUFDNE0sQ0FBQyxDQUFDNU0sQ0FBRCxDQUFELENBQUsrTixNQUFMLENBQVksWUFBVTtBQUFDLGlCQUFNLENBQUNuQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE1SCxJQUFSLENBQWFvRixDQUFDLENBQUNzUSxVQUFmLENBQVA7QUFBa0MsU0FBekQsQ0FBVDtBQUFvRSxPQUFqTSxFQUFrTWIsQ0FBQyxDQUFDQyxDQUFGLEdBQUksVUFBU0gsQ0FBVCxFQUFXO0FBQUMsYUFBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNELENBQUMsQ0FBQzlTLE1BQWhCLEVBQXVCK1MsQ0FBQyxFQUF4QixFQUEyQjtBQUFDLGNBQUlyRCxDQUFDLEdBQUN2VyxDQUFDLENBQUMrTixNQUFGLENBQVMsWUFBVTtBQUFDLG1CQUFPLFNBQU80TCxDQUFDLENBQUNDLENBQUQsQ0FBZjtBQUFtQixXQUF2QyxDQUFOO0FBQStDckQsVUFBQUEsQ0FBQyxDQUFDMVAsTUFBRixJQUFVcVQsQ0FBQyxDQUFDLENBQUMsQ0FBRixFQUFJM0QsQ0FBSixDQUFYO0FBQWtCO0FBQUMsT0FBaFQsRUFBaVQyRCxDQUFDLEVBQWxULEVBQXFUdE4sQ0FBQyxDQUFDeEMsQ0FBQyxDQUFDdVEsWUFBSCxDQUFELENBQWtCMWMsRUFBbEIsQ0FBcUIsWUFBVTBVLENBQVYsR0FBWSxVQUFaLEdBQXVCQSxDQUE1QyxFQUE4Q2tILENBQUMsQ0FBQ0QsQ0FBaEQsQ0FBL1UsQ0FBaEY7QUFBbWQ7O0FBQUEsYUFBU3RQLENBQVQsQ0FBV3FQLENBQVgsRUFBYTtBQUFDLFVBQUkzWixDQUFDLEdBQUNvSyxDQUFDLENBQUN3USxZQUFSO0FBQUEsVUFBcUJDLENBQUMsR0FBQ3pRLENBQUMsQ0FBQzBRLFdBQXpCO0FBQUEsVUFBcUNqQixDQUFDLEdBQUN6UCxDQUFDLENBQUMyUSxTQUF6QztBQUFBLFVBQW1EcEksQ0FBQyxHQUFDdkksQ0FBQyxDQUFDNFEsZUFBdkQ7QUFBQSxVQUF1RWxCLENBQUMsR0FBQzFQLENBQUMsQ0FBQzZRLGVBQTNFO0FBQUEsVUFBMkYzUSxDQUFDLEdBQUNGLENBQUMsQ0FBQzhRLEVBQUYsSUFBTSxFQUFuRztBQUFzR3ZCLE1BQUFBLENBQUMsR0FBQy9NLENBQUMsQ0FBQytNLENBQUQsQ0FBRCxDQUFLNUwsTUFBTCxDQUFZLFlBQVU7QUFBQyxZQUFJNEwsQ0FBQyxHQUFDL00sQ0FBQyxDQUFDLElBQUQsQ0FBUDtBQUFBLFlBQWMySixDQUFDLEdBQUM0RSxDQUFDLENBQUMsSUFBRCxDQUFqQjtBQUF3QixlQUFNLENBQUN4QixDQUFDLENBQUMzVSxJQUFGLENBQU9vRixDQUFDLENBQUNnUixXQUFULENBQUQsS0FBeUJ6QixDQUFDLENBQUNyVCxJQUFGLENBQU84RCxDQUFDLENBQUNpUixTQUFULEtBQXFCMUIsQ0FBQyxDQUFDclQsSUFBRixDQUFPcU0sQ0FBUCxDQUFyQixJQUFnQ2dILENBQUMsQ0FBQ3JULElBQUYsQ0FBT3dULENBQVAsQ0FBaEMsSUFBMkN4UCxDQUFDLENBQUNpTSxDQUFELENBQUQsS0FBT3FELENBQTNFLENBQU47QUFBb0YsT0FBbkksRUFBcUk1VSxJQUFySSxDQUEwSSxZQUFVb0YsQ0FBQyxDQUFDa1IsSUFBdEosRUFBMkovRSxDQUEzSixDQUFGOztBQUFnSyxXQUFJLElBQUkyRCxDQUFDLEdBQUMsQ0FBTixFQUFRcUIsQ0FBQyxHQUFDNUIsQ0FBQyxDQUFDOVMsTUFBaEIsRUFBdUJxVCxDQUFDLEdBQUNxQixDQUF6QixFQUEyQnJCLENBQUMsRUFBNUIsRUFBK0I7QUFBQyxZQUFJc0IsQ0FBQyxHQUFDNU8sQ0FBQyxDQUFDK00sQ0FBQyxDQUFDTyxDQUFELENBQUYsQ0FBUDtBQUFBLFlBQWNPLENBQUMsR0FBQ1UsQ0FBQyxDQUFDeEIsQ0FBQyxDQUFDTyxDQUFELENBQUYsQ0FBakI7QUFBQSxZQUF3QnVCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDbFYsSUFBRixDQUFPOEQsQ0FBQyxDQUFDc1Isa0JBQVQsS0FBOEI3QixDQUF4RDtBQUEwRFksUUFBQUEsQ0FBQyxLQUFHa0IsQ0FBSixJQUFPRixDQUFQLElBQVVELENBQUMsQ0FBQ2xWLElBQUYsQ0FBT3FNLENBQVAsQ0FBVixJQUFxQjZJLENBQUMsQ0FBQ2xWLElBQUYsQ0FBT3FNLENBQVAsRUFBU3RJLENBQUMsQ0FBQ21SLENBQUMsQ0FBQ2xWLElBQUYsQ0FBT3FNLENBQVAsQ0FBRCxFQUFXOEksQ0FBWCxDQUFWLENBQXJCLEVBQThDblIsQ0FBQyxDQUFDbVEsQ0FBRCxDQUFELEtBQU9iLENBQVAsSUFBVTRCLENBQUMsQ0FBQ2xWLElBQUYsQ0FBT3dULENBQVAsQ0FBVixJQUFxQjBCLENBQUMsQ0FBQ2xWLElBQUYsQ0FBT3dULENBQVAsRUFBU3hQLENBQUMsQ0FBQ21RLENBQUQsQ0FBVixDQUFuRSxFQUFrRkEsQ0FBQyxLQUFHa0IsQ0FBSixJQUFPM2IsQ0FBUCxJQUFVLENBQUN3YixDQUFDLENBQUNsVixJQUFGLENBQU9zVixDQUFQLENBQVgsR0FBcUJKLENBQUMsQ0FBQ2xWLElBQUYsQ0FBT3NWLENBQVAsRUFBUzViLENBQVQsQ0FBckIsR0FBaUN5YSxDQUFDLEtBQUdrQixDQUFKLElBQU8sQ0FBQ2QsQ0FBUixJQUFXVyxDQUFDLENBQUMxZCxHQUFGLENBQU0rZCxDQUFOLEtBQVUsV0FBU0wsQ0FBQyxDQUFDMWQsR0FBRixDQUFNK2QsQ0FBTixDQUE5QixJQUF3Q0wsQ0FBQyxDQUFDMWQsR0FBRixDQUFNK2QsQ0FBTixFQUFRLFVBQVFoQixDQUFSLEdBQVUsSUFBbEIsQ0FBM0o7QUFBbUw7O0FBQUEsYUFBT2xCLENBQVA7QUFBUzs7QUFBQSxhQUFTTyxDQUFULENBQVdQLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsVUFBRyxDQUFDNVosQ0FBQyxDQUFDNkcsTUFBTixFQUFhLE9BQU8sTUFBS3VELENBQUMsQ0FBQzBSLFdBQUYsSUFBZXZGLENBQUMsQ0FBQ2hKLE9BQUYsRUFBcEIsQ0FBUDs7QUFBd0MsV0FBSSxJQUFJc04sQ0FBQyxHQUFDakIsQ0FBQyxJQUFFNVosQ0FBVCxFQUFXNlosQ0FBQyxHQUFDLENBQUMsQ0FBZCxFQUFnQmxILENBQUMsR0FBQ3ZJLENBQUMsQ0FBQzJRLFNBQUYsSUFBYSxFQUEvQixFQUFrQ2pCLENBQUMsR0FBQzFQLENBQUMsQ0FBQzRRLGVBQXRDLEVBQXNEMVEsQ0FBQyxHQUFDRixDQUFDLENBQUNnUixXQUExRCxFQUFzRWxCLENBQUMsR0FBQyxDQUE1RSxFQUE4RUEsQ0FBQyxHQUFDVyxDQUFDLENBQUNoVSxNQUFsRixFQUF5RnFULENBQUMsRUFBMUY7QUFBNkYsWUFBR1AsQ0FBQyxJQUFFQyxDQUFILElBQU00QixDQUFDLENBQUNYLENBQUMsQ0FBQ1gsQ0FBRCxDQUFGLENBQVYsRUFBaUI7QUFBQyxjQUFJTyxDQUFDLEdBQUM3TixDQUFDLENBQUNpTyxDQUFDLENBQUNYLENBQUQsQ0FBRixDQUFQO0FBQUEsY0FBY3VCLENBQUMsR0FBQ04sQ0FBQyxDQUFDTixDQUFDLENBQUNYLENBQUQsQ0FBRixDQUFqQjtBQUFBLGNBQXdCN1AsQ0FBQyxHQUFDb1EsQ0FBQyxDQUFDblUsSUFBRixDQUFPOEQsQ0FBQyxDQUFDaVIsU0FBVCxDQUExQjtBQUFBLGNBQThDakIsQ0FBQyxHQUFDSyxDQUFDLENBQUNuVSxJQUFGLENBQU84RCxDQUFDLENBQUNzUixrQkFBVCxLQUE4Qi9JLENBQTlFO0FBQUEsY0FBZ0ZvSixDQUFDLEdBQUN0QixDQUFDLENBQUNuVSxJQUFGLENBQU84RCxDQUFDLENBQUM2USxlQUFULENBQWxGO0FBQTRHUixVQUFBQSxDQUFDLENBQUN6VixJQUFGLENBQU9zRixDQUFQLEtBQVdGLENBQUMsQ0FBQzRSLFdBQUYsSUFBZSxDQUFDdkIsQ0FBQyxDQUFDcE8sRUFBRixDQUFLLFVBQUwsQ0FBM0IsSUFBNkMsRUFBRSxDQUFDaEMsQ0FBQyxJQUFFb1EsQ0FBQyxDQUFDblUsSUFBRixDQUFPd1QsQ0FBUCxDQUFKLE1BQWlCMkIsQ0FBQyxLQUFHRSxDQUFKLEtBQVF2QixDQUFDLEdBQUMvUCxDQUFGLEtBQU1vUSxDQUFDLENBQUNuVSxJQUFGLENBQU9zVixDQUFQLENBQU4sSUFBaUJuQixDQUFDLENBQUNuVSxJQUFGLENBQU93VCxDQUFQLE1BQVlXLENBQUMsQ0FBQ25VLElBQUYsQ0FBTzJWLENBQVAsQ0FBckMsS0FBaURSLENBQUMsS0FBR0UsQ0FBSixJQUFPdkIsQ0FBQyxHQUFDL1AsQ0FBRixLQUFNb1EsQ0FBQyxDQUFDM2MsR0FBRixDQUFNK2QsQ0FBTixDQUEvRSxLQUEwRkUsQ0FBNUYsQ0FBN0MsS0FBOElsQyxDQUFDLEdBQUMsQ0FBQyxDQUFILEVBQUtZLENBQUMsQ0FBQ3pWLElBQUYsQ0FBT3NGLENBQVAsRUFBUyxDQUFDLENBQVYsQ0FBTCxFQUFrQmlSLENBQUMsQ0FBQ2QsQ0FBRCxFQUFHZ0IsQ0FBSCxFQUFLckIsQ0FBTCxFQUFPMkIsQ0FBUCxDQUFqSztBQUE0SztBQUF2WTs7QUFBdVlsQyxNQUFBQSxDQUFDLEtBQUc3WixDQUFDLEdBQUM0TSxDQUFDLENBQUM1TSxDQUFELENBQUQsQ0FBSytOLE1BQUwsQ0FBWSxZQUFVO0FBQUMsZUFBTSxDQUFDbkIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNUgsSUFBUixDQUFhc0YsQ0FBYixDQUFQO0FBQXVCLE9BQTlDLENBQUwsQ0FBRDtBQUF1RDs7QUFBQSxhQUFTaVIsQ0FBVCxDQUFXNUIsQ0FBWCxFQUFhQyxDQUFiLEVBQWVyRCxDQUFmLEVBQWlCdlcsQ0FBakIsRUFBbUI7QUFBQyxRQUFFa2MsQ0FBRjs7QUFBSSxVQUFJckIsRUFBQyxHQUFDLGFBQVU7QUFBQ25ILFFBQUFBLENBQUMsQ0FBQyxTQUFELEVBQVdpRyxDQUFYLENBQUQsRUFBZW9DLENBQUMsRUFBaEIsRUFBbUJsQixFQUFDLEdBQUNqTyxDQUFDLENBQUN1UCxJQUF2QjtBQUE0QixPQUE3Qzs7QUFBOEN6SSxNQUFBQSxDQUFDLENBQUMsWUFBRCxFQUFjaUcsQ0FBZCxDQUFEO0FBQWtCLFVBQUlFLENBQUMsR0FBQ3pQLENBQUMsQ0FBQ2lSLFNBQVI7QUFBQSxVQUFrQjFJLENBQUMsR0FBQ3ZJLENBQUMsQ0FBQzRRLGVBQXRCO0FBQUEsVUFBc0NsQixDQUFDLEdBQUMxUCxDQUFDLENBQUNnUyxjQUExQztBQUFBLFVBQXlEOVIsQ0FBQyxHQUFDRixDQUFDLENBQUNpUyxlQUE3RDtBQUFBLFVBQTZFbkMsQ0FBQyxHQUFDOVAsQ0FBQyxDQUFDa1MsZUFBakY7QUFBQSxVQUFpR2YsQ0FBQyxHQUFDblIsQ0FBQyxDQUFDc1EsVUFBckc7QUFBQSxVQUFnSGMsQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDclQsSUFBRixDQUFPZ0UsQ0FBUCxDQUFsSDs7QUFBNEgsVUFBR3RLLENBQUgsRUFBSztBQUFDLFlBQUl5YSxFQUFDLEdBQUMsYUFBVTtBQUFDUCxVQUFBQSxDQUFDLElBQUVQLENBQUMsQ0FBQ3BRLFVBQUYsQ0FBYWEsQ0FBQyxDQUFDNlEsZUFBZixDQUFILEVBQW1DdEIsQ0FBQyxDQUFDM1UsSUFBRixDQUFPdVcsQ0FBUCxFQUFTLENBQUMsQ0FBVixDQUFuQyxFQUFnRDdILENBQUMsQ0FBQzZJLENBQUQsRUFBRzVDLENBQUgsQ0FBakQsRUFBdURqUixVQUFVLENBQUNxVCxDQUFELEVBQUcsQ0FBSCxDQUFqRSxFQUF1RXRCLEVBQUMsR0FBQzdOLENBQUMsQ0FBQ3VQLElBQTNFO0FBQWdGLFNBQWpHOztBQUFrR3hDLFFBQUFBLENBQUMsQ0FBQzdNLEdBQUYsQ0FBTTBQLENBQU4sRUFBU0MsR0FBVCxDQUFhRCxDQUFiLEVBQWUzQixFQUFmLEVBQWtCNEIsR0FBbEIsQ0FBc0JDLENBQXRCLEVBQXdCakMsRUFBeEIsR0FBMkIvRyxDQUFDLENBQUMxVCxDQUFELEVBQUcyWixDQUFILEVBQUssVUFBU0MsQ0FBVCxFQUFXO0FBQUNBLFVBQUFBLENBQUMsSUFBRUQsQ0FBQyxDQUFDN00sR0FBRixDQUFNNFAsQ0FBTixHQUFTakMsRUFBQyxFQUFaLEtBQWlCZCxDQUFDLENBQUM3TSxHQUFGLENBQU0wUCxDQUFOLEdBQVMzQixFQUFDLEVBQTNCLENBQUQ7QUFBZ0MsU0FBakQsQ0FBRCxJQUFxRGxCLENBQUMsQ0FBQzdOLE9BQUYsQ0FBVTBRLENBQVYsQ0FBaEY7QUFBNkYsT0FBck0sTUFBeU07QUFBQyxZQUFJZixDQUFDLEdBQUM3TyxDQUFDLENBQUMsSUFBSStQLEtBQUosRUFBRCxDQUFQO0FBQW1CbEIsUUFBQUEsQ0FBQyxDQUFDZ0IsR0FBRixDQUFNRCxDQUFOLEVBQVEzQixFQUFSLEVBQVc0QixHQUFYLENBQWVDLENBQWYsRUFBaUIsWUFBVTtBQUFDL0MsVUFBQUEsQ0FBQyxDQUFDeEQsSUFBRixJQUFTeUQsQ0FBQyxLQUFHK0IsQ0FBSixHQUFNaEMsQ0FBQyxDQUFDclQsSUFBRixDQUFPc1csQ0FBUCxFQUFTbkIsQ0FBQyxDQUFDblYsSUFBRixDQUFPc1csQ0FBUCxDQUFULEVBQW9CdFcsSUFBcEIsQ0FBeUIyVixDQUF6QixFQUEyQlIsQ0FBQyxDQUFDblYsSUFBRixDQUFPMlYsQ0FBUCxDQUEzQixFQUFzQzNWLElBQXRDLENBQTJDc1YsQ0FBM0MsRUFBNkNILENBQUMsQ0FBQ25WLElBQUYsQ0FBT3NWLENBQVAsQ0FBN0MsQ0FBTixHQUE4RGpDLENBQUMsQ0FBQzdiLEdBQUYsQ0FBTStkLENBQU4sRUFBUSxVQUFRSixDQUFDLENBQUNuVixJQUFGLENBQU9zVixDQUFQLENBQVIsR0FBa0IsSUFBMUIsQ0FBdkUsRUFBdUdqQyxDQUFDLENBQUN2UCxDQUFDLENBQUN5UyxNQUFILENBQUQsQ0FBWXpTLENBQUMsQ0FBQzBTLFVBQWQsQ0FBdkcsRUFBaUk1QyxDQUFDLEtBQUdQLENBQUMsQ0FBQ3BRLFVBQUYsQ0FBYXNRLENBQUMsR0FBQyxHQUFGLEdBQU1sSCxDQUFOLEdBQVEsR0FBUixHQUFZckksQ0FBWixHQUFjLEdBQWQsR0FBa0JGLENBQUMsQ0FBQ3NSLGtCQUFqQyxHQUFxRDVCLENBQUMsS0FBRzhDLENBQUosSUFBT2pELENBQUMsQ0FBQ3BRLFVBQUYsQ0FBYXVRLENBQWIsQ0FBL0QsQ0FBbEksRUFBa05ILENBQUMsQ0FBQzNVLElBQUYsQ0FBT3VXLENBQVAsRUFBUyxDQUFDLENBQVYsQ0FBbE4sRUFBK043SCxDQUFDLENBQUM2SSxDQUFELEVBQUc1QyxDQUFILENBQWhPLEVBQXNPOEIsQ0FBQyxDQUFDak8sTUFBRixFQUF0TyxFQUFpUHVPLENBQUMsRUFBbFA7QUFBcVAsU0FBalI7QUFBbVIsWUFBSVosQ0FBQyxHQUFDLENBQUNwQixDQUFDLElBQUV5QixDQUFILEdBQUtBLENBQUwsR0FBTzdCLENBQUMsQ0FBQ3JULElBQUYsQ0FBT3VULENBQVAsQ0FBUixLQUFvQixFQUExQjtBQUE2QjRCLFFBQUFBLENBQUMsQ0FBQ25WLElBQUYsQ0FBT3NXLENBQVAsRUFBU2pELENBQUMsQ0FBQ3JULElBQUYsQ0FBT3dULENBQVAsQ0FBVCxFQUFvQnhULElBQXBCLENBQXlCMlYsQ0FBekIsRUFBMkJ0QyxDQUFDLENBQUNyVCxJQUFGLENBQU9xTSxDQUFQLENBQTNCLEVBQXNDck0sSUFBdEMsQ0FBMkNzVixDQUEzQyxFQUE2Q1QsQ0FBQyxHQUFDNUUsQ0FBQyxHQUFDNEUsQ0FBSCxHQUFLLElBQW5ELEdBQXlETSxDQUFDLENBQUNsVCxRQUFGLElBQVlrVCxDQUFDLENBQUMzUCxPQUFGLENBQVU0USxDQUFWLENBQXJFO0FBQWtGO0FBQUM7O0FBQUEsYUFBU2xCLENBQVQsQ0FBVzdCLENBQVgsRUFBYTtBQUFDLFVBQUlDLENBQUMsR0FBQ0QsQ0FBQyxDQUFDb0QscUJBQUYsRUFBTjtBQUFBLFVBQWdDeEcsQ0FBQyxHQUFDbk0sQ0FBQyxDQUFDNFMsZUFBcEM7QUFBQSxVQUFvRHBRLENBQUMsR0FBQ3hDLENBQUMsQ0FBQzZTLFNBQXhEO0FBQUEsVUFBa0VqZCxDQUFDLEdBQUN5YixDQUFDLEtBQUc3TyxDQUFKLEdBQU1nTixDQUFDLENBQUM1UixHQUFSLElBQWEsQ0FBQzRFLENBQUQsR0FBR2dOLENBQUMsQ0FBQ3NELE1BQXRGO0FBQUEsVUFBNkZyQyxDQUFDLEdBQUNKLENBQUMsS0FBRzdOLENBQUosR0FBTWdOLENBQUMsQ0FBQzdSLElBQVIsSUFBYyxDQUFDNkUsQ0FBRCxHQUFHZ04sQ0FBQyxDQUFDN0YsS0FBbEg7QUFBd0gsYUFBTSxlQUFhd0MsQ0FBYixHQUFldlcsQ0FBZixHQUFpQixpQkFBZXVXLENBQWYsR0FBaUJzRSxDQUFqQixHQUFtQjdhLENBQUMsSUFBRTZhLENBQTdDO0FBQStDOztBQUFBLGFBQVNKLENBQVQsR0FBWTtBQUFDLGFBQU9ILENBQUMsSUFBRSxDQUFILEdBQUtBLENBQUwsR0FBT0EsQ0FBQyxHQUFDMU4sQ0FBQyxDQUFDK00sQ0FBRCxDQUFELENBQUs5YyxLQUFMLEVBQWhCO0FBQTZCOztBQUFBLGFBQVM0ZSxDQUFULEdBQVk7QUFBQyxhQUFPbEIsQ0FBQyxJQUFFLENBQUgsR0FBS0EsQ0FBTCxHQUFPQSxDQUFDLEdBQUMzTixDQUFDLENBQUMrTSxDQUFELENBQUQsQ0FBSzdjLE1BQUwsRUFBaEI7QUFBOEI7O0FBQUEsYUFBU3FlLENBQVQsQ0FBV3hCLENBQVgsRUFBYTtBQUFDLGFBQU9BLENBQUMsQ0FBQy9JLE9BQUYsQ0FBVXVNLFdBQVYsRUFBUDtBQUErQjs7QUFBQSxhQUFTOVMsQ0FBVCxDQUFXc1AsQ0FBWCxFQUFhQyxDQUFiLEVBQWU7QUFBQyxVQUFHQSxDQUFILEVBQUs7QUFBQyxZQUFJckQsQ0FBQyxHQUFDb0QsQ0FBQyxDQUFDeUQsS0FBRixDQUFRLEdBQVIsQ0FBTjtBQUFtQnpELFFBQUFBLENBQUMsR0FBQyxFQUFGOztBQUFLLGFBQUksSUFBSXZQLENBQUMsR0FBQyxDQUFOLEVBQVF3QyxDQUFDLEdBQUMySixDQUFDLENBQUMxUCxNQUFoQixFQUF1QnVELENBQUMsR0FBQ3dDLENBQXpCLEVBQTJCeEMsQ0FBQyxFQUE1QjtBQUErQnVQLFVBQUFBLENBQUMsSUFBRUMsQ0FBQyxHQUFDckQsQ0FBQyxDQUFDbk0sQ0FBRCxDQUFELENBQUtpVCxJQUFMLEVBQUYsSUFBZWpULENBQUMsS0FBR3dDLENBQUMsR0FBQyxDQUFOLEdBQVEsR0FBUixHQUFZLEVBQTNCLENBQUg7QUFBL0I7QUFBaUU7O0FBQUEsYUFBTytNLENBQVA7QUFBUzs7QUFBQSxhQUFTUyxDQUFULENBQVdULENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsVUFBSWhOLENBQUo7QUFBQSxVQUFNNU0sQ0FBQyxHQUFDLENBQVI7QUFBVSxhQUFPLFVBQVM2YSxDQUFULEVBQVdoQixDQUFYLEVBQWE7QUFBQyxpQkFBU2xILENBQVQsR0FBWTtBQUFDM1MsVUFBQUEsQ0FBQyxHQUFDLENBQUMsSUFBSXNkLElBQUosRUFBSCxFQUFZMUQsQ0FBQyxDQUFDcFIsSUFBRixDQUFPK04sQ0FBUCxFQUFTc0UsQ0FBVCxDQUFaO0FBQXdCOztBQUFBLFlBQUlmLENBQUMsR0FBQyxDQUFDLElBQUl3RCxJQUFKLEVBQUQsR0FBVXRkLENBQWhCO0FBQWtCNE0sUUFBQUEsQ0FBQyxJQUFFb0csWUFBWSxDQUFDcEcsQ0FBRCxDQUFmLEVBQW1Ca04sQ0FBQyxHQUFDSCxDQUFGLElBQUssQ0FBQ3ZQLENBQUMsQ0FBQ21ULGNBQVIsSUFBd0IxRCxDQUF4QixHQUEwQmxILENBQUMsRUFBM0IsR0FBOEIvRixDQUFDLEdBQUNsRSxVQUFVLENBQUNpSyxDQUFELEVBQUdnSCxDQUFDLEdBQUNHLENBQUwsQ0FBN0Q7QUFBcUUsT0FBako7QUFBa0o7O0FBQUEsYUFBU2lDLENBQVQsR0FBWTtBQUFDLFFBQUVHLENBQUYsRUFBSWxjLENBQUMsQ0FBQzZHLE1BQUYsSUFBVXFWLENBQVYsSUFBYXhJLENBQUMsQ0FBQyxlQUFELENBQWxCO0FBQW9DOztBQUFBLGFBQVNBLENBQVQsQ0FBV2lHLENBQVgsRUFBYUMsQ0FBYixFQUFlaE4sQ0FBZixFQUFpQjtBQUFDLGFBQU0sQ0FBQyxFQUFFK00sQ0FBQyxHQUFDdlAsQ0FBQyxDQUFDdVAsQ0FBRCxDQUFMLENBQUQsS0FBYUEsQ0FBQyxDQUFDbEIsS0FBRixDQUFRbEMsQ0FBUixFQUFVLEdBQUc1RSxLQUFILENBQVNuSixJQUFULENBQWM2TCxTQUFkLEVBQXdCLENBQXhCLENBQVYsR0FBc0MsQ0FBQyxDQUFwRCxDQUFOO0FBQTZEOztBQUFBLFFBQUk2SCxDQUFDLEdBQUMsQ0FBTjtBQUFBLFFBQVE1QixDQUFDLEdBQUMsQ0FBQyxDQUFYO0FBQUEsUUFBYUMsQ0FBQyxHQUFDLENBQUMsQ0FBaEI7QUFBQSxRQUFrQlIsQ0FBQyxHQUFDLENBQUMsQ0FBckI7QUFBQSxRQUF1QndDLENBQUMsR0FBQyxXQUF6QjtBQUFBLFFBQXFDRyxDQUFDLEdBQUMsTUFBdkM7QUFBQSxRQUE4Q0YsQ0FBQyxHQUFDLE9BQWhEO0FBQUEsUUFBd0RiLENBQUMsR0FBQyxLQUExRDtBQUFBLFFBQWdFQyxDQUFDLEdBQUMsS0FBbEU7QUFBQSxRQUF3RUssQ0FBQyxHQUFDLFFBQTFFO0FBQUEsUUFBbUZXLENBQUMsR0FBQyxPQUFyRjtBQUFBLFFBQTZGZixDQUFDLEdBQUMsa0JBQS9GO0FBQWtILGdCQUFVelIsQ0FBQyxDQUFDb1QsSUFBWixJQUFrQjNDLENBQWxCLEdBQW9CZixDQUFDLEVBQXJCLEdBQXdCbE4sQ0FBQyxDQUFDK00sQ0FBRCxDQUFELENBQUsxYixFQUFMLENBQVF5ZSxDQUFDLEdBQUMsR0FBRixHQUFNL0osQ0FBZCxFQUFnQm1ILENBQWhCLENBQXhCO0FBQTJDOztBQUFBLFdBQVMxUCxDQUFULENBQVdBLENBQVgsRUFBYXlRLENBQWIsRUFBZTtBQUFDLFFBQUloQixDQUFDLEdBQUMsSUFBTjtBQUFBLFFBQVdsSCxDQUFDLEdBQUMvRixDQUFDLENBQUNyUCxNQUFGLENBQVMsRUFBVCxFQUFZc2MsQ0FBQyxDQUFDNEQsTUFBZCxFQUFxQjVDLENBQXJCLENBQWI7QUFBQSxRQUFxQ2YsQ0FBQyxHQUFDLEVBQXZDO0FBQUEsUUFBMEN4UCxDQUFDLEdBQUNxSSxDQUFDLENBQUMySSxJQUFGLEdBQU8sR0FBUCxHQUFZLEVBQUV0YixDQUExRDtBQUE0RCxXQUFPNlosQ0FBQyxDQUFDNEQsTUFBRixHQUFTLFVBQVM5RCxDQUFULEVBQVdwRCxDQUFYLEVBQWE7QUFBQyxhQUFPQSxDQUFDLEtBQUdxRCxDQUFKLEdBQU1qSCxDQUFDLENBQUNnSCxDQUFELENBQVAsSUFBWWhILENBQUMsQ0FBQ2dILENBQUQsQ0FBRCxHQUFLcEQsQ0FBTCxFQUFPc0QsQ0FBbkIsQ0FBUDtBQUE2QixLQUFwRCxFQUFxREEsQ0FBQyxDQUFDNkQsUUFBRixHQUFXLFVBQVMvRCxDQUFULEVBQVc7QUFBQyxhQUFPRyxDQUFDLENBQUMxUCxDQUFGLElBQUswUCxDQUFDLENBQUMxUCxDQUFGLENBQUksYUFBV3dDLENBQUMsQ0FBQ2lHLElBQUYsQ0FBTzhHLENBQVAsQ0FBWCxHQUFxQi9NLENBQUMsQ0FBQytNLENBQUQsQ0FBdEIsR0FBMEJBLENBQTlCLENBQUwsRUFBc0NFLENBQTdDO0FBQStDLEtBQTNILEVBQTRIQSxDQUFDLENBQUM4RCxRQUFGLEdBQVcsWUFBVTtBQUFDLGFBQU83RCxDQUFDLENBQUNXLENBQUYsR0FBSVgsQ0FBQyxDQUFDVyxDQUFGLEVBQUosR0FBVSxFQUFqQjtBQUFvQixLQUF0SyxFQUF1S1osQ0FBQyxDQUFDK0QsTUFBRixHQUFTLFVBQVNqRSxDQUFULEVBQVc7QUFBQyxhQUFPRyxDQUFDLENBQUNGLENBQUYsSUFBS0UsQ0FBQyxDQUFDRixDQUFGLENBQUksRUFBSixFQUFPLENBQUNELENBQVIsQ0FBTCxFQUFnQkUsQ0FBdkI7QUFBeUIsS0FBck4sRUFBc05BLENBQUMsQ0FBQ2dFLEtBQUYsR0FBUSxVQUFTbEUsQ0FBVCxFQUFXO0FBQUMsYUFBT0csQ0FBQyxDQUFDQSxDQUFGLElBQUtBLENBQUMsQ0FBQ0EsQ0FBRixDQUFJLGFBQVdsTixDQUFDLENBQUNpRyxJQUFGLENBQU84RyxDQUFQLENBQVgsR0FBcUIvTSxDQUFDLENBQUMrTSxDQUFELENBQXRCLEdBQTBCQSxDQUE5QixDQUFMLEVBQXNDRSxDQUE3QztBQUErQyxLQUF6UixFQUEwUkEsQ0FBQyxDQUFDaUUsT0FBRixHQUFVLFlBQVU7QUFBQyxhQUFPaEUsQ0FBQyxDQUFDRixDQUFGLElBQUtFLENBQUMsQ0FBQ0YsQ0FBRixDQUFJO0FBQUNZLFFBQUFBLEdBQUcsRUFBQyxDQUFDO0FBQU4sT0FBSixFQUFhLENBQUMsQ0FBZCxDQUFMLEVBQXNCWCxDQUE3QjtBQUErQixLQUE5VSxFQUErVUEsQ0FBQyxDQUFDdE0sT0FBRixHQUFVLFlBQVU7QUFBQyxhQUFPWCxDQUFDLENBQUMrRixDQUFDLENBQUNnSSxZQUFILENBQUQsQ0FBa0I3TixHQUFsQixDQUFzQixNQUFJeEMsQ0FBMUIsRUFBNEJ3UCxDQUFDLENBQUNGLENBQTlCLEdBQWlDaE4sQ0FBQyxDQUFDK00sQ0FBRCxDQUFELENBQUs3TSxHQUFMLENBQVMsTUFBSXhDLENBQWIsQ0FBakMsRUFBaUR3UCxDQUFDLEdBQUMsRUFBbkQsRUFBc0RGLENBQTdEO0FBQStELEtBQW5hLEVBQW9hckQsQ0FBQyxDQUFDc0QsQ0FBRCxFQUFHbEgsQ0FBSCxFQUFLdkksQ0FBTCxFQUFPMFAsQ0FBUCxFQUFTeFAsQ0FBVCxDQUFyYSxFQUFpYnFJLENBQUMsQ0FBQ29MLFNBQUYsR0FBWTNULENBQVosR0FBY3lQLENBQXRjO0FBQXdjOztBQUFBLE1BQUlqTixDQUFDLEdBQUMrTSxDQUFDLENBQUN2YixNQUFGLElBQVV1YixDQUFDLENBQUNxRSxLQUFsQjtBQUFBLE1BQXdCaGUsQ0FBQyxHQUFDLENBQTFCO0FBQUEsTUFBNEI2YSxDQUFDLEdBQUMsQ0FBQyxDQUEvQjtBQUFpQ2pPLEVBQUFBLENBQUMsQ0FBQ3RRLEVBQUYsQ0FBSzJoQixJQUFMLEdBQVVyUixDQUFDLENBQUN0USxFQUFGLENBQUs0aEIsSUFBTCxHQUFVLFVBQVN2RSxDQUFULEVBQVc7QUFBQyxXQUFPLElBQUl2UCxDQUFKLENBQU0sSUFBTixFQUFXdVAsQ0FBWCxDQUFQO0FBQXFCLEdBQXJELEVBQXNEL00sQ0FBQyxDQUFDcVIsSUFBRixHQUFPclIsQ0FBQyxDQUFDc1IsSUFBRixHQUFPLFVBQVN2RSxDQUFULEVBQVdwRCxDQUFYLEVBQWF2VyxDQUFiLEVBQWU7QUFBQyxRQUFHNE0sQ0FBQyxDQUFDdVIsVUFBRixDQUFhNUgsQ0FBYixNQUFrQnZXLENBQUMsR0FBQ3VXLENBQUYsRUFBSUEsQ0FBQyxHQUFDLEVBQXhCLEdBQTRCM0osQ0FBQyxDQUFDdVIsVUFBRixDQUFhbmUsQ0FBYixDQUEvQixFQUErQztBQUFDMlosTUFBQUEsQ0FBQyxHQUFDL00sQ0FBQyxDQUFDd1IsT0FBRixDQUFVekUsQ0FBVixJQUFhQSxDQUFiLEdBQWUsQ0FBQ0EsQ0FBRCxDQUFqQixFQUFxQnBELENBQUMsR0FBQzNKLENBQUMsQ0FBQ3dSLE9BQUYsQ0FBVTdILENBQVYsSUFBYUEsQ0FBYixHQUFlLENBQUNBLENBQUQsQ0FBdEM7O0FBQTBDLFdBQUksSUFBSXNFLENBQUMsR0FBQ3pRLENBQUMsQ0FBQ2pFLFNBQUYsQ0FBWXNYLE1BQWxCLEVBQXlCNUQsQ0FBQyxHQUFDZ0IsQ0FBQyxDQUFDSyxFQUFGLEtBQU9MLENBQUMsQ0FBQ0ssRUFBRixHQUFLLEVBQVosQ0FBM0IsRUFBMkN2SSxDQUFDLEdBQUMsQ0FBN0MsRUFBK0NtSCxDQUFDLEdBQUNILENBQUMsQ0FBQzlTLE1BQXZELEVBQThEOEwsQ0FBQyxHQUFDbUgsQ0FBaEUsRUFBa0VuSCxDQUFDLEVBQW5FO0FBQXNFLFNBQUNrSSxDQUFDLENBQUNsQixDQUFDLENBQUNoSCxDQUFELENBQUYsQ0FBRCxLQUFVaUgsQ0FBVixJQUFhaE4sQ0FBQyxDQUFDdVIsVUFBRixDQUFhdEQsQ0FBQyxDQUFDbEIsQ0FBQyxDQUFDaEgsQ0FBRCxDQUFGLENBQWQsQ0FBZCxNQUF1Q2tJLENBQUMsQ0FBQ2xCLENBQUMsQ0FBQ2hILENBQUQsQ0FBRixDQUFELEdBQVEzUyxDQUEvQztBQUF0RTs7QUFBd0gsV0FBSSxJQUFJc0ssQ0FBQyxHQUFDLENBQU4sRUFBUTRQLENBQUMsR0FBQzNELENBQUMsQ0FBQzFQLE1BQWhCLEVBQXVCeUQsQ0FBQyxHQUFDNFAsQ0FBekIsRUFBMkI1UCxDQUFDLEVBQTVCO0FBQStCdVAsUUFBQUEsQ0FBQyxDQUFDdEQsQ0FBQyxDQUFDak0sQ0FBRCxDQUFGLENBQUQsR0FBUXFQLENBQUMsQ0FBQyxDQUFELENBQVQ7QUFBL0I7QUFBNEM7QUFBQyxHQUFuVixFQUFvVnZQLENBQUMsQ0FBQ2pFLFNBQUYsQ0FBWXNYLE1BQVosR0FBbUI7QUFBQ25DLElBQUFBLElBQUksRUFBQyxNQUFOO0FBQWF5QyxJQUFBQSxTQUFTLEVBQUMsQ0FBQyxDQUF4QjtBQUEwQmpDLElBQUFBLFdBQVcsRUFBQyxDQUFDLENBQXZDO0FBQXlDMEIsSUFBQUEsSUFBSSxFQUFDLE1BQTlDO0FBQXFEUCxJQUFBQSxTQUFTLEVBQUMsR0FBL0Q7QUFBbUVqQixJQUFBQSxXQUFXLEVBQUMsQ0FBQyxDQUFoRjtBQUFrRnJCLElBQUFBLFlBQVksRUFBQ2hCLENBQS9GO0FBQWlHcUQsSUFBQUEsZUFBZSxFQUFDLE1BQWpIO0FBQXdIakMsSUFBQUEsU0FBUyxFQUFDLElBQWxJO0FBQXVJSCxJQUFBQSxZQUFZLEVBQUMsb0ZBQXBKO0FBQXlPRSxJQUFBQSxXQUFXLEVBQUMsSUFBclA7QUFBMFBiLElBQUFBLEtBQUssRUFBQyxDQUFDLENBQWpRO0FBQW1RRSxJQUFBQSxRQUFRLEVBQUMsQ0FBQyxDQUE3UTtBQUErUWtCLElBQUFBLFNBQVMsRUFBQyxVQUF6UjtBQUFvU0wsSUFBQUEsZUFBZSxFQUFDLGFBQXBUO0FBQWtVb0IsSUFBQUEsY0FBYyxFQUFDLFlBQWpWO0FBQThWQyxJQUFBQSxlQUFlLEVBQUMsYUFBOVc7QUFBNFhwQixJQUFBQSxlQUFlLEVBQUMsYUFBNVk7QUFBMFpTLElBQUFBLGtCQUFrQixFQUFDLGdCQUE3YTtBQUE4YlksSUFBQUEsZUFBZSxFQUFDLENBQUMsQ0FBL2M7QUFBaWRsQixJQUFBQSxXQUFXLEVBQUMsU0FBN2Q7QUFBdWVWLElBQUFBLFVBQVUsRUFBQyxRQUFsZjtBQUEyZm1DLElBQUFBLE1BQU0sRUFBQyxNQUFsZ0I7QUFBeWdCQyxJQUFBQSxVQUFVLEVBQUMsQ0FBcGhCO0FBQXNoQlMsSUFBQUEsY0FBYyxFQUFDLENBQUMsQ0FBdGlCO0FBQXdpQmxELElBQUFBLFFBQVEsRUFBQyxHQUFqakI7QUFBcWpCZ0UsSUFBQUEsVUFBVSxFQUFDekUsQ0FBaGtCO0FBQWtrQjBFLElBQUFBLFNBQVMsRUFBQzFFLENBQTVrQjtBQUE4a0IyRSxJQUFBQSxPQUFPLEVBQUMzRSxDQUF0bEI7QUFBd2xCNEUsSUFBQUEsYUFBYSxFQUFDNUU7QUFBdG1CLEdBQXZXLEVBQWc5QmhOLENBQUMsQ0FBQytNLENBQUQsQ0FBRCxDQUFLMWIsRUFBTCxDQUFRLE1BQVIsRUFBZSxZQUFVO0FBQUM0YyxJQUFBQSxDQUFDLEdBQUMsQ0FBQyxDQUFIO0FBQUssR0FBL0IsQ0FBaDlCO0FBQWkvQixDQUFoeEosQ0FBaXhKamMsTUFBanhKLENBQUQ7OztBQ0RBLENBQUMsVUFBU3ZDLENBQVQsRUFBWTtBQUNYLE1BQUlvaUIsT0FBTyxHQUFHcGlCLENBQUMsQ0FBQyxTQUFELENBQWY7QUFDQSxNQUFJcWlCLEtBQUssR0FBR0QsT0FBTyxDQUFDcFksSUFBUixDQUFhLE9BQWIsQ0FBWjs7QUFFQSxNQUFJc1ksWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBUzVTLEtBQVQsRUFBZ0I7QUFDakNBLElBQUFBLEtBQUssQ0FBQ08sY0FBTjtBQUNBb1MsSUFBQUEsS0FBSyxDQUFDeGdCLFdBQU4sQ0FBa0IsUUFBbEI7QUFDQXVnQixJQUFBQSxPQUFPLENBQUN2Z0IsV0FBUixDQUFvQixRQUFwQjtBQUNBdWdCLElBQUFBLE9BQU8sQ0FBQ3BZLElBQVIsQ0FBYSxPQUFiLEVBQXNCbkksV0FBdEIsQ0FBa0MsUUFBbEM7QUFDQTdCLElBQUFBLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0I2QixXQUFoQixDQUE0QixXQUE1QjtBQUNELEdBTkQ7O0FBUUEsTUFBSTBnQixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLEdBQVc7QUFDaEN2aUIsSUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUNHd1YsSUFESCxHQUVHM1QsV0FGSCxDQUVlLGtCQUZmO0FBSUE3QixJQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVE2QixXQUFSLENBQW9CLHdCQUFwQjtBQUNELEdBTkQ7O0FBUUF3Z0IsRUFBQUEsS0FBSyxDQUFDemdCLEVBQU4sQ0FBUyxPQUFULEVBQWtCLE9BQWxCLEVBQTJCMGdCLFlBQTNCO0FBQ0FGLEVBQUFBLE9BQU8sQ0FBQ3hnQixFQUFSLENBQVcsT0FBWCxFQUFvQixrQkFBcEIsRUFBd0MyZ0IsZ0JBQXhDO0FBQ0QsQ0F0QkQsRUFzQkd4Z0IsTUF0Qkg7Ozs7O0FDQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBO0FBQ0MsV0FBU0MsT0FBVCxFQUFrQjtBQUNmOztBQUNBLE1BQUksT0FBT0MsTUFBUCxLQUFrQixVQUFsQixJQUFnQ0EsTUFBTSxDQUFDQyxHQUEzQyxFQUFnRDtBQUM1Q0QsSUFBQUEsTUFBTSxDQUFDLENBQUMsUUFBRCxDQUFELEVBQWFELE9BQWIsQ0FBTjtBQUNILEdBRkQsTUFFTyxJQUFJLE9BQU9HLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDdkNDLElBQUFBLE1BQU0sQ0FBQ0QsT0FBUCxHQUFpQkgsT0FBTyxDQUFDSyxPQUFPLENBQUMsUUFBRCxDQUFSLENBQXhCO0FBQ0gsR0FGTSxNQUVBO0FBQ0hMLElBQUFBLE9BQU8sQ0FBQ0QsTUFBRCxDQUFQO0FBQ0g7QUFFSixDQVZBLEVBVUMsVUFBUy9CLENBQVQsRUFBWTtBQUNWOztBQUNBLE1BQUlzQyxLQUFLLEdBQUdDLE1BQU0sQ0FBQ0QsS0FBUCxJQUFnQixFQUE1Qjs7QUFFQUEsRUFBQUEsS0FBSyxHQUFJLFlBQVc7QUFFaEIsUUFBSUUsV0FBVyxHQUFHLENBQWxCOztBQUVBLGFBQVNGLEtBQVQsQ0FBZUcsT0FBZixFQUF3QnhCLFFBQXhCLEVBQWtDO0FBRTlCLFVBQUl5QixDQUFDLEdBQUcsSUFBUjtBQUFBLFVBQWNDLFlBQWQ7O0FBRUFELE1BQUFBLENBQUMsQ0FBQ3RDLFFBQUYsR0FBYTtBQUNUd0MsUUFBQUEsYUFBYSxFQUFFLElBRE47QUFFVEMsUUFBQUEsY0FBYyxFQUFFLEtBRlA7QUFHVEMsUUFBQUEsWUFBWSxFQUFFOUMsQ0FBQyxDQUFDeUMsT0FBRCxDQUhOO0FBSVRNLFFBQUFBLFVBQVUsRUFBRS9DLENBQUMsQ0FBQ3lDLE9BQUQsQ0FKSjtBQUtUTyxRQUFBQSxNQUFNLEVBQUUsSUFMQztBQU1UQyxRQUFBQSxRQUFRLEVBQUUsSUFORDtBQU9UQyxRQUFBQSxTQUFTLEVBQUUsOEhBUEY7QUFRVEMsUUFBQUEsU0FBUyxFQUFFLHNIQVJGO0FBU1RDLFFBQUFBLFFBQVEsRUFBRSxLQVREO0FBVVRDLFFBQUFBLGFBQWEsRUFBRSxJQVZOO0FBV1RDLFFBQUFBLFVBQVUsRUFBRSxLQVhIO0FBWVRDLFFBQUFBLGFBQWEsRUFBRSxNQVpOO0FBYVRDLFFBQUFBLE9BQU8sRUFBRSxNQWJBO0FBY1RDLFFBQUFBLFlBQVksRUFBRSxzQkFBU0MsTUFBVCxFQUFpQkMsQ0FBakIsRUFBb0I7QUFDOUIsaUJBQU8zRCxDQUFDLENBQUMsc0VBQUQsQ0FBRCxDQUEwRTRELElBQTFFLENBQStFRCxDQUFDLEdBQUcsQ0FBbkYsQ0FBUDtBQUNILFNBaEJRO0FBaUJURSxRQUFBQSxJQUFJLEVBQUUsS0FqQkc7QUFrQlRDLFFBQUFBLFNBQVMsRUFBRSxZQWxCRjtBQW1CVEMsUUFBQUEsU0FBUyxFQUFFLElBbkJGO0FBb0JUQyxRQUFBQSxNQUFNLEVBQUUsUUFwQkM7QUFxQlRDLFFBQUFBLFlBQVksRUFBRSxJQXJCTDtBQXNCVEMsUUFBQUEsSUFBSSxFQUFFLEtBdEJHO0FBdUJUQyxRQUFBQSxhQUFhLEVBQUUsS0F2Qk47QUF3QlRDLFFBQUFBLFFBQVEsRUFBRSxJQXhCRDtBQXlCVEMsUUFBQUEsWUFBWSxFQUFFLENBekJMO0FBMEJUQyxRQUFBQSxRQUFRLEVBQUUsVUExQkQ7QUEyQlRDLFFBQUFBLFdBQVcsRUFBRSxLQTNCSjtBQTRCVEMsUUFBQUEsWUFBWSxFQUFFLElBNUJMO0FBNkJUQyxRQUFBQSxZQUFZLEVBQUUsSUE3Qkw7QUE4QlRDLFFBQUFBLGdCQUFnQixFQUFFLEtBOUJUO0FBK0JUQyxRQUFBQSxTQUFTLEVBQUUsUUEvQkY7QUFnQ1RDLFFBQUFBLFVBQVUsRUFBRSxJQWhDSDtBQWlDVEMsUUFBQUEsSUFBSSxFQUFFLENBakNHO0FBa0NUQyxRQUFBQSxHQUFHLEVBQUUsS0FsQ0k7QUFtQ1RDLFFBQUFBLEtBQUssRUFBRSxFQW5DRTtBQW9DVEMsUUFBQUEsWUFBWSxFQUFFLENBcENMO0FBcUNUQyxRQUFBQSxZQUFZLEVBQUUsQ0FyQ0w7QUFzQ1RDLFFBQUFBLGNBQWMsRUFBRSxDQXRDUDtBQXVDVDVFLFFBQUFBLEtBQUssRUFBRSxHQXZDRTtBQXdDVDZFLFFBQUFBLEtBQUssRUFBRSxJQXhDRTtBQXlDVEMsUUFBQUEsWUFBWSxFQUFFLEtBekNMO0FBMENUQyxRQUFBQSxTQUFTLEVBQUUsSUExQ0Y7QUEyQ1RDLFFBQUFBLGNBQWMsRUFBRSxDQTNDUDtBQTRDVEMsUUFBQUEsTUFBTSxFQUFFLElBNUNDO0FBNkNUQyxRQUFBQSxZQUFZLEVBQUUsSUE3Q0w7QUE4Q1RDLFFBQUFBLGFBQWEsRUFBRSxLQTlDTjtBQStDVEMsUUFBQUEsUUFBUSxFQUFFLEtBL0NEO0FBZ0RUQyxRQUFBQSxlQUFlLEVBQUUsS0FoRFI7QUFpRFRDLFFBQUFBLGNBQWMsRUFBRSxJQWpEUDtBQWtEVEMsUUFBQUEsTUFBTSxFQUFFO0FBbERDLE9BQWI7QUFxREFuRCxNQUFBQSxDQUFDLENBQUNvRCxRQUFGLEdBQWE7QUFDVEMsUUFBQUEsU0FBUyxFQUFFLEtBREY7QUFFVEMsUUFBQUEsUUFBUSxFQUFFLEtBRkQ7QUFHVEMsUUFBQUEsYUFBYSxFQUFFLElBSE47QUFJVEMsUUFBQUEsZ0JBQWdCLEVBQUUsQ0FKVDtBQUtUQyxRQUFBQSxXQUFXLEVBQUUsSUFMSjtBQU1UQyxRQUFBQSxZQUFZLEVBQUUsQ0FOTDtBQU9UQyxRQUFBQSxTQUFTLEVBQUUsQ0FQRjtBQVFUQyxRQUFBQSxLQUFLLEVBQUUsSUFSRTtBQVNUQyxRQUFBQSxTQUFTLEVBQUUsSUFURjtBQVVUQyxRQUFBQSxVQUFVLEVBQUUsSUFWSDtBQVdUQyxRQUFBQSxTQUFTLEVBQUUsQ0FYRjtBQVlUQyxRQUFBQSxVQUFVLEVBQUUsSUFaSDtBQWFUQyxRQUFBQSxVQUFVLEVBQUUsSUFiSDtBQWNUQyxRQUFBQSxVQUFVLEVBQUUsSUFkSDtBQWVUQyxRQUFBQSxVQUFVLEVBQUUsSUFmSDtBQWdCVEMsUUFBQUEsV0FBVyxFQUFFLElBaEJKO0FBaUJUQyxRQUFBQSxPQUFPLEVBQUUsSUFqQkE7QUFrQlRDLFFBQUFBLE9BQU8sRUFBRSxLQWxCQTtBQW1CVEMsUUFBQUEsV0FBVyxFQUFFLENBbkJKO0FBb0JUQyxRQUFBQSxTQUFTLEVBQUUsSUFwQkY7QUFxQlRDLFFBQUFBLEtBQUssRUFBRSxJQXJCRTtBQXNCVEMsUUFBQUEsV0FBVyxFQUFFLEVBdEJKO0FBdUJUQyxRQUFBQSxpQkFBaUIsRUFBRSxLQXZCVjtBQXdCVEMsUUFBQUEsU0FBUyxFQUFFO0FBeEJGLE9BQWI7QUEyQkF0SCxNQUFBQSxDQUFDLENBQUNrQixNQUFGLENBQVN3QixDQUFULEVBQVlBLENBQUMsQ0FBQ29ELFFBQWQ7QUFFQXBELE1BQUFBLENBQUMsQ0FBQzZFLGdCQUFGLEdBQXFCLElBQXJCO0FBQ0E3RSxNQUFBQSxDQUFDLENBQUM4RSxRQUFGLEdBQWEsSUFBYjtBQUNBOUUsTUFBQUEsQ0FBQyxDQUFDK0UsUUFBRixHQUFhLElBQWI7QUFDQS9FLE1BQUFBLENBQUMsQ0FBQ2dGLFdBQUYsR0FBZ0IsRUFBaEI7QUFDQWhGLE1BQUFBLENBQUMsQ0FBQ2lGLGtCQUFGLEdBQXVCLEVBQXZCO0FBQ0FqRixNQUFBQSxDQUFDLENBQUNrRixjQUFGLEdBQW1CLEtBQW5CO0FBQ0FsRixNQUFBQSxDQUFDLENBQUNtRixRQUFGLEdBQWEsS0FBYjtBQUNBbkYsTUFBQUEsQ0FBQyxDQUFDb0YsV0FBRixHQUFnQixLQUFoQjtBQUNBcEYsTUFBQUEsQ0FBQyxDQUFDcUYsTUFBRixHQUFXLFFBQVg7QUFDQXJGLE1BQUFBLENBQUMsQ0FBQ3NGLE1BQUYsR0FBVyxJQUFYO0FBQ0F0RixNQUFBQSxDQUFDLENBQUN1RixZQUFGLEdBQWlCLElBQWpCO0FBQ0F2RixNQUFBQSxDQUFDLENBQUNpQyxTQUFGLEdBQWMsSUFBZDtBQUNBakMsTUFBQUEsQ0FBQyxDQUFDd0YsUUFBRixHQUFhLENBQWI7QUFDQXhGLE1BQUFBLENBQUMsQ0FBQ3lGLFdBQUYsR0FBZ0IsSUFBaEI7QUFDQXpGLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsR0FBWXBJLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBYjtBQUNBQyxNQUFBQSxDQUFDLENBQUMyRixZQUFGLEdBQWlCLElBQWpCO0FBQ0EzRixNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLElBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLElBQW5CO0FBQ0E3RixNQUFBQSxDQUFDLENBQUM4RixnQkFBRixHQUFxQixrQkFBckI7QUFDQTlGLE1BQUFBLENBQUMsQ0FBQytGLFdBQUYsR0FBZ0IsQ0FBaEI7QUFDQS9GLE1BQUFBLENBQUMsQ0FBQ2dHLFdBQUYsR0FBZ0IsSUFBaEI7QUFFQS9GLE1BQUFBLFlBQVksR0FBRzNDLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRCxDQUFXa0csSUFBWCxDQUFnQixPQUFoQixLQUE0QixFQUEzQztBQUVBakcsTUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixHQUFZSCxDQUFDLENBQUNrQixNQUFGLENBQVMsRUFBVCxFQUFhd0IsQ0FBQyxDQUFDdEMsUUFBZixFQUF5QmEsUUFBekIsRUFBbUMwQixZQUFuQyxDQUFaO0FBRUFELE1BQUFBLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUN2QyxPQUFGLENBQVVrRSxZQUEzQjtBQUVBM0IsTUFBQUEsQ0FBQyxDQUFDa0csZ0JBQUYsR0FBcUJsRyxDQUFDLENBQUN2QyxPQUF2Qjs7QUFFQSxVQUFJLE9BQU8wSSxRQUFRLENBQUNDLFNBQWhCLEtBQThCLFdBQWxDLEVBQStDO0FBQzNDcEcsUUFBQUEsQ0FBQyxDQUFDcUYsTUFBRixHQUFXLFdBQVg7QUFDQXJGLFFBQUFBLENBQUMsQ0FBQzhGLGdCQUFGLEdBQXFCLHFCQUFyQjtBQUNILE9BSEQsTUFHTyxJQUFJLE9BQU9LLFFBQVEsQ0FBQ0UsWUFBaEIsS0FBaUMsV0FBckMsRUFBa0Q7QUFDckRyRyxRQUFBQSxDQUFDLENBQUNxRixNQUFGLEdBQVcsY0FBWDtBQUNBckYsUUFBQUEsQ0FBQyxDQUFDOEYsZ0JBQUYsR0FBcUIsd0JBQXJCO0FBQ0g7O0FBRUQ5RixNQUFBQSxDQUFDLENBQUNzRyxRQUFGLEdBQWFoSixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNzRyxRQUFWLEVBQW9CdEcsQ0FBcEIsQ0FBYjtBQUNBQSxNQUFBQSxDQUFDLENBQUN3RyxhQUFGLEdBQWtCbEosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDd0csYUFBVixFQUF5QnhHLENBQXpCLENBQWxCO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3lHLGdCQUFGLEdBQXFCbkosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDeUcsZ0JBQVYsRUFBNEJ6RyxDQUE1QixDQUFyQjtBQUNBQSxNQUFBQSxDQUFDLENBQUMwRyxXQUFGLEdBQWdCcEosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDMEcsV0FBVixFQUF1QjFHLENBQXZCLENBQWhCO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzJHLFlBQUYsR0FBaUJySixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUMyRyxZQUFWLEVBQXdCM0csQ0FBeEIsQ0FBakI7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNEcsYUFBRixHQUFrQnRKLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQzRHLGFBQVYsRUFBeUI1RyxDQUF6QixDQUFsQjtBQUNBQSxNQUFBQSxDQUFDLENBQUM2RyxXQUFGLEdBQWdCdkosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDNkcsV0FBVixFQUF1QjdHLENBQXZCLENBQWhCO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzhHLFlBQUYsR0FBaUJ4SixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUM4RyxZQUFWLEVBQXdCOUcsQ0FBeEIsQ0FBakI7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDK0csV0FBRixHQUFnQnpKLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQytHLFdBQVYsRUFBdUIvRyxDQUF2QixDQUFoQjtBQUNBQSxNQUFBQSxDQUFDLENBQUNnSCxVQUFGLEdBQWUxSixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnSCxVQUFWLEVBQXNCaEgsQ0FBdEIsQ0FBZjtBQUVBQSxNQUFBQSxDQUFDLENBQUNGLFdBQUYsR0FBZ0JBLFdBQVcsRUFBM0IsQ0F2SThCLENBeUk5QjtBQUNBO0FBQ0E7O0FBQ0FFLE1BQUFBLENBQUMsQ0FBQ2lILFFBQUYsR0FBYSwyQkFBYjs7QUFHQWpILE1BQUFBLENBQUMsQ0FBQ2tILG1CQUFGOztBQUNBbEgsTUFBQUEsQ0FBQyxDQUFDbUgsSUFBRixDQUFPLElBQVA7QUFFSDs7QUFFRCxXQUFPdkgsS0FBUDtBQUVILEdBMUpRLEVBQVQ7O0FBNEpBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCQyxXQUFoQixHQUE4QixZQUFXO0FBQ3JDLFFBQUlySCxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFja0QsSUFBZCxDQUFtQixlQUFuQixFQUFvQ0MsSUFBcEMsQ0FBeUM7QUFDckMscUJBQWU7QUFEc0IsS0FBekMsRUFFR0QsSUFGSCxDQUVRLDBCQUZSLEVBRW9DQyxJQUZwQyxDQUV5QztBQUNyQyxrQkFBWTtBQUR5QixLQUZ6QztBQU1ILEdBVEQ7O0FBV0EzSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCSSxRQUFoQixHQUEyQjVILEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JLLFFBQWhCLEdBQTJCLFVBQVNDLE1BQVQsRUFBaUJDLEtBQWpCLEVBQXdCQyxTQUF4QixFQUFtQztBQUVyRixRQUFJNUgsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxPQUFPMkgsS0FBUCxLQUFrQixTQUF0QixFQUFpQztBQUM3QkMsTUFBQUEsU0FBUyxHQUFHRCxLQUFaO0FBQ0FBLE1BQUFBLEtBQUssR0FBRyxJQUFSO0FBQ0gsS0FIRCxNQUdPLElBQUlBLEtBQUssR0FBRyxDQUFSLElBQWNBLEtBQUssSUFBSTNILENBQUMsQ0FBQ2tFLFVBQTdCLEVBQTBDO0FBQzdDLGFBQU8sS0FBUDtBQUNIOztBQUVEbEUsSUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjs7QUFFQSxRQUFJLE9BQU9GLEtBQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDNUIsVUFBSUEsS0FBSyxLQUFLLENBQVYsSUFBZTNILENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlELE1BQVYsS0FBcUIsQ0FBeEMsRUFBMkM7QUFDdkN4SyxRQUFBQSxDQUFDLENBQUNvSyxNQUFELENBQUQsQ0FBVUssUUFBVixDQUFtQi9ILENBQUMsQ0FBQ29FLFdBQXJCO0FBQ0gsT0FGRCxNQUVPLElBQUl3RCxTQUFKLEVBQWU7QUFDbEJ0SyxRQUFBQSxDQUFDLENBQUNvSyxNQUFELENBQUQsQ0FBVU0sWUFBVixDQUF1QmhJLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYU4sS0FBYixDQUF2QjtBQUNILE9BRk0sTUFFQTtBQUNIckssUUFBQUEsQ0FBQyxDQUFDb0ssTUFBRCxDQUFELENBQVVRLFdBQVYsQ0FBc0JsSSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWFOLEtBQWIsQ0FBdEI7QUFDSDtBQUNKLEtBUkQsTUFRTztBQUNILFVBQUlDLFNBQVMsS0FBSyxJQUFsQixFQUF3QjtBQUNwQnRLLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRCxDQUFVUyxTQUFWLENBQW9CbkksQ0FBQyxDQUFDb0UsV0FBdEI7QUFDSCxPQUZELE1BRU87QUFDSDlHLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRCxDQUFVSyxRQUFWLENBQW1CL0gsQ0FBQyxDQUFDb0UsV0FBckI7QUFDSDtBQUNKOztBQUVEcEUsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixHQUFZckUsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixLQUFLM0ssT0FBTCxDQUFhNEUsS0FBcEMsQ0FBWjs7QUFFQXJDLElBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsS0FBSzNLLE9BQUwsQ0FBYTRFLEtBQXBDLEVBQTJDZ0csTUFBM0M7O0FBRUFySSxJQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNrRSxNQUFkLENBQXFCdEksQ0FBQyxDQUFDcUUsT0FBdkI7O0FBRUFyRSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVV6RixJQUFWLENBQWUsVUFBUytJLEtBQVQsRUFBZ0I1SCxPQUFoQixFQUF5QjtBQUNwQ3pDLE1BQUFBLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRCxDQUFXd0gsSUFBWCxDQUFnQixrQkFBaEIsRUFBb0NJLEtBQXBDO0FBQ0gsS0FGRDs7QUFJQTNILElBQUFBLENBQUMsQ0FBQzJGLFlBQUYsR0FBaUIzRixDQUFDLENBQUNxRSxPQUFuQjs7QUFFQXJFLElBQUFBLENBQUMsQ0FBQ3VJLE1BQUY7QUFFSCxHQTNDRDs7QUE2Q0EzSSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCb0IsYUFBaEIsR0FBZ0MsWUFBVztBQUN2QyxRQUFJeEksQ0FBQyxHQUFHLElBQVI7O0FBQ0EsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixLQUEyQixDQUEzQixJQUFnQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBDLGNBQVYsS0FBNkIsSUFBN0QsSUFBcUVILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBaEcsRUFBdUc7QUFDbkcsVUFBSXlGLFlBQVksR0FBR3pJLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYWpJLENBQUMsQ0FBQzBELFlBQWYsRUFBNkJnRixXQUE3QixDQUF5QyxJQUF6QyxDQUFuQjs7QUFDQTFJLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUWtFLE9BQVIsQ0FBZ0I7QUFDWjVLLFFBQUFBLE1BQU0sRUFBRTBLO0FBREksT0FBaEIsRUFFR3pJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FGYjtBQUdIO0FBQ0osR0FSRDs7QUFVQWdDLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J3QixZQUFoQixHQUErQixVQUFTQyxVQUFULEVBQXFCQyxRQUFyQixFQUErQjtBQUUxRCxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFBQSxRQUNJL0ksQ0FBQyxHQUFHLElBRFI7O0FBR0FBLElBQUFBLENBQUMsQ0FBQ3dJLGFBQUY7O0FBRUEsUUFBSXhJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBbEIsSUFBMEJwQyxDQUFDLENBQUN2QyxPQUFGLENBQVV1RixRQUFWLEtBQXVCLEtBQXJELEVBQTREO0FBQ3hENkYsTUFBQUEsVUFBVSxHQUFHLENBQUNBLFVBQWQ7QUFDSDs7QUFDRCxRQUFJN0ksQ0FBQyxDQUFDMkUsaUJBQUYsS0FBd0IsS0FBNUIsRUFBbUM7QUFDL0IsVUFBSTNFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUJoRCxRQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWN1RSxPQUFkLENBQXNCO0FBQ2xCSyxVQUFBQSxJQUFJLEVBQUVIO0FBRFksU0FBdEIsRUFFRzdJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FGYixFQUVvQm9DLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZELE1BRjlCLEVBRXNDd0gsUUFGdEM7QUFHSCxPQUpELE1BSU87QUFDSDlJLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3VFLE9BQWQsQ0FBc0I7QUFDbEJNLFVBQUFBLEdBQUcsRUFBRUo7QUFEYSxTQUF0QixFQUVHN0ksQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQUZiLEVBRW9Cb0MsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkQsTUFGOUIsRUFFc0N3SCxRQUZ0QztBQUdIO0FBRUosS0FYRCxNQVdPO0FBRUgsVUFBSTlJLENBQUMsQ0FBQ2tGLGNBQUYsS0FBcUIsS0FBekIsRUFBZ0M7QUFDNUIsWUFBSWxGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEJwQyxVQUFBQSxDQUFDLENBQUN5RCxXQUFGLEdBQWdCLENBQUV6RCxDQUFDLENBQUN5RCxXQUFwQjtBQUNIOztBQUNEbkcsUUFBQUEsQ0FBQyxDQUFDO0FBQ0U0TCxVQUFBQSxTQUFTLEVBQUVsSixDQUFDLENBQUN5RDtBQURmLFNBQUQsQ0FBRCxDQUVHa0YsT0FGSCxDQUVXO0FBQ1BPLFVBQUFBLFNBQVMsRUFBRUw7QUFESixTQUZYLEVBSUc7QUFDQ00sVUFBQUEsUUFBUSxFQUFFbkosQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQURyQjtBQUVDMEQsVUFBQUEsTUFBTSxFQUFFdEIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkQsTUFGbkI7QUFHQzhILFVBQUFBLElBQUksRUFBRSxjQUFTQyxHQUFULEVBQWM7QUFDaEJBLFlBQUFBLEdBQUcsR0FBR0MsSUFBSSxDQUFDQyxJQUFMLENBQVVGLEdBQVYsQ0FBTjs7QUFDQSxnQkFBSXJKLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUIrRixjQUFBQSxTQUFTLENBQUMvSSxDQUFDLENBQUM4RSxRQUFILENBQVQsR0FBd0IsZUFDcEJ1RSxHQURvQixHQUNkLFVBRFY7O0FBRUFySixjQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCZ0ssU0FBbEI7QUFDSCxhQUpELE1BSU87QUFDSEEsY0FBQUEsU0FBUyxDQUFDL0ksQ0FBQyxDQUFDOEUsUUFBSCxDQUFULEdBQXdCLG1CQUNwQnVFLEdBRG9CLEdBQ2QsS0FEVjs7QUFFQXJKLGNBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3JGLEdBQWQsQ0FBa0JnSyxTQUFsQjtBQUNIO0FBQ0osV0FkRjtBQWVDUyxVQUFBQSxRQUFRLEVBQUUsb0JBQVc7QUFDakIsZ0JBQUlWLFFBQUosRUFBYztBQUNWQSxjQUFBQSxRQUFRLENBQUNXLElBQVQ7QUFDSDtBQUNKO0FBbkJGLFNBSkg7QUEwQkgsT0E5QkQsTUE4Qk87QUFFSHpKLFFBQUFBLENBQUMsQ0FBQzBKLGVBQUY7O0FBQ0FiLFFBQUFBLFVBQVUsR0FBR1MsSUFBSSxDQUFDQyxJQUFMLENBQVVWLFVBQVYsQ0FBYjs7QUFFQSxZQUFJN0ksQ0FBQyxDQUFDdkMsT0FBRixDQUFVdUYsUUFBVixLQUF1QixLQUEzQixFQUFrQztBQUM5QitGLFVBQUFBLFNBQVMsQ0FBQy9JLENBQUMsQ0FBQzhFLFFBQUgsQ0FBVCxHQUF3QixpQkFBaUIrRCxVQUFqQixHQUE4QixlQUF0RDtBQUNILFNBRkQsTUFFTztBQUNIRSxVQUFBQSxTQUFTLENBQUMvSSxDQUFDLENBQUM4RSxRQUFILENBQVQsR0FBd0IscUJBQXFCK0QsVUFBckIsR0FBa0MsVUFBMUQ7QUFDSDs7QUFDRDdJLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3JGLEdBQWQsQ0FBa0JnSyxTQUFsQjs7QUFFQSxZQUFJRCxRQUFKLEVBQWM7QUFDVmEsVUFBQUEsVUFBVSxDQUFDLFlBQVc7QUFFbEIzSixZQUFBQSxDQUFDLENBQUM0SixpQkFBRjs7QUFFQWQsWUFBQUEsUUFBUSxDQUFDVyxJQUFUO0FBQ0gsV0FMUyxFQUtQekosQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQUxILENBQVY7QUFNSDtBQUVKO0FBRUo7QUFFSixHQTlFRDs7QUFnRkFnQyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCeUMsWUFBaEIsR0FBK0IsWUFBVztBQUV0QyxRQUFJN0osQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJTyxRQUFRLEdBQUdQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThDLFFBRHpCOztBQUdBLFFBQUtBLFFBQVEsSUFBSUEsUUFBUSxLQUFLLElBQTlCLEVBQXFDO0FBQ2pDQSxNQUFBQSxRQUFRLEdBQUdqRCxDQUFDLENBQUNpRCxRQUFELENBQUQsQ0FBWXVKLEdBQVosQ0FBZ0I5SixDQUFDLENBQUMwRixPQUFsQixDQUFYO0FBQ0g7O0FBRUQsV0FBT25GLFFBQVA7QUFFSCxHQVhEOztBQWFBWCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCN0csUUFBaEIsR0FBMkIsVUFBU29ILEtBQVQsRUFBZ0I7QUFFdkMsUUFBSTNILENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSU8sUUFBUSxHQUFHUCxDQUFDLENBQUM2SixZQUFGLEVBRGY7O0FBR0EsUUFBS3RKLFFBQVEsS0FBSyxJQUFiLElBQXFCLFFBQU9BLFFBQVAsTUFBb0IsUUFBOUMsRUFBeUQ7QUFDckRBLE1BQUFBLFFBQVEsQ0FBQzNCLElBQVQsQ0FBYyxZQUFXO0FBQ3JCLFlBQUltTCxNQUFNLEdBQUd6TSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVEwTSxLQUFSLENBQWMsVUFBZCxDQUFiOztBQUNBLFlBQUcsQ0FBQ0QsTUFBTSxDQUFDbkYsU0FBWCxFQUFzQjtBQUNsQm1GLFVBQUFBLE1BQU0sQ0FBQ0UsWUFBUCxDQUFvQnRDLEtBQXBCLEVBQTJCLElBQTNCO0FBQ0g7QUFDSixPQUxEO0FBTUg7QUFFSixHQWREOztBQWdCQS9ILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JzQyxlQUFoQixHQUFrQyxVQUFTckgsS0FBVCxFQUFnQjtBQUU5QyxRQUFJckMsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJa0ssVUFBVSxHQUFHLEVBRGpCOztBQUdBLFFBQUlsSyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCMEksTUFBQUEsVUFBVSxDQUFDbEssQ0FBQyxDQUFDNkYsY0FBSCxDQUFWLEdBQStCN0YsQ0FBQyxDQUFDNEYsYUFBRixHQUFrQixHQUFsQixHQUF3QjVGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FBbEMsR0FBMEMsS0FBMUMsR0FBa0RvQyxDQUFDLENBQUN2QyxPQUFGLENBQVVxRCxPQUEzRjtBQUNILEtBRkQsTUFFTztBQUNIb0osTUFBQUEsVUFBVSxDQUFDbEssQ0FBQyxDQUFDNkYsY0FBSCxDQUFWLEdBQStCLGFBQWE3RixDQUFDLENBQUN2QyxPQUFGLENBQVVHLEtBQXZCLEdBQStCLEtBQS9CLEdBQXVDb0MsQ0FBQyxDQUFDdkMsT0FBRixDQUFVcUQsT0FBaEY7QUFDSDs7QUFFRCxRQUFJZCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCeEIsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQm1MLFVBQWxCO0FBQ0gsS0FGRCxNQUVPO0FBQ0hsSyxNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWE1RixLQUFiLEVBQW9CdEQsR0FBcEIsQ0FBd0JtTCxVQUF4QjtBQUNIO0FBRUosR0FqQkQ7O0FBbUJBdEssRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmQsUUFBaEIsR0FBMkIsWUFBVztBQUVsQyxRQUFJdEcsQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3dHLGFBQUY7O0FBRUEsUUFBS3hHLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTlCLEVBQTZDO0FBQ3pDdkMsTUFBQUEsQ0FBQyxDQUFDdUQsYUFBRixHQUFrQjRHLFdBQVcsQ0FBRW5LLENBQUMsQ0FBQ3lHLGdCQUFKLEVBQXNCekcsQ0FBQyxDQUFDdkMsT0FBRixDQUFVa0QsYUFBaEMsQ0FBN0I7QUFDSDtBQUVKLEdBVkQ7O0FBWUFmLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JaLGFBQWhCLEdBQWdDLFlBQVc7QUFFdkMsUUFBSXhHLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3VELGFBQU4sRUFBcUI7QUFDakI2RyxNQUFBQSxhQUFhLENBQUNwSyxDQUFDLENBQUN1RCxhQUFILENBQWI7QUFDSDtBQUVKLEdBUkQ7O0FBVUEzRCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCWCxnQkFBaEIsR0FBbUMsWUFBVztBQUUxQyxRQUFJekcsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJcUssT0FBTyxHQUFHckssQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBRHpDOztBQUdBLFFBQUssQ0FBQ3hDLENBQUMsQ0FBQ3NGLE1BQUgsSUFBYSxDQUFDdEYsQ0FBQyxDQUFDb0YsV0FBaEIsSUFBK0IsQ0FBQ3BGLENBQUMsQ0FBQ21GLFFBQXZDLEVBQWtEO0FBRTlDLFVBQUtuRixDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLEtBQTVCLEVBQW9DO0FBRWhDLFlBQUsxQixDQUFDLENBQUMyRCxTQUFGLEtBQWdCLENBQWhCLElBQXVCM0QsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQixDQUFuQixLQUE2QjFELENBQUMsQ0FBQ2tFLFVBQUYsR0FBZSxDQUF0RSxFQUEyRTtBQUN2RWxFLFVBQUFBLENBQUMsQ0FBQzJELFNBQUYsR0FBYyxDQUFkO0FBQ0gsU0FGRCxNQUlLLElBQUszRCxDQUFDLENBQUMyRCxTQUFGLEtBQWdCLENBQXJCLEVBQXlCO0FBRTFCMEcsVUFBQUEsT0FBTyxHQUFHckssQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXJDOztBQUVBLGNBQUt4QyxDQUFDLENBQUMwRCxZQUFGLEdBQWlCLENBQWpCLEtBQXVCLENBQTVCLEVBQWdDO0FBQzVCMUQsWUFBQUEsQ0FBQyxDQUFDMkQsU0FBRixHQUFjLENBQWQ7QUFDSDtBQUVKO0FBRUo7O0FBRUQzRCxNQUFBQSxDQUFDLENBQUNpSyxZQUFGLENBQWdCSSxPQUFoQjtBQUVIO0FBRUosR0E3QkQ7O0FBK0JBekssRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmtELFdBQWhCLEdBQThCLFlBQVc7QUFFckMsUUFBSXRLLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZDLE1BQVYsS0FBcUIsSUFBekIsRUFBZ0M7QUFFNUJOLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUYsR0FBZTNHLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStDLFNBQVgsQ0FBRCxDQUF1QjlCLFFBQXZCLENBQWdDLGFBQWhDLENBQWY7QUFDQXNCLE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUYsR0FBZTFHLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWdELFNBQVgsQ0FBRCxDQUF1Qi9CLFFBQXZCLENBQWdDLGFBQWhDLENBQWY7O0FBRUEsVUFBSXNCLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTdCLEVBQTRDO0FBRXhDdkMsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFhc0csV0FBYixDQUF5QixjQUF6QixFQUF5Q0MsVUFBekMsQ0FBb0Qsc0JBQXBEOztBQUNBeEssUUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhdUcsV0FBYixDQUF5QixjQUF6QixFQUF5Q0MsVUFBekMsQ0FBb0Qsc0JBQXBEOztBQUVBLFlBQUl4SyxDQUFDLENBQUNpSCxRQUFGLENBQVd3RCxJQUFYLENBQWdCekssQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0MsU0FBMUIsQ0FBSixFQUEwQztBQUN0Q1IsVUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFha0UsU0FBYixDQUF1Qm5JLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJDLFlBQWpDO0FBQ0g7O0FBRUQsWUFBSUosQ0FBQyxDQUFDaUgsUUFBRixDQUFXd0QsSUFBWCxDQUFnQnpLLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWdELFNBQTFCLENBQUosRUFBMEM7QUFDdENULFVBQUFBLENBQUMsQ0FBQ2dFLFVBQUYsQ0FBYStELFFBQWIsQ0FBc0IvSCxDQUFDLENBQUN2QyxPQUFGLENBQVUyQyxZQUFoQztBQUNIOztBQUVELFlBQUlKLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsSUFBM0IsRUFBaUM7QUFDN0IxQixVQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQ0t2RixRQURMLENBQ2MsZ0JBRGQsRUFFSzZJLElBRkwsQ0FFVSxlQUZWLEVBRTJCLE1BRjNCO0FBR0g7QUFFSixPQW5CRCxNQW1CTztBQUVIdkgsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFheUcsR0FBYixDQUFrQjFLLENBQUMsQ0FBQ2dFLFVBQXBCLEVBRUt0RixRQUZMLENBRWMsY0FGZCxFQUdLNkksSUFITCxDQUdVO0FBQ0YsMkJBQWlCLE1BRGY7QUFFRixzQkFBWTtBQUZWLFNBSFY7QUFRSDtBQUVKO0FBRUosR0ExQ0Q7O0FBNENBM0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnVELFNBQWhCLEdBQTRCLFlBQVc7QUFFbkMsUUFBSTNLLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWlCLENBREo7QUFBQSxRQUNPMkosR0FEUDs7QUFHQSxRQUFJNUssQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEQsSUFBVixLQUFtQixJQUFuQixJQUEyQm5CLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXhELEVBQXNFO0FBRWxFdkMsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVaEgsUUFBVixDQUFtQixjQUFuQjs7QUFFQWtNLE1BQUFBLEdBQUcsR0FBR3ROLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWW9CLFFBQVosQ0FBcUJzQixDQUFDLENBQUN2QyxPQUFGLENBQVUyRCxTQUEvQixDQUFOOztBQUVBLFdBQUtILENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsSUFBSWpCLENBQUMsQ0FBQzZLLFdBQUYsRUFBakIsRUFBa0M1SixDQUFDLElBQUksQ0FBdkMsRUFBMEM7QUFDdEMySixRQUFBQSxHQUFHLENBQUN0QyxNQUFKLENBQVdoTCxDQUFDLENBQUMsUUFBRCxDQUFELENBQVlnTCxNQUFaLENBQW1CdEksQ0FBQyxDQUFDdkMsT0FBRixDQUFVc0QsWUFBVixDQUF1QjBJLElBQXZCLENBQTRCLElBQTVCLEVBQWtDekosQ0FBbEMsRUFBcUNpQixDQUFyQyxDQUFuQixDQUFYO0FBQ0g7O0FBRURqQixNQUFBQSxDQUFDLENBQUM0RCxLQUFGLEdBQVVnSCxHQUFHLENBQUM3QyxRQUFKLENBQWEvSCxDQUFDLENBQUN2QyxPQUFGLENBQVU0QyxVQUF2QixDQUFWOztBQUVBTCxNQUFBQSxDQUFDLENBQUM0RCxLQUFGLENBQVEwRCxJQUFSLENBQWEsSUFBYixFQUFtQndELEtBQW5CLEdBQTJCcE0sUUFBM0IsQ0FBb0MsY0FBcEMsRUFBb0Q2SSxJQUFwRCxDQUF5RCxhQUF6RCxFQUF3RSxPQUF4RTtBQUVIO0FBRUosR0FyQkQ7O0FBdUJBM0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjJELFFBQWhCLEdBQTJCLFlBQVc7QUFFbEMsUUFBSS9LLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLEdBQ0lyRSxDQUFDLENBQUMwRixPQUFGLENBQ0swQyxRQURMLENBQ2VwSSxDQUFDLENBQUN2QyxPQUFGLENBQVU0RSxLQUFWLEdBQWtCLHFCQURqQyxFQUVLM0QsUUFGTCxDQUVjLGFBRmQsQ0FESjtBQUtBc0IsSUFBQUEsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDcUUsT0FBRixDQUFVeUQsTUFBekI7O0FBRUE5SCxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVV6RixJQUFWLENBQWUsVUFBUytJLEtBQVQsRUFBZ0I1SCxPQUFoQixFQUF5QjtBQUNwQ3pDLE1BQUFBLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRCxDQUNLd0gsSUFETCxDQUNVLGtCQURWLEVBQzhCSSxLQUQ5QixFQUVLMUIsSUFGTCxDQUVVLGlCQUZWLEVBRTZCM0ksQ0FBQyxDQUFDeUMsT0FBRCxDQUFELENBQVd3SCxJQUFYLENBQWdCLE9BQWhCLEtBQTRCLEVBRnpEO0FBR0gsS0FKRDs7QUFNQXZILElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVWhILFFBQVYsQ0FBbUIsY0FBbkI7O0FBRUFzQixJQUFBQSxDQUFDLENBQUNvRSxXQUFGLEdBQWlCcEUsQ0FBQyxDQUFDa0UsVUFBRixLQUFpQixDQUFsQixHQUNaNUcsQ0FBQyxDQUFDLDRCQUFELENBQUQsQ0FBZ0N5SyxRQUFoQyxDQUF5Qy9ILENBQUMsQ0FBQzBGLE9BQTNDLENBRFksR0FFWjFGLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTJHLE9BQVYsQ0FBa0IsNEJBQWxCLEVBQWdEbE0sTUFBaEQsRUFGSjtBQUlBa0IsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixHQUFVekUsQ0FBQyxDQUFDb0UsV0FBRixDQUFjdkYsSUFBZCxDQUNOLDhDQURNLEVBQzBDQyxNQUQxQyxFQUFWOztBQUVBa0IsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQixTQUFsQixFQUE2QixDQUE3Qjs7QUFFQSxRQUFJaUIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUF6QixJQUFpQ1osQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUYsWUFBVixLQUEyQixJQUFoRSxFQUFzRTtBQUNsRTFDLE1BQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQVYsR0FBMkIsQ0FBM0I7QUFDSDs7QUFFRGxGLElBQUFBLENBQUMsQ0FBQyxnQkFBRCxFQUFtQjBDLENBQUMsQ0FBQzBGLE9BQXJCLENBQUQsQ0FBK0JvRSxHQUEvQixDQUFtQyxPQUFuQyxFQUE0Q3BMLFFBQTVDLENBQXFELGVBQXJEOztBQUVBc0IsSUFBQUEsQ0FBQyxDQUFDaUwsYUFBRjs7QUFFQWpMLElBQUFBLENBQUMsQ0FBQ3NLLFdBQUY7O0FBRUF0SyxJQUFBQSxDQUFDLENBQUMySyxTQUFGOztBQUVBM0ssSUFBQUEsQ0FBQyxDQUFDa0wsVUFBRjs7QUFHQWxMLElBQUFBLENBQUMsQ0FBQ21MLGVBQUYsQ0FBa0IsT0FBT25MLENBQUMsQ0FBQzBELFlBQVQsS0FBMEIsUUFBMUIsR0FBcUMxRCxDQUFDLENBQUMwRCxZQUF2QyxHQUFzRCxDQUF4RTs7QUFFQSxRQUFJMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNEQsU0FBVixLQUF3QixJQUE1QixFQUFrQztBQUM5QnJCLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUS9GLFFBQVIsQ0FBaUIsV0FBakI7QUFDSDtBQUVKLEdBaEREOztBQWtEQWtCLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JnRSxTQUFoQixHQUE0QixZQUFXO0FBRW5DLFFBQUlwTCxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQWNxTCxDQUFkO0FBQUEsUUFBaUJDLENBQWpCO0FBQUEsUUFBb0JDLENBQXBCO0FBQUEsUUFBdUJDLFNBQXZCO0FBQUEsUUFBa0NDLFdBQWxDO0FBQUEsUUFBK0NDLGNBQS9DO0FBQUEsUUFBOERDLGdCQUE5RDs7QUFFQUgsSUFBQUEsU0FBUyxHQUFHckYsUUFBUSxDQUFDeUYsc0JBQVQsRUFBWjtBQUNBRixJQUFBQSxjQUFjLEdBQUcxTCxDQUFDLENBQUMwRixPQUFGLENBQVUwQyxRQUFWLEVBQWpCOztBQUVBLFFBQUdwSSxDQUFDLENBQUN2QyxPQUFGLENBQVUwRSxJQUFWLEdBQWlCLENBQXBCLEVBQXVCO0FBRW5Cd0osTUFBQUEsZ0JBQWdCLEdBQUczTCxDQUFDLENBQUN2QyxPQUFGLENBQVU2RSxZQUFWLEdBQXlCdEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEUsSUFBdEQ7QUFDQXNKLE1BQUFBLFdBQVcsR0FBR25DLElBQUksQ0FBQ0MsSUFBTCxDQUNWbUMsY0FBYyxDQUFDNUQsTUFBZixHQUF3QjZELGdCQURkLENBQWQ7O0FBSUEsV0FBSU4sQ0FBQyxHQUFHLENBQVIsRUFBV0EsQ0FBQyxHQUFHSSxXQUFmLEVBQTRCSixDQUFDLEVBQTdCLEVBQWdDO0FBQzVCLFlBQUloSixLQUFLLEdBQUc4RCxRQUFRLENBQUMwRixhQUFULENBQXVCLEtBQXZCLENBQVo7O0FBQ0EsYUFBSVAsQ0FBQyxHQUFHLENBQVIsRUFBV0EsQ0FBQyxHQUFHdEwsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEUsSUFBekIsRUFBK0JtSixDQUFDLEVBQWhDLEVBQW9DO0FBQ2hDLGNBQUlRLEdBQUcsR0FBRzNGLFFBQVEsQ0FBQzBGLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVjs7QUFDQSxlQUFJTixDQUFDLEdBQUcsQ0FBUixFQUFXQSxDQUFDLEdBQUd2TCxDQUFDLENBQUN2QyxPQUFGLENBQVU2RSxZQUF6QixFQUF1Q2lKLENBQUMsRUFBeEMsRUFBNEM7QUFDeEMsZ0JBQUl4QixNQUFNLEdBQUlzQixDQUFDLEdBQUdNLGdCQUFKLElBQXlCTCxDQUFDLEdBQUd0TCxDQUFDLENBQUN2QyxPQUFGLENBQVU2RSxZQUFmLEdBQStCaUosQ0FBdkQsQ0FBZDs7QUFDQSxnQkFBSUcsY0FBYyxDQUFDSyxHQUFmLENBQW1CaEMsTUFBbkIsQ0FBSixFQUFnQztBQUM1QitCLGNBQUFBLEdBQUcsQ0FBQ0UsV0FBSixDQUFnQk4sY0FBYyxDQUFDSyxHQUFmLENBQW1CaEMsTUFBbkIsQ0FBaEI7QUFDSDtBQUNKOztBQUNEMUgsVUFBQUEsS0FBSyxDQUFDMkosV0FBTixDQUFrQkYsR0FBbEI7QUFDSDs7QUFDRE4sUUFBQUEsU0FBUyxDQUFDUSxXQUFWLENBQXNCM0osS0FBdEI7QUFDSDs7QUFFRHJDLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXVHLEtBQVYsR0FBa0IzRCxNQUFsQixDQUF5QmtELFNBQXpCOztBQUNBeEwsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVMEMsUUFBVixHQUFxQkEsUUFBckIsR0FBZ0NBLFFBQWhDLEdBQ0tySixHQURMLENBQ1M7QUFDRCxpQkFBUyxNQUFNaUIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkUsWUFBakIsR0FBaUMsR0FEeEM7QUFFRCxtQkFBVztBQUZWLE9BRFQ7QUFNSDtBQUVKLEdBdENEOztBQXdDQTFDLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I4RSxlQUFoQixHQUFrQyxVQUFTQyxPQUFULEVBQWtCQyxXQUFsQixFQUErQjtBQUU3RCxRQUFJcE0sQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJcU0sVUFESjtBQUFBLFFBQ2dCQyxnQkFEaEI7QUFBQSxRQUNrQ0MsY0FEbEM7QUFBQSxRQUNrREMsaUJBQWlCLEdBQUcsS0FEdEU7O0FBRUEsUUFBSUMsV0FBVyxHQUFHek0sQ0FBQyxDQUFDMEYsT0FBRixDQUFVNUgsS0FBVixFQUFsQjs7QUFDQSxRQUFJaUksV0FBVyxHQUFHbEcsTUFBTSxDQUFDNk0sVUFBUCxJQUFxQnBQLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVL0IsS0FBVixFQUF2Qzs7QUFFQSxRQUFJa0MsQ0FBQyxDQUFDaUMsU0FBRixLQUFnQixRQUFwQixFQUE4QjtBQUMxQnNLLE1BQUFBLGNBQWMsR0FBR3hHLFdBQWpCO0FBQ0gsS0FGRCxNQUVPLElBQUkvRixDQUFDLENBQUNpQyxTQUFGLEtBQWdCLFFBQXBCLEVBQThCO0FBQ2pDc0ssTUFBQUEsY0FBYyxHQUFHRSxXQUFqQjtBQUNILEtBRk0sTUFFQSxJQUFJek0sQ0FBQyxDQUFDaUMsU0FBRixLQUFnQixLQUFwQixFQUEyQjtBQUM5QnNLLE1BQUFBLGNBQWMsR0FBR2pELElBQUksQ0FBQ3FELEdBQUwsQ0FBUzVHLFdBQVQsRUFBc0IwRyxXQUF0QixDQUFqQjtBQUNIOztBQUVELFFBQUt6TSxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLElBQ0RsQyxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLENBQXFCNEYsTUFEcEIsSUFFRDlILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsS0FBeUIsSUFGN0IsRUFFbUM7QUFFL0JvSyxNQUFBQSxnQkFBZ0IsR0FBRyxJQUFuQjs7QUFFQSxXQUFLRCxVQUFMLElBQW1Cck0sQ0FBQyxDQUFDZ0YsV0FBckIsRUFBa0M7QUFDOUIsWUFBSWhGLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBYzRILGNBQWQsQ0FBNkJQLFVBQTdCLENBQUosRUFBOEM7QUFDMUMsY0FBSXJNLENBQUMsQ0FBQ2tHLGdCQUFGLENBQW1CckUsV0FBbkIsS0FBbUMsS0FBdkMsRUFBOEM7QUFDMUMsZ0JBQUkwSyxjQUFjLEdBQUd2TSxDQUFDLENBQUNnRixXQUFGLENBQWNxSCxVQUFkLENBQXJCLEVBQWdEO0FBQzVDQyxjQUFBQSxnQkFBZ0IsR0FBR3RNLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBY3FILFVBQWQsQ0FBbkI7QUFDSDtBQUNKLFdBSkQsTUFJTztBQUNILGdCQUFJRSxjQUFjLEdBQUd2TSxDQUFDLENBQUNnRixXQUFGLENBQWNxSCxVQUFkLENBQXJCLEVBQWdEO0FBQzVDQyxjQUFBQSxnQkFBZ0IsR0FBR3RNLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBY3FILFVBQWQsQ0FBbkI7QUFDSDtBQUNKO0FBQ0o7QUFDSjs7QUFFRCxVQUFJQyxnQkFBZ0IsS0FBSyxJQUF6QixFQUErQjtBQUMzQixZQUFJdE0sQ0FBQyxDQUFDNkUsZ0JBQUYsS0FBdUIsSUFBM0IsRUFBaUM7QUFDN0IsY0FBSXlILGdCQUFnQixLQUFLdE0sQ0FBQyxDQUFDNkUsZ0JBQXZCLElBQTJDdUgsV0FBL0MsRUFBNEQ7QUFDeERwTSxZQUFBQSxDQUFDLENBQUM2RSxnQkFBRixHQUNJeUgsZ0JBREo7O0FBRUEsZ0JBQUl0TSxDQUFDLENBQUNpRixrQkFBRixDQUFxQnFILGdCQUFyQixNQUEyQyxTQUEvQyxFQUEwRDtBQUN0RHRNLGNBQUFBLENBQUMsQ0FBQzZNLE9BQUYsQ0FBVVAsZ0JBQVY7QUFDSCxhQUZELE1BRU87QUFDSHRNLGNBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsR0FBWUgsQ0FBQyxDQUFDa0IsTUFBRixDQUFTLEVBQVQsRUFBYXdCLENBQUMsQ0FBQ2tHLGdCQUFmLEVBQ1JsRyxDQUFDLENBQUNpRixrQkFBRixDQUNJcUgsZ0JBREosQ0FEUSxDQUFaOztBQUdBLGtCQUFJSCxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDbEJuTSxnQkFBQUEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWtFLFlBQTNCO0FBQ0g7O0FBQ0QzQixjQUFBQSxDQUFDLENBQUM4TSxPQUFGLENBQVVYLE9BQVY7QUFDSDs7QUFDREssWUFBQUEsaUJBQWlCLEdBQUdGLGdCQUFwQjtBQUNIO0FBQ0osU0FqQkQsTUFpQk87QUFDSHRNLFVBQUFBLENBQUMsQ0FBQzZFLGdCQUFGLEdBQXFCeUgsZ0JBQXJCOztBQUNBLGNBQUl0TSxDQUFDLENBQUNpRixrQkFBRixDQUFxQnFILGdCQUFyQixNQUEyQyxTQUEvQyxFQUEwRDtBQUN0RHRNLFlBQUFBLENBQUMsQ0FBQzZNLE9BQUYsQ0FBVVAsZ0JBQVY7QUFDSCxXQUZELE1BRU87QUFDSHRNLFlBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsR0FBWUgsQ0FBQyxDQUFDa0IsTUFBRixDQUFTLEVBQVQsRUFBYXdCLENBQUMsQ0FBQ2tHLGdCQUFmLEVBQ1JsRyxDQUFDLENBQUNpRixrQkFBRixDQUNJcUgsZ0JBREosQ0FEUSxDQUFaOztBQUdBLGdCQUFJSCxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDbEJuTSxjQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVa0UsWUFBM0I7QUFDSDs7QUFDRDNCLFlBQUFBLENBQUMsQ0FBQzhNLE9BQUYsQ0FBVVgsT0FBVjtBQUNIOztBQUNESyxVQUFBQSxpQkFBaUIsR0FBR0YsZ0JBQXBCO0FBQ0g7QUFDSixPQWpDRCxNQWlDTztBQUNILFlBQUl0TSxDQUFDLENBQUM2RSxnQkFBRixLQUF1QixJQUEzQixFQUFpQztBQUM3QjdFLFVBQUFBLENBQUMsQ0FBQzZFLGdCQUFGLEdBQXFCLElBQXJCO0FBQ0E3RSxVQUFBQSxDQUFDLENBQUN2QyxPQUFGLEdBQVl1QyxDQUFDLENBQUNrRyxnQkFBZDs7QUFDQSxjQUFJaUcsT0FBTyxLQUFLLElBQWhCLEVBQXNCO0FBQ2xCbk0sWUFBQUEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWtFLFlBQTNCO0FBQ0g7O0FBQ0QzQixVQUFBQSxDQUFDLENBQUM4TSxPQUFGLENBQVVYLE9BQVY7O0FBQ0FLLFVBQUFBLGlCQUFpQixHQUFHRixnQkFBcEI7QUFDSDtBQUNKLE9BN0Q4QixDQStEL0I7OztBQUNBLFVBQUksQ0FBQ0gsT0FBRCxJQUFZSyxpQkFBaUIsS0FBSyxLQUF0QyxFQUE4QztBQUMxQ3hNLFFBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsWUFBbEIsRUFBZ0MsQ0FBQy9NLENBQUQsRUFBSXdNLGlCQUFKLENBQWhDO0FBQ0g7QUFDSjtBQUVKLEdBdEZEOztBQXdGQTVNLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JWLFdBQWhCLEdBQThCLFVBQVNzRyxLQUFULEVBQWdCQyxXQUFoQixFQUE2QjtBQUV2RCxRQUFJak4sQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJa04sT0FBTyxHQUFHNVAsQ0FBQyxDQUFDMFAsS0FBSyxDQUFDRyxhQUFQLENBRGY7QUFBQSxRQUVJQyxXQUZKO0FBQUEsUUFFaUI3SSxXQUZqQjtBQUFBLFFBRThCOEksWUFGOUIsQ0FGdUQsQ0FNdkQ7OztBQUNBLFFBQUdILE9BQU8sQ0FBQ0ksRUFBUixDQUFXLEdBQVgsQ0FBSCxFQUFvQjtBQUNoQk4sTUFBQUEsS0FBSyxDQUFDTyxjQUFOO0FBQ0gsS0FUc0QsQ0FXdkQ7OztBQUNBLFFBQUcsQ0FBQ0wsT0FBTyxDQUFDSSxFQUFSLENBQVcsSUFBWCxDQUFKLEVBQXNCO0FBQ2xCSixNQUFBQSxPQUFPLEdBQUdBLE9BQU8sQ0FBQzlOLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBVjtBQUNIOztBQUVEaU8sSUFBQUEsWUFBWSxHQUFJck4sQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBekIsS0FBNEMsQ0FBNUQ7QUFDQTRLLElBQUFBLFdBQVcsR0FBR0MsWUFBWSxHQUFHLENBQUgsR0FBTyxDQUFDck4sQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDMEQsWUFBbEIsSUFBa0MxRCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUE3RTs7QUFFQSxZQUFRd0ssS0FBSyxDQUFDL0csSUFBTixDQUFXdUgsT0FBbkI7QUFFSSxXQUFLLFVBQUw7QUFDSWpKLFFBQUFBLFdBQVcsR0FBRzZJLFdBQVcsS0FBSyxDQUFoQixHQUFvQnBOLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQTlCLEdBQStDeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QjZLLFdBQXRGOztBQUNBLFlBQUlwTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE3QixFQUEyQztBQUN2Q3ZDLFVBQUFBLENBQUMsQ0FBQ2lLLFlBQUYsQ0FBZWpLLENBQUMsQ0FBQzBELFlBQUYsR0FBaUJhLFdBQWhDLEVBQTZDLEtBQTdDLEVBQW9EMEksV0FBcEQ7QUFDSDs7QUFDRDs7QUFFSixXQUFLLE1BQUw7QUFDSTFJLFFBQUFBLFdBQVcsR0FBRzZJLFdBQVcsS0FBSyxDQUFoQixHQUFvQnBOLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQTlCLEdBQStDNEssV0FBN0Q7O0FBQ0EsWUFBSXBOLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTdCLEVBQTJDO0FBQ3ZDdkMsVUFBQUEsQ0FBQyxDQUFDaUssWUFBRixDQUFlakssQ0FBQyxDQUFDMEQsWUFBRixHQUFpQmEsV0FBaEMsRUFBNkMsS0FBN0MsRUFBb0QwSSxXQUFwRDtBQUNIOztBQUNEOztBQUVKLFdBQUssT0FBTDtBQUNJLFlBQUl0RixLQUFLLEdBQUdxRixLQUFLLENBQUMvRyxJQUFOLENBQVcwQixLQUFYLEtBQXFCLENBQXJCLEdBQXlCLENBQXpCLEdBQ1JxRixLQUFLLENBQUMvRyxJQUFOLENBQVcwQixLQUFYLElBQW9CdUYsT0FBTyxDQUFDdkYsS0FBUixLQUFrQjNILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBRHBEOztBQUdBeEMsUUFBQUEsQ0FBQyxDQUFDaUssWUFBRixDQUFlakssQ0FBQyxDQUFDeU4sY0FBRixDQUFpQjlGLEtBQWpCLENBQWYsRUFBd0MsS0FBeEMsRUFBK0NzRixXQUEvQzs7QUFDQUMsUUFBQUEsT0FBTyxDQUFDOUUsUUFBUixHQUFtQjJFLE9BQW5CLENBQTJCLE9BQTNCO0FBQ0E7O0FBRUo7QUFDSTtBQXpCUjtBQTRCSCxHQS9DRDs7QUFpREFuTixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCcUcsY0FBaEIsR0FBaUMsVUFBUzlGLEtBQVQsRUFBZ0I7QUFFN0MsUUFBSTNILENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSTBOLFVBREo7QUFBQSxRQUNnQkMsYUFEaEI7O0FBR0FELElBQUFBLFVBQVUsR0FBRzFOLENBQUMsQ0FBQzROLG1CQUFGLEVBQWI7QUFDQUQsSUFBQUEsYUFBYSxHQUFHLENBQWhCOztBQUNBLFFBQUloRyxLQUFLLEdBQUcrRixVQUFVLENBQUNBLFVBQVUsQ0FBQzVGLE1BQVgsR0FBb0IsQ0FBckIsQ0FBdEIsRUFBK0M7QUFDM0NILE1BQUFBLEtBQUssR0FBRytGLFVBQVUsQ0FBQ0EsVUFBVSxDQUFDNUYsTUFBWCxHQUFvQixDQUFyQixDQUFsQjtBQUNILEtBRkQsTUFFTztBQUNILFdBQUssSUFBSStGLENBQVQsSUFBY0gsVUFBZCxFQUEwQjtBQUN0QixZQUFJL0YsS0FBSyxHQUFHK0YsVUFBVSxDQUFDRyxDQUFELENBQXRCLEVBQTJCO0FBQ3ZCbEcsVUFBQUEsS0FBSyxHQUFHZ0csYUFBUjtBQUNBO0FBQ0g7O0FBQ0RBLFFBQUFBLGFBQWEsR0FBR0QsVUFBVSxDQUFDRyxDQUFELENBQTFCO0FBQ0g7QUFDSjs7QUFFRCxXQUFPbEcsS0FBUDtBQUNILEdBcEJEOztBQXNCQS9ILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IwRyxhQUFoQixHQUFnQyxZQUFXO0FBRXZDLFFBQUk5TixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVUwRCxJQUFWLElBQWtCbkIsQ0FBQyxDQUFDNEQsS0FBRixLQUFZLElBQWxDLEVBQXdDO0FBRXBDdEcsTUFBQUEsQ0FBQyxDQUFDLElBQUQsRUFBTzBDLENBQUMsQ0FBQzRELEtBQVQsQ0FBRCxDQUNLbUssR0FETCxDQUNTLGFBRFQsRUFDd0IvTixDQUFDLENBQUMwRyxXQUQxQixFQUVLcUgsR0FGTCxDQUVTLGtCQUZULEVBRTZCelEsQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDZ08sU0FBVixFQUFxQmhPLENBQXJCLEVBQXdCLElBQXhCLENBRjdCLEVBR0srTixHQUhMLENBR1Msa0JBSFQsRUFHNkJ6USxDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnTyxTQUFWLEVBQXFCaE8sQ0FBckIsRUFBd0IsS0FBeEIsQ0FIN0I7QUFLSDs7QUFFREEsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUksR0FBVixDQUFjLHdCQUFkOztBQUVBLFFBQUkvTixDQUFDLENBQUN2QyxPQUFGLENBQVU2QyxNQUFWLEtBQXFCLElBQXJCLElBQTZCTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExRCxFQUF3RTtBQUNwRXZDLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUYsSUFBZ0JqRSxDQUFDLENBQUNpRSxVQUFGLENBQWE4SixHQUFiLENBQWlCLGFBQWpCLEVBQWdDL04sQ0FBQyxDQUFDMEcsV0FBbEMsQ0FBaEI7QUFDQTFHLE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUYsSUFBZ0JoRSxDQUFDLENBQUNnRSxVQUFGLENBQWErSixHQUFiLENBQWlCLGFBQWpCLEVBQWdDL04sQ0FBQyxDQUFDMEcsV0FBbEMsQ0FBaEI7QUFDSDs7QUFFRDFHLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXNKLEdBQVIsQ0FBWSxrQ0FBWixFQUFnRC9OLENBQUMsQ0FBQzhHLFlBQWxEOztBQUNBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRc0osR0FBUixDQUFZLGlDQUFaLEVBQStDL04sQ0FBQyxDQUFDOEcsWUFBakQ7O0FBQ0E5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVFzSixHQUFSLENBQVksOEJBQVosRUFBNEMvTixDQUFDLENBQUM4RyxZQUE5Qzs7QUFDQTlHLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXNKLEdBQVIsQ0FBWSxvQ0FBWixFQUFrRC9OLENBQUMsQ0FBQzhHLFlBQXBEOztBQUVBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRc0osR0FBUixDQUFZLGFBQVosRUFBMkIvTixDQUFDLENBQUMyRyxZQUE3Qjs7QUFFQXJKLElBQUFBLENBQUMsQ0FBQzZJLFFBQUQsQ0FBRCxDQUFZNEgsR0FBWixDQUFnQi9OLENBQUMsQ0FBQzhGLGdCQUFsQixFQUFvQzlGLENBQUMsQ0FBQ2lPLFVBQXRDOztBQUVBak8sSUFBQUEsQ0FBQyxDQUFDa08sa0JBQUY7O0FBRUEsUUFBSWxPLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlDLGFBQVYsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbENGLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXNKLEdBQVIsQ0FBWSxlQUFaLEVBQTZCL04sQ0FBQyxDQUFDZ0gsVUFBL0I7QUFDSDs7QUFFRCxRQUFJaEgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0UsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ25FLE1BQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ29FLFdBQUgsQ0FBRCxDQUFpQmdFLFFBQWpCLEdBQTRCMkYsR0FBNUIsQ0FBZ0MsYUFBaEMsRUFBK0MvTixDQUFDLENBQUM0RyxhQUFqRDtBQUNIOztBQUVEdEosSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFELENBQVVrTyxHQUFWLENBQWMsbUNBQW1DL04sQ0FBQyxDQUFDRixXQUFuRCxFQUFnRUUsQ0FBQyxDQUFDbU8saUJBQWxFO0FBRUE3USxJQUFBQSxDQUFDLENBQUN1QyxNQUFELENBQUQsQ0FBVWtPLEdBQVYsQ0FBYyx3QkFBd0IvTixDQUFDLENBQUNGLFdBQXhDLEVBQXFERSxDQUFDLENBQUNvTyxNQUF2RDtBQUVBOVEsSUFBQUEsQ0FBQyxDQUFDLG1CQUFELEVBQXNCMEMsQ0FBQyxDQUFDb0UsV0FBeEIsQ0FBRCxDQUFzQzJKLEdBQXRDLENBQTBDLFdBQTFDLEVBQXVEL04sQ0FBQyxDQUFDdU4sY0FBekQ7QUFFQWpRLElBQUFBLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVa08sR0FBVixDQUFjLHNCQUFzQi9OLENBQUMsQ0FBQ0YsV0FBdEMsRUFBbURFLENBQUMsQ0FBQzZHLFdBQXJEO0FBQ0F2SixJQUFBQSxDQUFDLENBQUM2SSxRQUFELENBQUQsQ0FBWTRILEdBQVosQ0FBZ0IsdUJBQXVCL04sQ0FBQyxDQUFDRixXQUF6QyxFQUFzREUsQ0FBQyxDQUFDNkcsV0FBeEQ7QUFFSCxHQWhERDs7QUFrREFqSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCOEcsa0JBQWhCLEdBQXFDLFlBQVc7QUFFNUMsUUFBSWxPLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVFzSixHQUFSLENBQVksa0JBQVosRUFBZ0N6USxDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnTyxTQUFWLEVBQXFCaE8sQ0FBckIsRUFBd0IsSUFBeEIsQ0FBaEM7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXNKLEdBQVIsQ0FBWSxrQkFBWixFQUFnQ3pRLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixLQUF4QixDQUFoQztBQUVILEdBUEQ7O0FBU0FKLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JpSCxXQUFoQixHQUE4QixZQUFXO0FBRXJDLFFBQUlyTyxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQWMwTCxjQUFkOztBQUVBLFFBQUcxTCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRSxJQUFWLEdBQWlCLENBQXBCLEVBQXVCO0FBQ25CdUosTUFBQUEsY0FBYyxHQUFHMUwsQ0FBQyxDQUFDcUUsT0FBRixDQUFVK0QsUUFBVixHQUFxQkEsUUFBckIsRUFBakI7QUFDQXNELE1BQUFBLGNBQWMsQ0FBQ2xCLFVBQWYsQ0FBMEIsT0FBMUI7O0FBQ0F4SyxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVV1RyxLQUFWLEdBQWtCM0QsTUFBbEIsQ0FBeUJvRCxjQUF6QjtBQUNIO0FBRUosR0FWRDs7QUFZQTlMLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JULFlBQWhCLEdBQStCLFVBQVNxRyxLQUFULEVBQWdCO0FBRTNDLFFBQUloTixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN5RixXQUFGLEtBQWtCLEtBQXRCLEVBQTZCO0FBQ3pCdUgsTUFBQUEsS0FBSyxDQUFDc0Isd0JBQU47QUFDQXRCLE1BQUFBLEtBQUssQ0FBQ3VCLGVBQU47QUFDQXZCLE1BQUFBLEtBQUssQ0FBQ08sY0FBTjtBQUNIO0FBRUosR0FWRDs7QUFZQTNOLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JvSCxPQUFoQixHQUEwQixVQUFTMUIsT0FBVCxFQUFrQjtBQUV4QyxRQUFJOU0sQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3dHLGFBQUY7O0FBRUF4RyxJQUFBQSxDQUFDLENBQUMwRSxXQUFGLEdBQWdCLEVBQWhCOztBQUVBMUUsSUFBQUEsQ0FBQyxDQUFDOE4sYUFBRjs7QUFFQXhRLElBQUFBLENBQUMsQ0FBQyxlQUFELEVBQWtCMEMsQ0FBQyxDQUFDMEYsT0FBcEIsQ0FBRCxDQUE4QjJDLE1BQTlCOztBQUVBLFFBQUlySSxDQUFDLENBQUM0RCxLQUFOLEVBQWE7QUFDVDVELE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsQ0FBUTZLLE1BQVI7QUFDSDs7QUFHRCxRQUFLek8sQ0FBQyxDQUFDaUUsVUFBRixJQUFnQmpFLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYTZELE1BQWxDLEVBQTJDO0FBRXZDOUgsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUNLc0csV0FETCxDQUNpQix5Q0FEakIsRUFFS0MsVUFGTCxDQUVnQixvQ0FGaEIsRUFHS3pMLEdBSEwsQ0FHUyxTQUhULEVBR21CLEVBSG5COztBQUtBLFVBQUtpQixDQUFDLENBQUNpSCxRQUFGLENBQVd3RCxJQUFYLENBQWlCekssQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0MsU0FBM0IsQ0FBTCxFQUE2QztBQUN6Q1IsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFhd0ssTUFBYjtBQUNIO0FBQ0o7O0FBRUQsUUFBS3pPLENBQUMsQ0FBQ2dFLFVBQUYsSUFBZ0JoRSxDQUFDLENBQUNnRSxVQUFGLENBQWE4RCxNQUFsQyxFQUEyQztBQUV2QzlILE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUYsQ0FDS3VHLFdBREwsQ0FDaUIseUNBRGpCLEVBRUtDLFVBRkwsQ0FFZ0Isb0NBRmhCLEVBR0t6TCxHQUhMLENBR1MsU0FIVCxFQUdtQixFQUhuQjs7QUFLQSxVQUFLaUIsQ0FBQyxDQUFDaUgsUUFBRixDQUFXd0QsSUFBWCxDQUFpQnpLLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWdELFNBQTNCLENBQUwsRUFBNkM7QUFDekNULFFBQUFBLENBQUMsQ0FBQ2dFLFVBQUYsQ0FBYXlLLE1BQWI7QUFDSDtBQUVKOztBQUdELFFBQUl6TyxDQUFDLENBQUNxRSxPQUFOLEVBQWU7QUFFWHJFLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FDS2tHLFdBREwsQ0FDaUIsbUVBRGpCLEVBRUtDLFVBRkwsQ0FFZ0IsYUFGaEIsRUFHS0EsVUFITCxDQUdnQixrQkFIaEIsRUFJSzVMLElBSkwsQ0FJVSxZQUFVO0FBQ1p0QixRQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpSyxJQUFSLENBQWEsT0FBYixFQUFzQmpLLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTJJLElBQVIsQ0FBYSxpQkFBYixDQUF0QjtBQUNILE9BTkw7O0FBUUFqRyxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLEtBQUszSyxPQUFMLENBQWE0RSxLQUFwQyxFQUEyQ2dHLE1BQTNDOztBQUVBckksTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjaUUsTUFBZDs7QUFFQXJJLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTRELE1BQVI7O0FBRUFySSxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVU0QyxNQUFWLENBQWlCdEksQ0FBQyxDQUFDcUUsT0FBbkI7QUFDSDs7QUFFRHJFLElBQUFBLENBQUMsQ0FBQ3FPLFdBQUY7O0FBRUFyTyxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVU2RSxXQUFWLENBQXNCLGNBQXRCOztBQUNBdkssSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVNkUsV0FBVixDQUFzQixtQkFBdEI7O0FBQ0F2SyxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVU2RSxXQUFWLENBQXNCLGNBQXRCOztBQUVBdkssSUFBQUEsQ0FBQyxDQUFDNEUsU0FBRixHQUFjLElBQWQ7O0FBRUEsUUFBRyxDQUFDa0ksT0FBSixFQUFhO0FBQ1Q5TSxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLFNBQWxCLEVBQTZCLENBQUMvTSxDQUFELENBQTdCO0FBQ0g7QUFFSixHQTFFRDs7QUE0RUFKLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J3QyxpQkFBaEIsR0FBb0MsVUFBU3ZILEtBQVQsRUFBZ0I7QUFFaEQsUUFBSXJDLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWtLLFVBQVUsR0FBRyxFQURqQjs7QUFHQUEsSUFBQUEsVUFBVSxDQUFDbEssQ0FBQyxDQUFDNkYsY0FBSCxDQUFWLEdBQStCLEVBQS9COztBQUVBLFFBQUk3RixDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCeEIsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQm1MLFVBQWxCO0FBQ0gsS0FGRCxNQUVPO0FBQ0hsSyxNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWE1RixLQUFiLEVBQW9CdEQsR0FBcEIsQ0FBd0JtTCxVQUF4QjtBQUNIO0FBRUosR0FiRDs7QUFlQXRLLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JzSCxTQUFoQixHQUE0QixVQUFTQyxVQUFULEVBQXFCN0YsUUFBckIsRUFBK0I7QUFFdkQsUUFBSTlJLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ2tGLGNBQUYsS0FBcUIsS0FBekIsRUFBZ0M7QUFFNUJsRixNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWEwRyxVQUFiLEVBQXlCNVAsR0FBekIsQ0FBNkI7QUFDekJvRSxRQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRjtBQURPLE9BQTdCOztBQUlBbkQsTUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUFVNEQsRUFBVixDQUFhMEcsVUFBYixFQUF5QmhHLE9BQXpCLENBQWlDO0FBQzdCaUcsUUFBQUEsT0FBTyxFQUFFO0FBRG9CLE9BQWpDLEVBRUc1TyxDQUFDLENBQUN2QyxPQUFGLENBQVVHLEtBRmIsRUFFb0JvQyxDQUFDLENBQUN2QyxPQUFGLENBQVU2RCxNQUY5QixFQUVzQ3dILFFBRnRDO0FBSUgsS0FWRCxNQVVPO0FBRUg5SSxNQUFBQSxDQUFDLENBQUMwSixlQUFGLENBQWtCaUYsVUFBbEI7O0FBRUEzTyxNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWEwRyxVQUFiLEVBQXlCNVAsR0FBekIsQ0FBNkI7QUFDekI2UCxRQUFBQSxPQUFPLEVBQUUsQ0FEZ0I7QUFFekJ6TCxRQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRjtBQUZPLE9BQTdCOztBQUtBLFVBQUkyRixRQUFKLEVBQWM7QUFDVmEsUUFBQUEsVUFBVSxDQUFDLFlBQVc7QUFFbEIzSixVQUFBQSxDQUFDLENBQUM0SixpQkFBRixDQUFvQitFLFVBQXBCOztBQUVBN0YsVUFBQUEsUUFBUSxDQUFDVyxJQUFUO0FBQ0gsU0FMUyxFQUtQekosQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQUxILENBQVY7QUFNSDtBQUVKO0FBRUosR0FsQ0Q7O0FBb0NBZ0MsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnlILFlBQWhCLEdBQStCLFVBQVNGLFVBQVQsRUFBcUI7QUFFaEQsUUFBSTNPLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ2tGLGNBQUYsS0FBcUIsS0FBekIsRUFBZ0M7QUFFNUJsRixNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWEwRyxVQUFiLEVBQXlCaEcsT0FBekIsQ0FBaUM7QUFDN0JpRyxRQUFBQSxPQUFPLEVBQUUsQ0FEb0I7QUFFN0J6TCxRQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRixNQUFWLEdBQW1CO0FBRkUsT0FBakMsRUFHR25ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FIYixFQUdvQm9DLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZELE1BSDlCO0FBS0gsS0FQRCxNQU9PO0FBRUh0QixNQUFBQSxDQUFDLENBQUMwSixlQUFGLENBQWtCaUYsVUFBbEI7O0FBRUEzTyxNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWEwRyxVQUFiLEVBQXlCNVAsR0FBekIsQ0FBNkI7QUFDekI2UCxRQUFBQSxPQUFPLEVBQUUsQ0FEZ0I7QUFFekJ6TCxRQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRixNQUFWLEdBQW1CO0FBRkYsT0FBN0I7QUFLSDtBQUVKLEdBdEJEOztBQXdCQXZELEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IwSCxZQUFoQixHQUErQmxQLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IySCxXQUFoQixHQUE4QixVQUFTQyxNQUFULEVBQWlCO0FBRTFFLFFBQUloUCxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJZ1AsTUFBTSxLQUFLLElBQWYsRUFBcUI7QUFFakJoUCxNQUFBQSxDQUFDLENBQUMyRixZQUFGLEdBQWlCM0YsQ0FBQyxDQUFDcUUsT0FBbkI7O0FBRUFyRSxNQUFBQSxDQUFDLENBQUM2SCxNQUFGOztBQUVBN0gsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixLQUFLM0ssT0FBTCxDQUFhNEUsS0FBcEMsRUFBMkNnRyxNQUEzQzs7QUFFQXJJLE1BQUFBLENBQUMsQ0FBQzJGLFlBQUYsQ0FBZXFKLE1BQWYsQ0FBc0JBLE1BQXRCLEVBQThCakgsUUFBOUIsQ0FBdUMvSCxDQUFDLENBQUNvRSxXQUF6Qzs7QUFFQXBFLE1BQUFBLENBQUMsQ0FBQ3VJLE1BQUY7QUFFSDtBQUVKLEdBbEJEOztBQW9CQTNJLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I2SCxZQUFoQixHQUErQixZQUFXO0FBRXRDLFFBQUlqUCxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUNLcUksR0FETCxDQUNTLHdCQURULEVBRUs3TyxFQUZMLENBRVEsd0JBRlIsRUFHUSxxQkFIUixFQUcrQixVQUFTOE4sS0FBVCxFQUFnQjtBQUUzQ0EsTUFBQUEsS0FBSyxDQUFDc0Isd0JBQU47QUFDQSxVQUFJWSxHQUFHLEdBQUc1UixDQUFDLENBQUMsSUFBRCxDQUFYO0FBRUFxTSxNQUFBQSxVQUFVLENBQUMsWUFBVztBQUVsQixZQUFJM0osQ0FBQyxDQUFDdkMsT0FBRixDQUFVc0UsWUFBZCxFQUE2QjtBQUN6Qi9CLFVBQUFBLENBQUMsQ0FBQ21GLFFBQUYsR0FBYStKLEdBQUcsQ0FBQzVCLEVBQUosQ0FBTyxRQUFQLENBQWI7O0FBQ0F0TixVQUFBQSxDQUFDLENBQUNzRyxRQUFGO0FBQ0g7QUFFSixPQVBTLEVBT1AsQ0FQTyxDQUFWO0FBU0gsS0FqQkQ7QUFrQkgsR0F0QkQ7O0FBd0JBMUcsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQitILFVBQWhCLEdBQTZCdlAsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmdJLGlCQUFoQixHQUFvQyxZQUFXO0FBRXhFLFFBQUlwUCxDQUFDLEdBQUcsSUFBUjs7QUFDQSxXQUFPQSxDQUFDLENBQUMwRCxZQUFUO0FBRUgsR0FMRDs7QUFPQTlELEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J5RCxXQUFoQixHQUE4QixZQUFXO0FBRXJDLFFBQUk3SyxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJcVAsVUFBVSxHQUFHLENBQWpCO0FBQ0EsUUFBSUMsT0FBTyxHQUFHLENBQWQ7QUFDQSxRQUFJQyxRQUFRLEdBQUcsQ0FBZjs7QUFFQSxRQUFJdlAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUEzQixFQUFpQztBQUM3QixhQUFPMk4sVUFBVSxHQUFHclAsQ0FBQyxDQUFDa0UsVUFBdEIsRUFBa0M7QUFDOUIsVUFBRXFMLFFBQUY7QUFDQUYsUUFBQUEsVUFBVSxHQUFHQyxPQUFPLEdBQUd0UCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFqQztBQUNBOE0sUUFBQUEsT0FBTyxJQUFJdFAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBVixJQUE0QnhDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXRDLEdBQXFEdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBL0QsR0FBZ0Z4QyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFyRztBQUNIO0FBQ0osS0FORCxNQU1PLElBQUl2QyxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQTdCLEVBQW1DO0FBQ3RDMk8sTUFBQUEsUUFBUSxHQUFHdlAsQ0FBQyxDQUFDa0UsVUFBYjtBQUNILEtBRk0sTUFFQSxJQUFHLENBQUNsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4QyxRQUFkLEVBQXdCO0FBQzNCZ1AsTUFBQUEsUUFBUSxHQUFHLElBQUlqRyxJQUFJLENBQUNDLElBQUwsQ0FBVSxDQUFDdkosQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBMUIsSUFBMEN2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUE5RCxDQUFmO0FBQ0gsS0FGTSxNQUVEO0FBQ0YsYUFBTzZNLFVBQVUsR0FBR3JQLENBQUMsQ0FBQ2tFLFVBQXRCLEVBQWtDO0FBQzlCLFVBQUVxTCxRQUFGO0FBQ0FGLFFBQUFBLFVBQVUsR0FBR0MsT0FBTyxHQUFHdFAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBakM7QUFDQThNLFFBQUFBLE9BQU8sSUFBSXRQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQVYsSUFBNEJ4QyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF0QyxHQUFxRHZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQS9ELEdBQWdGeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBckc7QUFDSDtBQUNKOztBQUVELFdBQU9nTixRQUFRLEdBQUcsQ0FBbEI7QUFFSCxHQTVCRDs7QUE4QkEzUCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCb0ksT0FBaEIsR0FBMEIsVUFBU2IsVUFBVCxFQUFxQjtBQUUzQyxRQUFJM08sQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJNkksVUFESjtBQUFBLFFBRUk0RyxjQUZKO0FBQUEsUUFHSUMsY0FBYyxHQUFHLENBSHJCO0FBQUEsUUFJSUMsV0FKSjs7QUFNQTNQLElBQUFBLENBQUMsQ0FBQ3VFLFdBQUYsR0FBZ0IsQ0FBaEI7QUFDQWtMLElBQUFBLGNBQWMsR0FBR3pQLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlHLEtBQVYsR0FBa0JwQyxXQUFsQixDQUE4QixJQUE5QixDQUFqQjs7QUFFQSxRQUFJMUksQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUEzQixFQUFpQztBQUM3QixVQUFJMUIsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBN0IsRUFBMkM7QUFDdkN2QyxRQUFBQSxDQUFDLENBQUN1RSxXQUFGLEdBQWlCdkUsQ0FBQyxDQUFDbUUsVUFBRixHQUFlbkUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBMUIsR0FBMEMsQ0FBQyxDQUEzRDtBQUNBbU4sUUFBQUEsY0FBYyxHQUFJRCxjQUFjLEdBQUd6UCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE1QixHQUE0QyxDQUFDLENBQTlEO0FBQ0g7O0FBQ0QsVUFBSXZDLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXpCLEtBQTRDLENBQWhELEVBQW1EO0FBQy9DLFlBQUltTSxVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUF2QixHQUF3Q3hDLENBQUMsQ0FBQ2tFLFVBQTFDLElBQXdEbEUsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBckYsRUFBbUc7QUFDL0YsY0FBSW9NLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBQW5CLEVBQStCO0FBQzNCbEUsWUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixHQUFpQixDQUFDdkUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixJQUEwQm9NLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBQXpDLENBQUQsSUFBeURsRSxDQUFDLENBQUNtRSxVQUE1RCxHQUEwRSxDQUFDLENBQTNGO0FBQ0F1TCxZQUFBQSxjQUFjLEdBQUksQ0FBQzFQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsSUFBMEJvTSxVQUFVLEdBQUczTyxDQUFDLENBQUNrRSxVQUF6QyxDQUFELElBQXlEdUwsY0FBMUQsR0FBNEUsQ0FBQyxDQUE5RjtBQUNILFdBSEQsTUFHTztBQUNIelAsWUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixHQUFrQnZFLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQTFCLEdBQTRDeEMsQ0FBQyxDQUFDbUUsVUFBL0MsR0FBNkQsQ0FBQyxDQUE5RTtBQUNBdUwsWUFBQUEsY0FBYyxHQUFLMVAsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBMUIsR0FBNENpTixjQUE3QyxHQUErRCxDQUFDLENBQWpGO0FBQ0g7QUFDSjtBQUNKO0FBQ0osS0FoQkQsTUFnQk87QUFDSCxVQUFJZCxVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF2QixHQUFzQ3ZDLENBQUMsQ0FBQ2tFLFVBQTVDLEVBQXdEO0FBQ3BEbEUsUUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixHQUFnQixDQUFFb0ssVUFBVSxHQUFHM08sQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBeEIsR0FBd0N2QyxDQUFDLENBQUNrRSxVQUEzQyxJQUF5RGxFLENBQUMsQ0FBQ21FLFVBQTNFO0FBQ0F1TCxRQUFBQSxjQUFjLEdBQUcsQ0FBRWYsVUFBVSxHQUFHM08sQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBeEIsR0FBd0N2QyxDQUFDLENBQUNrRSxVQUEzQyxJQUF5RHVMLGNBQTFFO0FBQ0g7QUFDSjs7QUFFRCxRQUFJelAsQ0FBQyxDQUFDa0UsVUFBRixJQUFnQmxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTlCLEVBQTRDO0FBQ3hDdkMsTUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixHQUFnQixDQUFoQjtBQUNBbUwsTUFBQUEsY0FBYyxHQUFHLENBQWpCO0FBQ0g7O0FBRUQsUUFBSTFQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBekIsSUFBaUNaLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsSUFBNUQsRUFBa0U7QUFDOUQxQixNQUFBQSxDQUFDLENBQUN1RSxXQUFGLElBQWlCdkUsQ0FBQyxDQUFDbUUsVUFBRixHQUFlbUYsSUFBSSxDQUFDc0csS0FBTCxDQUFXNVAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUFwQyxDQUFmLEdBQXdEdkMsQ0FBQyxDQUFDbUUsVUFBM0U7QUFDSCxLQUZELE1BRU8sSUFBSW5FLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBN0IsRUFBbUM7QUFDdENaLE1BQUFBLENBQUMsQ0FBQ3VFLFdBQUYsR0FBZ0IsQ0FBaEI7QUFDQXZFLE1BQUFBLENBQUMsQ0FBQ3VFLFdBQUYsSUFBaUJ2RSxDQUFDLENBQUNtRSxVQUFGLEdBQWVtRixJQUFJLENBQUNzRyxLQUFMLENBQVc1UCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCLENBQXBDLENBQWhDO0FBQ0g7O0FBRUQsUUFBSXZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUI2RixNQUFBQSxVQUFVLEdBQUs4RixVQUFVLEdBQUczTyxDQUFDLENBQUNtRSxVQUFoQixHQUE4QixDQUFDLENBQWhDLEdBQXFDbkUsQ0FBQyxDQUFDdUUsV0FBcEQ7QUFDSCxLQUZELE1BRU87QUFDSHNFLE1BQUFBLFVBQVUsR0FBSzhGLFVBQVUsR0FBR2MsY0FBZCxHQUFnQyxDQUFDLENBQWxDLEdBQXVDQyxjQUFwRDtBQUNIOztBQUVELFFBQUkxUCxDQUFDLENBQUN2QyxPQUFGLENBQVVzRixhQUFWLEtBQTRCLElBQWhDLEVBQXNDO0FBRWxDLFVBQUkvQyxDQUFDLENBQUNrRSxVQUFGLElBQWdCbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBMUIsSUFBMEN2QyxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLEtBQXJFLEVBQTRFO0FBQ3hFaU8sUUFBQUEsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixjQUF2QixFQUF1Q0gsRUFBdkMsQ0FBMEMwRyxVQUExQyxDQUFkO0FBQ0gsT0FGRCxNQUVPO0FBQ0hnQixRQUFBQSxXQUFXLEdBQUczUCxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLGNBQXZCLEVBQXVDSCxFQUF2QyxDQUEwQzBHLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQWpFLENBQWQ7QUFDSDs7QUFFRCxVQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkUsR0FBVixLQUFrQixJQUF0QixFQUE0QjtBQUN4QixZQUFJdU4sV0FBVyxDQUFDLENBQUQsQ0FBZixFQUFvQjtBQUNoQjlHLFVBQUFBLFVBQVUsR0FBRyxDQUFDN0ksQ0FBQyxDQUFDb0UsV0FBRixDQUFjdEcsS0FBZCxLQUF3QjZSLFdBQVcsQ0FBQyxDQUFELENBQVgsQ0FBZUUsVUFBdkMsR0FBb0RGLFdBQVcsQ0FBQzdSLEtBQVosRUFBckQsSUFBNEUsQ0FBQyxDQUExRjtBQUNILFNBRkQsTUFFTztBQUNIK0ssVUFBQUEsVUFBVSxHQUFJLENBQWQ7QUFDSDtBQUNKLE9BTkQsTUFNTztBQUNIQSxRQUFBQSxVQUFVLEdBQUc4RyxXQUFXLENBQUMsQ0FBRCxDQUFYLEdBQWlCQSxXQUFXLENBQUMsQ0FBRCxDQUFYLENBQWVFLFVBQWYsR0FBNEIsQ0FBQyxDQUE5QyxHQUFrRCxDQUEvRDtBQUNIOztBQUVELFVBQUk3UCxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQTdCLEVBQW1DO0FBQy9CLFlBQUlaLENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExQixJQUEwQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsS0FBckUsRUFBNEU7QUFDeEVpTyxVQUFBQSxXQUFXLEdBQUczUCxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLGNBQXZCLEVBQXVDSCxFQUF2QyxDQUEwQzBHLFVBQTFDLENBQWQ7QUFDSCxTQUZELE1BRU87QUFDSGdCLFVBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsY0FBdkIsRUFBdUNILEVBQXZDLENBQTBDMEcsVUFBVSxHQUFHM08sQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBdkIsR0FBc0MsQ0FBaEYsQ0FBZDtBQUNIOztBQUVELFlBQUl2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLElBQXRCLEVBQTRCO0FBQ3hCLGNBQUl1TixXQUFXLENBQUMsQ0FBRCxDQUFmLEVBQW9CO0FBQ2hCOUcsWUFBQUEsVUFBVSxHQUFHLENBQUM3SSxDQUFDLENBQUNvRSxXQUFGLENBQWN0RyxLQUFkLEtBQXdCNlIsV0FBVyxDQUFDLENBQUQsQ0FBWCxDQUFlRSxVQUF2QyxHQUFvREYsV0FBVyxDQUFDN1IsS0FBWixFQUFyRCxJQUE0RSxDQUFDLENBQTFGO0FBQ0gsV0FGRCxNQUVPO0FBQ0grSyxZQUFBQSxVQUFVLEdBQUksQ0FBZDtBQUNIO0FBQ0osU0FORCxNQU1PO0FBQ0hBLFVBQUFBLFVBQVUsR0FBRzhHLFdBQVcsQ0FBQyxDQUFELENBQVgsR0FBaUJBLFdBQVcsQ0FBQyxDQUFELENBQVgsQ0FBZUUsVUFBZixHQUE0QixDQUFDLENBQTlDLEdBQWtELENBQS9EO0FBQ0g7O0FBRURoSCxRQUFBQSxVQUFVLElBQUksQ0FBQzdJLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTNHLEtBQVIsS0FBa0I2UixXQUFXLENBQUNHLFVBQVosRUFBbkIsSUFBK0MsQ0FBN0Q7QUFDSDtBQUNKOztBQUVELFdBQU9qSCxVQUFQO0FBRUgsR0E3RkQ7O0FBK0ZBakosRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjJJLFNBQWhCLEdBQTRCblEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjRJLGNBQWhCLEdBQWlDLFVBQVNDLE1BQVQsRUFBaUI7QUFFMUUsUUFBSWpRLENBQUMsR0FBRyxJQUFSOztBQUVBLFdBQU9BLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXdTLE1BQVYsQ0FBUDtBQUVILEdBTkQ7O0FBUUFyUSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCd0csbUJBQWhCLEdBQXNDLFlBQVc7QUFFN0MsUUFBSTVOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXFQLFVBQVUsR0FBRyxDQURqQjtBQUFBLFFBRUlDLE9BQU8sR0FBRyxDQUZkO0FBQUEsUUFHSVksT0FBTyxHQUFHLEVBSGQ7QUFBQSxRQUlJQyxHQUpKOztBQU1BLFFBQUluUSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCeU8sTUFBQUEsR0FBRyxHQUFHblEsQ0FBQyxDQUFDa0UsVUFBUjtBQUNILEtBRkQsTUFFTztBQUNIbUwsTUFBQUEsVUFBVSxHQUFHclAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBVixHQUEyQixDQUFDLENBQXpDO0FBQ0E4TSxNQUFBQSxPQUFPLEdBQUd0UCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFWLEdBQTJCLENBQUMsQ0FBdEM7QUFDQTJOLE1BQUFBLEdBQUcsR0FBR25RLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZSxDQUFyQjtBQUNIOztBQUVELFdBQU9tTCxVQUFVLEdBQUdjLEdBQXBCLEVBQXlCO0FBQ3JCRCxNQUFBQSxPQUFPLENBQUNFLElBQVIsQ0FBYWYsVUFBYjtBQUNBQSxNQUFBQSxVQUFVLEdBQUdDLE9BQU8sR0FBR3RQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQWpDO0FBQ0E4TSxNQUFBQSxPQUFPLElBQUl0UCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFWLElBQTRCeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBdEMsR0FBcUR2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUEvRCxHQUFnRnhDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXJHO0FBQ0g7O0FBRUQsV0FBTzJOLE9BQVA7QUFFSCxHQXhCRDs7QUEwQkF0USxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCaUosUUFBaEIsR0FBMkIsWUFBVztBQUVsQyxXQUFPLElBQVA7QUFFSCxHQUpEOztBQU1BelEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmtKLGFBQWhCLEdBQWdDLFlBQVc7QUFFdkMsUUFBSXRRLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXVRLGVBREo7QUFBQSxRQUNxQkMsV0FEckI7QUFBQSxRQUNrQ0MsWUFEbEM7O0FBR0FBLElBQUFBLFlBQVksR0FBR3pRLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBekIsR0FBZ0NaLENBQUMsQ0FBQ21FLFVBQUYsR0FBZW1GLElBQUksQ0FBQ3NHLEtBQUwsQ0FBVzVQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBcEMsQ0FBL0MsR0FBd0YsQ0FBdkc7O0FBRUEsUUFBSXZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlGLFlBQVYsS0FBMkIsSUFBL0IsRUFBcUM7QUFDakMxQyxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNrRCxJQUFkLENBQW1CLGNBQW5CLEVBQW1DMUksSUFBbkMsQ0FBd0MsVUFBUytJLEtBQVQsRUFBZ0J0RixLQUFoQixFQUF1QjtBQUMzRCxZQUFJQSxLQUFLLENBQUN3TixVQUFOLEdBQW1CWSxZQUFuQixHQUFtQ25ULENBQUMsQ0FBQytFLEtBQUQsQ0FBRCxDQUFTeU4sVUFBVCxLQUF3QixDQUEzRCxHQUFpRTlQLENBQUMsQ0FBQ3dFLFNBQUYsR0FBYyxDQUFDLENBQXBGLEVBQXdGO0FBQ3BGZ00sVUFBQUEsV0FBVyxHQUFHbk8sS0FBZDtBQUNBLGlCQUFPLEtBQVA7QUFDSDtBQUNKLE9BTEQ7O0FBT0FrTyxNQUFBQSxlQUFlLEdBQUdqSCxJQUFJLENBQUNvSCxHQUFMLENBQVNwVCxDQUFDLENBQUNrVCxXQUFELENBQUQsQ0FBZWpKLElBQWYsQ0FBb0Isa0JBQXBCLElBQTBDdkgsQ0FBQyxDQUFDMEQsWUFBckQsS0FBc0UsQ0FBeEY7QUFFQSxhQUFPNk0sZUFBUDtBQUVILEtBWkQsTUFZTztBQUNILGFBQU92USxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFqQjtBQUNIO0FBRUosR0F2QkQ7O0FBeUJBNUMsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnVKLElBQWhCLEdBQXVCL1EsS0FBSyxDQUFDd0gsU0FBTixDQUFnQndKLFNBQWhCLEdBQTRCLFVBQVN2TyxLQUFULEVBQWdCNEssV0FBaEIsRUFBNkI7QUFFNUUsUUFBSWpOLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUMwRyxXQUFGLENBQWM7QUFDVlQsTUFBQUEsSUFBSSxFQUFFO0FBQ0Z1SCxRQUFBQSxPQUFPLEVBQUUsT0FEUDtBQUVGN0YsUUFBQUEsS0FBSyxFQUFFa0osUUFBUSxDQUFDeE8sS0FBRDtBQUZiO0FBREksS0FBZCxFQUtHNEssV0FMSDtBQU9ILEdBWEQ7O0FBYUFyTixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCRCxJQUFoQixHQUF1QixVQUFTMkosUUFBVCxFQUFtQjtBQUV0QyxRQUFJOVEsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxDQUFDMUMsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDMEYsT0FBSCxDQUFELENBQWFxTCxRQUFiLENBQXNCLG1CQUF0QixDQUFMLEVBQWlEO0FBRTdDelQsTUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDMEYsT0FBSCxDQUFELENBQWFoSCxRQUFiLENBQXNCLG1CQUF0Qjs7QUFFQXNCLE1BQUFBLENBQUMsQ0FBQ29MLFNBQUY7O0FBQ0FwTCxNQUFBQSxDQUFDLENBQUMrSyxRQUFGOztBQUNBL0ssTUFBQUEsQ0FBQyxDQUFDZ1IsUUFBRjs7QUFDQWhSLE1BQUFBLENBQUMsQ0FBQ2lSLFNBQUY7O0FBQ0FqUixNQUFBQSxDQUFDLENBQUNrUixVQUFGOztBQUNBbFIsTUFBQUEsQ0FBQyxDQUFDbVIsZ0JBQUY7O0FBQ0FuUixNQUFBQSxDQUFDLENBQUNvUixZQUFGOztBQUNBcFIsTUFBQUEsQ0FBQyxDQUFDa0wsVUFBRjs7QUFDQWxMLE1BQUFBLENBQUMsQ0FBQ2tNLGVBQUYsQ0FBa0IsSUFBbEI7O0FBQ0FsTSxNQUFBQSxDQUFDLENBQUNpUCxZQUFGO0FBRUg7O0FBRUQsUUFBSTZCLFFBQUosRUFBYztBQUNWOVEsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixNQUFsQixFQUEwQixDQUFDL00sQ0FBRCxDQUExQjtBQUNIOztBQUVELFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlDLGFBQVYsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbENGLE1BQUFBLENBQUMsQ0FBQ3FSLE9BQUY7QUFDSDs7QUFFRCxRQUFLclIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUQsUUFBZixFQUEwQjtBQUV0QlYsTUFBQUEsQ0FBQyxDQUFDc0YsTUFBRixHQUFXLEtBQVg7O0FBQ0F0RixNQUFBQSxDQUFDLENBQUNzRyxRQUFGO0FBRUg7QUFFSixHQXBDRDs7QUFzQ0ExRyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCaUssT0FBaEIsR0FBMEIsWUFBVztBQUNqQyxRQUFJclIsQ0FBQyxHQUFHLElBQVI7O0FBQ0FBLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXFHLEdBQVYsQ0FBYzFLLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2tELElBQWQsQ0FBbUIsZUFBbkIsQ0FBZCxFQUFtREMsSUFBbkQsQ0FBd0Q7QUFDcEQscUJBQWUsTUFEcUM7QUFFcEQsa0JBQVk7QUFGd0MsS0FBeEQsRUFHR0QsSUFISCxDQUdRLDBCQUhSLEVBR29DQyxJQUhwQyxDQUd5QztBQUNyQyxrQkFBWTtBQUR5QixLQUh6Qzs7QUFPQXZILElBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY21ELElBQWQsQ0FBbUIsTUFBbkIsRUFBMkIsU0FBM0I7O0FBRUF2SCxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVV5RixHQUFWLENBQWM5SixDQUFDLENBQUNvRSxXQUFGLENBQWNrRCxJQUFkLENBQW1CLGVBQW5CLENBQWQsRUFBbUQxSSxJQUFuRCxDQUF3RCxVQUFTcUMsQ0FBVCxFQUFZO0FBQ2hFM0QsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUssSUFBUixDQUFhO0FBQ1QsZ0JBQVEsUUFEQztBQUVULDRCQUFvQixnQkFBZ0J2SCxDQUFDLENBQUNGLFdBQWxCLEdBQWdDbUIsQ0FBaEMsR0FBb0M7QUFGL0MsT0FBYjtBQUlILEtBTEQ7O0FBT0EsUUFBSWpCLENBQUMsQ0FBQzRELEtBQUYsS0FBWSxJQUFoQixFQUFzQjtBQUNsQjVELE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsQ0FBUTJELElBQVIsQ0FBYSxNQUFiLEVBQXFCLFNBQXJCLEVBQWdDRCxJQUFoQyxDQUFxQyxJQUFyQyxFQUEyQzFJLElBQTNDLENBQWdELFVBQVNxQyxDQUFULEVBQVk7QUFDeEQzRCxRQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpSyxJQUFSLENBQWE7QUFDVCxrQkFBUSxjQURDO0FBRVQsMkJBQWlCLE9BRlI7QUFHVCwyQkFBaUIsZUFBZXZILENBQUMsQ0FBQ0YsV0FBakIsR0FBK0JtQixDQUEvQixHQUFtQyxFQUgzQztBQUlULGdCQUFNLGdCQUFnQmpCLENBQUMsQ0FBQ0YsV0FBbEIsR0FBZ0NtQixDQUFoQyxHQUFvQztBQUpqQyxTQUFiO0FBTUgsT0FQRCxFQVFLNkosS0FSTCxHQVFhdkQsSUFSYixDQVFrQixlQVJsQixFQVFtQyxNQVJuQyxFQVEyQytKLEdBUjNDLEdBU0toSyxJQVRMLENBU1UsUUFUVixFQVNvQkMsSUFUcEIsQ0FTeUIsTUFUekIsRUFTaUMsUUFUakMsRUFTMkMrSixHQVQzQyxHQVVLbFMsT0FWTCxDQVVhLEtBVmIsRUFVb0JtSSxJQVZwQixDQVV5QixNQVZ6QixFQVVpQyxTQVZqQztBQVdIOztBQUNEdkgsSUFBQUEsQ0FBQyxDQUFDcUgsV0FBRjtBQUVILEdBakNEOztBQW1DQXpILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JtSyxlQUFoQixHQUFrQyxZQUFXO0FBRXpDLFFBQUl2UixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVU2QyxNQUFWLEtBQXFCLElBQXJCLElBQTZCTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExRCxFQUF3RTtBQUNwRXZDLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FDSThKLEdBREosQ0FDUSxhQURSLEVBRUk3TyxFQUZKLENBRU8sYUFGUCxFQUVzQjtBQUNkc08sUUFBQUEsT0FBTyxFQUFFO0FBREssT0FGdEIsRUFJTXhOLENBQUMsQ0FBQzBHLFdBSlI7O0FBS0ExRyxNQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQ0krSixHQURKLENBQ1EsYUFEUixFQUVJN08sRUFGSixDQUVPLGFBRlAsRUFFc0I7QUFDZHNPLFFBQUFBLE9BQU8sRUFBRTtBQURLLE9BRnRCLEVBSU14TixDQUFDLENBQUMwRyxXQUpSO0FBS0g7QUFFSixHQWpCRDs7QUFtQkE5RyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCb0ssYUFBaEIsR0FBZ0MsWUFBVztBQUV2QyxRQUFJeFIsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEQsSUFBVixLQUFtQixJQUFuQixJQUEyQm5CLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXhELEVBQXNFO0FBQ2xFakYsTUFBQUEsQ0FBQyxDQUFDLElBQUQsRUFBTzBDLENBQUMsQ0FBQzRELEtBQVQsQ0FBRCxDQUFpQjFFLEVBQWpCLENBQW9CLGFBQXBCLEVBQW1DO0FBQy9Cc08sUUFBQUEsT0FBTyxFQUFFO0FBRHNCLE9BQW5DLEVBRUd4TixDQUFDLENBQUMwRyxXQUZMO0FBR0g7O0FBRUQsUUFBSzFHLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBELElBQVYsS0FBbUIsSUFBbkIsSUFBMkJuQixDQUFDLENBQUN2QyxPQUFGLENBQVV1RSxnQkFBVixLQUErQixJQUEvRCxFQUFzRTtBQUVsRTFFLE1BQUFBLENBQUMsQ0FBQyxJQUFELEVBQU8wQyxDQUFDLENBQUM0RCxLQUFULENBQUQsQ0FDSzFFLEVBREwsQ0FDUSxrQkFEUixFQUM0QjVCLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixJQUF4QixDQUQ1QixFQUVLZCxFQUZMLENBRVEsa0JBRlIsRUFFNEI1QixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnTyxTQUFWLEVBQXFCaE8sQ0FBckIsRUFBd0IsS0FBeEIsQ0FGNUI7QUFJSDtBQUVKLEdBbEJEOztBQW9CQUosRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnFLLGVBQWhCLEdBQWtDLFlBQVc7QUFFekMsUUFBSXpSLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUtBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXFFLFlBQWYsRUFBOEI7QUFFMUI5QixNQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsa0JBQVgsRUFBK0I1QixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnTyxTQUFWLEVBQXFCaE8sQ0FBckIsRUFBd0IsSUFBeEIsQ0FBL0I7O0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXZGLEVBQVIsQ0FBVyxrQkFBWCxFQUErQjVCLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixLQUF4QixDQUEvQjtBQUVIO0FBRUosR0FYRDs7QUFhQUosRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQitKLGdCQUFoQixHQUFtQyxZQUFXO0FBRTFDLFFBQUluUixDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDdVIsZUFBRjs7QUFFQXZSLElBQUFBLENBQUMsQ0FBQ3dSLGFBQUY7O0FBQ0F4UixJQUFBQSxDQUFDLENBQUN5UixlQUFGOztBQUVBelIsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRdkYsRUFBUixDQUFXLGtDQUFYLEVBQStDO0FBQzNDd1MsTUFBQUEsTUFBTSxFQUFFO0FBRG1DLEtBQS9DLEVBRUcxUixDQUFDLENBQUM4RyxZQUZMOztBQUdBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRdkYsRUFBUixDQUFXLGlDQUFYLEVBQThDO0FBQzFDd1MsTUFBQUEsTUFBTSxFQUFFO0FBRGtDLEtBQTlDLEVBRUcxUixDQUFDLENBQUM4RyxZQUZMOztBQUdBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRdkYsRUFBUixDQUFXLDhCQUFYLEVBQTJDO0FBQ3ZDd1MsTUFBQUEsTUFBTSxFQUFFO0FBRCtCLEtBQTNDLEVBRUcxUixDQUFDLENBQUM4RyxZQUZMOztBQUdBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRdkYsRUFBUixDQUFXLG9DQUFYLEVBQWlEO0FBQzdDd1MsTUFBQUEsTUFBTSxFQUFFO0FBRHFDLEtBQWpELEVBRUcxUixDQUFDLENBQUM4RyxZQUZMOztBQUlBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRdkYsRUFBUixDQUFXLGFBQVgsRUFBMEJjLENBQUMsQ0FBQzJHLFlBQTVCOztBQUVBckosSUFBQUEsQ0FBQyxDQUFDNkksUUFBRCxDQUFELENBQVlqSCxFQUFaLENBQWVjLENBQUMsQ0FBQzhGLGdCQUFqQixFQUFtQ3hJLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2lPLFVBQVYsRUFBc0JqTyxDQUF0QixDQUFuQzs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVV5QyxhQUFWLEtBQTRCLElBQWhDLEVBQXNDO0FBQ2xDRixNQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsZUFBWCxFQUE0QmMsQ0FBQyxDQUFDZ0gsVUFBOUI7QUFDSDs7QUFFRCxRQUFJaEgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0UsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ25FLE1BQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ29FLFdBQUgsQ0FBRCxDQUFpQmdFLFFBQWpCLEdBQTRCbEosRUFBNUIsQ0FBK0IsYUFBL0IsRUFBOENjLENBQUMsQ0FBQzRHLGFBQWhEO0FBQ0g7O0FBRUR0SixJQUFBQSxDQUFDLENBQUN1QyxNQUFELENBQUQsQ0FBVVgsRUFBVixDQUFhLG1DQUFtQ2MsQ0FBQyxDQUFDRixXQUFsRCxFQUErRHhDLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ21PLGlCQUFWLEVBQTZCbk8sQ0FBN0IsQ0FBL0Q7QUFFQTFDLElBQUFBLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVWCxFQUFWLENBQWEsd0JBQXdCYyxDQUFDLENBQUNGLFdBQXZDLEVBQW9EeEMsQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDb08sTUFBVixFQUFrQnBPLENBQWxCLENBQXBEO0FBRUExQyxJQUFBQSxDQUFDLENBQUMsbUJBQUQsRUFBc0IwQyxDQUFDLENBQUNvRSxXQUF4QixDQUFELENBQXNDbEYsRUFBdEMsQ0FBeUMsV0FBekMsRUFBc0RjLENBQUMsQ0FBQ3VOLGNBQXhEO0FBRUFqUSxJQUFBQSxDQUFDLENBQUN1QyxNQUFELENBQUQsQ0FBVVgsRUFBVixDQUFhLHNCQUFzQmMsQ0FBQyxDQUFDRixXQUFyQyxFQUFrREUsQ0FBQyxDQUFDNkcsV0FBcEQ7QUFDQXZKLElBQUFBLENBQUMsQ0FBQzZJLFFBQUQsQ0FBRCxDQUFZakgsRUFBWixDQUFlLHVCQUF1QmMsQ0FBQyxDQUFDRixXQUF4QyxFQUFxREUsQ0FBQyxDQUFDNkcsV0FBdkQ7QUFFSCxHQTNDRDs7QUE2Q0FqSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCdUssTUFBaEIsR0FBeUIsWUFBVztBQUVoQyxRQUFJM1IsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkMsTUFBVixLQUFxQixJQUFyQixJQUE2Qk4sQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBMUQsRUFBd0U7QUFFcEV2QyxNQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWEyTixJQUFiOztBQUNBNVIsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhNE4sSUFBYjtBQUVIOztBQUVELFFBQUk1UixDQUFDLENBQUN2QyxPQUFGLENBQVUwRCxJQUFWLEtBQW1CLElBQW5CLElBQTJCbkIsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBeEQsRUFBc0U7QUFFbEV2QyxNQUFBQSxDQUFDLENBQUM0RCxLQUFGLENBQVFnTyxJQUFSO0FBRUg7QUFFSixHQWpCRDs7QUFtQkFoUyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCSixVQUFoQixHQUE2QixVQUFTZ0csS0FBVCxFQUFnQjtBQUV6QyxRQUFJaE4sQ0FBQyxHQUFHLElBQVIsQ0FGeUMsQ0FHeEM7OztBQUNELFFBQUcsQ0FBQ2dOLEtBQUssQ0FBQ2pELE1BQU4sQ0FBYThILE9BQWIsQ0FBcUJDLEtBQXJCLENBQTJCLHVCQUEzQixDQUFKLEVBQXlEO0FBQ3JELFVBQUk5RSxLQUFLLENBQUMrRSxPQUFOLEtBQWtCLEVBQWxCLElBQXdCL1IsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUMsYUFBVixLQUE0QixJQUF4RCxFQUE4RDtBQUMxREYsUUFBQUEsQ0FBQyxDQUFDMEcsV0FBRixDQUFjO0FBQ1ZULFVBQUFBLElBQUksRUFBRTtBQUNGdUgsWUFBQUEsT0FBTyxFQUFFeE4sQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkUsR0FBVixLQUFrQixJQUFsQixHQUF5QixNQUF6QixHQUFtQztBQUQxQztBQURJLFNBQWQ7QUFLSCxPQU5ELE1BTU8sSUFBSTRLLEtBQUssQ0FBQytFLE9BQU4sS0FBa0IsRUFBbEIsSUFBd0IvUixDQUFDLENBQUN2QyxPQUFGLENBQVV5QyxhQUFWLEtBQTRCLElBQXhELEVBQThEO0FBQ2pFRixRQUFBQSxDQUFDLENBQUMwRyxXQUFGLENBQWM7QUFDVlQsVUFBQUEsSUFBSSxFQUFFO0FBQ0Z1SCxZQUFBQSxPQUFPLEVBQUV4TixDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLElBQWxCLEdBQXlCLFVBQXpCLEdBQXNDO0FBRDdDO0FBREksU0FBZDtBQUtIO0FBQ0o7QUFFSixHQXBCRDs7QUFzQkF4QyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCeEYsUUFBaEIsR0FBMkIsWUFBVztBQUVsQyxRQUFJNUIsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJZ1MsU0FESjtBQUFBLFFBQ2VDLFVBRGY7QUFBQSxRQUMyQkMsVUFEM0I7QUFBQSxRQUN1Q0MsUUFEdkM7O0FBR0EsYUFBU0MsVUFBVCxDQUFvQkMsV0FBcEIsRUFBaUM7QUFFN0IvVSxNQUFBQSxDQUFDLENBQUMsZ0JBQUQsRUFBbUIrVSxXQUFuQixDQUFELENBQWlDelQsSUFBakMsQ0FBc0MsWUFBVztBQUU3QyxZQUFJMFQsS0FBSyxHQUFHaFYsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUFBLFlBQ0lpVixXQUFXLEdBQUdqVixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpSyxJQUFSLENBQWEsV0FBYixDQURsQjtBQUFBLFlBRUlpTCxXQUFXLEdBQUdyTSxRQUFRLENBQUMwRixhQUFULENBQXVCLEtBQXZCLENBRmxCOztBQUlBMkcsUUFBQUEsV0FBVyxDQUFDQyxNQUFaLEdBQXFCLFlBQVc7QUFFNUJILFVBQUFBLEtBQUssQ0FDQTNKLE9BREwsQ0FDYTtBQUFFaUcsWUFBQUEsT0FBTyxFQUFFO0FBQVgsV0FEYixFQUM2QixHQUQ3QixFQUNrQyxZQUFXO0FBQ3JDMEQsWUFBQUEsS0FBSyxDQUNBL0ssSUFETCxDQUNVLEtBRFYsRUFDaUJnTCxXQURqQixFQUVLNUosT0FGTCxDQUVhO0FBQUVpRyxjQUFBQSxPQUFPLEVBQUU7QUFBWCxhQUZiLEVBRTZCLEdBRjdCLEVBRWtDLFlBQVc7QUFDckMwRCxjQUFBQSxLQUFLLENBQ0E5SCxVQURMLENBQ2dCLFdBRGhCLEVBRUtELFdBRkwsQ0FFaUIsZUFGakI7QUFHSCxhQU5MOztBQU9BdkssWUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixZQUFsQixFQUFnQyxDQUFDL00sQ0FBRCxFQUFJc1MsS0FBSixFQUFXQyxXQUFYLENBQWhDO0FBQ0gsV0FWTDtBQVlILFNBZEQ7O0FBZ0JBQyxRQUFBQSxXQUFXLENBQUNFLE9BQVosR0FBc0IsWUFBVztBQUU3QkosVUFBQUEsS0FBSyxDQUNBOUgsVUFETCxDQUNpQixXQURqQixFQUVLRCxXQUZMLENBRWtCLGVBRmxCLEVBR0s3TCxRQUhMLENBR2Usc0JBSGY7O0FBS0FzQixVQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLGVBQWxCLEVBQW1DLENBQUUvTSxDQUFGLEVBQUtzUyxLQUFMLEVBQVlDLFdBQVosQ0FBbkM7QUFFSCxTQVREOztBQVdBQyxRQUFBQSxXQUFXLENBQUNHLEdBQVosR0FBa0JKLFdBQWxCO0FBRUgsT0FuQ0Q7QUFxQ0g7O0FBRUQsUUFBSXZTLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBN0IsRUFBbUM7QUFDL0IsVUFBSVosQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUEzQixFQUFpQztBQUM3QndRLFFBQUFBLFVBQVUsR0FBR2xTLENBQUMsQ0FBQzBELFlBQUYsSUFBa0IxRCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCLENBQXpCLEdBQTZCLENBQS9DLENBQWI7QUFDQTRQLFFBQUFBLFFBQVEsR0FBR0QsVUFBVSxHQUFHbFMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBdkIsR0FBc0MsQ0FBakQ7QUFDSCxPQUhELE1BR087QUFDSDJQLFFBQUFBLFVBQVUsR0FBRzVJLElBQUksQ0FBQzZHLEdBQUwsQ0FBUyxDQUFULEVBQVluUSxDQUFDLENBQUMwRCxZQUFGLElBQWtCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUF6QixHQUE2QixDQUEvQyxDQUFaLENBQWI7QUFDQTRQLFFBQUFBLFFBQVEsR0FBRyxLQUFLblMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUF6QixHQUE2QixDQUFsQyxJQUF1Q3ZDLENBQUMsQ0FBQzBELFlBQXBEO0FBQ0g7QUFDSixLQVJELE1BUU87QUFDSHdPLE1BQUFBLFVBQVUsR0FBR2xTLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsR0FBcUIxQixDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCdkMsQ0FBQyxDQUFDMEQsWUFBaEQsR0FBK0QxRCxDQUFDLENBQUMwRCxZQUE5RTtBQUNBeU8sTUFBQUEsUUFBUSxHQUFHN0ksSUFBSSxDQUFDQyxJQUFMLENBQVUySSxVQUFVLEdBQUdsUyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFqQyxDQUFYOztBQUNBLFVBQUl2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLElBQXZCLEVBQTZCO0FBQ3pCLFlBQUkwUSxVQUFVLEdBQUcsQ0FBakIsRUFBb0JBLFVBQVU7QUFDOUIsWUFBSUMsUUFBUSxJQUFJblMsQ0FBQyxDQUFDa0UsVUFBbEIsRUFBOEJpTyxRQUFRO0FBQ3pDO0FBQ0o7O0FBRURILElBQUFBLFNBQVMsR0FBR2hTLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTRCLElBQVYsQ0FBZSxjQUFmLEVBQStCc0wsS0FBL0IsQ0FBcUNWLFVBQXJDLEVBQWlEQyxRQUFqRCxDQUFaO0FBQ0FDLElBQUFBLFVBQVUsQ0FBQ0osU0FBRCxDQUFWOztBQUVBLFFBQUloUyxDQUFDLENBQUNrRSxVQUFGLElBQWdCbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBOUIsRUFBNEM7QUFDeEMwUCxNQUFBQSxVQUFVLEdBQUdqUyxDQUFDLENBQUMwRixPQUFGLENBQVU0QixJQUFWLENBQWUsY0FBZixDQUFiO0FBQ0E4SyxNQUFBQSxVQUFVLENBQUNILFVBQUQsQ0FBVjtBQUNILEtBSEQsTUFJQSxJQUFJalMsQ0FBQyxDQUFDMEQsWUFBRixJQUFrQjFELENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQS9DLEVBQTZEO0FBQ3pEMFAsTUFBQUEsVUFBVSxHQUFHalMsQ0FBQyxDQUFDMEYsT0FBRixDQUFVNEIsSUFBVixDQUFlLGVBQWYsRUFBZ0NzTCxLQUFoQyxDQUFzQyxDQUF0QyxFQUF5QzVTLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQW5ELENBQWI7QUFDQTZQLE1BQUFBLFVBQVUsQ0FBQ0gsVUFBRCxDQUFWO0FBQ0gsS0FIRCxNQUdPLElBQUlqUyxDQUFDLENBQUMwRCxZQUFGLEtBQW1CLENBQXZCLEVBQTBCO0FBQzdCdU8sTUFBQUEsVUFBVSxHQUFHalMsQ0FBQyxDQUFDMEYsT0FBRixDQUFVNEIsSUFBVixDQUFlLGVBQWYsRUFBZ0NzTCxLQUFoQyxDQUFzQzVTLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBQyxDQUFoRSxDQUFiO0FBQ0E2UCxNQUFBQSxVQUFVLENBQUNILFVBQUQsQ0FBVjtBQUNIO0FBRUosR0E5RUQ7O0FBZ0ZBclMsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjhKLFVBQWhCLEdBQTZCLFlBQVc7QUFFcEMsUUFBSWxSLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUM2RyxXQUFGOztBQUVBN0csSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQjtBQUNkNlAsTUFBQUEsT0FBTyxFQUFFO0FBREssS0FBbEI7O0FBSUE1TyxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVU2RSxXQUFWLENBQXNCLGVBQXRCOztBQUVBdkssSUFBQUEsQ0FBQyxDQUFDMlIsTUFBRjs7QUFFQSxRQUFJM1IsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUUsUUFBVixLQUF1QixhQUEzQixFQUEwQztBQUN0QzVCLE1BQUFBLENBQUMsQ0FBQzZTLG1CQUFGO0FBQ0g7QUFFSixHQWxCRDs7QUFvQkFqVCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCMEwsSUFBaEIsR0FBdUJsVCxLQUFLLENBQUN3SCxTQUFOLENBQWdCMkwsU0FBaEIsR0FBNEIsWUFBVztBQUUxRCxRQUFJL1MsQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzBHLFdBQUYsQ0FBYztBQUNWVCxNQUFBQSxJQUFJLEVBQUU7QUFDRnVILFFBQUFBLE9BQU8sRUFBRTtBQURQO0FBREksS0FBZDtBQU1ILEdBVkQ7O0FBWUE1TixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCK0csaUJBQWhCLEdBQW9DLFlBQVc7QUFFM0MsUUFBSW5PLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUNrTSxlQUFGOztBQUNBbE0sSUFBQUEsQ0FBQyxDQUFDNkcsV0FBRjtBQUVILEdBUEQ7O0FBU0FqSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNEwsS0FBaEIsR0FBd0JwVCxLQUFLLENBQUN3SCxTQUFOLENBQWdCNkwsVUFBaEIsR0FBNkIsWUFBVztBQUU1RCxRQUFJalQsQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3dHLGFBQUY7O0FBQ0F4RyxJQUFBQSxDQUFDLENBQUNzRixNQUFGLEdBQVcsSUFBWDtBQUVILEdBUEQ7O0FBU0ExRixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCOEwsSUFBaEIsR0FBdUJ0VCxLQUFLLENBQUN3SCxTQUFOLENBQWdCK0wsU0FBaEIsR0FBNEIsWUFBVztBQUUxRCxRQUFJblQsQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3NHLFFBQUY7O0FBQ0F0RyxJQUFBQSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRCxRQUFWLEdBQXFCLElBQXJCO0FBQ0FWLElBQUFBLENBQUMsQ0FBQ3NGLE1BQUYsR0FBVyxLQUFYO0FBQ0F0RixJQUFBQSxDQUFDLENBQUNtRixRQUFGLEdBQWEsS0FBYjtBQUNBbkYsSUFBQUEsQ0FBQyxDQUFDb0YsV0FBRixHQUFnQixLQUFoQjtBQUVILEdBVkQ7O0FBWUF4RixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCZ00sU0FBaEIsR0FBNEIsVUFBU3pMLEtBQVQsRUFBZ0I7QUFFeEMsUUFBSTNILENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUksQ0FBQ0EsQ0FBQyxDQUFDNEUsU0FBUCxFQUFtQjtBQUVmNUUsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixhQUFsQixFQUFpQyxDQUFDL00sQ0FBRCxFQUFJMkgsS0FBSixDQUFqQzs7QUFFQTNILE1BQUFBLENBQUMsQ0FBQ3FELFNBQUYsR0FBYyxLQUFkOztBQUVBckQsTUFBQUEsQ0FBQyxDQUFDNkcsV0FBRjs7QUFFQTdHLE1BQUFBLENBQUMsQ0FBQ3dFLFNBQUYsR0FBYyxJQUFkOztBQUVBLFVBQUt4RSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRCxRQUFmLEVBQTBCO0FBQ3RCVixRQUFBQSxDQUFDLENBQUNzRyxRQUFGO0FBQ0g7O0FBRUQsVUFBSXRHLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlDLGFBQVYsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbENGLFFBQUFBLENBQUMsQ0FBQ3FSLE9BQUY7QUFDSDtBQUVKO0FBRUosR0F4QkQ7O0FBMEJBelIsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmlNLElBQWhCLEdBQXVCelQsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmtNLFNBQWhCLEdBQTRCLFlBQVc7QUFFMUQsUUFBSXRULENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUMwRyxXQUFGLENBQWM7QUFDVlQsTUFBQUEsSUFBSSxFQUFFO0FBQ0Z1SCxRQUFBQSxPQUFPLEVBQUU7QUFEUDtBQURJLEtBQWQ7QUFNSCxHQVZEOztBQVlBNU4sRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQm1HLGNBQWhCLEdBQWlDLFVBQVNQLEtBQVQsRUFBZ0I7QUFFN0NBLElBQUFBLEtBQUssQ0FBQ08sY0FBTjtBQUVILEdBSkQ7O0FBTUEzTixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCeUwsbUJBQWhCLEdBQXNDLFVBQVVVLFFBQVYsRUFBcUI7QUFFdkRBLElBQUFBLFFBQVEsR0FBR0EsUUFBUSxJQUFJLENBQXZCOztBQUVBLFFBQUl2VCxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l3VCxXQUFXLEdBQUdsVyxDQUFDLENBQUUsZ0JBQUYsRUFBb0IwQyxDQUFDLENBQUMwRixPQUF0QixDQURuQjtBQUFBLFFBRUk0TSxLQUZKO0FBQUEsUUFHSUMsV0FISjtBQUFBLFFBSUlDLFdBSko7O0FBTUEsUUFBS2dCLFdBQVcsQ0FBQzFMLE1BQWpCLEVBQTBCO0FBRXRCd0ssTUFBQUEsS0FBSyxHQUFHa0IsV0FBVyxDQUFDMUksS0FBWixFQUFSO0FBQ0F5SCxNQUFBQSxXQUFXLEdBQUdELEtBQUssQ0FBQy9LLElBQU4sQ0FBVyxXQUFYLENBQWQ7QUFDQWlMLE1BQUFBLFdBQVcsR0FBR3JNLFFBQVEsQ0FBQzBGLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZDs7QUFFQTJHLE1BQUFBLFdBQVcsQ0FBQ0MsTUFBWixHQUFxQixZQUFXO0FBRTVCSCxRQUFBQSxLQUFLLENBQ0EvSyxJQURMLENBQ1csS0FEWCxFQUNrQmdMLFdBRGxCLEVBRUsvSCxVQUZMLENBRWdCLFdBRmhCLEVBR0tELFdBSEwsQ0FHaUIsZUFIakI7O0FBS0EsWUFBS3ZLLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBDLGNBQVYsS0FBNkIsSUFBbEMsRUFBeUM7QUFDckNILFVBQUFBLENBQUMsQ0FBQzZHLFdBQUY7QUFDSDs7QUFFRDdHLFFBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsWUFBbEIsRUFBZ0MsQ0FBRS9NLENBQUYsRUFBS3NTLEtBQUwsRUFBWUMsV0FBWixDQUFoQzs7QUFDQXZTLFFBQUFBLENBQUMsQ0FBQzZTLG1CQUFGO0FBRUgsT0FkRDs7QUFnQkFMLE1BQUFBLFdBQVcsQ0FBQ0UsT0FBWixHQUFzQixZQUFXO0FBRTdCLFlBQUthLFFBQVEsR0FBRyxDQUFoQixFQUFvQjtBQUVoQjs7Ozs7QUFLQTVKLFVBQUFBLFVBQVUsQ0FBRSxZQUFXO0FBQ25CM0osWUFBQUEsQ0FBQyxDQUFDNlMsbUJBQUYsQ0FBdUJVLFFBQVEsR0FBRyxDQUFsQztBQUNILFdBRlMsRUFFUCxHQUZPLENBQVY7QUFJSCxTQVhELE1BV087QUFFSGpCLFVBQUFBLEtBQUssQ0FDQTlILFVBREwsQ0FDaUIsV0FEakIsRUFFS0QsV0FGTCxDQUVrQixlQUZsQixFQUdLN0wsUUFITCxDQUdlLHNCQUhmOztBQUtBc0IsVUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixlQUFsQixFQUFtQyxDQUFFL00sQ0FBRixFQUFLc1MsS0FBTCxFQUFZQyxXQUFaLENBQW5DOztBQUVBdlMsVUFBQUEsQ0FBQyxDQUFDNlMsbUJBQUY7QUFFSDtBQUVKLE9BMUJEOztBQTRCQUwsTUFBQUEsV0FBVyxDQUFDRyxHQUFaLEdBQWtCSixXQUFsQjtBQUVILEtBcERELE1Bb0RPO0FBRUh2UyxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLGlCQUFsQixFQUFxQyxDQUFFL00sQ0FBRixDQUFyQztBQUVIO0FBRUosR0FwRUQ7O0FBc0VBSixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCMEYsT0FBaEIsR0FBMEIsVUFBVTJHLFlBQVYsRUFBeUI7QUFFL0MsUUFBSXpULENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBYzBELFlBQWQ7QUFBQSxRQUE0QmdRLGdCQUE1Qjs7QUFFQUEsSUFBQUEsZ0JBQWdCLEdBQUcxVCxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE1QyxDQUorQyxDQU0vQztBQUNBOztBQUNBLFFBQUksQ0FBQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVgsSUFBeUIxQixDQUFDLENBQUMwRCxZQUFGLEdBQWlCZ1EsZ0JBQTlDLEVBQWtFO0FBQzlEMVQsTUFBQUEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQmdRLGdCQUFqQjtBQUNILEtBVjhDLENBWS9DOzs7QUFDQSxRQUFLMVQsQ0FBQyxDQUFDa0UsVUFBRixJQUFnQmxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQS9CLEVBQThDO0FBQzFDdkMsTUFBQUEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQixDQUFqQjtBQUVIOztBQUVEQSxJQUFBQSxZQUFZLEdBQUcxRCxDQUFDLENBQUMwRCxZQUFqQjs7QUFFQTFELElBQUFBLENBQUMsQ0FBQ3dPLE9BQUYsQ0FBVSxJQUFWOztBQUVBbFIsSUFBQUEsQ0FBQyxDQUFDa0IsTUFBRixDQUFTd0IsQ0FBVCxFQUFZQSxDQUFDLENBQUNvRCxRQUFkLEVBQXdCO0FBQUVNLE1BQUFBLFlBQVksRUFBRUE7QUFBaEIsS0FBeEI7O0FBRUExRCxJQUFBQSxDQUFDLENBQUNtSCxJQUFGOztBQUVBLFFBQUksQ0FBQ3NNLFlBQUwsRUFBb0I7QUFFaEJ6VCxNQUFBQSxDQUFDLENBQUMwRyxXQUFGLENBQWM7QUFDVlQsUUFBQUEsSUFBSSxFQUFFO0FBQ0Z1SCxVQUFBQSxPQUFPLEVBQUUsT0FEUDtBQUVGN0YsVUFBQUEsS0FBSyxFQUFFakU7QUFGTDtBQURJLE9BQWQsRUFLRyxLQUxIO0FBT0g7QUFFSixHQXJDRDs7QUF1Q0E5RCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCRixtQkFBaEIsR0FBc0MsWUFBVztBQUU3QyxRQUFJbEgsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUFjcU0sVUFBZDtBQUFBLFFBQTBCc0gsaUJBQTFCO0FBQUEsUUFBNkNDLENBQTdDO0FBQUEsUUFDSUMsa0JBQWtCLEdBQUc3VCxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLElBQXdCLElBRGpEOztBQUdBLFFBQUs1RSxDQUFDLENBQUN3VyxJQUFGLENBQU9ELGtCQUFQLE1BQStCLE9BQS9CLElBQTBDQSxrQkFBa0IsQ0FBQy9MLE1BQWxFLEVBQTJFO0FBRXZFOUgsTUFBQUEsQ0FBQyxDQUFDaUMsU0FBRixHQUFjakMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVd0UsU0FBVixJQUF1QixRQUFyQzs7QUFFQSxXQUFNb0ssVUFBTixJQUFvQndILGtCQUFwQixFQUF5QztBQUVyQ0QsUUFBQUEsQ0FBQyxHQUFHNVQsQ0FBQyxDQUFDZ0YsV0FBRixDQUFjOEMsTUFBZCxHQUFxQixDQUF6QjtBQUNBNkwsUUFBQUEsaUJBQWlCLEdBQUdFLGtCQUFrQixDQUFDeEgsVUFBRCxDQUFsQixDQUErQkEsVUFBbkQ7O0FBRUEsWUFBSXdILGtCQUFrQixDQUFDakgsY0FBbkIsQ0FBa0NQLFVBQWxDLENBQUosRUFBbUQ7QUFFL0M7QUFDQTtBQUNBLGlCQUFPdUgsQ0FBQyxJQUFJLENBQVosRUFBZ0I7QUFDWixnQkFBSTVULENBQUMsQ0FBQ2dGLFdBQUYsQ0FBYzRPLENBQWQsS0FBb0I1VCxDQUFDLENBQUNnRixXQUFGLENBQWM0TyxDQUFkLE1BQXFCRCxpQkFBN0MsRUFBaUU7QUFDN0QzVCxjQUFBQSxDQUFDLENBQUNnRixXQUFGLENBQWMrTyxNQUFkLENBQXFCSCxDQUFyQixFQUF1QixDQUF2QjtBQUNIOztBQUNEQSxZQUFBQSxDQUFDO0FBQ0o7O0FBRUQ1VCxVQUFBQSxDQUFDLENBQUNnRixXQUFGLENBQWNvTCxJQUFkLENBQW1CdUQsaUJBQW5COztBQUNBM1QsVUFBQUEsQ0FBQyxDQUFDaUYsa0JBQUYsQ0FBcUIwTyxpQkFBckIsSUFBMENFLGtCQUFrQixDQUFDeEgsVUFBRCxDQUFsQixDQUErQjlOLFFBQXpFO0FBRUg7QUFFSjs7QUFFRHlCLE1BQUFBLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBY2dQLElBQWQsQ0FBbUIsVUFBUzNJLENBQVQsRUFBWUMsQ0FBWixFQUFlO0FBQzlCLGVBQVN0TCxDQUFDLENBQUN2QyxPQUFGLENBQVVvRSxXQUFaLEdBQTRCd0osQ0FBQyxHQUFDQyxDQUE5QixHQUFrQ0EsQ0FBQyxHQUFDRCxDQUEzQztBQUNILE9BRkQ7QUFJSDtBQUVKLEdBdENEOztBQXdDQXpMLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JtQixNQUFoQixHQUF5QixZQUFXO0FBRWhDLFFBQUl2SSxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixHQUNJckUsQ0FBQyxDQUFDb0UsV0FBRixDQUNLZ0UsUUFETCxDQUNjcEksQ0FBQyxDQUFDdkMsT0FBRixDQUFVNEUsS0FEeEIsRUFFSzNELFFBRkwsQ0FFYyxhQUZkLENBREo7QUFLQXNCLElBQUFBLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlELE1BQXpCOztBQUVBLFFBQUk5SCxDQUFDLENBQUMwRCxZQUFGLElBQWtCMUQsQ0FBQyxDQUFDa0UsVUFBcEIsSUFBa0NsRSxDQUFDLENBQUMwRCxZQUFGLEtBQW1CLENBQXpELEVBQTREO0FBQ3hEMUQsTUFBQUEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUE1QztBQUNIOztBQUVELFFBQUl4QyxDQUFDLENBQUNrRSxVQUFGLElBQWdCbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBOUIsRUFBNEM7QUFDeEN2QyxNQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCLENBQWpCO0FBQ0g7O0FBRUQxRCxJQUFBQSxDQUFDLENBQUNrSCxtQkFBRjs7QUFFQWxILElBQUFBLENBQUMsQ0FBQ2dSLFFBQUY7O0FBQ0FoUixJQUFBQSxDQUFDLENBQUNpTCxhQUFGOztBQUNBakwsSUFBQUEsQ0FBQyxDQUFDc0ssV0FBRjs7QUFDQXRLLElBQUFBLENBQUMsQ0FBQ29SLFlBQUY7O0FBQ0FwUixJQUFBQSxDQUFDLENBQUN1UixlQUFGOztBQUNBdlIsSUFBQUEsQ0FBQyxDQUFDMkssU0FBRjs7QUFDQTNLLElBQUFBLENBQUMsQ0FBQ2tMLFVBQUY7O0FBQ0FsTCxJQUFBQSxDQUFDLENBQUN3UixhQUFGOztBQUNBeFIsSUFBQUEsQ0FBQyxDQUFDa08sa0JBQUY7O0FBQ0FsTyxJQUFBQSxDQUFDLENBQUN5UixlQUFGOztBQUVBelIsSUFBQUEsQ0FBQyxDQUFDa00sZUFBRixDQUFrQixLQUFsQixFQUF5QixJQUF6Qjs7QUFFQSxRQUFJbE0sQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0UsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ25FLE1BQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ29FLFdBQUgsQ0FBRCxDQUFpQmdFLFFBQWpCLEdBQTRCbEosRUFBNUIsQ0FBK0IsYUFBL0IsRUFBOENjLENBQUMsQ0FBQzRHLGFBQWhEO0FBQ0g7O0FBRUQ1RyxJQUFBQSxDQUFDLENBQUNtTCxlQUFGLENBQWtCLE9BQU9uTCxDQUFDLENBQUMwRCxZQUFULEtBQTBCLFFBQTFCLEdBQXFDMUQsQ0FBQyxDQUFDMEQsWUFBdkMsR0FBc0QsQ0FBeEU7O0FBRUExRCxJQUFBQSxDQUFDLENBQUM2RyxXQUFGOztBQUNBN0csSUFBQUEsQ0FBQyxDQUFDaVAsWUFBRjs7QUFFQWpQLElBQUFBLENBQUMsQ0FBQ3NGLE1BQUYsR0FBVyxDQUFDdEYsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUQsUUFBdEI7O0FBQ0FWLElBQUFBLENBQUMsQ0FBQ3NHLFFBQUY7O0FBRUF0RyxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLFFBQWxCLEVBQTRCLENBQUMvTSxDQUFELENBQTVCO0FBRUgsR0FoREQ7O0FBa0RBSixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCZ0gsTUFBaEIsR0FBeUIsWUFBVztBQUVoQyxRQUFJcE8sQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSTFDLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVL0IsS0FBVixPQUFzQmtDLENBQUMsQ0FBQytGLFdBQTVCLEVBQXlDO0FBQ3JDa08sTUFBQUEsWUFBWSxDQUFDalUsQ0FBQyxDQUFDa1UsV0FBSCxDQUFaO0FBQ0FsVSxNQUFBQSxDQUFDLENBQUNrVSxXQUFGLEdBQWdCclUsTUFBTSxDQUFDOEosVUFBUCxDQUFrQixZQUFXO0FBQ3pDM0osUUFBQUEsQ0FBQyxDQUFDK0YsV0FBRixHQUFnQnpJLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVL0IsS0FBVixFQUFoQjs7QUFDQWtDLFFBQUFBLENBQUMsQ0FBQ2tNLGVBQUY7O0FBQ0EsWUFBSSxDQUFDbE0sQ0FBQyxDQUFDNEUsU0FBUCxFQUFtQjtBQUFFNUUsVUFBQUEsQ0FBQyxDQUFDNkcsV0FBRjtBQUFrQjtBQUMxQyxPQUplLEVBSWIsRUFKYSxDQUFoQjtBQUtIO0FBQ0osR0FaRDs7QUFjQWpILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IrTSxXQUFoQixHQUE4QnZVLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JnTixXQUFoQixHQUE4QixVQUFTek0sS0FBVCxFQUFnQjBNLFlBQWhCLEVBQThCQyxTQUE5QixFQUF5QztBQUVqRyxRQUFJdFUsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxPQUFPMkgsS0FBUCxLQUFrQixTQUF0QixFQUFpQztBQUM3QjBNLE1BQUFBLFlBQVksR0FBRzFNLEtBQWY7QUFDQUEsTUFBQUEsS0FBSyxHQUFHME0sWUFBWSxLQUFLLElBQWpCLEdBQXdCLENBQXhCLEdBQTRCclUsQ0FBQyxDQUFDa0UsVUFBRixHQUFlLENBQW5EO0FBQ0gsS0FIRCxNQUdPO0FBQ0h5RCxNQUFBQSxLQUFLLEdBQUcwTSxZQUFZLEtBQUssSUFBakIsR0FBd0IsRUFBRTFNLEtBQTFCLEdBQWtDQSxLQUExQztBQUNIOztBQUVELFFBQUkzSCxDQUFDLENBQUNrRSxVQUFGLEdBQWUsQ0FBZixJQUFvQnlELEtBQUssR0FBRyxDQUE1QixJQUFpQ0EsS0FBSyxHQUFHM0gsQ0FBQyxDQUFDa0UsVUFBRixHQUFlLENBQTVELEVBQStEO0FBQzNELGFBQU8sS0FBUDtBQUNIOztBQUVEbEUsSUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjs7QUFFQSxRQUFJeU0sU0FBUyxLQUFLLElBQWxCLEVBQXdCO0FBQ3BCdFUsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxHQUF5QnFHLE1BQXpCO0FBQ0gsS0FGRCxNQUVPO0FBQ0h6TyxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLEtBQUszSyxPQUFMLENBQWE0RSxLQUFwQyxFQUEyQzRGLEVBQTNDLENBQThDTixLQUE5QyxFQUFxRDhHLE1BQXJEO0FBQ0g7O0FBRUR6TyxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLEdBQVlyRSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLEtBQUszSyxPQUFMLENBQWE0RSxLQUFwQyxDQUFaOztBQUVBckMsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixLQUFLM0ssT0FBTCxDQUFhNEUsS0FBcEMsRUFBMkNnRyxNQUEzQzs7QUFFQXJJLElBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2tFLE1BQWQsQ0FBcUJ0SSxDQUFDLENBQUNxRSxPQUF2Qjs7QUFFQXJFLElBQUFBLENBQUMsQ0FBQzJGLFlBQUYsR0FBaUIzRixDQUFDLENBQUNxRSxPQUFuQjs7QUFFQXJFLElBQUFBLENBQUMsQ0FBQ3VJLE1BQUY7QUFFSCxHQWpDRDs7QUFtQ0EzSSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCbU4sTUFBaEIsR0FBeUIsVUFBU0MsUUFBVCxFQUFtQjtBQUV4QyxRQUFJeFUsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJeVUsYUFBYSxHQUFHLEVBRHBCO0FBQUEsUUFFSUMsQ0FGSjtBQUFBLFFBRU9DLENBRlA7O0FBSUEsUUFBSTNVLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEJvUyxNQUFBQSxRQUFRLEdBQUcsQ0FBQ0EsUUFBWjtBQUNIOztBQUNERSxJQUFBQSxDQUFDLEdBQUcxVSxDQUFDLENBQUN1RixZQUFGLElBQWtCLE1BQWxCLEdBQTJCK0QsSUFBSSxDQUFDQyxJQUFMLENBQVVpTCxRQUFWLElBQXNCLElBQWpELEdBQXdELEtBQTVEO0FBQ0FHLElBQUFBLENBQUMsR0FBRzNVLENBQUMsQ0FBQ3VGLFlBQUYsSUFBa0IsS0FBbEIsR0FBMEIrRCxJQUFJLENBQUNDLElBQUwsQ0FBVWlMLFFBQVYsSUFBc0IsSUFBaEQsR0FBdUQsS0FBM0Q7QUFFQUMsSUFBQUEsYUFBYSxDQUFDelUsQ0FBQyxDQUFDdUYsWUFBSCxDQUFiLEdBQWdDaVAsUUFBaEM7O0FBRUEsUUFBSXhVLENBQUMsQ0FBQzJFLGlCQUFGLEtBQXdCLEtBQTVCLEVBQW1DO0FBQy9CM0UsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQjBWLGFBQWxCO0FBQ0gsS0FGRCxNQUVPO0FBQ0hBLE1BQUFBLGFBQWEsR0FBRyxFQUFoQjs7QUFDQSxVQUFJelUsQ0FBQyxDQUFDa0YsY0FBRixLQUFxQixLQUF6QixFQUFnQztBQUM1QnVQLFFBQUFBLGFBQWEsQ0FBQ3pVLENBQUMsQ0FBQzhFLFFBQUgsQ0FBYixHQUE0QixlQUFlNFAsQ0FBZixHQUFtQixJQUFuQixHQUEwQkMsQ0FBMUIsR0FBOEIsR0FBMUQ7O0FBQ0EzVSxRQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCMFYsYUFBbEI7QUFDSCxPQUhELE1BR087QUFDSEEsUUFBQUEsYUFBYSxDQUFDelUsQ0FBQyxDQUFDOEUsUUFBSCxDQUFiLEdBQTRCLGlCQUFpQjRQLENBQWpCLEdBQXFCLElBQXJCLEdBQTRCQyxDQUE1QixHQUFnQyxRQUE1RDs7QUFDQTNVLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3JGLEdBQWQsQ0FBa0IwVixhQUFsQjtBQUNIO0FBQ0o7QUFFSixHQTNCRDs7QUE2QkE3VSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCd04sYUFBaEIsR0FBZ0MsWUFBVztBQUV2QyxRQUFJNVUsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVdUYsUUFBVixLQUF1QixLQUEzQixFQUFrQztBQUM5QixVQUFJaEQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUE3QixFQUFtQztBQUMvQlosUUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRMUYsR0FBUixDQUFZO0FBQ1I4VixVQUFBQSxPQUFPLEVBQUcsU0FBUzdVLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW9EO0FBRHJCLFNBQVo7QUFHSDtBQUNKLEtBTkQsTUFNTztBQUNIYixNQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVExRyxNQUFSLENBQWVpQyxDQUFDLENBQUNxRSxPQUFGLENBQVV5RyxLQUFWLEdBQWtCcEMsV0FBbEIsQ0FBOEIsSUFBOUIsSUFBc0MxSSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUEvRDs7QUFDQSxVQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUE3QixFQUFtQztBQUMvQlosUUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRMUYsR0FBUixDQUFZO0FBQ1I4VixVQUFBQSxPQUFPLEVBQUc3VSxDQUFDLENBQUN2QyxPQUFGLENBQVVvRCxhQUFWLEdBQTBCO0FBRDVCLFNBQVo7QUFHSDtBQUNKOztBQUVEYixJQUFBQSxDQUFDLENBQUM2RCxTQUFGLEdBQWM3RCxDQUFDLENBQUN5RSxLQUFGLENBQVEzRyxLQUFSLEVBQWQ7QUFDQWtDLElBQUFBLENBQUMsQ0FBQzhELFVBQUYsR0FBZTlELENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTFHLE1BQVIsRUFBZjs7QUFHQSxRQUFJaUMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVdUYsUUFBVixLQUF1QixLQUF2QixJQUFnQ2hELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXNGLGFBQVYsS0FBNEIsS0FBaEUsRUFBdUU7QUFDbkUvQyxNQUFBQSxDQUFDLENBQUNtRSxVQUFGLEdBQWVtRixJQUFJLENBQUNDLElBQUwsQ0FBVXZKLENBQUMsQ0FBQzZELFNBQUYsR0FBYzdELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQWxDLENBQWY7O0FBQ0F2QyxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWN0RyxLQUFkLENBQW9Cd0wsSUFBSSxDQUFDQyxJQUFMLENBQVd2SixDQUFDLENBQUNtRSxVQUFGLEdBQWVuRSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLGNBQXZCLEVBQXVDTixNQUFqRSxDQUFwQjtBQUVILEtBSkQsTUFJTyxJQUFJOUgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVc0YsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUN6Qy9DLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3RHLEtBQWQsQ0FBb0IsT0FBT2tDLENBQUMsQ0FBQ2tFLFVBQTdCO0FBQ0gsS0FGTSxNQUVBO0FBQ0hsRSxNQUFBQSxDQUFDLENBQUNtRSxVQUFGLEdBQWVtRixJQUFJLENBQUNDLElBQUwsQ0FBVXZKLENBQUMsQ0FBQzZELFNBQVosQ0FBZjs7QUFDQTdELE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3JHLE1BQWQsQ0FBcUJ1TCxJQUFJLENBQUNDLElBQUwsQ0FBV3ZKLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlHLEtBQVYsR0FBa0JwQyxXQUFsQixDQUE4QixJQUE5QixJQUFzQzFJLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsY0FBdkIsRUFBdUNOLE1BQXhGLENBQXJCO0FBQ0g7O0FBRUQsUUFBSWdOLE1BQU0sR0FBRzlVLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlHLEtBQVYsR0FBa0JnRixVQUFsQixDQUE2QixJQUE3QixJQUFxQzlQLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlHLEtBQVYsR0FBa0JoTixLQUFsQixFQUFsRDs7QUFDQSxRQUFJa0MsQ0FBQyxDQUFDdkMsT0FBRixDQUFVc0YsYUFBVixLQUE0QixLQUFoQyxFQUF1Qy9DLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsY0FBdkIsRUFBdUN0SyxLQUF2QyxDQUE2Q2tDLENBQUMsQ0FBQ21FLFVBQUYsR0FBZTJRLE1BQTVEO0FBRTFDLEdBckNEOztBQXVDQWxWLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IyTixPQUFoQixHQUEwQixZQUFXO0FBRWpDLFFBQUkvVSxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0k2SSxVQURKOztBQUdBN0ksSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUFVekYsSUFBVixDQUFlLFVBQVMrSSxLQUFULEVBQWdCNUgsT0FBaEIsRUFBeUI7QUFDcEM4SSxNQUFBQSxVQUFVLEdBQUk3SSxDQUFDLENBQUNtRSxVQUFGLEdBQWV3RCxLQUFoQixHQUF5QixDQUFDLENBQXZDOztBQUNBLFVBQUkzSCxDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLElBQXRCLEVBQTRCO0FBQ3hCOUUsUUFBQUEsQ0FBQyxDQUFDeUMsT0FBRCxDQUFELENBQVdoQixHQUFYLENBQWU7QUFDWHlWLFVBQUFBLFFBQVEsRUFBRSxVQURDO0FBRVhRLFVBQUFBLEtBQUssRUFBRW5NLFVBRkk7QUFHWEksVUFBQUEsR0FBRyxFQUFFLENBSE07QUFJWDlGLFVBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUIsQ0FKaEI7QUFLWHlMLFVBQUFBLE9BQU8sRUFBRTtBQUxFLFNBQWY7QUFPSCxPQVJELE1BUU87QUFDSHRSLFFBQUFBLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRCxDQUFXaEIsR0FBWCxDQUFlO0FBQ1h5VixVQUFBQSxRQUFRLEVBQUUsVUFEQztBQUVYeEwsVUFBQUEsSUFBSSxFQUFFSCxVQUZLO0FBR1hJLFVBQUFBLEdBQUcsRUFBRSxDQUhNO0FBSVg5RixVQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRixNQUFWLEdBQW1CLENBSmhCO0FBS1h5TCxVQUFBQSxPQUFPLEVBQUU7QUFMRSxTQUFmO0FBT0g7QUFDSixLQW5CRDs7QUFxQkE1TyxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWFqSSxDQUFDLENBQUMwRCxZQUFmLEVBQTZCM0UsR0FBN0IsQ0FBaUM7QUFDN0JvRSxNQUFBQSxNQUFNLEVBQUVuRCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRixNQUFWLEdBQW1CLENBREU7QUFFN0J5TCxNQUFBQSxPQUFPLEVBQUU7QUFGb0IsS0FBakM7QUFLSCxHQS9CRDs7QUFpQ0FoUCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNk4sU0FBaEIsR0FBNEIsWUFBVztBQUVuQyxRQUFJalYsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixLQUEyQixDQUEzQixJQUFnQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBDLGNBQVYsS0FBNkIsSUFBN0QsSUFBcUVILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBaEcsRUFBdUc7QUFDbkcsVUFBSXlGLFlBQVksR0FBR3pJLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYWpJLENBQUMsQ0FBQzBELFlBQWYsRUFBNkJnRixXQUE3QixDQUF5QyxJQUF6QyxDQUFuQjs7QUFDQTFJLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTFGLEdBQVIsQ0FBWSxRQUFaLEVBQXNCMEosWUFBdEI7QUFDSDtBQUVKLEdBVEQ7O0FBV0E3SSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCOE4sU0FBaEIsR0FDQXRWLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IrTixjQUFoQixHQUFpQyxZQUFXO0FBRXhDOzs7Ozs7Ozs7Ozs7QUFhQSxRQUFJblYsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUFjNFQsQ0FBZDtBQUFBLFFBQWlCd0IsSUFBakI7QUFBQSxRQUF1Qm5GLE1BQXZCO0FBQUEsUUFBK0JvRixLQUEvQjtBQUFBLFFBQXNDdkksT0FBTyxHQUFHLEtBQWhEO0FBQUEsUUFBdURnSCxJQUF2RDs7QUFFQSxRQUFJeFcsQ0FBQyxDQUFDd1csSUFBRixDQUFRd0IsU0FBUyxDQUFDLENBQUQsQ0FBakIsTUFBMkIsUUFBL0IsRUFBMEM7QUFFdENyRixNQUFBQSxNQUFNLEdBQUlxRixTQUFTLENBQUMsQ0FBRCxDQUFuQjtBQUNBeEksTUFBQUEsT0FBTyxHQUFHd0ksU0FBUyxDQUFDLENBQUQsQ0FBbkI7QUFDQXhCLE1BQUFBLElBQUksR0FBRyxVQUFQO0FBRUgsS0FORCxNQU1PLElBQUt4VyxDQUFDLENBQUN3VyxJQUFGLENBQVF3QixTQUFTLENBQUMsQ0FBRCxDQUFqQixNQUEyQixRQUFoQyxFQUEyQztBQUU5Q3JGLE1BQUFBLE1BQU0sR0FBSXFGLFNBQVMsQ0FBQyxDQUFELENBQW5CO0FBQ0FELE1BQUFBLEtBQUssR0FBR0MsU0FBUyxDQUFDLENBQUQsQ0FBakI7QUFDQXhJLE1BQUFBLE9BQU8sR0FBR3dJLFNBQVMsQ0FBQyxDQUFELENBQW5COztBQUVBLFVBQUtBLFNBQVMsQ0FBQyxDQUFELENBQVQsS0FBaUIsWUFBakIsSUFBaUNoWSxDQUFDLENBQUN3VyxJQUFGLENBQVF3QixTQUFTLENBQUMsQ0FBRCxDQUFqQixNQUEyQixPQUFqRSxFQUEyRTtBQUV2RXhCLFFBQUFBLElBQUksR0FBRyxZQUFQO0FBRUgsT0FKRCxNQUlPLElBQUssT0FBT3dCLFNBQVMsQ0FBQyxDQUFELENBQWhCLEtBQXdCLFdBQTdCLEVBQTJDO0FBRTlDeEIsUUFBQUEsSUFBSSxHQUFHLFFBQVA7QUFFSDtBQUVKOztBQUVELFFBQUtBLElBQUksS0FBSyxRQUFkLEVBQXlCO0FBRXJCOVQsTUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVd1MsTUFBVixJQUFvQm9GLEtBQXBCO0FBR0gsS0FMRCxNQUtPLElBQUt2QixJQUFJLEtBQUssVUFBZCxFQUEyQjtBQUU5QnhXLE1BQUFBLENBQUMsQ0FBQ3NCLElBQUYsQ0FBUXFSLE1BQVIsRUFBaUIsVUFBVXNGLEdBQVYsRUFBZUMsR0FBZixFQUFxQjtBQUVsQ3hWLFFBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThYLEdBQVYsSUFBaUJDLEdBQWpCO0FBRUgsT0FKRDtBQU9ILEtBVE0sTUFTQSxJQUFLMUIsSUFBSSxLQUFLLFlBQWQsRUFBNkI7QUFFaEMsV0FBTXNCLElBQU4sSUFBY0MsS0FBZCxFQUFzQjtBQUVsQixZQUFJL1gsQ0FBQyxDQUFDd1csSUFBRixDQUFROVQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUUsVUFBbEIsTUFBbUMsT0FBdkMsRUFBaUQ7QUFFN0NsQyxVQUFBQSxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLEdBQXVCLENBQUVtVCxLQUFLLENBQUNELElBQUQsQ0FBUCxDQUF2QjtBQUVILFNBSkQsTUFJTztBQUVIeEIsVUFBQUEsQ0FBQyxHQUFHNVQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUUsVUFBVixDQUFxQjRGLE1BQXJCLEdBQTRCLENBQWhDLENBRkcsQ0FJSDs7QUFDQSxpQkFBTzhMLENBQUMsSUFBSSxDQUFaLEVBQWdCO0FBRVosZ0JBQUk1VCxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLENBQXFCMFIsQ0FBckIsRUFBd0J2SCxVQUF4QixLQUF1Q2dKLEtBQUssQ0FBQ0QsSUFBRCxDQUFMLENBQVkvSSxVQUF2RCxFQUFvRTtBQUVoRXJNLGNBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsQ0FBcUI2UixNQUFyQixDQUE0QkgsQ0FBNUIsRUFBOEIsQ0FBOUI7QUFFSDs7QUFFREEsWUFBQUEsQ0FBQztBQUVKOztBQUVENVQsVUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUUsVUFBVixDQUFxQmtPLElBQXJCLENBQTJCaUYsS0FBSyxDQUFDRCxJQUFELENBQWhDO0FBRUg7QUFFSjtBQUVKOztBQUVELFFBQUt0SSxPQUFMLEVBQWU7QUFFWDlNLE1BQUFBLENBQUMsQ0FBQzZILE1BQUY7O0FBQ0E3SCxNQUFBQSxDQUFDLENBQUN1SSxNQUFGO0FBRUg7QUFFSixHQWhHRDs7QUFrR0EzSSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCUCxXQUFoQixHQUE4QixZQUFXO0FBRXJDLFFBQUk3RyxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDNFUsYUFBRjs7QUFFQTVVLElBQUFBLENBQUMsQ0FBQ2lWLFNBQUY7O0FBRUEsUUFBSWpWLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJ4QixNQUFBQSxDQUFDLENBQUN1VSxNQUFGLENBQVN2VSxDQUFDLENBQUN3UCxPQUFGLENBQVV4UCxDQUFDLENBQUMwRCxZQUFaLENBQVQ7QUFDSCxLQUZELE1BRU87QUFDSDFELE1BQUFBLENBQUMsQ0FBQytVLE9BQUY7QUFDSDs7QUFFRC9VLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsYUFBbEIsRUFBaUMsQ0FBQy9NLENBQUQsQ0FBakM7QUFFSCxHQWhCRDs7QUFrQkFKLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I0SixRQUFoQixHQUEyQixZQUFXO0FBRWxDLFFBQUloUixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l5VixTQUFTLEdBQUd0UCxRQUFRLENBQUN1UCxJQUFULENBQWNDLEtBRDlCOztBQUdBM1YsSUFBQUEsQ0FBQyxDQUFDdUYsWUFBRixHQUFpQnZGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsSUFBdkIsR0FBOEIsS0FBOUIsR0FBc0MsTUFBdkQ7O0FBRUEsUUFBSWhELENBQUMsQ0FBQ3VGLFlBQUYsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJ2RixNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVoSCxRQUFWLENBQW1CLGdCQUFuQjtBQUNILEtBRkQsTUFFTztBQUNIc0IsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVNkUsV0FBVixDQUFzQixnQkFBdEI7QUFDSDs7QUFFRCxRQUFJa0wsU0FBUyxDQUFDRyxnQkFBVixLQUErQkMsU0FBL0IsSUFDQUosU0FBUyxDQUFDSyxhQUFWLEtBQTRCRCxTQUQ1QixJQUVBSixTQUFTLENBQUNNLFlBQVYsS0FBMkJGLFNBRi9CLEVBRTBDO0FBQ3RDLFVBQUk3VixDQUFDLENBQUN2QyxPQUFGLENBQVVvRixNQUFWLEtBQXFCLElBQXpCLEVBQStCO0FBQzNCN0MsUUFBQUEsQ0FBQyxDQUFDa0YsY0FBRixHQUFtQixJQUFuQjtBQUNIO0FBQ0o7O0FBRUQsUUFBS2xGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQWYsRUFBc0I7QUFDbEIsVUFBSyxPQUFPeEIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEYsTUFBakIsS0FBNEIsUUFBakMsRUFBNEM7QUFDeEMsWUFBSW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUIsQ0FBdkIsRUFBMkI7QUFDdkJuRCxVQUFBQSxDQUFDLENBQUN2QyxPQUFGLENBQVUwRixNQUFWLEdBQW1CLENBQW5CO0FBQ0g7QUFDSixPQUpELE1BSU87QUFDSG5ELFFBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUJuRCxDQUFDLENBQUN0QyxRQUFGLENBQVd5RixNQUE5QjtBQUNIO0FBQ0o7O0FBRUQsUUFBSXNTLFNBQVMsQ0FBQ08sVUFBVixLQUF5QkgsU0FBN0IsRUFBd0M7QUFDcEM3VixNQUFBQSxDQUFDLENBQUM4RSxRQUFGLEdBQWEsWUFBYjtBQUNBOUUsTUFBQUEsQ0FBQyxDQUFDNEYsYUFBRixHQUFrQixjQUFsQjtBQUNBNUYsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRixHQUFtQixhQUFuQjtBQUNBLFVBQUk0UCxTQUFTLENBQUNRLG1CQUFWLEtBQWtDSixTQUFsQyxJQUErQ0osU0FBUyxDQUFDUyxpQkFBVixLQUFnQ0wsU0FBbkYsRUFBOEY3VixDQUFDLENBQUM4RSxRQUFGLEdBQWEsS0FBYjtBQUNqRzs7QUFDRCxRQUFJMlEsU0FBUyxDQUFDVSxZQUFWLEtBQTJCTixTQUEvQixFQUEwQztBQUN0QzdWLE1BQUFBLENBQUMsQ0FBQzhFLFFBQUYsR0FBYSxjQUFiO0FBQ0E5RSxNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLGdCQUFsQjtBQUNBNUYsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRixHQUFtQixlQUFuQjtBQUNBLFVBQUk0UCxTQUFTLENBQUNRLG1CQUFWLEtBQWtDSixTQUFsQyxJQUErQ0osU0FBUyxDQUFDVyxjQUFWLEtBQTZCUCxTQUFoRixFQUEyRjdWLENBQUMsQ0FBQzhFLFFBQUYsR0FBYSxLQUFiO0FBQzlGOztBQUNELFFBQUkyUSxTQUFTLENBQUNZLGVBQVYsS0FBOEJSLFNBQWxDLEVBQTZDO0FBQ3pDN1YsTUFBQUEsQ0FBQyxDQUFDOEUsUUFBRixHQUFhLGlCQUFiO0FBQ0E5RSxNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLG1CQUFsQjtBQUNBNUYsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRixHQUFtQixrQkFBbkI7QUFDQSxVQUFJNFAsU0FBUyxDQUFDUSxtQkFBVixLQUFrQ0osU0FBbEMsSUFBK0NKLFNBQVMsQ0FBQ1MsaUJBQVYsS0FBZ0NMLFNBQW5GLEVBQThGN1YsQ0FBQyxDQUFDOEUsUUFBRixHQUFhLEtBQWI7QUFDakc7O0FBQ0QsUUFBSTJRLFNBQVMsQ0FBQ2EsV0FBVixLQUEwQlQsU0FBOUIsRUFBeUM7QUFDckM3VixNQUFBQSxDQUFDLENBQUM4RSxRQUFGLEdBQWEsYUFBYjtBQUNBOUUsTUFBQUEsQ0FBQyxDQUFDNEYsYUFBRixHQUFrQixlQUFsQjtBQUNBNUYsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRixHQUFtQixjQUFuQjtBQUNBLFVBQUk0UCxTQUFTLENBQUNhLFdBQVYsS0FBMEJULFNBQTlCLEVBQXlDN1YsQ0FBQyxDQUFDOEUsUUFBRixHQUFhLEtBQWI7QUFDNUM7O0FBQ0QsUUFBSTJRLFNBQVMsQ0FBQ2MsU0FBVixLQUF3QlYsU0FBeEIsSUFBcUM3VixDQUFDLENBQUM4RSxRQUFGLEtBQWUsS0FBeEQsRUFBK0Q7QUFDM0Q5RSxNQUFBQSxDQUFDLENBQUM4RSxRQUFGLEdBQWEsV0FBYjtBQUNBOUUsTUFBQUEsQ0FBQyxDQUFDNEYsYUFBRixHQUFrQixXQUFsQjtBQUNBNUYsTUFBQUEsQ0FBQyxDQUFDNkYsY0FBRixHQUFtQixZQUFuQjtBQUNIOztBQUNEN0YsSUFBQUEsQ0FBQyxDQUFDMkUsaUJBQUYsR0FBc0IzRSxDQUFDLENBQUN2QyxPQUFGLENBQVVxRixZQUFWLElBQTJCOUMsQ0FBQyxDQUFDOEUsUUFBRixLQUFlLElBQWYsSUFBdUI5RSxDQUFDLENBQUM4RSxRQUFGLEtBQWUsS0FBdkY7QUFDSCxHQTdERDs7QUFnRUFsRixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCK0QsZUFBaEIsR0FBa0MsVUFBU3hELEtBQVQsRUFBZ0I7QUFFOUMsUUFBSTNILENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXlRLFlBREo7QUFBQSxRQUNrQitGLFNBRGxCO0FBQUEsUUFDNkJwSixXQUQ3QjtBQUFBLFFBQzBDcUosU0FEMUM7O0FBR0FELElBQUFBLFNBQVMsR0FBR3hXLENBQUMsQ0FBQzBGLE9BQUYsQ0FDUDRCLElBRE8sQ0FDRixjQURFLEVBRVBpRCxXQUZPLENBRUsseUNBRkwsRUFHUGhELElBSE8sQ0FHRixhQUhFLEVBR2EsTUFIYixDQUFaOztBQUtBdkgsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUNLNEQsRUFETCxDQUNRTixLQURSLEVBRUtqSixRQUZMLENBRWMsZUFGZDs7QUFJQSxRQUFJc0IsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUE3QixFQUFtQztBQUUvQjZQLE1BQUFBLFlBQVksR0FBR25ILElBQUksQ0FBQ3NHLEtBQUwsQ0FBVzVQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBcEMsQ0FBZjs7QUFFQSxVQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUEzQixFQUFpQztBQUU3QixZQUFJaUcsS0FBSyxJQUFJOEksWUFBVCxJQUF5QjlJLEtBQUssSUFBSzNILENBQUMsQ0FBQ2tFLFVBQUYsR0FBZSxDQUFoQixHQUFxQnVNLFlBQTNELEVBQXlFO0FBRXJFelEsVUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUNLdU8sS0FETCxDQUNXakwsS0FBSyxHQUFHOEksWUFEbkIsRUFDaUM5SSxLQUFLLEdBQUc4SSxZQUFSLEdBQXVCLENBRHhELEVBRUsvUixRQUZMLENBRWMsY0FGZCxFQUdLNkksSUFITCxDQUdVLGFBSFYsRUFHeUIsT0FIekI7QUFLSCxTQVBELE1BT087QUFFSDZGLFVBQUFBLFdBQVcsR0FBR3BOLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUJvRixLQUF2QztBQUNBNk8sVUFBQUEsU0FBUyxDQUNKNUQsS0FETCxDQUNXeEYsV0FBVyxHQUFHcUQsWUFBZCxHQUE2QixDQUR4QyxFQUMyQ3JELFdBQVcsR0FBR3FELFlBQWQsR0FBNkIsQ0FEeEUsRUFFSy9SLFFBRkwsQ0FFYyxjQUZkLEVBR0s2SSxJQUhMLENBR1UsYUFIVixFQUd5QixPQUh6QjtBQUtIOztBQUVELFlBQUlJLEtBQUssS0FBSyxDQUFkLEVBQWlCO0FBRWI2TyxVQUFBQSxTQUFTLENBQ0p2TyxFQURMLENBQ1F1TyxTQUFTLENBQUMxTyxNQUFWLEdBQW1CLENBQW5CLEdBQXVCOUgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFEekMsRUFFSzdELFFBRkwsQ0FFYyxjQUZkO0FBSUgsU0FORCxNQU1PLElBQUlpSixLQUFLLEtBQUszSCxDQUFDLENBQUNrRSxVQUFGLEdBQWUsQ0FBN0IsRUFBZ0M7QUFFbkNzUyxVQUFBQSxTQUFTLENBQ0p2TyxFQURMLENBQ1FqSSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQURsQixFQUVLN0QsUUFGTCxDQUVjLGNBRmQ7QUFJSDtBQUVKOztBQUVEc0IsTUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUNLNEQsRUFETCxDQUNRTixLQURSLEVBRUtqSixRQUZMLENBRWMsY0FGZDtBQUlILEtBM0NELE1BMkNPO0FBRUgsVUFBSWlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSzNILENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXJELEVBQW9FO0FBRWhFdkMsUUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUNLdU8sS0FETCxDQUNXakwsS0FEWCxFQUNrQkEsS0FBSyxHQUFHM0gsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFEcEMsRUFFSzdELFFBRkwsQ0FFYyxjQUZkLEVBR0s2SSxJQUhMLENBR1UsYUFIVixFQUd5QixPQUh6QjtBQUtILE9BUEQsTUFPTyxJQUFJaVAsU0FBUyxDQUFDMU8sTUFBVixJQUFvQjlILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQWxDLEVBQWdEO0FBRW5EaVUsUUFBQUEsU0FBUyxDQUNKOVgsUUFETCxDQUNjLGNBRGQsRUFFSzZJLElBRkwsQ0FFVSxhQUZWLEVBRXlCLE9BRnpCO0FBSUgsT0FOTSxNQU1BO0FBRUhrUCxRQUFBQSxTQUFTLEdBQUd6VyxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFyQztBQUNBNkssUUFBQUEsV0FBVyxHQUFHcE4sQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUF2QixHQUE4QjFCLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUJvRixLQUF2RCxHQUErREEsS0FBN0U7O0FBRUEsWUFBSTNILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsSUFBMEJ2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFwQyxJQUF1RHhDLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZXlELEtBQWhCLEdBQXlCM0gsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBN0YsRUFBMkc7QUFFdkdpVSxVQUFBQSxTQUFTLENBQ0o1RCxLQURMLENBQ1d4RixXQUFXLElBQUlwTixDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCa1UsU0FBN0IsQ0FEdEIsRUFDK0RySixXQUFXLEdBQUdxSixTQUQ3RSxFQUVLL1gsUUFGTCxDQUVjLGNBRmQsRUFHSzZJLElBSEwsQ0FHVSxhQUhWLEVBR3lCLE9BSHpCO0FBS0gsU0FQRCxNQU9PO0FBRUhpUCxVQUFBQSxTQUFTLENBQ0o1RCxLQURMLENBQ1d4RixXQURYLEVBQ3dCQSxXQUFXLEdBQUdwTixDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQURoRCxFQUVLN0QsUUFGTCxDQUVjLGNBRmQsRUFHSzZJLElBSEwsQ0FHVSxhQUhWLEVBR3lCLE9BSHpCO0FBS0g7QUFFSjtBQUVKOztBQUVELFFBQUl2SCxDQUFDLENBQUN2QyxPQUFGLENBQVVtRSxRQUFWLEtBQXVCLFVBQTNCLEVBQXVDO0FBQ25DNUIsTUFBQUEsQ0FBQyxDQUFDNEIsUUFBRjtBQUNIO0FBRUosR0FyR0Q7O0FBdUdBaEMsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjZELGFBQWhCLEdBQWdDLFlBQVc7QUFFdkMsUUFBSWpMLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWlCLENBREo7QUFBQSxRQUNPME4sVUFEUDtBQUFBLFFBQ21CK0gsYUFEbkI7O0FBR0EsUUFBSTFXLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsSUFBdkIsRUFBNkI7QUFDekJ4QixNQUFBQSxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEdBQXVCLEtBQXZCO0FBQ0g7O0FBRUQsUUFBSVosQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUF2QixJQUErQjFCLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsS0FBdEQsRUFBNkQ7QUFFekRtTixNQUFBQSxVQUFVLEdBQUcsSUFBYjs7QUFFQSxVQUFJM08sQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBN0IsRUFBMkM7QUFFdkMsWUFBSXZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBN0IsRUFBbUM7QUFDL0I4VixVQUFBQSxhQUFhLEdBQUcxVyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCLENBQXpDO0FBQ0gsU0FGRCxNQUVPO0FBQ0htVSxVQUFBQSxhQUFhLEdBQUcxVyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExQjtBQUNIOztBQUVELGFBQUt0QixDQUFDLEdBQUdqQixDQUFDLENBQUNrRSxVQUFYLEVBQXVCakQsQ0FBQyxHQUFJakIsQ0FBQyxDQUFDa0UsVUFBRixHQUNwQndTLGFBRFIsRUFDd0J6VixDQUFDLElBQUksQ0FEN0IsRUFDZ0M7QUFDNUIwTixVQUFBQSxVQUFVLEdBQUcxTixDQUFDLEdBQUcsQ0FBakI7QUFDQTNELFVBQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXNLLFVBQVYsQ0FBRCxDQUFELENBQXlCZ0ksS0FBekIsQ0FBK0IsSUFBL0IsRUFBcUNwUCxJQUFyQyxDQUEwQyxJQUExQyxFQUFnRCxFQUFoRCxFQUNLQSxJQURMLENBQ1Usa0JBRFYsRUFDOEJvSCxVQUFVLEdBQUczTyxDQUFDLENBQUNrRSxVQUQ3QyxFQUVLaUUsU0FGTCxDQUVlbkksQ0FBQyxDQUFDb0UsV0FGakIsRUFFOEIxRixRQUY5QixDQUV1QyxjQUZ2QztBQUdIOztBQUNELGFBQUt1QyxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUd5VixhQUFoQixFQUErQnpWLENBQUMsSUFBSSxDQUFwQyxFQUF1QztBQUNuQzBOLFVBQUFBLFVBQVUsR0FBRzFOLENBQWI7QUFDQTNELFVBQUFBLENBQUMsQ0FBQzBDLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXNLLFVBQVYsQ0FBRCxDQUFELENBQXlCZ0ksS0FBekIsQ0FBK0IsSUFBL0IsRUFBcUNwUCxJQUFyQyxDQUEwQyxJQUExQyxFQUFnRCxFQUFoRCxFQUNLQSxJQURMLENBQ1Usa0JBRFYsRUFDOEJvSCxVQUFVLEdBQUczTyxDQUFDLENBQUNrRSxVQUQ3QyxFQUVLNkQsUUFGTCxDQUVjL0gsQ0FBQyxDQUFDb0UsV0FGaEIsRUFFNkIxRixRQUY3QixDQUVzQyxjQUZ0QztBQUdIOztBQUNEc0IsUUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFja0QsSUFBZCxDQUFtQixlQUFuQixFQUFvQ0EsSUFBcEMsQ0FBeUMsTUFBekMsRUFBaUQxSSxJQUFqRCxDQUFzRCxZQUFXO0FBQzdEdEIsVUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUssSUFBUixDQUFhLElBQWIsRUFBbUIsRUFBbkI7QUFDSCxTQUZEO0FBSUg7QUFFSjtBQUVKLEdBMUNEOztBQTRDQTNILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I0RyxTQUFoQixHQUE0QixVQUFVNEksTUFBVixFQUFtQjtBQUUzQyxRQUFJNVcsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxDQUFDNFcsTUFBTCxFQUFjO0FBQ1Y1VyxNQUFBQSxDQUFDLENBQUNzRyxRQUFGO0FBQ0g7O0FBQ0R0RyxJQUFBQSxDQUFDLENBQUNvRixXQUFGLEdBQWdCd1IsTUFBaEI7QUFFSCxHQVREOztBQVdBaFgsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQlIsYUFBaEIsR0FBZ0MsVUFBU29HLEtBQVQsRUFBZ0I7QUFFNUMsUUFBSWhOLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUk2VyxhQUFhLEdBQ2J2WixDQUFDLENBQUMwUCxLQUFLLENBQUNqRCxNQUFQLENBQUQsQ0FBZ0J1RCxFQUFoQixDQUFtQixjQUFuQixJQUNJaFEsQ0FBQyxDQUFDMFAsS0FBSyxDQUFDakQsTUFBUCxDQURMLEdBRUl6TSxDQUFDLENBQUMwUCxLQUFLLENBQUNqRCxNQUFQLENBQUQsQ0FBZ0IrTSxPQUFoQixDQUF3QixjQUF4QixDQUhSO0FBS0EsUUFBSW5QLEtBQUssR0FBR2tKLFFBQVEsQ0FBQ2dHLGFBQWEsQ0FBQ3RQLElBQWQsQ0FBbUIsa0JBQW5CLENBQUQsQ0FBcEI7QUFFQSxRQUFJLENBQUNJLEtBQUwsRUFBWUEsS0FBSyxHQUFHLENBQVI7O0FBRVosUUFBSTNILENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE5QixFQUE0QztBQUV4Q3ZDLE1BQUFBLENBQUMsQ0FBQ21MLGVBQUYsQ0FBa0J4RCxLQUFsQjs7QUFDQTNILE1BQUFBLENBQUMsQ0FBQ08sUUFBRixDQUFXb0gsS0FBWDs7QUFDQTtBQUVIOztBQUVEM0gsSUFBQUEsQ0FBQyxDQUFDaUssWUFBRixDQUFldEMsS0FBZjtBQUVILEdBdkJEOztBQXlCQS9ILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I2QyxZQUFoQixHQUErQixVQUFTdEMsS0FBVCxFQUFnQm9QLElBQWhCLEVBQXNCOUosV0FBdEIsRUFBbUM7QUFFOUQsUUFBSTBDLFdBQUo7QUFBQSxRQUFpQnFILFNBQWpCO0FBQUEsUUFBNEJDLFFBQTVCO0FBQUEsUUFBc0NDLFNBQXRDO0FBQUEsUUFBaURyTyxVQUFVLEdBQUcsSUFBOUQ7QUFBQSxRQUNJN0ksQ0FBQyxHQUFHLElBRFI7QUFBQSxRQUNjbVgsU0FEZDs7QUFHQUosSUFBQUEsSUFBSSxHQUFHQSxJQUFJLElBQUksS0FBZjs7QUFFQSxRQUFJL1csQ0FBQyxDQUFDcUQsU0FBRixLQUFnQixJQUFoQixJQUF3QnJELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlGLGNBQVYsS0FBNkIsSUFBekQsRUFBK0Q7QUFDM0Q7QUFDSDs7QUFFRCxRQUFJbEQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixJQUFuQixJQUEyQnhCLENBQUMsQ0FBQzBELFlBQUYsS0FBbUJpRSxLQUFsRCxFQUF5RDtBQUNyRDtBQUNIOztBQUVELFFBQUkzSCxDQUFDLENBQUNrRSxVQUFGLElBQWdCbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBOUIsRUFBNEM7QUFDeEM7QUFDSDs7QUFFRCxRQUFJd1UsSUFBSSxLQUFLLEtBQWIsRUFBb0I7QUFDaEIvVyxNQUFBQSxDQUFDLENBQUNPLFFBQUYsQ0FBV29ILEtBQVg7QUFDSDs7QUFFRGdJLElBQUFBLFdBQVcsR0FBR2hJLEtBQWQ7QUFDQWtCLElBQUFBLFVBQVUsR0FBRzdJLENBQUMsQ0FBQ3dQLE9BQUYsQ0FBVUcsV0FBVixDQUFiO0FBQ0F1SCxJQUFBQSxTQUFTLEdBQUdsWCxDQUFDLENBQUN3UCxPQUFGLENBQVV4UCxDQUFDLENBQUMwRCxZQUFaLENBQVo7QUFFQTFELElBQUFBLENBQUMsQ0FBQ3lELFdBQUYsR0FBZ0J6RCxDQUFDLENBQUN3RSxTQUFGLEtBQWdCLElBQWhCLEdBQXVCMFMsU0FBdkIsR0FBbUNsWCxDQUFDLENBQUN3RSxTQUFyRDs7QUFFQSxRQUFJeEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixLQUF2QixJQUFnQzFCLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsS0FBekQsS0FBbUUrRyxLQUFLLEdBQUcsQ0FBUixJQUFhQSxLQUFLLEdBQUczSCxDQUFDLENBQUM2SyxXQUFGLEtBQWtCN0ssQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBcEgsQ0FBSixFQUF5STtBQUNySSxVQUFJeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixLQUF2QixFQUE4QjtBQUMxQm1PLFFBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQzBELFlBQWhCOztBQUNBLFlBQUl1SixXQUFXLEtBQUssSUFBcEIsRUFBMEI7QUFDdEJqTixVQUFBQSxDQUFDLENBQUM0SSxZQUFGLENBQWVzTyxTQUFmLEVBQTBCLFlBQVc7QUFDakNsWCxZQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVl6RCxXQUFaO0FBQ0gsV0FGRDtBQUdILFNBSkQsTUFJTztBQUNIM1AsVUFBQUEsQ0FBQyxDQUFDb1QsU0FBRixDQUFZekQsV0FBWjtBQUNIO0FBQ0o7O0FBQ0Q7QUFDSCxLQVpELE1BWU8sSUFBSTNQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsS0FBdkIsSUFBZ0MxQixDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQXpELEtBQWtFK0csS0FBSyxHQUFHLENBQVIsSUFBYUEsS0FBSyxHQUFJM0gsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBakgsQ0FBSixFQUF1STtBQUMxSSxVQUFJeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixLQUF2QixFQUE4QjtBQUMxQm1PLFFBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQzBELFlBQWhCOztBQUNBLFlBQUl1SixXQUFXLEtBQUssSUFBcEIsRUFBMEI7QUFDdEJqTixVQUFBQSxDQUFDLENBQUM0SSxZQUFGLENBQWVzTyxTQUFmLEVBQTBCLFlBQVc7QUFDakNsWCxZQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVl6RCxXQUFaO0FBQ0gsV0FGRDtBQUdILFNBSkQsTUFJTztBQUNIM1AsVUFBQUEsQ0FBQyxDQUFDb1QsU0FBRixDQUFZekQsV0FBWjtBQUNIO0FBQ0o7O0FBQ0Q7QUFDSDs7QUFFRCxRQUFLM1AsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUQsUUFBZixFQUEwQjtBQUN0QjBKLE1BQUFBLGFBQWEsQ0FBQ3BLLENBQUMsQ0FBQ3VELGFBQUgsQ0FBYjtBQUNIOztBQUVELFFBQUlvTSxXQUFXLEdBQUcsQ0FBbEIsRUFBcUI7QUFDakIsVUFBSTNQLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXpCLEtBQTRDLENBQWhELEVBQW1EO0FBQy9Dd1UsUUFBQUEsU0FBUyxHQUFHaFgsQ0FBQyxDQUFDa0UsVUFBRixHQUFnQmxFLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXJEO0FBQ0gsT0FGRCxNQUVPO0FBQ0h3VSxRQUFBQSxTQUFTLEdBQUdoWCxDQUFDLENBQUNrRSxVQUFGLEdBQWV5TCxXQUEzQjtBQUNIO0FBQ0osS0FORCxNQU1PLElBQUlBLFdBQVcsSUFBSTNQLENBQUMsQ0FBQ2tFLFVBQXJCLEVBQWlDO0FBQ3BDLFVBQUlsRSxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUF6QixLQUE0QyxDQUFoRCxFQUFtRDtBQUMvQ3dVLFFBQUFBLFNBQVMsR0FBRyxDQUFaO0FBQ0gsT0FGRCxNQUVPO0FBQ0hBLFFBQUFBLFNBQVMsR0FBR3JILFdBQVcsR0FBRzNQLENBQUMsQ0FBQ2tFLFVBQTVCO0FBQ0g7QUFDSixLQU5NLE1BTUE7QUFDSDhTLE1BQUFBLFNBQVMsR0FBR3JILFdBQVo7QUFDSDs7QUFFRDNQLElBQUFBLENBQUMsQ0FBQ3FELFNBQUYsR0FBYyxJQUFkOztBQUVBckQsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixjQUFsQixFQUFrQyxDQUFDL00sQ0FBRCxFQUFJQSxDQUFDLENBQUMwRCxZQUFOLEVBQW9Cc1QsU0FBcEIsQ0FBbEM7O0FBRUFDLElBQUFBLFFBQVEsR0FBR2pYLENBQUMsQ0FBQzBELFlBQWI7QUFDQTFELElBQUFBLENBQUMsQ0FBQzBELFlBQUYsR0FBaUJzVCxTQUFqQjs7QUFFQWhYLElBQUFBLENBQUMsQ0FBQ21MLGVBQUYsQ0FBa0JuTCxDQUFDLENBQUMwRCxZQUFwQjs7QUFFQSxRQUFLMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEMsUUFBZixFQUEwQjtBQUV0QjRXLE1BQUFBLFNBQVMsR0FBR25YLENBQUMsQ0FBQzZKLFlBQUYsRUFBWjtBQUNBc04sTUFBQUEsU0FBUyxHQUFHQSxTQUFTLENBQUNuTixLQUFWLENBQWdCLFVBQWhCLENBQVo7O0FBRUEsVUFBS21OLFNBQVMsQ0FBQ2pULFVBQVYsSUFBd0JpVCxTQUFTLENBQUMxWixPQUFWLENBQWtCOEUsWUFBL0MsRUFBOEQ7QUFDMUQ0VSxRQUFBQSxTQUFTLENBQUNoTSxlQUFWLENBQTBCbkwsQ0FBQyxDQUFDMEQsWUFBNUI7QUFDSDtBQUVKOztBQUVEMUQsSUFBQUEsQ0FBQyxDQUFDa0wsVUFBRjs7QUFDQWxMLElBQUFBLENBQUMsQ0FBQ29SLFlBQUY7O0FBRUEsUUFBSXBSLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsSUFBdkIsRUFBNkI7QUFDekIsVUFBSXlMLFdBQVcsS0FBSyxJQUFwQixFQUEwQjtBQUV0QmpOLFFBQUFBLENBQUMsQ0FBQzZPLFlBQUYsQ0FBZW9JLFFBQWY7O0FBRUFqWCxRQUFBQSxDQUFDLENBQUMwTyxTQUFGLENBQVlzSSxTQUFaLEVBQXVCLFlBQVc7QUFDOUJoWCxVQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVk0RCxTQUFaO0FBQ0gsU0FGRDtBQUlILE9BUkQsTUFRTztBQUNIaFgsUUFBQUEsQ0FBQyxDQUFDb1QsU0FBRixDQUFZNEQsU0FBWjtBQUNIOztBQUNEaFgsTUFBQUEsQ0FBQyxDQUFDd0ksYUFBRjs7QUFDQTtBQUNIOztBQUVELFFBQUl5RSxXQUFXLEtBQUssSUFBcEIsRUFBMEI7QUFDdEJqTixNQUFBQSxDQUFDLENBQUM0SSxZQUFGLENBQWVDLFVBQWYsRUFBMkIsWUFBVztBQUNsQzdJLFFBQUFBLENBQUMsQ0FBQ29ULFNBQUYsQ0FBWTRELFNBQVo7QUFDSCxPQUZEO0FBR0gsS0FKRCxNQUlPO0FBQ0hoWCxNQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVk0RCxTQUFaO0FBQ0g7QUFFSixHQTFIRDs7QUE0SEFwWCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNkosU0FBaEIsR0FBNEIsWUFBVztBQUVuQyxRQUFJalIsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkMsTUFBVixLQUFxQixJQUFyQixJQUE2Qk4sQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBMUQsRUFBd0U7QUFFcEV2QyxNQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWFtVCxJQUFiOztBQUNBcFgsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhb1QsSUFBYjtBQUVIOztBQUVELFFBQUlwWCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRCxJQUFWLEtBQW1CLElBQW5CLElBQTJCbkIsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBeEQsRUFBc0U7QUFFbEV2QyxNQUFBQSxDQUFDLENBQUM0RCxLQUFGLENBQVF3VCxJQUFSO0FBRUg7O0FBRURwWCxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVoSCxRQUFWLENBQW1CLGVBQW5CO0FBRUgsR0FuQkQ7O0FBcUJBa0IsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmlRLGNBQWhCLEdBQWlDLFlBQVc7QUFFeEMsUUFBSUMsS0FBSjtBQUFBLFFBQVdDLEtBQVg7QUFBQSxRQUFrQkMsQ0FBbEI7QUFBQSxRQUFxQkMsVUFBckI7QUFBQSxRQUFpQ3pYLENBQUMsR0FBRyxJQUFyQzs7QUFFQXNYLElBQUFBLEtBQUssR0FBR3RYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2dULE1BQWQsR0FBdUIxWCxDQUFDLENBQUMwRSxXQUFGLENBQWNpVCxJQUE3QztBQUNBSixJQUFBQSxLQUFLLEdBQUd2WCxDQUFDLENBQUMwRSxXQUFGLENBQWNrVCxNQUFkLEdBQXVCNVgsQ0FBQyxDQUFDMEUsV0FBRixDQUFjbVQsSUFBN0M7QUFDQUwsSUFBQUEsQ0FBQyxHQUFHbE8sSUFBSSxDQUFDd08sS0FBTCxDQUFXUCxLQUFYLEVBQWtCRCxLQUFsQixDQUFKO0FBRUFHLElBQUFBLFVBQVUsR0FBR25PLElBQUksQ0FBQ3lPLEtBQUwsQ0FBV1AsQ0FBQyxHQUFHLEdBQUosR0FBVWxPLElBQUksQ0FBQzBPLEVBQTFCLENBQWI7O0FBQ0EsUUFBSVAsVUFBVSxHQUFHLENBQWpCLEVBQW9CO0FBQ2hCQSxNQUFBQSxVQUFVLEdBQUcsTUFBTW5PLElBQUksQ0FBQ29ILEdBQUwsQ0FBUytHLFVBQVQsQ0FBbkI7QUFDSDs7QUFFRCxRQUFLQSxVQUFVLElBQUksRUFBZixJQUF1QkEsVUFBVSxJQUFJLENBQXpDLEVBQTZDO0FBQ3pDLGFBQVF6WCxDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLEtBQWxCLEdBQTBCLE1BQTFCLEdBQW1DLE9BQTNDO0FBQ0g7O0FBQ0QsUUFBS3FWLFVBQVUsSUFBSSxHQUFmLElBQXdCQSxVQUFVLElBQUksR0FBMUMsRUFBZ0Q7QUFDNUMsYUFBUXpYLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsS0FBbEIsR0FBMEIsTUFBMUIsR0FBbUMsT0FBM0M7QUFDSDs7QUFDRCxRQUFLcVYsVUFBVSxJQUFJLEdBQWYsSUFBd0JBLFVBQVUsSUFBSSxHQUExQyxFQUFnRDtBQUM1QyxhQUFRelgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkUsR0FBVixLQUFrQixLQUFsQixHQUEwQixPQUExQixHQUFvQyxNQUE1QztBQUNIOztBQUNELFFBQUlwQyxDQUFDLENBQUN2QyxPQUFGLENBQVV3RixlQUFWLEtBQThCLElBQWxDLEVBQXdDO0FBQ3BDLFVBQUt3VSxVQUFVLElBQUksRUFBZixJQUF1QkEsVUFBVSxJQUFJLEdBQXpDLEVBQStDO0FBQzNDLGVBQU8sTUFBUDtBQUNILE9BRkQsTUFFTztBQUNILGVBQU8sSUFBUDtBQUNIO0FBQ0o7O0FBRUQsV0FBTyxVQUFQO0FBRUgsR0FoQ0Q7O0FBa0NBN1gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjZRLFFBQWhCLEdBQTJCLFVBQVNqTCxLQUFULEVBQWdCO0FBRXZDLFFBQUloTixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lrRSxVQURKO0FBQUEsUUFFSVAsU0FGSjs7QUFJQTNELElBQUFBLENBQUMsQ0FBQ3NELFFBQUYsR0FBYSxLQUFiO0FBQ0F0RCxJQUFBQSxDQUFDLENBQUNvRixXQUFGLEdBQWdCLEtBQWhCO0FBQ0FwRixJQUFBQSxDQUFDLENBQUN5RixXQUFGLEdBQWtCekYsQ0FBQyxDQUFDMEUsV0FBRixDQUFjd1QsV0FBZCxHQUE0QixFQUE5QixHQUFxQyxLQUFyQyxHQUE2QyxJQUE3RDs7QUFFQSxRQUFLbFksQ0FBQyxDQUFDMEUsV0FBRixDQUFjaVQsSUFBZCxLQUF1QjlCLFNBQTVCLEVBQXdDO0FBQ3BDLGFBQU8sS0FBUDtBQUNIOztBQUVELFFBQUs3VixDQUFDLENBQUMwRSxXQUFGLENBQWN5VCxPQUFkLEtBQTBCLElBQS9CLEVBQXNDO0FBQ2xDblksTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixNQUFsQixFQUEwQixDQUFDL00sQ0FBRCxFQUFJQSxDQUFDLENBQUNxWCxjQUFGLEVBQUosQ0FBMUI7QUFDSDs7QUFFRCxRQUFLclgsQ0FBQyxDQUFDMEUsV0FBRixDQUFjd1QsV0FBZCxJQUE2QmxZLENBQUMsQ0FBQzBFLFdBQUYsQ0FBYzBULFFBQWhELEVBQTJEO0FBRXZEelUsTUFBQUEsU0FBUyxHQUFHM0QsQ0FBQyxDQUFDcVgsY0FBRixFQUFaOztBQUVBLGNBQVMxVCxTQUFUO0FBRUksYUFBSyxNQUFMO0FBQ0EsYUFBSyxNQUFMO0FBRUlPLFVBQUFBLFVBQVUsR0FDTmxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlGLFlBQVYsR0FDSTFDLENBQUMsQ0FBQ3lOLGNBQUYsQ0FBa0J6TixDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDc1EsYUFBRixFQUFuQyxDQURKLEdBRUl0USxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDc1EsYUFBRixFQUh6QjtBQUtBdFEsVUFBQUEsQ0FBQyxDQUFDd0QsZ0JBQUYsR0FBcUIsQ0FBckI7QUFFQTs7QUFFSixhQUFLLE9BQUw7QUFDQSxhQUFLLElBQUw7QUFFSVUsVUFBQUEsVUFBVSxHQUNObEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUYsWUFBVixHQUNJMUMsQ0FBQyxDQUFDeU4sY0FBRixDQUFrQnpOLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUNzUSxhQUFGLEVBQW5DLENBREosR0FFSXRRLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUNzUSxhQUFGLEVBSHpCO0FBS0F0USxVQUFBQSxDQUFDLENBQUN3RCxnQkFBRixHQUFxQixDQUFyQjtBQUVBOztBQUVKO0FBMUJKOztBQStCQSxVQUFJRyxTQUFTLElBQUksVUFBakIsRUFBOEI7QUFFMUIzRCxRQUFBQSxDQUFDLENBQUNpSyxZQUFGLENBQWdCL0YsVUFBaEI7O0FBQ0FsRSxRQUFBQSxDQUFDLENBQUMwRSxXQUFGLEdBQWdCLEVBQWhCOztBQUNBMUUsUUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixPQUFsQixFQUEyQixDQUFDL00sQ0FBRCxFQUFJMkQsU0FBSixDQUEzQjtBQUVIO0FBRUosS0EzQ0QsTUEyQ087QUFFSCxVQUFLM0QsQ0FBQyxDQUFDMEUsV0FBRixDQUFjZ1QsTUFBZCxLQUF5QjFYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2lULElBQTVDLEVBQW1EO0FBRS9DM1gsUUFBQUEsQ0FBQyxDQUFDaUssWUFBRixDQUFnQmpLLENBQUMsQ0FBQzBELFlBQWxCOztBQUNBMUQsUUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixHQUFnQixFQUFoQjtBQUVIO0FBRUo7QUFFSixHQXhFRDs7QUEwRUE5RSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCTixZQUFoQixHQUErQixVQUFTa0csS0FBVCxFQUFnQjtBQUUzQyxRQUFJaE4sQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBS0EsQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0YsS0FBVixLQUFvQixLQUFyQixJQUFnQyxnQkFBZ0IwRCxRQUFoQixJQUE0Qm5HLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWdGLEtBQVYsS0FBb0IsS0FBcEYsRUFBNEY7QUFDeEY7QUFDSCxLQUZELE1BRU8sSUFBSXpDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTRELFNBQVYsS0FBd0IsS0FBeEIsSUFBaUMyTCxLQUFLLENBQUM4RyxJQUFOLENBQVd1RSxPQUFYLENBQW1CLE9BQW5CLE1BQWdDLENBQUMsQ0FBdEUsRUFBeUU7QUFDNUU7QUFDSDs7QUFFRHJZLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBYzRULFdBQWQsR0FBNEJ0TCxLQUFLLENBQUN1TCxhQUFOLElBQXVCdkwsS0FBSyxDQUFDdUwsYUFBTixDQUFvQkMsT0FBcEIsS0FBZ0MzQyxTQUF2RCxHQUN4QjdJLEtBQUssQ0FBQ3VMLGFBQU4sQ0FBb0JDLE9BQXBCLENBQTRCMVEsTUFESixHQUNhLENBRHpDO0FBR0E5SCxJQUFBQSxDQUFDLENBQUMwRSxXQUFGLENBQWMwVCxRQUFkLEdBQXlCcFksQ0FBQyxDQUFDNkQsU0FBRixHQUFjN0QsQ0FBQyxDQUFDdkMsT0FBRixDQUNsQ21GLGNBREw7O0FBR0EsUUFBSTVDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXdGLGVBQVYsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcENqRCxNQUFBQSxDQUFDLENBQUMwRSxXQUFGLENBQWMwVCxRQUFkLEdBQXlCcFksQ0FBQyxDQUFDOEQsVUFBRixHQUFlOUQsQ0FBQyxDQUFDdkMsT0FBRixDQUNuQ21GLGNBREw7QUFFSDs7QUFFRCxZQUFRb0ssS0FBSyxDQUFDL0csSUFBTixDQUFXeUwsTUFBbkI7QUFFSSxXQUFLLE9BQUw7QUFDSTFSLFFBQUFBLENBQUMsQ0FBQ3lZLFVBQUYsQ0FBYXpMLEtBQWI7O0FBQ0E7O0FBRUosV0FBSyxNQUFMO0FBQ0loTixRQUFBQSxDQUFDLENBQUMwWSxTQUFGLENBQVkxTCxLQUFaOztBQUNBOztBQUVKLFdBQUssS0FBTDtBQUNJaE4sUUFBQUEsQ0FBQyxDQUFDaVksUUFBRixDQUFXakwsS0FBWDs7QUFDQTtBQVpSO0FBZ0JILEdBckNEOztBQXVDQXBOLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JzUixTQUFoQixHQUE0QixVQUFTMUwsS0FBVCxFQUFnQjtBQUV4QyxRQUFJaE4sQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJMlksVUFBVSxHQUFHLEtBRGpCO0FBQUEsUUFFSUMsT0FGSjtBQUFBLFFBRWF2QixjQUZiO0FBQUEsUUFFNkJhLFdBRjdCO0FBQUEsUUFFMENXLGNBRjFDO0FBQUEsUUFFMERMLE9BRjFEOztBQUlBQSxJQUFBQSxPQUFPLEdBQUd4TCxLQUFLLENBQUN1TCxhQUFOLEtBQXdCMUMsU0FBeEIsR0FBb0M3SSxLQUFLLENBQUN1TCxhQUFOLENBQW9CQyxPQUF4RCxHQUFrRSxJQUE1RTs7QUFFQSxRQUFJLENBQUN4WSxDQUFDLENBQUNzRCxRQUFILElBQWVrVixPQUFPLElBQUlBLE9BQU8sQ0FBQzFRLE1BQVIsS0FBbUIsQ0FBakQsRUFBb0Q7QUFDaEQsYUFBTyxLQUFQO0FBQ0g7O0FBRUQ4USxJQUFBQSxPQUFPLEdBQUc1WSxDQUFDLENBQUN3UCxPQUFGLENBQVV4UCxDQUFDLENBQUMwRCxZQUFaLENBQVY7QUFFQTFELElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2lULElBQWQsR0FBcUJhLE9BQU8sS0FBSzNDLFNBQVosR0FBd0IyQyxPQUFPLENBQUMsQ0FBRCxDQUFQLENBQVdNLEtBQW5DLEdBQTJDOUwsS0FBSyxDQUFDK0wsT0FBdEU7QUFDQS9ZLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY21ULElBQWQsR0FBcUJXLE9BQU8sS0FBSzNDLFNBQVosR0FBd0IyQyxPQUFPLENBQUMsQ0FBRCxDQUFQLENBQVdRLEtBQW5DLEdBQTJDaE0sS0FBSyxDQUFDaU0sT0FBdEU7QUFFQWpaLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3dULFdBQWQsR0FBNEI1TyxJQUFJLENBQUN5TyxLQUFMLENBQVd6TyxJQUFJLENBQUM0UCxJQUFMLENBQ25DNVAsSUFBSSxDQUFDNlAsR0FBTCxDQUFTblosQ0FBQyxDQUFDMEUsV0FBRixDQUFjaVQsSUFBZCxHQUFxQjNYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2dULE1BQTVDLEVBQW9ELENBQXBELENBRG1DLENBQVgsQ0FBNUI7O0FBR0EsUUFBSTFYLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXdGLGVBQVYsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcENqRCxNQUFBQSxDQUFDLENBQUMwRSxXQUFGLENBQWN3VCxXQUFkLEdBQTRCNU8sSUFBSSxDQUFDeU8sS0FBTCxDQUFXek8sSUFBSSxDQUFDNFAsSUFBTCxDQUNuQzVQLElBQUksQ0FBQzZQLEdBQUwsQ0FBU25aLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY21ULElBQWQsR0FBcUI3WCxDQUFDLENBQUMwRSxXQUFGLENBQWNrVCxNQUE1QyxFQUFvRCxDQUFwRCxDQURtQyxDQUFYLENBQTVCO0FBRUg7O0FBRURQLElBQUFBLGNBQWMsR0FBR3JYLENBQUMsQ0FBQ3FYLGNBQUYsRUFBakI7O0FBRUEsUUFBSUEsY0FBYyxLQUFLLFVBQXZCLEVBQW1DO0FBQy9CO0FBQ0g7O0FBRUQsUUFBSXJLLEtBQUssQ0FBQ3VMLGFBQU4sS0FBd0IxQyxTQUF4QixJQUFxQzdWLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3dULFdBQWQsR0FBNEIsQ0FBckUsRUFBd0U7QUFDcEVsTCxNQUFBQSxLQUFLLENBQUNPLGNBQU47QUFDSDs7QUFFRHNMLElBQUFBLGNBQWMsR0FBRyxDQUFDN1ksQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkUsR0FBVixLQUFrQixLQUFsQixHQUEwQixDQUExQixHQUE4QixDQUFDLENBQWhDLEtBQXNDcEMsQ0FBQyxDQUFDMEUsV0FBRixDQUFjaVQsSUFBZCxHQUFxQjNYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2dULE1BQW5DLEdBQTRDLENBQTVDLEdBQWdELENBQUMsQ0FBdkYsQ0FBakI7O0FBQ0EsUUFBSTFYLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXdGLGVBQVYsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcEM0VixNQUFBQSxjQUFjLEdBQUc3WSxDQUFDLENBQUMwRSxXQUFGLENBQWNtVCxJQUFkLEdBQXFCN1gsQ0FBQyxDQUFDMEUsV0FBRixDQUFja1QsTUFBbkMsR0FBNEMsQ0FBNUMsR0FBZ0QsQ0FBQyxDQUFsRTtBQUNIOztBQUdETSxJQUFBQSxXQUFXLEdBQUdsWSxDQUFDLENBQUMwRSxXQUFGLENBQWN3VCxXQUE1QjtBQUVBbFksSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjeVQsT0FBZCxHQUF3QixLQUF4Qjs7QUFFQSxRQUFJblksQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixLQUEzQixFQUFrQztBQUM5QixVQUFLMUIsQ0FBQyxDQUFDMEQsWUFBRixLQUFtQixDQUFuQixJQUF3QjJULGNBQWMsS0FBSyxPQUE1QyxJQUF5RHJYLENBQUMsQ0FBQzBELFlBQUYsSUFBa0IxRCxDQUFDLENBQUM2SyxXQUFGLEVBQWxCLElBQXFDd00sY0FBYyxLQUFLLE1BQXJILEVBQThIO0FBQzFIYSxRQUFBQSxXQUFXLEdBQUdsWSxDQUFDLENBQUMwRSxXQUFGLENBQWN3VCxXQUFkLEdBQTRCbFksQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEQsWUFBcEQ7QUFDQXZCLFFBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3lULE9BQWQsR0FBd0IsSUFBeEI7QUFDSDtBQUNKOztBQUVELFFBQUluWSxDQUFDLENBQUN2QyxPQUFGLENBQVV1RixRQUFWLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCaEQsTUFBQUEsQ0FBQyxDQUFDd0UsU0FBRixHQUFjb1UsT0FBTyxHQUFHVixXQUFXLEdBQUdXLGNBQXRDO0FBQ0gsS0FGRCxNQUVPO0FBQ0g3WSxNQUFBQSxDQUFDLENBQUN3RSxTQUFGLEdBQWNvVSxPQUFPLEdBQUlWLFdBQVcsSUFBSWxZLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTFHLE1BQVIsS0FBbUJpQyxDQUFDLENBQUM2RCxTQUF6QixDQUFaLEdBQW1EZ1YsY0FBM0U7QUFDSDs7QUFDRCxRQUFJN1ksQ0FBQyxDQUFDdkMsT0FBRixDQUFVd0YsZUFBVixLQUE4QixJQUFsQyxFQUF3QztBQUNwQ2pELE1BQUFBLENBQUMsQ0FBQ3dFLFNBQUYsR0FBY29VLE9BQU8sR0FBR1YsV0FBVyxHQUFHVyxjQUF0QztBQUNIOztBQUVELFFBQUk3WSxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLElBQW5CLElBQTJCeEIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVa0YsU0FBVixLQUF3QixLQUF2RCxFQUE4RDtBQUMxRCxhQUFPLEtBQVA7QUFDSDs7QUFFRCxRQUFJM0MsQ0FBQyxDQUFDcUQsU0FBRixLQUFnQixJQUFwQixFQUEwQjtBQUN0QnJELE1BQUFBLENBQUMsQ0FBQ3dFLFNBQUYsR0FBYyxJQUFkO0FBQ0EsYUFBTyxLQUFQO0FBQ0g7O0FBRUR4RSxJQUFBQSxDQUFDLENBQUN1VSxNQUFGLENBQVN2VSxDQUFDLENBQUN3RSxTQUFYO0FBRUgsR0F4RUQ7O0FBMEVBNUUsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnFSLFVBQWhCLEdBQTZCLFVBQVN6TCxLQUFULEVBQWdCO0FBRXpDLFFBQUloTixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l3WSxPQURKOztBQUdBeFksSUFBQUEsQ0FBQyxDQUFDb0YsV0FBRixHQUFnQixJQUFoQjs7QUFFQSxRQUFJcEYsQ0FBQyxDQUFDMEUsV0FBRixDQUFjNFQsV0FBZCxLQUE4QixDQUE5QixJQUFtQ3RZLENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFqRSxFQUErRTtBQUMzRXZDLE1BQUFBLENBQUMsQ0FBQzBFLFdBQUYsR0FBZ0IsRUFBaEI7QUFDQSxhQUFPLEtBQVA7QUFDSDs7QUFFRCxRQUFJc0ksS0FBSyxDQUFDdUwsYUFBTixLQUF3QjFDLFNBQXhCLElBQXFDN0ksS0FBSyxDQUFDdUwsYUFBTixDQUFvQkMsT0FBcEIsS0FBZ0MzQyxTQUF6RSxFQUFvRjtBQUNoRjJDLE1BQUFBLE9BQU8sR0FBR3hMLEtBQUssQ0FBQ3VMLGFBQU4sQ0FBb0JDLE9BQXBCLENBQTRCLENBQTVCLENBQVY7QUFDSDs7QUFFRHhZLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2dULE1BQWQsR0FBdUIxWCxDQUFDLENBQUMwRSxXQUFGLENBQWNpVCxJQUFkLEdBQXFCYSxPQUFPLEtBQUszQyxTQUFaLEdBQXdCMkMsT0FBTyxDQUFDTSxLQUFoQyxHQUF3QzlMLEtBQUssQ0FBQytMLE9BQTFGO0FBQ0EvWSxJQUFBQSxDQUFDLENBQUMwRSxXQUFGLENBQWNrVCxNQUFkLEdBQXVCNVgsQ0FBQyxDQUFDMEUsV0FBRixDQUFjbVQsSUFBZCxHQUFxQlcsT0FBTyxLQUFLM0MsU0FBWixHQUF3QjJDLE9BQU8sQ0FBQ1EsS0FBaEMsR0FBd0NoTSxLQUFLLENBQUNpTSxPQUExRjtBQUVBalosSUFBQUEsQ0FBQyxDQUFDc0QsUUFBRixHQUFhLElBQWI7QUFFSCxHQXJCRDs7QUF1QkExRCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCZ1MsY0FBaEIsR0FBaUN4WixLQUFLLENBQUN3SCxTQUFOLENBQWdCaVMsYUFBaEIsR0FBZ0MsWUFBVztBQUV4RSxRQUFJclosQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDMkYsWUFBRixLQUFtQixJQUF2QixFQUE2QjtBQUV6QjNGLE1BQUFBLENBQUMsQ0FBQzZILE1BQUY7O0FBRUE3SCxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLEtBQUszSyxPQUFMLENBQWE0RSxLQUFwQyxFQUEyQ2dHLE1BQTNDOztBQUVBckksTUFBQUEsQ0FBQyxDQUFDMkYsWUFBRixDQUFlb0MsUUFBZixDQUF3Qi9ILENBQUMsQ0FBQ29FLFdBQTFCOztBQUVBcEUsTUFBQUEsQ0FBQyxDQUFDdUksTUFBRjtBQUVIO0FBRUosR0FoQkQ7O0FBa0JBM0ksRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQlMsTUFBaEIsR0FBeUIsWUFBVztBQUVoQyxRQUFJN0gsQ0FBQyxHQUFHLElBQVI7O0FBRUExQyxJQUFBQSxDQUFDLENBQUMsZUFBRCxFQUFrQjBDLENBQUMsQ0FBQzBGLE9BQXBCLENBQUQsQ0FBOEIrSSxNQUE5Qjs7QUFFQSxRQUFJek8sQ0FBQyxDQUFDNEQsS0FBTixFQUFhO0FBQ1Q1RCxNQUFBQSxDQUFDLENBQUM0RCxLQUFGLENBQVE2SyxNQUFSO0FBQ0g7O0FBRUQsUUFBSXpPLENBQUMsQ0FBQ2lFLFVBQUYsSUFBZ0JqRSxDQUFDLENBQUNpSCxRQUFGLENBQVd3RCxJQUFYLENBQWdCekssQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0MsU0FBMUIsQ0FBcEIsRUFBMEQ7QUFDdERSLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYXdLLE1BQWI7QUFDSDs7QUFFRCxRQUFJek8sQ0FBQyxDQUFDZ0UsVUFBRixJQUFnQmhFLENBQUMsQ0FBQ2lILFFBQUYsQ0FBV3dELElBQVgsQ0FBZ0J6SyxDQUFDLENBQUN2QyxPQUFGLENBQVVnRCxTQUExQixDQUFwQixFQUEwRDtBQUN0RFQsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFheUssTUFBYjtBQUNIOztBQUVEek8sSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUNLa0csV0FETCxDQUNpQixzREFEakIsRUFFS2hELElBRkwsQ0FFVSxhQUZWLEVBRXlCLE1BRnpCLEVBR0t4SSxHQUhMLENBR1MsT0FIVCxFQUdrQixFQUhsQjtBQUtILEdBdkJEOztBQXlCQWEsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnlGLE9BQWhCLEdBQTBCLFVBQVN5TSxjQUFULEVBQXlCO0FBRS9DLFFBQUl0WixDQUFDLEdBQUcsSUFBUjs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixTQUFsQixFQUE2QixDQUFDL00sQ0FBRCxFQUFJc1osY0FBSixDQUE3Qjs7QUFDQXRaLElBQUFBLENBQUMsQ0FBQ3dPLE9BQUY7QUFFSCxHQU5EOztBQVFBNU8sRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmdLLFlBQWhCLEdBQStCLFlBQVc7QUFFdEMsUUFBSXBSLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXlRLFlBREo7O0FBR0FBLElBQUFBLFlBQVksR0FBR25ILElBQUksQ0FBQ3NHLEtBQUwsQ0FBVzVQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBcEMsQ0FBZjs7QUFFQSxRQUFLdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkMsTUFBVixLQUFxQixJQUFyQixJQUNETixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUR4QixJQUVELENBQUN2QyxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUZmLEVBRTBCO0FBRXRCMUIsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFhc0csV0FBYixDQUF5QixnQkFBekIsRUFBMkNoRCxJQUEzQyxDQUFnRCxlQUFoRCxFQUFpRSxPQUFqRTs7QUFDQXZILE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUYsQ0FBYXVHLFdBQWIsQ0FBeUIsZ0JBQXpCLEVBQTJDaEQsSUFBM0MsQ0FBZ0QsZUFBaEQsRUFBaUUsT0FBakU7O0FBRUEsVUFBSXZILENBQUMsQ0FBQzBELFlBQUYsS0FBbUIsQ0FBdkIsRUFBMEI7QUFFdEIxRCxRQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWF2RixRQUFiLENBQXNCLGdCQUF0QixFQUF3QzZJLElBQXhDLENBQTZDLGVBQTdDLEVBQThELE1BQTlEOztBQUNBdkgsUUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhdUcsV0FBYixDQUF5QixnQkFBekIsRUFBMkNoRCxJQUEzQyxDQUFnRCxlQUFoRCxFQUFpRSxPQUFqRTtBQUVILE9BTEQsTUFLTyxJQUFJdkgsQ0FBQyxDQUFDMEQsWUFBRixJQUFrQjFELENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTNDLElBQTJEdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixLQUF4RixFQUErRjtBQUVsR1osUUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhdEYsUUFBYixDQUFzQixnQkFBdEIsRUFBd0M2SSxJQUF4QyxDQUE2QyxlQUE3QyxFQUE4RCxNQUE5RDs7QUFDQXZILFFBQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYXNHLFdBQWIsQ0FBeUIsZ0JBQXpCLEVBQTJDaEQsSUFBM0MsQ0FBZ0QsZUFBaEQsRUFBaUUsT0FBakU7QUFFSCxPQUxNLE1BS0EsSUFBSXZILENBQUMsQ0FBQzBELFlBQUYsSUFBa0IxRCxDQUFDLENBQUNrRSxVQUFGLEdBQWUsQ0FBakMsSUFBc0NsRSxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQW5FLEVBQXlFO0FBRTVFWixRQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWF0RixRQUFiLENBQXNCLGdCQUF0QixFQUF3QzZJLElBQXhDLENBQTZDLGVBQTdDLEVBQThELE1BQTlEOztBQUNBdkgsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFhc0csV0FBYixDQUF5QixnQkFBekIsRUFBMkNoRCxJQUEzQyxDQUFnRCxlQUFoRCxFQUFpRSxPQUFqRTtBQUVIO0FBRUo7QUFFSixHQWpDRDs7QUFtQ0EzSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCOEQsVUFBaEIsR0FBNkIsWUFBVztBQUVwQyxRQUFJbEwsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDNEQsS0FBRixLQUFZLElBQWhCLEVBQXNCO0FBRWxCNUQsTUFBQUEsQ0FBQyxDQUFDNEQsS0FBRixDQUNLMEQsSUFETCxDQUNVLElBRFYsRUFFS2lELFdBRkwsQ0FFaUIsY0FGakIsRUFHS2hELElBSEwsQ0FHVSxhQUhWLEVBR3lCLE1BSHpCOztBQUtBdkgsTUFBQUEsQ0FBQyxDQUFDNEQsS0FBRixDQUNLMEQsSUFETCxDQUNVLElBRFYsRUFFS1csRUFGTCxDQUVRcUIsSUFBSSxDQUFDc0csS0FBTCxDQUFXNVAsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXRDLENBRlIsRUFHSzlELFFBSEwsQ0FHYyxjQUhkLEVBSUs2SSxJQUpMLENBSVUsYUFKVixFQUl5QixPQUp6QjtBQU1IO0FBRUosR0FuQkQ7O0FBcUJBM0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjZHLFVBQWhCLEdBQTZCLFlBQVc7QUFFcEMsUUFBSWpPLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUtBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlELFFBQWYsRUFBMEI7QUFFdEIsVUFBS3lGLFFBQVEsQ0FBQ25HLENBQUMsQ0FBQ3FGLE1BQUgsQ0FBYixFQUEwQjtBQUV0QnJGLFFBQUFBLENBQUMsQ0FBQ29GLFdBQUYsR0FBZ0IsSUFBaEI7QUFFSCxPQUpELE1BSU87QUFFSHBGLFFBQUFBLENBQUMsQ0FBQ29GLFdBQUYsR0FBZ0IsS0FBaEI7QUFFSDtBQUVKO0FBRUosR0FsQkQ7O0FBb0JBOUgsRUFBQUEsQ0FBQyxDQUFDQyxFQUFGLENBQUt5TSxLQUFMLEdBQWEsWUFBVztBQUNwQixRQUFJaEssQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJdVYsR0FBRyxHQUFHRCxTQUFTLENBQUMsQ0FBRCxDQURuQjtBQUFBLFFBRUlpRSxJQUFJLEdBQUdDLEtBQUssQ0FBQ3BTLFNBQU4sQ0FBZ0J3TCxLQUFoQixDQUFzQm5KLElBQXRCLENBQTJCNkwsU0FBM0IsRUFBc0MsQ0FBdEMsQ0FGWDtBQUFBLFFBR0kxQixDQUFDLEdBQUc1VCxDQUFDLENBQUM4SCxNQUhWO0FBQUEsUUFJSTdHLENBSko7QUFBQSxRQUtJd1ksR0FMSjs7QUFNQSxTQUFLeFksQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHMlMsQ0FBaEIsRUFBbUIzUyxDQUFDLEVBQXBCLEVBQXdCO0FBQ3BCLFVBQUksUUFBT3NVLEdBQVAsS0FBYyxRQUFkLElBQTBCLE9BQU9BLEdBQVAsSUFBYyxXQUE1QyxFQUNJdlYsQ0FBQyxDQUFDaUIsQ0FBRCxDQUFELENBQUsrSSxLQUFMLEdBQWEsSUFBSXBLLEtBQUosQ0FBVUksQ0FBQyxDQUFDaUIsQ0FBRCxDQUFYLEVBQWdCc1UsR0FBaEIsQ0FBYixDQURKLEtBR0lrRSxHQUFHLEdBQUd6WixDQUFDLENBQUNpQixDQUFELENBQUQsQ0FBSytJLEtBQUwsQ0FBV3VMLEdBQVgsRUFBZ0JtRSxLQUFoQixDQUFzQjFaLENBQUMsQ0FBQ2lCLENBQUQsQ0FBRCxDQUFLK0ksS0FBM0IsRUFBa0N1UCxJQUFsQyxDQUFOO0FBQ0osVUFBSSxPQUFPRSxHQUFQLElBQWMsV0FBbEIsRUFBK0IsT0FBT0EsR0FBUDtBQUNsQzs7QUFDRCxXQUFPelosQ0FBUDtBQUNILEdBZkQ7QUFpQkgsQ0ExekZBLENBQUQ7OztBQ2pCQTtBQUNBLENBQUMsVUFBUzFDLENBQVQsRUFBWTtBQUNYLE1BQUlBLENBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJ3SyxNQUFqQixHQUEwQixDQUE5QixFQUFpQztBQUMvQjtBQUNEOztBQUVELE1BQU1nWSxLQUFLLEdBQUd4aUIsQ0FBQyxDQUFDLE9BQUQsQ0FBZjtBQUNBLE1BQU1vSSxPQUFPLEdBQUdvYSxLQUFLLENBQUN4WSxJQUFOLENBQVcsY0FBWCxDQUFoQjs7QUFFQSxNQUFNeVksS0FBSyxHQUFHLFNBQVJBLEtBQVEsR0FBVztBQUN2QnJhLElBQUFBLE9BQU8sQ0FBQ3NFLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsR0FGRDs7QUFJQSxNQUFNZ1csS0FBSyxHQUFHLFNBQVJBLEtBQVEsR0FBVztBQUN2QnRhLElBQUFBLE9BQU8sQ0FBQ3NFLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsR0FGRDs7QUFJQThWLEVBQUFBLEtBQUssQ0FBQzVnQixFQUFOLENBQVMsT0FBVCxFQUFrQixhQUFsQixFQUFpQzZnQixLQUFqQztBQUNBRCxFQUFBQSxLQUFLLENBQUM1Z0IsRUFBTixDQUFTLE9BQVQsRUFBa0IsY0FBbEIsRUFBa0M4Z0IsS0FBbEM7QUFDRCxDQWxCRCxFQWtCRzNnQixNQWxCSCxFLENBb0JBOzs7QUFDQSxDQUFDLFVBQVMvQixDQUFULEVBQVk7QUFDWCxNQUFJQSxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCd0ssTUFBakIsR0FBMEIsQ0FBOUIsRUFBaUM7QUFDL0I7QUFDRDs7QUFFRCxNQUFNZ1ksS0FBSyxHQUFHeGlCLENBQUMsQ0FBQyxVQUFELENBQWY7O0FBRUEsTUFBTTJpQixPQUFPLEdBQUcsU0FBVkEsT0FBVSxHQUFXO0FBQ3pCM2lCLElBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FDR3dCLE1BREgsR0FFR0ssV0FGSCxDQUVlLFFBRmY7QUFHRCxHQUpEOztBQU1BMmdCLEVBQUFBLEtBQUssQ0FBQzVnQixFQUFOLENBQVMsT0FBVCxFQUFrQixJQUFsQixFQUF3QitnQixPQUF4QjtBQUNELENBZEQsRUFjRzVnQixNQWRIIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigkKXtcblxuLypcbiAgT3B0aW9uczpcblxuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcbiAgUmVxdWlyZWRcbiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cbiAgLS0gU2xpZGUgaW4gU2lkZSA9IGRlZmF1bHQgbGVmdFxuICAgIC0tIFBvc3NpYmxlIG9wdGlvbnMgXCJ0b3BcIiwgXCJsZWZ0XCIsIFwicmlnaHRcIiwgXCJib3R0b21cIlxuXG4gIC0tIFNsaWRlIGluIFNwZWVkID0gZGVmYXVsdCA0MDBcbiAgICAtLSBPcHRpb25zIGFueSBtaWxpc2Vjb25kc1xuXG4gIC0tIE9wdGlvbmFsIE9wZW4vQ2xvc2UgQnV0dG9uID0gZGVmYXVsdCBDcmVhdGUgb24gZm9yIHRoZW1cbiAgICAtLSBBbnkgb2JqZWN0IHdpdGggYSBkaXYgb3IgY2xhc3NcblxuICAtLSBXaWR0aCBvZiBNZW51ID0gZGVmYXVsdCAxMDAlXG4gICAgLS0gQW55IG51bWJlclxuXG4jIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuICBOb24gRXNzZW50YWlsc1xuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblxuICAtLSBGaXhlZCBvciBTdGF0aWMgPSBEZWZhdWx0IEZpeGVkXG4gICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG5cbiAgLS0gQW5pbWF0aW9uXG4gICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG5cbiAgLS0gT3B0aW9uYWwgQnV0dG9uIFN0eWxpbmdcbiAgICAtLSBOb24gZXNzZW50aWFsIGZvciBub3dcblxuXG5cbiovXG5cbiAgJC5mbi5tb2JpbGVfbWVudSA9IGZ1bmN0aW9uICggb3B0aW9ucyApIHtcblxuICAgIC8vIERlZmluZSBEZWZhdWx0IHNldHRpbmdzOlxuICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgIHNpZGU6ICdsZWZ0JyxcbiAgICAgIHNwZWVkOiAnMC41cycsXG4gICAgICB0b2dnbGVyOiAnLm1lbnUtdG9nZ2xlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIGJ1dHRvblN0eWxlczogdHJ1ZSxcbiAgICB9O1xuXG4gICAgdmFyIGluaXRhbHMgPSB7XG4gICAgICB0b2dnbGVCdXR0b246ICc8ZGl2IGNsYXNzPVwibWVudS10b2dnbGVcIj48L2Rpdj4nLFxuICAgICAgbWVudVdyYXA6ICc8ZGl2IGNsYXNzPVwib2ZmY2FudmFzLW1lbnUtd3JhcFwiPjwvZGl2PicsXG4gICAgfTtcblxuICAgIHZhciAkdG9nZ2xlcjtcbiAgICB2YXIgb2ZmY2FudmFzO1xuXG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICAgIHZhciBzZXR0aW5ncyA9ICQuZXh0ZW5kKCB7fSwgaW5pdGFscywgZGVmYXVsdHMsIG9wdGlvbnMgKTtcblxuICAgIGlmKHNldHRpbmdzLnRvZ2dsZXIgPT0gZGVmYXVsdHMudG9nZ2xlcil7XG4gICAgICAkdGhpcy5iZWZvcmUoc2V0dGluZ3MudG9nZ2xlQnV0dG9uKTtcbiAgICB9XG5cbiAgICAvKiBEZXRlcm1pbmUgaWYgeW91IHdhbnQgdG9nZ2xlciBTdHlsZXMgKi9cbiAgICAkdG9nZ2xlciA9ICQoc2V0dGluZ3MudG9nZ2xlcik7XG4gICAgaWYoc2V0dGluZ3MuYnV0dG9uU3R5bGVzKXtcbiAgICAgICR0b2dnbGVyLmFkZENsYXNzKCdvZmZjYW52YXMtdG9nZ2xlJyk7XG4gICAgfVxuXG4gICAgc3dpdGNoICggc2V0dGluZ3Muc2lkZSApIHtcbiAgICAgIGNhc2UgXCJsZWZ0XCI6XG4gICAgICAgIG9mZmNhbnZhcyA9ICd0cmFuc2xhdGVYKC0xMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcInRvcFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWSgtMTAwJSknO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJyaWdodFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWCgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcImJvdHRvbVwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWSgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIHZhciBzdHlsZXMgPSB7XG4gICAgICAndHJhbnNmb3JtJyA6IG9mZmNhbnZhcyxcbiAgICAgICd0cmFuc2l0aW9uJyA6IHNldHRpbmdzLnNwZWVkLFxuICAgICAgaGVpZ2h0IDogc2V0dGluZ3MuaGVpZ2h0LFxuICAgICAgd2lkdGggOiBzZXR0aW5ncy53aWR0aCxcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpIHtcblxuICAgICAgICAkdGhpcy5hZGRDbGFzcygnb2ZmY2FudmFzLW1lbnUnKS53cmFwKHNldHRpbmdzLm1lbnVXcmFwKTtcbiAgICAgICAgJHRoaXMucGFyZW50KCkuY3NzKCBzdHlsZXMgKTtcbiAgICAgICAgY29uc29sZS5sb2coJHRvZ2dsZXIpO1xuICAgICAgICAkdG9nZ2xlci5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdvcGVuL2Nsb3NlJyk7XG5cbiAgICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdvZmZjYW52YXMtYWN0aXZlJyk7XG4gICAgICAgICAgY29uc29sZS5sb2coJHRoaXMpO1xuICAgICAgICAgICR0aGlzLmNsb3Nlc3QoJy5vZmZjYW52YXMtbWVudS13cmFwJykudG9nZ2xlQ2xhc3MoJ29mZmNhbnZhcy1vcGVuJyk7XG5cbiAgICAgICAgfSk7XG5cblxuICAgIH0pO1xuICB9XG5cblxuICAkKCcjbW9iaWxlX21lbnUnKS5tb2JpbGVfbWVudSh7XG4gICAgc2lkZTogJ2JvdHRvbScsXG4gIH0pO1xuXG5cbn0pKGpRdWVyeSk7XG4iLCIoZnVuY3Rpb24gKCQpIHtcbiAgLypcbiAgICBPcHRpb25zOlxuICBcbiAgIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcbiAgICBSZXF1aXJlZFxuICAjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuICBcbiAgICAtLSBTbGlkZSBpbiBTaWRlID0gZGVmYXVsdCBsZWZ0XG4gICAgICAtLSBQb3NzaWJsZSBvcHRpb25zIFwidG9wXCIsIFwibGVmdFwiLCBcInJpZ2h0XCIsIFwiYm90dG9tXCJcbiAgXG4gICAgLS0gU2xpZGUgaW4gU3BlZWQgPSBkZWZhdWx0IDQwMFxuICAgICAgLS0gT3B0aW9ucyBhbnkgbWlsaXNlY29uZHNcbiAgXG4gICAgLS0gT3B0aW9uYWwgT3Blbi9DbG9zZSBCdXR0b24gPSBkZWZhdWx0IENyZWF0ZSBvbiBmb3IgdGhlbVxuICAgICAgLS0gQW55IG9iamVjdCB3aXRoIGEgZGl2IG9yIGNsYXNzXG4gIFxuICAgIC0tIFdpZHRoIG9mIE1lbnUgPSBkZWZhdWx0IDEwMCVcbiAgICAgIC0tIEFueSBudW1iZXJcbiAgXG4gICMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG4gICAgTm9uIEVzc2VudGFpbHNcbiAgIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcbiAgXG4gICAgLS0gRml4ZWQgb3IgU3RhdGljID0gRGVmYXVsdCBGaXhlZFxuICAgICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG4gIFxuICAgIC0tIEFuaW1hdGlvblxuICAgICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG4gIFxuICAgIC0tIE9wdGlvbmFsIEJ1dHRvbiBTdHlsaW5nXG4gICAgICAtLSBOb24gZXNzZW50aWFsIGZvciBub3dcbiAgXG4gIFxuICBcbiAgKi9cbiAgJC5mbi5tb2JpbGVfbWVudSA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgLy8gRGVmaW5lIERlZmF1bHQgc2V0dGluZ3M6XG4gICAgdmFyIGRlZmF1bHRzID0ge1xuICAgICAgc2lkZTogJ2xlZnQnLFxuICAgICAgc3BlZWQ6ICcwLjVzJyxcbiAgICAgIHRvZ2dsZXI6ICcubWVudS10b2dnbGUnLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJzEwMCUnLFxuICAgICAgYnV0dG9uU3R5bGVzOiB0cnVlXG4gICAgfTtcbiAgICB2YXIgaW5pdGFscyA9IHtcbiAgICAgIHRvZ2dsZUJ1dHRvbjogJzxkaXYgY2xhc3M9XCJtZW51LXRvZ2dsZVwiPjwvZGl2PicsXG4gICAgICBtZW51V3JhcDogJzxkaXYgY2xhc3M9XCJvZmZjYW52YXMtbWVudS13cmFwXCI+PC9kaXY+J1xuICAgIH07XG4gICAgdmFyICR0b2dnbGVyO1xuICAgIHZhciBvZmZjYW52YXM7XG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICB2YXIgc2V0dGluZ3MgPSAkLmV4dGVuZCh7fSwgaW5pdGFscywgZGVmYXVsdHMsIG9wdGlvbnMpO1xuXG4gICAgaWYgKHNldHRpbmdzLnRvZ2dsZXIgPT0gZGVmYXVsdHMudG9nZ2xlcikge1xuICAgICAgJHRoaXMuYmVmb3JlKHNldHRpbmdzLnRvZ2dsZUJ1dHRvbik7XG4gICAgfVxuICAgIC8qIERldGVybWluZSBpZiB5b3Ugd2FudCB0b2dnbGVyIFN0eWxlcyAqL1xuXG5cbiAgICAkdG9nZ2xlciA9ICQoc2V0dGluZ3MudG9nZ2xlcik7XG5cbiAgICBpZiAoc2V0dGluZ3MuYnV0dG9uU3R5bGVzKSB7XG4gICAgICAkdG9nZ2xlci5hZGRDbGFzcygnb2ZmY2FudmFzLXRvZ2dsZScpO1xuICAgIH1cblxuICAgIHN3aXRjaCAoc2V0dGluZ3Muc2lkZSkge1xuICAgICAgY2FzZSBcImxlZnRcIjpcbiAgICAgICAgb2ZmY2FudmFzID0gJ3RyYW5zbGF0ZVgoLTEwMCUpJztcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgXCJ0b3BcIjpcbiAgICAgICAgb2ZmY2FudmFzID0gJ3RyYW5zbGF0ZVkoLTEwMCUpJztcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgXCJyaWdodFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWCgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIFwiYm90dG9tXCI6XG4gICAgICAgIG9mZmNhbnZhcyA9ICd0cmFuc2xhdGVZKDEwMCUpJztcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgdmFyIHN0eWxlcyA9IHtcbiAgICAgICd0cmFuc2Zvcm0nOiBvZmZjYW52YXMsXG4gICAgICAndHJhbnNpdGlvbic6IHNldHRpbmdzLnNwZWVkLFxuICAgICAgaGVpZ2h0OiBzZXR0aW5ncy5oZWlnaHQsXG4gICAgICB3aWR0aDogc2V0dGluZ3Mud2lkdGhcbiAgICB9O1xuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgJHRoaXMuYWRkQ2xhc3MoJ29mZmNhbnZhcy1tZW51Jykud3JhcChzZXR0aW5ncy5tZW51V3JhcCk7XG4gICAgICAkdGhpcy5wYXJlbnQoKS5jc3Moc3R5bGVzKTtcbiAgICAgIGNvbnNvbGUubG9nKCR0b2dnbGVyKTtcbiAgICAgICR0b2dnbGVyLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ29wZW4vY2xvc2UnKTtcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygnb2ZmY2FudmFzLWFjdGl2ZScpO1xuICAgICAgICBjb25zb2xlLmxvZygkdGhpcyk7XG4gICAgICAgICR0aGlzLmNsb3Nlc3QoJy5vZmZjYW52YXMtbWVudS13cmFwJykudG9nZ2xlQ2xhc3MoJ29mZmNhbnZhcy1vcGVuJyk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfTtcblxuICAkKCcjbW9iaWxlX21lbnUnKS5tb2JpbGVfbWVudSh7XG4gICAgc2lkZTogJ2JvdHRvbSdcbiAgfSk7XG59KShqUXVlcnkpO1xuOy8qXG4gICAgIF8gXyAgICAgIF8gICAgICAgX1xuIF9fX3wgKF8pIF9fX3wgfCBfXyAgKF8pX19fXG4vIF9ffCB8IHwvIF9ffCB8LyAvICB8IC8gX198XG5cXF9fIFxcIHwgfCAoX198ICAgPCBfIHwgXFxfXyBcXFxufF9fXy9ffF98XFxfX198X3xcXF8oXykvIHxfX18vXG4gICAgICAgICAgICAgICAgICAgfF9fL1xuXG4gVmVyc2lvbjogMS42LjBcbiAgQXV0aG9yOiBLZW4gV2hlZWxlclxuIFdlYnNpdGU6IGh0dHA6Ly9rZW53aGVlbGVyLmdpdGh1Yi5pb1xuICAgIERvY3M6IGh0dHA6Ly9rZW53aGVlbGVyLmdpdGh1Yi5pby9zbGlja1xuICAgIFJlcG86IGh0dHA6Ly9naXRodWIuY29tL2tlbndoZWVsZXIvc2xpY2tcbiAgSXNzdWVzOiBodHRwOi8vZ2l0aHViLmNvbS9rZW53aGVlbGVyL3NsaWNrL2lzc3Vlc1xuXG4gKi9cblxuLyogZ2xvYmFsIHdpbmRvdywgZG9jdW1lbnQsIGRlZmluZSwgalF1ZXJ5LCBzZXRJbnRlcnZhbCwgY2xlYXJJbnRlcnZhbCAqL1xuKGZ1bmN0aW9uIChmYWN0b3J5KSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG4gICAgZGVmaW5lKFsnanF1ZXJ5J10sIGZhY3RvcnkpO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBleHBvcnRzICE9PSAndW5kZWZpbmVkJykge1xuICAgIG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShyZXF1aXJlKCdqcXVlcnknKSk7XG4gIH0gZWxzZSB7XG4gICAgZmFjdG9yeShqUXVlcnkpO1xuICB9XG59KShmdW5jdGlvbiAoJCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgdmFyIFNsaWNrID0gd2luZG93LlNsaWNrIHx8IHt9O1xuXG4gIFNsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBpbnN0YW5jZVVpZCA9IDA7XG5cbiAgICBmdW5jdGlvbiBTbGljayhlbGVtZW50LCBzZXR0aW5ncykge1xuICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgIGRhdGFTZXR0aW5ncztcblxuICAgICAgXy5kZWZhdWx0cyA9IHtcbiAgICAgICAgYWNjZXNzaWJpbGl0eTogdHJ1ZSxcbiAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxuICAgICAgICBhcHBlbmRBcnJvd3M6ICQoZWxlbWVudCksXG4gICAgICAgIGFwcGVuZERvdHM6ICQoZWxlbWVudCksXG4gICAgICAgIGFycm93czogdHJ1ZSxcbiAgICAgICAgYXNOYXZGb3I6IG51bGwsXG4gICAgICAgIHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGRhdGEtcm9sZT1cIm5vbmVcIiBjbGFzcz1cInNsaWNrLXByZXZcIiBhcmlhLWxhYmVsPVwiUHJldmlvdXNcIiB0YWJpbmRleD1cIjBcIiByb2xlPVwiYnV0dG9uXCI+UHJldmlvdXM8L2J1dHRvbj4nLFxuICAgICAgICBuZXh0QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBkYXRhLXJvbGU9XCJub25lXCIgY2xhc3M9XCJzbGljay1uZXh0XCIgYXJpYS1sYWJlbD1cIk5leHRcIiB0YWJpbmRleD1cIjBcIiByb2xlPVwiYnV0dG9uXCI+TmV4dDwvYnV0dG9uPicsXG4gICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcbiAgICAgICAgYXV0b3BsYXlTcGVlZDogMzAwMCxcbiAgICAgICAgY2VudGVyTW9kZTogZmFsc2UsXG4gICAgICAgIGNlbnRlclBhZGRpbmc6ICc1MHB4JyxcbiAgICAgICAgY3NzRWFzZTogJ2Vhc2UnLFxuICAgICAgICBjdXN0b21QYWdpbmc6IGZ1bmN0aW9uIChzbGlkZXIsIGkpIHtcbiAgICAgICAgICByZXR1cm4gJCgnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgZGF0YS1yb2xlPVwibm9uZVwiIHJvbGU9XCJidXR0b25cIiB0YWJpbmRleD1cIjBcIiAvPicpLnRleHQoaSArIDEpO1xuICAgICAgICB9LFxuICAgICAgICBkb3RzOiBmYWxzZSxcbiAgICAgICAgZG90c0NsYXNzOiAnc2xpY2stZG90cycsXG4gICAgICAgIGRyYWdnYWJsZTogdHJ1ZSxcbiAgICAgICAgZWFzaW5nOiAnbGluZWFyJyxcbiAgICAgICAgZWRnZUZyaWN0aW9uOiAwLjM1LFxuICAgICAgICBmYWRlOiBmYWxzZSxcbiAgICAgICAgZm9jdXNPblNlbGVjdDogZmFsc2UsXG4gICAgICAgIGluZmluaXRlOiB0cnVlLFxuICAgICAgICBpbml0aWFsU2xpZGU6IDAsXG4gICAgICAgIGxhenlMb2FkOiAnb25kZW1hbmQnLFxuICAgICAgICBtb2JpbGVGaXJzdDogZmFsc2UsXG4gICAgICAgIHBhdXNlT25Ib3ZlcjogdHJ1ZSxcbiAgICAgICAgcGF1c2VPbkZvY3VzOiB0cnVlLFxuICAgICAgICBwYXVzZU9uRG90c0hvdmVyOiBmYWxzZSxcbiAgICAgICAgcmVzcG9uZFRvOiAnd2luZG93JyxcbiAgICAgICAgcmVzcG9uc2l2ZTogbnVsbCxcbiAgICAgICAgcm93czogMSxcbiAgICAgICAgcnRsOiBmYWxzZSxcbiAgICAgICAgc2xpZGU6ICcnLFxuICAgICAgICBzbGlkZXNQZXJSb3c6IDEsXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgIHNwZWVkOiA1MDAsXG4gICAgICAgIHN3aXBlOiB0cnVlLFxuICAgICAgICBzd2lwZVRvU2xpZGU6IGZhbHNlLFxuICAgICAgICB0b3VjaE1vdmU6IHRydWUsXG4gICAgICAgIHRvdWNoVGhyZXNob2xkOiA1LFxuICAgICAgICB1c2VDU1M6IHRydWUsXG4gICAgICAgIHVzZVRyYW5zZm9ybTogdHJ1ZSxcbiAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXG4gICAgICAgIHZlcnRpY2FsOiBmYWxzZSxcbiAgICAgICAgdmVydGljYWxTd2lwaW5nOiBmYWxzZSxcbiAgICAgICAgd2FpdEZvckFuaW1hdGU6IHRydWUsXG4gICAgICAgIHpJbmRleDogMTAwMFxuICAgICAgfTtcbiAgICAgIF8uaW5pdGlhbHMgPSB7XG4gICAgICAgIGFuaW1hdGluZzogZmFsc2UsXG4gICAgICAgIGRyYWdnaW5nOiBmYWxzZSxcbiAgICAgICAgYXV0b1BsYXlUaW1lcjogbnVsbCxcbiAgICAgICAgY3VycmVudERpcmVjdGlvbjogMCxcbiAgICAgICAgY3VycmVudExlZnQ6IG51bGwsXG4gICAgICAgIGN1cnJlbnRTbGlkZTogMCxcbiAgICAgICAgZGlyZWN0aW9uOiAxLFxuICAgICAgICAkZG90czogbnVsbCxcbiAgICAgICAgbGlzdFdpZHRoOiBudWxsLFxuICAgICAgICBsaXN0SGVpZ2h0OiBudWxsLFxuICAgICAgICBsb2FkSW5kZXg6IDAsXG4gICAgICAgICRuZXh0QXJyb3c6IG51bGwsXG4gICAgICAgICRwcmV2QXJyb3c6IG51bGwsXG4gICAgICAgIHNsaWRlQ291bnQ6IG51bGwsXG4gICAgICAgIHNsaWRlV2lkdGg6IG51bGwsXG4gICAgICAgICRzbGlkZVRyYWNrOiBudWxsLFxuICAgICAgICAkc2xpZGVzOiBudWxsLFxuICAgICAgICBzbGlkaW5nOiBmYWxzZSxcbiAgICAgICAgc2xpZGVPZmZzZXQ6IDAsXG4gICAgICAgIHN3aXBlTGVmdDogbnVsbCxcbiAgICAgICAgJGxpc3Q6IG51bGwsXG4gICAgICAgIHRvdWNoT2JqZWN0OiB7fSxcbiAgICAgICAgdHJhbnNmb3Jtc0VuYWJsZWQ6IGZhbHNlLFxuICAgICAgICB1bnNsaWNrZWQ6IGZhbHNlXG4gICAgICB9O1xuICAgICAgJC5leHRlbmQoXywgXy5pbml0aWFscyk7XG4gICAgICBfLmFjdGl2ZUJyZWFrcG9pbnQgPSBudWxsO1xuICAgICAgXy5hbmltVHlwZSA9IG51bGw7XG4gICAgICBfLmFuaW1Qcm9wID0gbnVsbDtcbiAgICAgIF8uYnJlYWtwb2ludHMgPSBbXTtcbiAgICAgIF8uYnJlYWtwb2ludFNldHRpbmdzID0gW107XG4gICAgICBfLmNzc1RyYW5zaXRpb25zID0gZmFsc2U7XG4gICAgICBfLmZvY3Vzc2VkID0gZmFsc2U7XG4gICAgICBfLmludGVycnVwdGVkID0gZmFsc2U7XG4gICAgICBfLmhpZGRlbiA9ICdoaWRkZW4nO1xuICAgICAgXy5wYXVzZWQgPSB0cnVlO1xuICAgICAgXy5wb3NpdGlvblByb3AgPSBudWxsO1xuICAgICAgXy5yZXNwb25kVG8gPSBudWxsO1xuICAgICAgXy5yb3dDb3VudCA9IDE7XG4gICAgICBfLnNob3VsZENsaWNrID0gdHJ1ZTtcbiAgICAgIF8uJHNsaWRlciA9ICQoZWxlbWVudCk7XG4gICAgICBfLiRzbGlkZXNDYWNoZSA9IG51bGw7XG4gICAgICBfLnRyYW5zZm9ybVR5cGUgPSBudWxsO1xuICAgICAgXy50cmFuc2l0aW9uVHlwZSA9IG51bGw7XG4gICAgICBfLnZpc2liaWxpdHlDaGFuZ2UgPSAndmlzaWJpbGl0eWNoYW5nZSc7XG4gICAgICBfLndpbmRvd1dpZHRoID0gMDtcbiAgICAgIF8ud2luZG93VGltZXIgPSBudWxsO1xuICAgICAgZGF0YVNldHRpbmdzID0gJChlbGVtZW50KS5kYXRhKCdzbGljaycpIHx8IHt9O1xuICAgICAgXy5vcHRpb25zID0gJC5leHRlbmQoe30sIF8uZGVmYXVsdHMsIHNldHRpbmdzLCBkYXRhU2V0dGluZ3MpO1xuICAgICAgXy5jdXJyZW50U2xpZGUgPSBfLm9wdGlvbnMuaW5pdGlhbFNsaWRlO1xuICAgICAgXy5vcmlnaW5hbFNldHRpbmdzID0gXy5vcHRpb25zO1xuXG4gICAgICBpZiAodHlwZW9mIGRvY3VtZW50Lm1vekhpZGRlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgXy5oaWRkZW4gPSAnbW96SGlkZGVuJztcbiAgICAgICAgXy52aXNpYmlsaXR5Q2hhbmdlID0gJ21venZpc2liaWxpdHljaGFuZ2UnO1xuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZG9jdW1lbnQud2Via2l0SGlkZGVuICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBfLmhpZGRlbiA9ICd3ZWJraXRIaWRkZW4nO1xuICAgICAgICBfLnZpc2liaWxpdHlDaGFuZ2UgPSAnd2Via2l0dmlzaWJpbGl0eWNoYW5nZSc7XG4gICAgICB9XG5cbiAgICAgIF8uYXV0b1BsYXkgPSAkLnByb3h5KF8uYXV0b1BsYXksIF8pO1xuICAgICAgXy5hdXRvUGxheUNsZWFyID0gJC5wcm94eShfLmF1dG9QbGF5Q2xlYXIsIF8pO1xuICAgICAgXy5hdXRvUGxheUl0ZXJhdG9yID0gJC5wcm94eShfLmF1dG9QbGF5SXRlcmF0b3IsIF8pO1xuICAgICAgXy5jaGFuZ2VTbGlkZSA9ICQucHJveHkoXy5jaGFuZ2VTbGlkZSwgXyk7XG4gICAgICBfLmNsaWNrSGFuZGxlciA9ICQucHJveHkoXy5jbGlja0hhbmRsZXIsIF8pO1xuICAgICAgXy5zZWxlY3RIYW5kbGVyID0gJC5wcm94eShfLnNlbGVjdEhhbmRsZXIsIF8pO1xuICAgICAgXy5zZXRQb3NpdGlvbiA9ICQucHJveHkoXy5zZXRQb3NpdGlvbiwgXyk7XG4gICAgICBfLnN3aXBlSGFuZGxlciA9ICQucHJveHkoXy5zd2lwZUhhbmRsZXIsIF8pO1xuICAgICAgXy5kcmFnSGFuZGxlciA9ICQucHJveHkoXy5kcmFnSGFuZGxlciwgXyk7XG4gICAgICBfLmtleUhhbmRsZXIgPSAkLnByb3h5KF8ua2V5SGFuZGxlciwgXyk7XG4gICAgICBfLmluc3RhbmNlVWlkID0gaW5zdGFuY2VVaWQrKzsgLy8gQSBzaW1wbGUgd2F5IHRvIGNoZWNrIGZvciBIVE1MIHN0cmluZ3NcbiAgICAgIC8vIFN0cmljdCBIVE1MIHJlY29nbml0aW9uIChtdXN0IHN0YXJ0IHdpdGggPClcbiAgICAgIC8vIEV4dHJhY3RlZCBmcm9tIGpRdWVyeSB2MS4xMSBzb3VyY2VcblxuICAgICAgXy5odG1sRXhwciA9IC9eKD86XFxzKig8W1xcd1xcV10rPilbXj5dKikkLztcblxuICAgICAgXy5yZWdpc3RlckJyZWFrcG9pbnRzKCk7XG5cbiAgICAgIF8uaW5pdCh0cnVlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gU2xpY2s7XG4gIH0oKTtcblxuICBTbGljay5wcm90b3R5cGUuYWN0aXZhdGVBREEgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stYWN0aXZlJykuYXR0cih7XG4gICAgICAnYXJpYS1oaWRkZW4nOiAnZmFsc2UnXG4gICAgfSkuZmluZCgnYSwgaW5wdXQsIGJ1dHRvbiwgc2VsZWN0JykuYXR0cih7XG4gICAgICAndGFiaW5kZXgnOiAnMCdcbiAgICB9KTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuYWRkU2xpZGUgPSBTbGljay5wcm90b3R5cGUuc2xpY2tBZGQgPSBmdW5jdGlvbiAobWFya3VwLCBpbmRleCwgYWRkQmVmb3JlKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKHR5cGVvZiBpbmRleCA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICBhZGRCZWZvcmUgPSBpbmRleDtcbiAgICAgIGluZGV4ID0gbnVsbDtcbiAgICB9IGVsc2UgaWYgKGluZGV4IDwgMCB8fCBpbmRleCA+PSBfLnNsaWRlQ291bnQpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBfLnVubG9hZCgpO1xuXG4gICAgaWYgKHR5cGVvZiBpbmRleCA9PT0gJ251bWJlcicpIHtcbiAgICAgIGlmIChpbmRleCA9PT0gMCAmJiBfLiRzbGlkZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICQobWFya3VwKS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcbiAgICAgIH0gZWxzZSBpZiAoYWRkQmVmb3JlKSB7XG4gICAgICAgICQobWFya3VwKS5pbnNlcnRCZWZvcmUoXy4kc2xpZGVzLmVxKGluZGV4KSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKG1hcmt1cCkuaW5zZXJ0QWZ0ZXIoXy4kc2xpZGVzLmVxKGluZGV4KSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChhZGRCZWZvcmUgPT09IHRydWUpIHtcbiAgICAgICAgJChtYXJrdXApLnByZXBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQobWFya3VwKS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfLiRzbGlkZXMgPSBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSk7XG5cbiAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSkuZGV0YWNoKCk7XG5cbiAgICBfLiRzbGlkZVRyYWNrLmFwcGVuZChfLiRzbGlkZXMpO1xuXG4gICAgXy4kc2xpZGVzLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XG4gICAgICAkKGVsZW1lbnQpLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnLCBpbmRleCk7XG4gICAgfSk7XG5cbiAgICBfLiRzbGlkZXNDYWNoZSA9IF8uJHNsaWRlcztcblxuICAgIF8ucmVpbml0KCk7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmFuaW1hdGVIZWlnaHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPT09IDEgJiYgXy5vcHRpb25zLmFkYXB0aXZlSGVpZ2h0ID09PSB0cnVlICYmIF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcbiAgICAgIHZhciB0YXJnZXRIZWlnaHQgPSBfLiRzbGlkZXMuZXEoXy5jdXJyZW50U2xpZGUpLm91dGVySGVpZ2h0KHRydWUpO1xuXG4gICAgICBfLiRsaXN0LmFuaW1hdGUoe1xuICAgICAgICBoZWlnaHQ6IHRhcmdldEhlaWdodFxuICAgICAgfSwgXy5vcHRpb25zLnNwZWVkKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmFuaW1hdGVTbGlkZSA9IGZ1bmN0aW9uICh0YXJnZXRMZWZ0LCBjYWxsYmFjaykge1xuICAgIHZhciBhbmltUHJvcHMgPSB7fSxcbiAgICAgICAgXyA9IHRoaXM7XG5cbiAgICBfLmFuaW1hdGVIZWlnaHQoKTtcblxuICAgIGlmIChfLm9wdGlvbnMucnRsID09PSB0cnVlICYmIF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcbiAgICAgIHRhcmdldExlZnQgPSAtdGFyZ2V0TGVmdDtcbiAgICB9XG5cbiAgICBpZiAoXy50cmFuc2Zvcm1zRW5hYmxlZCA9PT0gZmFsc2UpIHtcbiAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XG4gICAgICAgIF8uJHNsaWRlVHJhY2suYW5pbWF0ZSh7XG4gICAgICAgICAgbGVmdDogdGFyZ2V0TGVmdFxuICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQsIF8ub3B0aW9ucy5lYXNpbmcsIGNhbGxiYWNrKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIF8uJHNsaWRlVHJhY2suYW5pbWF0ZSh7XG4gICAgICAgICAgdG9wOiB0YXJnZXRMZWZ0XG4gICAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCwgXy5vcHRpb25zLmVhc2luZywgY2FsbGJhY2spO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcbiAgICAgICAgICBfLmN1cnJlbnRMZWZ0ID0gLV8uY3VycmVudExlZnQ7XG4gICAgICAgIH1cblxuICAgICAgICAkKHtcbiAgICAgICAgICBhbmltU3RhcnQ6IF8uY3VycmVudExlZnRcbiAgICAgICAgfSkuYW5pbWF0ZSh7XG4gICAgICAgICAgYW5pbVN0YXJ0OiB0YXJnZXRMZWZ0XG4gICAgICAgIH0sIHtcbiAgICAgICAgICBkdXJhdGlvbjogXy5vcHRpb25zLnNwZWVkLFxuICAgICAgICAgIGVhc2luZzogXy5vcHRpb25zLmVhc2luZyxcbiAgICAgICAgICBzdGVwOiBmdW5jdGlvbiAobm93KSB7XG4gICAgICAgICAgICBub3cgPSBNYXRoLmNlaWwobm93KTtcblxuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZSgnICsgbm93ICsgJ3B4LCAwcHgpJztcblxuICAgICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhhbmltUHJvcHMpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZSgwcHgsJyArIG5vdyArICdweCknO1xuXG4gICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKGFuaW1Qcm9wcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgIGNhbGxiYWNrLmNhbGwoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgXy5hcHBseVRyYW5zaXRpb24oKTtcblxuICAgICAgICB0YXJnZXRMZWZ0ID0gTWF0aC5jZWlsKHRhcmdldExlZnQpO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XG4gICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZTNkKCcgKyB0YXJnZXRMZWZ0ICsgJ3B4LCAwcHgsIDBweCknO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGFuaW1Qcm9wc1tfLmFuaW1UeXBlXSA9ICd0cmFuc2xhdGUzZCgwcHgsJyArIHRhcmdldExlZnQgKyAncHgsIDBweCknO1xuICAgICAgICB9XG5cbiAgICAgICAgXy4kc2xpZGVUcmFjay5jc3MoYW5pbVByb3BzKTtcblxuICAgICAgICBpZiAoY2FsbGJhY2spIHtcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIF8uZGlzYWJsZVRyYW5zaXRpb24oKTtcblxuICAgICAgICAgICAgY2FsbGJhY2suY2FsbCgpO1xuICAgICAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmdldE5hdlRhcmdldCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGFzTmF2Rm9yID0gXy5vcHRpb25zLmFzTmF2Rm9yO1xuXG4gICAgaWYgKGFzTmF2Rm9yICYmIGFzTmF2Rm9yICE9PSBudWxsKSB7XG4gICAgICBhc05hdkZvciA9ICQoYXNOYXZGb3IpLm5vdChfLiRzbGlkZXIpO1xuICAgIH1cblxuICAgIHJldHVybiBhc05hdkZvcjtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuYXNOYXZGb3IgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGFzTmF2Rm9yID0gXy5nZXROYXZUYXJnZXQoKTtcblxuICAgIGlmIChhc05hdkZvciAhPT0gbnVsbCAmJiB0eXBlb2YgYXNOYXZGb3IgPT09ICdvYmplY3QnKSB7XG4gICAgICBhc05hdkZvci5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHRhcmdldCA9ICQodGhpcykuc2xpY2soJ2dldFNsaWNrJyk7XG5cbiAgICAgICAgaWYgKCF0YXJnZXQudW5zbGlja2VkKSB7XG4gICAgICAgICAgdGFyZ2V0LnNsaWRlSGFuZGxlcihpbmRleCwgdHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuYXBwbHlUcmFuc2l0aW9uID0gZnVuY3Rpb24gKHNsaWRlKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICB0cmFuc2l0aW9uID0ge307XG5cbiAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IGZhbHNlKSB7XG4gICAgICB0cmFuc2l0aW9uW18udHJhbnNpdGlvblR5cGVdID0gXy50cmFuc2Zvcm1UeXBlICsgJyAnICsgXy5vcHRpb25zLnNwZWVkICsgJ21zICcgKyBfLm9wdGlvbnMuY3NzRWFzZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdHJhbnNpdGlvbltfLnRyYW5zaXRpb25UeXBlXSA9ICdvcGFjaXR5ICcgKyBfLm9wdGlvbnMuc3BlZWQgKyAnbXMgJyArIF8ub3B0aW9ucy5jc3NFYXNlO1xuICAgIH1cblxuICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcbiAgICAgIF8uJHNsaWRlVHJhY2suY3NzKHRyYW5zaXRpb24pO1xuICAgIH0gZWxzZSB7XG4gICAgICBfLiRzbGlkZXMuZXEoc2xpZGUpLmNzcyh0cmFuc2l0aW9uKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmF1dG9QbGF5ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIF8uYXV0b1BsYXlDbGVhcigpO1xuXG4gICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uYXV0b1BsYXlUaW1lciA9IHNldEludGVydmFsKF8uYXV0b1BsYXlJdGVyYXRvciwgXy5vcHRpb25zLmF1dG9wbGF5U3BlZWQpO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuYXV0b1BsYXlDbGVhciA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5hdXRvUGxheVRpbWVyKSB7XG4gICAgICBjbGVhckludGVydmFsKF8uYXV0b1BsYXlUaW1lcik7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5hdXRvUGxheUl0ZXJhdG9yID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgc2xpZGVUbyA9IF8uY3VycmVudFNsaWRlICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuXG4gICAgaWYgKCFfLnBhdXNlZCAmJiAhXy5pbnRlcnJ1cHRlZCAmJiAhXy5mb2N1c3NlZCkge1xuICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgaWYgKF8uZGlyZWN0aW9uID09PSAxICYmIF8uY3VycmVudFNsaWRlICsgMSA9PT0gXy5zbGlkZUNvdW50IC0gMSkge1xuICAgICAgICAgIF8uZGlyZWN0aW9uID0gMDtcbiAgICAgICAgfSBlbHNlIGlmIChfLmRpcmVjdGlvbiA9PT0gMCkge1xuICAgICAgICAgIHNsaWRlVG8gPSBfLmN1cnJlbnRTbGlkZSAtIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcblxuICAgICAgICAgIGlmIChfLmN1cnJlbnRTbGlkZSAtIDEgPT09IDApIHtcbiAgICAgICAgICAgIF8uZGlyZWN0aW9uID0gMTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgXy5zbGlkZUhhbmRsZXIoc2xpZGVUbyk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5idWlsZEFycm93cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5vcHRpb25zLmFycm93cyA9PT0gdHJ1ZSkge1xuICAgICAgXy4kcHJldkFycm93ID0gJChfLm9wdGlvbnMucHJldkFycm93KS5hZGRDbGFzcygnc2xpY2stYXJyb3cnKTtcbiAgICAgIF8uJG5leHRBcnJvdyA9ICQoXy5vcHRpb25zLm5leHRBcnJvdykuYWRkQ2xhc3MoJ3NsaWNrLWFycm93Jyk7XG5cbiAgICAgIGlmIChfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2staGlkZGVuJykucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gdGFiaW5kZXgnKTtcblxuICAgICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWhpZGRlbicpLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuIHRhYmluZGV4Jyk7XG5cbiAgICAgICAgaWYgKF8uaHRtbEV4cHIudGVzdChfLm9wdGlvbnMucHJldkFycm93KSkge1xuICAgICAgICAgIF8uJHByZXZBcnJvdy5wcmVwZW5kVG8oXy5vcHRpb25zLmFwcGVuZEFycm93cyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5uZXh0QXJyb3cpKSB7XG4gICAgICAgICAgXy4kbmV4dEFycm93LmFwcGVuZFRvKF8ub3B0aW9ucy5hcHBlbmRBcnJvd3MpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSAhPT0gdHJ1ZSkge1xuICAgICAgICAgIF8uJHByZXZBcnJvdy5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgXy4kcHJldkFycm93LmFkZChfLiRuZXh0QXJyb3cpLmFkZENsYXNzKCdzbGljay1oaWRkZW4nKS5hdHRyKHtcbiAgICAgICAgICAnYXJpYS1kaXNhYmxlZCc6ICd0cnVlJyxcbiAgICAgICAgICAndGFiaW5kZXgnOiAnLTEnXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuYnVpbGREb3RzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgaSxcbiAgICAgICAgZG90O1xuXG4gICAgaWYgKF8ub3B0aW9ucy5kb3RzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uJHNsaWRlci5hZGRDbGFzcygnc2xpY2stZG90dGVkJyk7XG5cbiAgICAgIGRvdCA9ICQoJzx1bCAvPicpLmFkZENsYXNzKF8ub3B0aW9ucy5kb3RzQ2xhc3MpO1xuXG4gICAgICBmb3IgKGkgPSAwOyBpIDw9IF8uZ2V0RG90Q291bnQoKTsgaSArPSAxKSB7XG4gICAgICAgIGRvdC5hcHBlbmQoJCgnPGxpIC8+JykuYXBwZW5kKF8ub3B0aW9ucy5jdXN0b21QYWdpbmcuY2FsbCh0aGlzLCBfLCBpKSkpO1xuICAgICAgfVxuXG4gICAgICBfLiRkb3RzID0gZG90LmFwcGVuZFRvKF8ub3B0aW9ucy5hcHBlbmREb3RzKTtcblxuICAgICAgXy4kZG90cy5maW5kKCdsaScpLmZpcnN0KCkuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5idWlsZE91dCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBfLiRzbGlkZXMgPSBfLiRzbGlkZXIuY2hpbGRyZW4oXy5vcHRpb25zLnNsaWRlICsgJzpub3QoLnNsaWNrLWNsb25lZCknKS5hZGRDbGFzcygnc2xpY2stc2xpZGUnKTtcbiAgICBfLnNsaWRlQ291bnQgPSBfLiRzbGlkZXMubGVuZ3RoO1xuXG4gICAgXy4kc2xpZGVzLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XG4gICAgICAkKGVsZW1lbnQpLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnLCBpbmRleCkuZGF0YSgnb3JpZ2luYWxTdHlsaW5nJywgJChlbGVtZW50KS5hdHRyKCdzdHlsZScpIHx8ICcnKTtcbiAgICB9KTtcblxuICAgIF8uJHNsaWRlci5hZGRDbGFzcygnc2xpY2stc2xpZGVyJyk7XG5cbiAgICBfLiRzbGlkZVRyYWNrID0gXy5zbGlkZUNvdW50ID09PSAwID8gJCgnPGRpdiBjbGFzcz1cInNsaWNrLXRyYWNrXCIvPicpLmFwcGVuZFRvKF8uJHNsaWRlcikgOiBfLiRzbGlkZXMud3JhcEFsbCgnPGRpdiBjbGFzcz1cInNsaWNrLXRyYWNrXCIvPicpLnBhcmVudCgpO1xuICAgIF8uJGxpc3QgPSBfLiRzbGlkZVRyYWNrLndyYXAoJzxkaXYgYXJpYS1saXZlPVwicG9saXRlXCIgY2xhc3M9XCJzbGljay1saXN0XCIvPicpLnBhcmVudCgpO1xuXG4gICAgXy4kc2xpZGVUcmFjay5jc3MoJ29wYWNpdHknLCAwKTtcblxuICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSB8fCBfLm9wdGlvbnMuc3dpcGVUb1NsaWRlID09PSB0cnVlKSB7XG4gICAgICBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgPSAxO1xuICAgIH1cblxuICAgICQoJ2ltZ1tkYXRhLWxhenldJywgXy4kc2xpZGVyKS5ub3QoJ1tzcmNdJykuYWRkQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcblxuICAgIF8uc2V0dXBJbmZpbml0ZSgpO1xuXG4gICAgXy5idWlsZEFycm93cygpO1xuXG4gICAgXy5idWlsZERvdHMoKTtcblxuICAgIF8udXBkYXRlRG90cygpO1xuXG4gICAgXy5zZXRTbGlkZUNsYXNzZXModHlwZW9mIF8uY3VycmVudFNsaWRlID09PSAnbnVtYmVyJyA/IF8uY3VycmVudFNsaWRlIDogMCk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmRyYWdnYWJsZSA9PT0gdHJ1ZSkge1xuICAgICAgXy4kbGlzdC5hZGRDbGFzcygnZHJhZ2dhYmxlJyk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5idWlsZFJvd3MgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBhLFxuICAgICAgICBiLFxuICAgICAgICBjLFxuICAgICAgICBuZXdTbGlkZXMsXG4gICAgICAgIG51bU9mU2xpZGVzLFxuICAgICAgICBvcmlnaW5hbFNsaWRlcyxcbiAgICAgICAgc2xpZGVzUGVyU2VjdGlvbjtcblxuICAgIG5ld1NsaWRlcyA9IGRvY3VtZW50LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKTtcbiAgICBvcmlnaW5hbFNsaWRlcyA9IF8uJHNsaWRlci5jaGlsZHJlbigpO1xuXG4gICAgaWYgKF8ub3B0aW9ucy5yb3dzID4gMSkge1xuICAgICAgc2xpZGVzUGVyU2VjdGlvbiA9IF8ub3B0aW9ucy5zbGlkZXNQZXJSb3cgKiBfLm9wdGlvbnMucm93cztcbiAgICAgIG51bU9mU2xpZGVzID0gTWF0aC5jZWlsKG9yaWdpbmFsU2xpZGVzLmxlbmd0aCAvIHNsaWRlc1BlclNlY3Rpb24pO1xuXG4gICAgICBmb3IgKGEgPSAwOyBhIDwgbnVtT2ZTbGlkZXM7IGErKykge1xuICAgICAgICB2YXIgc2xpZGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblxuICAgICAgICBmb3IgKGIgPSAwOyBiIDwgXy5vcHRpb25zLnJvd3M7IGIrKykge1xuICAgICAgICAgIHZhciByb3cgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblxuICAgICAgICAgIGZvciAoYyA9IDA7IGMgPCBfLm9wdGlvbnMuc2xpZGVzUGVyUm93OyBjKyspIHtcbiAgICAgICAgICAgIHZhciB0YXJnZXQgPSBhICogc2xpZGVzUGVyU2VjdGlvbiArIChiICogXy5vcHRpb25zLnNsaWRlc1BlclJvdyArIGMpO1xuXG4gICAgICAgICAgICBpZiAob3JpZ2luYWxTbGlkZXMuZ2V0KHRhcmdldCkpIHtcbiAgICAgICAgICAgICAgcm93LmFwcGVuZENoaWxkKG9yaWdpbmFsU2xpZGVzLmdldCh0YXJnZXQpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICBzbGlkZS5hcHBlbmRDaGlsZChyb3cpO1xuICAgICAgICB9XG5cbiAgICAgICAgbmV3U2xpZGVzLmFwcGVuZENoaWxkKHNsaWRlKTtcbiAgICAgIH1cblxuICAgICAgXy4kc2xpZGVyLmVtcHR5KCkuYXBwZW5kKG5ld1NsaWRlcyk7XG5cbiAgICAgIF8uJHNsaWRlci5jaGlsZHJlbigpLmNoaWxkcmVuKCkuY2hpbGRyZW4oKS5jc3Moe1xuICAgICAgICAnd2lkdGgnOiAxMDAgLyBfLm9wdGlvbnMuc2xpZGVzUGVyUm93ICsgJyUnLFxuICAgICAgICAnZGlzcGxheSc6ICdpbmxpbmUtYmxvY2snXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmNoZWNrUmVzcG9uc2l2ZSA9IGZ1bmN0aW9uIChpbml0aWFsLCBmb3JjZVVwZGF0ZSkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgYnJlYWtwb2ludCxcbiAgICAgICAgdGFyZ2V0QnJlYWtwb2ludCxcbiAgICAgICAgcmVzcG9uZFRvV2lkdGgsXG4gICAgICAgIHRyaWdnZXJCcmVha3BvaW50ID0gZmFsc2U7XG5cbiAgICB2YXIgc2xpZGVyV2lkdGggPSBfLiRzbGlkZXIud2lkdGgoKTtcblxuICAgIHZhciB3aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoIHx8ICQod2luZG93KS53aWR0aCgpO1xuXG4gICAgaWYgKF8ucmVzcG9uZFRvID09PSAnd2luZG93Jykge1xuICAgICAgcmVzcG9uZFRvV2lkdGggPSB3aW5kb3dXaWR0aDtcbiAgICB9IGVsc2UgaWYgKF8ucmVzcG9uZFRvID09PSAnc2xpZGVyJykge1xuICAgICAgcmVzcG9uZFRvV2lkdGggPSBzbGlkZXJXaWR0aDtcbiAgICB9IGVsc2UgaWYgKF8ucmVzcG9uZFRvID09PSAnbWluJykge1xuICAgICAgcmVzcG9uZFRvV2lkdGggPSBNYXRoLm1pbih3aW5kb3dXaWR0aCwgc2xpZGVyV2lkdGgpO1xuICAgIH1cblxuICAgIGlmIChfLm9wdGlvbnMucmVzcG9uc2l2ZSAmJiBfLm9wdGlvbnMucmVzcG9uc2l2ZS5sZW5ndGggJiYgXy5vcHRpb25zLnJlc3BvbnNpdmUgIT09IG51bGwpIHtcbiAgICAgIHRhcmdldEJyZWFrcG9pbnQgPSBudWxsO1xuXG4gICAgICBmb3IgKGJyZWFrcG9pbnQgaW4gXy5icmVha3BvaW50cykge1xuICAgICAgICBpZiAoXy5icmVha3BvaW50cy5oYXNPd25Qcm9wZXJ0eShicmVha3BvaW50KSkge1xuICAgICAgICAgIGlmIChfLm9yaWdpbmFsU2V0dGluZ3MubW9iaWxlRmlyc3QgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICBpZiAocmVzcG9uZFRvV2lkdGggPCBfLmJyZWFrcG9pbnRzW2JyZWFrcG9pbnRdKSB7XG4gICAgICAgICAgICAgIHRhcmdldEJyZWFrcG9pbnQgPSBfLmJyZWFrcG9pbnRzW2JyZWFrcG9pbnRdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAocmVzcG9uZFRvV2lkdGggPiBfLmJyZWFrcG9pbnRzW2JyZWFrcG9pbnRdKSB7XG4gICAgICAgICAgICAgIHRhcmdldEJyZWFrcG9pbnQgPSBfLmJyZWFrcG9pbnRzW2JyZWFrcG9pbnRdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAodGFyZ2V0QnJlYWtwb2ludCAhPT0gbnVsbCkge1xuICAgICAgICBpZiAoXy5hY3RpdmVCcmVha3BvaW50ICE9PSBudWxsKSB7XG4gICAgICAgICAgaWYgKHRhcmdldEJyZWFrcG9pbnQgIT09IF8uYWN0aXZlQnJlYWtwb2ludCB8fCBmb3JjZVVwZGF0ZSkge1xuICAgICAgICAgICAgXy5hY3RpdmVCcmVha3BvaW50ID0gdGFyZ2V0QnJlYWtwb2ludDtcblxuICAgICAgICAgICAgaWYgKF8uYnJlYWtwb2ludFNldHRpbmdzW3RhcmdldEJyZWFrcG9pbnRdID09PSAndW5zbGljaycpIHtcbiAgICAgICAgICAgICAgXy51bnNsaWNrKHRhcmdldEJyZWFrcG9pbnQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgXy5vcHRpb25zID0gJC5leHRlbmQoe30sIF8ub3JpZ2luYWxTZXR0aW5ncywgXy5icmVha3BvaW50U2V0dGluZ3NbdGFyZ2V0QnJlYWtwb2ludF0pO1xuXG4gICAgICAgICAgICAgIGlmIChpbml0aWFsID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgXy5jdXJyZW50U2xpZGUgPSBfLm9wdGlvbnMuaW5pdGlhbFNsaWRlO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgXy5yZWZyZXNoKGluaXRpYWwpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0cmlnZ2VyQnJlYWtwb2ludCA9IHRhcmdldEJyZWFrcG9pbnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF8uYWN0aXZlQnJlYWtwb2ludCA9IHRhcmdldEJyZWFrcG9pbnQ7XG5cbiAgICAgICAgICBpZiAoXy5icmVha3BvaW50U2V0dGluZ3NbdGFyZ2V0QnJlYWtwb2ludF0gPT09ICd1bnNsaWNrJykge1xuICAgICAgICAgICAgXy51bnNsaWNrKHRhcmdldEJyZWFrcG9pbnQpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgXy5vcmlnaW5hbFNldHRpbmdzLCBfLmJyZWFrcG9pbnRTZXR0aW5nc1t0YXJnZXRCcmVha3BvaW50XSk7XG5cbiAgICAgICAgICAgIGlmIChpbml0aWFsID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gXy5vcHRpb25zLmluaXRpYWxTbGlkZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgXy5yZWZyZXNoKGluaXRpYWwpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHRyaWdnZXJCcmVha3BvaW50ID0gdGFyZ2V0QnJlYWtwb2ludDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKF8uYWN0aXZlQnJlYWtwb2ludCAhPT0gbnVsbCkge1xuICAgICAgICAgIF8uYWN0aXZlQnJlYWtwb2ludCA9IG51bGw7XG4gICAgICAgICAgXy5vcHRpb25zID0gXy5vcmlnaW5hbFNldHRpbmdzO1xuXG4gICAgICAgICAgaWYgKGluaXRpYWwgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gXy5vcHRpb25zLmluaXRpYWxTbGlkZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBfLnJlZnJlc2goaW5pdGlhbCk7XG5cbiAgICAgICAgICB0cmlnZ2VyQnJlYWtwb2ludCA9IHRhcmdldEJyZWFrcG9pbnQ7XG4gICAgICAgIH1cbiAgICAgIH0gLy8gb25seSB0cmlnZ2VyIGJyZWFrcG9pbnRzIGR1cmluZyBhbiBhY3R1YWwgYnJlYWsuIG5vdCBvbiBpbml0aWFsaXplLlxuXG5cbiAgICAgIGlmICghaW5pdGlhbCAmJiB0cmlnZ2VyQnJlYWtwb2ludCAhPT0gZmFsc2UpIHtcbiAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2JyZWFrcG9pbnQnLCBbXywgdHJpZ2dlckJyZWFrcG9pbnRdKTtcbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmNoYW5nZVNsaWRlID0gZnVuY3Rpb24gKGV2ZW50LCBkb250QW5pbWF0ZSkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgJHRhcmdldCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCksXG4gICAgICAgIGluZGV4T2Zmc2V0LFxuICAgICAgICBzbGlkZU9mZnNldCxcbiAgICAgICAgdW5ldmVuT2Zmc2V0OyAvLyBJZiB0YXJnZXQgaXMgYSBsaW5rLCBwcmV2ZW50IGRlZmF1bHQgYWN0aW9uLlxuXG5cbiAgICBpZiAoJHRhcmdldC5pcygnYScpKSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0gLy8gSWYgdGFyZ2V0IGlzIG5vdCB0aGUgPGxpPiBlbGVtZW50IChpZTogYSBjaGlsZCksIGZpbmQgdGhlIDxsaT4uXG5cblxuICAgIGlmICghJHRhcmdldC5pcygnbGknKSkge1xuICAgICAgJHRhcmdldCA9ICR0YXJnZXQuY2xvc2VzdCgnbGknKTtcbiAgICB9XG5cbiAgICB1bmV2ZW5PZmZzZXQgPSBfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgIT09IDA7XG4gICAgaW5kZXhPZmZzZXQgPSB1bmV2ZW5PZmZzZXQgPyAwIDogKF8uc2xpZGVDb3VudCAtIF8uY3VycmVudFNsaWRlKSAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcblxuICAgIHN3aXRjaCAoZXZlbnQuZGF0YS5tZXNzYWdlKSB7XG4gICAgICBjYXNlICdwcmV2aW91cyc6XG4gICAgICAgIHNsaWRlT2Zmc2V0ID0gaW5kZXhPZmZzZXQgPT09IDAgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gaW5kZXhPZmZzZXQ7XG5cbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICBfLnNsaWRlSGFuZGxlcihfLmN1cnJlbnRTbGlkZSAtIHNsaWRlT2Zmc2V0LCBmYWxzZSwgZG9udEFuaW1hdGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJ25leHQnOlxuICAgICAgICBzbGlkZU9mZnNldCA9IGluZGV4T2Zmc2V0ID09PSAwID8gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDogaW5kZXhPZmZzZXQ7XG5cbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICBfLnNsaWRlSGFuZGxlcihfLmN1cnJlbnRTbGlkZSArIHNsaWRlT2Zmc2V0LCBmYWxzZSwgZG9udEFuaW1hdGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJ2luZGV4JzpcbiAgICAgICAgdmFyIGluZGV4ID0gZXZlbnQuZGF0YS5pbmRleCA9PT0gMCA/IDAgOiBldmVudC5kYXRhLmluZGV4IHx8ICR0YXJnZXQuaW5kZXgoKSAqIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcblxuICAgICAgICBfLnNsaWRlSGFuZGxlcihfLmNoZWNrTmF2aWdhYmxlKGluZGV4KSwgZmFsc2UsIGRvbnRBbmltYXRlKTtcblxuICAgICAgICAkdGFyZ2V0LmNoaWxkcmVuKCkudHJpZ2dlcignZm9jdXMnKTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmNoZWNrTmF2aWdhYmxlID0gZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBuYXZpZ2FibGVzLFxuICAgICAgICBwcmV2TmF2aWdhYmxlO1xuXG4gICAgbmF2aWdhYmxlcyA9IF8uZ2V0TmF2aWdhYmxlSW5kZXhlcygpO1xuICAgIHByZXZOYXZpZ2FibGUgPSAwO1xuXG4gICAgaWYgKGluZGV4ID4gbmF2aWdhYmxlc1tuYXZpZ2FibGVzLmxlbmd0aCAtIDFdKSB7XG4gICAgICBpbmRleCA9IG5hdmlnYWJsZXNbbmF2aWdhYmxlcy5sZW5ndGggLSAxXTtcbiAgICB9IGVsc2Uge1xuICAgICAgZm9yICh2YXIgbiBpbiBuYXZpZ2FibGVzKSB7XG4gICAgICAgIGlmIChpbmRleCA8IG5hdmlnYWJsZXNbbl0pIHtcbiAgICAgICAgICBpbmRleCA9IHByZXZOYXZpZ2FibGU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICAgICBwcmV2TmF2aWdhYmxlID0gbmF2aWdhYmxlc1tuXTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gaW5kZXg7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmNsZWFuVXBFdmVudHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKF8ub3B0aW9ucy5kb3RzICYmIF8uJGRvdHMgIT09IG51bGwpIHtcbiAgICAgICQoJ2xpJywgXy4kZG90cykub2ZmKCdjbGljay5zbGljaycsIF8uY2hhbmdlU2xpZGUpLm9mZignbW91c2VlbnRlci5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIHRydWUpKS5vZmYoJ21vdXNlbGVhdmUuc2xpY2snLCAkLnByb3h5KF8uaW50ZXJydXB0LCBfLCBmYWxzZSkpO1xuICAgIH1cblxuICAgIF8uJHNsaWRlci5vZmYoJ2ZvY3VzLnNsaWNrIGJsdXIuc2xpY2snKTtcblxuICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uJHByZXZBcnJvdyAmJiBfLiRwcmV2QXJyb3cub2ZmKCdjbGljay5zbGljaycsIF8uY2hhbmdlU2xpZGUpO1xuICAgICAgXy4kbmV4dEFycm93ICYmIF8uJG5leHRBcnJvdy5vZmYoJ2NsaWNrLnNsaWNrJywgXy5jaGFuZ2VTbGlkZSk7XG4gICAgfVxuXG4gICAgXy4kbGlzdC5vZmYoJ3RvdWNoc3RhcnQuc2xpY2sgbW91c2Vkb3duLnNsaWNrJywgXy5zd2lwZUhhbmRsZXIpO1xuXG4gICAgXy4kbGlzdC5vZmYoJ3RvdWNobW92ZS5zbGljayBtb3VzZW1vdmUuc2xpY2snLCBfLnN3aXBlSGFuZGxlcik7XG5cbiAgICBfLiRsaXN0Lm9mZigndG91Y2hlbmQuc2xpY2sgbW91c2V1cC5zbGljaycsIF8uc3dpcGVIYW5kbGVyKTtcblxuICAgIF8uJGxpc3Qub2ZmKCd0b3VjaGNhbmNlbC5zbGljayBtb3VzZWxlYXZlLnNsaWNrJywgXy5zd2lwZUhhbmRsZXIpO1xuXG4gICAgXy4kbGlzdC5vZmYoJ2NsaWNrLnNsaWNrJywgXy5jbGlja0hhbmRsZXIpO1xuXG4gICAgJChkb2N1bWVudCkub2ZmKF8udmlzaWJpbGl0eUNoYW5nZSwgXy52aXNpYmlsaXR5KTtcblxuICAgIF8uY2xlYW5VcFNsaWRlRXZlbnRzKCk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcbiAgICAgIF8uJGxpc3Qub2ZmKCdrZXlkb3duLnNsaWNrJywgXy5rZXlIYW5kbGVyKTtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmZvY3VzT25TZWxlY3QgPT09IHRydWUpIHtcbiAgICAgICQoXy4kc2xpZGVUcmFjaykuY2hpbGRyZW4oKS5vZmYoJ2NsaWNrLnNsaWNrJywgXy5zZWxlY3RIYW5kbGVyKTtcbiAgICB9XG5cbiAgICAkKHdpbmRvdykub2ZmKCdvcmllbnRhdGlvbmNoYW5nZS5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgXy5vcmllbnRhdGlvbkNoYW5nZSk7XG4gICAgJCh3aW5kb3cpLm9mZigncmVzaXplLnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCBfLnJlc2l6ZSk7XG4gICAgJCgnW2RyYWdnYWJsZSE9dHJ1ZV0nLCBfLiRzbGlkZVRyYWNrKS5vZmYoJ2RyYWdzdGFydCcsIF8ucHJldmVudERlZmF1bHQpO1xuICAgICQod2luZG93KS5vZmYoJ2xvYWQuc2xpY2suc2xpY2stJyArIF8uaW5zdGFuY2VVaWQsIF8uc2V0UG9zaXRpb24pO1xuICAgICQoZG9jdW1lbnQpLm9mZigncmVhZHkuc2xpY2suc2xpY2stJyArIF8uaW5zdGFuY2VVaWQsIF8uc2V0UG9zaXRpb24pO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5jbGVhblVwU2xpZGVFdmVudHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgXy4kbGlzdC5vZmYoJ21vdXNlZW50ZXIuc2xpY2snLCAkLnByb3h5KF8uaW50ZXJydXB0LCBfLCB0cnVlKSk7XG5cbiAgICBfLiRsaXN0Lm9mZignbW91c2VsZWF2ZS5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIGZhbHNlKSk7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmNsZWFuVXBSb3dzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgb3JpZ2luYWxTbGlkZXM7XG5cbiAgICBpZiAoXy5vcHRpb25zLnJvd3MgPiAxKSB7XG4gICAgICBvcmlnaW5hbFNsaWRlcyA9IF8uJHNsaWRlcy5jaGlsZHJlbigpLmNoaWxkcmVuKCk7XG4gICAgICBvcmlnaW5hbFNsaWRlcy5yZW1vdmVBdHRyKCdzdHlsZScpO1xuXG4gICAgICBfLiRzbGlkZXIuZW1wdHkoKS5hcHBlbmQob3JpZ2luYWxTbGlkZXMpO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuY2xpY2tIYW5kbGVyID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKF8uc2hvdWxkQ2xpY2sgPT09IGZhbHNlKSB7XG4gICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcbiAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAocmVmcmVzaCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIF8uYXV0b1BsYXlDbGVhcigpO1xuXG4gICAgXy50b3VjaE9iamVjdCA9IHt9O1xuXG4gICAgXy5jbGVhblVwRXZlbnRzKCk7XG5cbiAgICAkKCcuc2xpY2stY2xvbmVkJywgXy4kc2xpZGVyKS5kZXRhY2goKTtcblxuICAgIGlmIChfLiRkb3RzKSB7XG4gICAgICBfLiRkb3RzLnJlbW92ZSgpO1xuICAgIH1cblxuICAgIGlmIChfLiRwcmV2QXJyb3cgJiYgXy4kcHJldkFycm93Lmxlbmd0aCkge1xuICAgICAgXy4kcHJldkFycm93LnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCBzbGljay1hcnJvdyBzbGljay1oaWRkZW4nKS5yZW1vdmVBdHRyKCdhcmlhLWhpZGRlbiBhcmlhLWRpc2FibGVkIHRhYmluZGV4JykuY3NzKCdkaXNwbGF5JywgJycpO1xuXG4gICAgICBpZiAoXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5wcmV2QXJyb3cpKSB7XG4gICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmUoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoXy4kbmV4dEFycm93ICYmIF8uJG5leHRBcnJvdy5sZW5ndGgpIHtcbiAgICAgIF8uJG5leHRBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2stZGlzYWJsZWQgc2xpY2stYXJyb3cgc2xpY2staGlkZGVuJykucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gYXJpYS1kaXNhYmxlZCB0YWJpbmRleCcpLmNzcygnZGlzcGxheScsICcnKTtcblxuICAgICAgaWYgKF8uaHRtbEV4cHIudGVzdChfLm9wdGlvbnMubmV4dEFycm93KSkge1xuICAgICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlKCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKF8uJHNsaWRlcykge1xuICAgICAgXy4kc2xpZGVzLnJlbW92ZUNsYXNzKCdzbGljay1zbGlkZSBzbGljay1hY3RpdmUgc2xpY2stY2VudGVyIHNsaWNrLXZpc2libGUgc2xpY2stY3VycmVudCcpLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuJykucmVtb3ZlQXR0cignZGF0YS1zbGljay1pbmRleCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAkKHRoaXMpLmF0dHIoJ3N0eWxlJywgJCh0aGlzKS5kYXRhKCdvcmlnaW5hbFN0eWxpbmcnKSk7XG4gICAgICB9KTtcblxuICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xuXG4gICAgICBfLiRzbGlkZVRyYWNrLmRldGFjaCgpO1xuXG4gICAgICBfLiRsaXN0LmRldGFjaCgpO1xuXG4gICAgICBfLiRzbGlkZXIuYXBwZW5kKF8uJHNsaWRlcyk7XG4gICAgfVxuXG4gICAgXy5jbGVhblVwUm93cygpO1xuXG4gICAgXy4kc2xpZGVyLnJlbW92ZUNsYXNzKCdzbGljay1zbGlkZXInKTtcblxuICAgIF8uJHNsaWRlci5yZW1vdmVDbGFzcygnc2xpY2staW5pdGlhbGl6ZWQnKTtcblxuICAgIF8uJHNsaWRlci5yZW1vdmVDbGFzcygnc2xpY2stZG90dGVkJyk7XG5cbiAgICBfLnVuc2xpY2tlZCA9IHRydWU7XG5cbiAgICBpZiAoIXJlZnJlc2gpIHtcbiAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdkZXN0cm95JywgW19dKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmRpc2FibGVUcmFuc2l0aW9uID0gZnVuY3Rpb24gKHNsaWRlKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICB0cmFuc2l0aW9uID0ge307XG5cbiAgICB0cmFuc2l0aW9uW18udHJhbnNpdGlvblR5cGVdID0gJyc7XG5cbiAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IGZhbHNlKSB7XG4gICAgICBfLiRzbGlkZVRyYWNrLmNzcyh0cmFuc2l0aW9uKTtcbiAgICB9IGVsc2Uge1xuICAgICAgXy4kc2xpZGVzLmVxKHNsaWRlKS5jc3ModHJhbnNpdGlvbik7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5mYWRlU2xpZGUgPSBmdW5jdGlvbiAoc2xpZGVJbmRleCwgY2FsbGJhY2spIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcbiAgICAgIF8uJHNsaWRlcy5lcShzbGlkZUluZGV4KS5jc3Moe1xuICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXhcbiAgICAgIH0pO1xuXG4gICAgICBfLiRzbGlkZXMuZXEoc2xpZGVJbmRleCkuYW5pbWF0ZSh7XG4gICAgICAgIG9wYWNpdHk6IDFcbiAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCwgXy5vcHRpb25zLmVhc2luZywgY2FsbGJhY2spO1xuICAgIH0gZWxzZSB7XG4gICAgICBfLmFwcGx5VHJhbnNpdGlvbihzbGlkZUluZGV4KTtcblxuICAgICAgXy4kc2xpZGVzLmVxKHNsaWRlSW5kZXgpLmNzcyh7XG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogXy5vcHRpb25zLnpJbmRleFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBfLmRpc2FibGVUcmFuc2l0aW9uKHNsaWRlSW5kZXgpO1xuXG4gICAgICAgICAgY2FsbGJhY2suY2FsbCgpO1xuICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuZmFkZVNsaWRlT3V0ID0gZnVuY3Rpb24gKHNsaWRlSW5kZXgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcbiAgICAgIF8uJHNsaWRlcy5lcShzbGlkZUluZGV4KS5hbmltYXRlKHtcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4IC0gMlxuICAgICAgfSwgXy5vcHRpb25zLnNwZWVkLCBfLm9wdGlvbnMuZWFzaW5nKTtcbiAgICB9IGVsc2Uge1xuICAgICAgXy5hcHBseVRyYW5zaXRpb24oc2xpZGVJbmRleCk7XG5cbiAgICAgIF8uJHNsaWRlcy5lcShzbGlkZUluZGV4KS5jc3Moe1xuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAyXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmZpbHRlclNsaWRlcyA9IFNsaWNrLnByb3RvdHlwZS5zbGlja0ZpbHRlciA9IGZ1bmN0aW9uIChmaWx0ZXIpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoZmlsdGVyICE9PSBudWxsKSB7XG4gICAgICBfLiRzbGlkZXNDYWNoZSA9IF8uJHNsaWRlcztcblxuICAgICAgXy51bmxvYWQoKTtcblxuICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xuXG4gICAgICBfLiRzbGlkZXNDYWNoZS5maWx0ZXIoZmlsdGVyKS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcblxuICAgICAgXy5yZWluaXQoKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmZvY3VzSGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBfLiRzbGlkZXIub2ZmKCdmb2N1cy5zbGljayBibHVyLnNsaWNrJykub24oJ2ZvY3VzLnNsaWNrIGJsdXIuc2xpY2snLCAnKjpub3QoLnNsaWNrLWFycm93KScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XG4gICAgICB2YXIgJHNmID0gJCh0aGlzKTtcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoXy5vcHRpb25zLnBhdXNlT25Gb2N1cykge1xuICAgICAgICAgIF8uZm9jdXNzZWQgPSAkc2YuaXMoJzpmb2N1cycpO1xuXG4gICAgICAgICAgXy5hdXRvUGxheSgpO1xuICAgICAgICB9XG4gICAgICB9LCAwKTtcbiAgICB9KTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuZ2V0Q3VycmVudCA9IFNsaWNrLnByb3RvdHlwZS5zbGlja0N1cnJlbnRTbGlkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICByZXR1cm4gXy5jdXJyZW50U2xpZGU7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmdldERvdENvdW50ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIHZhciBicmVha1BvaW50ID0gMDtcbiAgICB2YXIgY291bnRlciA9IDA7XG4gICAgdmFyIHBhZ2VyUXR5ID0gMDtcblxuICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUpIHtcbiAgICAgIHdoaWxlIChicmVha1BvaW50IDwgXy5zbGlkZUNvdW50KSB7XG4gICAgICAgICsrcGFnZXJRdHk7XG4gICAgICAgIGJyZWFrUG9pbnQgPSBjb3VudGVyICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgICAgICBjb3VudGVyICs9IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID8gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDogXy5vcHRpb25zLnNsaWRlc1RvU2hvdztcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICBwYWdlclF0eSA9IF8uc2xpZGVDb3VudDtcbiAgICB9IGVsc2UgaWYgKCFfLm9wdGlvbnMuYXNOYXZGb3IpIHtcbiAgICAgIHBhZ2VyUXR5ID0gMSArIE1hdGguY2VpbCgoXy5zbGlkZUNvdW50IC0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykgLyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwpO1xuICAgIH0gZWxzZSB7XG4gICAgICB3aGlsZSAoYnJlYWtQb2ludCA8IF8uc2xpZGVDb3VudCkge1xuICAgICAgICArK3BhZ2VyUXR5O1xuICAgICAgICBicmVha1BvaW50ID0gY291bnRlciArIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcbiAgICAgICAgY291bnRlciArPSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA/IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA6IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3c7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHBhZ2VyUXR5IC0gMTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuZ2V0TGVmdCA9IGZ1bmN0aW9uIChzbGlkZUluZGV4KSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICB0YXJnZXRMZWZ0LFxuICAgICAgICB2ZXJ0aWNhbEhlaWdodCxcbiAgICAgICAgdmVydGljYWxPZmZzZXQgPSAwLFxuICAgICAgICB0YXJnZXRTbGlkZTtcblxuICAgIF8uc2xpZGVPZmZzZXQgPSAwO1xuICAgIHZlcnRpY2FsSGVpZ2h0ID0gXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJIZWlnaHQodHJ1ZSk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlKSB7XG4gICAgICBpZiAoXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gXy5zbGlkZVdpZHRoICogXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAqIC0xO1xuICAgICAgICB2ZXJ0aWNhbE9mZnNldCA9IHZlcnRpY2FsSGVpZ2h0ICogXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAqIC0xO1xuICAgICAgfVxuXG4gICAgICBpZiAoXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsICE9PSAwKSB7XG4gICAgICAgIGlmIChzbGlkZUluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsID4gXy5zbGlkZUNvdW50ICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICBpZiAoc2xpZGVJbmRleCA+IF8uc2xpZGVDb3VudCkge1xuICAgICAgICAgICAgXy5zbGlkZU9mZnNldCA9IChfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gKHNsaWRlSW5kZXggLSBfLnNsaWRlQ291bnQpKSAqIF8uc2xpZGVXaWR0aCAqIC0xO1xuICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAtIChzbGlkZUluZGV4IC0gXy5zbGlkZUNvdW50KSkgKiB2ZXJ0aWNhbEhlaWdodCAqIC0xO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsICogXy5zbGlkZVdpZHRoICogLTE7XG4gICAgICAgICAgICB2ZXJ0aWNhbE9mZnNldCA9IF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAqIHZlcnRpY2FsSGVpZ2h0ICogLTE7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChzbGlkZUluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA+IF8uc2xpZGVDb3VudCkge1xuICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gXy5zbGlkZUNvdW50KSAqIF8uc2xpZGVXaWR0aDtcbiAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoc2xpZGVJbmRleCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLSBfLnNsaWRlQ291bnQpICogdmVydGljYWxIZWlnaHQ7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICBfLnNsaWRlT2Zmc2V0ID0gMDtcbiAgICAgIHZlcnRpY2FsT2Zmc2V0ID0gMDtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgJiYgXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlKSB7XG4gICAgICBfLnNsaWRlT2Zmc2V0ICs9IF8uc2xpZGVXaWR0aCAqIE1hdGguZmxvb3IoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIpIC0gXy5zbGlkZVdpZHRoO1xuICAgIH0gZWxzZSBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcbiAgICAgIF8uc2xpZGVPZmZzZXQgPSAwO1xuICAgICAgXy5zbGlkZU9mZnNldCArPSBfLnNsaWRlV2lkdGggKiBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKTtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgdGFyZ2V0TGVmdCA9IHNsaWRlSW5kZXggKiBfLnNsaWRlV2lkdGggKiAtMSArIF8uc2xpZGVPZmZzZXQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRhcmdldExlZnQgPSBzbGlkZUluZGV4ICogdmVydGljYWxIZWlnaHQgKiAtMSArIHZlcnRpY2FsT2Zmc2V0O1xuICAgIH1cblxuICAgIGlmIChfLm9wdGlvbnMudmFyaWFibGVXaWR0aCA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IHx8IF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgdGFyZ2V0U2xpZGUgPSBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCcuc2xpY2stc2xpZGUnKS5lcShzbGlkZUluZGV4KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRhcmdldFNsaWRlID0gXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykuZXEoc2xpZGVJbmRleCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpO1xuICAgICAgfVxuXG4gICAgICBpZiAoXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSkge1xuICAgICAgICBpZiAodGFyZ2V0U2xpZGVbMF0pIHtcbiAgICAgICAgICB0YXJnZXRMZWZ0ID0gKF8uJHNsaWRlVHJhY2sud2lkdGgoKSAtIHRhcmdldFNsaWRlWzBdLm9mZnNldExlZnQgLSB0YXJnZXRTbGlkZS53aWR0aCgpKSAqIC0xO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRhcmdldExlZnQgPSAwO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0YXJnZXRMZWZ0ID0gdGFyZ2V0U2xpZGVbMF0gPyB0YXJnZXRTbGlkZVswXS5vZmZzZXRMZWZ0ICogLTEgOiAwO1xuICAgICAgfVxuXG4gICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IHx8IF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmVxKHNsaWRlSW5kZXgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRhcmdldFNsaWRlID0gXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykuZXEoc2xpZGVJbmRleCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyAxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLm9wdGlvbnMucnRsID09PSB0cnVlKSB7XG4gICAgICAgICAgaWYgKHRhcmdldFNsaWRlWzBdKSB7XG4gICAgICAgICAgICB0YXJnZXRMZWZ0ID0gKF8uJHNsaWRlVHJhY2sud2lkdGgoKSAtIHRhcmdldFNsaWRlWzBdLm9mZnNldExlZnQgLSB0YXJnZXRTbGlkZS53aWR0aCgpKSAqIC0xO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0YXJnZXRMZWZ0ID0gMDtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGFyZ2V0TGVmdCA9IHRhcmdldFNsaWRlWzBdID8gdGFyZ2V0U2xpZGVbMF0ub2Zmc2V0TGVmdCAqIC0xIDogMDtcbiAgICAgICAgfVxuXG4gICAgICAgIHRhcmdldExlZnQgKz0gKF8uJGxpc3Qud2lkdGgoKSAtIHRhcmdldFNsaWRlLm91dGVyV2lkdGgoKSkgLyAyO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0YXJnZXRMZWZ0O1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5nZXRPcHRpb24gPSBTbGljay5wcm90b3R5cGUuc2xpY2tHZXRPcHRpb24gPSBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgcmV0dXJuIF8ub3B0aW9uc1tvcHRpb25dO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5nZXROYXZpZ2FibGVJbmRleGVzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgYnJlYWtQb2ludCA9IDAsXG4gICAgICAgIGNvdW50ZXIgPSAwLFxuICAgICAgICBpbmRleGVzID0gW10sXG4gICAgICAgIG1heDtcblxuICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IGZhbHNlKSB7XG4gICAgICBtYXggPSBfLnNsaWRlQ291bnQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIGJyZWFrUG9pbnQgPSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgKiAtMTtcbiAgICAgIGNvdW50ZXIgPSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgKiAtMTtcbiAgICAgIG1heCA9IF8uc2xpZGVDb3VudCAqIDI7XG4gICAgfVxuXG4gICAgd2hpbGUgKGJyZWFrUG9pbnQgPCBtYXgpIHtcbiAgICAgIGluZGV4ZXMucHVzaChicmVha1BvaW50KTtcbiAgICAgIGJyZWFrUG9pbnQgPSBjb3VudGVyICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgICAgY291bnRlciArPSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA/IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA6IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3c7XG4gICAgfVxuXG4gICAgcmV0dXJuIGluZGV4ZXM7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmdldFNsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5nZXRTbGlkZUNvdW50ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgc2xpZGVzVHJhdmVyc2VkLFxuICAgICAgICBzd2lwZWRTbGlkZSxcbiAgICAgICAgY2VudGVyT2Zmc2V0O1xuXG4gICAgY2VudGVyT2Zmc2V0ID0gXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgPyBfLnNsaWRlV2lkdGggKiBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKSA6IDA7XG5cbiAgICBpZiAoXy5vcHRpb25zLnN3aXBlVG9TbGlkZSA9PT0gdHJ1ZSkge1xuICAgICAgXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stc2xpZGUnKS5lYWNoKGZ1bmN0aW9uIChpbmRleCwgc2xpZGUpIHtcbiAgICAgICAgaWYgKHNsaWRlLm9mZnNldExlZnQgLSBjZW50ZXJPZmZzZXQgKyAkKHNsaWRlKS5vdXRlcldpZHRoKCkgLyAyID4gXy5zd2lwZUxlZnQgKiAtMSkge1xuICAgICAgICAgIHN3aXBlZFNsaWRlID0gc2xpZGU7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgc2xpZGVzVHJhdmVyc2VkID0gTWF0aC5hYnMoJChzd2lwZWRTbGlkZSkuYXR0cignZGF0YS1zbGljay1pbmRleCcpIC0gXy5jdXJyZW50U2xpZGUpIHx8IDE7XG4gICAgICByZXR1cm4gc2xpZGVzVHJhdmVyc2VkO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuZ29UbyA9IFNsaWNrLnByb3RvdHlwZS5zbGlja0dvVG8gPSBmdW5jdGlvbiAoc2xpZGUsIGRvbnRBbmltYXRlKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgXy5jaGFuZ2VTbGlkZSh7XG4gICAgICBkYXRhOiB7XG4gICAgICAgIG1lc3NhZ2U6ICdpbmRleCcsXG4gICAgICAgIGluZGV4OiBwYXJzZUludChzbGlkZSlcbiAgICAgIH1cbiAgICB9LCBkb250QW5pbWF0ZSk7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoY3JlYXRpb24pIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoISQoXy4kc2xpZGVyKS5oYXNDbGFzcygnc2xpY2staW5pdGlhbGl6ZWQnKSkge1xuICAgICAgJChfLiRzbGlkZXIpLmFkZENsYXNzKCdzbGljay1pbml0aWFsaXplZCcpO1xuXG4gICAgICBfLmJ1aWxkUm93cygpO1xuXG4gICAgICBfLmJ1aWxkT3V0KCk7XG5cbiAgICAgIF8uc2V0UHJvcHMoKTtcblxuICAgICAgXy5zdGFydExvYWQoKTtcblxuICAgICAgXy5sb2FkU2xpZGVyKCk7XG5cbiAgICAgIF8uaW5pdGlhbGl6ZUV2ZW50cygpO1xuXG4gICAgICBfLnVwZGF0ZUFycm93cygpO1xuXG4gICAgICBfLnVwZGF0ZURvdHMoKTtcblxuICAgICAgXy5jaGVja1Jlc3BvbnNpdmUodHJ1ZSk7XG5cbiAgICAgIF8uZm9jdXNIYW5kbGVyKCk7XG4gICAgfVxuXG4gICAgaWYgKGNyZWF0aW9uKSB7XG4gICAgICBfLiRzbGlkZXIudHJpZ2dlcignaW5pdCcsIFtfXSk7XG4gICAgfVxuXG4gICAgaWYgKF8ub3B0aW9ucy5hY2Nlc3NpYmlsaXR5ID09PSB0cnVlKSB7XG4gICAgICBfLmluaXRBREEoKTtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmF1dG9wbGF5KSB7XG4gICAgICBfLnBhdXNlZCA9IGZhbHNlO1xuXG4gICAgICBfLmF1dG9QbGF5KCk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5pbml0QURBID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIF8uJHNsaWRlcy5hZGQoXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stY2xvbmVkJykpLmF0dHIoe1xuICAgICAgJ2FyaWEtaGlkZGVuJzogJ3RydWUnLFxuICAgICAgJ3RhYmluZGV4JzogJy0xJ1xuICAgIH0pLmZpbmQoJ2EsIGlucHV0LCBidXR0b24sIHNlbGVjdCcpLmF0dHIoe1xuICAgICAgJ3RhYmluZGV4JzogJy0xJ1xuICAgIH0pO1xuXG4gICAgXy4kc2xpZGVUcmFjay5hdHRyKCdyb2xlJywgJ2xpc3Rib3gnKTtcblxuICAgIF8uJHNsaWRlcy5ub3QoXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stY2xvbmVkJykpLmVhY2goZnVuY3Rpb24gKGkpIHtcbiAgICAgICQodGhpcykuYXR0cih7XG4gICAgICAgICdyb2xlJzogJ29wdGlvbicsXG4gICAgICAgICdhcmlhLWRlc2NyaWJlZGJ5JzogJ3NsaWNrLXNsaWRlJyArIF8uaW5zdGFuY2VVaWQgKyBpICsgJydcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgaWYgKF8uJGRvdHMgIT09IG51bGwpIHtcbiAgICAgIF8uJGRvdHMuYXR0cigncm9sZScsICd0YWJsaXN0JykuZmluZCgnbGknKS5lYWNoKGZ1bmN0aW9uIChpKSB7XG4gICAgICAgICQodGhpcykuYXR0cih7XG4gICAgICAgICAgJ3JvbGUnOiAncHJlc2VudGF0aW9uJyxcbiAgICAgICAgICAnYXJpYS1zZWxlY3RlZCc6ICdmYWxzZScsXG4gICAgICAgICAgJ2FyaWEtY29udHJvbHMnOiAnbmF2aWdhdGlvbicgKyBfLmluc3RhbmNlVWlkICsgaSArICcnLFxuICAgICAgICAgICdpZCc6ICdzbGljay1zbGlkZScgKyBfLmluc3RhbmNlVWlkICsgaSArICcnXG4gICAgICAgIH0pO1xuICAgICAgfSkuZmlyc3QoKS5hdHRyKCdhcmlhLXNlbGVjdGVkJywgJ3RydWUnKS5lbmQoKS5maW5kKCdidXR0b24nKS5hdHRyKCdyb2xlJywgJ2J1dHRvbicpLmVuZCgpLmNsb3Nlc3QoJ2RpdicpLmF0dHIoJ3JvbGUnLCAndG9vbGJhcicpO1xuICAgIH1cblxuICAgIF8uYWN0aXZhdGVBREEoKTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuaW5pdEFycm93RXZlbnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uJHByZXZBcnJvdy5vZmYoJ2NsaWNrLnNsaWNrJykub24oJ2NsaWNrLnNsaWNrJywge1xuICAgICAgICBtZXNzYWdlOiAncHJldmlvdXMnXG4gICAgICB9LCBfLmNoYW5nZVNsaWRlKTtcblxuICAgICAgXy4kbmV4dEFycm93Lm9mZignY2xpY2suc2xpY2snKS5vbignY2xpY2suc2xpY2snLCB7XG4gICAgICAgIG1lc3NhZ2U6ICduZXh0J1xuICAgICAgfSwgXy5jaGFuZ2VTbGlkZSk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5pbml0RG90RXZlbnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAkKCdsaScsIF8uJGRvdHMpLm9uKCdjbGljay5zbGljaycsIHtcbiAgICAgICAgbWVzc2FnZTogJ2luZGV4J1xuICAgICAgfSwgXy5jaGFuZ2VTbGlkZSk7XG4gICAgfVxuXG4gICAgaWYgKF8ub3B0aW9ucy5kb3RzID09PSB0cnVlICYmIF8ub3B0aW9ucy5wYXVzZU9uRG90c0hvdmVyID09PSB0cnVlKSB7XG4gICAgICAkKCdsaScsIF8uJGRvdHMpLm9uKCdtb3VzZWVudGVyLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgdHJ1ZSkpLm9uKCdtb3VzZWxlYXZlLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgZmFsc2UpKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLmluaXRTbGlkZUV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5vcHRpb25zLnBhdXNlT25Ib3Zlcikge1xuICAgICAgXy4kbGlzdC5vbignbW91c2VlbnRlci5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIHRydWUpKTtcblxuICAgICAgXy4kbGlzdC5vbignbW91c2VsZWF2ZS5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIGZhbHNlKSk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5pbml0aWFsaXplRXZlbnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIF8uaW5pdEFycm93RXZlbnRzKCk7XG5cbiAgICBfLmluaXREb3RFdmVudHMoKTtcblxuICAgIF8uaW5pdFNsaWRlRXZlbnRzKCk7XG5cbiAgICBfLiRsaXN0Lm9uKCd0b3VjaHN0YXJ0LnNsaWNrIG1vdXNlZG93bi5zbGljaycsIHtcbiAgICAgIGFjdGlvbjogJ3N0YXJ0J1xuICAgIH0sIF8uc3dpcGVIYW5kbGVyKTtcblxuICAgIF8uJGxpc3Qub24oJ3RvdWNobW92ZS5zbGljayBtb3VzZW1vdmUuc2xpY2snLCB7XG4gICAgICBhY3Rpb246ICdtb3ZlJ1xuICAgIH0sIF8uc3dpcGVIYW5kbGVyKTtcblxuICAgIF8uJGxpc3Qub24oJ3RvdWNoZW5kLnNsaWNrIG1vdXNldXAuc2xpY2snLCB7XG4gICAgICBhY3Rpb246ICdlbmQnXG4gICAgfSwgXy5zd2lwZUhhbmRsZXIpO1xuXG4gICAgXy4kbGlzdC5vbigndG91Y2hjYW5jZWwuc2xpY2sgbW91c2VsZWF2ZS5zbGljaycsIHtcbiAgICAgIGFjdGlvbjogJ2VuZCdcbiAgICB9LCBfLnN3aXBlSGFuZGxlcik7XG5cbiAgICBfLiRsaXN0Lm9uKCdjbGljay5zbGljaycsIF8uY2xpY2tIYW5kbGVyKTtcblxuICAgICQoZG9jdW1lbnQpLm9uKF8udmlzaWJpbGl0eUNoYW5nZSwgJC5wcm94eShfLnZpc2liaWxpdHksIF8pKTtcblxuICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xuICAgICAgXy4kbGlzdC5vbigna2V5ZG93bi5zbGljaycsIF8ua2V5SGFuZGxlcik7XG4gICAgfVxuXG4gICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uU2VsZWN0ID09PSB0cnVlKSB7XG4gICAgICAkKF8uJHNsaWRlVHJhY2spLmNoaWxkcmVuKCkub24oJ2NsaWNrLnNsaWNrJywgXy5zZWxlY3RIYW5kbGVyKTtcbiAgICB9XG5cbiAgICAkKHdpbmRvdykub24oJ29yaWVudGF0aW9uY2hhbmdlLnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCAkLnByb3h5KF8ub3JpZW50YXRpb25DaGFuZ2UsIF8pKTtcbiAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZS5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgJC5wcm94eShfLnJlc2l6ZSwgXykpO1xuICAgICQoJ1tkcmFnZ2FibGUhPXRydWVdJywgXy4kc2xpZGVUcmFjaykub24oJ2RyYWdzdGFydCcsIF8ucHJldmVudERlZmF1bHQpO1xuICAgICQod2luZG93KS5vbignbG9hZC5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgXy5zZXRQb3NpdGlvbik7XG4gICAgJChkb2N1bWVudCkub24oJ3JlYWR5LnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCBfLnNldFBvc2l0aW9uKTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuaW5pdFVJID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uJHByZXZBcnJvdy5zaG93KCk7XG5cbiAgICAgIF8uJG5leHRBcnJvdy5zaG93KCk7XG4gICAgfVxuXG4gICAgaWYgKF8ub3B0aW9ucy5kb3RzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uJGRvdHMuc2hvdygpO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUua2V5SGFuZGxlciA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciBfID0gdGhpczsgLy9Eb250IHNsaWRlIGlmIHRoZSBjdXJzb3IgaXMgaW5zaWRlIHRoZSBmb3JtIGZpZWxkcyBhbmQgYXJyb3cga2V5cyBhcmUgcHJlc3NlZFxuXG5cbiAgICBpZiAoIWV2ZW50LnRhcmdldC50YWdOYW1lLm1hdGNoKCdURVhUQVJFQXxJTlBVVHxTRUxFQ1QnKSkge1xuICAgICAgaWYgKGV2ZW50LmtleUNvZGUgPT09IDM3ICYmIF8ub3B0aW9ucy5hY2Nlc3NpYmlsaXR5ID09PSB0cnVlKSB7XG4gICAgICAgIF8uY2hhbmdlU2xpZGUoe1xuICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgIG1lc3NhZ2U6IF8ub3B0aW9ucy5ydGwgPT09IHRydWUgPyAnbmV4dCcgOiAncHJldmlvdXMnXG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzkgJiYgXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcbiAgICAgICAgXy5jaGFuZ2VTbGlkZSh7XG4gICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgbWVzc2FnZTogXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSA/ICdwcmV2aW91cycgOiAnbmV4dCdcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUubGF6eUxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBsb2FkUmFuZ2UsXG4gICAgICAgIGNsb25lUmFuZ2UsXG4gICAgICAgIHJhbmdlU3RhcnQsXG4gICAgICAgIHJhbmdlRW5kO1xuXG4gICAgZnVuY3Rpb24gbG9hZEltYWdlcyhpbWFnZXNTY29wZSkge1xuICAgICAgJCgnaW1nW2RhdGEtbGF6eV0nLCBpbWFnZXNTY29wZSkuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBpbWFnZSA9ICQodGhpcyksXG4gICAgICAgICAgICBpbWFnZVNvdXJjZSA9ICQodGhpcykuYXR0cignZGF0YS1sYXp5JyksXG4gICAgICAgICAgICBpbWFnZVRvTG9hZCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2ltZycpO1xuXG4gICAgICAgIGltYWdlVG9Mb2FkLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpbWFnZS5hbmltYXRlKHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDBcbiAgICAgICAgICB9LCAxMDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGltYWdlLmF0dHIoJ3NyYycsIGltYWdlU291cmNlKS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgb3BhY2l0eTogMVxuICAgICAgICAgICAgfSwgMjAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIGltYWdlLnJlbW92ZUF0dHIoJ2RhdGEtbGF6eScpLnJlbW92ZUNsYXNzKCdzbGljay1sb2FkaW5nJyk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkZWQnLCBbXywgaW1hZ2UsIGltYWdlU291cmNlXSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgaW1hZ2VUb0xvYWQub25lcnJvciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpbWFnZS5yZW1vdmVBdHRyKCdkYXRhLWxhenknKS5yZW1vdmVDbGFzcygnc2xpY2stbG9hZGluZycpLmFkZENsYXNzKCdzbGljay1sYXp5bG9hZC1lcnJvcicpO1xuXG4gICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkRXJyb3InLCBbXywgaW1hZ2UsIGltYWdlU291cmNlXSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgaW1hZ2VUb0xvYWQuc3JjID0gaW1hZ2VTb3VyY2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcbiAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUpIHtcbiAgICAgICAgcmFuZ2VTdGFydCA9IF8uY3VycmVudFNsaWRlICsgKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyICsgMSk7XG4gICAgICAgIHJhbmdlRW5kID0gcmFuZ2VTdGFydCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyAyO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmFuZ2VTdGFydCA9IE1hdGgubWF4KDAsIF8uY3VycmVudFNsaWRlIC0gKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyICsgMSkpO1xuICAgICAgICByYW5nZUVuZCA9IDIgKyAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIgKyAxKSArIF8uY3VycmVudFNsaWRlO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByYW5nZVN0YXJ0ID0gXy5vcHRpb25zLmluZmluaXRlID8gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyArIF8uY3VycmVudFNsaWRlIDogXy5jdXJyZW50U2xpZGU7XG4gICAgICByYW5nZUVuZCA9IE1hdGguY2VpbChyYW5nZVN0YXJ0ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdyk7XG5cbiAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xuICAgICAgICBpZiAocmFuZ2VTdGFydCA+IDApIHJhbmdlU3RhcnQtLTtcbiAgICAgICAgaWYgKHJhbmdlRW5kIDw9IF8uc2xpZGVDb3VudCkgcmFuZ2VFbmQrKztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsb2FkUmFuZ2UgPSBfLiRzbGlkZXIuZmluZCgnLnNsaWNrLXNsaWRlJykuc2xpY2UocmFuZ2VTdGFydCwgcmFuZ2VFbmQpO1xuICAgIGxvYWRJbWFnZXMobG9hZFJhbmdlKTtcblxuICAgIGlmIChfLnNsaWRlQ291bnQgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgY2xvbmVSYW5nZSA9IF8uJHNsaWRlci5maW5kKCcuc2xpY2stc2xpZGUnKTtcbiAgICAgIGxvYWRJbWFnZXMoY2xvbmVSYW5nZSk7XG4gICAgfSBlbHNlIGlmIChfLmN1cnJlbnRTbGlkZSA+PSBfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICBjbG9uZVJhbmdlID0gXy4kc2xpZGVyLmZpbmQoJy5zbGljay1jbG9uZWQnKS5zbGljZSgwLCBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcbiAgICAgIGxvYWRJbWFnZXMoY2xvbmVSYW5nZSk7XG4gICAgfSBlbHNlIGlmIChfLmN1cnJlbnRTbGlkZSA9PT0gMCkge1xuICAgICAgY2xvbmVSYW5nZSA9IF8uJHNsaWRlci5maW5kKCcuc2xpY2stY2xvbmVkJykuc2xpY2UoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAqIC0xKTtcbiAgICAgIGxvYWRJbWFnZXMoY2xvbmVSYW5nZSk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5sb2FkU2xpZGVyID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIF8uc2V0UG9zaXRpb24oKTtcblxuICAgIF8uJHNsaWRlVHJhY2suY3NzKHtcbiAgICAgIG9wYWNpdHk6IDFcbiAgICB9KTtcblxuICAgIF8uJHNsaWRlci5yZW1vdmVDbGFzcygnc2xpY2stbG9hZGluZycpO1xuXG4gICAgXy5pbml0VUkoKTtcblxuICAgIGlmIChfLm9wdGlvbnMubGF6eUxvYWQgPT09ICdwcm9ncmVzc2l2ZScpIHtcbiAgICAgIF8ucHJvZ3Jlc3NpdmVMYXp5TG9hZCgpO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUubmV4dCA9IFNsaWNrLnByb3RvdHlwZS5zbGlja05leHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgXy5jaGFuZ2VTbGlkZSh7XG4gICAgICBkYXRhOiB7XG4gICAgICAgIG1lc3NhZ2U6ICduZXh0J1xuICAgICAgfVxuICAgIH0pO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5vcmllbnRhdGlvbkNoYW5nZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBfLmNoZWNrUmVzcG9uc2l2ZSgpO1xuXG4gICAgXy5zZXRQb3NpdGlvbigpO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5wYXVzZSA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1BhdXNlID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIF8uYXV0b1BsYXlDbGVhcigpO1xuXG4gICAgXy5wYXVzZWQgPSB0cnVlO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5wbGF5ID0gU2xpY2sucHJvdG90eXBlLnNsaWNrUGxheSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBfLmF1dG9QbGF5KCk7XG5cbiAgICBfLm9wdGlvbnMuYXV0b3BsYXkgPSB0cnVlO1xuICAgIF8ucGF1c2VkID0gZmFsc2U7XG4gICAgXy5mb2N1c3NlZCA9IGZhbHNlO1xuICAgIF8uaW50ZXJydXB0ZWQgPSBmYWxzZTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUucG9zdFNsaWRlID0gZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKCFfLnVuc2xpY2tlZCkge1xuICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2FmdGVyQ2hhbmdlJywgW18sIGluZGV4XSk7XG5cbiAgICAgIF8uYW5pbWF0aW5nID0gZmFsc2U7XG5cbiAgICAgIF8uc2V0UG9zaXRpb24oKTtcblxuICAgICAgXy5zd2lwZUxlZnQgPSBudWxsO1xuXG4gICAgICBpZiAoXy5vcHRpb25zLmF1dG9wbGF5KSB7XG4gICAgICAgIF8uYXV0b1BsYXkoKTtcbiAgICAgIH1cblxuICAgICAgaWYgKF8ub3B0aW9ucy5hY2Nlc3NpYmlsaXR5ID09PSB0cnVlKSB7XG4gICAgICAgIF8uaW5pdEFEQSgpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUucHJldiA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1ByZXYgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgXy5jaGFuZ2VTbGlkZSh7XG4gICAgICBkYXRhOiB7XG4gICAgICAgIG1lc3NhZ2U6ICdwcmV2aW91cydcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUucHJldmVudERlZmF1bHQgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5wcm9ncmVzc2l2ZUxhenlMb2FkID0gZnVuY3Rpb24gKHRyeUNvdW50KSB7XG4gICAgdHJ5Q291bnQgPSB0cnlDb3VudCB8fCAxO1xuXG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAkaW1nc1RvTG9hZCA9ICQoJ2ltZ1tkYXRhLWxhenldJywgXy4kc2xpZGVyKSxcbiAgICAgICAgaW1hZ2UsXG4gICAgICAgIGltYWdlU291cmNlLFxuICAgICAgICBpbWFnZVRvTG9hZDtcblxuICAgIGlmICgkaW1nc1RvTG9hZC5sZW5ndGgpIHtcbiAgICAgIGltYWdlID0gJGltZ3NUb0xvYWQuZmlyc3QoKTtcbiAgICAgIGltYWdlU291cmNlID0gaW1hZ2UuYXR0cignZGF0YS1sYXp5Jyk7XG4gICAgICBpbWFnZVRvTG9hZCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2ltZycpO1xuXG4gICAgICBpbWFnZVRvTG9hZC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGltYWdlLmF0dHIoJ3NyYycsIGltYWdlU291cmNlKS5yZW1vdmVBdHRyKCdkYXRhLWxhenknKS5yZW1vdmVDbGFzcygnc2xpY2stbG9hZGluZycpO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYWRhcHRpdmVIZWlnaHQgPT09IHRydWUpIHtcbiAgICAgICAgICBfLnNldFBvc2l0aW9uKCk7XG4gICAgICAgIH1cblxuICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignbGF6eUxvYWRlZCcsIFtfLCBpbWFnZSwgaW1hZ2VTb3VyY2VdKTtcblxuICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcbiAgICAgIH07XG5cbiAgICAgIGltYWdlVG9Mb2FkLm9uZXJyb3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0cnlDb3VudCA8IDMpIHtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiB0cnkgdG8gbG9hZCB0aGUgaW1hZ2UgMyB0aW1lcyxcbiAgICAgICAgICAgKiBsZWF2ZSBhIHNsaWdodCBkZWxheSBzbyB3ZSBkb24ndCBnZXRcbiAgICAgICAgICAgKiBzZXJ2ZXJzIGJsb2NraW5nIHRoZSByZXF1ZXN0LlxuICAgICAgICAgICAqL1xuICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgXy5wcm9ncmVzc2l2ZUxhenlMb2FkKHRyeUNvdW50ICsgMSk7XG4gICAgICAgICAgfSwgNTAwKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpbWFnZS5yZW1vdmVBdHRyKCdkYXRhLWxhenknKS5yZW1vdmVDbGFzcygnc2xpY2stbG9hZGluZycpLmFkZENsYXNzKCdzbGljay1sYXp5bG9hZC1lcnJvcicpO1xuXG4gICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkRXJyb3InLCBbXywgaW1hZ2UsIGltYWdlU291cmNlXSk7XG5cbiAgICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcbiAgICAgICAgfVxuICAgICAgfTtcblxuICAgICAgaW1hZ2VUb0xvYWQuc3JjID0gaW1hZ2VTb3VyY2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdhbGxJbWFnZXNMb2FkZWQnLCBbX10pO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUucmVmcmVzaCA9IGZ1bmN0aW9uIChpbml0aWFsaXppbmcpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGN1cnJlbnRTbGlkZSxcbiAgICAgICAgbGFzdFZpc2libGVJbmRleDtcblxuICAgIGxhc3RWaXNpYmxlSW5kZXggPSBfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93OyAvLyBpbiBub24taW5maW5pdGUgc2xpZGVycywgd2UgZG9uJ3Qgd2FudCB0byBnbyBwYXN0IHRoZVxuICAgIC8vIGxhc3QgdmlzaWJsZSBpbmRleC5cblxuICAgIGlmICghXy5vcHRpb25zLmluZmluaXRlICYmIF8uY3VycmVudFNsaWRlID4gbGFzdFZpc2libGVJbmRleCkge1xuICAgICAgXy5jdXJyZW50U2xpZGUgPSBsYXN0VmlzaWJsZUluZGV4O1xuICAgIH0gLy8gaWYgbGVzcyBzbGlkZXMgdGhhbiB0byBzaG93LCBnbyB0byBzdGFydC5cblxuXG4gICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICBfLmN1cnJlbnRTbGlkZSA9IDA7XG4gICAgfVxuXG4gICAgY3VycmVudFNsaWRlID0gXy5jdXJyZW50U2xpZGU7XG5cbiAgICBfLmRlc3Ryb3kodHJ1ZSk7XG5cbiAgICAkLmV4dGVuZChfLCBfLmluaXRpYWxzLCB7XG4gICAgICBjdXJyZW50U2xpZGU6IGN1cnJlbnRTbGlkZVxuICAgIH0pO1xuXG4gICAgXy5pbml0KCk7XG5cbiAgICBpZiAoIWluaXRpYWxpemluZykge1xuICAgICAgXy5jaGFuZ2VTbGlkZSh7XG4gICAgICAgIGRhdGE6IHtcbiAgICAgICAgICBtZXNzYWdlOiAnaW5kZXgnLFxuICAgICAgICAgIGluZGV4OiBjdXJyZW50U2xpZGVcbiAgICAgICAgfVxuICAgICAgfSwgZmFsc2UpO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUucmVnaXN0ZXJCcmVha3BvaW50cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGJyZWFrcG9pbnQsXG4gICAgICAgIGN1cnJlbnRCcmVha3BvaW50LFxuICAgICAgICBsLFxuICAgICAgICByZXNwb25zaXZlU2V0dGluZ3MgPSBfLm9wdGlvbnMucmVzcG9uc2l2ZSB8fCBudWxsO1xuXG4gICAgaWYgKCQudHlwZShyZXNwb25zaXZlU2V0dGluZ3MpID09PSAnYXJyYXknICYmIHJlc3BvbnNpdmVTZXR0aW5ncy5sZW5ndGgpIHtcbiAgICAgIF8ucmVzcG9uZFRvID0gXy5vcHRpb25zLnJlc3BvbmRUbyB8fCAnd2luZG93JztcblxuICAgICAgZm9yIChicmVha3BvaW50IGluIHJlc3BvbnNpdmVTZXR0aW5ncykge1xuICAgICAgICBsID0gXy5icmVha3BvaW50cy5sZW5ndGggLSAxO1xuICAgICAgICBjdXJyZW50QnJlYWtwb2ludCA9IHJlc3BvbnNpdmVTZXR0aW5nc1ticmVha3BvaW50XS5icmVha3BvaW50O1xuXG4gICAgICAgIGlmIChyZXNwb25zaXZlU2V0dGluZ3MuaGFzT3duUHJvcGVydHkoYnJlYWtwb2ludCkpIHtcbiAgICAgICAgICAvLyBsb29wIHRocm91Z2ggdGhlIGJyZWFrcG9pbnRzIGFuZCBjdXQgb3V0IGFueSBleGlzdGluZ1xuICAgICAgICAgIC8vIG9uZXMgd2l0aCB0aGUgc2FtZSBicmVha3BvaW50IG51bWJlciwgd2UgZG9uJ3Qgd2FudCBkdXBlcy5cbiAgICAgICAgICB3aGlsZSAobCA+PSAwKSB7XG4gICAgICAgICAgICBpZiAoXy5icmVha3BvaW50c1tsXSAmJiBfLmJyZWFrcG9pbnRzW2xdID09PSBjdXJyZW50QnJlYWtwb2ludCkge1xuICAgICAgICAgICAgICBfLmJyZWFrcG9pbnRzLnNwbGljZShsLCAxKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbC0tO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIF8uYnJlYWtwb2ludHMucHVzaChjdXJyZW50QnJlYWtwb2ludCk7XG5cbiAgICAgICAgICBfLmJyZWFrcG9pbnRTZXR0aW5nc1tjdXJyZW50QnJlYWtwb2ludF0gPSByZXNwb25zaXZlU2V0dGluZ3NbYnJlYWtwb2ludF0uc2V0dGluZ3M7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgXy5icmVha3BvaW50cy5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgIHJldHVybiBfLm9wdGlvbnMubW9iaWxlRmlyc3QgPyBhIC0gYiA6IGIgLSBhO1xuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5yZWluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgXy4kc2xpZGVzID0gXy4kc2xpZGVUcmFjay5jaGlsZHJlbihfLm9wdGlvbnMuc2xpZGUpLmFkZENsYXNzKCdzbGljay1zbGlkZScpO1xuICAgIF8uc2xpZGVDb3VudCA9IF8uJHNsaWRlcy5sZW5ndGg7XG5cbiAgICBpZiAoXy5jdXJyZW50U2xpZGUgPj0gXy5zbGlkZUNvdW50ICYmIF8uY3VycmVudFNsaWRlICE9PSAwKSB7XG4gICAgICBfLmN1cnJlbnRTbGlkZSA9IF8uY3VycmVudFNsaWRlIC0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgIH1cblxuICAgIGlmIChfLnNsaWRlQ291bnQgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgXy5jdXJyZW50U2xpZGUgPSAwO1xuICAgIH1cblxuICAgIF8ucmVnaXN0ZXJCcmVha3BvaW50cygpO1xuXG4gICAgXy5zZXRQcm9wcygpO1xuXG4gICAgXy5zZXR1cEluZmluaXRlKCk7XG5cbiAgICBfLmJ1aWxkQXJyb3dzKCk7XG5cbiAgICBfLnVwZGF0ZUFycm93cygpO1xuXG4gICAgXy5pbml0QXJyb3dFdmVudHMoKTtcblxuICAgIF8uYnVpbGREb3RzKCk7XG5cbiAgICBfLnVwZGF0ZURvdHMoKTtcblxuICAgIF8uaW5pdERvdEV2ZW50cygpO1xuXG4gICAgXy5jbGVhblVwU2xpZGVFdmVudHMoKTtcblxuICAgIF8uaW5pdFNsaWRlRXZlbnRzKCk7XG5cbiAgICBfLmNoZWNrUmVzcG9uc2l2ZShmYWxzZSwgdHJ1ZSk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmZvY3VzT25TZWxlY3QgPT09IHRydWUpIHtcbiAgICAgICQoXy4kc2xpZGVUcmFjaykuY2hpbGRyZW4oKS5vbignY2xpY2suc2xpY2snLCBfLnNlbGVjdEhhbmRsZXIpO1xuICAgIH1cblxuICAgIF8uc2V0U2xpZGVDbGFzc2VzKHR5cGVvZiBfLmN1cnJlbnRTbGlkZSA9PT0gJ251bWJlcicgPyBfLmN1cnJlbnRTbGlkZSA6IDApO1xuXG4gICAgXy5zZXRQb3NpdGlvbigpO1xuXG4gICAgXy5mb2N1c0hhbmRsZXIoKTtcblxuICAgIF8ucGF1c2VkID0gIV8ub3B0aW9ucy5hdXRvcGxheTtcblxuICAgIF8uYXV0b1BsYXkoKTtcblxuICAgIF8uJHNsaWRlci50cmlnZ2VyKCdyZUluaXQnLCBbX10pO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5yZXNpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKCQod2luZG93KS53aWR0aCgpICE9PSBfLndpbmRvd1dpZHRoKSB7XG4gICAgICBjbGVhclRpbWVvdXQoXy53aW5kb3dEZWxheSk7XG4gICAgICBfLndpbmRvd0RlbGF5ID0gd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICBfLndpbmRvd1dpZHRoID0gJCh3aW5kb3cpLndpZHRoKCk7XG5cbiAgICAgICAgXy5jaGVja1Jlc3BvbnNpdmUoKTtcblxuICAgICAgICBpZiAoIV8udW5zbGlja2VkKSB7XG4gICAgICAgICAgXy5zZXRQb3NpdGlvbigpO1xuICAgICAgICB9XG4gICAgICB9LCA1MCk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5yZW1vdmVTbGlkZSA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1JlbW92ZSA9IGZ1bmN0aW9uIChpbmRleCwgcmVtb3ZlQmVmb3JlLCByZW1vdmVBbGwpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAodHlwZW9mIGluZGV4ID09PSAnYm9vbGVhbicpIHtcbiAgICAgIHJlbW92ZUJlZm9yZSA9IGluZGV4O1xuICAgICAgaW5kZXggPSByZW1vdmVCZWZvcmUgPT09IHRydWUgPyAwIDogXy5zbGlkZUNvdW50IC0gMTtcbiAgICB9IGVsc2Uge1xuICAgICAgaW5kZXggPSByZW1vdmVCZWZvcmUgPT09IHRydWUgPyAtLWluZGV4IDogaW5kZXg7XG4gICAgfVxuXG4gICAgaWYgKF8uc2xpZGVDb3VudCA8IDEgfHwgaW5kZXggPCAwIHx8IGluZGV4ID4gXy5zbGlkZUNvdW50IC0gMSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIF8udW5sb2FkKCk7XG5cbiAgICBpZiAocmVtb3ZlQWxsID09PSB0cnVlKSB7XG4gICAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCkucmVtb3ZlKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4odGhpcy5vcHRpb25zLnNsaWRlKS5lcShpbmRleCkucmVtb3ZlKCk7XG4gICAgfVxuXG4gICAgXy4kc2xpZGVzID0gXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpO1xuXG4gICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xuXG4gICAgXy4kc2xpZGVUcmFjay5hcHBlbmQoXy4kc2xpZGVzKTtcblxuICAgIF8uJHNsaWRlc0NhY2hlID0gXy4kc2xpZGVzO1xuXG4gICAgXy5yZWluaXQoKTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc2V0Q1NTID0gZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBwb3NpdGlvblByb3BzID0ge30sXG4gICAgICAgIHgsXG4gICAgICAgIHk7XG5cbiAgICBpZiAoXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSkge1xuICAgICAgcG9zaXRpb24gPSAtcG9zaXRpb247XG4gICAgfVxuXG4gICAgeCA9IF8ucG9zaXRpb25Qcm9wID09ICdsZWZ0JyA/IE1hdGguY2VpbChwb3NpdGlvbikgKyAncHgnIDogJzBweCc7XG4gICAgeSA9IF8ucG9zaXRpb25Qcm9wID09ICd0b3AnID8gTWF0aC5jZWlsKHBvc2l0aW9uKSArICdweCcgOiAnMHB4JztcbiAgICBwb3NpdGlvblByb3BzW18ucG9zaXRpb25Qcm9wXSA9IHBvc2l0aW9uO1xuXG4gICAgaWYgKF8udHJhbnNmb3Jtc0VuYWJsZWQgPT09IGZhbHNlKSB7XG4gICAgICBfLiRzbGlkZVRyYWNrLmNzcyhwb3NpdGlvblByb3BzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcG9zaXRpb25Qcm9wcyA9IHt9O1xuXG4gICAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcbiAgICAgICAgcG9zaXRpb25Qcm9wc1tfLmFuaW1UeXBlXSA9ICd0cmFuc2xhdGUoJyArIHggKyAnLCAnICsgeSArICcpJztcblxuICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhwb3NpdGlvblByb3BzKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBvc2l0aW9uUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlM2QoJyArIHggKyAnLCAnICsgeSArICcsIDBweCknO1xuXG4gICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKHBvc2l0aW9uUHJvcHMpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc2V0RGltZW5zaW9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgIF8uJGxpc3QuY3NzKHtcbiAgICAgICAgICBwYWRkaW5nOiAnMHB4ICcgKyBfLm9wdGlvbnMuY2VudGVyUGFkZGluZ1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgXy4kbGlzdC5oZWlnaHQoXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJIZWlnaHQodHJ1ZSkgKiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcblxuICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgIF8uJGxpc3QuY3NzKHtcbiAgICAgICAgICBwYWRkaW5nOiBfLm9wdGlvbnMuY2VudGVyUGFkZGluZyArICcgMHB4J1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfLmxpc3RXaWR0aCA9IF8uJGxpc3Qud2lkdGgoKTtcbiAgICBfLmxpc3RIZWlnaHQgPSBfLiRsaXN0LmhlaWdodCgpO1xuXG4gICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UgJiYgXy5vcHRpb25zLnZhcmlhYmxlV2lkdGggPT09IGZhbHNlKSB7XG4gICAgICBfLnNsaWRlV2lkdGggPSBNYXRoLmNlaWwoXy5saXN0V2lkdGggLyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcblxuICAgICAgXy4kc2xpZGVUcmFjay53aWR0aChNYXRoLmNlaWwoXy5zbGlkZVdpZHRoICogXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykubGVuZ3RoKSk7XG4gICAgfSBlbHNlIGlmIChfLm9wdGlvbnMudmFyaWFibGVXaWR0aCA9PT0gdHJ1ZSkge1xuICAgICAgXy4kc2xpZGVUcmFjay53aWR0aCg1MDAwICogXy5zbGlkZUNvdW50KTtcbiAgICB9IGVsc2Uge1xuICAgICAgXy5zbGlkZVdpZHRoID0gTWF0aC5jZWlsKF8ubGlzdFdpZHRoKTtcblxuICAgICAgXy4kc2xpZGVUcmFjay5oZWlnaHQoTWF0aC5jZWlsKF8uJHNsaWRlcy5maXJzdCgpLm91dGVySGVpZ2h0KHRydWUpICogXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykubGVuZ3RoKSk7XG4gICAgfVxuXG4gICAgdmFyIG9mZnNldCA9IF8uJHNsaWRlcy5maXJzdCgpLm91dGVyV2lkdGgodHJ1ZSkgLSBfLiRzbGlkZXMuZmlyc3QoKS53aWR0aCgpO1xuXG4gICAgaWYgKF8ub3B0aW9ucy52YXJpYWJsZVdpZHRoID09PSBmYWxzZSkgXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykud2lkdGgoXy5zbGlkZVdpZHRoIC0gb2Zmc2V0KTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc2V0RmFkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIHRhcmdldExlZnQ7XG5cbiAgICBfLiRzbGlkZXMuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGVsZW1lbnQpIHtcbiAgICAgIHRhcmdldExlZnQgPSBfLnNsaWRlV2lkdGggKiBpbmRleCAqIC0xO1xuXG4gICAgICBpZiAoXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSkge1xuICAgICAgICAkKGVsZW1lbnQpLmNzcyh7XG4gICAgICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICAgICAgcmlnaHQ6IHRhcmdldExlZnQsXG4gICAgICAgICAgdG9wOiAwLFxuICAgICAgICAgIHpJbmRleDogXy5vcHRpb25zLnpJbmRleCAtIDIsXG4gICAgICAgICAgb3BhY2l0eTogMFxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoZWxlbWVudCkuY3NzKHtcbiAgICAgICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgICAgICBsZWZ0OiB0YXJnZXRMZWZ0LFxuICAgICAgICAgIHRvcDogMCxcbiAgICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAyLFxuICAgICAgICAgIG9wYWNpdHk6IDBcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBfLiRzbGlkZXMuZXEoXy5jdXJyZW50U2xpZGUpLmNzcyh7XG4gICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAxLFxuICAgICAgb3BhY2l0eTogMVxuICAgIH0pO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5zZXRIZWlnaHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPT09IDEgJiYgXy5vcHRpb25zLmFkYXB0aXZlSGVpZ2h0ID09PSB0cnVlICYmIF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcbiAgICAgIHZhciB0YXJnZXRIZWlnaHQgPSBfLiRzbGlkZXMuZXEoXy5jdXJyZW50U2xpZGUpLm91dGVySGVpZ2h0KHRydWUpO1xuXG4gICAgICBfLiRsaXN0LmNzcygnaGVpZ2h0JywgdGFyZ2V0SGVpZ2h0KTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLnNldE9wdGlvbiA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1NldE9wdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAvKipcbiAgICAgKiBhY2NlcHRzIGFyZ3VtZW50cyBpbiBmb3JtYXQgb2Y6XG4gICAgICpcbiAgICAgKiAgLSBmb3IgY2hhbmdpbmcgYSBzaW5nbGUgb3B0aW9uJ3MgdmFsdWU6XG4gICAgICogICAgIC5zbGljayhcInNldE9wdGlvblwiLCBvcHRpb24sIHZhbHVlLCByZWZyZXNoIClcbiAgICAgKlxuICAgICAqICAtIGZvciBjaGFuZ2luZyBhIHNldCBvZiByZXNwb25zaXZlIG9wdGlvbnM6XG4gICAgICogICAgIC5zbGljayhcInNldE9wdGlvblwiLCAncmVzcG9uc2l2ZScsIFt7fSwgLi4uXSwgcmVmcmVzaCApXG4gICAgICpcbiAgICAgKiAgLSBmb3IgdXBkYXRpbmcgbXVsdGlwbGUgdmFsdWVzIGF0IG9uY2UgKG5vdCByZXNwb25zaXZlKVxuICAgICAqICAgICAuc2xpY2soXCJzZXRPcHRpb25cIiwgeyAnb3B0aW9uJzogdmFsdWUsIC4uLiB9LCByZWZyZXNoIClcbiAgICAgKi9cbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGwsXG4gICAgICAgIGl0ZW0sXG4gICAgICAgIG9wdGlvbixcbiAgICAgICAgdmFsdWUsXG4gICAgICAgIHJlZnJlc2ggPSBmYWxzZSxcbiAgICAgICAgdHlwZTtcblxuICAgIGlmICgkLnR5cGUoYXJndW1lbnRzWzBdKSA9PT0gJ29iamVjdCcpIHtcbiAgICAgIG9wdGlvbiA9IGFyZ3VtZW50c1swXTtcbiAgICAgIHJlZnJlc2ggPSBhcmd1bWVudHNbMV07XG4gICAgICB0eXBlID0gJ211bHRpcGxlJztcbiAgICB9IGVsc2UgaWYgKCQudHlwZShhcmd1bWVudHNbMF0pID09PSAnc3RyaW5nJykge1xuICAgICAgb3B0aW9uID0gYXJndW1lbnRzWzBdO1xuICAgICAgdmFsdWUgPSBhcmd1bWVudHNbMV07XG4gICAgICByZWZyZXNoID0gYXJndW1lbnRzWzJdO1xuXG4gICAgICBpZiAoYXJndW1lbnRzWzBdID09PSAncmVzcG9uc2l2ZScgJiYgJC50eXBlKGFyZ3VtZW50c1sxXSkgPT09ICdhcnJheScpIHtcbiAgICAgICAgdHlwZSA9ICdyZXNwb25zaXZlJztcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGFyZ3VtZW50c1sxXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdHlwZSA9ICdzaW5nbGUnO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0eXBlID09PSAnc2luZ2xlJykge1xuICAgICAgXy5vcHRpb25zW29wdGlvbl0gPSB2YWx1ZTtcbiAgICB9IGVsc2UgaWYgKHR5cGUgPT09ICdtdWx0aXBsZScpIHtcbiAgICAgICQuZWFjaChvcHRpb24sIGZ1bmN0aW9uIChvcHQsIHZhbCkge1xuICAgICAgICBfLm9wdGlvbnNbb3B0XSA9IHZhbDtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gJ3Jlc3BvbnNpdmUnKSB7XG4gICAgICBmb3IgKGl0ZW0gaW4gdmFsdWUpIHtcbiAgICAgICAgaWYgKCQudHlwZShfLm9wdGlvbnMucmVzcG9uc2l2ZSkgIT09ICdhcnJheScpIHtcbiAgICAgICAgICBfLm9wdGlvbnMucmVzcG9uc2l2ZSA9IFt2YWx1ZVtpdGVtXV07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbCA9IF8ub3B0aW9ucy5yZXNwb25zaXZlLmxlbmd0aCAtIDE7IC8vIGxvb3AgdGhyb3VnaCB0aGUgcmVzcG9uc2l2ZSBvYmplY3QgYW5kIHNwbGljZSBvdXQgZHVwbGljYXRlcy5cblxuICAgICAgICAgIHdoaWxlIChsID49IDApIHtcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMucmVzcG9uc2l2ZVtsXS5icmVha3BvaW50ID09PSB2YWx1ZVtpdGVtXS5icmVha3BvaW50KSB7XG4gICAgICAgICAgICAgIF8ub3B0aW9ucy5yZXNwb25zaXZlLnNwbGljZShsLCAxKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbC0tO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIF8ub3B0aW9ucy5yZXNwb25zaXZlLnB1c2godmFsdWVbaXRlbV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHJlZnJlc2gpIHtcbiAgICAgIF8udW5sb2FkKCk7XG5cbiAgICAgIF8ucmVpbml0KCk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5zZXRQb3NpdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBfLnNldERpbWVuc2lvbnMoKTtcblxuICAgIF8uc2V0SGVpZ2h0KCk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IGZhbHNlKSB7XG4gICAgICBfLnNldENTUyhfLmdldExlZnQoXy5jdXJyZW50U2xpZGUpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgXy5zZXRGYWRlKCk7XG4gICAgfVxuXG4gICAgXy4kc2xpZGVyLnRyaWdnZXIoJ3NldFBvc2l0aW9uJywgW19dKTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc2V0UHJvcHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBib2R5U3R5bGUgPSBkb2N1bWVudC5ib2R5LnN0eWxlO1xuXG4gICAgXy5wb3NpdGlvblByb3AgPSBfLm9wdGlvbnMudmVydGljYWwgPT09IHRydWUgPyAndG9wJyA6ICdsZWZ0JztcblxuICAgIGlmIChfLnBvc2l0aW9uUHJvcCA9PT0gJ3RvcCcpIHtcbiAgICAgIF8uJHNsaWRlci5hZGRDbGFzcygnc2xpY2stdmVydGljYWwnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgXy4kc2xpZGVyLnJlbW92ZUNsYXNzKCdzbGljay12ZXJ0aWNhbCcpO1xuICAgIH1cblxuICAgIGlmIChib2R5U3R5bGUuV2Via2l0VHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkIHx8IGJvZHlTdHlsZS5Nb3pUcmFuc2l0aW9uICE9PSB1bmRlZmluZWQgfHwgYm9keVN0eWxlLm1zVHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBpZiAoXy5vcHRpb25zLnVzZUNTUyA9PT0gdHJ1ZSkge1xuICAgICAgICBfLmNzc1RyYW5zaXRpb25zID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmZhZGUpIHtcbiAgICAgIGlmICh0eXBlb2YgXy5vcHRpb25zLnpJbmRleCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgaWYgKF8ub3B0aW9ucy56SW5kZXggPCAzKSB7XG4gICAgICAgICAgXy5vcHRpb25zLnpJbmRleCA9IDM7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIF8ub3B0aW9ucy56SW5kZXggPSBfLmRlZmF1bHRzLnpJbmRleDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoYm9keVN0eWxlLk9UcmFuc2Zvcm0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgXy5hbmltVHlwZSA9ICdPVHJhbnNmb3JtJztcbiAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctby10cmFuc2Zvcm0nO1xuICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICdPVHJhbnNpdGlvbic7XG4gICAgICBpZiAoYm9keVN0eWxlLnBlcnNwZWN0aXZlUHJvcGVydHkgPT09IHVuZGVmaW5lZCAmJiBib2R5U3R5bGUud2Via2l0UGVyc3BlY3RpdmUgPT09IHVuZGVmaW5lZCkgXy5hbmltVHlwZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIGlmIChib2R5U3R5bGUuTW96VHJhbnNmb3JtICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIF8uYW5pbVR5cGUgPSAnTW96VHJhbnNmb3JtJztcbiAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctbW96LXRyYW5zZm9ybSc7XG4gICAgICBfLnRyYW5zaXRpb25UeXBlID0gJ01velRyYW5zaXRpb24nO1xuICAgICAgaWYgKGJvZHlTdHlsZS5wZXJzcGVjdGl2ZVByb3BlcnR5ID09PSB1bmRlZmluZWQgJiYgYm9keVN0eWxlLk1velBlcnNwZWN0aXZlID09PSB1bmRlZmluZWQpIF8uYW5pbVR5cGUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBpZiAoYm9keVN0eWxlLndlYmtpdFRyYW5zZm9ybSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBfLmFuaW1UeXBlID0gJ3dlYmtpdFRyYW5zZm9ybSc7XG4gICAgICBfLnRyYW5zZm9ybVR5cGUgPSAnLXdlYmtpdC10cmFuc2Zvcm0nO1xuICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICd3ZWJraXRUcmFuc2l0aW9uJztcbiAgICAgIGlmIChib2R5U3R5bGUucGVyc3BlY3RpdmVQcm9wZXJ0eSA9PT0gdW5kZWZpbmVkICYmIGJvZHlTdHlsZS53ZWJraXRQZXJzcGVjdGl2ZSA9PT0gdW5kZWZpbmVkKSBfLmFuaW1UeXBlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKGJvZHlTdHlsZS5tc1RyYW5zZm9ybSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBfLmFuaW1UeXBlID0gJ21zVHJhbnNmb3JtJztcbiAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctbXMtdHJhbnNmb3JtJztcbiAgICAgIF8udHJhbnNpdGlvblR5cGUgPSAnbXNUcmFuc2l0aW9uJztcbiAgICAgIGlmIChib2R5U3R5bGUubXNUcmFuc2Zvcm0gPT09IHVuZGVmaW5lZCkgXy5hbmltVHlwZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIGlmIChib2R5U3R5bGUudHJhbnNmb3JtICE9PSB1bmRlZmluZWQgJiYgXy5hbmltVHlwZSAhPT0gZmFsc2UpIHtcbiAgICAgIF8uYW5pbVR5cGUgPSAndHJhbnNmb3JtJztcbiAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICd0cmFuc2Zvcm0nO1xuICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICd0cmFuc2l0aW9uJztcbiAgICB9XG5cbiAgICBfLnRyYW5zZm9ybXNFbmFibGVkID0gXy5vcHRpb25zLnVzZVRyYW5zZm9ybSAmJiBfLmFuaW1UeXBlICE9PSBudWxsICYmIF8uYW5pbVR5cGUgIT09IGZhbHNlO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5zZXRTbGlkZUNsYXNzZXMgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGNlbnRlck9mZnNldCxcbiAgICAgICAgYWxsU2xpZGVzLFxuICAgICAgICBpbmRleE9mZnNldCxcbiAgICAgICAgcmVtYWluZGVyO1xuXG4gICAgYWxsU2xpZGVzID0gXy4kc2xpZGVyLmZpbmQoJy5zbGljay1zbGlkZScpLnJlbW92ZUNsYXNzKCdzbGljay1hY3RpdmUgc2xpY2stY2VudGVyIHNsaWNrLWN1cnJlbnQnKS5hdHRyKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG5cbiAgICBfLiRzbGlkZXMuZXEoaW5kZXgpLmFkZENsYXNzKCdzbGljay1jdXJyZW50Jyk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcbiAgICAgIGNlbnRlck9mZnNldCA9IE1hdGguZmxvb3IoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIpO1xuXG4gICAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlKSB7XG4gICAgICAgIGlmIChpbmRleCA+PSBjZW50ZXJPZmZzZXQgJiYgaW5kZXggPD0gXy5zbGlkZUNvdW50IC0gMSAtIGNlbnRlck9mZnNldCkge1xuICAgICAgICAgIF8uJHNsaWRlcy5zbGljZShpbmRleCAtIGNlbnRlck9mZnNldCwgaW5kZXggKyBjZW50ZXJPZmZzZXQgKyAxKS5hZGRDbGFzcygnc2xpY2stYWN0aXZlJykuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpbmRleE9mZnNldCA9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyBpbmRleDtcbiAgICAgICAgICBhbGxTbGlkZXMuc2xpY2UoaW5kZXhPZmZzZXQgLSBjZW50ZXJPZmZzZXQgKyAxLCBpbmRleE9mZnNldCArIGNlbnRlck9mZnNldCArIDIpLmFkZENsYXNzKCdzbGljay1hY3RpdmUnKS5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGluZGV4ID09PSAwKSB7XG4gICAgICAgICAgYWxsU2xpZGVzLmVxKGFsbFNsaWRlcy5sZW5ndGggLSAxIC0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykuYWRkQ2xhc3MoJ3NsaWNrLWNlbnRlcicpO1xuICAgICAgICB9IGVsc2UgaWYgKGluZGV4ID09PSBfLnNsaWRlQ291bnQgLSAxKSB7XG4gICAgICAgICAgYWxsU2xpZGVzLmVxKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpLmFkZENsYXNzKCdzbGljay1jZW50ZXInKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBfLiRzbGlkZXMuZXEoaW5kZXgpLmFkZENsYXNzKCdzbGljay1jZW50ZXInKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGluZGV4ID49IDAgJiYgaW5kZXggPD0gXy5zbGlkZUNvdW50IC0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICBfLiRzbGlkZXMuc2xpY2UoaW5kZXgsIGluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdykuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG4gICAgICB9IGVsc2UgaWYgKGFsbFNsaWRlcy5sZW5ndGggPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICBhbGxTbGlkZXMuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZW1haW5kZXIgPSBfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xuICAgICAgICBpbmRleE9mZnNldCA9IF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gdHJ1ZSA/IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyBpbmRleCA6IGluZGV4O1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID09IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAmJiBfLnNsaWRlQ291bnQgLSBpbmRleCA8IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICBhbGxTbGlkZXMuc2xpY2UoaW5kZXhPZmZzZXQgLSAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAtIHJlbWFpbmRlciksIGluZGV4T2Zmc2V0ICsgcmVtYWluZGVyKS5hZGRDbGFzcygnc2xpY2stYWN0aXZlJykuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBhbGxTbGlkZXMuc2xpY2UoaW5kZXhPZmZzZXQsIGluZGV4T2Zmc2V0ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdykuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmxhenlMb2FkID09PSAnb25kZW1hbmQnKSB7XG4gICAgICBfLmxhenlMb2FkKCk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5zZXR1cEluZmluaXRlID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgaSxcbiAgICAgICAgc2xpZGVJbmRleCxcbiAgICAgICAgaW5maW5pdGVDb3VudDtcblxuICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xuICAgICAgXy5vcHRpb25zLmNlbnRlck1vZGUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlICYmIF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xuICAgICAgc2xpZGVJbmRleCA9IG51bGw7XG5cbiAgICAgIGlmIChfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGluZmluaXRlQ291bnQgPSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgMTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpbmZpbml0ZUNvdW50ID0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdztcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAoaSA9IF8uc2xpZGVDb3VudDsgaSA+IF8uc2xpZGVDb3VudCAtIGluZmluaXRlQ291bnQ7IGkgLT0gMSkge1xuICAgICAgICAgIHNsaWRlSW5kZXggPSBpIC0gMTtcbiAgICAgICAgICAkKF8uJHNsaWRlc1tzbGlkZUluZGV4XSkuY2xvbmUodHJ1ZSkuYXR0cignaWQnLCAnJykuYXR0cignZGF0YS1zbGljay1pbmRleCcsIHNsaWRlSW5kZXggLSBfLnNsaWRlQ291bnQpLnByZXBlbmRUbyhfLiRzbGlkZVRyYWNrKS5hZGRDbGFzcygnc2xpY2stY2xvbmVkJyk7XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKGkgPSAwOyBpIDwgaW5maW5pdGVDb3VudDsgaSArPSAxKSB7XG4gICAgICAgICAgc2xpZGVJbmRleCA9IGk7XG4gICAgICAgICAgJChfLiRzbGlkZXNbc2xpZGVJbmRleF0pLmNsb25lKHRydWUpLmF0dHIoJ2lkJywgJycpLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnLCBzbGlkZUluZGV4ICsgXy5zbGlkZUNvdW50KS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKS5hZGRDbGFzcygnc2xpY2stY2xvbmVkJyk7XG4gICAgICAgIH1cblxuICAgICAgICBfLiRzbGlkZVRyYWNrLmZpbmQoJy5zbGljay1jbG9uZWQnKS5maW5kKCdbaWRdJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgJCh0aGlzKS5hdHRyKCdpZCcsICcnKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5pbnRlcnJ1cHQgPSBmdW5jdGlvbiAodG9nZ2xlKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgaWYgKCF0b2dnbGUpIHtcbiAgICAgIF8uYXV0b1BsYXkoKTtcbiAgICB9XG5cbiAgICBfLmludGVycnVwdGVkID0gdG9nZ2xlO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5zZWxlY3RIYW5kbGVyID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgdmFyIHRhcmdldEVsZW1lbnQgPSAkKGV2ZW50LnRhcmdldCkuaXMoJy5zbGljay1zbGlkZScpID8gJChldmVudC50YXJnZXQpIDogJChldmVudC50YXJnZXQpLnBhcmVudHMoJy5zbGljay1zbGlkZScpO1xuICAgIHZhciBpbmRleCA9IHBhcnNlSW50KHRhcmdldEVsZW1lbnQuYXR0cignZGF0YS1zbGljay1pbmRleCcpKTtcbiAgICBpZiAoIWluZGV4KSBpbmRleCA9IDA7XG5cbiAgICBpZiAoXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8uc2V0U2xpZGVDbGFzc2VzKGluZGV4KTtcblxuICAgICAgXy5hc05hdkZvcihpbmRleCk7XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBfLnNsaWRlSGFuZGxlcihpbmRleCk7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLnNsaWRlSGFuZGxlciA9IGZ1bmN0aW9uIChpbmRleCwgc3luYywgZG9udEFuaW1hdGUpIHtcbiAgICB2YXIgdGFyZ2V0U2xpZGUsXG4gICAgICAgIGFuaW1TbGlkZSxcbiAgICAgICAgb2xkU2xpZGUsXG4gICAgICAgIHNsaWRlTGVmdCxcbiAgICAgICAgdGFyZ2V0TGVmdCA9IG51bGwsXG4gICAgICAgIF8gPSB0aGlzLFxuICAgICAgICBuYXZUYXJnZXQ7XG5cbiAgICBzeW5jID0gc3luYyB8fCBmYWxzZTtcblxuICAgIGlmIChfLmFuaW1hdGluZyA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMud2FpdEZvckFuaW1hdGUgPT09IHRydWUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IHRydWUgJiYgXy5jdXJyZW50U2xpZGUgPT09IGluZGV4KSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHN5bmMgPT09IGZhbHNlKSB7XG4gICAgICBfLmFzTmF2Rm9yKGluZGV4KTtcbiAgICB9XG5cbiAgICB0YXJnZXRTbGlkZSA9IGluZGV4O1xuICAgIHRhcmdldExlZnQgPSBfLmdldExlZnQodGFyZ2V0U2xpZGUpO1xuICAgIHNsaWRlTGVmdCA9IF8uZ2V0TGVmdChfLmN1cnJlbnRTbGlkZSk7XG4gICAgXy5jdXJyZW50TGVmdCA9IF8uc3dpcGVMZWZ0ID09PSBudWxsID8gc2xpZGVMZWZ0IDogXy5zd2lwZUxlZnQ7XG5cbiAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSBmYWxzZSAmJiBfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gZmFsc2UgJiYgKGluZGV4IDwgMCB8fCBpbmRleCA+IF8uZ2V0RG90Q291bnQoKSAqIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCkpIHtcbiAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgdGFyZ2V0U2xpZGUgPSBfLmN1cnJlbnRTbGlkZTtcblxuICAgICAgICBpZiAoZG9udEFuaW1hdGUgIT09IHRydWUpIHtcbiAgICAgICAgICBfLmFuaW1hdGVTbGlkZShzbGlkZUxlZnQsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIF8ucG9zdFNsaWRlKHRhcmdldFNsaWRlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBfLnBvc3RTbGlkZSh0YXJnZXRTbGlkZSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuO1xuICAgIH0gZWxzZSBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSBmYWxzZSAmJiBfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSAmJiAoaW5kZXggPCAwIHx8IGluZGV4ID4gXy5zbGlkZUNvdW50IC0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsKSkge1xuICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xuICAgICAgICB0YXJnZXRTbGlkZSA9IF8uY3VycmVudFNsaWRlO1xuXG4gICAgICAgIGlmIChkb250QW5pbWF0ZSAhPT0gdHJ1ZSkge1xuICAgICAgICAgIF8uYW5pbWF0ZVNsaWRlKHNsaWRlTGVmdCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgXy5wb3N0U2xpZGUodGFyZ2V0U2xpZGUpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF8ucG9zdFNsaWRlKHRhcmdldFNsaWRlKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKF8ub3B0aW9ucy5hdXRvcGxheSkge1xuICAgICAgY2xlYXJJbnRlcnZhbChfLmF1dG9QbGF5VGltZXIpO1xuICAgIH1cblxuICAgIGlmICh0YXJnZXRTbGlkZSA8IDApIHtcbiAgICAgIGlmIChfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgIT09IDApIHtcbiAgICAgICAgYW5pbVNsaWRlID0gXy5zbGlkZUNvdW50IC0gXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYW5pbVNsaWRlID0gXy5zbGlkZUNvdW50ICsgdGFyZ2V0U2xpZGU7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICh0YXJnZXRTbGlkZSA+PSBfLnNsaWRlQ291bnQpIHtcbiAgICAgIGlmIChfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgIT09IDApIHtcbiAgICAgICAgYW5pbVNsaWRlID0gMDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGFuaW1TbGlkZSA9IHRhcmdldFNsaWRlIC0gXy5zbGlkZUNvdW50O1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBhbmltU2xpZGUgPSB0YXJnZXRTbGlkZTtcbiAgICB9XG5cbiAgICBfLmFuaW1hdGluZyA9IHRydWU7XG5cbiAgICBfLiRzbGlkZXIudHJpZ2dlcignYmVmb3JlQ2hhbmdlJywgW18sIF8uY3VycmVudFNsaWRlLCBhbmltU2xpZGVdKTtcblxuICAgIG9sZFNsaWRlID0gXy5jdXJyZW50U2xpZGU7XG4gICAgXy5jdXJyZW50U2xpZGUgPSBhbmltU2xpZGU7XG5cbiAgICBfLnNldFNsaWRlQ2xhc3NlcyhfLmN1cnJlbnRTbGlkZSk7XG5cbiAgICBpZiAoXy5vcHRpb25zLmFzTmF2Rm9yKSB7XG4gICAgICBuYXZUYXJnZXQgPSBfLmdldE5hdlRhcmdldCgpO1xuICAgICAgbmF2VGFyZ2V0ID0gbmF2VGFyZ2V0LnNsaWNrKCdnZXRTbGljaycpO1xuXG4gICAgICBpZiAobmF2VGFyZ2V0LnNsaWRlQ291bnQgPD0gbmF2VGFyZ2V0Lm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgIG5hdlRhcmdldC5zZXRTbGlkZUNsYXNzZXMoXy5jdXJyZW50U2xpZGUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIF8udXBkYXRlRG90cygpO1xuXG4gICAgXy51cGRhdGVBcnJvd3MoKTtcblxuICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKGRvbnRBbmltYXRlICE9PSB0cnVlKSB7XG4gICAgICAgIF8uZmFkZVNsaWRlT3V0KG9sZFNsaWRlKTtcblxuICAgICAgICBfLmZhZGVTbGlkZShhbmltU2xpZGUsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBfLnBvc3RTbGlkZShhbmltU2xpZGUpO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIF8ucG9zdFNsaWRlKGFuaW1TbGlkZSk7XG4gICAgICB9XG5cbiAgICAgIF8uYW5pbWF0ZUhlaWdodCgpO1xuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKGRvbnRBbmltYXRlICE9PSB0cnVlKSB7XG4gICAgICBfLmFuaW1hdGVTbGlkZSh0YXJnZXRMZWZ0LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIF8ucG9zdFNsaWRlKGFuaW1TbGlkZSk7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgXy5wb3N0U2xpZGUoYW5pbVNsaWRlKTtcbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLnN0YXJ0TG9hZCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy5vcHRpb25zLmFycm93cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICBfLiRwcmV2QXJyb3cuaGlkZSgpO1xuXG4gICAgICBfLiRuZXh0QXJyb3cuaGlkZSgpO1xuICAgIH1cblxuICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICBfLiRkb3RzLmhpZGUoKTtcbiAgICB9XG5cbiAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc3dpcGVEaXJlY3Rpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHhEaXN0LFxuICAgICAgICB5RGlzdCxcbiAgICAgICAgcixcbiAgICAgICAgc3dpcGVBbmdsZSxcbiAgICAgICAgXyA9IHRoaXM7XG5cbiAgICB4RGlzdCA9IF8udG91Y2hPYmplY3Quc3RhcnRYIC0gXy50b3VjaE9iamVjdC5jdXJYO1xuICAgIHlEaXN0ID0gXy50b3VjaE9iamVjdC5zdGFydFkgLSBfLnRvdWNoT2JqZWN0LmN1clk7XG4gICAgciA9IE1hdGguYXRhbjIoeURpc3QsIHhEaXN0KTtcbiAgICBzd2lwZUFuZ2xlID0gTWF0aC5yb3VuZChyICogMTgwIC8gTWF0aC5QSSk7XG5cbiAgICBpZiAoc3dpcGVBbmdsZSA8IDApIHtcbiAgICAgIHN3aXBlQW5nbGUgPSAzNjAgLSBNYXRoLmFicyhzd2lwZUFuZ2xlKTtcbiAgICB9XG5cbiAgICBpZiAoc3dpcGVBbmdsZSA8PSA0NSAmJiBzd2lwZUFuZ2xlID49IDApIHtcbiAgICAgIHJldHVybiBfLm9wdGlvbnMucnRsID09PSBmYWxzZSA/ICdsZWZ0JyA6ICdyaWdodCc7XG4gICAgfVxuXG4gICAgaWYgKHN3aXBlQW5nbGUgPD0gMzYwICYmIHN3aXBlQW5nbGUgPj0gMzE1KSB7XG4gICAgICByZXR1cm4gXy5vcHRpb25zLnJ0bCA9PT0gZmFsc2UgPyAnbGVmdCcgOiAncmlnaHQnO1xuICAgIH1cblxuICAgIGlmIChzd2lwZUFuZ2xlID49IDEzNSAmJiBzd2lwZUFuZ2xlIDw9IDIyNSkge1xuICAgICAgcmV0dXJuIF8ub3B0aW9ucy5ydGwgPT09IGZhbHNlID8gJ3JpZ2h0JyA6ICdsZWZ0JztcbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKHN3aXBlQW5nbGUgPj0gMzUgJiYgc3dpcGVBbmdsZSA8PSAxMzUpIHtcbiAgICAgICAgcmV0dXJuICdkb3duJztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAndXAnO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiAndmVydGljYWwnO1xuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS5zd2lwZUVuZCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgc2xpZGVDb3VudCxcbiAgICAgICAgZGlyZWN0aW9uO1xuXG4gICAgXy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgIF8uaW50ZXJydXB0ZWQgPSBmYWxzZTtcbiAgICBfLnNob3VsZENsaWNrID0gXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA+IDEwID8gZmFsc2UgOiB0cnVlO1xuXG4gICAgaWYgKF8udG91Y2hPYmplY3QuY3VyWCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKF8udG91Y2hPYmplY3QuZWRnZUhpdCA9PT0gdHJ1ZSkge1xuICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2VkZ2UnLCBbXywgXy5zd2lwZURpcmVjdGlvbigpXSk7XG4gICAgfVxuXG4gICAgaWYgKF8udG91Y2hPYmplY3Quc3dpcGVMZW5ndGggPj0gXy50b3VjaE9iamVjdC5taW5Td2lwZSkge1xuICAgICAgZGlyZWN0aW9uID0gXy5zd2lwZURpcmVjdGlvbigpO1xuXG4gICAgICBzd2l0Y2ggKGRpcmVjdGlvbikge1xuICAgICAgICBjYXNlICdsZWZ0JzpcbiAgICAgICAgY2FzZSAnZG93bic6XG4gICAgICAgICAgc2xpZGVDb3VudCA9IF8ub3B0aW9ucy5zd2lwZVRvU2xpZGUgPyBfLmNoZWNrTmF2aWdhYmxlKF8uY3VycmVudFNsaWRlICsgXy5nZXRTbGlkZUNvdW50KCkpIDogXy5jdXJyZW50U2xpZGUgKyBfLmdldFNsaWRlQ291bnQoKTtcbiAgICAgICAgICBfLmN1cnJlbnREaXJlY3Rpb24gPSAwO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ3JpZ2h0JzpcbiAgICAgICAgY2FzZSAndXAnOlxuICAgICAgICAgIHNsaWRlQ291bnQgPSBfLm9wdGlvbnMuc3dpcGVUb1NsaWRlID8gXy5jaGVja05hdmlnYWJsZShfLmN1cnJlbnRTbGlkZSAtIF8uZ2V0U2xpZGVDb3VudCgpKSA6IF8uY3VycmVudFNsaWRlIC0gXy5nZXRTbGlkZUNvdW50KCk7XG4gICAgICAgICAgXy5jdXJyZW50RGlyZWN0aW9uID0gMTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBkZWZhdWx0OlxuICAgICAgfVxuXG4gICAgICBpZiAoZGlyZWN0aW9uICE9ICd2ZXJ0aWNhbCcpIHtcbiAgICAgICAgXy5zbGlkZUhhbmRsZXIoc2xpZGVDb3VudCk7XG5cbiAgICAgICAgXy50b3VjaE9iamVjdCA9IHt9O1xuXG4gICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdzd2lwZScsIFtfLCBkaXJlY3Rpb25dKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKF8udG91Y2hPYmplY3Quc3RhcnRYICE9PSBfLnRvdWNoT2JqZWN0LmN1clgpIHtcbiAgICAgICAgXy5zbGlkZUhhbmRsZXIoXy5jdXJyZW50U2xpZGUpO1xuXG4gICAgICAgIF8udG91Y2hPYmplY3QgPSB7fTtcbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLnN3aXBlSGFuZGxlciA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIGlmIChfLm9wdGlvbnMuc3dpcGUgPT09IGZhbHNlIHx8ICdvbnRvdWNoZW5kJyBpbiBkb2N1bWVudCAmJiBfLm9wdGlvbnMuc3dpcGUgPT09IGZhbHNlKSB7XG4gICAgICByZXR1cm47XG4gICAgfSBlbHNlIGlmIChfLm9wdGlvbnMuZHJhZ2dhYmxlID09PSBmYWxzZSAmJiBldmVudC50eXBlLmluZGV4T2YoJ21vdXNlJykgIT09IC0xKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgXy50b3VjaE9iamVjdC5maW5nZXJDb3VudCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQgJiYgZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzICE9PSB1bmRlZmluZWQgPyBldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMubGVuZ3RoIDogMTtcbiAgICBfLnRvdWNoT2JqZWN0Lm1pblN3aXBlID0gXy5saXN0V2lkdGggLyBfLm9wdGlvbnMudG91Y2hUaHJlc2hvbGQ7XG5cbiAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xuICAgICAgXy50b3VjaE9iamVjdC5taW5Td2lwZSA9IF8ubGlzdEhlaWdodCAvIF8ub3B0aW9ucy50b3VjaFRocmVzaG9sZDtcbiAgICB9XG5cbiAgICBzd2l0Y2ggKGV2ZW50LmRhdGEuYWN0aW9uKSB7XG4gICAgICBjYXNlICdzdGFydCc6XG4gICAgICAgIF8uc3dpcGVTdGFydChldmVudCk7XG5cbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJ21vdmUnOlxuICAgICAgICBfLnN3aXBlTW92ZShldmVudCk7XG5cbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJ2VuZCc6XG4gICAgICAgIF8uc3dpcGVFbmQoZXZlbnQpO1xuXG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc3dpcGVNb3ZlID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBlZGdlV2FzSGl0ID0gZmFsc2UsXG4gICAgICAgIGN1ckxlZnQsXG4gICAgICAgIHN3aXBlRGlyZWN0aW9uLFxuICAgICAgICBzd2lwZUxlbmd0aCxcbiAgICAgICAgcG9zaXRpb25PZmZzZXQsXG4gICAgICAgIHRvdWNoZXM7XG5cbiAgICB0b3VjaGVzID0gZXZlbnQub3JpZ2luYWxFdmVudCAhPT0gdW5kZWZpbmVkID8gZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzIDogbnVsbDtcblxuICAgIGlmICghXy5kcmFnZ2luZyB8fCB0b3VjaGVzICYmIHRvdWNoZXMubGVuZ3RoICE9PSAxKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgY3VyTGVmdCA9IF8uZ2V0TGVmdChfLmN1cnJlbnRTbGlkZSk7XG4gICAgXy50b3VjaE9iamVjdC5jdXJYID0gdG91Y2hlcyAhPT0gdW5kZWZpbmVkID8gdG91Y2hlc1swXS5wYWdlWCA6IGV2ZW50LmNsaWVudFg7XG4gICAgXy50b3VjaE9iamVjdC5jdXJZID0gdG91Y2hlcyAhPT0gdW5kZWZpbmVkID8gdG91Y2hlc1swXS5wYWdlWSA6IGV2ZW50LmNsaWVudFk7XG4gICAgXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA9IE1hdGgucm91bmQoTWF0aC5zcXJ0KE1hdGgucG93KF8udG91Y2hPYmplY3QuY3VyWCAtIF8udG91Y2hPYmplY3Quc3RhcnRYLCAyKSkpO1xuXG4gICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcbiAgICAgIF8udG91Y2hPYmplY3Quc3dpcGVMZW5ndGggPSBNYXRoLnJvdW5kKE1hdGguc3FydChNYXRoLnBvdyhfLnRvdWNoT2JqZWN0LmN1clkgLSBfLnRvdWNoT2JqZWN0LnN0YXJ0WSwgMikpKTtcbiAgICB9XG5cbiAgICBzd2lwZURpcmVjdGlvbiA9IF8uc3dpcGVEaXJlY3Rpb24oKTtcblxuICAgIGlmIChzd2lwZURpcmVjdGlvbiA9PT0gJ3ZlcnRpY2FsJykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChldmVudC5vcmlnaW5hbEV2ZW50ICE9PSB1bmRlZmluZWQgJiYgXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA+IDQpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfVxuXG4gICAgcG9zaXRpb25PZmZzZXQgPSAoXy5vcHRpb25zLnJ0bCA9PT0gZmFsc2UgPyAxIDogLTEpICogKF8udG91Y2hPYmplY3QuY3VyWCA+IF8udG91Y2hPYmplY3Quc3RhcnRYID8gMSA6IC0xKTtcblxuICAgIGlmIChfLm9wdGlvbnMudmVydGljYWxTd2lwaW5nID09PSB0cnVlKSB7XG4gICAgICBwb3NpdGlvbk9mZnNldCA9IF8udG91Y2hPYmplY3QuY3VyWSA+IF8udG91Y2hPYmplY3Quc3RhcnRZID8gMSA6IC0xO1xuICAgIH1cblxuICAgIHN3aXBlTGVuZ3RoID0gXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aDtcbiAgICBfLnRvdWNoT2JqZWN0LmVkZ2VIaXQgPSBmYWxzZTtcblxuICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IGZhbHNlKSB7XG4gICAgICBpZiAoXy5jdXJyZW50U2xpZGUgPT09IDAgJiYgc3dpcGVEaXJlY3Rpb24gPT09ICdyaWdodCcgfHwgXy5jdXJyZW50U2xpZGUgPj0gXy5nZXREb3RDb3VudCgpICYmIHN3aXBlRGlyZWN0aW9uID09PSAnbGVmdCcpIHtcbiAgICAgICAgc3dpcGVMZW5ndGggPSBfLnRvdWNoT2JqZWN0LnN3aXBlTGVuZ3RoICogXy5vcHRpb25zLmVkZ2VGcmljdGlvbjtcbiAgICAgICAgXy50b3VjaE9iamVjdC5lZGdlSGl0ID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgXy5zd2lwZUxlZnQgPSBjdXJMZWZ0ICsgc3dpcGVMZW5ndGggKiBwb3NpdGlvbk9mZnNldDtcbiAgICB9IGVsc2Uge1xuICAgICAgXy5zd2lwZUxlZnQgPSBjdXJMZWZ0ICsgc3dpcGVMZW5ndGggKiAoXy4kbGlzdC5oZWlnaHQoKSAvIF8ubGlzdFdpZHRoKSAqIHBvc2l0aW9uT2Zmc2V0O1xuICAgIH1cblxuICAgIGlmIChfLm9wdGlvbnMudmVydGljYWxTd2lwaW5nID09PSB0cnVlKSB7XG4gICAgICBfLnN3aXBlTGVmdCA9IGN1ckxlZnQgKyBzd2lwZUxlbmd0aCAqIHBvc2l0aW9uT2Zmc2V0O1xuICAgIH1cblxuICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSB8fCBfLm9wdGlvbnMudG91Y2hNb3ZlID09PSBmYWxzZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGlmIChfLmFuaW1hdGluZyA9PT0gdHJ1ZSkge1xuICAgICAgXy5zd2lwZUxlZnQgPSBudWxsO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIF8uc2V0Q1NTKF8uc3dpcGVMZWZ0KTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUuc3dpcGVTdGFydCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgdG91Y2hlcztcblxuICAgIF8uaW50ZXJydXB0ZWQgPSB0cnVlO1xuXG4gICAgaWYgKF8udG91Y2hPYmplY3QuZmluZ2VyQ291bnQgIT09IDEgfHwgXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIF8udG91Y2hPYmplY3QgPSB7fTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBpZiAoZXZlbnQub3JpZ2luYWxFdmVudCAhPT0gdW5kZWZpbmVkICYmIGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlcyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICB0b3VjaGVzID0gZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdO1xuICAgIH1cblxuICAgIF8udG91Y2hPYmplY3Quc3RhcnRYID0gXy50b3VjaE9iamVjdC5jdXJYID0gdG91Y2hlcyAhPT0gdW5kZWZpbmVkID8gdG91Y2hlcy5wYWdlWCA6IGV2ZW50LmNsaWVudFg7XG4gICAgXy50b3VjaE9iamVjdC5zdGFydFkgPSBfLnRvdWNoT2JqZWN0LmN1clkgPSB0b3VjaGVzICE9PSB1bmRlZmluZWQgPyB0b3VjaGVzLnBhZ2VZIDogZXZlbnQuY2xpZW50WTtcbiAgICBfLmRyYWdnaW5nID0gdHJ1ZTtcbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUudW5maWx0ZXJTbGlkZXMgPSBTbGljay5wcm90b3R5cGUuc2xpY2tVbmZpbHRlciA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy4kc2xpZGVzQ2FjaGUgIT09IG51bGwpIHtcbiAgICAgIF8udW5sb2FkKCk7XG5cbiAgICAgIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4odGhpcy5vcHRpb25zLnNsaWRlKS5kZXRhY2goKTtcblxuICAgICAgXy4kc2xpZGVzQ2FjaGUuYXBwZW5kVG8oXy4kc2xpZGVUcmFjayk7XG5cbiAgICAgIF8ucmVpbml0KCk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS51bmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgJCgnLnNsaWNrLWNsb25lZCcsIF8uJHNsaWRlcikucmVtb3ZlKCk7XG5cbiAgICBpZiAoXy4kZG90cykge1xuICAgICAgXy4kZG90cy5yZW1vdmUoKTtcbiAgICB9XG5cbiAgICBpZiAoXy4kcHJldkFycm93ICYmIF8uaHRtbEV4cHIudGVzdChfLm9wdGlvbnMucHJldkFycm93KSkge1xuICAgICAgXy4kcHJldkFycm93LnJlbW92ZSgpO1xuICAgIH1cblxuICAgIGlmIChfLiRuZXh0QXJyb3cgJiYgXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5uZXh0QXJyb3cpKSB7XG4gICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlKCk7XG4gICAgfVxuXG4gICAgXy4kc2xpZGVzLnJlbW92ZUNsYXNzKCdzbGljay1zbGlkZSBzbGljay1hY3RpdmUgc2xpY2stdmlzaWJsZSBzbGljay1jdXJyZW50JykuYXR0cignYXJpYS1oaWRkZW4nLCAndHJ1ZScpLmNzcygnd2lkdGgnLCAnJyk7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLnVuc2xpY2sgPSBmdW5jdGlvbiAoZnJvbUJyZWFrcG9pbnQpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBfLiRzbGlkZXIudHJpZ2dlcigndW5zbGljaycsIFtfLCBmcm9tQnJlYWtwb2ludF0pO1xuXG4gICAgXy5kZXN0cm95KCk7XG4gIH07XG5cbiAgU2xpY2sucHJvdG90eXBlLnVwZGF0ZUFycm93cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgIGNlbnRlck9mZnNldDtcblxuICAgIGNlbnRlck9mZnNldCA9IE1hdGguZmxvb3IoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIpO1xuXG4gICAgaWYgKF8ub3B0aW9ucy5hcnJvd3MgPT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAmJiAhXy5vcHRpb25zLmluZmluaXRlKSB7XG4gICAgICBfLiRwcmV2QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICdmYWxzZScpO1xuXG4gICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICdmYWxzZScpO1xuXG4gICAgICBpZiAoXy5jdXJyZW50U2xpZGUgPT09IDApIHtcbiAgICAgICAgXy4kcHJldkFycm93LmFkZENsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAndHJ1ZScpO1xuXG4gICAgICAgIF8uJG5leHRBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ2ZhbHNlJyk7XG4gICAgICB9IGVsc2UgaWYgKF8uY3VycmVudFNsaWRlID49IF8uc2xpZGVDb3VudCAtIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgJiYgXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IGZhbHNlKSB7XG4gICAgICAgIF8uJG5leHRBcnJvdy5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcblxuICAgICAgICBfLiRwcmV2QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICdmYWxzZScpO1xuICAgICAgfSBlbHNlIGlmIChfLmN1cnJlbnRTbGlkZSA+PSBfLnNsaWRlQ291bnQgLSAxICYmIF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgIF8uJG5leHRBcnJvdy5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcblxuICAgICAgICBfLiRwcmV2QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICdmYWxzZScpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTbGljay5wcm90b3R5cGUudXBkYXRlRG90cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICBpZiAoXy4kZG90cyAhPT0gbnVsbCkge1xuICAgICAgXy4kZG90cy5maW5kKCdsaScpLnJlbW92ZUNsYXNzKCdzbGljay1hY3RpdmUnKS5hdHRyKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG5cbiAgICAgIF8uJGRvdHMuZmluZCgnbGknKS5lcShNYXRoLmZsb29yKF8uY3VycmVudFNsaWRlIC8gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsKSkuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG4gICAgfVxuICB9O1xuXG4gIFNsaWNrLnByb3RvdHlwZS52aXNpYmlsaXR5ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfID0gdGhpcztcblxuICAgIGlmIChfLm9wdGlvbnMuYXV0b3BsYXkpIHtcbiAgICAgIGlmIChkb2N1bWVudFtfLmhpZGRlbl0pIHtcbiAgICAgICAgXy5pbnRlcnJ1cHRlZCA9IHRydWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBfLmludGVycnVwdGVkID0gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gICQuZm4uc2xpY2sgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICBvcHQgPSBhcmd1bWVudHNbMF0sXG4gICAgICAgIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuICAgICAgICBsID0gXy5sZW5ndGgsXG4gICAgICAgIGksXG4gICAgICAgIHJldDtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBsOyBpKyspIHtcbiAgICAgIGlmICh0eXBlb2Ygb3B0ID09ICdvYmplY3QnIHx8IHR5cGVvZiBvcHQgPT0gJ3VuZGVmaW5lZCcpIF9baV0uc2xpY2sgPSBuZXcgU2xpY2soX1tpXSwgb3B0KTtlbHNlIHJldCA9IF9baV0uc2xpY2tbb3B0XS5hcHBseShfW2ldLnNsaWNrLCBhcmdzKTtcbiAgICAgIGlmICh0eXBlb2YgcmV0ICE9ICd1bmRlZmluZWQnKSByZXR1cm4gcmV0O1xuICAgIH1cblxuICAgIHJldHVybiBfO1xuICB9O1xufSk7XG47XG47XG47XG47XG47XG5cbiIsbnVsbCwiKGZ1bmN0aW9uKCQpIHt9KShqUXVlcnkpO1xuIiwiKGZ1bmN0aW9uKCQpIHtcbiAgY29uc3QgX3RvZ2dsZUhlYWRlckZvcm0gPSBmdW5jdGlvbigpIHtcbiAgICAkKFwiLmhlYWRlci1mb3JtXCIpLnRvZ2dsZUNsYXNzKFwiYWN0aXZlXCIpO1xuICB9O1xuXG4gICQoXCIubm90aWNlXCIpLm9uKFwiY2xpY2tcIiwgX3RvZ2dsZUhlYWRlckZvcm0pO1xufSkoalF1ZXJ5KTtcbiIsIi8qKlxuICogQmFzaWMgU2xpZGVyXG4gKi9cbihmdW5jdGlvbigkKSB7XG4gIGlmICgkKFwiLnNsaWRlclwiKS5sZW5ndGggPCAxKSB7XG4gICAgcmV0dXJuO1xuICB9XG4gICQoXCIuc2xpZGVyXCIpLnNsaWNrKHtcbiAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgYXJyb3dzOiBmYWxzZVxuICB9KTtcbn0pKGpRdWVyeSk7XG5cbi8qKlxuICogU2VjdGlvbiBTbGlkZXJcbiAqL1xuKGZ1bmN0aW9uKCQpIHtcbiAgaWYgKCQoXCIuc2VjdGlvbi1zbGlkZXJcIikubGVuZ3RoIDwgMSkge1xuICAgIHJldHVybjtcbiAgfVxuICAkKFwiLnNlY3Rpb24tc2xpZGVyXCIpLnNsaWNrKHtcbiAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgcHJldkFycm93OlxuICAgICAgJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpY2stcHJldlwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHdpZHRoPVwiMjJcIiBoZWlnaHQ9XCIzM1wiIHZpZXdCb3g9XCIwIDAgMjIgMzNcIj48cGF0aCBmaWxsPVwiI0UxRTNERlwiIGQ9XCJNMjIuMjM4ODM5MywzMDYuNzYxMTYxIEw3LjMzMjU4OTI5LDMyMS42Njc0MTEgQzcuMDc4MTIzNzMsMzIxLjkyMTg3NiA2Ljc3Njc4NzQ2LDMyMi4wNDkxMDcgNi40Mjg1NzE0MywzMjIuMDQ5MTA3IEM2LjA4MDM1NTQsMzIyLjA0OTEwNyA1Ljc3OTAxOTEzLDMyMS45MjE4NzYgNS41MjQ1NTM1NywzMjEuNjY3NDExIEwyLjE4OTczMjE0LDMxOC4zMzI1ODkgQzEuOTM1MjY2NTgsMzE4LjA3ODEyNCAxLjgwODAzNTcxLDMxNy43NzY3ODcgMS44MDgwMzU3MSwzMTcuNDI4NTcxIEMxLjgwODAzNTcxLDMxNy4wODAzNTUgMS45MzUyNjY1OCwzMTYuNzc5MDE5IDIuMTg5NzMyMTQsMzE2LjUyNDU1NCBMMTIuODU3MTQyOSwzMDUuODU3MTQzIEwyLjE4OTczMjE0LDI5NS4xODk3MzIgQzEuOTM1MjY2NTgsMjk0LjkzNTI2NyAxLjgwODAzNTcxLDI5NC42MzM5MyAxLjgwODAzNTcxLDI5NC4yODU3MTQgQzEuODA4MDM1NzEsMjkzLjkzNzQ5OCAxLjkzNTI2NjU4LDI5My42MzYxNjIgMi4xODk3MzIxNCwyOTMuMzgxNjk2IEw1LjUyNDU1MzU3LDI5MC4wNDY4NzUgQzUuNzc5MDE5MTMsMjg5Ljc5MjQwOSA2LjA4MDM1NTQsMjg5LjY2NTE3OSA2LjQyODU3MTQzLDI4OS42NjUxNzkgQzYuNzc2Nzg3NDYsMjg5LjY2NTE3OSA3LjA3ODEyMzczLDI4OS43OTI0MDkgNy4zMzI1ODkyOSwyOTAuMDQ2ODc1IEwyMi4yMzg4MzkzLDMwNC45NTMxMjUgQzIyLjQ5MzMwNDgsMzA1LjIwNzU5MSAyMi42MjA1MzU3LDMwNS41MDg5MjcgMjIuNjIwNTM1NywzMDUuODU3MTQzIEMyMi42MjA1MzU3LDMwNi4yMDUzNTkgMjIuNDkzMzA0OCwzMDYuNTA2Njk1IDIyLjIzODgzOTMsMzA2Ljc2MTE2MSBaXCIgdHJhbnNmb3JtPVwicm90YXRlKC0xODAgMTEuNzE0IDE2MS4zNTcpXCIvPjwvc3ZnPjwvYnV0dG9uPicsXG4gICAgbmV4dEFycm93OlxuICAgICAgJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpY2stbmV4dFwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHdpZHRoPVwiMjJcIiBoZWlnaHQ9XCIzNFwiIHZpZXdCb3g9XCIwIDAgMjIgMzRcIj48cGF0aCBmaWxsPVwiI0UxRTNERlwiIGQ9XCJNMTE3Mi4yMzg4NCwzMDUuNzYxMTYxIEwxMTU3LjMzMjU5LDMyMC42Njc0MTEgQzExNTcuMDc4MTIsMzIwLjkyMTg3NiAxMTU2Ljc3Njc5LDMyMS4wNDkxMDcgMTE1Ni40Mjg1NywzMjEuMDQ5MTA3IEMxMTU2LjA4MDM2LDMyMS4wNDkxMDcgMTE1NS43NzkwMiwzMjAuOTIxODc2IDExNTUuNTI0NTUsMzIwLjY2NzQxMSBMMTE1Mi4xODk3MywzMTcuMzMyNTg5IEMxMTUxLjkzNTI3LDMxNy4wNzgxMjQgMTE1MS44MDgwNCwzMTYuNzc2Nzg3IDExNTEuODA4MDQsMzE2LjQyODU3MSBDMTE1MS44MDgwNCwzMTYuMDgwMzU1IDExNTEuOTM1MjcsMzE1Ljc3OTAxOSAxMTUyLjE4OTczLDMxNS41MjQ1NTQgTDExNjIuODU3MTQsMzA0Ljg1NzE0MyBMMTE1Mi4xODk3MywyOTQuMTg5NzMyIEMxMTUxLjkzNTI3LDI5My45MzUyNjcgMTE1MS44MDgwNCwyOTMuNjMzOTMgMTE1MS44MDgwNCwyOTMuMjg1NzE0IEMxMTUxLjgwODA0LDI5Mi45Mzc0OTggMTE1MS45MzUyNywyOTIuNjM2MTYyIDExNTIuMTg5NzMsMjkyLjM4MTY5NiBMMTE1NS41MjQ1NSwyODkuMDQ2ODc1IEMxMTU1Ljc3OTAyLDI4OC43OTI0MDkgMTE1Ni4wODAzNiwyODguNjY1MTc5IDExNTYuNDI4NTcsMjg4LjY2NTE3OSBDMTE1Ni43NzY3OSwyODguNjY1MTc5IDExNTcuMDc4MTIsMjg4Ljc5MjQwOSAxMTU3LjMzMjU5LDI4OS4wNDY4NzUgTDExNzIuMjM4ODQsMzAzLjk1MzEyNSBDMTE3Mi40OTMzLDMwNC4yMDc1OTEgMTE3Mi42MjA1NCwzMDQuNTA4OTI3IDExNzIuNjIwNTQsMzA0Ljg1NzE0MyBDMTE3Mi42MjA1NCwzMDUuMjA1MzU5IDExNzIuNDkzMywzMDUuNTA2Njk1IDExNzIuMjM4ODQsMzA1Ljc2MTE2MSBaXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKC0xMTUxIC0yODgpXCIvPjwvc3ZnPjwvYnV0dG9uPidcbiAgfSk7XG59KShqUXVlcnkpO1xuXG4vKipcbiAqIFByb2R1Y3QgU2xpZGVyXG4gKi9cbihmdW5jdGlvbigkKSB7XG4gIGlmICgkKFwiLnByb2R1Y3Qtc2xpZGVyXCIpLmxlbmd0aCA8IDEpIHtcbiAgICByZXR1cm47XG4gIH1cbiAgJChcIi5wcm9kdWN0LXNsaWRlclwiKS5zbGljayh7XG4gICAgc2xpZGVzVG9TaG93OiAxLFxuICAgIHByZXZBcnJvdzpcbiAgICAgICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWNrLXByZXZcIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB3aWR0aD1cIjIyXCIgaGVpZ2h0PVwiMzNcIiB2aWV3Qm94PVwiMCAwIDIyIDMzXCI+PHBhdGggZmlsbD1cIiNFMUUzREZcIiBkPVwiTTIyLjIzODgzOTMsMzA2Ljc2MTE2MSBMNy4zMzI1ODkyOSwzMjEuNjY3NDExIEM3LjA3ODEyMzczLDMyMS45MjE4NzYgNi43NzY3ODc0NiwzMjIuMDQ5MTA3IDYuNDI4NTcxNDMsMzIyLjA0OTEwNyBDNi4wODAzNTU0LDMyMi4wNDkxMDcgNS43NzkwMTkxMywzMjEuOTIxODc2IDUuNTI0NTUzNTcsMzIxLjY2NzQxMSBMMi4xODk3MzIxNCwzMTguMzMyNTg5IEMxLjkzNTI2NjU4LDMxOC4wNzgxMjQgMS44MDgwMzU3MSwzMTcuNzc2Nzg3IDEuODA4MDM1NzEsMzE3LjQyODU3MSBDMS44MDgwMzU3MSwzMTcuMDgwMzU1IDEuOTM1MjY2NTgsMzE2Ljc3OTAxOSAyLjE4OTczMjE0LDMxNi41MjQ1NTQgTDEyLjg1NzE0MjksMzA1Ljg1NzE0MyBMMi4xODk3MzIxNCwyOTUuMTg5NzMyIEMxLjkzNTI2NjU4LDI5NC45MzUyNjcgMS44MDgwMzU3MSwyOTQuNjMzOTMgMS44MDgwMzU3MSwyOTQuMjg1NzE0IEMxLjgwODAzNTcxLDI5My45Mzc0OTggMS45MzUyNjY1OCwyOTMuNjM2MTYyIDIuMTg5NzMyMTQsMjkzLjM4MTY5NiBMNS41MjQ1NTM1NywyOTAuMDQ2ODc1IEM1Ljc3OTAxOTEzLDI4OS43OTI0MDkgNi4wODAzNTU0LDI4OS42NjUxNzkgNi40Mjg1NzE0MywyODkuNjY1MTc5IEM2Ljc3Njc4NzQ2LDI4OS42NjUxNzkgNy4wNzgxMjM3MywyODkuNzkyNDA5IDcuMzMyNTg5MjksMjkwLjA0Njg3NSBMMjIuMjM4ODM5MywzMDQuOTUzMTI1IEMyMi40OTMzMDQ4LDMwNS4yMDc1OTEgMjIuNjIwNTM1NywzMDUuNTA4OTI3IDIyLjYyMDUzNTcsMzA1Ljg1NzE0MyBDMjIuNjIwNTM1NywzMDYuMjA1MzU5IDIyLjQ5MzMwNDgsMzA2LjUwNjY5NSAyMi4yMzg4MzkzLDMwNi43NjExNjEgWlwiIHRyYW5zZm9ybT1cInJvdGF0ZSgtMTgwIDExLjcxNCAxNjEuMzU3KVwiLz48L3N2Zz48L2J1dHRvbj4nLFxuICAgIG5leHRBcnJvdzpcbiAgICAgICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWNrLW5leHRcIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB3aWR0aD1cIjIyXCIgaGVpZ2h0PVwiMzRcIiB2aWV3Qm94PVwiMCAwIDIyIDM0XCI+PHBhdGggZmlsbD1cIiNFMUUzREZcIiBkPVwiTTExNzIuMjM4ODQsMzA1Ljc2MTE2MSBMMTE1Ny4zMzI1OSwzMjAuNjY3NDExIEMxMTU3LjA3ODEyLDMyMC45MjE4NzYgMTE1Ni43NzY3OSwzMjEuMDQ5MTA3IDExNTYuNDI4NTcsMzIxLjA0OTEwNyBDMTE1Ni4wODAzNiwzMjEuMDQ5MTA3IDExNTUuNzc5MDIsMzIwLjkyMTg3NiAxMTU1LjUyNDU1LDMyMC42Njc0MTEgTDExNTIuMTg5NzMsMzE3LjMzMjU4OSBDMTE1MS45MzUyNywzMTcuMDc4MTI0IDExNTEuODA4MDQsMzE2Ljc3Njc4NyAxMTUxLjgwODA0LDMxNi40Mjg1NzEgQzExNTEuODA4MDQsMzE2LjA4MDM1NSAxMTUxLjkzNTI3LDMxNS43NzkwMTkgMTE1Mi4xODk3MywzMTUuNTI0NTU0IEwxMTYyLjg1NzE0LDMwNC44NTcxNDMgTDExNTIuMTg5NzMsMjk0LjE4OTczMiBDMTE1MS45MzUyNywyOTMuOTM1MjY3IDExNTEuODA4MDQsMjkzLjYzMzkzIDExNTEuODA4MDQsMjkzLjI4NTcxNCBDMTE1MS44MDgwNCwyOTIuOTM3NDk4IDExNTEuOTM1MjcsMjkyLjYzNjE2MiAxMTUyLjE4OTczLDI5Mi4zODE2OTYgTDExNTUuNTI0NTUsMjg5LjA0Njg3NSBDMTE1NS43NzkwMiwyODguNzkyNDA5IDExNTYuMDgwMzYsMjg4LjY2NTE3OSAxMTU2LjQyODU3LDI4OC42NjUxNzkgQzExNTYuNzc2NzksMjg4LjY2NTE3OSAxMTU3LjA3ODEyLDI4OC43OTI0MDkgMTE1Ny4zMzI1OSwyODkuMDQ2ODc1IEwxMTcyLjIzODg0LDMwMy45NTMxMjUgQzExNzIuNDkzMywzMDQuMjA3NTkxIDExNzIuNjIwNTQsMzA0LjUwODkyNyAxMTcyLjYyMDU0LDMwNC44NTcxNDMgQzExNzIuNjIwNTQsMzA1LjIwNTM1OSAxMTcyLjQ5MzMsMzA1LjUwNjY5NSAxMTcyLjIzODg0LDMwNS43NjExNjEgWlwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgtMTE1MSAtMjg4KVwiLz48L3N2Zz48L2J1dHRvbj4nXG4gIH0pO1xufSkoalF1ZXJ5KTtcbiIsInZhciBsYXp5SW1hZ2VMb2FkID0gZnVuY3Rpb24oKSB7XG4gIHZhciBsYXp5SW1hZ2VzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sYXp5bG9hZFwiKTtcbiAgdmFyIGluQWR2YW5jZSA9IDMwMDtcbiAgbGF6eUltYWdlcy5mb3JFYWNoKGZ1bmN0aW9uKGltYWdlKSB7XG4gICAgaWYgKGltYWdlLm9mZnNldFBhcmVudCA9PSBudWxsKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmIChpbWFnZS5jbGFzc0xpc3QuY29udGFpbnMoXCJsb2FkZWRcIikpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaWYgKGltYWdlLm9mZnNldFRvcCA8IHdpbmRvdy5pbm5lckhlaWdodCArIHdpbmRvdy5wYWdlWU9mZnNldCArIGluQWR2YW5jZSkge1xuICAgICAgaWYgKGltYWdlLmRhdGFzZXQuc3JjICE9IHVuZGVmaW5lZCkge1xuICAgICAgICBpbWFnZS5zcmMgPSBpbWFnZS5kYXRhc2V0LnNyYztcbiAgICAgIH1cbiAgICAgIGlmIChpbWFnZS5kYXRhc2V0LmJnICE9IHVuZGVmaW5lZCkge1xuICAgICAgICBpbWFnZS5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgPSBcInVybChcIiArIGltYWdlLmRhdGFzZXQuYmcgKyBcIilcIjtcbiAgICAgIH1cbiAgICAgIGlmIChpbWFnZS5kYXRhc2V0LmlmcmFtZSAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgaW1hZ2Uuc3JjID0gaW1hZ2UuZGF0YXNldC5pZnJhbWU7XG4gICAgICB9XG4gICAgICBpbWFnZS5vbmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgaW1hZ2UuY2xhc3NMaXN0LmFkZChcImxvYWRlZFwiKTtcbiAgICAgIH07XG4gICAgfVxuICB9KTtcbn07XG5cbihmdW5jdGlvbigkKSB7XG4gICQoZG9jdW1lbnQpLm9uKFwicmVhZHlcIiwgbGF6eUltYWdlTG9hZCk7XG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsIGxhenlJbWFnZUxvYWQpO1xuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBsYXp5SW1hZ2VMb2FkKTtcbn0pKGpRdWVyeSk7XG4iLCIvKiEgalF1ZXJ5ICYgWmVwdG8gTGF6eSB2MS43LjEwIC0gaHR0cDovL2pxdWVyeS5laXNiZWhyLmRlL2xhenkgLSBNSVQmR1BMLTIuMCBsaWNlbnNlIC0gQ29weXJpZ2h0IDIwMTItMjAxOCBEYW5pZWwgJ0Vpc2JlaHInIEtlcm4gKi9cbiFmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIocixhLGksdSxsKXtmdW5jdGlvbiBmKCl7TD10LmRldmljZVBpeGVsUmF0aW8+MSxpPWMoaSksYS5kZWxheT49MCYmc2V0VGltZW91dChmdW5jdGlvbigpe3MoITApfSxhLmRlbGF5KSwoYS5kZWxheTwwfHxhLmNvbWJpbmVkKSYmKHUuZT12KGEudGhyb3R0bGUsZnVuY3Rpb24odCl7XCJyZXNpemVcIj09PXQudHlwZSYmKHc9Qj0tMSkscyh0LmFsbCl9KSx1LmE9ZnVuY3Rpb24odCl7dD1jKHQpLGkucHVzaC5hcHBseShpLHQpfSx1Lmc9ZnVuY3Rpb24oKXtyZXR1cm4gaT1uKGkpLmZpbHRlcihmdW5jdGlvbigpe3JldHVybiFuKHRoaXMpLmRhdGEoYS5sb2FkZWROYW1lKX0pfSx1LmY9ZnVuY3Rpb24odCl7Zm9yKHZhciBlPTA7ZTx0Lmxlbmd0aDtlKyspe3ZhciByPWkuZmlsdGVyKGZ1bmN0aW9uKCl7cmV0dXJuIHRoaXM9PT10W2VdfSk7ci5sZW5ndGgmJnMoITEscil9fSxzKCksbihhLmFwcGVuZFNjcm9sbCkub24oXCJzY3JvbGwuXCIrbCtcIiByZXNpemUuXCIrbCx1LmUpKX1mdW5jdGlvbiBjKHQpe3ZhciBpPWEuZGVmYXVsdEltYWdlLG89YS5wbGFjZWhvbGRlcix1PWEuaW1hZ2VCYXNlLGw9YS5zcmNzZXRBdHRyaWJ1dGUsZj1hLmxvYWRlckF0dHJpYnV0ZSxjPWEuX2Z8fHt9O3Q9bih0KS5maWx0ZXIoZnVuY3Rpb24oKXt2YXIgdD1uKHRoaXMpLHI9bSh0aGlzKTtyZXR1cm4hdC5kYXRhKGEuaGFuZGxlZE5hbWUpJiYodC5hdHRyKGEuYXR0cmlidXRlKXx8dC5hdHRyKGwpfHx0LmF0dHIoZil8fGNbcl0hPT1lKX0pLmRhdGEoXCJwbHVnaW5fXCIrYS5uYW1lLHIpO2Zvcih2YXIgcz0wLGQ9dC5sZW5ndGg7czxkO3MrKyl7dmFyIEE9bih0W3NdKSxnPW0odFtzXSksaD1BLmF0dHIoYS5pbWFnZUJhc2VBdHRyaWJ1dGUpfHx1O2c9PT1OJiZoJiZBLmF0dHIobCkmJkEuYXR0cihsLGIoQS5hdHRyKGwpLGgpKSxjW2ddPT09ZXx8QS5hdHRyKGYpfHxBLmF0dHIoZixjW2ddKSxnPT09TiYmaSYmIUEuYXR0cihFKT9BLmF0dHIoRSxpKTpnPT09Tnx8IW98fEEuY3NzKE8pJiZcIm5vbmVcIiE9PUEuY3NzKE8pfHxBLmNzcyhPLFwidXJsKCdcIitvK1wiJylcIil9cmV0dXJuIHR9ZnVuY3Rpb24gcyh0LGUpe2lmKCFpLmxlbmd0aClyZXR1cm4gdm9pZChhLmF1dG9EZXN0cm95JiZyLmRlc3Ryb3koKSk7Zm9yKHZhciBvPWV8fGksdT0hMSxsPWEuaW1hZ2VCYXNlfHxcIlwiLGY9YS5zcmNzZXRBdHRyaWJ1dGUsYz1hLmhhbmRsZWROYW1lLHM9MDtzPG8ubGVuZ3RoO3MrKylpZih0fHxlfHxBKG9bc10pKXt2YXIgZz1uKG9bc10pLGg9bShvW3NdKSxiPWcuYXR0cihhLmF0dHJpYnV0ZSksdj1nLmF0dHIoYS5pbWFnZUJhc2VBdHRyaWJ1dGUpfHxsLHA9Zy5hdHRyKGEubG9hZGVyQXR0cmlidXRlKTtnLmRhdGEoYyl8fGEudmlzaWJsZU9ubHkmJiFnLmlzKFwiOnZpc2libGVcIil8fCEoKGJ8fGcuYXR0cihmKSkmJihoPT09TiYmKHYrYiE9PWcuYXR0cihFKXx8Zy5hdHRyKGYpIT09Zy5hdHRyKEYpKXx8aCE9PU4mJnYrYiE9PWcuY3NzKE8pKXx8cCl8fCh1PSEwLGcuZGF0YShjLCEwKSxkKGcsaCx2LHApKX11JiYoaT1uKGkpLmZpbHRlcihmdW5jdGlvbigpe3JldHVybiFuKHRoaXMpLmRhdGEoYyl9KSl9ZnVuY3Rpb24gZCh0LGUscixpKXsrK3o7dmFyIG89ZnVuY3Rpb24oKXt5KFwib25FcnJvclwiLHQpLHAoKSxvPW4ubm9vcH07eShcImJlZm9yZUxvYWRcIix0KTt2YXIgdT1hLmF0dHJpYnV0ZSxsPWEuc3Jjc2V0QXR0cmlidXRlLGY9YS5zaXplc0F0dHJpYnV0ZSxjPWEucmV0aW5hQXR0cmlidXRlLHM9YS5yZW1vdmVBdHRyaWJ1dGUsZD1hLmxvYWRlZE5hbWUsQT10LmF0dHIoYyk7aWYoaSl7dmFyIGc9ZnVuY3Rpb24oKXtzJiZ0LnJlbW92ZUF0dHIoYS5sb2FkZXJBdHRyaWJ1dGUpLHQuZGF0YShkLCEwKSx5KFQsdCksc2V0VGltZW91dChwLDEpLGc9bi5ub29wfTt0Lm9mZihJKS5vbmUoSSxvKS5vbmUoRCxnKSx5KGksdCxmdW5jdGlvbihlKXtlPyh0Lm9mZihEKSxnKCkpOih0Lm9mZihJKSxvKCkpfSl8fHQudHJpZ2dlcihJKX1lbHNle3ZhciBoPW4obmV3IEltYWdlKTtoLm9uZShJLG8pLm9uZShELGZ1bmN0aW9uKCl7dC5oaWRlKCksZT09PU4/dC5hdHRyKEMsaC5hdHRyKEMpKS5hdHRyKEYsaC5hdHRyKEYpKS5hdHRyKEUsaC5hdHRyKEUpKTp0LmNzcyhPLFwidXJsKCdcIitoLmF0dHIoRSkrXCInKVwiKSx0W2EuZWZmZWN0XShhLmVmZmVjdFRpbWUpLHMmJih0LnJlbW92ZUF0dHIodStcIiBcIitsK1wiIFwiK2MrXCIgXCIrYS5pbWFnZUJhc2VBdHRyaWJ1dGUpLGYhPT1DJiZ0LnJlbW92ZUF0dHIoZikpLHQuZGF0YShkLCEwKSx5KFQsdCksaC5yZW1vdmUoKSxwKCl9KTt2YXIgbT0oTCYmQT9BOnQuYXR0cih1KSl8fFwiXCI7aC5hdHRyKEMsdC5hdHRyKGYpKS5hdHRyKEYsdC5hdHRyKGwpKS5hdHRyKEUsbT9yK206bnVsbCksaC5jb21wbGV0ZSYmaC50cmlnZ2VyKEQpfX1mdW5jdGlvbiBBKHQpe3ZhciBlPXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkscj1hLnNjcm9sbERpcmVjdGlvbixuPWEudGhyZXNob2xkLGk9aCgpK24+ZS50b3AmJi1uPGUuYm90dG9tLG89ZygpK24+ZS5sZWZ0JiYtbjxlLnJpZ2h0O3JldHVyblwidmVydGljYWxcIj09PXI/aTpcImhvcml6b250YWxcIj09PXI/bzppJiZvfWZ1bmN0aW9uIGcoKXtyZXR1cm4gdz49MD93Onc9bih0KS53aWR0aCgpfWZ1bmN0aW9uIGgoKXtyZXR1cm4gQj49MD9COkI9bih0KS5oZWlnaHQoKX1mdW5jdGlvbiBtKHQpe3JldHVybiB0LnRhZ05hbWUudG9Mb3dlckNhc2UoKX1mdW5jdGlvbiBiKHQsZSl7aWYoZSl7dmFyIHI9dC5zcGxpdChcIixcIik7dD1cIlwiO2Zvcih2YXIgYT0wLG49ci5sZW5ndGg7YTxuO2ErKyl0Kz1lK3JbYV0udHJpbSgpKyhhIT09bi0xP1wiLFwiOlwiXCIpfXJldHVybiB0fWZ1bmN0aW9uIHYodCxlKXt2YXIgbixpPTA7cmV0dXJuIGZ1bmN0aW9uKG8sdSl7ZnVuY3Rpb24gbCgpe2k9K25ldyBEYXRlLGUuY2FsbChyLG8pfXZhciBmPStuZXcgRGF0ZS1pO24mJmNsZWFyVGltZW91dChuKSxmPnR8fCFhLmVuYWJsZVRocm90dGxlfHx1P2woKTpuPXNldFRpbWVvdXQobCx0LWYpfX1mdW5jdGlvbiBwKCl7LS16LGkubGVuZ3RofHx6fHx5KFwib25GaW5pc2hlZEFsbFwiKX1mdW5jdGlvbiB5KHQsZSxuKXtyZXR1cm4hISh0PWFbdF0pJiYodC5hcHBseShyLFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLDEpKSwhMCl9dmFyIHo9MCx3PS0xLEI9LTEsTD0hMSxUPVwiYWZ0ZXJMb2FkXCIsRD1cImxvYWRcIixJPVwiZXJyb3JcIixOPVwiaW1nXCIsRT1cInNyY1wiLEY9XCJzcmNzZXRcIixDPVwic2l6ZXNcIixPPVwiYmFja2dyb3VuZC1pbWFnZVwiO1wiZXZlbnRcIj09PWEuYmluZHx8bz9mKCk6bih0KS5vbihEK1wiLlwiK2wsZil9ZnVuY3Rpb24gYShhLG8pe3ZhciB1PXRoaXMsbD1uLmV4dGVuZCh7fSx1LmNvbmZpZyxvKSxmPXt9LGM9bC5uYW1lK1wiLVwiKyArK2k7cmV0dXJuIHUuY29uZmlnPWZ1bmN0aW9uKHQscil7cmV0dXJuIHI9PT1lP2xbdF06KGxbdF09cix1KX0sdS5hZGRJdGVtcz1mdW5jdGlvbih0KXtyZXR1cm4gZi5hJiZmLmEoXCJzdHJpbmdcIj09PW4udHlwZSh0KT9uKHQpOnQpLHV9LHUuZ2V0SXRlbXM9ZnVuY3Rpb24oKXtyZXR1cm4gZi5nP2YuZygpOnt9fSx1LnVwZGF0ZT1mdW5jdGlvbih0KXtyZXR1cm4gZi5lJiZmLmUoe30sIXQpLHV9LHUuZm9yY2U9ZnVuY3Rpb24odCl7cmV0dXJuIGYuZiYmZi5mKFwic3RyaW5nXCI9PT1uLnR5cGUodCk/bih0KTp0KSx1fSx1LmxvYWRBbGw9ZnVuY3Rpb24oKXtyZXR1cm4gZi5lJiZmLmUoe2FsbDohMH0sITApLHV9LHUuZGVzdHJveT1mdW5jdGlvbigpe3JldHVybiBuKGwuYXBwZW5kU2Nyb2xsKS5vZmYoXCIuXCIrYyxmLmUpLG4odCkub2ZmKFwiLlwiK2MpLGY9e30sZX0scih1LGwsYSxmLGMpLGwuY2hhaW5hYmxlP2E6dX12YXIgbj10LmpRdWVyeXx8dC5aZXB0byxpPTAsbz0hMTtuLmZuLkxhenk9bi5mbi5sYXp5PWZ1bmN0aW9uKHQpe3JldHVybiBuZXcgYSh0aGlzLHQpfSxuLkxhenk9bi5sYXp5PWZ1bmN0aW9uKHQscixpKXtpZihuLmlzRnVuY3Rpb24ocikmJihpPXIscj1bXSksbi5pc0Z1bmN0aW9uKGkpKXt0PW4uaXNBcnJheSh0KT90Olt0XSxyPW4uaXNBcnJheShyKT9yOltyXTtmb3IodmFyIG89YS5wcm90b3R5cGUuY29uZmlnLHU9by5fZnx8KG8uX2Y9e30pLGw9MCxmPXQubGVuZ3RoO2w8ZjtsKyspKG9bdFtsXV09PT1lfHxuLmlzRnVuY3Rpb24ob1t0W2xdXSkpJiYob1t0W2xdXT1pKTtmb3IodmFyIGM9MCxzPXIubGVuZ3RoO2M8cztjKyspdVtyW2NdXT10WzBdfX0sYS5wcm90b3R5cGUuY29uZmlnPXtuYW1lOlwibGF6eVwiLGNoYWluYWJsZTohMCxhdXRvRGVzdHJveTohMCxiaW5kOlwibG9hZFwiLHRocmVzaG9sZDo1MDAsdmlzaWJsZU9ubHk6ITEsYXBwZW5kU2Nyb2xsOnQsc2Nyb2xsRGlyZWN0aW9uOlwiYm90aFwiLGltYWdlQmFzZTpudWxsLGRlZmF1bHRJbWFnZTpcImRhdGE6aW1hZ2UvZ2lmO2Jhc2U2NCxSMGxHT0RsaEFRQUJBSUFBQVAvLy93QUFBQ0g1QkFFQUFBQUFMQUFBQUFBQkFBRUFBQUlDUkFFQU93PT1cIixwbGFjZWhvbGRlcjpudWxsLGRlbGF5Oi0xLGNvbWJpbmVkOiExLGF0dHJpYnV0ZTpcImRhdGEtc3JjXCIsc3Jjc2V0QXR0cmlidXRlOlwiZGF0YS1zcmNzZXRcIixzaXplc0F0dHJpYnV0ZTpcImRhdGEtc2l6ZXNcIixyZXRpbmFBdHRyaWJ1dGU6XCJkYXRhLXJldGluYVwiLGxvYWRlckF0dHJpYnV0ZTpcImRhdGEtbG9hZGVyXCIsaW1hZ2VCYXNlQXR0cmlidXRlOlwiZGF0YS1pbWFnZWJhc2VcIixyZW1vdmVBdHRyaWJ1dGU6ITAsaGFuZGxlZE5hbWU6XCJoYW5kbGVkXCIsbG9hZGVkTmFtZTpcImxvYWRlZFwiLGVmZmVjdDpcInNob3dcIixlZmZlY3RUaW1lOjAsZW5hYmxlVGhyb3R0bGU6ITAsdGhyb3R0bGU6MjUwLGJlZm9yZUxvYWQ6ZSxhZnRlckxvYWQ6ZSxvbkVycm9yOmUsb25GaW5pc2hlZEFsbDplfSxuKHQpLm9uKFwibG9hZFwiLGZ1bmN0aW9uKCl7bz0hMH0pfSh3aW5kb3cpOyIsIihmdW5jdGlvbigkKSB7XG4gIHZhciAkaGVhZGVyID0gJChcIiNoZWFkZXJcIik7XG4gIHZhciAkbWVudSA9ICRoZWFkZXIuZmluZChcIiNtZW51XCIpO1xuXG4gIHZhciB0b2dnbGVBY3RpdmUgPSBmdW5jdGlvbihldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgJG1lbnUudG9nZ2xlQ2xhc3MoXCJjaGFuZ2VcIik7XG4gICAgJGhlYWRlci50b2dnbGVDbGFzcyhcImNoYW5nZVwiKTtcbiAgICAkaGVhZGVyLmZpbmQoXCIuYmFyc1wiKS50b2dnbGVDbGFzcyhcImNoYW5nZVwiKTtcbiAgICAkKFwiYm9keSwgaHRtbFwiKS50b2dnbGVDbGFzcyhcIm5vLXNjcm9sbFwiKTtcbiAgfTtcblxuICB2YXIgbW9iaWxlX29wZW5fbWVudSA9IGZ1bmN0aW9uKCkge1xuICAgICQodGhpcylcbiAgICAgIC5uZXh0KClcbiAgICAgIC50b2dnbGVDbGFzcyhcIm1vYmlsZS1kcm9wLW9wZW5cIik7XG5cbiAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKFwicm90YXRlLWRyb3Bkb3duLWJ1dHRvblwiKTtcbiAgfTtcblxuICAkbWVudS5vbihcImNsaWNrXCIsIFwiLmJhcnNcIiwgdG9nZ2xlQWN0aXZlKTtcbiAgJGhlYWRlci5vbihcImNsaWNrXCIsIFwiLmRyb3Bkb3duLWJ1dHRvblwiLCBtb2JpbGVfb3Blbl9tZW51KTtcbn0pKGpRdWVyeSk7XG4iLCIvKlxuICAgICBfIF8gICAgICBfICAgICAgIF9cbiBfX198IChfKSBfX198IHwgX18gIChfKV9fX1xuLyBfX3wgfCB8LyBfX3wgfC8gLyAgfCAvIF9ffFxuXFxfXyBcXCB8IHwgKF9ffCAgIDwgXyB8IFxcX18gXFxcbnxfX18vX3xffFxcX19ffF98XFxfKF8pLyB8X19fL1xuICAgICAgICAgICAgICAgICAgIHxfXy9cblxuIFZlcnNpb246IDEuNi4wXG4gIEF1dGhvcjogS2VuIFdoZWVsZXJcbiBXZWJzaXRlOiBodHRwOi8va2Vud2hlZWxlci5naXRodWIuaW9cbiAgICBEb2NzOiBodHRwOi8va2Vud2hlZWxlci5naXRodWIuaW8vc2xpY2tcbiAgICBSZXBvOiBodHRwOi8vZ2l0aHViLmNvbS9rZW53aGVlbGVyL3NsaWNrXG4gIElzc3VlczogaHR0cDovL2dpdGh1Yi5jb20va2Vud2hlZWxlci9zbGljay9pc3N1ZXNcblxuICovXG4vKiBnbG9iYWwgd2luZG93LCBkb2N1bWVudCwgZGVmaW5lLCBqUXVlcnksIHNldEludGVydmFsLCBjbGVhckludGVydmFsICovXG4oZnVuY3Rpb24oZmFjdG9yeSkge1xuICAgICd1c2Ugc3RyaWN0JztcbiAgICBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG4gICAgICAgIGRlZmluZShbJ2pxdWVyeSddLCBmYWN0b3J5KTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBleHBvcnRzICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkocmVxdWlyZSgnanF1ZXJ5JykpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGZhY3RvcnkoalF1ZXJ5KTtcbiAgICB9XG5cbn0oZnVuY3Rpb24oJCkge1xuICAgICd1c2Ugc3RyaWN0JztcbiAgICB2YXIgU2xpY2sgPSB3aW5kb3cuU2xpY2sgfHwge307XG5cbiAgICBTbGljayA9IChmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgaW5zdGFuY2VVaWQgPSAwO1xuXG4gICAgICAgIGZ1bmN0aW9uIFNsaWNrKGVsZW1lbnQsIHNldHRpbmdzKSB7XG5cbiAgICAgICAgICAgIHZhciBfID0gdGhpcywgZGF0YVNldHRpbmdzO1xuXG4gICAgICAgICAgICBfLmRlZmF1bHRzID0ge1xuICAgICAgICAgICAgICAgIGFjY2Vzc2liaWxpdHk6IHRydWUsXG4gICAgICAgICAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGFwcGVuZEFycm93czogJChlbGVtZW50KSxcbiAgICAgICAgICAgICAgICBhcHBlbmREb3RzOiAkKGVsZW1lbnQpLFxuICAgICAgICAgICAgICAgIGFycm93czogdHJ1ZSxcbiAgICAgICAgICAgICAgICBhc05hdkZvcjogbnVsbCxcbiAgICAgICAgICAgICAgICBwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBkYXRhLXJvbGU9XCJub25lXCIgY2xhc3M9XCJzbGljay1wcmV2XCIgYXJpYS1sYWJlbD1cIlByZXZpb3VzXCIgdGFiaW5kZXg9XCIwXCIgcm9sZT1cImJ1dHRvblwiPlByZXZpb3VzPC9idXR0b24+JyxcbiAgICAgICAgICAgICAgICBuZXh0QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBkYXRhLXJvbGU9XCJub25lXCIgY2xhc3M9XCJzbGljay1uZXh0XCIgYXJpYS1sYWJlbD1cIk5leHRcIiB0YWJpbmRleD1cIjBcIiByb2xlPVwiYnV0dG9uXCI+TmV4dDwvYnV0dG9uPicsXG4gICAgICAgICAgICAgICAgYXV0b3BsYXk6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGF1dG9wbGF5U3BlZWQ6IDMwMDAsXG4gICAgICAgICAgICAgICAgY2VudGVyTW9kZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgY2VudGVyUGFkZGluZzogJzUwcHgnLFxuICAgICAgICAgICAgICAgIGNzc0Vhc2U6ICdlYXNlJyxcbiAgICAgICAgICAgICAgICBjdXN0b21QYWdpbmc6IGZ1bmN0aW9uKHNsaWRlciwgaSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJCgnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgZGF0YS1yb2xlPVwibm9uZVwiIHJvbGU9XCJidXR0b25cIiB0YWJpbmRleD1cIjBcIiAvPicpLnRleHQoaSArIDEpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZG90czogZmFsc2UsXG4gICAgICAgICAgICAgICAgZG90c0NsYXNzOiAnc2xpY2stZG90cycsXG4gICAgICAgICAgICAgICAgZHJhZ2dhYmxlOiB0cnVlLFxuICAgICAgICAgICAgICAgIGVhc2luZzogJ2xpbmVhcicsXG4gICAgICAgICAgICAgICAgZWRnZUZyaWN0aW9uOiAwLjM1LFxuICAgICAgICAgICAgICAgIGZhZGU6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGZvY3VzT25TZWxlY3Q6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxuICAgICAgICAgICAgICAgIGluaXRpYWxTbGlkZTogMCxcbiAgICAgICAgICAgICAgICBsYXp5TG9hZDogJ29uZGVtYW5kJyxcbiAgICAgICAgICAgICAgICBtb2JpbGVGaXJzdDogZmFsc2UsXG4gICAgICAgICAgICAgICAgcGF1c2VPbkhvdmVyOiB0cnVlLFxuICAgICAgICAgICAgICAgIHBhdXNlT25Gb2N1czogdHJ1ZSxcbiAgICAgICAgICAgICAgICBwYXVzZU9uRG90c0hvdmVyOiBmYWxzZSxcbiAgICAgICAgICAgICAgICByZXNwb25kVG86ICd3aW5kb3cnLFxuICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IG51bGwsXG4gICAgICAgICAgICAgICAgcm93czogMSxcbiAgICAgICAgICAgICAgICBydGw6IGZhbHNlLFxuICAgICAgICAgICAgICAgIHNsaWRlOiAnJyxcbiAgICAgICAgICAgICAgICBzbGlkZXNQZXJSb3c6IDEsXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgICAgICAgICAgIHNwZWVkOiA1MDAsXG4gICAgICAgICAgICAgICAgc3dpcGU6IHRydWUsXG4gICAgICAgICAgICAgICAgc3dpcGVUb1NsaWRlOiBmYWxzZSxcbiAgICAgICAgICAgICAgICB0b3VjaE1vdmU6IHRydWUsXG4gICAgICAgICAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUsXG4gICAgICAgICAgICAgICAgdXNlQ1NTOiB0cnVlLFxuICAgICAgICAgICAgICAgIHVzZVRyYW5zZm9ybTogdHJ1ZSxcbiAgICAgICAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbDogZmFsc2UsXG4gICAgICAgICAgICAgICAgdmVydGljYWxTd2lwaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICB3YWl0Rm9yQW5pbWF0ZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICB6SW5kZXg6IDEwMDBcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIF8uaW5pdGlhbHMgPSB7XG4gICAgICAgICAgICAgICAgYW5pbWF0aW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBkcmFnZ2luZzogZmFsc2UsXG4gICAgICAgICAgICAgICAgYXV0b1BsYXlUaW1lcjogbnVsbCxcbiAgICAgICAgICAgICAgICBjdXJyZW50RGlyZWN0aW9uOiAwLFxuICAgICAgICAgICAgICAgIGN1cnJlbnRMZWZ0OiBudWxsLFxuICAgICAgICAgICAgICAgIGN1cnJlbnRTbGlkZTogMCxcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb246IDEsXG4gICAgICAgICAgICAgICAgJGRvdHM6IG51bGwsXG4gICAgICAgICAgICAgICAgbGlzdFdpZHRoOiBudWxsLFxuICAgICAgICAgICAgICAgIGxpc3RIZWlnaHQ6IG51bGwsXG4gICAgICAgICAgICAgICAgbG9hZEluZGV4OiAwLFxuICAgICAgICAgICAgICAgICRuZXh0QXJyb3c6IG51bGwsXG4gICAgICAgICAgICAgICAgJHByZXZBcnJvdzogbnVsbCxcbiAgICAgICAgICAgICAgICBzbGlkZUNvdW50OiBudWxsLFxuICAgICAgICAgICAgICAgIHNsaWRlV2lkdGg6IG51bGwsXG4gICAgICAgICAgICAgICAgJHNsaWRlVHJhY2s6IG51bGwsXG4gICAgICAgICAgICAgICAgJHNsaWRlczogbnVsbCxcbiAgICAgICAgICAgICAgICBzbGlkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBzbGlkZU9mZnNldDogMCxcbiAgICAgICAgICAgICAgICBzd2lwZUxlZnQ6IG51bGwsXG4gICAgICAgICAgICAgICAgJGxpc3Q6IG51bGwsXG4gICAgICAgICAgICAgICAgdG91Y2hPYmplY3Q6IHt9LFxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybXNFbmFibGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICB1bnNsaWNrZWQ6IGZhbHNlXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkLmV4dGVuZChfLCBfLmluaXRpYWxzKTtcblxuICAgICAgICAgICAgXy5hY3RpdmVCcmVha3BvaW50ID0gbnVsbDtcbiAgICAgICAgICAgIF8uYW5pbVR5cGUgPSBudWxsO1xuICAgICAgICAgICAgXy5hbmltUHJvcCA9IG51bGw7XG4gICAgICAgICAgICBfLmJyZWFrcG9pbnRzID0gW107XG4gICAgICAgICAgICBfLmJyZWFrcG9pbnRTZXR0aW5ncyA9IFtdO1xuICAgICAgICAgICAgXy5jc3NUcmFuc2l0aW9ucyA9IGZhbHNlO1xuICAgICAgICAgICAgXy5mb2N1c3NlZCA9IGZhbHNlO1xuICAgICAgICAgICAgXy5pbnRlcnJ1cHRlZCA9IGZhbHNlO1xuICAgICAgICAgICAgXy5oaWRkZW4gPSAnaGlkZGVuJztcbiAgICAgICAgICAgIF8ucGF1c2VkID0gdHJ1ZTtcbiAgICAgICAgICAgIF8ucG9zaXRpb25Qcm9wID0gbnVsbDtcbiAgICAgICAgICAgIF8ucmVzcG9uZFRvID0gbnVsbDtcbiAgICAgICAgICAgIF8ucm93Q291bnQgPSAxO1xuICAgICAgICAgICAgXy5zaG91bGRDbGljayA9IHRydWU7XG4gICAgICAgICAgICBfLiRzbGlkZXIgPSAkKGVsZW1lbnQpO1xuICAgICAgICAgICAgXy4kc2xpZGVzQ2FjaGUgPSBudWxsO1xuICAgICAgICAgICAgXy50cmFuc2Zvcm1UeXBlID0gbnVsbDtcbiAgICAgICAgICAgIF8udHJhbnNpdGlvblR5cGUgPSBudWxsO1xuICAgICAgICAgICAgXy52aXNpYmlsaXR5Q2hhbmdlID0gJ3Zpc2liaWxpdHljaGFuZ2UnO1xuICAgICAgICAgICAgXy53aW5kb3dXaWR0aCA9IDA7XG4gICAgICAgICAgICBfLndpbmRvd1RpbWVyID0gbnVsbDtcblxuICAgICAgICAgICAgZGF0YVNldHRpbmdzID0gJChlbGVtZW50KS5kYXRhKCdzbGljaycpIHx8IHt9O1xuXG4gICAgICAgICAgICBfLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgXy5kZWZhdWx0cywgc2V0dGluZ3MsIGRhdGFTZXR0aW5ncyk7XG5cbiAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gXy5vcHRpb25zLmluaXRpYWxTbGlkZTtcblxuICAgICAgICAgICAgXy5vcmlnaW5hbFNldHRpbmdzID0gXy5vcHRpb25zO1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGRvY3VtZW50Lm1vekhpZGRlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICBfLmhpZGRlbiA9ICdtb3pIaWRkZW4nO1xuICAgICAgICAgICAgICAgIF8udmlzaWJpbGl0eUNoYW5nZSA9ICdtb3p2aXNpYmlsaXR5Y2hhbmdlJztcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRvY3VtZW50LndlYmtpdEhpZGRlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICBfLmhpZGRlbiA9ICd3ZWJraXRIaWRkZW4nO1xuICAgICAgICAgICAgICAgIF8udmlzaWJpbGl0eUNoYW5nZSA9ICd3ZWJraXR2aXNpYmlsaXR5Y2hhbmdlJztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgXy5hdXRvUGxheSA9ICQucHJveHkoXy5hdXRvUGxheSwgXyk7XG4gICAgICAgICAgICBfLmF1dG9QbGF5Q2xlYXIgPSAkLnByb3h5KF8uYXV0b1BsYXlDbGVhciwgXyk7XG4gICAgICAgICAgICBfLmF1dG9QbGF5SXRlcmF0b3IgPSAkLnByb3h5KF8uYXV0b1BsYXlJdGVyYXRvciwgXyk7XG4gICAgICAgICAgICBfLmNoYW5nZVNsaWRlID0gJC5wcm94eShfLmNoYW5nZVNsaWRlLCBfKTtcbiAgICAgICAgICAgIF8uY2xpY2tIYW5kbGVyID0gJC5wcm94eShfLmNsaWNrSGFuZGxlciwgXyk7XG4gICAgICAgICAgICBfLnNlbGVjdEhhbmRsZXIgPSAkLnByb3h5KF8uc2VsZWN0SGFuZGxlciwgXyk7XG4gICAgICAgICAgICBfLnNldFBvc2l0aW9uID0gJC5wcm94eShfLnNldFBvc2l0aW9uLCBfKTtcbiAgICAgICAgICAgIF8uc3dpcGVIYW5kbGVyID0gJC5wcm94eShfLnN3aXBlSGFuZGxlciwgXyk7XG4gICAgICAgICAgICBfLmRyYWdIYW5kbGVyID0gJC5wcm94eShfLmRyYWdIYW5kbGVyLCBfKTtcbiAgICAgICAgICAgIF8ua2V5SGFuZGxlciA9ICQucHJveHkoXy5rZXlIYW5kbGVyLCBfKTtcblxuICAgICAgICAgICAgXy5pbnN0YW5jZVVpZCA9IGluc3RhbmNlVWlkKys7XG5cbiAgICAgICAgICAgIC8vIEEgc2ltcGxlIHdheSB0byBjaGVjayBmb3IgSFRNTCBzdHJpbmdzXG4gICAgICAgICAgICAvLyBTdHJpY3QgSFRNTCByZWNvZ25pdGlvbiAobXVzdCBzdGFydCB3aXRoIDwpXG4gICAgICAgICAgICAvLyBFeHRyYWN0ZWQgZnJvbSBqUXVlcnkgdjEuMTEgc291cmNlXG4gICAgICAgICAgICBfLmh0bWxFeHByID0gL14oPzpcXHMqKDxbXFx3XFxXXSs+KVtePl0qKSQvO1xuXG5cbiAgICAgICAgICAgIF8ucmVnaXN0ZXJCcmVha3BvaW50cygpO1xuICAgICAgICAgICAgXy5pbml0KHRydWUpO1xuXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gU2xpY2s7XG5cbiAgICB9KCkpO1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmFjdGl2YXRlQURBID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLiRzbGlkZVRyYWNrLmZpbmQoJy5zbGljay1hY3RpdmUnKS5hdHRyKHtcbiAgICAgICAgICAgICdhcmlhLWhpZGRlbic6ICdmYWxzZSdcbiAgICAgICAgfSkuZmluZCgnYSwgaW5wdXQsIGJ1dHRvbiwgc2VsZWN0JykuYXR0cih7XG4gICAgICAgICAgICAndGFiaW5kZXgnOiAnMCdcbiAgICAgICAgfSk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmFkZFNsaWRlID0gU2xpY2sucHJvdG90eXBlLnNsaWNrQWRkID0gZnVuY3Rpb24obWFya3VwLCBpbmRleCwgYWRkQmVmb3JlKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmICh0eXBlb2YoaW5kZXgpID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgICAgIGFkZEJlZm9yZSA9IGluZGV4O1xuICAgICAgICAgICAgaW5kZXggPSBudWxsO1xuICAgICAgICB9IGVsc2UgaWYgKGluZGV4IDwgMCB8fCAoaW5kZXggPj0gXy5zbGlkZUNvdW50KSkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgXy51bmxvYWQoKTtcblxuICAgICAgICBpZiAodHlwZW9mKGluZGV4KSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIGlmIChpbmRleCA9PT0gMCAmJiBfLiRzbGlkZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgJChtYXJrdXApLmFwcGVuZFRvKF8uJHNsaWRlVHJhY2spO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChhZGRCZWZvcmUpIHtcbiAgICAgICAgICAgICAgICAkKG1hcmt1cCkuaW5zZXJ0QmVmb3JlKF8uJHNsaWRlcy5lcShpbmRleCkpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKG1hcmt1cCkuaW5zZXJ0QWZ0ZXIoXy4kc2xpZGVzLmVxKGluZGV4KSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoYWRkQmVmb3JlID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgJChtYXJrdXApLnByZXBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJChtYXJrdXApLmFwcGVuZFRvKF8uJHNsaWRlVHJhY2spO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgXy4kc2xpZGVzID0gXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpO1xuXG4gICAgICAgIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4odGhpcy5vcHRpb25zLnNsaWRlKS5kZXRhY2goKTtcblxuICAgICAgICBfLiRzbGlkZVRyYWNrLmFwcGVuZChfLiRzbGlkZXMpO1xuXG4gICAgICAgIF8uJHNsaWRlcy5lYWNoKGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7XG4gICAgICAgICAgICAkKGVsZW1lbnQpLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnLCBpbmRleCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIF8uJHNsaWRlc0NhY2hlID0gXy4kc2xpZGVzO1xuXG4gICAgICAgIF8ucmVpbml0KCk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmFuaW1hdGVIZWlnaHQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuICAgICAgICBpZiAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA9PT0gMSAmJiBfLm9wdGlvbnMuYWRhcHRpdmVIZWlnaHQgPT09IHRydWUgJiYgXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgICAgICAgdmFyIHRhcmdldEhlaWdodCA9IF8uJHNsaWRlcy5lcShfLmN1cnJlbnRTbGlkZSkub3V0ZXJIZWlnaHQodHJ1ZSk7XG4gICAgICAgICAgICBfLiRsaXN0LmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIGhlaWdodDogdGFyZ2V0SGVpZ2h0XG4gICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5hbmltYXRlU2xpZGUgPSBmdW5jdGlvbih0YXJnZXRMZWZ0LCBjYWxsYmFjaykge1xuXG4gICAgICAgIHZhciBhbmltUHJvcHMgPSB7fSxcbiAgICAgICAgICAgIF8gPSB0aGlzO1xuXG4gICAgICAgIF8uYW5pbWF0ZUhlaWdodCgpO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMucnRsID09PSB0cnVlICYmIF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIHRhcmdldExlZnQgPSAtdGFyZ2V0TGVmdDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoXy50cmFuc2Zvcm1zRW5hYmxlZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgbGVmdDogdGFyZ2V0TGVmdFxuICAgICAgICAgICAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCwgXy5vcHRpb25zLmVhc2luZywgY2FsbGJhY2spO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICB0b3A6IHRhcmdldExlZnRcbiAgICAgICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQsIF8ub3B0aW9ucy5lYXNpbmcsIGNhbGxiYWNrKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICBpZiAoXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRMZWZ0ID0gLShfLmN1cnJlbnRMZWZ0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJCh7XG4gICAgICAgICAgICAgICAgICAgIGFuaW1TdGFydDogXy5jdXJyZW50TGVmdFxuICAgICAgICAgICAgICAgIH0pLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICBhbmltU3RhcnQ6IHRhcmdldExlZnRcbiAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiBfLm9wdGlvbnMuc3BlZWQsXG4gICAgICAgICAgICAgICAgICAgIGVhc2luZzogXy5vcHRpb25zLmVhc2luZyxcbiAgICAgICAgICAgICAgICAgICAgc3RlcDogZnVuY3Rpb24obm93KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBub3cgPSBNYXRoLmNlaWwobm93KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZSgnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm93ICsgJ3B4LCAwcHgpJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhhbmltUHJvcHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmltUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlKDBweCwnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm93ICsgJ3B4KSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jc3MoYW5pbVByb3BzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2suY2FsbCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICBfLmFwcGx5VHJhbnNpdGlvbigpO1xuICAgICAgICAgICAgICAgIHRhcmdldExlZnQgPSBNYXRoLmNlaWwodGFyZ2V0TGVmdCk7XG5cbiAgICAgICAgICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICBhbmltUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlM2QoJyArIHRhcmdldExlZnQgKyAncHgsIDBweCwgMHB4KSc7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZTNkKDBweCwnICsgdGFyZ2V0TGVmdCArICdweCwgMHB4KSc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKGFuaW1Qcm9wcyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoY2FsbGJhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgXy5kaXNhYmxlVHJhbnNpdGlvbigpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKCk7XG4gICAgICAgICAgICAgICAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXROYXZUYXJnZXQgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICBhc05hdkZvciA9IF8ub3B0aW9ucy5hc05hdkZvcjtcblxuICAgICAgICBpZiAoIGFzTmF2Rm9yICYmIGFzTmF2Rm9yICE9PSBudWxsICkge1xuICAgICAgICAgICAgYXNOYXZGb3IgPSAkKGFzTmF2Rm9yKS5ub3QoXy4kc2xpZGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBhc05hdkZvcjtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuYXNOYXZGb3IgPSBmdW5jdGlvbihpbmRleCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIGFzTmF2Rm9yID0gXy5nZXROYXZUYXJnZXQoKTtcblxuICAgICAgICBpZiAoIGFzTmF2Rm9yICE9PSBudWxsICYmIHR5cGVvZiBhc05hdkZvciA9PT0gJ29iamVjdCcgKSB7XG4gICAgICAgICAgICBhc05hdkZvci5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciB0YXJnZXQgPSAkKHRoaXMpLnNsaWNrKCdnZXRTbGljaycpO1xuICAgICAgICAgICAgICAgIGlmKCF0YXJnZXQudW5zbGlja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldC5zbGlkZUhhbmRsZXIoaW5kZXgsIHRydWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmFwcGx5VHJhbnNpdGlvbiA9IGZ1bmN0aW9uKHNsaWRlKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgdHJhbnNpdGlvbiA9IHt9O1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIHRyYW5zaXRpb25bXy50cmFuc2l0aW9uVHlwZV0gPSBfLnRyYW5zZm9ybVR5cGUgKyAnICcgKyBfLm9wdGlvbnMuc3BlZWQgKyAnbXMgJyArIF8ub3B0aW9ucy5jc3NFYXNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdHJhbnNpdGlvbltfLnRyYW5zaXRpb25UeXBlXSA9ICdvcGFjaXR5ICcgKyBfLm9wdGlvbnMuc3BlZWQgKyAnbXMgJyArIF8ub3B0aW9ucy5jc3NFYXNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jc3ModHJhbnNpdGlvbik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGUpLmNzcyh0cmFuc2l0aW9uKTtcbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5hdXRvUGxheSA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLmF1dG9QbGF5Q2xlYXIoKTtcblxuICAgICAgICBpZiAoIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKSB7XG4gICAgICAgICAgICBfLmF1dG9QbGF5VGltZXIgPSBzZXRJbnRlcnZhbCggXy5hdXRvUGxheUl0ZXJhdG9yLCBfLm9wdGlvbnMuYXV0b3BsYXlTcGVlZCApO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmF1dG9QbGF5Q2xlYXIgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKF8uYXV0b1BsYXlUaW1lcikge1xuICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChfLmF1dG9QbGF5VGltZXIpO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmF1dG9QbGF5SXRlcmF0b3IgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICBzbGlkZVRvID0gXy5jdXJyZW50U2xpZGUgKyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XG5cbiAgICAgICAgaWYgKCAhXy5wYXVzZWQgJiYgIV8uaW50ZXJydXB0ZWQgJiYgIV8uZm9jdXNzZWQgKSB7XG5cbiAgICAgICAgICAgIGlmICggXy5vcHRpb25zLmluZmluaXRlID09PSBmYWxzZSApIHtcblxuICAgICAgICAgICAgICAgIGlmICggXy5kaXJlY3Rpb24gPT09IDEgJiYgKCBfLmN1cnJlbnRTbGlkZSArIDEgKSA9PT0gKCBfLnNsaWRlQ291bnQgLSAxICkpIHtcbiAgICAgICAgICAgICAgICAgICAgXy5kaXJlY3Rpb24gPSAwO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKCBfLmRpcmVjdGlvbiA9PT0gMCApIHtcblxuICAgICAgICAgICAgICAgICAgICBzbGlkZVRvID0gXy5jdXJyZW50U2xpZGUgLSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKCBfLmN1cnJlbnRTbGlkZSAtIDEgPT09IDAgKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfLmRpcmVjdGlvbiA9IDE7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBfLnNsaWRlSGFuZGxlciggc2xpZGVUbyApO1xuXG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuYnVpbGRBcnJvd3MgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5hcnJvd3MgPT09IHRydWUgKSB7XG5cbiAgICAgICAgICAgIF8uJHByZXZBcnJvdyA9ICQoXy5vcHRpb25zLnByZXZBcnJvdykuYWRkQ2xhc3MoJ3NsaWNrLWFycm93Jyk7XG4gICAgICAgICAgICBfLiRuZXh0QXJyb3cgPSAkKF8ub3B0aW9ucy5uZXh0QXJyb3cpLmFkZENsYXNzKCdzbGljay1hcnJvdycpO1xuXG4gICAgICAgICAgICBpZiggXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyApIHtcblxuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2staGlkZGVuJykucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gdGFiaW5kZXgnKTtcbiAgICAgICAgICAgICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWhpZGRlbicpLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuIHRhYmluZGV4Jyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5wcmV2QXJyb3cpKSB7XG4gICAgICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5wcmVwZW5kVG8oXy5vcHRpb25zLmFwcGVuZEFycm93cyk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKF8uaHRtbEV4cHIudGVzdChfLm9wdGlvbnMubmV4dEFycm93KSkge1xuICAgICAgICAgICAgICAgICAgICBfLiRuZXh0QXJyb3cuYXBwZW5kVG8oXy5vcHRpb25zLmFwcGVuZEFycm93cyk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSAhPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBfLiRwcmV2QXJyb3dcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAndHJ1ZScpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5hZGQoIF8uJG5leHRBcnJvdyApXG5cbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1oaWRkZW4nKVxuICAgICAgICAgICAgICAgICAgICAuYXR0cih7XG4gICAgICAgICAgICAgICAgICAgICAgICAnYXJpYS1kaXNhYmxlZCc6ICd0cnVlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICd0YWJpbmRleCc6ICctMSdcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmJ1aWxkRG90cyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIGksIGRvdDtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmRvdHMgPT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuXG4gICAgICAgICAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLWRvdHRlZCcpO1xuXG4gICAgICAgICAgICBkb3QgPSAkKCc8dWwgLz4nKS5hZGRDbGFzcyhfLm9wdGlvbnMuZG90c0NsYXNzKTtcblxuICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8PSBfLmdldERvdENvdW50KCk7IGkgKz0gMSkge1xuICAgICAgICAgICAgICAgIGRvdC5hcHBlbmQoJCgnPGxpIC8+JykuYXBwZW5kKF8ub3B0aW9ucy5jdXN0b21QYWdpbmcuY2FsbCh0aGlzLCBfLCBpKSkpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBfLiRkb3RzID0gZG90LmFwcGVuZFRvKF8ub3B0aW9ucy5hcHBlbmREb3RzKTtcblxuICAgICAgICAgICAgXy4kZG90cy5maW5kKCdsaScpLmZpcnN0KCkuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5idWlsZE91dCA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLiRzbGlkZXMgPVxuICAgICAgICAgICAgXy4kc2xpZGVyXG4gICAgICAgICAgICAgICAgLmNoaWxkcmVuKCBfLm9wdGlvbnMuc2xpZGUgKyAnOm5vdCguc2xpY2stY2xvbmVkKScpXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1zbGlkZScpO1xuXG4gICAgICAgIF8uc2xpZGVDb3VudCA9IF8uJHNsaWRlcy5sZW5ndGg7XG5cbiAgICAgICAgXy4kc2xpZGVzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICQoZWxlbWVudClcbiAgICAgICAgICAgICAgICAuYXR0cignZGF0YS1zbGljay1pbmRleCcsIGluZGV4KVxuICAgICAgICAgICAgICAgIC5kYXRhKCdvcmlnaW5hbFN0eWxpbmcnLCAkKGVsZW1lbnQpLmF0dHIoJ3N0eWxlJykgfHwgJycpO1xuICAgICAgICB9KTtcblxuICAgICAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLXNsaWRlcicpO1xuXG4gICAgICAgIF8uJHNsaWRlVHJhY2sgPSAoXy5zbGlkZUNvdW50ID09PSAwKSA/XG4gICAgICAgICAgICAkKCc8ZGl2IGNsYXNzPVwic2xpY2stdHJhY2tcIi8+JykuYXBwZW5kVG8oXy4kc2xpZGVyKSA6XG4gICAgICAgICAgICBfLiRzbGlkZXMud3JhcEFsbCgnPGRpdiBjbGFzcz1cInNsaWNrLXRyYWNrXCIvPicpLnBhcmVudCgpO1xuXG4gICAgICAgIF8uJGxpc3QgPSBfLiRzbGlkZVRyYWNrLndyYXAoXG4gICAgICAgICAgICAnPGRpdiBhcmlhLWxpdmU9XCJwb2xpdGVcIiBjbGFzcz1cInNsaWNrLWxpc3RcIi8+JykucGFyZW50KCk7XG4gICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKCdvcGFjaXR5JywgMCk7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlIHx8IF8ub3B0aW9ucy5zd2lwZVRvU2xpZGUgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA9IDE7XG4gICAgICAgIH1cblxuICAgICAgICAkKCdpbWdbZGF0YS1sYXp5XScsIF8uJHNsaWRlcikubm90KCdbc3JjXScpLmFkZENsYXNzKCdzbGljay1sb2FkaW5nJyk7XG5cbiAgICAgICAgXy5zZXR1cEluZmluaXRlKCk7XG5cbiAgICAgICAgXy5idWlsZEFycm93cygpO1xuXG4gICAgICAgIF8uYnVpbGREb3RzKCk7XG5cbiAgICAgICAgXy51cGRhdGVEb3RzKCk7XG5cblxuICAgICAgICBfLnNldFNsaWRlQ2xhc3Nlcyh0eXBlb2YgXy5jdXJyZW50U2xpZGUgPT09ICdudW1iZXInID8gXy5jdXJyZW50U2xpZGUgOiAwKTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmRyYWdnYWJsZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgXy4kbGlzdC5hZGRDbGFzcygnZHJhZ2dhYmxlJyk7XG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuYnVpbGRSb3dzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLCBhLCBiLCBjLCBuZXdTbGlkZXMsIG51bU9mU2xpZGVzLCBvcmlnaW5hbFNsaWRlcyxzbGlkZXNQZXJTZWN0aW9uO1xuXG4gICAgICAgIG5ld1NsaWRlcyA9IGRvY3VtZW50LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKTtcbiAgICAgICAgb3JpZ2luYWxTbGlkZXMgPSBfLiRzbGlkZXIuY2hpbGRyZW4oKTtcblxuICAgICAgICBpZihfLm9wdGlvbnMucm93cyA+IDEpIHtcblxuICAgICAgICAgICAgc2xpZGVzUGVyU2VjdGlvbiA9IF8ub3B0aW9ucy5zbGlkZXNQZXJSb3cgKiBfLm9wdGlvbnMucm93cztcbiAgICAgICAgICAgIG51bU9mU2xpZGVzID0gTWF0aC5jZWlsKFxuICAgICAgICAgICAgICAgIG9yaWdpbmFsU2xpZGVzLmxlbmd0aCAvIHNsaWRlc1BlclNlY3Rpb25cbiAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgIGZvcihhID0gMDsgYSA8IG51bU9mU2xpZGVzOyBhKyspe1xuICAgICAgICAgICAgICAgIHZhciBzbGlkZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICAgICAgICAgIGZvcihiID0gMDsgYiA8IF8ub3B0aW9ucy5yb3dzOyBiKyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHJvdyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICAgICAgICAgICAgICBmb3IoYyA9IDA7IGMgPCBfLm9wdGlvbnMuc2xpZGVzUGVyUm93OyBjKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0YXJnZXQgPSAoYSAqIHNsaWRlc1BlclNlY3Rpb24gKyAoKGIgKiBfLm9wdGlvbnMuc2xpZGVzUGVyUm93KSArIGMpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcmlnaW5hbFNsaWRlcy5nZXQodGFyZ2V0KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdy5hcHBlbmRDaGlsZChvcmlnaW5hbFNsaWRlcy5nZXQodGFyZ2V0KSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgc2xpZGUuYXBwZW5kQ2hpbGQocm93KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbmV3U2xpZGVzLmFwcGVuZENoaWxkKHNsaWRlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgXy4kc2xpZGVyLmVtcHR5KCkuYXBwZW5kKG5ld1NsaWRlcyk7XG4gICAgICAgICAgICBfLiRzbGlkZXIuY2hpbGRyZW4oKS5jaGlsZHJlbigpLmNoaWxkcmVuKClcbiAgICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzooMTAwIC8gXy5vcHRpb25zLnNsaWRlc1BlclJvdykgKyAnJScsXG4gICAgICAgICAgICAgICAgICAgICdkaXNwbGF5JzogJ2lubGluZS1ibG9jaydcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmNoZWNrUmVzcG9uc2l2ZSA9IGZ1bmN0aW9uKGluaXRpYWwsIGZvcmNlVXBkYXRlKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgYnJlYWtwb2ludCwgdGFyZ2V0QnJlYWtwb2ludCwgcmVzcG9uZFRvV2lkdGgsIHRyaWdnZXJCcmVha3BvaW50ID0gZmFsc2U7XG4gICAgICAgIHZhciBzbGlkZXJXaWR0aCA9IF8uJHNsaWRlci53aWR0aCgpO1xuICAgICAgICB2YXIgd2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aCB8fCAkKHdpbmRvdykud2lkdGgoKTtcblxuICAgICAgICBpZiAoXy5yZXNwb25kVG8gPT09ICd3aW5kb3cnKSB7XG4gICAgICAgICAgICByZXNwb25kVG9XaWR0aCA9IHdpbmRvd1dpZHRoO1xuICAgICAgICB9IGVsc2UgaWYgKF8ucmVzcG9uZFRvID09PSAnc2xpZGVyJykge1xuICAgICAgICAgICAgcmVzcG9uZFRvV2lkdGggPSBzbGlkZXJXaWR0aDtcbiAgICAgICAgfSBlbHNlIGlmIChfLnJlc3BvbmRUbyA9PT0gJ21pbicpIHtcbiAgICAgICAgICAgIHJlc3BvbmRUb1dpZHRoID0gTWF0aC5taW4od2luZG93V2lkdGgsIHNsaWRlcldpZHRoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICggXy5vcHRpb25zLnJlc3BvbnNpdmUgJiZcbiAgICAgICAgICAgIF8ub3B0aW9ucy5yZXNwb25zaXZlLmxlbmd0aCAmJlxuICAgICAgICAgICAgXy5vcHRpb25zLnJlc3BvbnNpdmUgIT09IG51bGwpIHtcblxuICAgICAgICAgICAgdGFyZ2V0QnJlYWtwb2ludCA9IG51bGw7XG5cbiAgICAgICAgICAgIGZvciAoYnJlYWtwb2ludCBpbiBfLmJyZWFrcG9pbnRzKSB7XG4gICAgICAgICAgICAgICAgaWYgKF8uYnJlYWtwb2ludHMuaGFzT3duUHJvcGVydHkoYnJlYWtwb2ludCkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKF8ub3JpZ2luYWxTZXR0aW5ncy5tb2JpbGVGaXJzdCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25kVG9XaWR0aCA8IF8uYnJlYWtwb2ludHNbYnJlYWtwb2ludF0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRCcmVha3BvaW50ID0gXy5icmVha3BvaW50c1ticmVha3BvaW50XTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25kVG9XaWR0aCA+IF8uYnJlYWtwb2ludHNbYnJlYWtwb2ludF0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRCcmVha3BvaW50ID0gXy5icmVha3BvaW50c1ticmVha3BvaW50XTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRhcmdldEJyZWFrcG9pbnQgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBpZiAoXy5hY3RpdmVCcmVha3BvaW50ICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0YXJnZXRCcmVha3BvaW50ICE9PSBfLmFjdGl2ZUJyZWFrcG9pbnQgfHwgZm9yY2VVcGRhdGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uYWN0aXZlQnJlYWtwb2ludCA9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0QnJlYWtwb2ludDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLmJyZWFrcG9pbnRTZXR0aW5nc1t0YXJnZXRCcmVha3BvaW50XSA9PT0gJ3Vuc2xpY2snKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy51bnNsaWNrKHRhcmdldEJyZWFrcG9pbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgXy5vcmlnaW5hbFNldHRpbmdzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmJyZWFrcG9pbnRTZXR0aW5nc1tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldEJyZWFrcG9pbnRdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW5pdGlhbCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IF8ub3B0aW9ucy5pbml0aWFsU2xpZGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8ucmVmcmVzaChpbml0aWFsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRyaWdnZXJCcmVha3BvaW50ID0gdGFyZ2V0QnJlYWtwb2ludDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIF8uYWN0aXZlQnJlYWtwb2ludCA9IHRhcmdldEJyZWFrcG9pbnQ7XG4gICAgICAgICAgICAgICAgICAgIGlmIChfLmJyZWFrcG9pbnRTZXR0aW5nc1t0YXJnZXRCcmVha3BvaW50XSA9PT0gJ3Vuc2xpY2snKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfLnVuc2xpY2sodGFyZ2V0QnJlYWtwb2ludCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgXy5vcmlnaW5hbFNldHRpbmdzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uYnJlYWtwb2ludFNldHRpbmdzW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRCcmVha3BvaW50XSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW5pdGlhbCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gXy5vcHRpb25zLmluaXRpYWxTbGlkZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIF8ucmVmcmVzaChpbml0aWFsKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyQnJlYWtwb2ludCA9IHRhcmdldEJyZWFrcG9pbnQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZiAoXy5hY3RpdmVCcmVha3BvaW50ICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIF8uYWN0aXZlQnJlYWtwb2ludCA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIF8ub3B0aW9ucyA9IF8ub3JpZ2luYWxTZXR0aW5ncztcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluaXRpYWwgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gXy5vcHRpb25zLmluaXRpYWxTbGlkZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBfLnJlZnJlc2goaW5pdGlhbCk7XG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXJCcmVha3BvaW50ID0gdGFyZ2V0QnJlYWtwb2ludDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIG9ubHkgdHJpZ2dlciBicmVha3BvaW50cyBkdXJpbmcgYW4gYWN0dWFsIGJyZWFrLiBub3Qgb24gaW5pdGlhbGl6ZS5cbiAgICAgICAgICAgIGlmKCAhaW5pdGlhbCAmJiB0cmlnZ2VyQnJlYWtwb2ludCAhPT0gZmFsc2UgKSB7XG4gICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2JyZWFrcG9pbnQnLCBbXywgdHJpZ2dlckJyZWFrcG9pbnRdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5jaGFuZ2VTbGlkZSA9IGZ1bmN0aW9uKGV2ZW50LCBkb250QW5pbWF0ZSkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgICR0YXJnZXQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLFxuICAgICAgICAgICAgaW5kZXhPZmZzZXQsIHNsaWRlT2Zmc2V0LCB1bmV2ZW5PZmZzZXQ7XG5cbiAgICAgICAgLy8gSWYgdGFyZ2V0IGlzIGEgbGluaywgcHJldmVudCBkZWZhdWx0IGFjdGlvbi5cbiAgICAgICAgaWYoJHRhcmdldC5pcygnYScpKSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gSWYgdGFyZ2V0IGlzIG5vdCB0aGUgPGxpPiBlbGVtZW50IChpZTogYSBjaGlsZCksIGZpbmQgdGhlIDxsaT4uXG4gICAgICAgIGlmKCEkdGFyZ2V0LmlzKCdsaScpKSB7XG4gICAgICAgICAgICAkdGFyZ2V0ID0gJHRhcmdldC5jbG9zZXN0KCdsaScpO1xuICAgICAgICB9XG5cbiAgICAgICAgdW5ldmVuT2Zmc2V0ID0gKF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAhPT0gMCk7XG4gICAgICAgIGluZGV4T2Zmc2V0ID0gdW5ldmVuT2Zmc2V0ID8gMCA6IChfLnNsaWRlQ291bnQgLSBfLmN1cnJlbnRTbGlkZSkgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XG5cbiAgICAgICAgc3dpdGNoIChldmVudC5kYXRhLm1lc3NhZ2UpIHtcblxuICAgICAgICAgICAgY2FzZSAncHJldmlvdXMnOlxuICAgICAgICAgICAgICAgIHNsaWRlT2Zmc2V0ID0gaW5kZXhPZmZzZXQgPT09IDAgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gaW5kZXhPZmZzZXQ7XG4gICAgICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICAgICAgICAgICAgXy5zbGlkZUhhbmRsZXIoXy5jdXJyZW50U2xpZGUgLSBzbGlkZU9mZnNldCwgZmFsc2UsIGRvbnRBbmltYXRlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgJ25leHQnOlxuICAgICAgICAgICAgICAgIHNsaWRlT2Zmc2V0ID0gaW5kZXhPZmZzZXQgPT09IDAgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBpbmRleE9mZnNldDtcbiAgICAgICAgICAgICAgICBpZiAoXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICAgICAgICAgICAgICBfLnNsaWRlSGFuZGxlcihfLmN1cnJlbnRTbGlkZSArIHNsaWRlT2Zmc2V0LCBmYWxzZSwgZG9udEFuaW1hdGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSAnaW5kZXgnOlxuICAgICAgICAgICAgICAgIHZhciBpbmRleCA9IGV2ZW50LmRhdGEuaW5kZXggPT09IDAgPyAwIDpcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQuZGF0YS5pbmRleCB8fCAkdGFyZ2V0LmluZGV4KCkgKiBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XG5cbiAgICAgICAgICAgICAgICBfLnNsaWRlSGFuZGxlcihfLmNoZWNrTmF2aWdhYmxlKGluZGV4KSwgZmFsc2UsIGRvbnRBbmltYXRlKTtcbiAgICAgICAgICAgICAgICAkdGFyZ2V0LmNoaWxkcmVuKCkudHJpZ2dlcignZm9jdXMnKTtcbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuY2hlY2tOYXZpZ2FibGUgPSBmdW5jdGlvbihpbmRleCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIG5hdmlnYWJsZXMsIHByZXZOYXZpZ2FibGU7XG5cbiAgICAgICAgbmF2aWdhYmxlcyA9IF8uZ2V0TmF2aWdhYmxlSW5kZXhlcygpO1xuICAgICAgICBwcmV2TmF2aWdhYmxlID0gMDtcbiAgICAgICAgaWYgKGluZGV4ID4gbmF2aWdhYmxlc1tuYXZpZ2FibGVzLmxlbmd0aCAtIDFdKSB7XG4gICAgICAgICAgICBpbmRleCA9IG5hdmlnYWJsZXNbbmF2aWdhYmxlcy5sZW5ndGggLSAxXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGZvciAodmFyIG4gaW4gbmF2aWdhYmxlcykge1xuICAgICAgICAgICAgICAgIGlmIChpbmRleCA8IG5hdmlnYWJsZXNbbl0pIHtcbiAgICAgICAgICAgICAgICAgICAgaW5kZXggPSBwcmV2TmF2aWdhYmxlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcHJldk5hdmlnYWJsZSA9IG5hdmlnYWJsZXNbbl07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gaW5kZXg7XG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5jbGVhblVwRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZG90cyAmJiBfLiRkb3RzICE9PSBudWxsKSB7XG5cbiAgICAgICAgICAgICQoJ2xpJywgXy4kZG90cylcbiAgICAgICAgICAgICAgICAub2ZmKCdjbGljay5zbGljaycsIF8uY2hhbmdlU2xpZGUpXG4gICAgICAgICAgICAgICAgLm9mZignbW91c2VlbnRlci5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIHRydWUpKVxuICAgICAgICAgICAgICAgIC5vZmYoJ21vdXNlbGVhdmUuc2xpY2snLCAkLnByb3h5KF8uaW50ZXJydXB0LCBfLCBmYWxzZSkpO1xuXG4gICAgICAgIH1cblxuICAgICAgICBfLiRzbGlkZXIub2ZmKCdmb2N1cy5zbGljayBibHVyLnNsaWNrJyk7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5hcnJvd3MgPT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICAgICAgXy4kcHJldkFycm93ICYmIF8uJHByZXZBcnJvdy5vZmYoJ2NsaWNrLnNsaWNrJywgXy5jaGFuZ2VTbGlkZSk7XG4gICAgICAgICAgICBfLiRuZXh0QXJyb3cgJiYgXy4kbmV4dEFycm93Lm9mZignY2xpY2suc2xpY2snLCBfLmNoYW5nZVNsaWRlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIF8uJGxpc3Qub2ZmKCd0b3VjaHN0YXJ0LnNsaWNrIG1vdXNlZG93bi5zbGljaycsIF8uc3dpcGVIYW5kbGVyKTtcbiAgICAgICAgXy4kbGlzdC5vZmYoJ3RvdWNobW92ZS5zbGljayBtb3VzZW1vdmUuc2xpY2snLCBfLnN3aXBlSGFuZGxlcik7XG4gICAgICAgIF8uJGxpc3Qub2ZmKCd0b3VjaGVuZC5zbGljayBtb3VzZXVwLnNsaWNrJywgXy5zd2lwZUhhbmRsZXIpO1xuICAgICAgICBfLiRsaXN0Lm9mZigndG91Y2hjYW5jZWwuc2xpY2sgbW91c2VsZWF2ZS5zbGljaycsIF8uc3dpcGVIYW5kbGVyKTtcblxuICAgICAgICBfLiRsaXN0Lm9mZignY2xpY2suc2xpY2snLCBfLmNsaWNrSGFuZGxlcik7XG5cbiAgICAgICAgJChkb2N1bWVudCkub2ZmKF8udmlzaWJpbGl0eUNoYW5nZSwgXy52aXNpYmlsaXR5KTtcblxuICAgICAgICBfLmNsZWFuVXBTbGlkZUV2ZW50cygpO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgXy4kbGlzdC5vZmYoJ2tleWRvd24uc2xpY2snLCBfLmtleUhhbmRsZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uU2VsZWN0ID09PSB0cnVlKSB7XG4gICAgICAgICAgICAkKF8uJHNsaWRlVHJhY2spLmNoaWxkcmVuKCkub2ZmKCdjbGljay5zbGljaycsIF8uc2VsZWN0SGFuZGxlcik7XG4gICAgICAgIH1cblxuICAgICAgICAkKHdpbmRvdykub2ZmKCdvcmllbnRhdGlvbmNoYW5nZS5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgXy5vcmllbnRhdGlvbkNoYW5nZSk7XG5cbiAgICAgICAgJCh3aW5kb3cpLm9mZigncmVzaXplLnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCBfLnJlc2l6ZSk7XG5cbiAgICAgICAgJCgnW2RyYWdnYWJsZSE9dHJ1ZV0nLCBfLiRzbGlkZVRyYWNrKS5vZmYoJ2RyYWdzdGFydCcsIF8ucHJldmVudERlZmF1bHQpO1xuXG4gICAgICAgICQod2luZG93KS5vZmYoJ2xvYWQuc2xpY2suc2xpY2stJyArIF8uaW5zdGFuY2VVaWQsIF8uc2V0UG9zaXRpb24pO1xuICAgICAgICAkKGRvY3VtZW50KS5vZmYoJ3JlYWR5LnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCBfLnNldFBvc2l0aW9uKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuY2xlYW5VcFNsaWRlRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIF8uJGxpc3Qub2ZmKCdtb3VzZWVudGVyLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgdHJ1ZSkpO1xuICAgICAgICBfLiRsaXN0Lm9mZignbW91c2VsZWF2ZS5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIGZhbHNlKSk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmNsZWFuVXBSb3dzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLCBvcmlnaW5hbFNsaWRlcztcblxuICAgICAgICBpZihfLm9wdGlvbnMucm93cyA+IDEpIHtcbiAgICAgICAgICAgIG9yaWdpbmFsU2xpZGVzID0gXy4kc2xpZGVzLmNoaWxkcmVuKCkuY2hpbGRyZW4oKTtcbiAgICAgICAgICAgIG9yaWdpbmFsU2xpZGVzLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG4gICAgICAgICAgICBfLiRzbGlkZXIuZW1wdHkoKS5hcHBlbmQob3JpZ2luYWxTbGlkZXMpO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmNsaWNrSGFuZGxlciA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmIChfLnNob3VsZENsaWNrID09PSBmYWxzZSkge1xuICAgICAgICAgICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uKHJlZnJlc2gpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgXy5hdXRvUGxheUNsZWFyKCk7XG5cbiAgICAgICAgXy50b3VjaE9iamVjdCA9IHt9O1xuXG4gICAgICAgIF8uY2xlYW5VcEV2ZW50cygpO1xuXG4gICAgICAgICQoJy5zbGljay1jbG9uZWQnLCBfLiRzbGlkZXIpLmRldGFjaCgpO1xuXG4gICAgICAgIGlmIChfLiRkb3RzKSB7XG4gICAgICAgICAgICBfLiRkb3RzLnJlbW92ZSgpO1xuICAgICAgICB9XG5cblxuICAgICAgICBpZiAoIF8uJHByZXZBcnJvdyAmJiBfLiRwcmV2QXJyb3cubGVuZ3RoICkge1xuXG4gICAgICAgICAgICBfLiRwcmV2QXJyb3dcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkIHNsaWNrLWFycm93IHNsaWNrLWhpZGRlbicpXG4gICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuIGFyaWEtZGlzYWJsZWQgdGFiaW5kZXgnKVxuICAgICAgICAgICAgICAgIC5jc3MoJ2Rpc3BsYXknLCcnKTtcblxuICAgICAgICAgICAgaWYgKCBfLmh0bWxFeHByLnRlc3QoIF8ub3B0aW9ucy5wcmV2QXJyb3cgKSkge1xuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICggXy4kbmV4dEFycm93ICYmIF8uJG5leHRBcnJvdy5sZW5ndGggKSB7XG5cbiAgICAgICAgICAgIF8uJG5leHRBcnJvd1xuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnc2xpY2stZGlzYWJsZWQgc2xpY2stYXJyb3cgc2xpY2staGlkZGVuJylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gYXJpYS1kaXNhYmxlZCB0YWJpbmRleCcpXG4gICAgICAgICAgICAgICAgLmNzcygnZGlzcGxheScsJycpO1xuXG4gICAgICAgICAgICBpZiAoIF8uaHRtbEV4cHIudGVzdCggXy5vcHRpb25zLm5leHRBcnJvdyApKSB7XG4gICAgICAgICAgICAgICAgXy4kbmV4dEFycm93LnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1cblxuXG4gICAgICAgIGlmIChfLiRzbGlkZXMpIHtcblxuICAgICAgICAgICAgXy4kc2xpZGVzXG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdzbGljay1zbGlkZSBzbGljay1hY3RpdmUgc2xpY2stY2VudGVyIHNsaWNrLXZpc2libGUgc2xpY2stY3VycmVudCcpXG4gICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuJylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cignZGF0YS1zbGljay1pbmRleCcpXG4gICAgICAgICAgICAgICAgLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hdHRyKCdzdHlsZScsICQodGhpcykuZGF0YSgnb3JpZ2luYWxTdHlsaW5nJykpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSkuZGV0YWNoKCk7XG5cbiAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suZGV0YWNoKCk7XG5cbiAgICAgICAgICAgIF8uJGxpc3QuZGV0YWNoKCk7XG5cbiAgICAgICAgICAgIF8uJHNsaWRlci5hcHBlbmQoXy4kc2xpZGVzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIF8uY2xlYW5VcFJvd3MoKTtcblxuICAgICAgICBfLiRzbGlkZXIucmVtb3ZlQ2xhc3MoJ3NsaWNrLXNsaWRlcicpO1xuICAgICAgICBfLiRzbGlkZXIucmVtb3ZlQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJyk7XG4gICAgICAgIF8uJHNsaWRlci5yZW1vdmVDbGFzcygnc2xpY2stZG90dGVkJyk7XG5cbiAgICAgICAgXy51bnNsaWNrZWQgPSB0cnVlO1xuXG4gICAgICAgIGlmKCFyZWZyZXNoKSB7XG4gICAgICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignZGVzdHJveScsIFtfXSk7XG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuZGlzYWJsZVRyYW5zaXRpb24gPSBmdW5jdGlvbihzbGlkZSkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIHRyYW5zaXRpb24gPSB7fTtcblxuICAgICAgICB0cmFuc2l0aW9uW18udHJhbnNpdGlvblR5cGVdID0gJyc7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jc3ModHJhbnNpdGlvbik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGUpLmNzcyh0cmFuc2l0aW9uKTtcbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5mYWRlU2xpZGUgPSBmdW5jdGlvbihzbGlkZUluZGV4LCBjYWxsYmFjaykge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcblxuICAgICAgICAgICAgXy4kc2xpZGVzLmVxKHNsaWRlSW5kZXgpLmNzcyh7XG4gICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgXy4kc2xpZGVzLmVxKHNsaWRlSW5kZXgpLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDFcbiAgICAgICAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCwgXy5vcHRpb25zLmVhc2luZywgY2FsbGJhY2spO1xuXG4gICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgIF8uYXBwbHlUcmFuc2l0aW9uKHNsaWRlSW5kZXgpO1xuXG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGVJbmRleCkuY3NzKHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgIHpJbmRleDogXy5vcHRpb25zLnpJbmRleFxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgXy5kaXNhYmxlVHJhbnNpdGlvbihzbGlkZUluZGV4KTtcblxuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKCk7XG4gICAgICAgICAgICAgICAgfSwgXy5vcHRpb25zLnNwZWVkKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmZhZGVTbGlkZU91dCA9IGZ1bmN0aW9uKHNsaWRlSW5kZXgpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKF8uY3NzVHJhbnNpdGlvbnMgPT09IGZhbHNlKSB7XG5cbiAgICAgICAgICAgIF8uJHNsaWRlcy5lcShzbGlkZUluZGV4KS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgIHpJbmRleDogXy5vcHRpb25zLnpJbmRleCAtIDJcbiAgICAgICAgICAgIH0sIF8ub3B0aW9ucy5zcGVlZCwgXy5vcHRpb25zLmVhc2luZyk7XG5cbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgXy5hcHBseVRyYW5zaXRpb24oc2xpZGVJbmRleCk7XG5cbiAgICAgICAgICAgIF8uJHNsaWRlcy5lcShzbGlkZUluZGV4KS5jc3Moe1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4IC0gMlxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5maWx0ZXJTbGlkZXMgPSBTbGljay5wcm90b3R5cGUuc2xpY2tGaWx0ZXIgPSBmdW5jdGlvbihmaWx0ZXIpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKGZpbHRlciAhPT0gbnVsbCkge1xuXG4gICAgICAgICAgICBfLiRzbGlkZXNDYWNoZSA9IF8uJHNsaWRlcztcblxuICAgICAgICAgICAgXy51bmxvYWQoKTtcblxuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xuXG4gICAgICAgICAgICBfLiRzbGlkZXNDYWNoZS5maWx0ZXIoZmlsdGVyKS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcblxuICAgICAgICAgICAgXy5yZWluaXQoKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmZvY3VzSGFuZGxlciA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLiRzbGlkZXJcbiAgICAgICAgICAgIC5vZmYoJ2ZvY3VzLnNsaWNrIGJsdXIuc2xpY2snKVxuICAgICAgICAgICAgLm9uKCdmb2N1cy5zbGljayBibHVyLnNsaWNrJyxcbiAgICAgICAgICAgICAgICAnKjpub3QoLnNsaWNrLWFycm93KScsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cbiAgICAgICAgICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgdmFyICRzZiA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgICAgICAgICBpZiggXy5vcHRpb25zLnBhdXNlT25Gb2N1cyApIHtcbiAgICAgICAgICAgICAgICAgICAgXy5mb2N1c3NlZCA9ICRzZi5pcygnOmZvY3VzJyk7XG4gICAgICAgICAgICAgICAgICAgIF8uYXV0b1BsYXkoKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0sIDApO1xuXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuZ2V0Q3VycmVudCA9IFNsaWNrLnByb3RvdHlwZS5zbGlja0N1cnJlbnRTbGlkZSA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcbiAgICAgICAgcmV0dXJuIF8uY3VycmVudFNsaWRlO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXREb3RDb3VudCA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICB2YXIgYnJlYWtQb2ludCA9IDA7XG4gICAgICAgIHZhciBjb3VudGVyID0gMDtcbiAgICAgICAgdmFyIHBhZ2VyUXR5ID0gMDtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlKSB7XG4gICAgICAgICAgICB3aGlsZSAoYnJlYWtQb2ludCA8IF8uc2xpZGVDb3VudCkge1xuICAgICAgICAgICAgICAgICsrcGFnZXJRdHk7XG4gICAgICAgICAgICAgICAgYnJlYWtQb2ludCA9IGNvdW50ZXIgKyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XG4gICAgICAgICAgICAgICAgY291bnRlciArPSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA/IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA6IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3c7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcbiAgICAgICAgICAgIHBhZ2VyUXR5ID0gXy5zbGlkZUNvdW50O1xuICAgICAgICB9IGVsc2UgaWYoIV8ub3B0aW9ucy5hc05hdkZvcikge1xuICAgICAgICAgICAgcGFnZXJRdHkgPSAxICsgTWF0aC5jZWlsKChfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSAvIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCk7XG4gICAgICAgIH1lbHNlIHtcbiAgICAgICAgICAgIHdoaWxlIChicmVha1BvaW50IDwgXy5zbGlkZUNvdW50KSB7XG4gICAgICAgICAgICAgICAgKytwYWdlclF0eTtcbiAgICAgICAgICAgICAgICBicmVha1BvaW50ID0gY291bnRlciArIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcbiAgICAgICAgICAgICAgICBjb3VudGVyICs9IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID8gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDogXy5vcHRpb25zLnNsaWRlc1RvU2hvdztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBwYWdlclF0eSAtIDE7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmdldExlZnQgPSBmdW5jdGlvbihzbGlkZUluZGV4KSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgdGFyZ2V0TGVmdCxcbiAgICAgICAgICAgIHZlcnRpY2FsSGVpZ2h0LFxuICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAwLFxuICAgICAgICAgICAgdGFyZ2V0U2xpZGU7XG5cbiAgICAgICAgXy5zbGlkZU9mZnNldCA9IDA7XG4gICAgICAgIHZlcnRpY2FsSGVpZ2h0ID0gXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJIZWlnaHQodHJ1ZSk7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gKF8uc2xpZGVXaWR0aCAqIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpICogLTE7XG4gICAgICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAodmVydGljYWxIZWlnaHQgKiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSAqIC0xO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAhPT0gMCkge1xuICAgICAgICAgICAgICAgIGlmIChzbGlkZUluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsID4gXy5zbGlkZUNvdW50ICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNsaWRlSW5kZXggPiBfLnNsaWRlQ291bnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uc2xpZGVPZmZzZXQgPSAoKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLSAoc2xpZGVJbmRleCAtIF8uc2xpZGVDb3VudCkpICogXy5zbGlkZVdpZHRoKSAqIC0xO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLSAoc2xpZGVJbmRleCAtIF8uc2xpZGVDb3VudCkpICogdmVydGljYWxIZWlnaHQpICogLTE7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gKChfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwpICogXy5zbGlkZVdpZHRoKSAqIC0xO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoKF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCkgKiB2ZXJ0aWNhbEhlaWdodCkgKiAtMTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmIChzbGlkZUluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA+IF8uc2xpZGVDb3VudCkge1xuICAgICAgICAgICAgICAgIF8uc2xpZGVPZmZzZXQgPSAoKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSAtIF8uc2xpZGVDb3VudCkgKiBfLnNsaWRlV2lkdGg7XG4gICAgICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSAtIF8uc2xpZGVDb3VudCkgKiB2ZXJ0aWNhbEhlaWdodDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLnNsaWRlQ291bnQgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICAgICAgXy5zbGlkZU9mZnNldCA9IDA7XG4gICAgICAgICAgICB2ZXJ0aWNhbE9mZnNldCA9IDA7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgJiYgXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlKSB7XG4gICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ICs9IF8uc2xpZGVXaWR0aCAqIE1hdGguZmxvb3IoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIpIC0gXy5zbGlkZVdpZHRoO1xuICAgICAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gMDtcbiAgICAgICAgICAgIF8uc2xpZGVPZmZzZXQgKz0gXy5zbGlkZVdpZHRoICogTWF0aC5mbG9vcihfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC8gMik7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgICAgICAgdGFyZ2V0TGVmdCA9ICgoc2xpZGVJbmRleCAqIF8uc2xpZGVXaWR0aCkgKiAtMSkgKyBfLnNsaWRlT2Zmc2V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGFyZ2V0TGVmdCA9ICgoc2xpZGVJbmRleCAqIHZlcnRpY2FsSGVpZ2h0KSAqIC0xKSArIHZlcnRpY2FsT2Zmc2V0O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy52YXJpYWJsZVdpZHRoID09PSB0cnVlKSB7XG5cbiAgICAgICAgICAgIGlmIChfLnNsaWRlQ291bnQgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyB8fCBfLm9wdGlvbnMuaW5maW5pdGUgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0U2xpZGUgPSBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCcuc2xpY2stc2xpZGUnKS5lcShzbGlkZUluZGV4KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0U2xpZGUgPSBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCcuc2xpY2stc2xpZGUnKS5lcShzbGlkZUluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMucnRsID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRhcmdldFNsaWRlWzBdKSB7XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldExlZnQgPSAoXy4kc2xpZGVUcmFjay53aWR0aCgpIC0gdGFyZ2V0U2xpZGVbMF0ub2Zmc2V0TGVmdCAtIHRhcmdldFNsaWRlLndpZHRoKCkpICogLTE7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0TGVmdCA9ICAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0TGVmdCA9IHRhcmdldFNsaWRlWzBdID8gdGFyZ2V0U2xpZGVbMF0ub2Zmc2V0TGVmdCAqIC0xIDogMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IHx8IF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0U2xpZGUgPSBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCcuc2xpY2stc2xpZGUnKS5lcShzbGlkZUluZGV4KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmVxKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgMSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhcmdldFNsaWRlWzBdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRMZWZ0ID0gKF8uJHNsaWRlVHJhY2sud2lkdGgoKSAtIHRhcmdldFNsaWRlWzBdLm9mZnNldExlZnQgLSB0YXJnZXRTbGlkZS53aWR0aCgpKSAqIC0xO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0TGVmdCA9ICAwO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0TGVmdCA9IHRhcmdldFNsaWRlWzBdID8gdGFyZ2V0U2xpZGVbMF0ub2Zmc2V0TGVmdCAqIC0xIDogMDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0YXJnZXRMZWZ0ICs9IChfLiRsaXN0LndpZHRoKCkgLSB0YXJnZXRTbGlkZS5vdXRlcldpZHRoKCkpIC8gMjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0YXJnZXRMZWZ0O1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXRPcHRpb24gPSBTbGljay5wcm90b3R5cGUuc2xpY2tHZXRPcHRpb24gPSBmdW5jdGlvbihvcHRpb24pIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgcmV0dXJuIF8ub3B0aW9uc1tvcHRpb25dO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXROYXZpZ2FibGVJbmRleGVzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgYnJlYWtQb2ludCA9IDAsXG4gICAgICAgICAgICBjb3VudGVyID0gMCxcbiAgICAgICAgICAgIGluZGV4ZXMgPSBbXSxcbiAgICAgICAgICAgIG1heDtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgbWF4ID0gXy5zbGlkZUNvdW50O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYnJlYWtQb2ludCA9IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAqIC0xO1xuICAgICAgICAgICAgY291bnRlciA9IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAqIC0xO1xuICAgICAgICAgICAgbWF4ID0gXy5zbGlkZUNvdW50ICogMjtcbiAgICAgICAgfVxuXG4gICAgICAgIHdoaWxlIChicmVha1BvaW50IDwgbWF4KSB7XG4gICAgICAgICAgICBpbmRleGVzLnB1c2goYnJlYWtQb2ludCk7XG4gICAgICAgICAgICBicmVha1BvaW50ID0gY291bnRlciArIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcbiAgICAgICAgICAgIGNvdW50ZXIgKz0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGluZGV4ZXM7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmdldFNsaWNrID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmdldFNsaWRlQ291bnQgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICBzbGlkZXNUcmF2ZXJzZWQsIHN3aXBlZFNsaWRlLCBjZW50ZXJPZmZzZXQ7XG5cbiAgICAgICAgY2VudGVyT2Zmc2V0ID0gXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgPyBfLnNsaWRlV2lkdGggKiBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKSA6IDA7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5zd2lwZVRvU2xpZGUgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suZmluZCgnLnNsaWNrLXNsaWRlJykuZWFjaChmdW5jdGlvbihpbmRleCwgc2xpZGUpIHtcbiAgICAgICAgICAgICAgICBpZiAoc2xpZGUub2Zmc2V0TGVmdCAtIGNlbnRlck9mZnNldCArICgkKHNsaWRlKS5vdXRlcldpZHRoKCkgLyAyKSA+IChfLnN3aXBlTGVmdCAqIC0xKSkge1xuICAgICAgICAgICAgICAgICAgICBzd2lwZWRTbGlkZSA9IHNsaWRlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHNsaWRlc1RyYXZlcnNlZCA9IE1hdGguYWJzKCQoc3dpcGVkU2xpZGUpLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnKSAtIF8uY3VycmVudFNsaWRlKSB8fCAxO1xuXG4gICAgICAgICAgICByZXR1cm4gc2xpZGVzVHJhdmVyc2VkO1xuXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmdvVG8gPSBTbGljay5wcm90b3R5cGUuc2xpY2tHb1RvID0gZnVuY3Rpb24oc2xpZGUsIGRvbnRBbmltYXRlKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIF8uY2hhbmdlU2xpZGUoe1xuICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdpbmRleCcsXG4gICAgICAgICAgICAgICAgaW5kZXg6IHBhcnNlSW50KHNsaWRlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCBkb250QW5pbWF0ZSk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbihjcmVhdGlvbikge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAoISQoXy4kc2xpZGVyKS5oYXNDbGFzcygnc2xpY2staW5pdGlhbGl6ZWQnKSkge1xuXG4gICAgICAgICAgICAkKF8uJHNsaWRlcikuYWRkQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJyk7XG5cbiAgICAgICAgICAgIF8uYnVpbGRSb3dzKCk7XG4gICAgICAgICAgICBfLmJ1aWxkT3V0KCk7XG4gICAgICAgICAgICBfLnNldFByb3BzKCk7XG4gICAgICAgICAgICBfLnN0YXJ0TG9hZCgpO1xuICAgICAgICAgICAgXy5sb2FkU2xpZGVyKCk7XG4gICAgICAgICAgICBfLmluaXRpYWxpemVFdmVudHMoKTtcbiAgICAgICAgICAgIF8udXBkYXRlQXJyb3dzKCk7XG4gICAgICAgICAgICBfLnVwZGF0ZURvdHMoKTtcbiAgICAgICAgICAgIF8uY2hlY2tSZXNwb25zaXZlKHRydWUpO1xuICAgICAgICAgICAgXy5mb2N1c0hhbmRsZXIoKTtcblxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNyZWF0aW9uKSB7XG4gICAgICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignaW5pdCcsIFtfXSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8uaW5pdEFEQSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCBfLm9wdGlvbnMuYXV0b3BsYXkgKSB7XG5cbiAgICAgICAgICAgIF8ucGF1c2VkID0gZmFsc2U7XG4gICAgICAgICAgICBfLmF1dG9QbGF5KCk7XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5pbml0QURBID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBfID0gdGhpcztcbiAgICAgICAgXy4kc2xpZGVzLmFkZChfLiRzbGlkZVRyYWNrLmZpbmQoJy5zbGljay1jbG9uZWQnKSkuYXR0cih7XG4gICAgICAgICAgICAnYXJpYS1oaWRkZW4nOiAndHJ1ZScsXG4gICAgICAgICAgICAndGFiaW5kZXgnOiAnLTEnXG4gICAgICAgIH0pLmZpbmQoJ2EsIGlucHV0LCBidXR0b24sIHNlbGVjdCcpLmF0dHIoe1xuICAgICAgICAgICAgJ3RhYmluZGV4JzogJy0xJ1xuICAgICAgICB9KTtcblxuICAgICAgICBfLiRzbGlkZVRyYWNrLmF0dHIoJ3JvbGUnLCAnbGlzdGJveCcpO1xuXG4gICAgICAgIF8uJHNsaWRlcy5ub3QoXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stY2xvbmVkJykpLmVhY2goZnVuY3Rpb24oaSkge1xuICAgICAgICAgICAgJCh0aGlzKS5hdHRyKHtcbiAgICAgICAgICAgICAgICAncm9sZSc6ICdvcHRpb24nLFxuICAgICAgICAgICAgICAgICdhcmlhLWRlc2NyaWJlZGJ5JzogJ3NsaWNrLXNsaWRlJyArIF8uaW5zdGFuY2VVaWQgKyBpICsgJydcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBpZiAoXy4kZG90cyAhPT0gbnVsbCkge1xuICAgICAgICAgICAgXy4kZG90cy5hdHRyKCdyb2xlJywgJ3RhYmxpc3QnKS5maW5kKCdsaScpLmVhY2goZnVuY3Rpb24oaSkge1xuICAgICAgICAgICAgICAgICQodGhpcykuYXR0cih7XG4gICAgICAgICAgICAgICAgICAgICdyb2xlJzogJ3ByZXNlbnRhdGlvbicsXG4gICAgICAgICAgICAgICAgICAgICdhcmlhLXNlbGVjdGVkJzogJ2ZhbHNlJyxcbiAgICAgICAgICAgICAgICAgICAgJ2FyaWEtY29udHJvbHMnOiAnbmF2aWdhdGlvbicgKyBfLmluc3RhbmNlVWlkICsgaSArICcnLFxuICAgICAgICAgICAgICAgICAgICAnaWQnOiAnc2xpY2stc2xpZGUnICsgXy5pbnN0YW5jZVVpZCArIGkgKyAnJ1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmlyc3QoKS5hdHRyKCdhcmlhLXNlbGVjdGVkJywgJ3RydWUnKS5lbmQoKVxuICAgICAgICAgICAgICAgIC5maW5kKCdidXR0b24nKS5hdHRyKCdyb2xlJywgJ2J1dHRvbicpLmVuZCgpXG4gICAgICAgICAgICAgICAgLmNsb3Nlc3QoJ2RpdicpLmF0dHIoJ3JvbGUnLCAndG9vbGJhcicpO1xuICAgICAgICB9XG4gICAgICAgIF8uYWN0aXZhdGVBREEoKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuaW5pdEFycm93RXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICAgIF8uJHByZXZBcnJvd1xuICAgICAgICAgICAgICAgLm9mZignY2xpY2suc2xpY2snKVxuICAgICAgICAgICAgICAgLm9uKCdjbGljay5zbGljaycsIHtcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ3ByZXZpb3VzJ1xuICAgICAgICAgICAgICAgfSwgXy5jaGFuZ2VTbGlkZSk7XG4gICAgICAgICAgICBfLiRuZXh0QXJyb3dcbiAgICAgICAgICAgICAgIC5vZmYoJ2NsaWNrLnNsaWNrJylcbiAgICAgICAgICAgICAgIC5vbignY2xpY2suc2xpY2snLCB7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICduZXh0J1xuICAgICAgICAgICAgICAgfSwgXy5jaGFuZ2VTbGlkZSk7XG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuaW5pdERvdEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmRvdHMgPT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuICAgICAgICAgICAgJCgnbGknLCBfLiRkb3RzKS5vbignY2xpY2suc2xpY2snLCB7XG4gICAgICAgICAgICAgICAgbWVzc2FnZTogJ2luZGV4J1xuICAgICAgICAgICAgfSwgXy5jaGFuZ2VTbGlkZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5kb3RzID09PSB0cnVlICYmIF8ub3B0aW9ucy5wYXVzZU9uRG90c0hvdmVyID09PSB0cnVlICkge1xuXG4gICAgICAgICAgICAkKCdsaScsIF8uJGRvdHMpXG4gICAgICAgICAgICAgICAgLm9uKCdtb3VzZWVudGVyLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgdHJ1ZSkpXG4gICAgICAgICAgICAgICAgLm9uKCdtb3VzZWxlYXZlLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgZmFsc2UpKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmluaXRTbGlkZUV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5wYXVzZU9uSG92ZXIgKSB7XG5cbiAgICAgICAgICAgIF8uJGxpc3Qub24oJ21vdXNlZW50ZXIuc2xpY2snLCAkLnByb3h5KF8uaW50ZXJydXB0LCBfLCB0cnVlKSk7XG4gICAgICAgICAgICBfLiRsaXN0Lm9uKCdtb3VzZWxlYXZlLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgZmFsc2UpKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmluaXRpYWxpemVFdmVudHMgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgXy5pbml0QXJyb3dFdmVudHMoKTtcblxuICAgICAgICBfLmluaXREb3RFdmVudHMoKTtcbiAgICAgICAgXy5pbml0U2xpZGVFdmVudHMoKTtcblxuICAgICAgICBfLiRsaXN0Lm9uKCd0b3VjaHN0YXJ0LnNsaWNrIG1vdXNlZG93bi5zbGljaycsIHtcbiAgICAgICAgICAgIGFjdGlvbjogJ3N0YXJ0J1xuICAgICAgICB9LCBfLnN3aXBlSGFuZGxlcik7XG4gICAgICAgIF8uJGxpc3Qub24oJ3RvdWNobW92ZS5zbGljayBtb3VzZW1vdmUuc2xpY2snLCB7XG4gICAgICAgICAgICBhY3Rpb246ICdtb3ZlJ1xuICAgICAgICB9LCBfLnN3aXBlSGFuZGxlcik7XG4gICAgICAgIF8uJGxpc3Qub24oJ3RvdWNoZW5kLnNsaWNrIG1vdXNldXAuc2xpY2snLCB7XG4gICAgICAgICAgICBhY3Rpb246ICdlbmQnXG4gICAgICAgIH0sIF8uc3dpcGVIYW5kbGVyKTtcbiAgICAgICAgXy4kbGlzdC5vbigndG91Y2hjYW5jZWwuc2xpY2sgbW91c2VsZWF2ZS5zbGljaycsIHtcbiAgICAgICAgICAgIGFjdGlvbjogJ2VuZCdcbiAgICAgICAgfSwgXy5zd2lwZUhhbmRsZXIpO1xuXG4gICAgICAgIF8uJGxpc3Qub24oJ2NsaWNrLnNsaWNrJywgXy5jbGlja0hhbmRsZXIpO1xuXG4gICAgICAgICQoZG9jdW1lbnQpLm9uKF8udmlzaWJpbGl0eUNoYW5nZSwgJC5wcm94eShfLnZpc2liaWxpdHksIF8pKTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8uJGxpc3Qub24oJ2tleWRvd24uc2xpY2snLCBfLmtleUhhbmRsZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uU2VsZWN0ID09PSB0cnVlKSB7XG4gICAgICAgICAgICAkKF8uJHNsaWRlVHJhY2spLmNoaWxkcmVuKCkub24oJ2NsaWNrLnNsaWNrJywgXy5zZWxlY3RIYW5kbGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgICQod2luZG93KS5vbignb3JpZW50YXRpb25jaGFuZ2Uuc2xpY2suc2xpY2stJyArIF8uaW5zdGFuY2VVaWQsICQucHJveHkoXy5vcmllbnRhdGlvbkNoYW5nZSwgXykpO1xuXG4gICAgICAgICQod2luZG93KS5vbigncmVzaXplLnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCAkLnByb3h5KF8ucmVzaXplLCBfKSk7XG5cbiAgICAgICAgJCgnW2RyYWdnYWJsZSE9dHJ1ZV0nLCBfLiRzbGlkZVRyYWNrKS5vbignZHJhZ3N0YXJ0JywgXy5wcmV2ZW50RGVmYXVsdCk7XG5cbiAgICAgICAgJCh3aW5kb3cpLm9uKCdsb2FkLnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCBfLnNldFBvc2l0aW9uKTtcbiAgICAgICAgJChkb2N1bWVudCkub24oJ3JlYWR5LnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCBfLnNldFBvc2l0aW9uKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuaW5pdFVJID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcblxuICAgICAgICAgICAgXy4kcHJldkFycm93LnNob3coKTtcbiAgICAgICAgICAgIF8uJG5leHRBcnJvdy5zaG93KCk7XG5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG5cbiAgICAgICAgICAgIF8uJGRvdHMuc2hvdygpO1xuXG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUua2V5SGFuZGxlciA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuICAgICAgICAgLy9Eb250IHNsaWRlIGlmIHRoZSBjdXJzb3IgaXMgaW5zaWRlIHRoZSBmb3JtIGZpZWxkcyBhbmQgYXJyb3cga2V5cyBhcmUgcHJlc3NlZFxuICAgICAgICBpZighZXZlbnQudGFyZ2V0LnRhZ05hbWUubWF0Y2goJ1RFWFRBUkVBfElOUFVUfFNFTEVDVCcpKSB7XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzcgJiYgXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBfLmNoYW5nZVNsaWRlKHtcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSA/ICduZXh0JyA6ICAncHJldmlvdXMnXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzkgJiYgXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBfLmNoYW5nZVNsaWRlKHtcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSA/ICdwcmV2aW91cycgOiAnbmV4dCdcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmxhenlMb2FkID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgbG9hZFJhbmdlLCBjbG9uZVJhbmdlLCByYW5nZVN0YXJ0LCByYW5nZUVuZDtcblxuICAgICAgICBmdW5jdGlvbiBsb2FkSW1hZ2VzKGltYWdlc1Njb3BlKSB7XG5cbiAgICAgICAgICAgICQoJ2ltZ1tkYXRhLWxhenldJywgaW1hZ2VzU2NvcGUpLmVhY2goZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgICAgICAgICB2YXIgaW1hZ2UgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICAgICBpbWFnZVNvdXJjZSA9ICQodGhpcykuYXR0cignZGF0YS1sYXp5JyksXG4gICAgICAgICAgICAgICAgICAgIGltYWdlVG9Mb2FkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW1nJyk7XG5cbiAgICAgICAgICAgICAgICBpbWFnZVRvTG9hZC5vbmxvYWQgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgICAgICAgICBpbWFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGUoeyBvcGFjaXR5OiAwIH0sIDEwMCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ3NyYycsIGltYWdlU291cmNlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7IG9wYWNpdHk6IDEgfSwgMjAwLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGltYWdlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoJ2RhdGEtbGF6eScpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdzbGljay1sb2FkaW5nJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdsYXp5TG9hZGVkJywgW18sIGltYWdlLCBpbWFnZVNvdXJjZV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgaW1hZ2VUb0xvYWQub25lcnJvciA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgICAgIGltYWdlXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQXR0ciggJ2RhdGEtbGF6eScgKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCAnc2xpY2stbG9hZGluZycgKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCAnc2xpY2stbGF6eWxvYWQtZXJyb3InICk7XG5cbiAgICAgICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkRXJyb3InLCBbIF8sIGltYWdlLCBpbWFnZVNvdXJjZSBdKTtcblxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICBpbWFnZVRvTG9hZC5zcmMgPSBpbWFnZVNvdXJjZTtcblxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHJhbmdlU3RhcnQgPSBfLmN1cnJlbnRTbGlkZSArIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC8gMiArIDEpO1xuICAgICAgICAgICAgICAgIHJhbmdlRW5kID0gcmFuZ2VTdGFydCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyAyO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByYW5nZVN0YXJ0ID0gTWF0aC5tYXgoMCwgXy5jdXJyZW50U2xpZGUgLSAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIgKyAxKSk7XG4gICAgICAgICAgICAgICAgcmFuZ2VFbmQgPSAyICsgKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyICsgMSkgKyBfLmN1cnJlbnRTbGlkZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJhbmdlU3RhcnQgPSBfLm9wdGlvbnMuaW5maW5pdGUgPyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgXy5jdXJyZW50U2xpZGUgOiBfLmN1cnJlbnRTbGlkZTtcbiAgICAgICAgICAgIHJhbmdlRW5kID0gTWF0aC5jZWlsKHJhbmdlU3RhcnQgKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIGlmIChyYW5nZVN0YXJ0ID4gMCkgcmFuZ2VTdGFydC0tO1xuICAgICAgICAgICAgICAgIGlmIChyYW5nZUVuZCA8PSBfLnNsaWRlQ291bnQpIHJhbmdlRW5kKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBsb2FkUmFuZ2UgPSBfLiRzbGlkZXIuZmluZCgnLnNsaWNrLXNsaWRlJykuc2xpY2UocmFuZ2VTdGFydCwgcmFuZ2VFbmQpO1xuICAgICAgICBsb2FkSW1hZ2VzKGxvYWRSYW5nZSk7XG5cbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgICAgICBjbG9uZVJhbmdlID0gXy4kc2xpZGVyLmZpbmQoJy5zbGljay1zbGlkZScpO1xuICAgICAgICAgICAgbG9hZEltYWdlcyhjbG9uZVJhbmdlKTtcbiAgICAgICAgfSBlbHNlXG4gICAgICAgIGlmIChfLmN1cnJlbnRTbGlkZSA+PSBfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgICAgICBjbG9uZVJhbmdlID0gXy4kc2xpZGVyLmZpbmQoJy5zbGljay1jbG9uZWQnKS5zbGljZSgwLCBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcbiAgICAgICAgICAgIGxvYWRJbWFnZXMoY2xvbmVSYW5nZSk7XG4gICAgICAgIH0gZWxzZSBpZiAoXy5jdXJyZW50U2xpZGUgPT09IDApIHtcbiAgICAgICAgICAgIGNsb25lUmFuZ2UgPSBfLiRzbGlkZXIuZmluZCgnLnNsaWNrLWNsb25lZCcpLnNsaWNlKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKiAtMSk7XG4gICAgICAgICAgICBsb2FkSW1hZ2VzKGNsb25lUmFuZ2UpO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLmxvYWRTbGlkZXIgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgXy5zZXRQb3NpdGlvbigpO1xuXG4gICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDFcbiAgICAgICAgfSk7XG5cbiAgICAgICAgXy4kc2xpZGVyLnJlbW92ZUNsYXNzKCdzbGljay1sb2FkaW5nJyk7XG5cbiAgICAgICAgXy5pbml0VUkoKTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmxhenlMb2FkID09PSAncHJvZ3Jlc3NpdmUnKSB7XG4gICAgICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5uZXh0ID0gU2xpY2sucHJvdG90eXBlLnNsaWNrTmV4dCA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLmNoYW5nZVNsaWRlKHtcbiAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAnbmV4dCdcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLm9yaWVudGF0aW9uQ2hhbmdlID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIF8uY2hlY2tSZXNwb25zaXZlKCk7XG4gICAgICAgIF8uc2V0UG9zaXRpb24oKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUucGF1c2UgPSBTbGljay5wcm90b3R5cGUuc2xpY2tQYXVzZSA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLmF1dG9QbGF5Q2xlYXIoKTtcbiAgICAgICAgXy5wYXVzZWQgPSB0cnVlO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5wbGF5ID0gU2xpY2sucHJvdG90eXBlLnNsaWNrUGxheSA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLmF1dG9QbGF5KCk7XG4gICAgICAgIF8ub3B0aW9ucy5hdXRvcGxheSA9IHRydWU7XG4gICAgICAgIF8ucGF1c2VkID0gZmFsc2U7XG4gICAgICAgIF8uZm9jdXNzZWQgPSBmYWxzZTtcbiAgICAgICAgXy5pbnRlcnJ1cHRlZCA9IGZhbHNlO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5wb3N0U2xpZGUgPSBmdW5jdGlvbihpbmRleCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiggIV8udW5zbGlja2VkICkge1xuXG4gICAgICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignYWZ0ZXJDaGFuZ2UnLCBbXywgaW5kZXhdKTtcblxuICAgICAgICAgICAgXy5hbmltYXRpbmcgPSBmYWxzZTtcblxuICAgICAgICAgICAgXy5zZXRQb3NpdGlvbigpO1xuXG4gICAgICAgICAgICBfLnN3aXBlTGVmdCA9IG51bGw7XG5cbiAgICAgICAgICAgIGlmICggXy5vcHRpb25zLmF1dG9wbGF5ICkge1xuICAgICAgICAgICAgICAgIF8uYXV0b1BsYXkoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5hY2Nlc3NpYmlsaXR5ID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgXy5pbml0QURBKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5wcmV2ID0gU2xpY2sucHJvdG90eXBlLnNsaWNrUHJldiA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLmNoYW5nZVNsaWRlKHtcbiAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAncHJldmlvdXMnXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5wcmV2ZW50RGVmYXVsdCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUucHJvZ3Jlc3NpdmVMYXp5TG9hZCA9IGZ1bmN0aW9uKCB0cnlDb3VudCApIHtcblxuICAgICAgICB0cnlDb3VudCA9IHRyeUNvdW50IHx8IDE7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgJGltZ3NUb0xvYWQgPSAkKCAnaW1nW2RhdGEtbGF6eV0nLCBfLiRzbGlkZXIgKSxcbiAgICAgICAgICAgIGltYWdlLFxuICAgICAgICAgICAgaW1hZ2VTb3VyY2UsXG4gICAgICAgICAgICBpbWFnZVRvTG9hZDtcblxuICAgICAgICBpZiAoICRpbWdzVG9Mb2FkLmxlbmd0aCApIHtcblxuICAgICAgICAgICAgaW1hZ2UgPSAkaW1nc1RvTG9hZC5maXJzdCgpO1xuICAgICAgICAgICAgaW1hZ2VTb3VyY2UgPSBpbWFnZS5hdHRyKCdkYXRhLWxhenknKTtcbiAgICAgICAgICAgIGltYWdlVG9Mb2FkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW1nJyk7XG5cbiAgICAgICAgICAgIGltYWdlVG9Mb2FkLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgaW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgLmF0dHIoICdzcmMnLCBpbWFnZVNvdXJjZSApXG4gICAgICAgICAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdkYXRhLWxhenknKVxuICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcblxuICAgICAgICAgICAgICAgIGlmICggXy5vcHRpb25zLmFkYXB0aXZlSGVpZ2h0ID09PSB0cnVlICkge1xuICAgICAgICAgICAgICAgICAgICBfLnNldFBvc2l0aW9uKCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkZWQnLCBbIF8sIGltYWdlLCBpbWFnZVNvdXJjZSBdKTtcbiAgICAgICAgICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcblxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaW1hZ2VUb0xvYWQub25lcnJvciA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgaWYgKCB0cnlDb3VudCA8IDMgKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgICAgICAgICAqIHRyeSB0byBsb2FkIHRoZSBpbWFnZSAzIHRpbWVzLFxuICAgICAgICAgICAgICAgICAgICAgKiBsZWF2ZSBhIHNsaWdodCBkZWxheSBzbyB3ZSBkb24ndCBnZXRcbiAgICAgICAgICAgICAgICAgICAgICogc2VydmVycyBibG9ja2luZyB0aGUgcmVxdWVzdC5cbiAgICAgICAgICAgICAgICAgICAgICovXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgXy5wcm9ncmVzc2l2ZUxhenlMb2FkKCB0cnlDb3VudCArIDEgKTtcbiAgICAgICAgICAgICAgICAgICAgfSwgNTAwICk7XG5cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgICAgIGltYWdlXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQXR0ciggJ2RhdGEtbGF6eScgKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCAnc2xpY2stbG9hZGluZycgKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCAnc2xpY2stbGF6eWxvYWQtZXJyb3InICk7XG5cbiAgICAgICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkRXJyb3InLCBbIF8sIGltYWdlLCBpbWFnZVNvdXJjZSBdKTtcblxuICAgICAgICAgICAgICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaW1hZ2VUb0xvYWQuc3JjID0gaW1hZ2VTb3VyY2U7XG5cbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2FsbEltYWdlc0xvYWRlZCcsIFsgXyBdKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnJlZnJlc2ggPSBmdW5jdGlvbiggaW5pdGlhbGl6aW5nICkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcywgY3VycmVudFNsaWRlLCBsYXN0VmlzaWJsZUluZGV4O1xuXG4gICAgICAgIGxhc3RWaXNpYmxlSW5kZXggPSBfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xuXG4gICAgICAgIC8vIGluIG5vbi1pbmZpbml0ZSBzbGlkZXJzLCB3ZSBkb24ndCB3YW50IHRvIGdvIHBhc3QgdGhlXG4gICAgICAgIC8vIGxhc3QgdmlzaWJsZSBpbmRleC5cbiAgICAgICAgaWYoICFfLm9wdGlvbnMuaW5maW5pdGUgJiYgKCBfLmN1cnJlbnRTbGlkZSA+IGxhc3RWaXNpYmxlSW5kZXggKSkge1xuICAgICAgICAgICAgXy5jdXJyZW50U2xpZGUgPSBsYXN0VmlzaWJsZUluZGV4O1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gaWYgbGVzcyBzbGlkZXMgdGhhbiB0byBzaG93LCBnbyB0byBzdGFydC5cbiAgICAgICAgaWYgKCBfLnNsaWRlQ291bnQgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyApIHtcbiAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gMDtcblxuICAgICAgICB9XG5cbiAgICAgICAgY3VycmVudFNsaWRlID0gXy5jdXJyZW50U2xpZGU7XG5cbiAgICAgICAgXy5kZXN0cm95KHRydWUpO1xuXG4gICAgICAgICQuZXh0ZW5kKF8sIF8uaW5pdGlhbHMsIHsgY3VycmVudFNsaWRlOiBjdXJyZW50U2xpZGUgfSk7XG5cbiAgICAgICAgXy5pbml0KCk7XG5cbiAgICAgICAgaWYoICFpbml0aWFsaXppbmcgKSB7XG5cbiAgICAgICAgICAgIF8uY2hhbmdlU2xpZGUoe1xuICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ2luZGV4JyxcbiAgICAgICAgICAgICAgICAgICAgaW5kZXg6IGN1cnJlbnRTbGlkZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIGZhbHNlKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnJlZ2lzdGVyQnJlYWtwb2ludHMgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsIGJyZWFrcG9pbnQsIGN1cnJlbnRCcmVha3BvaW50LCBsLFxuICAgICAgICAgICAgcmVzcG9uc2l2ZVNldHRpbmdzID0gXy5vcHRpb25zLnJlc3BvbnNpdmUgfHwgbnVsbDtcblxuICAgICAgICBpZiAoICQudHlwZShyZXNwb25zaXZlU2V0dGluZ3MpID09PSAnYXJyYXknICYmIHJlc3BvbnNpdmVTZXR0aW5ncy5sZW5ndGggKSB7XG5cbiAgICAgICAgICAgIF8ucmVzcG9uZFRvID0gXy5vcHRpb25zLnJlc3BvbmRUbyB8fCAnd2luZG93JztcblxuICAgICAgICAgICAgZm9yICggYnJlYWtwb2ludCBpbiByZXNwb25zaXZlU2V0dGluZ3MgKSB7XG5cbiAgICAgICAgICAgICAgICBsID0gXy5icmVha3BvaW50cy5sZW5ndGgtMTtcbiAgICAgICAgICAgICAgICBjdXJyZW50QnJlYWtwb2ludCA9IHJlc3BvbnNpdmVTZXR0aW5nc1ticmVha3BvaW50XS5icmVha3BvaW50O1xuXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNpdmVTZXR0aW5ncy5oYXNPd25Qcm9wZXJ0eShicmVha3BvaW50KSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvb3AgdGhyb3VnaCB0aGUgYnJlYWtwb2ludHMgYW5kIGN1dCBvdXQgYW55IGV4aXN0aW5nXG4gICAgICAgICAgICAgICAgICAgIC8vIG9uZXMgd2l0aCB0aGUgc2FtZSBicmVha3BvaW50IG51bWJlciwgd2UgZG9uJ3Qgd2FudCBkdXBlcy5cbiAgICAgICAgICAgICAgICAgICAgd2hpbGUoIGwgPj0gMCApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCBfLmJyZWFrcG9pbnRzW2xdICYmIF8uYnJlYWtwb2ludHNbbF0gPT09IGN1cnJlbnRCcmVha3BvaW50ICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uYnJlYWtwb2ludHMuc3BsaWNlKGwsMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBsLS07XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBfLmJyZWFrcG9pbnRzLnB1c2goY3VycmVudEJyZWFrcG9pbnQpO1xuICAgICAgICAgICAgICAgICAgICBfLmJyZWFrcG9pbnRTZXR0aW5nc1tjdXJyZW50QnJlYWtwb2ludF0gPSByZXNwb25zaXZlU2V0dGluZ3NbYnJlYWtwb2ludF0uc2V0dGluZ3M7XG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgXy5icmVha3BvaW50cy5zb3J0KGZ1bmN0aW9uKGEsIGIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKCBfLm9wdGlvbnMubW9iaWxlRmlyc3QgKSA/IGEtYiA6IGItYTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUucmVpbml0ID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIF8uJHNsaWRlcyA9XG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrXG4gICAgICAgICAgICAgICAgLmNoaWxkcmVuKF8ub3B0aW9ucy5zbGlkZSlcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLXNsaWRlJyk7XG5cbiAgICAgICAgXy5zbGlkZUNvdW50ID0gXy4kc2xpZGVzLmxlbmd0aDtcblxuICAgICAgICBpZiAoXy5jdXJyZW50U2xpZGUgPj0gXy5zbGlkZUNvdW50ICYmIF8uY3VycmVudFNsaWRlICE9PSAwKSB7XG4gICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IF8uY3VycmVudFNsaWRlIC0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IDA7XG4gICAgICAgIH1cblxuICAgICAgICBfLnJlZ2lzdGVyQnJlYWtwb2ludHMoKTtcblxuICAgICAgICBfLnNldFByb3BzKCk7XG4gICAgICAgIF8uc2V0dXBJbmZpbml0ZSgpO1xuICAgICAgICBfLmJ1aWxkQXJyb3dzKCk7XG4gICAgICAgIF8udXBkYXRlQXJyb3dzKCk7XG4gICAgICAgIF8uaW5pdEFycm93RXZlbnRzKCk7XG4gICAgICAgIF8uYnVpbGREb3RzKCk7XG4gICAgICAgIF8udXBkYXRlRG90cygpO1xuICAgICAgICBfLmluaXREb3RFdmVudHMoKTtcbiAgICAgICAgXy5jbGVhblVwU2xpZGVFdmVudHMoKTtcbiAgICAgICAgXy5pbml0U2xpZGVFdmVudHMoKTtcblxuICAgICAgICBfLmNoZWNrUmVzcG9uc2l2ZShmYWxzZSwgdHJ1ZSk7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uU2VsZWN0ID09PSB0cnVlKSB7XG4gICAgICAgICAgICAkKF8uJHNsaWRlVHJhY2spLmNoaWxkcmVuKCkub24oJ2NsaWNrLnNsaWNrJywgXy5zZWxlY3RIYW5kbGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIF8uc2V0U2xpZGVDbGFzc2VzKHR5cGVvZiBfLmN1cnJlbnRTbGlkZSA9PT0gJ251bWJlcicgPyBfLmN1cnJlbnRTbGlkZSA6IDApO1xuXG4gICAgICAgIF8uc2V0UG9zaXRpb24oKTtcbiAgICAgICAgXy5mb2N1c0hhbmRsZXIoKTtcblxuICAgICAgICBfLnBhdXNlZCA9ICFfLm9wdGlvbnMuYXV0b3BsYXk7XG4gICAgICAgIF8uYXV0b1BsYXkoKTtcblxuICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcigncmVJbml0JywgW19dKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUucmVzaXplID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSAhPT0gXy53aW5kb3dXaWR0aCkge1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KF8ud2luZG93RGVsYXkpO1xuICAgICAgICAgICAgXy53aW5kb3dEZWxheSA9IHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIF8ud2luZG93V2lkdGggPSAkKHdpbmRvdykud2lkdGgoKTtcbiAgICAgICAgICAgICAgICBfLmNoZWNrUmVzcG9uc2l2ZSgpO1xuICAgICAgICAgICAgICAgIGlmKCAhXy51bnNsaWNrZWQgKSB7IF8uc2V0UG9zaXRpb24oKTsgfVxuICAgICAgICAgICAgfSwgNTApO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5yZW1vdmVTbGlkZSA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1JlbW92ZSA9IGZ1bmN0aW9uKGluZGV4LCByZW1vdmVCZWZvcmUsIHJlbW92ZUFsbCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAodHlwZW9mKGluZGV4KSA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgICAgICByZW1vdmVCZWZvcmUgPSBpbmRleDtcbiAgICAgICAgICAgIGluZGV4ID0gcmVtb3ZlQmVmb3JlID09PSB0cnVlID8gMCA6IF8uc2xpZGVDb3VudCAtIDE7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpbmRleCA9IHJlbW92ZUJlZm9yZSA9PT0gdHJ1ZSA/IC0taW5kZXggOiBpbmRleDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLnNsaWRlQ291bnQgPCAxIHx8IGluZGV4IDwgMCB8fCBpbmRleCA+IF8uc2xpZGVDb3VudCAtIDEpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIF8udW5sb2FkKCk7XG5cbiAgICAgICAgaWYgKHJlbW92ZUFsbCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbigpLnJlbW92ZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmVxKGluZGV4KS5yZW1vdmUoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIF8uJHNsaWRlcyA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4odGhpcy5vcHRpb25zLnNsaWRlKTtcblxuICAgICAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSkuZGV0YWNoKCk7XG5cbiAgICAgICAgXy4kc2xpZGVUcmFjay5hcHBlbmQoXy4kc2xpZGVzKTtcblxuICAgICAgICBfLiRzbGlkZXNDYWNoZSA9IF8uJHNsaWRlcztcblxuICAgICAgICBfLnJlaW5pdCgpO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRDU1MgPSBmdW5jdGlvbihwb3NpdGlvbikge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIHBvc2l0aW9uUHJvcHMgPSB7fSxcbiAgICAgICAgICAgIHgsIHk7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcbiAgICAgICAgICAgIHBvc2l0aW9uID0gLXBvc2l0aW9uO1xuICAgICAgICB9XG4gICAgICAgIHggPSBfLnBvc2l0aW9uUHJvcCA9PSAnbGVmdCcgPyBNYXRoLmNlaWwocG9zaXRpb24pICsgJ3B4JyA6ICcwcHgnO1xuICAgICAgICB5ID0gXy5wb3NpdGlvblByb3AgPT0gJ3RvcCcgPyBNYXRoLmNlaWwocG9zaXRpb24pICsgJ3B4JyA6ICcwcHgnO1xuXG4gICAgICAgIHBvc2l0aW9uUHJvcHNbXy5wb3NpdGlvblByb3BdID0gcG9zaXRpb247XG5cbiAgICAgICAgaWYgKF8udHJhbnNmb3Jtc0VuYWJsZWQgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhwb3NpdGlvblByb3BzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHBvc2l0aW9uUHJvcHMgPSB7fTtcbiAgICAgICAgICAgIGlmIChfLmNzc1RyYW5zaXRpb25zID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlKCcgKyB4ICsgJywgJyArIHkgKyAnKSc7XG4gICAgICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jc3MocG9zaXRpb25Qcm9wcyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlM2QoJyArIHggKyAnLCAnICsgeSArICcsIDBweCknO1xuICAgICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKHBvc2l0aW9uUHJvcHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnNldERpbWVuc2lvbnMgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIF8uJGxpc3QuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogKCcwcHggJyArIF8ub3B0aW9ucy5jZW50ZXJQYWRkaW5nKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXy4kbGlzdC5oZWlnaHQoXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJIZWlnaHQodHJ1ZSkgKiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIF8uJGxpc3QuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogKF8ub3B0aW9ucy5jZW50ZXJQYWRkaW5nICsgJyAwcHgnKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgXy5saXN0V2lkdGggPSBfLiRsaXN0LndpZHRoKCk7XG4gICAgICAgIF8ubGlzdEhlaWdodCA9IF8uJGxpc3QuaGVpZ2h0KCk7XG5cblxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSAmJiBfLm9wdGlvbnMudmFyaWFibGVXaWR0aCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIF8uc2xpZGVXaWR0aCA9IE1hdGguY2VpbChfLmxpc3RXaWR0aCAvIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpO1xuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay53aWR0aChNYXRoLmNlaWwoKF8uc2xpZGVXaWR0aCAqIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmxlbmd0aCkpKTtcblxuICAgICAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy52YXJpYWJsZVdpZHRoID09PSB0cnVlKSB7XG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLndpZHRoKDUwMDAgKiBfLnNsaWRlQ291bnQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXy5zbGlkZVdpZHRoID0gTWF0aC5jZWlsKF8ubGlzdFdpZHRoKTtcbiAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suaGVpZ2h0KE1hdGguY2VpbCgoXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJIZWlnaHQodHJ1ZSkgKiBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCcuc2xpY2stc2xpZGUnKS5sZW5ndGgpKSk7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgb2Zmc2V0ID0gXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJXaWR0aCh0cnVlKSAtIF8uJHNsaWRlcy5maXJzdCgpLndpZHRoKCk7XG4gICAgICAgIGlmIChfLm9wdGlvbnMudmFyaWFibGVXaWR0aCA9PT0gZmFsc2UpIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLndpZHRoKF8uc2xpZGVXaWR0aCAtIG9mZnNldCk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnNldEZhZGUgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICB0YXJnZXRMZWZ0O1xuXG4gICAgICAgIF8uJHNsaWRlcy5lYWNoKGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7XG4gICAgICAgICAgICB0YXJnZXRMZWZ0ID0gKF8uc2xpZGVXaWR0aCAqIGluZGV4KSAqIC0xO1xuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAkKGVsZW1lbnQpLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgICAgICAgICAgICAgICByaWdodDogdGFyZ2V0TGVmdCxcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAwLFxuICAgICAgICAgICAgICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAyLFxuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQoZWxlbWVudCkuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IHRhcmdldExlZnQsXG4gICAgICAgICAgICAgICAgICAgIHRvcDogMCxcbiAgICAgICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4IC0gMixcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBfLiRzbGlkZXMuZXEoXy5jdXJyZW50U2xpZGUpLmNzcyh7XG4gICAgICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAxLFxuICAgICAgICAgICAgb3BhY2l0eTogMVxuICAgICAgICB9KTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuc2V0SGVpZ2h0ID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID09PSAxICYmIF8ub3B0aW9ucy5hZGFwdGl2ZUhlaWdodCA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICB2YXIgdGFyZ2V0SGVpZ2h0ID0gXy4kc2xpZGVzLmVxKF8uY3VycmVudFNsaWRlKS5vdXRlckhlaWdodCh0cnVlKTtcbiAgICAgICAgICAgIF8uJGxpc3QuY3NzKCdoZWlnaHQnLCB0YXJnZXRIZWlnaHQpO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnNldE9wdGlvbiA9XG4gICAgU2xpY2sucHJvdG90eXBlLnNsaWNrU2V0T3B0aW9uID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIGFjY2VwdHMgYXJndW1lbnRzIGluIGZvcm1hdCBvZjpcbiAgICAgICAgICpcbiAgICAgICAgICogIC0gZm9yIGNoYW5naW5nIGEgc2luZ2xlIG9wdGlvbidzIHZhbHVlOlxuICAgICAgICAgKiAgICAgLnNsaWNrKFwic2V0T3B0aW9uXCIsIG9wdGlvbiwgdmFsdWUsIHJlZnJlc2ggKVxuICAgICAgICAgKlxuICAgICAgICAgKiAgLSBmb3IgY2hhbmdpbmcgYSBzZXQgb2YgcmVzcG9uc2l2ZSBvcHRpb25zOlxuICAgICAgICAgKiAgICAgLnNsaWNrKFwic2V0T3B0aW9uXCIsICdyZXNwb25zaXZlJywgW3t9LCAuLi5dLCByZWZyZXNoIClcbiAgICAgICAgICpcbiAgICAgICAgICogIC0gZm9yIHVwZGF0aW5nIG11bHRpcGxlIHZhbHVlcyBhdCBvbmNlIChub3QgcmVzcG9uc2l2ZSlcbiAgICAgICAgICogICAgIC5zbGljayhcInNldE9wdGlvblwiLCB7ICdvcHRpb24nOiB2YWx1ZSwgLi4uIH0sIHJlZnJlc2ggKVxuICAgICAgICAgKi9cblxuICAgICAgICB2YXIgXyA9IHRoaXMsIGwsIGl0ZW0sIG9wdGlvbiwgdmFsdWUsIHJlZnJlc2ggPSBmYWxzZSwgdHlwZTtcblxuICAgICAgICBpZiggJC50eXBlKCBhcmd1bWVudHNbMF0gKSA9PT0gJ29iamVjdCcgKSB7XG5cbiAgICAgICAgICAgIG9wdGlvbiA9ICBhcmd1bWVudHNbMF07XG4gICAgICAgICAgICByZWZyZXNoID0gYXJndW1lbnRzWzFdO1xuICAgICAgICAgICAgdHlwZSA9ICdtdWx0aXBsZSc7XG5cbiAgICAgICAgfSBlbHNlIGlmICggJC50eXBlKCBhcmd1bWVudHNbMF0gKSA9PT0gJ3N0cmluZycgKSB7XG5cbiAgICAgICAgICAgIG9wdGlvbiA9ICBhcmd1bWVudHNbMF07XG4gICAgICAgICAgICB2YWx1ZSA9IGFyZ3VtZW50c1sxXTtcbiAgICAgICAgICAgIHJlZnJlc2ggPSBhcmd1bWVudHNbMl07XG5cbiAgICAgICAgICAgIGlmICggYXJndW1lbnRzWzBdID09PSAncmVzcG9uc2l2ZScgJiYgJC50eXBlKCBhcmd1bWVudHNbMV0gKSA9PT0gJ2FycmF5JyApIHtcblxuICAgICAgICAgICAgICAgIHR5cGUgPSAncmVzcG9uc2l2ZSc7XG5cbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIHR5cGVvZiBhcmd1bWVudHNbMV0gIT09ICd1bmRlZmluZWQnICkge1xuXG4gICAgICAgICAgICAgICAgdHlwZSA9ICdzaW5nbGUnO1xuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICggdHlwZSA9PT0gJ3NpbmdsZScgKSB7XG5cbiAgICAgICAgICAgIF8ub3B0aW9uc1tvcHRpb25dID0gdmFsdWU7XG5cblxuICAgICAgICB9IGVsc2UgaWYgKCB0eXBlID09PSAnbXVsdGlwbGUnICkge1xuXG4gICAgICAgICAgICAkLmVhY2goIG9wdGlvbiAsIGZ1bmN0aW9uKCBvcHQsIHZhbCApIHtcblxuICAgICAgICAgICAgICAgIF8ub3B0aW9uc1tvcHRdID0gdmFsO1xuXG4gICAgICAgICAgICB9KTtcblxuXG4gICAgICAgIH0gZWxzZSBpZiAoIHR5cGUgPT09ICdyZXNwb25zaXZlJyApIHtcblxuICAgICAgICAgICAgZm9yICggaXRlbSBpbiB2YWx1ZSApIHtcblxuICAgICAgICAgICAgICAgIGlmKCAkLnR5cGUoIF8ub3B0aW9ucy5yZXNwb25zaXZlICkgIT09ICdhcnJheScgKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnJlc3BvbnNpdmUgPSBbIHZhbHVlW2l0ZW1dIF07XG5cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgICAgIGwgPSBfLm9wdGlvbnMucmVzcG9uc2l2ZS5sZW5ndGgtMTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBsb29wIHRocm91Z2ggdGhlIHJlc3BvbnNpdmUgb2JqZWN0IGFuZCBzcGxpY2Ugb3V0IGR1cGxpY2F0ZXMuXG4gICAgICAgICAgICAgICAgICAgIHdoaWxlKCBsID49IDAgKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCBfLm9wdGlvbnMucmVzcG9uc2l2ZVtsXS5icmVha3BvaW50ID09PSB2YWx1ZVtpdGVtXS5icmVha3BvaW50ICkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnJlc3BvbnNpdmUuc3BsaWNlKGwsMSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgbC0tO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBfLm9wdGlvbnMucmVzcG9uc2l2ZS5wdXNoKCB2YWx1ZVtpdGVtXSApO1xuXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICggcmVmcmVzaCApIHtcblxuICAgICAgICAgICAgXy51bmxvYWQoKTtcbiAgICAgICAgICAgIF8ucmVpbml0KCk7XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRQb3NpdGlvbiA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBfLnNldERpbWVuc2lvbnMoKTtcblxuICAgICAgICBfLnNldEhlaWdodCgpO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIF8uc2V0Q1NTKF8uZ2V0TGVmdChfLmN1cnJlbnRTbGlkZSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXy5zZXRGYWRlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignc2V0UG9zaXRpb24nLCBbX10pO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRQcm9wcyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIGJvZHlTdHlsZSA9IGRvY3VtZW50LmJvZHkuc3R5bGU7XG5cbiAgICAgICAgXy5wb3NpdGlvblByb3AgPSBfLm9wdGlvbnMudmVydGljYWwgPT09IHRydWUgPyAndG9wJyA6ICdsZWZ0JztcblxuICAgICAgICBpZiAoXy5wb3NpdGlvblByb3AgPT09ICd0b3AnKSB7XG4gICAgICAgICAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLXZlcnRpY2FsJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfLiRzbGlkZXIucmVtb3ZlQ2xhc3MoJ3NsaWNrLXZlcnRpY2FsJyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYm9keVN0eWxlLldlYmtpdFRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCB8fFxuICAgICAgICAgICAgYm9keVN0eWxlLk1velRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCB8fFxuICAgICAgICAgICAgYm9keVN0eWxlLm1zVHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZiAoXy5vcHRpb25zLnVzZUNTUyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIF8uY3NzVHJhbnNpdGlvbnMgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCBfLm9wdGlvbnMuZmFkZSApIHtcbiAgICAgICAgICAgIGlmICggdHlwZW9mIF8ub3B0aW9ucy56SW5kZXggPT09ICdudW1iZXInICkge1xuICAgICAgICAgICAgICAgIGlmKCBfLm9wdGlvbnMuekluZGV4IDwgMyApIHtcbiAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnpJbmRleCA9IDM7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBfLm9wdGlvbnMuekluZGV4ID0gXy5kZWZhdWx0cy56SW5kZXg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYm9keVN0eWxlLk9UcmFuc2Zvcm0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgXy5hbmltVHlwZSA9ICdPVHJhbnNmb3JtJztcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctby10cmFuc2Zvcm0nO1xuICAgICAgICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICdPVHJhbnNpdGlvbic7XG4gICAgICAgICAgICBpZiAoYm9keVN0eWxlLnBlcnNwZWN0aXZlUHJvcGVydHkgPT09IHVuZGVmaW5lZCAmJiBib2R5U3R5bGUud2Via2l0UGVyc3BlY3RpdmUgPT09IHVuZGVmaW5lZCkgXy5hbmltVHlwZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChib2R5U3R5bGUuTW96VHJhbnNmb3JtICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIF8uYW5pbVR5cGUgPSAnTW96VHJhbnNmb3JtJztcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctbW96LXRyYW5zZm9ybSc7XG4gICAgICAgICAgICBfLnRyYW5zaXRpb25UeXBlID0gJ01velRyYW5zaXRpb24nO1xuICAgICAgICAgICAgaWYgKGJvZHlTdHlsZS5wZXJzcGVjdGl2ZVByb3BlcnR5ID09PSB1bmRlZmluZWQgJiYgYm9keVN0eWxlLk1velBlcnNwZWN0aXZlID09PSB1bmRlZmluZWQpIF8uYW5pbVR5cGUgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoYm9keVN0eWxlLndlYmtpdFRyYW5zZm9ybSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBfLmFuaW1UeXBlID0gJ3dlYmtpdFRyYW5zZm9ybSc7XG4gICAgICAgICAgICBfLnRyYW5zZm9ybVR5cGUgPSAnLXdlYmtpdC10cmFuc2Zvcm0nO1xuICAgICAgICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICd3ZWJraXRUcmFuc2l0aW9uJztcbiAgICAgICAgICAgIGlmIChib2R5U3R5bGUucGVyc3BlY3RpdmVQcm9wZXJ0eSA9PT0gdW5kZWZpbmVkICYmIGJvZHlTdHlsZS53ZWJraXRQZXJzcGVjdGl2ZSA9PT0gdW5kZWZpbmVkKSBfLmFuaW1UeXBlID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGJvZHlTdHlsZS5tc1RyYW5zZm9ybSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBfLmFuaW1UeXBlID0gJ21zVHJhbnNmb3JtJztcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctbXMtdHJhbnNmb3JtJztcbiAgICAgICAgICAgIF8udHJhbnNpdGlvblR5cGUgPSAnbXNUcmFuc2l0aW9uJztcbiAgICAgICAgICAgIGlmIChib2R5U3R5bGUubXNUcmFuc2Zvcm0gPT09IHVuZGVmaW5lZCkgXy5hbmltVHlwZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChib2R5U3R5bGUudHJhbnNmb3JtICE9PSB1bmRlZmluZWQgJiYgXy5hbmltVHlwZSAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgIF8uYW5pbVR5cGUgPSAndHJhbnNmb3JtJztcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICd0cmFuc2Zvcm0nO1xuICAgICAgICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICd0cmFuc2l0aW9uJztcbiAgICAgICAgfVxuICAgICAgICBfLnRyYW5zZm9ybXNFbmFibGVkID0gXy5vcHRpb25zLnVzZVRyYW5zZm9ybSAmJiAoXy5hbmltVHlwZSAhPT0gbnVsbCAmJiBfLmFuaW1UeXBlICE9PSBmYWxzZSk7XG4gICAgfTtcblxuXG4gICAgU2xpY2sucHJvdG90eXBlLnNldFNsaWRlQ2xhc3NlcyA9IGZ1bmN0aW9uKGluZGV4KSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzLFxuICAgICAgICAgICAgY2VudGVyT2Zmc2V0LCBhbGxTbGlkZXMsIGluZGV4T2Zmc2V0LCByZW1haW5kZXI7XG5cbiAgICAgICAgYWxsU2xpZGVzID0gXy4kc2xpZGVyXG4gICAgICAgICAgICAuZmluZCgnLnNsaWNrLXNsaWRlJylcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnc2xpY2stYWN0aXZlIHNsaWNrLWNlbnRlciBzbGljay1jdXJyZW50JylcbiAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG5cbiAgICAgICAgXy4kc2xpZGVzXG4gICAgICAgICAgICAuZXEoaW5kZXgpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWN1cnJlbnQnKTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcblxuICAgICAgICAgICAgY2VudGVyT2Zmc2V0ID0gTWF0aC5mbG9vcihfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC8gMik7XG5cbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUpIHtcblxuICAgICAgICAgICAgICAgIGlmIChpbmRleCA+PSBjZW50ZXJPZmZzZXQgJiYgaW5kZXggPD0gKF8uc2xpZGVDb3VudCAtIDEpIC0gY2VudGVyT2Zmc2V0KSB7XG5cbiAgICAgICAgICAgICAgICAgICAgXy4kc2xpZGVzXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2xpY2UoaW5kZXggLSBjZW50ZXJPZmZzZXQsIGluZGV4ICsgY2VudGVyT2Zmc2V0ICsgMSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xuXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICBpbmRleE9mZnNldCA9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyBpbmRleDtcbiAgICAgICAgICAgICAgICAgICAgYWxsU2xpZGVzXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2xpY2UoaW5kZXhPZmZzZXQgLSBjZW50ZXJPZmZzZXQgKyAxLCBpbmRleE9mZnNldCArIGNlbnRlck9mZnNldCArIDIpXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpXG4gICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuXG4gICAgICAgICAgICAgICAgICAgIGFsbFNsaWRlc1xuICAgICAgICAgICAgICAgICAgICAgICAgLmVxKGFsbFNsaWRlcy5sZW5ndGggLSAxIC0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stY2VudGVyJyk7XG5cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGluZGV4ID09PSBfLnNsaWRlQ291bnQgLSAxKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgYWxsU2xpZGVzXG4gICAgICAgICAgICAgICAgICAgICAgICAuZXEoXy5vcHRpb25zLnNsaWRlc1RvU2hvdylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stY2VudGVyJyk7XG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgXy4kc2xpZGVzXG4gICAgICAgICAgICAgICAgLmVxKGluZGV4KVxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stY2VudGVyJyk7XG5cbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgaWYgKGluZGV4ID49IDAgJiYgaW5kZXggPD0gKF8uc2xpZGVDb3VudCAtIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpKSB7XG5cbiAgICAgICAgICAgICAgICBfLiRzbGlkZXNcbiAgICAgICAgICAgICAgICAgICAgLnNsaWNlKGluZGV4LCBpbmRleCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpXG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG5cbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYWxsU2xpZGVzLmxlbmd0aCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG5cbiAgICAgICAgICAgICAgICBhbGxTbGlkZXNcbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1hY3RpdmUnKVxuICAgICAgICAgICAgICAgICAgICAuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIHJlbWFpbmRlciA9IF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3c7XG4gICAgICAgICAgICAgICAgaW5kZXhPZmZzZXQgPSBfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUgPyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgaW5kZXggOiBpbmRleDtcblxuICAgICAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID09IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAmJiAoXy5zbGlkZUNvdW50IC0gaW5kZXgpIDwgXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuXG4gICAgICAgICAgICAgICAgICAgIGFsbFNsaWRlc1xuICAgICAgICAgICAgICAgICAgICAgICAgLnNsaWNlKGluZGV4T2Zmc2V0IC0gKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLSByZW1haW5kZXIpLCBpbmRleE9mZnNldCArIHJlbWFpbmRlcilcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xuXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICBhbGxTbGlkZXNcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zbGljZShpbmRleE9mZnNldCwgaW5kZXhPZmZzZXQgKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1hY3RpdmUnKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy5sYXp5TG9hZCA9PT0gJ29uZGVtYW5kJykge1xuICAgICAgICAgICAgXy5sYXp5TG9hZCgpO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnNldHVwSW5maW5pdGUgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICBpLCBzbGlkZUluZGV4LCBpbmZpbml0ZUNvdW50O1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgXy5vcHRpb25zLmNlbnRlck1vZGUgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUgJiYgXy5vcHRpb25zLmZhZGUgPT09IGZhbHNlKSB7XG5cbiAgICAgICAgICAgIHNsaWRlSW5kZXggPSBudWxsO1xuXG4gICAgICAgICAgICBpZiAoXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xuXG4gICAgICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGluZmluaXRlQ291bnQgPSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgMTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZUNvdW50ID0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdztcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBmb3IgKGkgPSBfLnNsaWRlQ291bnQ7IGkgPiAoXy5zbGlkZUNvdW50IC1cbiAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlQ291bnQpOyBpIC09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVJbmRleCA9IGkgLSAxO1xuICAgICAgICAgICAgICAgICAgICAkKF8uJHNsaWRlc1tzbGlkZUluZGV4XSkuY2xvbmUodHJ1ZSkuYXR0cignaWQnLCAnJylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdkYXRhLXNsaWNrLWluZGV4Jywgc2xpZGVJbmRleCAtIF8uc2xpZGVDb3VudClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5wcmVwZW5kVG8oXy4kc2xpZGVUcmFjaykuYWRkQ2xhc3MoJ3NsaWNrLWNsb25lZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgaW5maW5pdGVDb3VudDsgaSArPSAxKSB7XG4gICAgICAgICAgICAgICAgICAgIHNsaWRlSW5kZXggPSBpO1xuICAgICAgICAgICAgICAgICAgICAkKF8uJHNsaWRlc1tzbGlkZUluZGV4XSkuY2xvbmUodHJ1ZSkuYXR0cignaWQnLCAnJylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdkYXRhLXNsaWNrLWluZGV4Jywgc2xpZGVJbmRleCArIF8uc2xpZGVDb3VudClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKS5hZGRDbGFzcygnc2xpY2stY2xvbmVkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suZmluZCgnLnNsaWNrLWNsb25lZCcpLmZpbmQoJ1tpZF0nKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmF0dHIoJ2lkJywgJycpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5pbnRlcnJ1cHQgPSBmdW5jdGlvbiggdG9nZ2xlICkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiggIXRvZ2dsZSApIHtcbiAgICAgICAgICAgIF8uYXV0b1BsYXkoKTtcbiAgICAgICAgfVxuICAgICAgICBfLmludGVycnVwdGVkID0gdG9nZ2xlO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5zZWxlY3RIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgdmFyIHRhcmdldEVsZW1lbnQgPVxuICAgICAgICAgICAgJChldmVudC50YXJnZXQpLmlzKCcuc2xpY2stc2xpZGUnKSA/XG4gICAgICAgICAgICAgICAgJChldmVudC50YXJnZXQpIDpcbiAgICAgICAgICAgICAgICAkKGV2ZW50LnRhcmdldCkucGFyZW50cygnLnNsaWNrLXNsaWRlJyk7XG5cbiAgICAgICAgdmFyIGluZGV4ID0gcGFyc2VJbnQodGFyZ2V0RWxlbWVudC5hdHRyKCdkYXRhLXNsaWNrLWluZGV4JykpO1xuXG4gICAgICAgIGlmICghaW5kZXgpIGluZGV4ID0gMDtcblxuICAgICAgICBpZiAoXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcblxuICAgICAgICAgICAgXy5zZXRTbGlkZUNsYXNzZXMoaW5kZXgpO1xuICAgICAgICAgICAgXy5hc05hdkZvcihpbmRleCk7XG4gICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgfVxuXG4gICAgICAgIF8uc2xpZGVIYW5kbGVyKGluZGV4KTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuc2xpZGVIYW5kbGVyID0gZnVuY3Rpb24oaW5kZXgsIHN5bmMsIGRvbnRBbmltYXRlKSB7XG5cbiAgICAgICAgdmFyIHRhcmdldFNsaWRlLCBhbmltU2xpZGUsIG9sZFNsaWRlLCBzbGlkZUxlZnQsIHRhcmdldExlZnQgPSBudWxsLFxuICAgICAgICAgICAgXyA9IHRoaXMsIG5hdlRhcmdldDtcblxuICAgICAgICBzeW5jID0gc3luYyB8fCBmYWxzZTtcblxuICAgICAgICBpZiAoXy5hbmltYXRpbmcgPT09IHRydWUgJiYgXy5vcHRpb25zLndhaXRGb3JBbmltYXRlID09PSB0cnVlKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IHRydWUgJiYgXy5jdXJyZW50U2xpZGUgPT09IGluZGV4KSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChzeW5jID09PSBmYWxzZSkge1xuICAgICAgICAgICAgXy5hc05hdkZvcihpbmRleCk7XG4gICAgICAgIH1cblxuICAgICAgICB0YXJnZXRTbGlkZSA9IGluZGV4O1xuICAgICAgICB0YXJnZXRMZWZ0ID0gXy5nZXRMZWZ0KHRhcmdldFNsaWRlKTtcbiAgICAgICAgc2xpZGVMZWZ0ID0gXy5nZXRMZWZ0KF8uY3VycmVudFNsaWRlKTtcblxuICAgICAgICBfLmN1cnJlbnRMZWZ0ID0gXy5zd2lwZUxlZnQgPT09IG51bGwgPyBzbGlkZUxlZnQgOiBfLnN3aXBlTGVmdDtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSBmYWxzZSAmJiBfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gZmFsc2UgJiYgKGluZGV4IDwgMCB8fCBpbmRleCA+IF8uZ2V0RG90Q291bnQoKSAqIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCkpIHtcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uY3VycmVudFNsaWRlO1xuICAgICAgICAgICAgICAgIGlmIChkb250QW5pbWF0ZSAhPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBfLmFuaW1hdGVTbGlkZShzbGlkZUxlZnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgXy5wb3N0U2xpZGUodGFyZ2V0U2xpZGUpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBfLnBvc3RTbGlkZSh0YXJnZXRTbGlkZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UgJiYgXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgJiYgKGluZGV4IDwgMCB8fCBpbmRleCA+IChfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwpKSkge1xuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHRhcmdldFNsaWRlID0gXy5jdXJyZW50U2xpZGU7XG4gICAgICAgICAgICAgICAgaWYgKGRvbnRBbmltYXRlICE9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIF8uYW5pbWF0ZVNsaWRlKHNsaWRlTGVmdCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfLnBvc3RTbGlkZSh0YXJnZXRTbGlkZSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIF8ucG9zdFNsaWRlKHRhcmdldFNsaWRlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5hdXRvcGxheSApIHtcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoXy5hdXRvUGxheVRpbWVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0YXJnZXRTbGlkZSA8IDApIHtcbiAgICAgICAgICAgIGlmIChfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgIT09IDApIHtcbiAgICAgICAgICAgICAgICBhbmltU2xpZGUgPSBfLnNsaWRlQ291bnQgLSAoXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgYW5pbVNsaWRlID0gXy5zbGlkZUNvdW50ICsgdGFyZ2V0U2xpZGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAodGFyZ2V0U2xpZGUgPj0gXy5zbGlkZUNvdW50KSB7XG4gICAgICAgICAgICBpZiAoXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgYW5pbVNsaWRlID0gMDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgYW5pbVNsaWRlID0gdGFyZ2V0U2xpZGUgLSBfLnNsaWRlQ291bnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhbmltU2xpZGUgPSB0YXJnZXRTbGlkZTtcbiAgICAgICAgfVxuXG4gICAgICAgIF8uYW5pbWF0aW5nID0gdHJ1ZTtcblxuICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignYmVmb3JlQ2hhbmdlJywgW18sIF8uY3VycmVudFNsaWRlLCBhbmltU2xpZGVdKTtcblxuICAgICAgICBvbGRTbGlkZSA9IF8uY3VycmVudFNsaWRlO1xuICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IGFuaW1TbGlkZTtcblxuICAgICAgICBfLnNldFNsaWRlQ2xhc3NlcyhfLmN1cnJlbnRTbGlkZSk7XG5cbiAgICAgICAgaWYgKCBfLm9wdGlvbnMuYXNOYXZGb3IgKSB7XG5cbiAgICAgICAgICAgIG5hdlRhcmdldCA9IF8uZ2V0TmF2VGFyZ2V0KCk7XG4gICAgICAgICAgICBuYXZUYXJnZXQgPSBuYXZUYXJnZXQuc2xpY2soJ2dldFNsaWNrJyk7XG5cbiAgICAgICAgICAgIGlmICggbmF2VGFyZ2V0LnNsaWRlQ291bnQgPD0gbmF2VGFyZ2V0Lm9wdGlvbnMuc2xpZGVzVG9TaG93ICkge1xuICAgICAgICAgICAgICAgIG5hdlRhcmdldC5zZXRTbGlkZUNsYXNzZXMoXy5jdXJyZW50U2xpZGUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1cblxuICAgICAgICBfLnVwZGF0ZURvdHMoKTtcbiAgICAgICAgXy51cGRhdGVBcnJvd3MoKTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IHRydWUpIHtcbiAgICAgICAgICAgIGlmIChkb250QW5pbWF0ZSAhPT0gdHJ1ZSkge1xuXG4gICAgICAgICAgICAgICAgXy5mYWRlU2xpZGVPdXQob2xkU2xpZGUpO1xuXG4gICAgICAgICAgICAgICAgXy5mYWRlU2xpZGUoYW5pbVNsaWRlLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgXy5wb3N0U2xpZGUoYW5pbVNsaWRlKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBfLnBvc3RTbGlkZShhbmltU2xpZGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXy5hbmltYXRlSGVpZ2h0KCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZG9udEFuaW1hdGUgIT09IHRydWUpIHtcbiAgICAgICAgICAgIF8uYW5pbWF0ZVNsaWRlKHRhcmdldExlZnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIF8ucG9zdFNsaWRlKGFuaW1TbGlkZSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIF8ucG9zdFNsaWRlKGFuaW1TbGlkZSk7XG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuc3RhcnRMb2FkID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgdmFyIF8gPSB0aGlzO1xuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcblxuICAgICAgICAgICAgXy4kcHJldkFycm93LmhpZGUoKTtcbiAgICAgICAgICAgIF8uJG5leHRBcnJvdy5oaWRlKCk7XG5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG5cbiAgICAgICAgICAgIF8uJGRvdHMuaGlkZSgpO1xuXG4gICAgICAgIH1cblxuICAgICAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuc3dpcGVEaXJlY3Rpb24gPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgeERpc3QsIHlEaXN0LCByLCBzd2lwZUFuZ2xlLCBfID0gdGhpcztcblxuICAgICAgICB4RGlzdCA9IF8udG91Y2hPYmplY3Quc3RhcnRYIC0gXy50b3VjaE9iamVjdC5jdXJYO1xuICAgICAgICB5RGlzdCA9IF8udG91Y2hPYmplY3Quc3RhcnRZIC0gXy50b3VjaE9iamVjdC5jdXJZO1xuICAgICAgICByID0gTWF0aC5hdGFuMih5RGlzdCwgeERpc3QpO1xuXG4gICAgICAgIHN3aXBlQW5nbGUgPSBNYXRoLnJvdW5kKHIgKiAxODAgLyBNYXRoLlBJKTtcbiAgICAgICAgaWYgKHN3aXBlQW5nbGUgPCAwKSB7XG4gICAgICAgICAgICBzd2lwZUFuZ2xlID0gMzYwIC0gTWF0aC5hYnMoc3dpcGVBbmdsZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoKHN3aXBlQW5nbGUgPD0gNDUpICYmIChzd2lwZUFuZ2xlID49IDApKSB7XG4gICAgICAgICAgICByZXR1cm4gKF8ub3B0aW9ucy5ydGwgPT09IGZhbHNlID8gJ2xlZnQnIDogJ3JpZ2h0Jyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKChzd2lwZUFuZ2xlIDw9IDM2MCkgJiYgKHN3aXBlQW5nbGUgPj0gMzE1KSkge1xuICAgICAgICAgICAgcmV0dXJuIChfLm9wdGlvbnMucnRsID09PSBmYWxzZSA/ICdsZWZ0JyA6ICdyaWdodCcpO1xuICAgICAgICB9XG4gICAgICAgIGlmICgoc3dpcGVBbmdsZSA+PSAxMzUpICYmIChzd2lwZUFuZ2xlIDw9IDIyNSkpIHtcbiAgICAgICAgICAgIHJldHVybiAoXy5vcHRpb25zLnJ0bCA9PT0gZmFsc2UgPyAncmlnaHQnIDogJ2xlZnQnKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgaWYgKChzd2lwZUFuZ2xlID49IDM1KSAmJiAoc3dpcGVBbmdsZSA8PSAxMzUpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICdkb3duJztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICd1cCc7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gJ3ZlcnRpY2FsJztcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuc3dpcGVFbmQgPSBmdW5jdGlvbihldmVudCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIHNsaWRlQ291bnQsXG4gICAgICAgICAgICBkaXJlY3Rpb247XG5cbiAgICAgICAgXy5kcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgICBfLmludGVycnVwdGVkID0gZmFsc2U7XG4gICAgICAgIF8uc2hvdWxkQ2xpY2sgPSAoIF8udG91Y2hPYmplY3Quc3dpcGVMZW5ndGggPiAxMCApID8gZmFsc2UgOiB0cnVlO1xuXG4gICAgICAgIGlmICggXy50b3VjaE9iamVjdC5jdXJYID09PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIF8udG91Y2hPYmplY3QuZWRnZUhpdCA9PT0gdHJ1ZSApIHtcbiAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdlZGdlJywgW18sIF8uc3dpcGVEaXJlY3Rpb24oKSBdKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICggXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA+PSBfLnRvdWNoT2JqZWN0Lm1pblN3aXBlICkge1xuXG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBfLnN3aXBlRGlyZWN0aW9uKCk7XG5cbiAgICAgICAgICAgIHN3aXRjaCAoIGRpcmVjdGlvbiApIHtcblxuICAgICAgICAgICAgICAgIGNhc2UgJ2xlZnQnOlxuICAgICAgICAgICAgICAgIGNhc2UgJ2Rvd24nOlxuXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlQ291bnQgPVxuICAgICAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnN3aXBlVG9TbGlkZSA/XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5jaGVja05hdmlnYWJsZSggXy5jdXJyZW50U2xpZGUgKyBfLmdldFNsaWRlQ291bnQoKSApIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSArIF8uZ2V0U2xpZGVDb3VudCgpO1xuXG4gICAgICAgICAgICAgICAgICAgIF8uY3VycmVudERpcmVjdGlvbiA9IDA7XG5cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlICdyaWdodCc6XG4gICAgICAgICAgICAgICAgY2FzZSAndXAnOlxuXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlQ291bnQgPVxuICAgICAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnN3aXBlVG9TbGlkZSA/XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5jaGVja05hdmlnYWJsZSggXy5jdXJyZW50U2xpZGUgLSBfLmdldFNsaWRlQ291bnQoKSApIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSAtIF8uZ2V0U2xpZGVDb3VudCgpO1xuXG4gICAgICAgICAgICAgICAgICAgIF8uY3VycmVudERpcmVjdGlvbiA9IDE7XG5cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuXG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYoIGRpcmVjdGlvbiAhPSAndmVydGljYWwnICkge1xuXG4gICAgICAgICAgICAgICAgXy5zbGlkZUhhbmRsZXIoIHNsaWRlQ291bnQgKTtcbiAgICAgICAgICAgICAgICBfLnRvdWNoT2JqZWN0ID0ge307XG4gICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ3N3aXBlJywgW18sIGRpcmVjdGlvbiBdKTtcblxuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgIGlmICggXy50b3VjaE9iamVjdC5zdGFydFggIT09IF8udG91Y2hPYmplY3QuY3VyWCApIHtcblxuICAgICAgICAgICAgICAgIF8uc2xpZGVIYW5kbGVyKCBfLmN1cnJlbnRTbGlkZSApO1xuICAgICAgICAgICAgICAgIF8udG91Y2hPYmplY3QgPSB7fTtcblxuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUuc3dpcGVIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKChfLm9wdGlvbnMuc3dpcGUgPT09IGZhbHNlKSB8fCAoJ29udG91Y2hlbmQnIGluIGRvY3VtZW50ICYmIF8ub3B0aW9ucy5zd2lwZSA9PT0gZmFsc2UpKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gZWxzZSBpZiAoXy5vcHRpb25zLmRyYWdnYWJsZSA9PT0gZmFsc2UgJiYgZXZlbnQudHlwZS5pbmRleE9mKCdtb3VzZScpICE9PSAtMSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgXy50b3VjaE9iamVjdC5maW5nZXJDb3VudCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQgJiYgZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzICE9PSB1bmRlZmluZWQgP1xuICAgICAgICAgICAgZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzLmxlbmd0aCA6IDE7XG5cbiAgICAgICAgXy50b3VjaE9iamVjdC5taW5Td2lwZSA9IF8ubGlzdFdpZHRoIC8gXy5vcHRpb25zXG4gICAgICAgICAgICAudG91Y2hUaHJlc2hvbGQ7XG5cbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8udG91Y2hPYmplY3QubWluU3dpcGUgPSBfLmxpc3RIZWlnaHQgLyBfLm9wdGlvbnNcbiAgICAgICAgICAgICAgICAudG91Y2hUaHJlc2hvbGQ7XG4gICAgICAgIH1cblxuICAgICAgICBzd2l0Y2ggKGV2ZW50LmRhdGEuYWN0aW9uKSB7XG5cbiAgICAgICAgICAgIGNhc2UgJ3N0YXJ0JzpcbiAgICAgICAgICAgICAgICBfLnN3aXBlU3RhcnQoZXZlbnQpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlICdtb3ZlJzpcbiAgICAgICAgICAgICAgICBfLnN3aXBlTW92ZShldmVudCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgJ2VuZCc6XG4gICAgICAgICAgICAgICAgXy5zd2lwZUVuZChldmVudCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5zd2lwZU1vdmUgPSBmdW5jdGlvbihldmVudCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIGVkZ2VXYXNIaXQgPSBmYWxzZSxcbiAgICAgICAgICAgIGN1ckxlZnQsIHN3aXBlRGlyZWN0aW9uLCBzd2lwZUxlbmd0aCwgcG9zaXRpb25PZmZzZXQsIHRvdWNoZXM7XG5cbiAgICAgICAgdG91Y2hlcyA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQgIT09IHVuZGVmaW5lZCA/IGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlcyA6IG51bGw7XG5cbiAgICAgICAgaWYgKCFfLmRyYWdnaW5nIHx8IHRvdWNoZXMgJiYgdG91Y2hlcy5sZW5ndGggIT09IDEpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGN1ckxlZnQgPSBfLmdldExlZnQoXy5jdXJyZW50U2xpZGUpO1xuXG4gICAgICAgIF8udG91Y2hPYmplY3QuY3VyWCA9IHRvdWNoZXMgIT09IHVuZGVmaW5lZCA/IHRvdWNoZXNbMF0ucGFnZVggOiBldmVudC5jbGllbnRYO1xuICAgICAgICBfLnRvdWNoT2JqZWN0LmN1clkgPSB0b3VjaGVzICE9PSB1bmRlZmluZWQgPyB0b3VjaGVzWzBdLnBhZ2VZIDogZXZlbnQuY2xpZW50WTtcblxuICAgICAgICBfLnRvdWNoT2JqZWN0LnN3aXBlTGVuZ3RoID0gTWF0aC5yb3VuZChNYXRoLnNxcnQoXG4gICAgICAgICAgICBNYXRoLnBvdyhfLnRvdWNoT2JqZWN0LmN1clggLSBfLnRvdWNoT2JqZWN0LnN0YXJ0WCwgMikpKTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA9IE1hdGgucm91bmQoTWF0aC5zcXJ0KFxuICAgICAgICAgICAgICAgIE1hdGgucG93KF8udG91Y2hPYmplY3QuY3VyWSAtIF8udG91Y2hPYmplY3Quc3RhcnRZLCAyKSkpO1xuICAgICAgICB9XG5cbiAgICAgICAgc3dpcGVEaXJlY3Rpb24gPSBfLnN3aXBlRGlyZWN0aW9uKCk7XG5cbiAgICAgICAgaWYgKHN3aXBlRGlyZWN0aW9uID09PSAndmVydGljYWwnKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZXZlbnQub3JpZ2luYWxFdmVudCAhPT0gdW5kZWZpbmVkICYmIF8udG91Y2hPYmplY3Quc3dpcGVMZW5ndGggPiA0KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgcG9zaXRpb25PZmZzZXQgPSAoXy5vcHRpb25zLnJ0bCA9PT0gZmFsc2UgPyAxIDogLTEpICogKF8udG91Y2hPYmplY3QuY3VyWCA+IF8udG91Y2hPYmplY3Quc3RhcnRYID8gMSA6IC0xKTtcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcbiAgICAgICAgICAgIHBvc2l0aW9uT2Zmc2V0ID0gXy50b3VjaE9iamVjdC5jdXJZID4gXy50b3VjaE9iamVjdC5zdGFydFkgPyAxIDogLTE7XG4gICAgICAgIH1cblxuXG4gICAgICAgIHN3aXBlTGVuZ3RoID0gXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aDtcblxuICAgICAgICBfLnRvdWNoT2JqZWN0LmVkZ2VIaXQgPSBmYWxzZTtcblxuICAgICAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgaWYgKChfLmN1cnJlbnRTbGlkZSA9PT0gMCAmJiBzd2lwZURpcmVjdGlvbiA9PT0gJ3JpZ2h0JykgfHwgKF8uY3VycmVudFNsaWRlID49IF8uZ2V0RG90Q291bnQoKSAmJiBzd2lwZURpcmVjdGlvbiA9PT0gJ2xlZnQnKSkge1xuICAgICAgICAgICAgICAgIHN3aXBlTGVuZ3RoID0gXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCAqIF8ub3B0aW9ucy5lZGdlRnJpY3Rpb247XG4gICAgICAgICAgICAgICAgXy50b3VjaE9iamVjdC5lZGdlSGl0ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICBfLnN3aXBlTGVmdCA9IGN1ckxlZnQgKyBzd2lwZUxlbmd0aCAqIHBvc2l0aW9uT2Zmc2V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXy5zd2lwZUxlZnQgPSBjdXJMZWZ0ICsgKHN3aXBlTGVuZ3RoICogKF8uJGxpc3QuaGVpZ2h0KCkgLyBfLmxpc3RXaWR0aCkpICogcG9zaXRpb25PZmZzZXQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcbiAgICAgICAgICAgIF8uc3dpcGVMZWZ0ID0gY3VyTGVmdCArIHN3aXBlTGVuZ3RoICogcG9zaXRpb25PZmZzZXQ7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IHRydWUgfHwgXy5vcHRpb25zLnRvdWNoTW92ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLmFuaW1hdGluZyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgXy5zd2lwZUxlZnQgPSBudWxsO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgXy5zZXRDU1MoXy5zd2lwZUxlZnQpO1xuXG4gICAgfTtcblxuICAgIFNsaWNrLnByb3RvdHlwZS5zd2lwZVN0YXJ0ID0gZnVuY3Rpb24oZXZlbnQpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICB0b3VjaGVzO1xuXG4gICAgICAgIF8uaW50ZXJydXB0ZWQgPSB0cnVlO1xuXG4gICAgICAgIGlmIChfLnRvdWNoT2JqZWN0LmZpbmdlckNvdW50ICE9PSAxIHx8IF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XG4gICAgICAgICAgICBfLnRvdWNoT2JqZWN0ID0ge307XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZXZlbnQub3JpZ2luYWxFdmVudCAhPT0gdW5kZWZpbmVkICYmIGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlcyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0b3VjaGVzID0gZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdO1xuICAgICAgICB9XG5cbiAgICAgICAgXy50b3VjaE9iamVjdC5zdGFydFggPSBfLnRvdWNoT2JqZWN0LmN1clggPSB0b3VjaGVzICE9PSB1bmRlZmluZWQgPyB0b3VjaGVzLnBhZ2VYIDogZXZlbnQuY2xpZW50WDtcbiAgICAgICAgXy50b3VjaE9iamVjdC5zdGFydFkgPSBfLnRvdWNoT2JqZWN0LmN1clkgPSB0b3VjaGVzICE9PSB1bmRlZmluZWQgPyB0b3VjaGVzLnBhZ2VZIDogZXZlbnQuY2xpZW50WTtcblxuICAgICAgICBfLmRyYWdnaW5nID0gdHJ1ZTtcblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUudW5maWx0ZXJTbGlkZXMgPSBTbGljay5wcm90b3R5cGUuc2xpY2tVbmZpbHRlciA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAoXy4kc2xpZGVzQ2FjaGUgIT09IG51bGwpIHtcblxuICAgICAgICAgICAgXy51bmxvYWQoKTtcblxuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xuXG4gICAgICAgICAgICBfLiRzbGlkZXNDYWNoZS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcblxuICAgICAgICAgICAgXy5yZWluaXQoKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnVubG9hZCA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICAkKCcuc2xpY2stY2xvbmVkJywgXy4kc2xpZGVyKS5yZW1vdmUoKTtcblxuICAgICAgICBpZiAoXy4kZG90cykge1xuICAgICAgICAgICAgXy4kZG90cy5yZW1vdmUoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLiRwcmV2QXJyb3cgJiYgXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5wcmV2QXJyb3cpKSB7XG4gICAgICAgICAgICBfLiRwcmV2QXJyb3cucmVtb3ZlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXy4kbmV4dEFycm93ICYmIF8uaHRtbEV4cHIudGVzdChfLm9wdGlvbnMubmV4dEFycm93KSkge1xuICAgICAgICAgICAgXy4kbmV4dEFycm93LnJlbW92ZSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgXy4kc2xpZGVzXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLXNsaWRlIHNsaWNrLWFjdGl2ZSBzbGljay12aXNpYmxlIHNsaWNrLWN1cnJlbnQnKVxuICAgICAgICAgICAgLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKVxuICAgICAgICAgICAgLmNzcygnd2lkdGgnLCAnJyk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnVuc2xpY2sgPSBmdW5jdGlvbihmcm9tQnJlYWtwb2ludCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcbiAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ3Vuc2xpY2snLCBbXywgZnJvbUJyZWFrcG9pbnRdKTtcbiAgICAgICAgXy5kZXN0cm95KCk7XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnVwZGF0ZUFycm93cyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcyxcbiAgICAgICAgICAgIGNlbnRlck9mZnNldDtcblxuICAgICAgICBjZW50ZXJPZmZzZXQgPSBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKTtcblxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5hcnJvd3MgPT09IHRydWUgJiZcbiAgICAgICAgICAgIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgJiZcbiAgICAgICAgICAgICFfLm9wdGlvbnMuaW5maW5pdGUgKSB7XG5cbiAgICAgICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ2ZhbHNlJyk7XG4gICAgICAgICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICdmYWxzZScpO1xuXG4gICAgICAgICAgICBpZiAoXy5jdXJyZW50U2xpZGUgPT09IDApIHtcblxuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcbiAgICAgICAgICAgICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICdmYWxzZScpO1xuXG4gICAgICAgICAgICB9IGVsc2UgaWYgKF8uY3VycmVudFNsaWRlID49IF8uc2xpZGVDb3VudCAtIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgJiYgXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IGZhbHNlKSB7XG5cbiAgICAgICAgICAgICAgICBfLiRuZXh0QXJyb3cuYWRkQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICd0cnVlJyk7XG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93LnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAnZmFsc2UnKTtcblxuICAgICAgICAgICAgfSBlbHNlIGlmIChfLmN1cnJlbnRTbGlkZSA+PSBfLnNsaWRlQ291bnQgLSAxICYmIF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG5cbiAgICAgICAgICAgICAgICBfLiRuZXh0QXJyb3cuYWRkQ2xhc3MoJ3NsaWNrLWRpc2FibGVkJykuYXR0cignYXJpYS1kaXNhYmxlZCcsICd0cnVlJyk7XG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93LnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAnZmFsc2UnKTtcblxuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1cblxuICAgIH07XG5cbiAgICBTbGljay5wcm90b3R5cGUudXBkYXRlRG90cyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHZhciBfID0gdGhpcztcblxuICAgICAgICBpZiAoXy4kZG90cyAhPT0gbnVsbCkge1xuXG4gICAgICAgICAgICBfLiRkb3RzXG4gICAgICAgICAgICAgICAgLmZpbmQoJ2xpJylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpXG4gICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKTtcblxuICAgICAgICAgICAgXy4kZG90c1xuICAgICAgICAgICAgICAgIC5maW5kKCdsaScpXG4gICAgICAgICAgICAgICAgLmVxKE1hdGguZmxvb3IoXy5jdXJyZW50U2xpZGUgLyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwpKVxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stYWN0aXZlJylcbiAgICAgICAgICAgICAgICAuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgU2xpY2sucHJvdG90eXBlLnZpc2liaWxpdHkgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgXyA9IHRoaXM7XG5cbiAgICAgICAgaWYgKCBfLm9wdGlvbnMuYXV0b3BsYXkgKSB7XG5cbiAgICAgICAgICAgIGlmICggZG9jdW1lbnRbXy5oaWRkZW5dICkge1xuXG4gICAgICAgICAgICAgICAgXy5pbnRlcnJ1cHRlZCA9IHRydWU7XG5cbiAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICBfLmludGVycnVwdGVkID0gZmFsc2U7XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgJC5mbi5zbGljayA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgXyA9IHRoaXMsXG4gICAgICAgICAgICBvcHQgPSBhcmd1bWVudHNbMF0sXG4gICAgICAgICAgICBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcbiAgICAgICAgICAgIGwgPSBfLmxlbmd0aCxcbiAgICAgICAgICAgIGksXG4gICAgICAgICAgICByZXQ7XG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICAgIGlmICh0eXBlb2Ygb3B0ID09ICdvYmplY3QnIHx8IHR5cGVvZiBvcHQgPT0gJ3VuZGVmaW5lZCcpXG4gICAgICAgICAgICAgICAgX1tpXS5zbGljayA9IG5ldyBTbGljayhfW2ldLCBvcHQpO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHJldCA9IF9baV0uc2xpY2tbb3B0XS5hcHBseShfW2ldLnNsaWNrLCBhcmdzKTtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgcmV0ICE9ICd1bmRlZmluZWQnKSByZXR1cm4gcmV0O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBfO1xuICAgIH07XG5cbn0pKTtcbiIsIi8vIE1lZXQgU2xpZGVyXG4oZnVuY3Rpb24oJCkge1xuICBpZiAoJChcIi5mcm9udC1wYWdlXCIpLmxlbmd0aCA8IDEpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBjb25zdCAkd3JhcCA9ICQoXCIubWVldFwiKTtcbiAgY29uc3QgJHNsaWRlciA9ICR3cmFwLmZpbmQoXCIubWVldC1zbGlkZXJcIik7XG5cbiAgY29uc3QgX3ByZXYgPSBmdW5jdGlvbigpIHtcbiAgICAkc2xpZGVyLnNsaWNrKFwic2xpY2tQcmV2XCIpO1xuICB9O1xuXG4gIGNvbnN0IF9uZXh0ID0gZnVuY3Rpb24oKSB7XG4gICAgJHNsaWRlci5zbGljayhcInNsaWNrTmV4dFwiKTtcbiAgfTtcblxuICAkd3JhcC5vbihcImNsaWNrXCIsIFwiLmFycm93LWxlZnRcIiwgX3ByZXYpO1xuICAkd3JhcC5vbihcImNsaWNrXCIsIFwiLmFycm93LXJpZ2h0XCIsIF9uZXh0KTtcbn0pKGpRdWVyeSk7XG5cbi8vIEltcG9ydGFudCBRdWVzdGlvbnNcbihmdW5jdGlvbigkKSB7XG4gIGlmICgkKFwiLmZyb250LXBhZ2VcIikubGVuZ3RoIDwgMSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGNvbnN0ICR3cmFwID0gJChcIi50b2dnbGVzXCIpO1xuXG4gIGNvbnN0IF90b2dnbGUgPSBmdW5jdGlvbigpIHtcbiAgICAkKHRoaXMpXG4gICAgICAucGFyZW50KClcbiAgICAgIC50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgfTtcblxuICAkd3JhcC5vbihcImNsaWNrXCIsIFwiaDRcIiwgX3RvZ2dsZSk7XG59KShqUXVlcnkpO1xuIl19