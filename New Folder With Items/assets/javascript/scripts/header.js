(function($) {
  const _toggleHeaderForm = function() {
    $(".header-form").toggleClass("active");
  };

  $(".notice").on("click", _toggleHeaderForm);
})(jQuery);
