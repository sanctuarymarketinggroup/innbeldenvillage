<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 *
 * Styles Format
 * wp_enqueue_style( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, string $media = 'all' );
 * $handle = required, $src = optional, $deps = optional, $ver = optional, $media = optional
 *
 * Scripts Format
 * wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false );
 * $handle = required, $src = optional, $deps = optional, $ver = optional, $in_footer = optional
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

if ( ! function_exists( 'smg_scripts' ) ) {
	function smg_scripts() {

		// Enqueue the main Stylesheet.
		wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/assets/dist/app.css', array(), '1.0.0', 'all' );

		// Google Fonts
		$fonts = 'Crimson+Text:400,400i,600,600i,700,700i|Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i';
		wp_enqueue_style( 'google_fonts', '//fonts.googleapis.com/css?family='.$fonts.'&display=swap', array(), null );
		
		// Font Awesome (Icons)
		// wp_enqueue_style( 'font-awesome-5' );
		
		// Stylesheet

		// Enqueue jQuery
		wp_enqueue_script( 'jquery' ); 

		// Enqueue the main Scripts 
		wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/dist/app.js', array(), '1.0.0', true ); 
		
	}
	add_action( 'wp_enqueue_scripts', 'smg_scripts' );
}

if( ! function_exists('smg_footer_scripts') ) {
	function smg_footer_scripts() {
		if (current_user_can('administrator') && is_user_logged_in() ) {
			/** Uncomment if you want the Wordpress bar to be in the footer */
			wp_enqueue_style('loggedin-css', get_stylesheet_directory_uri() . '/assets/logged-in.css', array(), '1.0', 'all');
		}
	}
	add_action('wp_footer', 'smg_footer_scripts');
}
