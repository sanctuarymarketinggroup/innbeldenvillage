<?php
/**
 * Register theme support for languages, menus, post-thumbnails, post-formats etc.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

/**
 * Add Theme Support for Wordpress Settings
 */
if ( ! function_exists( 'smg_theme_support' ) ) {
  function smg_theme_support() {
    // Add language support
    load_theme_textdomain( 'smg', get_template_directory() . '/languages' );

    // Switch default core markup for search form, comment form, and comments to output valid HTML5
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Add menu support
    add_theme_support( 'menus' );

    // Let WordPress manage the document title
    add_theme_support( 'title-tag' );

    // Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
    add_theme_support( 'post-thumbnails', array( 'post', 'page', 'featured_project' ) );

    // RSS thingy
    add_theme_support( 'automatic-feed-links' );

    // Add post formats support: http://codex.wordpress.org/Post_Formats
    add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );

    // Declare WooCommerce support per http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
    add_theme_support( 'woocommerce' );

    // Add app.css as editor style https://codex.wordpress.org/Editor_Style
    add_editor_style( 'assets/stylesheets/app.css' );
  }
  add_action( 'after_setup_theme', 'smg_theme_support' );
}


/**
 * Add Theme Support for ACF Options Page
 * @title Theme Settings
 */
if ( function_exists('acf_add_options_page') ) { 
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
  ));
  // add sub page 
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Interactive Graphic',
		'menu_title' 	=> 'Interactive Graphic',
		'parent_slug' 	=> $parent['menu_slug'],
	));
}

/**
 * Add Theme Support for Yoast Breadcrumbs
 * @before String
 * @after String
 */
if ( function_exists('yoast_breadcrumb') ) {
  function smg_breadcrumbs( $before = '', $after = '' ) {
    yoast_breadcrumb( $before, $after );
  }
}

/**
 * Add Theme Support for Beaver Builder Templates
 * @id Integer|String
 */
if ( class_exists('FLBuilder') ) {
  function smg_beaver_template($id) {
    FLBuilder::render_query( array(
      'post_type' => 'fl-builder-template',
      'p'         => $id
    ) );
  }
}

/**
 * Add Theme Support for Shortcodes of All Template Parts
 * Auto-Creates Shortcodes = [cp-sample-inc] (It must have cp prefix)
 */
class Shortcode {

  private $file = '';

  public function __construct($var) {
    $this->file = $this->file($var);
    add_shortcode($this->file, array($this, 'shortcode'));
  }

  public function file($file) {
    return trim( explode('.', $file)[0] );
  }

  public function shortcode( $args = array(), $content = '' ) {
    $args = $this->set_args($args);
    ob_start();
    echo '<section class="cp container ' . $this->file . ' ' . $args['section'] . '" style="' . $args['sStyle'] . '">';
      echo '<div class="row ' . $args['row'] . '" style="' . $args['rStyle'] . '">';
        echo show_template($this->file, $args);
      echo '</div>';
    echo '</section>';
    return ob_get_clean();
  }

  private function set_args($args) {
    if (!isset($args) || !is_array($args)) {
      $args = array();
    }
    if (!isset($args['sStyle'])) {
      $args['sStyle'] = '';
    }
    if (!isset($args['rStyle'])) {
      $args['rStyle'] = '';
    }
    if (isset($content) && !empty($content)) {
      $args['shortcode_content'] = $content;
    }
    if (!isset($args['section'])) {
      $args['section'] = 'default';
    }
    if (!isset($args['row'])) {
      $args['row'] = 'default';
    }
    if (isset($args['sBg'])) {
      $args['sStyle'] .= 'background:' . $args['sBg'] . ';';
    }
    if (isset($args['rBg'])) {
      $args['rStyle'] .= 'background:' . $args['rBg'] . ';';
    }
    return $args;
  }
}


/**
 * smg_inc_shortcodes
 * 
 * Automatically create shortcode for all items in inc folder starting with cp.
 * 
 * @since 1.0.0
 */
if( ! function_exists('smg_inc_shortcodes') ){
  function smg_inc_shortcodes() {
    $dir = scandir(SMG_THEME . '/inc/');
    foreach($dir as $file) {
      if (substr( $file, 0, 3 ) === 'cp-') {
        $foo = new Shortcode($file);
      }
    }
  }
  add_action('init', 'smg_inc_shortcodes');
}


/**
 * mondial_filter_states
 * 
 * Remove unneeded states from Mondial
 * 
 * @since 1.0.0
 */
if( ! function_exists('mondial_filter_states') ){
  function mondial_filter_states( $states ) {
    $excluded_states = array('AK', 'HI');
    foreach($excluded_states as $no_state){
      unset($states['US'][$no_state]);
    }
    return $states;
  }
  add_filter( 'woocommerce_states', 'mondial_filter_states' );
}