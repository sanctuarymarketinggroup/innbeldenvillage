<?php

/**
 * Get Main Settings of the Site
 */
function get_setting($pri, $sec = null, $tir = null){
  $vars = array(
    'sitename' => get_bloginfo('name'),
    'header-logo' => array(
      'url' => THEME_ROOT . '/assets/images/svg/logo.svg',
      'width' => '',
      'height' => '',
      'alt' => get_bloginfo('name'),
      'title' => get_bloginfo('name') . ' Logo',
    ),
    'address' => array(
      'street' => '000 Sanctuary St.',
      'city' => 'North Canton',
      'state' => 'OH',
      'zipcode' => '44720',
      'full' => '2020 Case Parkway, Twinsburg, OH 44087'
    ),
    'phone' => array(
      'primary' => array(
        'text' => '(330) 888-8888',
        'link' => 'tel:+1-330-405-5587'
      ),
      'secondary' => array(
        'text' => '(330) 111-1111',
        'link' => 'tel:+1-330-405-5587'
      )
    ),
    'fax' => '330-222-2222'
  );
  if ($tir) {
    return $vars[$pri][$sec][$tir];
  }
  if ($sec) {
    return $vars[$pri][$sec];
  }
  if ($pri) {
    return $vars[$pri];
  }
  return;
}

/**
 * Get Full Address
 */
function get_full_address() {
  $address = get_setting('address');
  $street = $address['street'];
  $city = $address['city'];
  $state = $address['state'];
  $zip = $address['zip'];
  return "$street, $city, $state $zip";
}

/**
 * Enable Gravity Forms Hidden Titles
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );