<?php

/**
 * Defines for Modules
 */
define('SMG_MOD_DIR', get_template_directory() . '/modules');
define('SMG_MOD_ROOT', get_stylesheet_directory_uri() . '/modules');

/**
 * Require Utiility Function
 */
require_once('util.php');