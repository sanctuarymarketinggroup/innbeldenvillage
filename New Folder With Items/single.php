<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

$class = 'default';
if ( FLBuilderModel::is_builder_enabled() ) {
	$class = 'full-width';
}

get_header(); 
//TO DO add in stuff from Theme options
?>
<script type="application/ld+json">
{ 
	"@context": "http://schema.org", 
	"@type": "BlogPosting",
	"headline": "<?php echo get_the_title(); ?>",
	"name": "<?php echo get_the_title(); ?>",
	<?php if( has_post_thumbnail() ) { ?>
	"image": "<?php echo get_the_post_thumbnail_url(); ?>",
	<?php } ?>", 
	"keywords": "<?php echo get_the_category(', '); ?>", 
	"articleSection": "<?php echo get_the_category(', '); ?>", 
	"wordcount": "<?php echo word_count();?>",
	"url": "<?php echo get_permalink(); ?>",
	"mainEntityOfPage": "<?php echo get_permalink(); ?>",
	"datePublished": "<?php echo get_the_date(); ?>",
	"dateCreated": "<?php echo get_the_date(); ?>",
	"isFamilyFriendly": "Yes",
	"dateModified": "<?php echo get_the_modified_date('F d, Y');?>",
	"description": "<?php echo get_the_excerpt();?>",
	"articleBody": "<?php echo get_the_content(); ?>",
  "author": {
    "@type": "Person",
    "name": "<?php get_the_author(); ?>"
  }
 }
</script>

<div id="post" class="template <?php echo $class . '-template'; ?>" role="main">
	<?php while ( have_posts() ) { the_post(); ?>
		<article class="entry" id="post-<?php the_ID(); ?>">
			<header class="page-header entry-header">
				<?php echo show_template('components/page-title', array('class' => 'entry-title')); ?>
				<?php smg_entry_meta(); ?>
			</header>
			<?php echo show_template('components/entry-image'); ?>
			<main class="page-body entry-body">
				<?php do_action('before_entry_body'); ?>
				<?php echo show_template('components/page-content', array('class' => 'entry-content')); ?>
				<?php do_action('after_entry_body'); ?>
			</main>
			<footer class="page-footer entry-footer">
				<?php // if(is_single()) comments_template(); ?>
			</footer>
		</article>
		<aside class="sidebar">
			<?php get_sidebar(); ?>
		</aside>
	<?php } ?>
</div>

<?php get_footer();