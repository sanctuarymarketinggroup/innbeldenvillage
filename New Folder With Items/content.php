<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>

<article class="entry" id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<?php echo show_template('components/entry-title'); ?>
	</header>
	<?php echo show_template('components/entry-image'); ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading...', 'smg' ) ); ?>
		<?php echo show_template('components/entry-link'); ?>
	</div>
</article>

