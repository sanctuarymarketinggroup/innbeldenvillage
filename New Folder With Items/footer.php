<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>
		
		<?php show_template('st-footer'); ?>
		<?php wp_footer(); ?>
	</body>
</html>
