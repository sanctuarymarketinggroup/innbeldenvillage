<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

$class = 'default';


get_header(); ?>

<div id="index" class="template <?php echo $class . '-template'; ?>" role="main">
	<div class="archive">
		<header class="page-header">
			<?php echo show_template('components/page-title', array('title' => get_the_title(get_option( 'page_for_posts' )))); ?>
		</header>
		<main class="page-entries">
			<?php
				if( have_posts() ) {
					while ( have_posts() ) { the_post();
						get_template_part( 'content', get_post_format() );
					}
					smg_pagination();
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
		</main>
	</div>
	<aside class="sidebar">
		<?php get_sidebar(); ?>
	</aside>
</div>

<?php get_footer(); ?>
