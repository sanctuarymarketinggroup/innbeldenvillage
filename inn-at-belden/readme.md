# The Inn at Belden Village Theme

## Installation

In this theme there are 2 zip files. "SMG-MU" is a must-use plugin. Please install this in your wordpress development enviroment. The second (SMG-SPEED) is a custom speed optimization plugin, please install as normal plugin in your development environment, and instructions on how to use/configure are below.

## How to compile

1. Download and install current version of nodeJS (https://nodejs.org/en/)
2. Once downloaded change to current directory
3. Open terminal and run "npm install"
4. Next type in terminal "npm run watch"
5. SCSS and JS is starting to compile

## Notes

- Basic page outlines have been done for you, feel free to change up div's but please keep our current structure of our theme intact.
- If you want to add more scss files, please include them in "/assets/scss/app.scss"
- All css and js after compiling has been automatically pulled into the theme via enqueue_scripts, no need to re-enqueue.
- Homepage doesn't need to be editable by the client (leave hardcoded) or then the sections specifically labelled "dynamic" or called out in the design.
- Put scss in the files that make the most sense, we aren't super particular about this, but it helps to keep the whole project organized

## Using smg-speed plugin

- For images, please use the function "get_img_lazy( URL TO IMAGE );" This builds the image html to allow them to be lazy loaded.
  - EXAMPLE get_img_lazy( "http://www.example.com/example.jpg" ) OR get_img_lazy( get_the_post_thumbnail_url(get_the_ID(),'full') )
- For background images, please add in a class="lazyload" to the div. Then add the attribute data-bg="URL TO IMAGE" to the div.
  - EXAMPLE <div class="lazyload" data-bg="http://www.example.com/example.jpg"></div>
- For svgs, please use the following function "get_img_lazy( URL TO IMAGE )"
  - EXAMPLE: get_img_lazy("http://www.example.com/example.svg")

## File Structure

- Custom Loop: Please use the WP_QUERY class
- Custom Shortcodes: /library/shortcodes.php
- Enqueue Scripts: /library/enqueue-scripts.php
- Entry Meta: /library/entry-meta.php
- Google Fonts: These should already be installed, please check /assets/scss/global/settings.scss for the variable names. ($font, $font-alt)
- Gravity Forms: Please rebuild and place where marked in template files
- SCSS Media Queries: You can use mixins -
  - @include breakpoint(small down){}
  - @include breakpoint(small only){}
  - @include breakpoint(small up){}
  - @include breakpoint(medium down){}
  - @include breakpoint(medium only){}
  - @include breakpoint(medium up){}
  - @include breakpoint(large down){}
  - @include breakpoint(large only){}
  - @include breakpoint(large up){}
- SCSS Mixins: /assets/scss/global/mixings.scss
- SCSS Variables: /assets/scss/global/settings.scss
- Template Part: /inc (please use the **show_template()** function to pull in file contents)
