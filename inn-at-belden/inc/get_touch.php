<section class="get-in-touch">
    <div class="row">
        <h2>
            <i><?php echo get_svg( 'chat' ); ?></i>
            Get In Touch with Us
        </h2>
        <div class="form-footer-wrap">
            <div class="get_touch_btn">
                <p>
                    Contact Us
                </p>
            </div>
            <?php echo do_shortcode( '[gravityform id=3 title=false description=false ajax=true]' ) ?>
        </div>
    </div>
</section>