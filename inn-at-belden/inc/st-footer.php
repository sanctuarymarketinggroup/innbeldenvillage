<?php //$logo = get_setting('header-logo'); ?>
<?php // $logo = acf_image( $settings['header-logo'] ); ?>
<?php $logo = get_svg( 'logo' ); ?>
<?php //$logo = get_setting( 'header-logo' ); ?>
<?php $sitename = get_setting( 'sitename' ); ?>
<?php $address = get_setting( 'address' ); ?>
<?php $phone = get_setting( 'phone' ); ?>

<footer id="footer" class="container lazyload">
    <div class="row row-bg">
        <div class="footer-fl">
            <div class="logo">
                <a href="<?php echo HOME_URL; ?>"><?php echo $logo; ?></a>
            </div>
            <div class="info">
                <h4><?php echo $sitename; ?></h4>
                <div class="address">
                    <div class="address_wrap">
                        <p><?php echo $address['street']; ?><br/>
                            <?php echo $address['city']; ?>
                            , <?php echo $address['state']; ?> <?php echo $address['zipcode']; ?><br/>
                            Ph:
                            <a href="<?php echo $phone['primary']['link']; ?>"><?php echo $phone['primary']['text']; ?></a><br/>
                    </div>
                </div>
            </div>
            <nav class="footer-menu">
                <?php smg_footer_menu(); ?>
            </nav>

            <nav class="footer-menu fm-2">
                <?php smg_footer_menu_2(); ?>
            </nav>
        </div>
    </div>
</footer>

<div class="copyright">
    <div class="row">
        <?php $fb_link = get_setting( 'facebook' ) ?>
        <div class="social">
            <a target="_blank" href="<?php echo $fb_link['url'] ? : '#'; ?>" class="facebook-wrap">
                <?php echo get_svg( 'fb' ); ?>
            </a>
        </div>
        <p>&copy;<?php echo date( 'Y' ); ?> <?php echo $sitename; ?> - All Right Reserved</p>
    </div>
</div>