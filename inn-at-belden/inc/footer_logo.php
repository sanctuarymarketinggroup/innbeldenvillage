<?php
$logos_footer = get_setting( 'footer-logos' );
?>

<?php if ( $logos_footer ): ?>
    <section class="associatations">
        <div class="row">
            <?php foreach ( $logos_footer as $footer_logo ) : ?>
                <div class="logo">
                    <a target="_blank" href="<?php echo $footer_logo['link']; ?>">
                        <?php echo get_img_lazy( get_theme_img_url( $footer_logo['url'] ) ); ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
