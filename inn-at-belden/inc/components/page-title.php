<?php
/**
 * @var $title
 */
// $class = (isset($class) && !empty($class)) ? array_merge(array('page-title'), $class) : array('page-title'); ?>
<h1 class="<?php echo get_incclass($class, 'page-title'); ?>">
  <?php
  if (isset($title)) {
    echo $title;
  } elseif (isset($customid)) {
    echo get_the_title( $customid );
  } else {
    the_title(); 
  }
  ?>
</h1>