<?php $class = (isset($class) && !empty($class)) ? $class : 'btn-primary'; ?>

<a href="<?php echo get_permalink(); ?>" class="btn <?php echo $class; ?>">
  <?php
    if (isset($text) && ! empty($text)) {
      echo $text;
    } else {
      echo 'Read More';
    }
  ?>
</a>