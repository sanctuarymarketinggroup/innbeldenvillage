<?php $settings = get_setting( 'header-logo' ); ?>
<?php // $logo = acf_image( $settings['header-logo'] ); ?>
<?php //$logo = get_svg( $settings['header-logo'] ); ?>
<?php //$logo = get_bloginfo( 'name' ); ?>
<?php $phone = get_setting( 'phone' ); ?>

<div class="sticky-header mobile-fixed">

<!--    <header id="header" class="container lazyload"-->
<!--            data-bg="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/fancy-bg.png">-->

    <header id="header" class="container">
        <div class="row">

            <div id="mobile-menu">
                <span>MENU</span>
                <div class="bars">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
            </div>

            <div class="logo">
                <a href="<?php echo HOME_URL; ?>">
                    <?php //echo get_svg( 'logo-new' ); ?>
                    <?php echo get_img( get_theme_img_url( 'logo.png' ) ); ?>
                </a>
            </div>
            <div class="notice">
                <?php echo get_img( get_theme_img_url( 'header-button-sprite.png' ) ); ?>
                <i class="icon">
                    <?php echo get_svg( 'clock' ); ?>
                </i>
                <p>Come Tour and Enjoy Lunch on us!</p>
            </div>
            <div class="info">
                <div class="phone"> <span class="phone-text">Call Us:</span> <a class="phone-link"
                                               href="<?php echo $phone['primary']['link']; ?>"><?php echo $phone['primary']['text']; ?></a>
                </div>
                <div class="actions">
                    <?php $fb = get_setting('facebook') ?>
                    <a target="_blank" href="<?php echo $fb['url']; ?>" class="facebook">
                        <?php echo get_svg( 'fb' ); ?>
                    </a>
                    <div class="directions">
                        <?php $google_map = get_setting('google_map') ?>

                        <a target="_blank" href="<?php echo $google_map; ?>" class="icon">
                            <?php echo get_svg( 'location' ); ?>
                            <span class="direction">Directions</span>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </header>
</div>


<div class="mainmenu">

    <div class="row">
        <?php echo smg_menu(); ?>
    </div>
</div>

<div class="header-form">
    <div class="row">
        <h3>Schedule a Personal Tour:</h3>
        <div class="form-wrapper-top">
            <?php echo do_shortcode( '[gravityform id=1 title=false description=false ajax=true tabindex=70]
' ) ?></div>
<!--        <div class="close-form">-->
<!--            --><?php //echo get_svg( 'close-form' ); ?>
<!--        </div>-->
    </div>
</div>