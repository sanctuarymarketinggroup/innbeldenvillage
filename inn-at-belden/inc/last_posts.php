<div class="articles">
    <?php
    $articles = new WP_Query( array(
        'post_type'      => 'post',
        'showposts'      => 3,
        'posts_per_page' => 3
    ) );
    ?>
    <div class="row lazyload"
         data-bg="<?php echo get_template_directory_uri(); ?>/assets/images/svg/blog-bg.svg"
    >
        <h2>What's going on at The Inn?</h2> 
        <div class="entries"> 
            <?php
            if ( $articles->have_posts() ) {
                while ( $articles->have_posts() ) {
                    $articles->the_post(); ?>
                    <div class="entry"> 
                        <a href="<?php echo get_permalink( get_the_id() ); ?>" class="img">
                            <img class="lazyload" data-src="<?php echo get_the_post_thumbnail_url( get_the_id(), 'get-in-touch' ); ?>" alt="<?php echo  get_the_title(); ?>">
                        </a>
                        <h3>
                            <a href="<?php echo get_permalink( get_the_id() ); ?>">
                                <?php
                                    $title = get_the_title();

                                echo checkTitleSize($title);
                                ?>

                            </a>
                        </h3>

                        <a href="<?php echo get_permalink( get_the_id() ); ?>" class="readmore">Read
                            More</a>
                    </div>
                <?php }
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>