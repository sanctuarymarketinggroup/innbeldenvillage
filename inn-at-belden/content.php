<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>

<article class="entry" id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<h2 class="entry-title"><?php the_title(); ?></h2>
	</header>
	<?php $featured = get_featured_image(); ?>
	<?php if( $featured !== null ){ ?> 
		<img class="entry-image lazyload" data-src="<?php echo $featured; ?>" />
	<?php } ?> 
	<div class="entry-content">
		<?php // the_content( __( 'Continue reading...', 'smg' ) ); ?>
		<?php echo apply_filters( 'the_excerpt', get_the_excerpt() ); ?>
		<?php echo show_template('components/entry-link'); ?>
	</div>
</article>

