<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header(); ?>

<div id="not-found-page" class="template default-template" role="main">
	<article class="entry" id="post-not-found">
		<header class="page-header entry-header">
			<?php echo show_template('components/page-title', array('class' => 'entry-title', 'title' => 'You earned the 404 page award!')); ?>
		</header>
		<main class="page-body entry-body">
			<?php do_action('before_entry_body'); ?>
			<?php echo show_template('components/page-content', array(
				'class' => 'entry-content',
				'customContent' => '<p>No worries you probably mistyped something, looked for something that does not exist anymore or just downright broke stuffs - just carry on with the menu at the top!</p><a class="btn" href="/">Go Home</a>')); ?>
			<?php do_action('after_entry_body'); ?>
		</main>
		<footer class="page-footer entry-footer"></footer>
	</article>
</div>

<?php get_footer();
