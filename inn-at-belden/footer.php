<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>
		<?php show_template('last_posts'); ?>
        <?php show_template('footer_logo'); ?>
        <?php show_template('get_touch'); ?>
		<?php show_template('st-footer'); ?>
		<?php wp_footer(); ?>
		
		<!-- Privacy policy at http://tag.brandcdn.com/privacy -->
		<script type="text/javascript" src="//tag.brandcdn.com/autoscript/innatbeldenvillage_vg1wuk0wnvvtvda9/inn_at_belden_village.js"></script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    ' https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '236998861703764');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src=" https://www.facebook.com/tr?id=236998861703764&ev=PageView&noscript=1 "
    /></noscript>
    <!-- End Facebook Pixel Code -->
		
	</body>
</html>