<?php
/**
 * The template for displaying the front page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 */
//gravity_form_enqueue_scripts( 2, true );
get_header(); ?>

    <main id="front-page" class="front-page container" role="main">

        <div class="front-hero lazyload" data-bg="<?php echo wp_get_attachment_url(3241); ?>">
            <?php if(!wp_is_mobile()){ ?>
                <div class="video-wrapper">
                    <video autoplay="autoplay" muted="muted" loop="loop"  plays-inline class="video-home"> 
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/video/home_video.mp4" type="video/mp4">  
                    </video>     
                </div>
            <?php } ?>
            <div class="row-wrapper">
                <div class="row">
                    <h1>Celebrate your life in style.</h1>
                    <p>Serving residents as if we’re caring for our own mothers and fathers.</p>
                </div>
            </div>

            <a class="arrow-down" href="#tour">
                <?php echo get_svg( 'arrow-down2' ); ?>
            </a>
        </div>

        <div id="tour"></div>

        <section class="tour">
            <div class="wrapp-arrow">
                <div class="row">
                    <h2>
                        <i><?php echo get_svg( 'chat' ); ?></i>
                        Schedule a Personal Tour
                    </h2>
                    <div class="middle-form-wrapper">
                    <?php echo do_shortcode( '[gravityform id=2 title=false description=false ajax=true]' ) ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="discover">
            <div class="row full">
                <div class="left-block half lazyload"
                     data-bg="<?php echo get_template_directory_uri(); ?>/assets/images/svg/fancy-bg-sec.svg">
                    <div class="inner half-row">
                        <h2>Discover the Difference</h2>
                        <p>Entering The Inn at Belden Village, you’ll see and feel the difference — nurturing, love,
                            caring, family and faith.
                            <br>
                            See the transformation in your loved one.</p>
                        <ul>
                            <li>Nurse on duty 24 hours a day</li>
                            <li>Fun-loving caregivers help with everyday needs</li>
                            <li>Activities offered throughout the day</li>
                            <li>Chauffeured transportation to doctors, shopping, dining and recreation</li>
                            <li>Private suites and areas to visit that feel like home</li>
                            <li>Caring staff provides meals, housekeeping, laundry and personal care</li>
                            <li>Amenities include a beauty shop, spa, library, chapel, theatre, courtyard, health &
                                wellness room and much more
                            </li>
                        </ul>
                        <?php $svg = get_svg( 'arrow-btn' ); ?>
                        <a href="<?php echo get_permalink(46); ?>" class="btn outline learn-more"
                           >Learn More <?php echo get_theme_svg( 'arrow-btn' ) ?></a>
                    </div>
                </div>
                <div class="right-block half">
                    <?php echo get_img_lazy( get_theme_img_url( 'smile.jpg' ) ); ?>

                    <div class="inner half-row">
                        <h2>Discover</h2>
                    </div>
                </div>
            </div>
        </section>

        <div class="explore">
            <div class="row full">
                <div class="left-block half">
                    <?php echo get_img_lazy( get_theme_img_url( 'inn.jpg' ) ); ?>

                    <div class="inner half-row">

                        <div class="wrapper-video">
                            <a href="<?php echo get_permalink(3104); ?>">
                                <?php echo get_img_lazy( get_theme_img_url( 'video_button.png' ) ); ?>
                            </a>

                            <p class="view-video">View Videos</p>

                            <h2>Explore</h2>
                        </div>

                    </div>
                </div>
                <div class="right-block half lazyload"
                     data-bg="<?php echo get_template_directory_uri(); ?>/assets/images/svg/bg-left.svg">
                    <div class="inner half-row">
                        <h2>Explore The Inn At Belden Village</h2>
                        <p class="explore-describe">Tour our private suites, beautiful patio, great room and
                            fireplace, dining room and more
                            as you discover the splendor and warmth of The Inn at Belden Village.</p>

                        <div class="icon-groups">
                            <div class="icon-group">
                                <i class="icon"><?php echo get_svg( 'photo' ); ?></i>
                                <a href="<?php echo get_permalink(509); ?>" class="btn btn-first outline"
                                   >Photo Gallery <?php echo get_theme_svg( 'arrow-btn' ) ?></a>
                            </div>
                            <div class="icon-group">
                                <i class="icon"><?php echo get_svg( 'brochure' ); ?></i>
                                <a href="<?php echo get_permalink(1707); ?>" class="btn btn-second outline">Online Brochure <?php echo get_theme_svg( 'arrow-btn' ) ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="meet">
            <div class="row full">
                <div class="left-block half lazyload"
                     data-bg="<?php echo get_template_directory_uri(); ?>/assets/images/svg/fancy-bg-sec.svg">
                    <div class="inner half-row">
                        <h2>What Families Say</h2>
                        <div class="entries slider meet-slider">
                            <div class="entry testimonial">
                                <div class="entry-content">
                                   The staff and aides have been very helpful, and The Inn at Belden Village is infused with a spirit of kindness, good humor, patience and compassion. They have shown a willingness to work with our family on a variety of issues and have communicated with us every step of the way. It’s impressive that so many of the team are continuing their professional training.
                                </div>
                                <div class="entry-resident">-Kathy</div>
                            </div>
                            <div class="entry testimonial">
                                <div class="entry-content">
                                    For a lucky few, there is a new family at The Inn at Belden Village. The minute you walk in the door, a friendly staff will greet you. You can be sure all questions will be answered. Once you move your loved one into the new room, all worries are over. Our loved one now has three meals a day, laundry services, transportation, medication administration and yes, even a place to get his hair cut. Living in Florida, we can smile as we drive down the highway knowing we made the right choice for our loved one.
                                </div>
                                <div class="entry-resident">-Richard</div>
                            </div>
                        </div>
                        <div class="actions">
                            <div class="entries-nav">
                                <div class="arrow arrow-left">
                                    <?php echo get_svg( 'slide-arrow' ); ?>
                                </div>
                                <div class="arrow arrow-right">
                                    <?php echo get_svg( 'slide-arrow' ); ?>
                                </div>
                            </div>
                            <a href="<?php echo get_permalink(169); ?>" class="btn outline">Hear more from families</a>
                        </div>
                    </div>
                </div>
                <div class="right-block half">
                    <div class="residents section-slider">
                        <?php 
                        $args = array(
                            'post_type' => 'post',
                            'category__in' => 5,
                            'showposts' => 4
                        );
                        // The Query
                        $the_query = new WP_Query( $args );
                        
                        // The Loop
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                ?>
                                 <a href="<?php echo get_permalink(); ?>" class="resident lazyload"
                                    data-bg="<?php echo get_the_post_thumbnail_url(); ?>">
                                    <h2>Meet Our Residents</h2>
                                    <p class="resident-name">Senior Spotlight: <span><?php echo str_replace('Senior Spotlight: ','',get_the_title()); ?></span></p>
                                </a>
                                <?php
                            }
                        } else {
                            // no posts found
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="questions">
            <?php $link_read_more = 'http://www.theinnatbeldenvillage.com/about-us/'; ?>
            <?php $read_more_text = 'Read More'; ?>
            <?php
            $qs = array(
                array(
                    'title' => 'How do I start the conversation about assisted living with my loved one?',
                    'body'  => 'Talking with your loved one about a change to assisted living can be stressful and overwhelming for both of you.
                        It must be handled with respect and dignity. Research your options ahead of time so you have some facts to present about the progression of any disease or the ability to live alone, as well as the assisted living communities available.
                        Address their fear of loss of independence and loneliness. Let them help make the decision.',
                    'link' => HOME_URL . '/start-conversation-assisted-living-loved-one/',
                ),
                array(
                    'title' => 'How do I choose the best community for my loved one?',
                    'body'  => 'Research the options in your area that could accommodate your loved one. Tour each one and look at the care available and the care that is being given. What are the attitudes of the staff and residents? Are they friendly and accommodating or disinterested? Also check for activities that your loved one will enjoy. Is it bright and cheerful, are their smiles and laughter or is it dark and idle? Are residents content and positive or do they seem lonely and lost?',
                'link' => HOME_URL . '/choose-best-community-loved-one/'
                ),
                array(
                    'title' => 'How do we afford assisted living?',
                    'body'  => 'Assisted Living communities vary widely in cost depending on room size, services offered and other factors. Although private pay is the most common, there are other options: long-term care policies, Veteran’s Aid and Attendance Benefit, Medicaid or SSI. Tax deductions may be available for you. Be sure to ask the admissions director at your intended community for ideas on financial aid.',
                'link' => HOME_URL . '/afford-assisted-living/'
                ),
                array(
                    'title' => 'What is assisted living and how do I know it\'s right for my loved one?',
                    'body'  => 'Assisted living communities are for people needing assistance with Activities of Daily Living (ADLs) but wishing to live as independently as possible for as long as possible. Assisted living exists to bridge the gap between independent living and nursing homes. Residents in assisted living centers are not able to live by themselves, but do not require constant care either. ',
                'link' => HOME_URL . '/assisted-living-know-right-loved-one/'
                )
            );
            ?>
            <div class="row">
                <h2>Answers to Important Questions</h2>
                <div class="toggles">
                    <?php foreach ( $qs as $q ) { ?>
                        <div class="toggle">
                            <h4>
                                <?php echo $q['title']; ?>
                            </h4>
                            <div class="body">
                                <?php echo add_read_more($q['body'],$q['link'], $read_more_text ); ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="gallery">
            <div class="row">
                <!-- Static Images -->
                <?php echo get_img_lazy( get_theme_img_url( 'man222.jpg' ) ); ?>
                <?php echo get_img_lazy( get_theme_img_url( 'man111.jpg' ) ); ?>
                <?php echo get_img_lazy( get_theme_img_url( 'flower444.jpg' ) ); ?>
                <?php echo get_img_lazy( get_theme_img_url( 'man333.jpg' ) ); ?>
            </div>
        </div>
    </main>

<?php get_footer(); ?>