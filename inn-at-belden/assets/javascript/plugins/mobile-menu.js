(function($) {
  var $header = $("#header");
  var $menu = $header.find("#menu");

  var toggleActive = function(event) {
    event.preventDefault();
    $menu.toggleClass("change");
    $header.toggleClass("change");
    $header.find(".bars").toggleClass("change");
    $("body, html").toggleClass("no-scroll");
  };

  var mobile_open_menu = function() {
    $(this)
      .next()
      .toggleClass("mobile-drop-open");

    $(this).toggleClass("rotate-dropdown-button");
  };

  $menu.on("click", ".bars", toggleActive);
  $header.on("click", ".dropdown-button", mobile_open_menu);
})(jQuery);
