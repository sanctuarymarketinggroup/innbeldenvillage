// Meet Slider
(function ($) {
    if ($(".front-page").length < 1) {
        return;
    }

    const $wrap = $(".meet");
    const $slider = $wrap.find(".meet-slider");

    const _prev = function () {
        $slider.slick("slickPrev");
    };

    const _next = function () {
        $slider.slick("slickNext");
    };

    $wrap.on("click", ".arrow-left", _prev);
    $wrap.on("click", ".arrow-right", _next);
})(jQuery);

// Important Questions
(function ($) {
    if ($(".front-page").length < 1) {
        return;
    }

    const $wrap = $(".toggles");

    const _toggle = function () {
        $(this)
            .parent()
            .toggleClass("active");
    };

    $wrap.on("click", "h4", _toggle);

})(jQuery);
