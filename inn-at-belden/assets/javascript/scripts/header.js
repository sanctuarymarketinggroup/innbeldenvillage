(function($) {
  const _toggleHeaderForm = function() {
    $(".header-form").toggleClass("active");
  };

  $(".notice").on("click", _toggleHeaderForm);

  const closeForm = function() {
    $(".header-form").removeClass("active");
  };

  $(".close-form svg").on("click", closeForm);

  /* ------------------------------------
     Block: Sticky
  ------------------------------------- */
  var lastScrollTop = 0;
  var mobileStopScroll = false;
  $(window).scroll(function() {
    if (mobileStopScroll) {
      return;
    }
    var winTop = $(window).scrollTop();
    var winWidth = $(window).width();

    if (winTop < 30) {
      if ($("body").hasClass("sticky-shrinknav-wrapper")) {
        $("body").removeClass("sticky-shrinknav-wrapper");
      }
      if ($("body").hasClass("not-top")) {
        $("body").removeClass("not-top");
      }
    } else if (winTop < lastScrollTop) {
      if ($("body").hasClass("sticky-shrinknav-wrapper")) {
        $("body").removeClass("sticky-shrinknav-wrapper");
      }
    } else if (winTop >= 30 && winWidth >= 1200) {
      $("body").addClass("sticky-shrinknav-wrapper");
      $("body").addClass("not-top");
      // $("#header").removeAttr('data-bg');
    } else {
      $("body").removeClass("sticky-shrinknav-wrapper");
      $("body").removeClass("not-top");
    }
    lastScrollTop = winTop;
  });

  /* ------------------------------------
     Block: FancyBox
  ------------------------------------- */
  $(".card a").fancybox();

  /* ------------------------------------
     Block: Menu Button
  ------------------------------------- */

  $("#mobile-menu").on("click", function() {
    $("body").toggleClass("mobile_menu_open");
    $("#menu-header-menu")
      .toggle()
      .toggleClass("menu_ready");
  });

  $(".menu-item-has-children").on("click", function() {
    $(this)
      .find(".dropdown-button")
      .toggleClass("active_arrow");
    $(this)
      .find(".dropdown.vertical")
      .toggle()
      .toggleClass("open_dropdown");
  });

  $(window).on("resize", function() {
    var winWidth = $(window).width();

    if (winWidth >= 1200) {
      $("body").removeClass("mobile_menu_open");
      $("#menu-header-menu").removeClass("menu_ready");
    }
  });

  /* ------------------------------------
     Block: Video
  ------------------------------------- */
})(jQuery);
