(function($) {


    /* ------------------------------------
    Block: scroll
    ------------------------------------- */

    var $page = $('html, body');
    $('a[href*="#"]:not(.tabs-title a)').click(function () {
        $page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 90
        }, 500);
        return false;
    });


    /* ------------------------------------
    Block: Form Footer
    ------------------------------------- */

    $('.get_touch_btn p').on('click', function () {

        console.log('work-1');

        $(this).css('display', 'none');
        $('#gform_wrapper_3').css('display','block');

    })

    $('.icon svg').on('mouseover', function(){
        $('span.direction').addClass('hover')
    });

    $('.icon svg').on('mouseleave', function(){
        $('span.direction').removeClass('hover')
    });

})(jQuery);
