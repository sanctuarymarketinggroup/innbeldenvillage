(function ($) {
  /*
    Options:
  
  #############################
    Required
  #############################
  
    -- Slide in Side = default left
      -- Possible options "top", "left", "right", "bottom"
  
    -- Slide in Speed = default 400
      -- Options any miliseconds
  
    -- Optional Open/Close Button = default Create on for them
      -- Any object with a div or class
  
    -- Width of Menu = default 100%
      -- Any number
  
  #############################
    Non Essentails
  #############################
  
    -- Fixed or Static = Default Fixed
      -- Non essential for now
  
    -- Animation
      -- Non essential for now
  
    -- Optional Button Styling
      -- Non essential for now
  
  
  
  */
  $.fn.mobile_menu = function (options) {
    // Define Default settings:
    var defaults = {
      side: 'left',
      speed: '0.5s',
      toggler: '.menu-toggle',
      width: '100%',
      height: '100%',
      buttonStyles: true
    };
    var initals = {
      toggleButton: '<div class="menu-toggle"></div>',
      menuWrap: '<div class="offcanvas-menu-wrap"></div>'
    };
    var $toggler;
    var offcanvas;
    var $this = $(this);
    var settings = $.extend({}, initals, defaults, options);

    if (settings.toggler == defaults.toggler) {
      $this.before(settings.toggleButton);
    }
    /* Determine if you want toggler Styles */


    $toggler = $(settings.toggler);

    if (settings.buttonStyles) {
      $toggler.addClass('offcanvas-toggle');
    }

    switch (settings.side) {
      case "left":
        offcanvas = 'translateX(-100%)';
        break;

      case "top":
        offcanvas = 'translateY(-100%)';
        break;

      case "right":
        offcanvas = 'translateX(100%)';
        break;

      case "bottom":
        offcanvas = 'translateY(100%)';
        break;
    }

    var styles = {
      'transform': offcanvas,
      'transition': settings.speed,
      height: settings.height,
      width: settings.width
    };
    return this.each(function () {
      $this.addClass('offcanvas-menu').wrap(settings.menuWrap);
      $this.parent().css(styles);
      console.log($toggler);
      $toggler.on('click', function () {
        console.log('open/close');
        $(this).toggleClass('offcanvas-active');
        console.log($this);
        $this.closest('.offcanvas-menu-wrap').toggleClass('offcanvas-open');
      });
    });
  };

  $('#mobile_menu').mobile_menu({
    side: 'bottom'
  });
})(jQuery);
;/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.6.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */

/* global window, document, define, jQuery, setInterval, clearInterval */
(function (factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }
})(function ($) {
  'use strict';

  var Slick = window.Slick || {};

  Slick = function () {
    var instanceUid = 0;

    function Slick(element, settings) {
      var _ = this,
          dataSettings;

      _.defaults = {
        accessibility: true,
        adaptiveHeight: false,
        appendArrows: $(element),
        appendDots: $(element),
        arrows: true,
        asNavFor: null,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        customPaging: function (slider, i) {
          return $('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1);
        },
        dots: false,
        dotsClass: 'slick-dots',
        draggable: true,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: true,
        swipeToSlide: false,
        touchMove: true,
        touchThreshold: 5,
        useCSS: true,
        useTransform: true,
        variableWidth: false,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      };
      _.initials = {
        animating: false,
        dragging: false,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: false,
        slideOffset: 0,
        swipeLeft: null,
        $list: null,
        touchObject: {},
        transformsEnabled: false,
        unslicked: false
      };
      $.extend(_, _.initials);
      _.activeBreakpoint = null;
      _.animType = null;
      _.animProp = null;
      _.breakpoints = [];
      _.breakpointSettings = [];
      _.cssTransitions = false;
      _.focussed = false;
      _.interrupted = false;
      _.hidden = 'hidden';
      _.paused = true;
      _.positionProp = null;
      _.respondTo = null;
      _.rowCount = 1;
      _.shouldClick = true;
      _.$slider = $(element);
      _.$slidesCache = null;
      _.transformType = null;
      _.transitionType = null;
      _.visibilityChange = 'visibilitychange';
      _.windowWidth = 0;
      _.windowTimer = null;
      dataSettings = $(element).data('slick') || {};
      _.options = $.extend({}, _.defaults, settings, dataSettings);
      _.currentSlide = _.options.initialSlide;
      _.originalSettings = _.options;

      if (typeof document.mozHidden !== 'undefined') {
        _.hidden = 'mozHidden';
        _.visibilityChange = 'mozvisibilitychange';
      } else if (typeof document.webkitHidden !== 'undefined') {
        _.hidden = 'webkitHidden';
        _.visibilityChange = 'webkitvisibilitychange';
      }

      _.autoPlay = $.proxy(_.autoPlay, _);
      _.autoPlayClear = $.proxy(_.autoPlayClear, _);
      _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
      _.changeSlide = $.proxy(_.changeSlide, _);
      _.clickHandler = $.proxy(_.clickHandler, _);
      _.selectHandler = $.proxy(_.selectHandler, _);
      _.setPosition = $.proxy(_.setPosition, _);
      _.swipeHandler = $.proxy(_.swipeHandler, _);
      _.dragHandler = $.proxy(_.dragHandler, _);
      _.keyHandler = $.proxy(_.keyHandler, _);
      _.instanceUid = instanceUid++; // A simple way to check for HTML strings
      // Strict HTML recognition (must start with <)
      // Extracted from jQuery v1.11 source

      _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;

      _.registerBreakpoints();

      _.init(true);
    }

    return Slick;
  }();

  Slick.prototype.activateADA = function () {
    var _ = this;

    _.$slideTrack.find('.slick-active').attr({
      'aria-hidden': 'false'
    }).find('a, input, button, select').attr({
      'tabindex': '0'
    });
  };

  Slick.prototype.addSlide = Slick.prototype.slickAdd = function (markup, index, addBefore) {
    var _ = this;

    if (typeof index === 'boolean') {
      addBefore = index;
      index = null;
    } else if (index < 0 || index >= _.slideCount) {
      return false;
    }

    _.unload();

    if (typeof index === 'number') {
      if (index === 0 && _.$slides.length === 0) {
        $(markup).appendTo(_.$slideTrack);
      } else if (addBefore) {
        $(markup).insertBefore(_.$slides.eq(index));
      } else {
        $(markup).insertAfter(_.$slides.eq(index));
      }
    } else {
      if (addBefore === true) {
        $(markup).prependTo(_.$slideTrack);
      } else {
        $(markup).appendTo(_.$slideTrack);
      }
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slides.each(function (index, element) {
      $(element).attr('data-slick-index', index);
    });

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.animateHeight = function () {
    var _ = this;

    if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);

      _.$list.animate({
        height: targetHeight
      }, _.options.speed);
    }
  };

  Slick.prototype.animateSlide = function (targetLeft, callback) {
    var animProps = {},
        _ = this;

    _.animateHeight();

    if (_.options.rtl === true && _.options.vertical === false) {
      targetLeft = -targetLeft;
    }

    if (_.transformsEnabled === false) {
      if (_.options.vertical === false) {
        _.$slideTrack.animate({
          left: targetLeft
        }, _.options.speed, _.options.easing, callback);
      } else {
        _.$slideTrack.animate({
          top: targetLeft
        }, _.options.speed, _.options.easing, callback);
      }
    } else {
      if (_.cssTransitions === false) {
        if (_.options.rtl === true) {
          _.currentLeft = -_.currentLeft;
        }

        $({
          animStart: _.currentLeft
        }).animate({
          animStart: targetLeft
        }, {
          duration: _.options.speed,
          easing: _.options.easing,
          step: function (now) {
            now = Math.ceil(now);

            if (_.options.vertical === false) {
              animProps[_.animType] = 'translate(' + now + 'px, 0px)';

              _.$slideTrack.css(animProps);
            } else {
              animProps[_.animType] = 'translate(0px,' + now + 'px)';

              _.$slideTrack.css(animProps);
            }
          },
          complete: function () {
            if (callback) {
              callback.call();
            }
          }
        });
      } else {
        _.applyTransition();

        targetLeft = Math.ceil(targetLeft);

        if (_.options.vertical === false) {
          animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
        } else {
          animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
        }

        _.$slideTrack.css(animProps);

        if (callback) {
          setTimeout(function () {
            _.disableTransition();

            callback.call();
          }, _.options.speed);
        }
      }
    }
  };

  Slick.prototype.getNavTarget = function () {
    var _ = this,
        asNavFor = _.options.asNavFor;

    if (asNavFor && asNavFor !== null) {
      asNavFor = $(asNavFor).not(_.$slider);
    }

    return asNavFor;
  };

  Slick.prototype.asNavFor = function (index) {
    var _ = this,
        asNavFor = _.getNavTarget();

    if (asNavFor !== null && typeof asNavFor === 'object') {
      asNavFor.each(function () {
        var target = $(this).slick('getSlick');

        if (!target.unslicked) {
          target.slideHandler(index, true);
        }
      });
    }
  };

  Slick.prototype.applyTransition = function (slide) {
    var _ = this,
        transition = {};

    if (_.options.fade === false) {
      transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
    } else {
      transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
    }

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.autoPlay = function () {
    var _ = this;

    _.autoPlayClear();

    if (_.slideCount > _.options.slidesToShow) {
      _.autoPlayTimer = setInterval(_.autoPlayIterator, _.options.autoplaySpeed);
    }
  };

  Slick.prototype.autoPlayClear = function () {
    var _ = this;

    if (_.autoPlayTimer) {
      clearInterval(_.autoPlayTimer);
    }
  };

  Slick.prototype.autoPlayIterator = function () {
    var _ = this,
        slideTo = _.currentSlide + _.options.slidesToScroll;

    if (!_.paused && !_.interrupted && !_.focussed) {
      if (_.options.infinite === false) {
        if (_.direction === 1 && _.currentSlide + 1 === _.slideCount - 1) {
          _.direction = 0;
        } else if (_.direction === 0) {
          slideTo = _.currentSlide - _.options.slidesToScroll;

          if (_.currentSlide - 1 === 0) {
            _.direction = 1;
          }
        }
      }

      _.slideHandler(slideTo);
    }
  };

  Slick.prototype.buildArrows = function () {
    var _ = this;

    if (_.options.arrows === true) {
      _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
      _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

      if (_.slideCount > _.options.slidesToShow) {
        _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

        _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

        if (_.htmlExpr.test(_.options.prevArrow)) {
          _.$prevArrow.prependTo(_.options.appendArrows);
        }

        if (_.htmlExpr.test(_.options.nextArrow)) {
          _.$nextArrow.appendTo(_.options.appendArrows);
        }

        if (_.options.infinite !== true) {
          _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        }
      } else {
        _.$prevArrow.add(_.$nextArrow).addClass('slick-hidden').attr({
          'aria-disabled': 'true',
          'tabindex': '-1'
        });
      }
    }
  };

  Slick.prototype.buildDots = function () {
    var _ = this,
        i,
        dot;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$slider.addClass('slick-dotted');

      dot = $('<ul />').addClass(_.options.dotsClass);

      for (i = 0; i <= _.getDotCount(); i += 1) {
        dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
      }

      _.$dots = dot.appendTo(_.options.appendDots);

      _.$dots.find('li').first().addClass('slick-active').attr('aria-hidden', 'false');
    }
  };

  Slick.prototype.buildOut = function () {
    var _ = this;

    _.$slides = _.$slider.children(_.options.slide + ':not(.slick-cloned)').addClass('slick-slide');
    _.slideCount = _.$slides.length;

    _.$slides.each(function (index, element) {
      $(element).attr('data-slick-index', index).data('originalStyling', $(element).attr('style') || '');
    });

    _.$slider.addClass('slick-slider');

    _.$slideTrack = _.slideCount === 0 ? $('<div class="slick-track"/>').appendTo(_.$slider) : _.$slides.wrapAll('<div class="slick-track"/>').parent();
    _.$list = _.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent();

    _.$slideTrack.css('opacity', 0);

    if (_.options.centerMode === true || _.options.swipeToSlide === true) {
      _.options.slidesToScroll = 1;
    }

    $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

    _.setupInfinite();

    _.buildArrows();

    _.buildDots();

    _.updateDots();

    _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

    if (_.options.draggable === true) {
      _.$list.addClass('draggable');
    }
  };

  Slick.prototype.buildRows = function () {
    var _ = this,
        a,
        b,
        c,
        newSlides,
        numOfSlides,
        originalSlides,
        slidesPerSection;

    newSlides = document.createDocumentFragment();
    originalSlides = _.$slider.children();

    if (_.options.rows > 1) {
      slidesPerSection = _.options.slidesPerRow * _.options.rows;
      numOfSlides = Math.ceil(originalSlides.length / slidesPerSection);

      for (a = 0; a < numOfSlides; a++) {
        var slide = document.createElement('div');

        for (b = 0; b < _.options.rows; b++) {
          var row = document.createElement('div');

          for (c = 0; c < _.options.slidesPerRow; c++) {
            var target = a * slidesPerSection + (b * _.options.slidesPerRow + c);

            if (originalSlides.get(target)) {
              row.appendChild(originalSlides.get(target));
            }
          }

          slide.appendChild(row);
        }

        newSlides.appendChild(slide);
      }

      _.$slider.empty().append(newSlides);

      _.$slider.children().children().children().css({
        'width': 100 / _.options.slidesPerRow + '%',
        'display': 'inline-block'
      });
    }
  };

  Slick.prototype.checkResponsive = function (initial, forceUpdate) {
    var _ = this,
        breakpoint,
        targetBreakpoint,
        respondToWidth,
        triggerBreakpoint = false;

    var sliderWidth = _.$slider.width();

    var windowWidth = window.innerWidth || $(window).width();

    if (_.respondTo === 'window') {
      respondToWidth = windowWidth;
    } else if (_.respondTo === 'slider') {
      respondToWidth = sliderWidth;
    } else if (_.respondTo === 'min') {
      respondToWidth = Math.min(windowWidth, sliderWidth);
    }

    if (_.options.responsive && _.options.responsive.length && _.options.responsive !== null) {
      targetBreakpoint = null;

      for (breakpoint in _.breakpoints) {
        if (_.breakpoints.hasOwnProperty(breakpoint)) {
          if (_.originalSettings.mobileFirst === false) {
            if (respondToWidth < _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          } else {
            if (respondToWidth > _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          }
        }
      }

      if (targetBreakpoint !== null) {
        if (_.activeBreakpoint !== null) {
          if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
            _.activeBreakpoint = targetBreakpoint;

            if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
              _.unslick(targetBreakpoint);
            } else {
              _.options = $.extend({}, _.originalSettings, _.breakpointSettings[targetBreakpoint]);

              if (initial === true) {
                _.currentSlide = _.options.initialSlide;
              }

              _.refresh(initial);
            }

            triggerBreakpoint = targetBreakpoint;
          }
        } else {
          _.activeBreakpoint = targetBreakpoint;

          if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
            _.unslick(targetBreakpoint);
          } else {
            _.options = $.extend({}, _.originalSettings, _.breakpointSettings[targetBreakpoint]);

            if (initial === true) {
              _.currentSlide = _.options.initialSlide;
            }

            _.refresh(initial);
          }

          triggerBreakpoint = targetBreakpoint;
        }
      } else {
        if (_.activeBreakpoint !== null) {
          _.activeBreakpoint = null;
          _.options = _.originalSettings;

          if (initial === true) {
            _.currentSlide = _.options.initialSlide;
          }

          _.refresh(initial);

          triggerBreakpoint = targetBreakpoint;
        }
      } // only trigger breakpoints during an actual break. not on initialize.


      if (!initial && triggerBreakpoint !== false) {
        _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
      }
    }
  };

  Slick.prototype.changeSlide = function (event, dontAnimate) {
    var _ = this,
        $target = $(event.currentTarget),
        indexOffset,
        slideOffset,
        unevenOffset; // If target is a link, prevent default action.


    if ($target.is('a')) {
      event.preventDefault();
    } // If target is not the <li> element (ie: a child), find the <li>.


    if (!$target.is('li')) {
      $target = $target.closest('li');
    }

    unevenOffset = _.slideCount % _.options.slidesToScroll !== 0;
    indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

    switch (event.data.message) {
      case 'previous':
        slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;

        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
        }

        break;

      case 'next':
        slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;

        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
        }

        break;

      case 'index':
        var index = event.data.index === 0 ? 0 : event.data.index || $target.index() * _.options.slidesToScroll;

        _.slideHandler(_.checkNavigable(index), false, dontAnimate);

        $target.children().trigger('focus');
        break;

      default:
        return;
    }
  };

  Slick.prototype.checkNavigable = function (index) {
    var _ = this,
        navigables,
        prevNavigable;

    navigables = _.getNavigableIndexes();
    prevNavigable = 0;

    if (index > navigables[navigables.length - 1]) {
      index = navigables[navigables.length - 1];
    } else {
      for (var n in navigables) {
        if (index < navigables[n]) {
          index = prevNavigable;
          break;
        }

        prevNavigable = navigables[n];
      }
    }

    return index;
  };

  Slick.prototype.cleanUpEvents = function () {
    var _ = this;

    if (_.options.dots && _.$dots !== null) {
      $('li', _.$dots).off('click.slick', _.changeSlide).off('mouseenter.slick', $.proxy(_.interrupt, _, true)).off('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }

    _.$slider.off('focus.slick blur.slick');

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
      _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);
    }

    _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);

    _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);

    _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);

    _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

    _.$list.off('click.slick', _.clickHandler);

    $(document).off(_.visibilityChange, _.visibility);

    _.cleanUpSlideEvents();

    if (_.options.accessibility === true) {
      _.$list.off('keydown.slick', _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().off('click.slick', _.selectHandler);
    }

    $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);
    $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);
    $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);
    $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);
    $(document).off('ready.slick.slick-' + _.instanceUid, _.setPosition);
  };

  Slick.prototype.cleanUpSlideEvents = function () {
    var _ = this;

    _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));

    _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
  };

  Slick.prototype.cleanUpRows = function () {
    var _ = this,
        originalSlides;

    if (_.options.rows > 1) {
      originalSlides = _.$slides.children().children();
      originalSlides.removeAttr('style');

      _.$slider.empty().append(originalSlides);
    }
  };

  Slick.prototype.clickHandler = function (event) {
    var _ = this;

    if (_.shouldClick === false) {
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  };

  Slick.prototype.destroy = function (refresh) {
    var _ = this;

    _.autoPlayClear();

    _.touchObject = {};

    _.cleanUpEvents();

    $('.slick-cloned', _.$slider).detach();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.$prevArrow.length) {
      _.$prevArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');

      if (_.htmlExpr.test(_.options.prevArrow)) {
        _.$prevArrow.remove();
      }
    }

    if (_.$nextArrow && _.$nextArrow.length) {
      _.$nextArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');

      if (_.htmlExpr.test(_.options.nextArrow)) {
        _.$nextArrow.remove();
      }
    }

    if (_.$slides) {
      _.$slides.removeClass('slick-slide slick-active slick-center slick-visible slick-current').removeAttr('aria-hidden').removeAttr('data-slick-index').each(function () {
        $(this).attr('style', $(this).data('originalStyling'));
      });

      _.$slideTrack.children(this.options.slide).detach();

      _.$slideTrack.detach();

      _.$list.detach();

      _.$slider.append(_.$slides);
    }

    _.cleanUpRows();

    _.$slider.removeClass('slick-slider');

    _.$slider.removeClass('slick-initialized');

    _.$slider.removeClass('slick-dotted');

    _.unslicked = true;

    if (!refresh) {
      _.$slider.trigger('destroy', [_]);
    }
  };

  Slick.prototype.disableTransition = function (slide) {
    var _ = this,
        transition = {};

    transition[_.transitionType] = '';

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.fadeSlide = function (slideIndex, callback) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).css({
        zIndex: _.options.zIndex
      });

      _.$slides.eq(slideIndex).animate({
        opacity: 1
      }, _.options.speed, _.options.easing, callback);
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 1,
        zIndex: _.options.zIndex
      });

      if (callback) {
        setTimeout(function () {
          _.disableTransition(slideIndex);

          callback.call();
        }, _.options.speed);
      }
    }
  };

  Slick.prototype.fadeSlideOut = function (slideIndex) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).animate({
        opacity: 0,
        zIndex: _.options.zIndex - 2
      }, _.options.speed, _.options.easing);
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 0,
        zIndex: _.options.zIndex - 2
      });
    }
  };

  Slick.prototype.filterSlides = Slick.prototype.slickFilter = function (filter) {
    var _ = this;

    if (filter !== null) {
      _.$slidesCache = _.$slides;

      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.focusHandler = function () {
    var _ = this;

    _.$slider.off('focus.slick blur.slick').on('focus.slick blur.slick', '*:not(.slick-arrow)', function (event) {
      event.stopImmediatePropagation();
      var $sf = $(this);
      setTimeout(function () {
        if (_.options.pauseOnFocus) {
          _.focussed = $sf.is(':focus');

          _.autoPlay();
        }
      }, 0);
    });
  };

  Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function () {
    var _ = this;

    return _.currentSlide;
  };

  Slick.prototype.getDotCount = function () {
    var _ = this;

    var breakPoint = 0;
    var counter = 0;
    var pagerQty = 0;

    if (_.options.infinite === true) {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
      }
    } else if (_.options.centerMode === true) {
      pagerQty = _.slideCount;
    } else if (!_.options.asNavFor) {
      pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
    } else {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
      }
    }

    return pagerQty - 1;
  };

  Slick.prototype.getLeft = function (slideIndex) {
    var _ = this,
        targetLeft,
        verticalHeight,
        verticalOffset = 0,
        targetSlide;

    _.slideOffset = 0;
    verticalHeight = _.$slides.first().outerHeight(true);

    if (_.options.infinite === true) {
      if (_.slideCount > _.options.slidesToShow) {
        _.slideOffset = _.slideWidth * _.options.slidesToShow * -1;
        verticalOffset = verticalHeight * _.options.slidesToShow * -1;
      }

      if (_.slideCount % _.options.slidesToScroll !== 0) {
        if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
          if (slideIndex > _.slideCount) {
            _.slideOffset = (_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth * -1;
            verticalOffset = (_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight * -1;
          } else {
            _.slideOffset = _.slideCount % _.options.slidesToScroll * _.slideWidth * -1;
            verticalOffset = _.slideCount % _.options.slidesToScroll * verticalHeight * -1;
          }
        }
      }
    } else {
      if (slideIndex + _.options.slidesToShow > _.slideCount) {
        _.slideOffset = (slideIndex + _.options.slidesToShow - _.slideCount) * _.slideWidth;
        verticalOffset = (slideIndex + _.options.slidesToShow - _.slideCount) * verticalHeight;
      }
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.slideOffset = 0;
      verticalOffset = 0;
    }

    if (_.options.centerMode === true && _.options.infinite === true) {
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
    } else if (_.options.centerMode === true) {
      _.slideOffset = 0;
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
    }

    if (_.options.vertical === false) {
      targetLeft = slideIndex * _.slideWidth * -1 + _.slideOffset;
    } else {
      targetLeft = slideIndex * verticalHeight * -1 + verticalOffset;
    }

    if (_.options.variableWidth === true) {
      if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
        targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
      } else {
        targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
      }

      if (_.options.rtl === true) {
        if (targetSlide[0]) {
          targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
        } else {
          targetLeft = 0;
        }
      } else {
        targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
      }

      if (_.options.centerMode === true) {
        if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
          targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
        } else {
          targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
        }

        if (_.options.rtl === true) {
          if (targetSlide[0]) {
            targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
          } else {
            targetLeft = 0;
          }
        } else {
          targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
        }

        targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
      }
    }

    return targetLeft;
  };

  Slick.prototype.getOption = Slick.prototype.slickGetOption = function (option) {
    var _ = this;

    return _.options[option];
  };

  Slick.prototype.getNavigableIndexes = function () {
    var _ = this,
        breakPoint = 0,
        counter = 0,
        indexes = [],
        max;

    if (_.options.infinite === false) {
      max = _.slideCount;
    } else {
      breakPoint = _.options.slidesToScroll * -1;
      counter = _.options.slidesToScroll * -1;
      max = _.slideCount * 2;
    }

    while (breakPoint < max) {
      indexes.push(breakPoint);
      breakPoint = counter + _.options.slidesToScroll;
      counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
    }

    return indexes;
  };

  Slick.prototype.getSlick = function () {
    return this;
  };

  Slick.prototype.getSlideCount = function () {
    var _ = this,
        slidesTraversed,
        swipedSlide,
        centerOffset;

    centerOffset = _.options.centerMode === true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0;

    if (_.options.swipeToSlide === true) {
      _.$slideTrack.find('.slick-slide').each(function (index, slide) {
        if (slide.offsetLeft - centerOffset + $(slide).outerWidth() / 2 > _.swipeLeft * -1) {
          swipedSlide = slide;
          return false;
        }
      });

      slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;
      return slidesTraversed;
    } else {
      return _.options.slidesToScroll;
    }
  };

  Slick.prototype.goTo = Slick.prototype.slickGoTo = function (slide, dontAnimate) {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'index',
        index: parseInt(slide)
      }
    }, dontAnimate);
  };

  Slick.prototype.init = function (creation) {
    var _ = this;

    if (!$(_.$slider).hasClass('slick-initialized')) {
      $(_.$slider).addClass('slick-initialized');

      _.buildRows();

      _.buildOut();

      _.setProps();

      _.startLoad();

      _.loadSlider();

      _.initializeEvents();

      _.updateArrows();

      _.updateDots();

      _.checkResponsive(true);

      _.focusHandler();
    }

    if (creation) {
      _.$slider.trigger('init', [_]);
    }

    if (_.options.accessibility === true) {
      _.initADA();
    }

    if (_.options.autoplay) {
      _.paused = false;

      _.autoPlay();
    }
  };

  Slick.prototype.initADA = function () {
    var _ = this;

    _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
      'aria-hidden': 'true',
      'tabindex': '-1'
    }).find('a, input, button, select').attr({
      'tabindex': '-1'
    });

    _.$slideTrack.attr('role', 'listbox');

    _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function (i) {
      $(this).attr({
        'role': 'option',
        'aria-describedby': 'slick-slide' + _.instanceUid + i + ''
      });
    });

    if (_.$dots !== null) {
      _.$dots.attr('role', 'tablist').find('li').each(function (i) {
        $(this).attr({
          'role': 'presentation',
          'aria-selected': 'false',
          'aria-controls': 'navigation' + _.instanceUid + i + '',
          'id': 'slick-slide' + _.instanceUid + i + ''
        });
      }).first().attr('aria-selected', 'true').end().find('button').attr('role', 'button').end().closest('div').attr('role', 'toolbar');
    }

    _.activateADA();
  };

  Slick.prototype.initArrowEvents = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.off('click.slick').on('click.slick', {
        message: 'previous'
      }, _.changeSlide);

      _.$nextArrow.off('click.slick').on('click.slick', {
        message: 'next'
      }, _.changeSlide);
    }
  };

  Slick.prototype.initDotEvents = function () {
    var _ = this;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      $('li', _.$dots).on('click.slick', {
        message: 'index'
      }, _.changeSlide);
    }

    if (_.options.dots === true && _.options.pauseOnDotsHover === true) {
      $('li', _.$dots).on('mouseenter.slick', $.proxy(_.interrupt, _, true)).on('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initSlideEvents = function () {
    var _ = this;

    if (_.options.pauseOnHover) {
      _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));

      _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initializeEvents = function () {
    var _ = this;

    _.initArrowEvents();

    _.initDotEvents();

    _.initSlideEvents();

    _.$list.on('touchstart.slick mousedown.slick', {
      action: 'start'
    }, _.swipeHandler);

    _.$list.on('touchmove.slick mousemove.slick', {
      action: 'move'
    }, _.swipeHandler);

    _.$list.on('touchend.slick mouseup.slick', {
      action: 'end'
    }, _.swipeHandler);

    _.$list.on('touchcancel.slick mouseleave.slick', {
      action: 'end'
    }, _.swipeHandler);

    _.$list.on('click.slick', _.clickHandler);

    $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

    if (_.options.accessibility === true) {
      _.$list.on('keydown.slick', _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on('click.slick', _.selectHandler);
    }

    $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));
    $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));
    $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);
    $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
    $(document).on('ready.slick.slick-' + _.instanceUid, _.setPosition);
  };

  Slick.prototype.initUI = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.show();

      _.$nextArrow.show();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.show();
    }
  };

  Slick.prototype.keyHandler = function (event) {
    var _ = this; //Dont slide if the cursor is inside the form fields and arrow keys are pressed


    if (!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
      if (event.keyCode === 37 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? 'next' : 'previous'
          }
        });
      } else if (event.keyCode === 39 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? 'previous' : 'next'
          }
        });
      }
    }
  };

  Slick.prototype.lazyLoad = function () {
    var _ = this,
        loadRange,
        cloneRange,
        rangeStart,
        rangeEnd;

    function loadImages(imagesScope) {
      $('img[data-lazy]', imagesScope).each(function () {
        var image = $(this),
            imageSource = $(this).attr('data-lazy'),
            imageToLoad = document.createElement('img');

        imageToLoad.onload = function () {
          image.animate({
            opacity: 0
          }, 100, function () {
            image.attr('src', imageSource).animate({
              opacity: 1
            }, 200, function () {
              image.removeAttr('data-lazy').removeClass('slick-loading');
            });

            _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
          });
        };

        imageToLoad.onerror = function () {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');

          _.$slider.trigger('lazyLoadError', [_, image, imageSource]);
        };

        imageToLoad.src = imageSource;
      });
    }

    if (_.options.centerMode === true) {
      if (_.options.infinite === true) {
        rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
        rangeEnd = rangeStart + _.options.slidesToShow + 2;
      } else {
        rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
        rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
      }
    } else {
      rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
      rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);

      if (_.options.fade === true) {
        if (rangeStart > 0) rangeStart--;
        if (rangeEnd <= _.slideCount) rangeEnd++;
      }
    }

    loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
    loadImages(loadRange);

    if (_.slideCount <= _.options.slidesToShow) {
      cloneRange = _.$slider.find('.slick-slide');
      loadImages(cloneRange);
    } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
      cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
      loadImages(cloneRange);
    } else if (_.currentSlide === 0) {
      cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
      loadImages(cloneRange);
    }
  };

  Slick.prototype.loadSlider = function () {
    var _ = this;

    _.setPosition();

    _.$slideTrack.css({
      opacity: 1
    });

    _.$slider.removeClass('slick-loading');

    _.initUI();

    if (_.options.lazyLoad === 'progressive') {
      _.progressiveLazyLoad();
    }
  };

  Slick.prototype.next = Slick.prototype.slickNext = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'next'
      }
    });
  };

  Slick.prototype.orientationChange = function () {
    var _ = this;

    _.checkResponsive();

    _.setPosition();
  };

  Slick.prototype.pause = Slick.prototype.slickPause = function () {
    var _ = this;

    _.autoPlayClear();

    _.paused = true;
  };

  Slick.prototype.play = Slick.prototype.slickPlay = function () {
    var _ = this;

    _.autoPlay();

    _.options.autoplay = true;
    _.paused = false;
    _.focussed = false;
    _.interrupted = false;
  };

  Slick.prototype.postSlide = function (index) {
    var _ = this;

    if (!_.unslicked) {
      _.$slider.trigger('afterChange', [_, index]);

      _.animating = false;

      _.setPosition();

      _.swipeLeft = null;

      if (_.options.autoplay) {
        _.autoPlay();
      }

      if (_.options.accessibility === true) {
        _.initADA();
      }
    }
  };

  Slick.prototype.prev = Slick.prototype.slickPrev = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: 'previous'
      }
    });
  };

  Slick.prototype.preventDefault = function (event) {
    event.preventDefault();
  };

  Slick.prototype.progressiveLazyLoad = function (tryCount) {
    tryCount = tryCount || 1;

    var _ = this,
        $imgsToLoad = $('img[data-lazy]', _.$slider),
        image,
        imageSource,
        imageToLoad;

    if ($imgsToLoad.length) {
      image = $imgsToLoad.first();
      imageSource = image.attr('data-lazy');
      imageToLoad = document.createElement('img');

      imageToLoad.onload = function () {
        image.attr('src', imageSource).removeAttr('data-lazy').removeClass('slick-loading');

        if (_.options.adaptiveHeight === true) {
          _.setPosition();
        }

        _.$slider.trigger('lazyLoaded', [_, image, imageSource]);

        _.progressiveLazyLoad();
      };

      imageToLoad.onerror = function () {
        if (tryCount < 3) {
          /**
           * try to load the image 3 times,
           * leave a slight delay so we don't get
           * servers blocking the request.
           */
          setTimeout(function () {
            _.progressiveLazyLoad(tryCount + 1);
          }, 500);
        } else {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');

          _.$slider.trigger('lazyLoadError', [_, image, imageSource]);

          _.progressiveLazyLoad();
        }
      };

      imageToLoad.src = imageSource;
    } else {
      _.$slider.trigger('allImagesLoaded', [_]);
    }
  };

  Slick.prototype.refresh = function (initializing) {
    var _ = this,
        currentSlide,
        lastVisibleIndex;

    lastVisibleIndex = _.slideCount - _.options.slidesToShow; // in non-infinite sliders, we don't want to go past the
    // last visible index.

    if (!_.options.infinite && _.currentSlide > lastVisibleIndex) {
      _.currentSlide = lastVisibleIndex;
    } // if less slides than to show, go to start.


    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    currentSlide = _.currentSlide;

    _.destroy(true);

    $.extend(_, _.initials, {
      currentSlide: currentSlide
    });

    _.init();

    if (!initializing) {
      _.changeSlide({
        data: {
          message: 'index',
          index: currentSlide
        }
      }, false);
    }
  };

  Slick.prototype.registerBreakpoints = function () {
    var _ = this,
        breakpoint,
        currentBreakpoint,
        l,
        responsiveSettings = _.options.responsive || null;

    if ($.type(responsiveSettings) === 'array' && responsiveSettings.length) {
      _.respondTo = _.options.respondTo || 'window';

      for (breakpoint in responsiveSettings) {
        l = _.breakpoints.length - 1;
        currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

        if (responsiveSettings.hasOwnProperty(breakpoint)) {
          // loop through the breakpoints and cut out any existing
          // ones with the same breakpoint number, we don't want dupes.
          while (l >= 0) {
            if (_.breakpoints[l] && _.breakpoints[l] === currentBreakpoint) {
              _.breakpoints.splice(l, 1);
            }

            l--;
          }

          _.breakpoints.push(currentBreakpoint);

          _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;
        }
      }

      _.breakpoints.sort(function (a, b) {
        return _.options.mobileFirst ? a - b : b - a;
      });
    }
  };

  Slick.prototype.reinit = function () {
    var _ = this;

    _.$slides = _.$slideTrack.children(_.options.slide).addClass('slick-slide');
    _.slideCount = _.$slides.length;

    if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
      _.currentSlide = _.currentSlide - _.options.slidesToScroll;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    _.registerBreakpoints();

    _.setProps();

    _.setupInfinite();

    _.buildArrows();

    _.updateArrows();

    _.initArrowEvents();

    _.buildDots();

    _.updateDots();

    _.initDotEvents();

    _.cleanUpSlideEvents();

    _.initSlideEvents();

    _.checkResponsive(false, true);

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on('click.slick', _.selectHandler);
    }

    _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

    _.setPosition();

    _.focusHandler();

    _.paused = !_.options.autoplay;

    _.autoPlay();

    _.$slider.trigger('reInit', [_]);
  };

  Slick.prototype.resize = function () {
    var _ = this;

    if ($(window).width() !== _.windowWidth) {
      clearTimeout(_.windowDelay);
      _.windowDelay = window.setTimeout(function () {
        _.windowWidth = $(window).width();

        _.checkResponsive();

        if (!_.unslicked) {
          _.setPosition();
        }
      }, 50);
    }
  };

  Slick.prototype.removeSlide = Slick.prototype.slickRemove = function (index, removeBefore, removeAll) {
    var _ = this;

    if (typeof index === 'boolean') {
      removeBefore = index;
      index = removeBefore === true ? 0 : _.slideCount - 1;
    } else {
      index = removeBefore === true ? --index : index;
    }

    if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
      return false;
    }

    _.unload();

    if (removeAll === true) {
      _.$slideTrack.children().remove();
    } else {
      _.$slideTrack.children(this.options.slide).eq(index).remove();
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.setCSS = function (position) {
    var _ = this,
        positionProps = {},
        x,
        y;

    if (_.options.rtl === true) {
      position = -position;
    }

    x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
    y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';
    positionProps[_.positionProp] = position;

    if (_.transformsEnabled === false) {
      _.$slideTrack.css(positionProps);
    } else {
      positionProps = {};

      if (_.cssTransitions === false) {
        positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';

        _.$slideTrack.css(positionProps);
      } else {
        positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';

        _.$slideTrack.css(positionProps);
      }
    }
  };

  Slick.prototype.setDimensions = function () {
    var _ = this;

    if (_.options.vertical === false) {
      if (_.options.centerMode === true) {
        _.$list.css({
          padding: '0px ' + _.options.centerPadding
        });
      }
    } else {
      _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);

      if (_.options.centerMode === true) {
        _.$list.css({
          padding: _.options.centerPadding + ' 0px'
        });
      }
    }

    _.listWidth = _.$list.width();
    _.listHeight = _.$list.height();

    if (_.options.vertical === false && _.options.variableWidth === false) {
      _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);

      _.$slideTrack.width(Math.ceil(_.slideWidth * _.$slideTrack.children('.slick-slide').length));
    } else if (_.options.variableWidth === true) {
      _.$slideTrack.width(5000 * _.slideCount);
    } else {
      _.slideWidth = Math.ceil(_.listWidth);

      _.$slideTrack.height(Math.ceil(_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length));
    }

    var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();

    if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);
  };

  Slick.prototype.setFade = function () {
    var _ = this,
        targetLeft;

    _.$slides.each(function (index, element) {
      targetLeft = _.slideWidth * index * -1;

      if (_.options.rtl === true) {
        $(element).css({
          position: 'relative',
          right: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0
        });
      } else {
        $(element).css({
          position: 'relative',
          left: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0
        });
      }
    });

    _.$slides.eq(_.currentSlide).css({
      zIndex: _.options.zIndex - 1,
      opacity: 1
    });
  };

  Slick.prototype.setHeight = function () {
    var _ = this;

    if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);

      _.$list.css('height', targetHeight);
    }
  };

  Slick.prototype.setOption = Slick.prototype.slickSetOption = function () {
    /**
     * accepts arguments in format of:
     *
     *  - for changing a single option's value:
     *     .slick("setOption", option, value, refresh )
     *
     *  - for changing a set of responsive options:
     *     .slick("setOption", 'responsive', [{}, ...], refresh )
     *
     *  - for updating multiple values at once (not responsive)
     *     .slick("setOption", { 'option': value, ... }, refresh )
     */
    var _ = this,
        l,
        item,
        option,
        value,
        refresh = false,
        type;

    if ($.type(arguments[0]) === 'object') {
      option = arguments[0];
      refresh = arguments[1];
      type = 'multiple';
    } else if ($.type(arguments[0]) === 'string') {
      option = arguments[0];
      value = arguments[1];
      refresh = arguments[2];

      if (arguments[0] === 'responsive' && $.type(arguments[1]) === 'array') {
        type = 'responsive';
      } else if (typeof arguments[1] !== 'undefined') {
        type = 'single';
      }
    }

    if (type === 'single') {
      _.options[option] = value;
    } else if (type === 'multiple') {
      $.each(option, function (opt, val) {
        _.options[opt] = val;
      });
    } else if (type === 'responsive') {
      for (item in value) {
        if ($.type(_.options.responsive) !== 'array') {
          _.options.responsive = [value[item]];
        } else {
          l = _.options.responsive.length - 1; // loop through the responsive object and splice out duplicates.

          while (l >= 0) {
            if (_.options.responsive[l].breakpoint === value[item].breakpoint) {
              _.options.responsive.splice(l, 1);
            }

            l--;
          }

          _.options.responsive.push(value[item]);
        }
      }
    }

    if (refresh) {
      _.unload();

      _.reinit();
    }
  };

  Slick.prototype.setPosition = function () {
    var _ = this;

    _.setDimensions();

    _.setHeight();

    if (_.options.fade === false) {
      _.setCSS(_.getLeft(_.currentSlide));
    } else {
      _.setFade();
    }

    _.$slider.trigger('setPosition', [_]);
  };

  Slick.prototype.setProps = function () {
    var _ = this,
        bodyStyle = document.body.style;

    _.positionProp = _.options.vertical === true ? 'top' : 'left';

    if (_.positionProp === 'top') {
      _.$slider.addClass('slick-vertical');
    } else {
      _.$slider.removeClass('slick-vertical');
    }

    if (bodyStyle.WebkitTransition !== undefined || bodyStyle.MozTransition !== undefined || bodyStyle.msTransition !== undefined) {
      if (_.options.useCSS === true) {
        _.cssTransitions = true;
      }
    }

    if (_.options.fade) {
      if (typeof _.options.zIndex === 'number') {
        if (_.options.zIndex < 3) {
          _.options.zIndex = 3;
        }
      } else {
        _.options.zIndex = _.defaults.zIndex;
      }
    }

    if (bodyStyle.OTransform !== undefined) {
      _.animType = 'OTransform';
      _.transformType = '-o-transform';
      _.transitionType = 'OTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.MozTransform !== undefined) {
      _.animType = 'MozTransform';
      _.transformType = '-moz-transform';
      _.transitionType = 'MozTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.webkitTransform !== undefined) {
      _.animType = 'webkitTransform';
      _.transformType = '-webkit-transform';
      _.transitionType = 'webkitTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
    }

    if (bodyStyle.msTransform !== undefined) {
      _.animType = 'msTransform';
      _.transformType = '-ms-transform';
      _.transitionType = 'msTransition';
      if (bodyStyle.msTransform === undefined) _.animType = false;
    }

    if (bodyStyle.transform !== undefined && _.animType !== false) {
      _.animType = 'transform';
      _.transformType = 'transform';
      _.transitionType = 'transition';
    }

    _.transformsEnabled = _.options.useTransform && _.animType !== null && _.animType !== false;
  };

  Slick.prototype.setSlideClasses = function (index) {
    var _ = this,
        centerOffset,
        allSlides,
        indexOffset,
        remainder;

    allSlides = _.$slider.find('.slick-slide').removeClass('slick-active slick-center slick-current').attr('aria-hidden', 'true');

    _.$slides.eq(index).addClass('slick-current');

    if (_.options.centerMode === true) {
      centerOffset = Math.floor(_.options.slidesToShow / 2);

      if (_.options.infinite === true) {
        if (index >= centerOffset && index <= _.slideCount - 1 - centerOffset) {
          _.$slides.slice(index - centerOffset, index + centerOffset + 1).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          indexOffset = _.options.slidesToShow + index;
          allSlides.slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2).addClass('slick-active').attr('aria-hidden', 'false');
        }

        if (index === 0) {
          allSlides.eq(allSlides.length - 1 - _.options.slidesToShow).addClass('slick-center');
        } else if (index === _.slideCount - 1) {
          allSlides.eq(_.options.slidesToShow).addClass('slick-center');
        }
      }

      _.$slides.eq(index).addClass('slick-center');
    } else {
      if (index >= 0 && index <= _.slideCount - _.options.slidesToShow) {
        _.$slides.slice(index, index + _.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
      } else if (allSlides.length <= _.options.slidesToShow) {
        allSlides.addClass('slick-active').attr('aria-hidden', 'false');
      } else {
        remainder = _.slideCount % _.options.slidesToShow;
        indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

        if (_.options.slidesToShow == _.options.slidesToScroll && _.slideCount - index < _.options.slidesToShow) {
          allSlides.slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          allSlides.slice(indexOffset, indexOffset + _.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
        }
      }
    }

    if (_.options.lazyLoad === 'ondemand') {
      _.lazyLoad();
    }
  };

  Slick.prototype.setupInfinite = function () {
    var _ = this,
        i,
        slideIndex,
        infiniteCount;

    if (_.options.fade === true) {
      _.options.centerMode = false;
    }

    if (_.options.infinite === true && _.options.fade === false) {
      slideIndex = null;

      if (_.slideCount > _.options.slidesToShow) {
        if (_.options.centerMode === true) {
          infiniteCount = _.options.slidesToShow + 1;
        } else {
          infiniteCount = _.options.slidesToShow;
        }

        for (i = _.slideCount; i > _.slideCount - infiniteCount; i -= 1) {
          slideIndex = i - 1;
          $(_.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex - _.slideCount).prependTo(_.$slideTrack).addClass('slick-cloned');
        }

        for (i = 0; i < infiniteCount; i += 1) {
          slideIndex = i;
          $(_.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex + _.slideCount).appendTo(_.$slideTrack).addClass('slick-cloned');
        }

        _.$slideTrack.find('.slick-cloned').find('[id]').each(function () {
          $(this).attr('id', '');
        });
      }
    }
  };

  Slick.prototype.interrupt = function (toggle) {
    var _ = this;

    if (!toggle) {
      _.autoPlay();
    }

    _.interrupted = toggle;
  };

  Slick.prototype.selectHandler = function (event) {
    var _ = this;

    var targetElement = $(event.target).is('.slick-slide') ? $(event.target) : $(event.target).parents('.slick-slide');
    var index = parseInt(targetElement.attr('data-slick-index'));
    if (!index) index = 0;

    if (_.slideCount <= _.options.slidesToShow) {
      _.setSlideClasses(index);

      _.asNavFor(index);

      return;
    }

    _.slideHandler(index);
  };

  Slick.prototype.slideHandler = function (index, sync, dontAnimate) {
    var targetSlide,
        animSlide,
        oldSlide,
        slideLeft,
        targetLeft = null,
        _ = this,
        navTarget;

    sync = sync || false;

    if (_.animating === true && _.options.waitForAnimate === true) {
      return;
    }

    if (_.options.fade === true && _.currentSlide === index) {
      return;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      return;
    }

    if (sync === false) {
      _.asNavFor(index);
    }

    targetSlide = index;
    targetLeft = _.getLeft(targetSlide);
    slideLeft = _.getLeft(_.currentSlide);
    _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

    if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;

        if (dontAnimate !== true) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }

      return;
    } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > _.slideCount - _.options.slidesToScroll)) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;

        if (dontAnimate !== true) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }

      return;
    }

    if (_.options.autoplay) {
      clearInterval(_.autoPlayTimer);
    }

    if (targetSlide < 0) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = _.slideCount - _.slideCount % _.options.slidesToScroll;
      } else {
        animSlide = _.slideCount + targetSlide;
      }
    } else if (targetSlide >= _.slideCount) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = 0;
      } else {
        animSlide = targetSlide - _.slideCount;
      }
    } else {
      animSlide = targetSlide;
    }

    _.animating = true;

    _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

    oldSlide = _.currentSlide;
    _.currentSlide = animSlide;

    _.setSlideClasses(_.currentSlide);

    if (_.options.asNavFor) {
      navTarget = _.getNavTarget();
      navTarget = navTarget.slick('getSlick');

      if (navTarget.slideCount <= navTarget.options.slidesToShow) {
        navTarget.setSlideClasses(_.currentSlide);
      }
    }

    _.updateDots();

    _.updateArrows();

    if (_.options.fade === true) {
      if (dontAnimate !== true) {
        _.fadeSlideOut(oldSlide);

        _.fadeSlide(animSlide, function () {
          _.postSlide(animSlide);
        });
      } else {
        _.postSlide(animSlide);
      }

      _.animateHeight();

      return;
    }

    if (dontAnimate !== true) {
      _.animateSlide(targetLeft, function () {
        _.postSlide(animSlide);
      });
    } else {
      _.postSlide(animSlide);
    }
  };

  Slick.prototype.startLoad = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.hide();

      _.$nextArrow.hide();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.hide();
    }

    _.$slider.addClass('slick-loading');
  };

  Slick.prototype.swipeDirection = function () {
    var xDist,
        yDist,
        r,
        swipeAngle,
        _ = this;

    xDist = _.touchObject.startX - _.touchObject.curX;
    yDist = _.touchObject.startY - _.touchObject.curY;
    r = Math.atan2(yDist, xDist);
    swipeAngle = Math.round(r * 180 / Math.PI);

    if (swipeAngle < 0) {
      swipeAngle = 360 - Math.abs(swipeAngle);
    }

    if (swipeAngle <= 45 && swipeAngle >= 0) {
      return _.options.rtl === false ? 'left' : 'right';
    }

    if (swipeAngle <= 360 && swipeAngle >= 315) {
      return _.options.rtl === false ? 'left' : 'right';
    }

    if (swipeAngle >= 135 && swipeAngle <= 225) {
      return _.options.rtl === false ? 'right' : 'left';
    }

    if (_.options.verticalSwiping === true) {
      if (swipeAngle >= 35 && swipeAngle <= 135) {
        return 'down';
      } else {
        return 'up';
      }
    }

    return 'vertical';
  };

  Slick.prototype.swipeEnd = function (event) {
    var _ = this,
        slideCount,
        direction;

    _.dragging = false;
    _.interrupted = false;
    _.shouldClick = _.touchObject.swipeLength > 10 ? false : true;

    if (_.touchObject.curX === undefined) {
      return false;
    }

    if (_.touchObject.edgeHit === true) {
      _.$slider.trigger('edge', [_, _.swipeDirection()]);
    }

    if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {
      direction = _.swipeDirection();

      switch (direction) {
        case 'left':
        case 'down':
          slideCount = _.options.swipeToSlide ? _.checkNavigable(_.currentSlide + _.getSlideCount()) : _.currentSlide + _.getSlideCount();
          _.currentDirection = 0;
          break;

        case 'right':
        case 'up':
          slideCount = _.options.swipeToSlide ? _.checkNavigable(_.currentSlide - _.getSlideCount()) : _.currentSlide - _.getSlideCount();
          _.currentDirection = 1;
          break;

        default:
      }

      if (direction != 'vertical') {
        _.slideHandler(slideCount);

        _.touchObject = {};

        _.$slider.trigger('swipe', [_, direction]);
      }
    } else {
      if (_.touchObject.startX !== _.touchObject.curX) {
        _.slideHandler(_.currentSlide);

        _.touchObject = {};
      }
    }
  };

  Slick.prototype.swipeHandler = function (event) {
    var _ = this;

    if (_.options.swipe === false || 'ontouchend' in document && _.options.swipe === false) {
      return;
    } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
      return;
    }

    _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ? event.originalEvent.touches.length : 1;
    _.touchObject.minSwipe = _.listWidth / _.options.touchThreshold;

    if (_.options.verticalSwiping === true) {
      _.touchObject.minSwipe = _.listHeight / _.options.touchThreshold;
    }

    switch (event.data.action) {
      case 'start':
        _.swipeStart(event);

        break;

      case 'move':
        _.swipeMove(event);

        break;

      case 'end':
        _.swipeEnd(event);

        break;
    }
  };

  Slick.prototype.swipeMove = function (event) {
    var _ = this,
        edgeWasHit = false,
        curLeft,
        swipeDirection,
        swipeLength,
        positionOffset,
        touches;

    touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

    if (!_.dragging || touches && touches.length !== 1) {
      return false;
    }

    curLeft = _.getLeft(_.currentSlide);
    _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
    _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;
    _.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

    if (_.options.verticalSwiping === true) {
      _.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));
    }

    swipeDirection = _.swipeDirection();

    if (swipeDirection === 'vertical') {
      return;
    }

    if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
      event.preventDefault();
    }

    positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);

    if (_.options.verticalSwiping === true) {
      positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
    }

    swipeLength = _.touchObject.swipeLength;
    _.touchObject.edgeHit = false;

    if (_.options.infinite === false) {
      if (_.currentSlide === 0 && swipeDirection === 'right' || _.currentSlide >= _.getDotCount() && swipeDirection === 'left') {
        swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
        _.touchObject.edgeHit = true;
      }
    }

    if (_.options.vertical === false) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    } else {
      _.swipeLeft = curLeft + swipeLength * (_.$list.height() / _.listWidth) * positionOffset;
    }

    if (_.options.verticalSwiping === true) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    }

    if (_.options.fade === true || _.options.touchMove === false) {
      return false;
    }

    if (_.animating === true) {
      _.swipeLeft = null;
      return false;
    }

    _.setCSS(_.swipeLeft);
  };

  Slick.prototype.swipeStart = function (event) {
    var _ = this,
        touches;

    _.interrupted = true;

    if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
      _.touchObject = {};
      return false;
    }

    if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
      touches = event.originalEvent.touches[0];
    }

    _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
    _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;
    _.dragging = true;
  };

  Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function () {
    var _ = this;

    if (_.$slidesCache !== null) {
      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.unload = function () {
    var _ = this;

    $('.slick-cloned', _.$slider).remove();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
      _.$prevArrow.remove();
    }

    if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
      _.$nextArrow.remove();
    }

    _.$slides.removeClass('slick-slide slick-active slick-visible slick-current').attr('aria-hidden', 'true').css('width', '');
  };

  Slick.prototype.unslick = function (fromBreakpoint) {
    var _ = this;

    _.$slider.trigger('unslick', [_, fromBreakpoint]);

    _.destroy();
  };

  Slick.prototype.updateArrows = function () {
    var _ = this,
        centerOffset;

    centerOffset = Math.floor(_.options.slidesToShow / 2);

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow && !_.options.infinite) {
      _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

      _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

      if (_.currentSlide === 0) {
        _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {
        _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {
        _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');

        _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      }
    }
  };

  Slick.prototype.updateDots = function () {
    var _ = this;

    if (_.$dots !== null) {
      _.$dots.find('li').removeClass('slick-active').attr('aria-hidden', 'true');

      _.$dots.find('li').eq(Math.floor(_.currentSlide / _.options.slidesToScroll)).addClass('slick-active').attr('aria-hidden', 'false');
    }
  };

  Slick.prototype.visibility = function () {
    var _ = this;

    if (_.options.autoplay) {
      if (document[_.hidden]) {
        _.interrupted = true;
      } else {
        _.interrupted = false;
      }
    }
  };

  $.fn.slick = function () {
    var _ = this,
        opt = arguments[0],
        args = Array.prototype.slice.call(arguments, 1),
        l = _.length,
        i,
        ret;

    for (i = 0; i < l; i++) {
      if (typeof opt == 'object' || typeof opt == 'undefined') _[i].slick = new Slick(_[i], opt);else ret = _[i].slick[opt].apply(_[i].slick, args);
      if (typeof ret != 'undefined') return ret;
    }

    return _;
  };
});
;
;
;
;
;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vYmlsZS1tZW51LmpzIiwic2xpY2suanMiLCJmb290ZXIuanMiLCJnZW5lcmFsLmpzIiwiaGVhZGVyLmpzIiwiaW5pdC5qcyJdLCJuYW1lcyI6WyIkIiwiZm4iLCJtb2JpbGVfbWVudSIsIm9wdGlvbnMiLCJkZWZhdWx0cyIsInNpZGUiLCJzcGVlZCIsInRvZ2dsZXIiLCJ3aWR0aCIsImhlaWdodCIsImJ1dHRvblN0eWxlcyIsImluaXRhbHMiLCJ0b2dnbGVCdXR0b24iLCJtZW51V3JhcCIsIiR0b2dnbGVyIiwib2ZmY2FudmFzIiwiJHRoaXMiLCJzZXR0aW5ncyIsImV4dGVuZCIsImJlZm9yZSIsImFkZENsYXNzIiwic3R5bGVzIiwiZWFjaCIsIndyYXAiLCJwYXJlbnQiLCJjc3MiLCJjb25zb2xlIiwibG9nIiwib24iLCJ0b2dnbGVDbGFzcyIsImNsb3Nlc3QiLCJqUXVlcnkiLCJmYWN0b3J5IiwiZGVmaW5lIiwiYW1kIiwiZXhwb3J0cyIsIm1vZHVsZSIsInJlcXVpcmUiLCJTbGljayIsIndpbmRvdyIsImluc3RhbmNlVWlkIiwiZWxlbWVudCIsIl8iLCJkYXRhU2V0dGluZ3MiLCJhY2Nlc3NpYmlsaXR5IiwiYWRhcHRpdmVIZWlnaHQiLCJhcHBlbmRBcnJvd3MiLCJhcHBlbmREb3RzIiwiYXJyb3dzIiwiYXNOYXZGb3IiLCJwcmV2QXJyb3ciLCJuZXh0QXJyb3ciLCJhdXRvcGxheSIsImF1dG9wbGF5U3BlZWQiLCJjZW50ZXJNb2RlIiwiY2VudGVyUGFkZGluZyIsImNzc0Vhc2UiLCJjdXN0b21QYWdpbmciLCJzbGlkZXIiLCJpIiwidGV4dCIsImRvdHMiLCJkb3RzQ2xhc3MiLCJkcmFnZ2FibGUiLCJlYXNpbmciLCJlZGdlRnJpY3Rpb24iLCJmYWRlIiwiZm9jdXNPblNlbGVjdCIsImluZmluaXRlIiwiaW5pdGlhbFNsaWRlIiwibGF6eUxvYWQiLCJtb2JpbGVGaXJzdCIsInBhdXNlT25Ib3ZlciIsInBhdXNlT25Gb2N1cyIsInBhdXNlT25Eb3RzSG92ZXIiLCJyZXNwb25kVG8iLCJyZXNwb25zaXZlIiwicm93cyIsInJ0bCIsInNsaWRlIiwic2xpZGVzUGVyUm93Iiwic2xpZGVzVG9TaG93Iiwic2xpZGVzVG9TY3JvbGwiLCJzd2lwZSIsInN3aXBlVG9TbGlkZSIsInRvdWNoTW92ZSIsInRvdWNoVGhyZXNob2xkIiwidXNlQ1NTIiwidXNlVHJhbnNmb3JtIiwidmFyaWFibGVXaWR0aCIsInZlcnRpY2FsIiwidmVydGljYWxTd2lwaW5nIiwid2FpdEZvckFuaW1hdGUiLCJ6SW5kZXgiLCJpbml0aWFscyIsImFuaW1hdGluZyIsImRyYWdnaW5nIiwiYXV0b1BsYXlUaW1lciIsImN1cnJlbnREaXJlY3Rpb24iLCJjdXJyZW50TGVmdCIsImN1cnJlbnRTbGlkZSIsImRpcmVjdGlvbiIsIiRkb3RzIiwibGlzdFdpZHRoIiwibGlzdEhlaWdodCIsImxvYWRJbmRleCIsIiRuZXh0QXJyb3ciLCIkcHJldkFycm93Iiwic2xpZGVDb3VudCIsInNsaWRlV2lkdGgiLCIkc2xpZGVUcmFjayIsIiRzbGlkZXMiLCJzbGlkaW5nIiwic2xpZGVPZmZzZXQiLCJzd2lwZUxlZnQiLCIkbGlzdCIsInRvdWNoT2JqZWN0IiwidHJhbnNmb3Jtc0VuYWJsZWQiLCJ1bnNsaWNrZWQiLCJhY3RpdmVCcmVha3BvaW50IiwiYW5pbVR5cGUiLCJhbmltUHJvcCIsImJyZWFrcG9pbnRzIiwiYnJlYWtwb2ludFNldHRpbmdzIiwiY3NzVHJhbnNpdGlvbnMiLCJmb2N1c3NlZCIsImludGVycnVwdGVkIiwiaGlkZGVuIiwicGF1c2VkIiwicG9zaXRpb25Qcm9wIiwicm93Q291bnQiLCJzaG91bGRDbGljayIsIiRzbGlkZXIiLCIkc2xpZGVzQ2FjaGUiLCJ0cmFuc2Zvcm1UeXBlIiwidHJhbnNpdGlvblR5cGUiLCJ2aXNpYmlsaXR5Q2hhbmdlIiwid2luZG93V2lkdGgiLCJ3aW5kb3dUaW1lciIsImRhdGEiLCJvcmlnaW5hbFNldHRpbmdzIiwiZG9jdW1lbnQiLCJtb3pIaWRkZW4iLCJ3ZWJraXRIaWRkZW4iLCJhdXRvUGxheSIsInByb3h5IiwiYXV0b1BsYXlDbGVhciIsImF1dG9QbGF5SXRlcmF0b3IiLCJjaGFuZ2VTbGlkZSIsImNsaWNrSGFuZGxlciIsInNlbGVjdEhhbmRsZXIiLCJzZXRQb3NpdGlvbiIsInN3aXBlSGFuZGxlciIsImRyYWdIYW5kbGVyIiwia2V5SGFuZGxlciIsImh0bWxFeHByIiwicmVnaXN0ZXJCcmVha3BvaW50cyIsImluaXQiLCJwcm90b3R5cGUiLCJhY3RpdmF0ZUFEQSIsImZpbmQiLCJhdHRyIiwiYWRkU2xpZGUiLCJzbGlja0FkZCIsIm1hcmt1cCIsImluZGV4IiwiYWRkQmVmb3JlIiwidW5sb2FkIiwibGVuZ3RoIiwiYXBwZW5kVG8iLCJpbnNlcnRCZWZvcmUiLCJlcSIsImluc2VydEFmdGVyIiwicHJlcGVuZFRvIiwiY2hpbGRyZW4iLCJkZXRhY2giLCJhcHBlbmQiLCJyZWluaXQiLCJhbmltYXRlSGVpZ2h0IiwidGFyZ2V0SGVpZ2h0Iiwib3V0ZXJIZWlnaHQiLCJhbmltYXRlIiwiYW5pbWF0ZVNsaWRlIiwidGFyZ2V0TGVmdCIsImNhbGxiYWNrIiwiYW5pbVByb3BzIiwibGVmdCIsInRvcCIsImFuaW1TdGFydCIsImR1cmF0aW9uIiwic3RlcCIsIm5vdyIsIk1hdGgiLCJjZWlsIiwiY29tcGxldGUiLCJjYWxsIiwiYXBwbHlUcmFuc2l0aW9uIiwic2V0VGltZW91dCIsImRpc2FibGVUcmFuc2l0aW9uIiwiZ2V0TmF2VGFyZ2V0Iiwibm90IiwidGFyZ2V0Iiwic2xpY2siLCJzbGlkZUhhbmRsZXIiLCJ0cmFuc2l0aW9uIiwic2V0SW50ZXJ2YWwiLCJjbGVhckludGVydmFsIiwic2xpZGVUbyIsImJ1aWxkQXJyb3dzIiwicmVtb3ZlQ2xhc3MiLCJyZW1vdmVBdHRyIiwidGVzdCIsImFkZCIsImJ1aWxkRG90cyIsImRvdCIsImdldERvdENvdW50IiwiZmlyc3QiLCJidWlsZE91dCIsIndyYXBBbGwiLCJzZXR1cEluZmluaXRlIiwidXBkYXRlRG90cyIsInNldFNsaWRlQ2xhc3NlcyIsImJ1aWxkUm93cyIsImEiLCJiIiwiYyIsIm5ld1NsaWRlcyIsIm51bU9mU2xpZGVzIiwib3JpZ2luYWxTbGlkZXMiLCJzbGlkZXNQZXJTZWN0aW9uIiwiY3JlYXRlRG9jdW1lbnRGcmFnbWVudCIsImNyZWF0ZUVsZW1lbnQiLCJyb3ciLCJnZXQiLCJhcHBlbmRDaGlsZCIsImVtcHR5IiwiY2hlY2tSZXNwb25zaXZlIiwiaW5pdGlhbCIsImZvcmNlVXBkYXRlIiwiYnJlYWtwb2ludCIsInRhcmdldEJyZWFrcG9pbnQiLCJyZXNwb25kVG9XaWR0aCIsInRyaWdnZXJCcmVha3BvaW50Iiwic2xpZGVyV2lkdGgiLCJpbm5lcldpZHRoIiwibWluIiwiaGFzT3duUHJvcGVydHkiLCJ1bnNsaWNrIiwicmVmcmVzaCIsInRyaWdnZXIiLCJldmVudCIsImRvbnRBbmltYXRlIiwiJHRhcmdldCIsImN1cnJlbnRUYXJnZXQiLCJpbmRleE9mZnNldCIsInVuZXZlbk9mZnNldCIsImlzIiwicHJldmVudERlZmF1bHQiLCJtZXNzYWdlIiwiY2hlY2tOYXZpZ2FibGUiLCJuYXZpZ2FibGVzIiwicHJldk5hdmlnYWJsZSIsImdldE5hdmlnYWJsZUluZGV4ZXMiLCJuIiwiY2xlYW5VcEV2ZW50cyIsIm9mZiIsImludGVycnVwdCIsInZpc2liaWxpdHkiLCJjbGVhblVwU2xpZGVFdmVudHMiLCJvcmllbnRhdGlvbkNoYW5nZSIsInJlc2l6ZSIsImNsZWFuVXBSb3dzIiwic3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uIiwic3RvcFByb3BhZ2F0aW9uIiwiZGVzdHJveSIsInJlbW92ZSIsImZhZGVTbGlkZSIsInNsaWRlSW5kZXgiLCJvcGFjaXR5IiwiZmFkZVNsaWRlT3V0IiwiZmlsdGVyU2xpZGVzIiwic2xpY2tGaWx0ZXIiLCJmaWx0ZXIiLCJmb2N1c0hhbmRsZXIiLCIkc2YiLCJnZXRDdXJyZW50Iiwic2xpY2tDdXJyZW50U2xpZGUiLCJicmVha1BvaW50IiwiY291bnRlciIsInBhZ2VyUXR5IiwiZ2V0TGVmdCIsInZlcnRpY2FsSGVpZ2h0IiwidmVydGljYWxPZmZzZXQiLCJ0YXJnZXRTbGlkZSIsImZsb29yIiwib2Zmc2V0TGVmdCIsIm91dGVyV2lkdGgiLCJnZXRPcHRpb24iLCJzbGlja0dldE9wdGlvbiIsIm9wdGlvbiIsImluZGV4ZXMiLCJtYXgiLCJwdXNoIiwiZ2V0U2xpY2siLCJnZXRTbGlkZUNvdW50Iiwic2xpZGVzVHJhdmVyc2VkIiwic3dpcGVkU2xpZGUiLCJjZW50ZXJPZmZzZXQiLCJhYnMiLCJnb1RvIiwic2xpY2tHb1RvIiwicGFyc2VJbnQiLCJjcmVhdGlvbiIsImhhc0NsYXNzIiwic2V0UHJvcHMiLCJzdGFydExvYWQiLCJsb2FkU2xpZGVyIiwiaW5pdGlhbGl6ZUV2ZW50cyIsInVwZGF0ZUFycm93cyIsImluaXRBREEiLCJlbmQiLCJpbml0QXJyb3dFdmVudHMiLCJpbml0RG90RXZlbnRzIiwiaW5pdFNsaWRlRXZlbnRzIiwiYWN0aW9uIiwiaW5pdFVJIiwic2hvdyIsInRhZ05hbWUiLCJtYXRjaCIsImtleUNvZGUiLCJsb2FkUmFuZ2UiLCJjbG9uZVJhbmdlIiwicmFuZ2VTdGFydCIsInJhbmdlRW5kIiwibG9hZEltYWdlcyIsImltYWdlc1Njb3BlIiwiaW1hZ2UiLCJpbWFnZVNvdXJjZSIsImltYWdlVG9Mb2FkIiwib25sb2FkIiwib25lcnJvciIsInNyYyIsInNsaWNlIiwicHJvZ3Jlc3NpdmVMYXp5TG9hZCIsIm5leHQiLCJzbGlja05leHQiLCJwYXVzZSIsInNsaWNrUGF1c2UiLCJwbGF5Iiwic2xpY2tQbGF5IiwicG9zdFNsaWRlIiwicHJldiIsInNsaWNrUHJldiIsInRyeUNvdW50IiwiJGltZ3NUb0xvYWQiLCJpbml0aWFsaXppbmciLCJsYXN0VmlzaWJsZUluZGV4IiwiY3VycmVudEJyZWFrcG9pbnQiLCJsIiwicmVzcG9uc2l2ZVNldHRpbmdzIiwidHlwZSIsInNwbGljZSIsInNvcnQiLCJjbGVhclRpbWVvdXQiLCJ3aW5kb3dEZWxheSIsInJlbW92ZVNsaWRlIiwic2xpY2tSZW1vdmUiLCJyZW1vdmVCZWZvcmUiLCJyZW1vdmVBbGwiLCJzZXRDU1MiLCJwb3NpdGlvbiIsInBvc2l0aW9uUHJvcHMiLCJ4IiwieSIsInNldERpbWVuc2lvbnMiLCJwYWRkaW5nIiwib2Zmc2V0Iiwic2V0RmFkZSIsInJpZ2h0Iiwic2V0SGVpZ2h0Iiwic2V0T3B0aW9uIiwic2xpY2tTZXRPcHRpb24iLCJpdGVtIiwidmFsdWUiLCJhcmd1bWVudHMiLCJvcHQiLCJ2YWwiLCJib2R5U3R5bGUiLCJib2R5Iiwic3R5bGUiLCJXZWJraXRUcmFuc2l0aW9uIiwidW5kZWZpbmVkIiwiTW96VHJhbnNpdGlvbiIsIm1zVHJhbnNpdGlvbiIsIk9UcmFuc2Zvcm0iLCJwZXJzcGVjdGl2ZVByb3BlcnR5Iiwid2Via2l0UGVyc3BlY3RpdmUiLCJNb3pUcmFuc2Zvcm0iLCJNb3pQZXJzcGVjdGl2ZSIsIndlYmtpdFRyYW5zZm9ybSIsIm1zVHJhbnNmb3JtIiwidHJhbnNmb3JtIiwiYWxsU2xpZGVzIiwicmVtYWluZGVyIiwiaW5maW5pdGVDb3VudCIsImNsb25lIiwidG9nZ2xlIiwidGFyZ2V0RWxlbWVudCIsInBhcmVudHMiLCJzeW5jIiwiYW5pbVNsaWRlIiwib2xkU2xpZGUiLCJzbGlkZUxlZnQiLCJuYXZUYXJnZXQiLCJoaWRlIiwic3dpcGVEaXJlY3Rpb24iLCJ4RGlzdCIsInlEaXN0IiwiciIsInN3aXBlQW5nbGUiLCJzdGFydFgiLCJjdXJYIiwic3RhcnRZIiwiY3VyWSIsImF0YW4yIiwicm91bmQiLCJQSSIsInN3aXBlRW5kIiwic3dpcGVMZW5ndGgiLCJlZGdlSGl0IiwibWluU3dpcGUiLCJpbmRleE9mIiwiZmluZ2VyQ291bnQiLCJvcmlnaW5hbEV2ZW50IiwidG91Y2hlcyIsInN3aXBlU3RhcnQiLCJzd2lwZU1vdmUiLCJlZGdlV2FzSGl0IiwiY3VyTGVmdCIsInBvc2l0aW9uT2Zmc2V0IiwicGFnZVgiLCJjbGllbnRYIiwicGFnZVkiLCJjbGllbnRZIiwic3FydCIsInBvdyIsInVuZmlsdGVyU2xpZGVzIiwic2xpY2tVbmZpbHRlciIsImZyb21CcmVha3BvaW50IiwiYXJncyIsIkFycmF5IiwicmV0IiwiYXBwbHkiXSwibWFwcGluZ3MiOiJBQUFBLENBQUMsVUFBU0EsQ0FBVCxFQUFXO0FBRVo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0NFQSxFQUFBQSxDQUFDLENBQUNDLEVBQUYsQ0FBS0MsV0FBTCxHQUFtQixVQUFXQyxPQUFYLEVBQXFCO0FBRXRDO0FBQ0EsUUFBSUMsUUFBUSxHQUFHO0FBQ2JDLE1BQUFBLElBQUksRUFBRSxNQURPO0FBRWJDLE1BQUFBLEtBQUssRUFBRSxNQUZNO0FBR2JDLE1BQUFBLE9BQU8sRUFBRSxjQUhJO0FBSWJDLE1BQUFBLEtBQUssRUFBRSxNQUpNO0FBS2JDLE1BQUFBLE1BQU0sRUFBRSxNQUxLO0FBTWJDLE1BQUFBLFlBQVksRUFBRTtBQU5ELEtBQWY7QUFTQSxRQUFJQyxPQUFPLEdBQUc7QUFDWkMsTUFBQUEsWUFBWSxFQUFFLGlDQURGO0FBRVpDLE1BQUFBLFFBQVEsRUFBRTtBQUZFLEtBQWQ7QUFLQSxRQUFJQyxRQUFKO0FBQ0EsUUFBSUMsU0FBSjtBQUVBLFFBQUlDLEtBQUssR0FBR2hCLENBQUMsQ0FBQyxJQUFELENBQWI7QUFFQSxRQUFJaUIsUUFBUSxHQUFHakIsQ0FBQyxDQUFDa0IsTUFBRixDQUFVLEVBQVYsRUFBY1AsT0FBZCxFQUF1QlAsUUFBdkIsRUFBaUNELE9BQWpDLENBQWY7O0FBRUEsUUFBR2MsUUFBUSxDQUFDVixPQUFULElBQW9CSCxRQUFRLENBQUNHLE9BQWhDLEVBQXdDO0FBQ3RDUyxNQUFBQSxLQUFLLENBQUNHLE1BQU4sQ0FBYUYsUUFBUSxDQUFDTCxZQUF0QjtBQUNEO0FBRUQ7OztBQUNBRSxJQUFBQSxRQUFRLEdBQUdkLENBQUMsQ0FBQ2lCLFFBQVEsQ0FBQ1YsT0FBVixDQUFaOztBQUNBLFFBQUdVLFFBQVEsQ0FBQ1AsWUFBWixFQUF5QjtBQUN2QkksTUFBQUEsUUFBUSxDQUFDTSxRQUFULENBQWtCLGtCQUFsQjtBQUNEOztBQUVELFlBQVNILFFBQVEsQ0FBQ1osSUFBbEI7QUFDRSxXQUFLLE1BQUw7QUFDRVUsUUFBQUEsU0FBUyxHQUFHLG1CQUFaO0FBQ0E7O0FBQ0YsV0FBSyxLQUFMO0FBQ0VBLFFBQUFBLFNBQVMsR0FBRyxtQkFBWjtBQUNBOztBQUNGLFdBQUssT0FBTDtBQUNFQSxRQUFBQSxTQUFTLEdBQUcsa0JBQVo7QUFDQTs7QUFDRixXQUFLLFFBQUw7QUFDRUEsUUFBQUEsU0FBUyxHQUFHLGtCQUFaO0FBQ0E7QUFaSjs7QUFlQSxRQUFJTSxNQUFNLEdBQUc7QUFDWCxtQkFBY04sU0FESDtBQUVYLG9CQUFlRSxRQUFRLENBQUNYLEtBRmI7QUFHWEcsTUFBQUEsTUFBTSxFQUFHUSxRQUFRLENBQUNSLE1BSFA7QUFJWEQsTUFBQUEsS0FBSyxFQUFHUyxRQUFRLENBQUNUO0FBSk4sS0FBYjtBQU9BLFdBQU8sS0FBS2MsSUFBTCxDQUFVLFlBQVc7QUFFeEJOLE1BQUFBLEtBQUssQ0FBQ0ksUUFBTixDQUFlLGdCQUFmLEVBQWlDRyxJQUFqQyxDQUFzQ04sUUFBUSxDQUFDSixRQUEvQztBQUNBRyxNQUFBQSxLQUFLLENBQUNRLE1BQU4sR0FBZUMsR0FBZixDQUFvQkosTUFBcEI7QUFDQUssTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVliLFFBQVo7QUFDQUEsTUFBQUEsUUFBUSxDQUFDYyxFQUFULENBQVksT0FBWixFQUFxQixZQUFVO0FBQzdCRixRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxZQUFaO0FBRUEzQixRQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVE2QixXQUFSLENBQW9CLGtCQUFwQjtBQUNBSCxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWVgsS0FBWjtBQUNBQSxRQUFBQSxLQUFLLENBQUNjLE9BQU4sQ0FBYyxzQkFBZCxFQUFzQ0QsV0FBdEMsQ0FBa0QsZ0JBQWxEO0FBRUQsT0FQRDtBQVVILEtBZk0sQ0FBUDtBQWdCRCxHQXhFRDs7QUEyRUE3QixFQUFBQSxDQUFDLENBQUMsY0FBRCxDQUFELENBQWtCRSxXQUFsQixDQUE4QjtBQUM1QkcsSUFBQUEsSUFBSSxFQUFFO0FBRHNCLEdBQTlCO0FBS0QsQ0F0SEQsRUFzSEcwQixNQXRISDtDQ0FBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTtBQUNDLFdBQVNDLE9BQVQsRUFBa0I7QUFDZjs7QUFDQSxNQUFJLE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0NBLE1BQU0sQ0FBQ0MsR0FBM0MsRUFBZ0Q7QUFDNUNELElBQUFBLE1BQU0sQ0FBQyxDQUFDLFFBQUQsQ0FBRCxFQUFhRCxPQUFiLENBQU47QUFDSCxHQUZELE1BRU8sSUFBSSxPQUFPRyxPQUFQLEtBQW1CLFdBQXZCLEVBQW9DO0FBQ3ZDQyxJQUFBQSxNQUFNLENBQUNELE9BQVAsR0FBaUJILE9BQU8sQ0FBQ0ssT0FBTyxDQUFDLFFBQUQsQ0FBUixDQUF4QjtBQUNILEdBRk0sTUFFQTtBQUNITCxJQUFBQSxPQUFPLENBQUNELE1BQUQsQ0FBUDtBQUNIO0FBRUosQ0FWQSxFQVVDLFVBQVMvQixDQUFULEVBQVk7QUFDVjs7QUFDQSxNQUFJc0MsS0FBSyxHQUFHQyxNQUFNLENBQUNELEtBQVAsSUFBZ0IsRUFBNUI7O0FBRUFBLEVBQUFBLEtBQUssR0FBSSxZQUFXO0FBRWhCLFFBQUlFLFdBQVcsR0FBRyxDQUFsQjs7QUFFQSxhQUFTRixLQUFULENBQWVHLE9BQWYsRUFBd0J4QixRQUF4QixFQUFrQztBQUU5QixVQUFJeUIsQ0FBQyxHQUFHLElBQVI7QUFBQSxVQUFjQyxZQUFkOztBQUVBRCxNQUFBQSxDQUFDLENBQUN0QyxRQUFGLEdBQWE7QUFDVHdDLFFBQUFBLGFBQWEsRUFBRSxJQUROO0FBRVRDLFFBQUFBLGNBQWMsRUFBRSxLQUZQO0FBR1RDLFFBQUFBLFlBQVksRUFBRTlDLENBQUMsQ0FBQ3lDLE9BQUQsQ0FITjtBQUlUTSxRQUFBQSxVQUFVLEVBQUUvQyxDQUFDLENBQUN5QyxPQUFELENBSko7QUFLVE8sUUFBQUEsTUFBTSxFQUFFLElBTEM7QUFNVEMsUUFBQUEsUUFBUSxFQUFFLElBTkQ7QUFPVEMsUUFBQUEsU0FBUyxFQUFFLDhIQVBGO0FBUVRDLFFBQUFBLFNBQVMsRUFBRSxzSEFSRjtBQVNUQyxRQUFBQSxRQUFRLEVBQUUsS0FURDtBQVVUQyxRQUFBQSxhQUFhLEVBQUUsSUFWTjtBQVdUQyxRQUFBQSxVQUFVLEVBQUUsS0FYSDtBQVlUQyxRQUFBQSxhQUFhLEVBQUUsTUFaTjtBQWFUQyxRQUFBQSxPQUFPLEVBQUUsTUFiQTtBQWNUQyxRQUFBQSxZQUFZLEVBQUUsVUFBU0MsTUFBVCxFQUFpQkMsQ0FBakIsRUFBb0I7QUFDOUIsaUJBQU8zRCxDQUFDLENBQUMsc0VBQUQsQ0FBRCxDQUEwRTRELElBQTFFLENBQStFRCxDQUFDLEdBQUcsQ0FBbkYsQ0FBUDtBQUNILFNBaEJRO0FBaUJURSxRQUFBQSxJQUFJLEVBQUUsS0FqQkc7QUFrQlRDLFFBQUFBLFNBQVMsRUFBRSxZQWxCRjtBQW1CVEMsUUFBQUEsU0FBUyxFQUFFLElBbkJGO0FBb0JUQyxRQUFBQSxNQUFNLEVBQUUsUUFwQkM7QUFxQlRDLFFBQUFBLFlBQVksRUFBRSxJQXJCTDtBQXNCVEMsUUFBQUEsSUFBSSxFQUFFLEtBdEJHO0FBdUJUQyxRQUFBQSxhQUFhLEVBQUUsS0F2Qk47QUF3QlRDLFFBQUFBLFFBQVEsRUFBRSxJQXhCRDtBQXlCVEMsUUFBQUEsWUFBWSxFQUFFLENBekJMO0FBMEJUQyxRQUFBQSxRQUFRLEVBQUUsVUExQkQ7QUEyQlRDLFFBQUFBLFdBQVcsRUFBRSxLQTNCSjtBQTRCVEMsUUFBQUEsWUFBWSxFQUFFLElBNUJMO0FBNkJUQyxRQUFBQSxZQUFZLEVBQUUsSUE3Qkw7QUE4QlRDLFFBQUFBLGdCQUFnQixFQUFFLEtBOUJUO0FBK0JUQyxRQUFBQSxTQUFTLEVBQUUsUUEvQkY7QUFnQ1RDLFFBQUFBLFVBQVUsRUFBRSxJQWhDSDtBQWlDVEMsUUFBQUEsSUFBSSxFQUFFLENBakNHO0FBa0NUQyxRQUFBQSxHQUFHLEVBQUUsS0FsQ0k7QUFtQ1RDLFFBQUFBLEtBQUssRUFBRSxFQW5DRTtBQW9DVEMsUUFBQUEsWUFBWSxFQUFFLENBcENMO0FBcUNUQyxRQUFBQSxZQUFZLEVBQUUsQ0FyQ0w7QUFzQ1RDLFFBQUFBLGNBQWMsRUFBRSxDQXRDUDtBQXVDVDVFLFFBQUFBLEtBQUssRUFBRSxHQXZDRTtBQXdDVDZFLFFBQUFBLEtBQUssRUFBRSxJQXhDRTtBQXlDVEMsUUFBQUEsWUFBWSxFQUFFLEtBekNMO0FBMENUQyxRQUFBQSxTQUFTLEVBQUUsSUExQ0Y7QUEyQ1RDLFFBQUFBLGNBQWMsRUFBRSxDQTNDUDtBQTRDVEMsUUFBQUEsTUFBTSxFQUFFLElBNUNDO0FBNkNUQyxRQUFBQSxZQUFZLEVBQUUsSUE3Q0w7QUE4Q1RDLFFBQUFBLGFBQWEsRUFBRSxLQTlDTjtBQStDVEMsUUFBQUEsUUFBUSxFQUFFLEtBL0NEO0FBZ0RUQyxRQUFBQSxlQUFlLEVBQUUsS0FoRFI7QUFpRFRDLFFBQUFBLGNBQWMsRUFBRSxJQWpEUDtBQWtEVEMsUUFBQUEsTUFBTSxFQUFFO0FBbERDLE9BQWI7QUFxREFuRCxNQUFBQSxDQUFDLENBQUNvRCxRQUFGLEdBQWE7QUFDVEMsUUFBQUEsU0FBUyxFQUFFLEtBREY7QUFFVEMsUUFBQUEsUUFBUSxFQUFFLEtBRkQ7QUFHVEMsUUFBQUEsYUFBYSxFQUFFLElBSE47QUFJVEMsUUFBQUEsZ0JBQWdCLEVBQUUsQ0FKVDtBQUtUQyxRQUFBQSxXQUFXLEVBQUUsSUFMSjtBQU1UQyxRQUFBQSxZQUFZLEVBQUUsQ0FOTDtBQU9UQyxRQUFBQSxTQUFTLEVBQUUsQ0FQRjtBQVFUQyxRQUFBQSxLQUFLLEVBQUUsSUFSRTtBQVNUQyxRQUFBQSxTQUFTLEVBQUUsSUFURjtBQVVUQyxRQUFBQSxVQUFVLEVBQUUsSUFWSDtBQVdUQyxRQUFBQSxTQUFTLEVBQUUsQ0FYRjtBQVlUQyxRQUFBQSxVQUFVLEVBQUUsSUFaSDtBQWFUQyxRQUFBQSxVQUFVLEVBQUUsSUFiSDtBQWNUQyxRQUFBQSxVQUFVLEVBQUUsSUFkSDtBQWVUQyxRQUFBQSxVQUFVLEVBQUUsSUFmSDtBQWdCVEMsUUFBQUEsV0FBVyxFQUFFLElBaEJKO0FBaUJUQyxRQUFBQSxPQUFPLEVBQUUsSUFqQkE7QUFrQlRDLFFBQUFBLE9BQU8sRUFBRSxLQWxCQTtBQW1CVEMsUUFBQUEsV0FBVyxFQUFFLENBbkJKO0FBb0JUQyxRQUFBQSxTQUFTLEVBQUUsSUFwQkY7QUFxQlRDLFFBQUFBLEtBQUssRUFBRSxJQXJCRTtBQXNCVEMsUUFBQUEsV0FBVyxFQUFFLEVBdEJKO0FBdUJUQyxRQUFBQSxpQkFBaUIsRUFBRSxLQXZCVjtBQXdCVEMsUUFBQUEsU0FBUyxFQUFFO0FBeEJGLE9BQWI7QUEyQkF0SCxNQUFBQSxDQUFDLENBQUNrQixNQUFGLENBQVN3QixDQUFULEVBQVlBLENBQUMsQ0FBQ29ELFFBQWQ7QUFFQXBELE1BQUFBLENBQUMsQ0FBQzZFLGdCQUFGLEdBQXFCLElBQXJCO0FBQ0E3RSxNQUFBQSxDQUFDLENBQUM4RSxRQUFGLEdBQWEsSUFBYjtBQUNBOUUsTUFBQUEsQ0FBQyxDQUFDK0UsUUFBRixHQUFhLElBQWI7QUFDQS9FLE1BQUFBLENBQUMsQ0FBQ2dGLFdBQUYsR0FBZ0IsRUFBaEI7QUFDQWhGLE1BQUFBLENBQUMsQ0FBQ2lGLGtCQUFGLEdBQXVCLEVBQXZCO0FBQ0FqRixNQUFBQSxDQUFDLENBQUNrRixjQUFGLEdBQW1CLEtBQW5CO0FBQ0FsRixNQUFBQSxDQUFDLENBQUNtRixRQUFGLEdBQWEsS0FBYjtBQUNBbkYsTUFBQUEsQ0FBQyxDQUFDb0YsV0FBRixHQUFnQixLQUFoQjtBQUNBcEYsTUFBQUEsQ0FBQyxDQUFDcUYsTUFBRixHQUFXLFFBQVg7QUFDQXJGLE1BQUFBLENBQUMsQ0FBQ3NGLE1BQUYsR0FBVyxJQUFYO0FBQ0F0RixNQUFBQSxDQUFDLENBQUN1RixZQUFGLEdBQWlCLElBQWpCO0FBQ0F2RixNQUFBQSxDQUFDLENBQUNpQyxTQUFGLEdBQWMsSUFBZDtBQUNBakMsTUFBQUEsQ0FBQyxDQUFDd0YsUUFBRixHQUFhLENBQWI7QUFDQXhGLE1BQUFBLENBQUMsQ0FBQ3lGLFdBQUYsR0FBZ0IsSUFBaEI7QUFDQXpGLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsR0FBWXBJLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBYjtBQUNBQyxNQUFBQSxDQUFDLENBQUMyRixZQUFGLEdBQWlCLElBQWpCO0FBQ0EzRixNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLElBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLElBQW5CO0FBQ0E3RixNQUFBQSxDQUFDLENBQUM4RixnQkFBRixHQUFxQixrQkFBckI7QUFDQTlGLE1BQUFBLENBQUMsQ0FBQytGLFdBQUYsR0FBZ0IsQ0FBaEI7QUFDQS9GLE1BQUFBLENBQUMsQ0FBQ2dHLFdBQUYsR0FBZ0IsSUFBaEI7QUFFQS9GLE1BQUFBLFlBQVksR0FBRzNDLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRCxDQUFXa0csSUFBWCxDQUFnQixPQUFoQixLQUE0QixFQUEzQztBQUVBakcsTUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixHQUFZSCxDQUFDLENBQUNrQixNQUFGLENBQVMsRUFBVCxFQUFhd0IsQ0FBQyxDQUFDdEMsUUFBZixFQUF5QmEsUUFBekIsRUFBbUMwQixZQUFuQyxDQUFaO0FBRUFELE1BQUFBLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUN2QyxPQUFGLENBQVVrRSxZQUEzQjtBQUVBM0IsTUFBQUEsQ0FBQyxDQUFDa0csZ0JBQUYsR0FBcUJsRyxDQUFDLENBQUN2QyxPQUF2Qjs7QUFFQSxVQUFJLE9BQU8wSSxRQUFRLENBQUNDLFNBQWhCLEtBQThCLFdBQWxDLEVBQStDO0FBQzNDcEcsUUFBQUEsQ0FBQyxDQUFDcUYsTUFBRixHQUFXLFdBQVg7QUFDQXJGLFFBQUFBLENBQUMsQ0FBQzhGLGdCQUFGLEdBQXFCLHFCQUFyQjtBQUNILE9BSEQsTUFHTyxJQUFJLE9BQU9LLFFBQVEsQ0FBQ0UsWUFBaEIsS0FBaUMsV0FBckMsRUFBa0Q7QUFDckRyRyxRQUFBQSxDQUFDLENBQUNxRixNQUFGLEdBQVcsY0FBWDtBQUNBckYsUUFBQUEsQ0FBQyxDQUFDOEYsZ0JBQUYsR0FBcUIsd0JBQXJCO0FBQ0g7O0FBRUQ5RixNQUFBQSxDQUFDLENBQUNzRyxRQUFGLEdBQWFoSixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNzRyxRQUFWLEVBQW9CdEcsQ0FBcEIsQ0FBYjtBQUNBQSxNQUFBQSxDQUFDLENBQUN3RyxhQUFGLEdBQWtCbEosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDd0csYUFBVixFQUF5QnhHLENBQXpCLENBQWxCO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ3lHLGdCQUFGLEdBQXFCbkosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDeUcsZ0JBQVYsRUFBNEJ6RyxDQUE1QixDQUFyQjtBQUNBQSxNQUFBQSxDQUFDLENBQUMwRyxXQUFGLEdBQWdCcEosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDMEcsV0FBVixFQUF1QjFHLENBQXZCLENBQWhCO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzJHLFlBQUYsR0FBaUJySixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUMyRyxZQUFWLEVBQXdCM0csQ0FBeEIsQ0FBakI7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDNEcsYUFBRixHQUFrQnRKLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQzRHLGFBQVYsRUFBeUI1RyxDQUF6QixDQUFsQjtBQUNBQSxNQUFBQSxDQUFDLENBQUM2RyxXQUFGLEdBQWdCdkosQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDNkcsV0FBVixFQUF1QjdHLENBQXZCLENBQWhCO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQzhHLFlBQUYsR0FBaUJ4SixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUM4RyxZQUFWLEVBQXdCOUcsQ0FBeEIsQ0FBakI7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDK0csV0FBRixHQUFnQnpKLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQytHLFdBQVYsRUFBdUIvRyxDQUF2QixDQUFoQjtBQUNBQSxNQUFBQSxDQUFDLENBQUNnSCxVQUFGLEdBQWUxSixDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnSCxVQUFWLEVBQXNCaEgsQ0FBdEIsQ0FBZjtBQUVBQSxNQUFBQSxDQUFDLENBQUNGLFdBQUYsR0FBZ0JBLFdBQVcsRUFBM0IsQ0F2SThCLENBeUk5QjtBQUNBO0FBQ0E7O0FBQ0FFLE1BQUFBLENBQUMsQ0FBQ2lILFFBQUYsR0FBYSwyQkFBYjs7QUFHQWpILE1BQUFBLENBQUMsQ0FBQ2tILG1CQUFGOztBQUNBbEgsTUFBQUEsQ0FBQyxDQUFDbUgsSUFBRixDQUFPLElBQVA7QUFFSDs7QUFFRCxXQUFPdkgsS0FBUDtBQUVILEdBMUpRLEVBQVQ7O0FBNEpBQSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCQyxXQUFoQixHQUE4QixZQUFXO0FBQ3JDLFFBQUlySCxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFja0QsSUFBZCxDQUFtQixlQUFuQixFQUFvQ0MsSUFBcEMsQ0FBeUM7QUFDckMscUJBQWU7QUFEc0IsS0FBekMsRUFFR0QsSUFGSCxDQUVRLDBCQUZSLEVBRW9DQyxJQUZwQyxDQUV5QztBQUNyQyxrQkFBWTtBQUR5QixLQUZ6QztBQU1ILEdBVEQ7O0FBV0EzSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCSSxRQUFoQixHQUEyQjVILEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JLLFFBQWhCLEdBQTJCLFVBQVNDLE1BQVQsRUFBaUJDLEtBQWpCLEVBQXdCQyxTQUF4QixFQUFtQztBQUVyRixRQUFJNUgsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxPQUFPMkgsS0FBUCxLQUFrQixTQUF0QixFQUFpQztBQUM3QkMsTUFBQUEsU0FBUyxHQUFHRCxLQUFaO0FBQ0FBLE1BQUFBLEtBQUssR0FBRyxJQUFSO0FBQ0gsS0FIRCxNQUdPLElBQUlBLEtBQUssR0FBRyxDQUFSLElBQWNBLEtBQUssSUFBSTNILENBQUMsQ0FBQ2tFLFVBQTdCLEVBQTBDO0FBQzdDLGFBQU8sS0FBUDtBQUNIOztBQUVEbEUsSUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjs7QUFFQSxRQUFJLE9BQU9GLEtBQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDNUIsVUFBSUEsS0FBSyxLQUFLLENBQVYsSUFBZTNILENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlELE1BQVYsS0FBcUIsQ0FBeEMsRUFBMkM7QUFDdkN4SyxRQUFBQSxDQUFDLENBQUNvSyxNQUFELENBQUQsQ0FBVUssUUFBVixDQUFtQi9ILENBQUMsQ0FBQ29FLFdBQXJCO0FBQ0gsT0FGRCxNQUVPLElBQUl3RCxTQUFKLEVBQWU7QUFDbEJ0SyxRQUFBQSxDQUFDLENBQUNvSyxNQUFELENBQUQsQ0FBVU0sWUFBVixDQUF1QmhJLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYU4sS0FBYixDQUF2QjtBQUNILE9BRk0sTUFFQTtBQUNIckssUUFBQUEsQ0FBQyxDQUFDb0ssTUFBRCxDQUFELENBQVVRLFdBQVYsQ0FBc0JsSSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWFOLEtBQWIsQ0FBdEI7QUFDSDtBQUNKLEtBUkQsTUFRTztBQUNILFVBQUlDLFNBQVMsS0FBSyxJQUFsQixFQUF3QjtBQUNwQnRLLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRCxDQUFVUyxTQUFWLENBQW9CbkksQ0FBQyxDQUFDb0UsV0FBdEI7QUFDSCxPQUZELE1BRU87QUFDSDlHLFFBQUFBLENBQUMsQ0FBQ29LLE1BQUQsQ0FBRCxDQUFVSyxRQUFWLENBQW1CL0gsQ0FBQyxDQUFDb0UsV0FBckI7QUFDSDtBQUNKOztBQUVEcEUsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixHQUFZckUsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixLQUFLM0ssT0FBTCxDQUFhNEUsS0FBcEMsQ0FBWjs7QUFFQXJDLElBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsS0FBSzNLLE9BQUwsQ0FBYTRFLEtBQXBDLEVBQTJDZ0csTUFBM0M7O0FBRUFySSxJQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNrRSxNQUFkLENBQXFCdEksQ0FBQyxDQUFDcUUsT0FBdkI7O0FBRUFyRSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVV6RixJQUFWLENBQWUsVUFBUytJLEtBQVQsRUFBZ0I1SCxPQUFoQixFQUF5QjtBQUNwQ3pDLE1BQUFBLENBQUMsQ0FBQ3lDLE9BQUQsQ0FBRCxDQUFXd0gsSUFBWCxDQUFnQixrQkFBaEIsRUFBb0NJLEtBQXBDO0FBQ0gsS0FGRDs7QUFJQTNILElBQUFBLENBQUMsQ0FBQzJGLFlBQUYsR0FBaUIzRixDQUFDLENBQUNxRSxPQUFuQjs7QUFFQXJFLElBQUFBLENBQUMsQ0FBQ3VJLE1BQUY7QUFFSCxHQTNDRDs7QUE2Q0EzSSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCb0IsYUFBaEIsR0FBZ0MsWUFBVztBQUN2QyxRQUFJeEksQ0FBQyxHQUFHLElBQVI7O0FBQ0EsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixLQUEyQixDQUEzQixJQUFnQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBDLGNBQVYsS0FBNkIsSUFBN0QsSUFBcUVILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBaEcsRUFBdUc7QUFDbkcsVUFBSXlGLFlBQVksR0FBR3pJLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYWpJLENBQUMsQ0FBQzBELFlBQWYsRUFBNkJnRixXQUE3QixDQUF5QyxJQUF6QyxDQUFuQjs7QUFDQTFJLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUWtFLE9BQVIsQ0FBZ0I7QUFDWjVLLFFBQUFBLE1BQU0sRUFBRTBLO0FBREksT0FBaEIsRUFFR3pJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FGYjtBQUdIO0FBQ0osR0FSRDs7QUFVQWdDLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J3QixZQUFoQixHQUErQixVQUFTQyxVQUFULEVBQXFCQyxRQUFyQixFQUErQjtBQUUxRCxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFBQSxRQUNJL0ksQ0FBQyxHQUFHLElBRFI7O0FBR0FBLElBQUFBLENBQUMsQ0FBQ3dJLGFBQUY7O0FBRUEsUUFBSXhJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBbEIsSUFBMEJwQyxDQUFDLENBQUN2QyxPQUFGLENBQVV1RixRQUFWLEtBQXVCLEtBQXJELEVBQTREO0FBQ3hENkYsTUFBQUEsVUFBVSxHQUFHLENBQUNBLFVBQWQ7QUFDSDs7QUFDRCxRQUFJN0ksQ0FBQyxDQUFDMkUsaUJBQUYsS0FBd0IsS0FBNUIsRUFBbUM7QUFDL0IsVUFBSTNFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUJoRCxRQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWN1RSxPQUFkLENBQXNCO0FBQ2xCSyxVQUFBQSxJQUFJLEVBQUVIO0FBRFksU0FBdEIsRUFFRzdJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FGYixFQUVvQm9DLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZELE1BRjlCLEVBRXNDd0gsUUFGdEM7QUFHSCxPQUpELE1BSU87QUFDSDlJLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3VFLE9BQWQsQ0FBc0I7QUFDbEJNLFVBQUFBLEdBQUcsRUFBRUo7QUFEYSxTQUF0QixFQUVHN0ksQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQUZiLEVBRW9Cb0MsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkQsTUFGOUIsRUFFc0N3SCxRQUZ0QztBQUdIO0FBRUosS0FYRCxNQVdPO0FBRUgsVUFBSTlJLENBQUMsQ0FBQ2tGLGNBQUYsS0FBcUIsS0FBekIsRUFBZ0M7QUFDNUIsWUFBSWxGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEJwQyxVQUFBQSxDQUFDLENBQUN5RCxXQUFGLEdBQWdCLENBQUV6RCxDQUFDLENBQUN5RCxXQUFwQjtBQUNIOztBQUNEbkcsUUFBQUEsQ0FBQyxDQUFDO0FBQ0U0TCxVQUFBQSxTQUFTLEVBQUVsSixDQUFDLENBQUN5RDtBQURmLFNBQUQsQ0FBRCxDQUVHa0YsT0FGSCxDQUVXO0FBQ1BPLFVBQUFBLFNBQVMsRUFBRUw7QUFESixTQUZYLEVBSUc7QUFDQ00sVUFBQUEsUUFBUSxFQUFFbkosQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQURyQjtBQUVDMEQsVUFBQUEsTUFBTSxFQUFFdEIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkQsTUFGbkI7QUFHQzhILFVBQUFBLElBQUksRUFBRSxVQUFTQyxHQUFULEVBQWM7QUFDaEJBLFlBQUFBLEdBQUcsR0FBR0MsSUFBSSxDQUFDQyxJQUFMLENBQVVGLEdBQVYsQ0FBTjs7QUFDQSxnQkFBSXJKLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUIrRixjQUFBQSxTQUFTLENBQUMvSSxDQUFDLENBQUM4RSxRQUFILENBQVQsR0FBd0IsZUFDcEJ1RSxHQURvQixHQUNkLFVBRFY7O0FBRUFySixjQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCZ0ssU0FBbEI7QUFDSCxhQUpELE1BSU87QUFDSEEsY0FBQUEsU0FBUyxDQUFDL0ksQ0FBQyxDQUFDOEUsUUFBSCxDQUFULEdBQXdCLG1CQUNwQnVFLEdBRG9CLEdBQ2QsS0FEVjs7QUFFQXJKLGNBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3JGLEdBQWQsQ0FBa0JnSyxTQUFsQjtBQUNIO0FBQ0osV0FkRjtBQWVDUyxVQUFBQSxRQUFRLEVBQUUsWUFBVztBQUNqQixnQkFBSVYsUUFBSixFQUFjO0FBQ1ZBLGNBQUFBLFFBQVEsQ0FBQ1csSUFBVDtBQUNIO0FBQ0o7QUFuQkYsU0FKSDtBQTBCSCxPQTlCRCxNQThCTztBQUVIekosUUFBQUEsQ0FBQyxDQUFDMEosZUFBRjs7QUFDQWIsUUFBQUEsVUFBVSxHQUFHUyxJQUFJLENBQUNDLElBQUwsQ0FBVVYsVUFBVixDQUFiOztBQUVBLFlBQUk3SSxDQUFDLENBQUN2QyxPQUFGLENBQVV1RixRQUFWLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCK0YsVUFBQUEsU0FBUyxDQUFDL0ksQ0FBQyxDQUFDOEUsUUFBSCxDQUFULEdBQXdCLGlCQUFpQitELFVBQWpCLEdBQThCLGVBQXREO0FBQ0gsU0FGRCxNQUVPO0FBQ0hFLFVBQUFBLFNBQVMsQ0FBQy9JLENBQUMsQ0FBQzhFLFFBQUgsQ0FBVCxHQUF3QixxQkFBcUIrRCxVQUFyQixHQUFrQyxVQUExRDtBQUNIOztBQUNEN0ksUUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQmdLLFNBQWxCOztBQUVBLFlBQUlELFFBQUosRUFBYztBQUNWYSxVQUFBQSxVQUFVLENBQUMsWUFBVztBQUVsQjNKLFlBQUFBLENBQUMsQ0FBQzRKLGlCQUFGOztBQUVBZCxZQUFBQSxRQUFRLENBQUNXLElBQVQ7QUFDSCxXQUxTLEVBS1B6SixDQUFDLENBQUN2QyxPQUFGLENBQVVHLEtBTEgsQ0FBVjtBQU1IO0FBRUo7QUFFSjtBQUVKLEdBOUVEOztBQWdGQWdDLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J5QyxZQUFoQixHQUErQixZQUFXO0FBRXRDLFFBQUk3SixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lPLFFBQVEsR0FBR1AsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEMsUUFEekI7O0FBR0EsUUFBS0EsUUFBUSxJQUFJQSxRQUFRLEtBQUssSUFBOUIsRUFBcUM7QUFDakNBLE1BQUFBLFFBQVEsR0FBR2pELENBQUMsQ0FBQ2lELFFBQUQsQ0FBRCxDQUFZdUosR0FBWixDQUFnQjlKLENBQUMsQ0FBQzBGLE9BQWxCLENBQVg7QUFDSDs7QUFFRCxXQUFPbkYsUUFBUDtBQUVILEdBWEQ7O0FBYUFYLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I3RyxRQUFoQixHQUEyQixVQUFTb0gsS0FBVCxFQUFnQjtBQUV2QyxRQUFJM0gsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJTyxRQUFRLEdBQUdQLENBQUMsQ0FBQzZKLFlBQUYsRUFEZjs7QUFHQSxRQUFLdEosUUFBUSxLQUFLLElBQWIsSUFBcUIsT0FBT0EsUUFBUCxLQUFvQixRQUE5QyxFQUF5RDtBQUNyREEsTUFBQUEsUUFBUSxDQUFDM0IsSUFBVCxDQUFjLFlBQVc7QUFDckIsWUFBSW1MLE1BQU0sR0FBR3pNLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBNLEtBQVIsQ0FBYyxVQUFkLENBQWI7O0FBQ0EsWUFBRyxDQUFDRCxNQUFNLENBQUNuRixTQUFYLEVBQXNCO0FBQ2xCbUYsVUFBQUEsTUFBTSxDQUFDRSxZQUFQLENBQW9CdEMsS0FBcEIsRUFBMkIsSUFBM0I7QUFDSDtBQUNKLE9BTEQ7QUFNSDtBQUVKLEdBZEQ7O0FBZ0JBL0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnNDLGVBQWhCLEdBQWtDLFVBQVNySCxLQUFULEVBQWdCO0FBRTlDLFFBQUlyQyxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lrSyxVQUFVLEdBQUcsRUFEakI7O0FBR0EsUUFBSWxLLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUIwSSxNQUFBQSxVQUFVLENBQUNsSyxDQUFDLENBQUM2RixjQUFILENBQVYsR0FBK0I3RixDQUFDLENBQUM0RixhQUFGLEdBQWtCLEdBQWxCLEdBQXdCNUYsQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQUFsQyxHQUEwQyxLQUExQyxHQUFrRG9DLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXFELE9BQTNGO0FBQ0gsS0FGRCxNQUVPO0FBQ0hvSixNQUFBQSxVQUFVLENBQUNsSyxDQUFDLENBQUM2RixjQUFILENBQVYsR0FBK0IsYUFBYTdGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FBdkIsR0FBK0IsS0FBL0IsR0FBdUNvQyxDQUFDLENBQUN2QyxPQUFGLENBQVVxRCxPQUFoRjtBQUNIOztBQUVELFFBQUlkLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJ4QixNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCbUwsVUFBbEI7QUFDSCxLQUZELE1BRU87QUFDSGxLLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYTVGLEtBQWIsRUFBb0J0RCxHQUFwQixDQUF3Qm1MLFVBQXhCO0FBQ0g7QUFFSixHQWpCRDs7QUFtQkF0SyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCZCxRQUFoQixHQUEyQixZQUFXO0FBRWxDLFFBQUl0RyxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDd0csYUFBRjs7QUFFQSxRQUFLeEcsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBOUIsRUFBNkM7QUFDekN2QyxNQUFBQSxDQUFDLENBQUN1RCxhQUFGLEdBQWtCNEcsV0FBVyxDQUFFbkssQ0FBQyxDQUFDeUcsZ0JBQUosRUFBc0J6RyxDQUFDLENBQUN2QyxPQUFGLENBQVVrRCxhQUFoQyxDQUE3QjtBQUNIO0FBRUosR0FWRDs7QUFZQWYsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQlosYUFBaEIsR0FBZ0MsWUFBVztBQUV2QyxRQUFJeEcsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdUQsYUFBTixFQUFxQjtBQUNqQjZHLE1BQUFBLGFBQWEsQ0FBQ3BLLENBQUMsQ0FBQ3VELGFBQUgsQ0FBYjtBQUNIO0FBRUosR0FSRDs7QUFVQTNELEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JYLGdCQUFoQixHQUFtQyxZQUFXO0FBRTFDLFFBQUl6RyxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lxSyxPQUFPLEdBQUdySyxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FEekM7O0FBR0EsUUFBSyxDQUFDeEMsQ0FBQyxDQUFDc0YsTUFBSCxJQUFhLENBQUN0RixDQUFDLENBQUNvRixXQUFoQixJQUErQixDQUFDcEYsQ0FBQyxDQUFDbUYsUUFBdkMsRUFBa0Q7QUFFOUMsVUFBS25GLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsS0FBNUIsRUFBb0M7QUFFaEMsWUFBSzFCLENBQUMsQ0FBQzJELFNBQUYsS0FBZ0IsQ0FBaEIsSUFBdUIzRCxDQUFDLENBQUMwRCxZQUFGLEdBQWlCLENBQW5CLEtBQTZCMUQsQ0FBQyxDQUFDa0UsVUFBRixHQUFlLENBQXRFLEVBQTJFO0FBQ3ZFbEUsVUFBQUEsQ0FBQyxDQUFDMkQsU0FBRixHQUFjLENBQWQ7QUFDSCxTQUZELE1BSUssSUFBSzNELENBQUMsQ0FBQzJELFNBQUYsS0FBZ0IsQ0FBckIsRUFBeUI7QUFFMUIwRyxVQUFBQSxPQUFPLEdBQUdySyxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBckM7O0FBRUEsY0FBS3hDLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIsQ0FBakIsS0FBdUIsQ0FBNUIsRUFBZ0M7QUFDNUIxRCxZQUFBQSxDQUFDLENBQUMyRCxTQUFGLEdBQWMsQ0FBZDtBQUNIO0FBRUo7QUFFSjs7QUFFRDNELE1BQUFBLENBQUMsQ0FBQ2lLLFlBQUYsQ0FBZ0JJLE9BQWhCO0FBRUg7QUFFSixHQTdCRDs7QUErQkF6SyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCa0QsV0FBaEIsR0FBOEIsWUFBVztBQUVyQyxRQUFJdEssQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkMsTUFBVixLQUFxQixJQUF6QixFQUFnQztBQUU1Qk4sTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixHQUFlM0csQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0MsU0FBWCxDQUFELENBQXVCOUIsUUFBdkIsQ0FBZ0MsYUFBaEMsQ0FBZjtBQUNBc0IsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixHQUFlMUcsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0QsU0FBWCxDQUFELENBQXVCL0IsUUFBdkIsQ0FBZ0MsYUFBaEMsQ0FBZjs7QUFFQSxVQUFJc0IsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBN0IsRUFBNEM7QUFFeEN2QyxRQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWFzRyxXQUFiLENBQXlCLGNBQXpCLEVBQXlDQyxVQUF6QyxDQUFvRCxzQkFBcEQ7O0FBQ0F4SyxRQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWF1RyxXQUFiLENBQXlCLGNBQXpCLEVBQXlDQyxVQUF6QyxDQUFvRCxzQkFBcEQ7O0FBRUEsWUFBSXhLLENBQUMsQ0FBQ2lILFFBQUYsQ0FBV3dELElBQVgsQ0FBZ0J6SyxDQUFDLENBQUN2QyxPQUFGLENBQVUrQyxTQUExQixDQUFKLEVBQTBDO0FBQ3RDUixVQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWFrRSxTQUFiLENBQXVCbkksQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkMsWUFBakM7QUFDSDs7QUFFRCxZQUFJSixDQUFDLENBQUNpSCxRQUFGLENBQVd3RCxJQUFYLENBQWdCekssQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0QsU0FBMUIsQ0FBSixFQUEwQztBQUN0Q1QsVUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhK0QsUUFBYixDQUFzQi9ILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJDLFlBQWhDO0FBQ0g7O0FBRUQsWUFBSUosQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUEzQixFQUFpQztBQUM3QjFCLFVBQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FDS3ZGLFFBREwsQ0FDYyxnQkFEZCxFQUVLNkksSUFGTCxDQUVVLGVBRlYsRUFFMkIsTUFGM0I7QUFHSDtBQUVKLE9BbkJELE1BbUJPO0FBRUh2SCxRQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWF5RyxHQUFiLENBQWtCMUssQ0FBQyxDQUFDZ0UsVUFBcEIsRUFFS3RGLFFBRkwsQ0FFYyxjQUZkLEVBR0s2SSxJQUhMLENBR1U7QUFDRiwyQkFBaUIsTUFEZjtBQUVGLHNCQUFZO0FBRlYsU0FIVjtBQVFIO0FBRUo7QUFFSixHQTFDRDs7QUE0Q0EzSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCdUQsU0FBaEIsR0FBNEIsWUFBVztBQUVuQyxRQUFJM0ssQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJaUIsQ0FESjtBQUFBLFFBQ08ySixHQURQOztBQUdBLFFBQUk1SyxDQUFDLENBQUN2QyxPQUFGLENBQVUwRCxJQUFWLEtBQW1CLElBQW5CLElBQTJCbkIsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBeEQsRUFBc0U7QUFFbEV2QyxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVoSCxRQUFWLENBQW1CLGNBQW5COztBQUVBa00sTUFBQUEsR0FBRyxHQUFHdE4sQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZb0IsUUFBWixDQUFxQnNCLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJELFNBQS9CLENBQU47O0FBRUEsV0FBS0gsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxJQUFJakIsQ0FBQyxDQUFDNkssV0FBRixFQUFqQixFQUFrQzVKLENBQUMsSUFBSSxDQUF2QyxFQUEwQztBQUN0QzJKLFFBQUFBLEdBQUcsQ0FBQ3RDLE1BQUosQ0FBV2hMLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWWdMLE1BQVosQ0FBbUJ0SSxDQUFDLENBQUN2QyxPQUFGLENBQVVzRCxZQUFWLENBQXVCMEksSUFBdkIsQ0FBNEIsSUFBNUIsRUFBa0N6SixDQUFsQyxFQUFxQ2lCLENBQXJDLENBQW5CLENBQVg7QUFDSDs7QUFFRGpCLE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsR0FBVWdILEdBQUcsQ0FBQzdDLFFBQUosQ0FBYS9ILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTRDLFVBQXZCLENBQVY7O0FBRUFMLE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsQ0FBUTBELElBQVIsQ0FBYSxJQUFiLEVBQW1Cd0QsS0FBbkIsR0FBMkJwTSxRQUEzQixDQUFvQyxjQUFwQyxFQUFvRDZJLElBQXBELENBQXlELGFBQXpELEVBQXdFLE9BQXhFO0FBRUg7QUFFSixHQXJCRDs7QUF1QkEzSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCMkQsUUFBaEIsR0FBMkIsWUFBVztBQUVsQyxRQUFJL0ssQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUYsR0FDSXJFLENBQUMsQ0FBQzBGLE9BQUYsQ0FDSzBDLFFBREwsQ0FDZXBJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTRFLEtBQVYsR0FBa0IscUJBRGpDLEVBRUszRCxRQUZMLENBRWMsYUFGZCxDQURKO0FBS0FzQixJQUFBQSxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUNxRSxPQUFGLENBQVV5RCxNQUF6Qjs7QUFFQTlILElBQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXpGLElBQVYsQ0FBZSxVQUFTK0ksS0FBVCxFQUFnQjVILE9BQWhCLEVBQXlCO0FBQ3BDekMsTUFBQUEsQ0FBQyxDQUFDeUMsT0FBRCxDQUFELENBQ0t3SCxJQURMLENBQ1Usa0JBRFYsRUFDOEJJLEtBRDlCLEVBRUsxQixJQUZMLENBRVUsaUJBRlYsRUFFNkIzSSxDQUFDLENBQUN5QyxPQUFELENBQUQsQ0FBV3dILElBQVgsQ0FBZ0IsT0FBaEIsS0FBNEIsRUFGekQ7QUFHSCxLQUpEOztBQU1BdkgsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVaEgsUUFBVixDQUFtQixjQUFuQjs7QUFFQXNCLElBQUFBLENBQUMsQ0FBQ29FLFdBQUYsR0FBaUJwRSxDQUFDLENBQUNrRSxVQUFGLEtBQWlCLENBQWxCLEdBQ1o1RyxDQUFDLENBQUMsNEJBQUQsQ0FBRCxDQUFnQ3lLLFFBQWhDLENBQXlDL0gsQ0FBQyxDQUFDMEYsT0FBM0MsQ0FEWSxHQUVaMUYsQ0FBQyxDQUFDcUUsT0FBRixDQUFVMkcsT0FBVixDQUFrQiw0QkFBbEIsRUFBZ0RsTSxNQUFoRCxFQUZKO0FBSUFrQixJQUFBQSxDQUFDLENBQUN5RSxLQUFGLEdBQVV6RSxDQUFDLENBQUNvRSxXQUFGLENBQWN2RixJQUFkLENBQ04sOENBRE0sRUFDMENDLE1BRDFDLEVBQVY7O0FBRUFrQixJQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCLFNBQWxCLEVBQTZCLENBQTdCOztBQUVBLFFBQUlpQixDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQXpCLElBQWlDWixDQUFDLENBQUN2QyxPQUFGLENBQVVpRixZQUFWLEtBQTJCLElBQWhFLEVBQXNFO0FBQ2xFMUMsTUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBVixHQUEyQixDQUEzQjtBQUNIOztBQUVEbEYsSUFBQUEsQ0FBQyxDQUFDLGdCQUFELEVBQW1CMEMsQ0FBQyxDQUFDMEYsT0FBckIsQ0FBRCxDQUErQm9FLEdBQS9CLENBQW1DLE9BQW5DLEVBQTRDcEwsUUFBNUMsQ0FBcUQsZUFBckQ7O0FBRUFzQixJQUFBQSxDQUFDLENBQUNpTCxhQUFGOztBQUVBakwsSUFBQUEsQ0FBQyxDQUFDc0ssV0FBRjs7QUFFQXRLLElBQUFBLENBQUMsQ0FBQzJLLFNBQUY7O0FBRUEzSyxJQUFBQSxDQUFDLENBQUNrTCxVQUFGOztBQUdBbEwsSUFBQUEsQ0FBQyxDQUFDbUwsZUFBRixDQUFrQixPQUFPbkwsQ0FBQyxDQUFDMEQsWUFBVCxLQUEwQixRQUExQixHQUFxQzFELENBQUMsQ0FBQzBELFlBQXZDLEdBQXNELENBQXhFOztBQUVBLFFBQUkxRCxDQUFDLENBQUN2QyxPQUFGLENBQVU0RCxTQUFWLEtBQXdCLElBQTVCLEVBQWtDO0FBQzlCckIsTUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRL0YsUUFBUixDQUFpQixXQUFqQjtBQUNIO0FBRUosR0FoREQ7O0FBa0RBa0IsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmdFLFNBQWhCLEdBQTRCLFlBQVc7QUFFbkMsUUFBSXBMLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBY3FMLENBQWQ7QUFBQSxRQUFpQkMsQ0FBakI7QUFBQSxRQUFvQkMsQ0FBcEI7QUFBQSxRQUF1QkMsU0FBdkI7QUFBQSxRQUFrQ0MsV0FBbEM7QUFBQSxRQUErQ0MsY0FBL0M7QUFBQSxRQUE4REMsZ0JBQTlEOztBQUVBSCxJQUFBQSxTQUFTLEdBQUdyRixRQUFRLENBQUN5RixzQkFBVCxFQUFaO0FBQ0FGLElBQUFBLGNBQWMsR0FBRzFMLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTBDLFFBQVYsRUFBakI7O0FBRUEsUUFBR3BJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBFLElBQVYsR0FBaUIsQ0FBcEIsRUFBdUI7QUFFbkJ3SixNQUFBQSxnQkFBZ0IsR0FBRzNMLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZFLFlBQVYsR0FBeUJ0QyxDQUFDLENBQUN2QyxPQUFGLENBQVUwRSxJQUF0RDtBQUNBc0osTUFBQUEsV0FBVyxHQUFHbkMsSUFBSSxDQUFDQyxJQUFMLENBQ1ZtQyxjQUFjLENBQUM1RCxNQUFmLEdBQXdCNkQsZ0JBRGQsQ0FBZDs7QUFJQSxXQUFJTixDQUFDLEdBQUcsQ0FBUixFQUFXQSxDQUFDLEdBQUdJLFdBQWYsRUFBNEJKLENBQUMsRUFBN0IsRUFBZ0M7QUFDNUIsWUFBSWhKLEtBQUssR0FBRzhELFFBQVEsQ0FBQzBGLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBWjs7QUFDQSxhQUFJUCxDQUFDLEdBQUcsQ0FBUixFQUFXQSxDQUFDLEdBQUd0TCxDQUFDLENBQUN2QyxPQUFGLENBQVUwRSxJQUF6QixFQUErQm1KLENBQUMsRUFBaEMsRUFBb0M7QUFDaEMsY0FBSVEsR0FBRyxHQUFHM0YsUUFBUSxDQUFDMEYsYUFBVCxDQUF1QixLQUF2QixDQUFWOztBQUNBLGVBQUlOLENBQUMsR0FBRyxDQUFSLEVBQVdBLENBQUMsR0FBR3ZMLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZFLFlBQXpCLEVBQXVDaUosQ0FBQyxFQUF4QyxFQUE0QztBQUN4QyxnQkFBSXhCLE1BQU0sR0FBSXNCLENBQUMsR0FBR00sZ0JBQUosSUFBeUJMLENBQUMsR0FBR3RMLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZFLFlBQWYsR0FBK0JpSixDQUF2RCxDQUFkOztBQUNBLGdCQUFJRyxjQUFjLENBQUNLLEdBQWYsQ0FBbUJoQyxNQUFuQixDQUFKLEVBQWdDO0FBQzVCK0IsY0FBQUEsR0FBRyxDQUFDRSxXQUFKLENBQWdCTixjQUFjLENBQUNLLEdBQWYsQ0FBbUJoQyxNQUFuQixDQUFoQjtBQUNIO0FBQ0o7O0FBQ0QxSCxVQUFBQSxLQUFLLENBQUMySixXQUFOLENBQWtCRixHQUFsQjtBQUNIOztBQUNETixRQUFBQSxTQUFTLENBQUNRLFdBQVYsQ0FBc0IzSixLQUF0QjtBQUNIOztBQUVEckMsTUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVdUcsS0FBVixHQUFrQjNELE1BQWxCLENBQXlCa0QsU0FBekI7O0FBQ0F4TCxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVUwQyxRQUFWLEdBQXFCQSxRQUFyQixHQUFnQ0EsUUFBaEMsR0FDS3JKLEdBREwsQ0FDUztBQUNELGlCQUFTLE1BQU1pQixDQUFDLENBQUN2QyxPQUFGLENBQVU2RSxZQUFqQixHQUFpQyxHQUR4QztBQUVELG1CQUFXO0FBRlYsT0FEVDtBQU1IO0FBRUosR0F0Q0Q7O0FBd0NBMUMsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjhFLGVBQWhCLEdBQWtDLFVBQVNDLE9BQVQsRUFBa0JDLFdBQWxCLEVBQStCO0FBRTdELFFBQUlwTSxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lxTSxVQURKO0FBQUEsUUFDZ0JDLGdCQURoQjtBQUFBLFFBQ2tDQyxjQURsQztBQUFBLFFBQ2tEQyxpQkFBaUIsR0FBRyxLQUR0RTs7QUFFQSxRQUFJQyxXQUFXLEdBQUd6TSxDQUFDLENBQUMwRixPQUFGLENBQVU1SCxLQUFWLEVBQWxCOztBQUNBLFFBQUlpSSxXQUFXLEdBQUdsRyxNQUFNLENBQUM2TSxVQUFQLElBQXFCcFAsQ0FBQyxDQUFDdUMsTUFBRCxDQUFELENBQVUvQixLQUFWLEVBQXZDOztBQUVBLFFBQUlrQyxDQUFDLENBQUNpQyxTQUFGLEtBQWdCLFFBQXBCLEVBQThCO0FBQzFCc0ssTUFBQUEsY0FBYyxHQUFHeEcsV0FBakI7QUFDSCxLQUZELE1BRU8sSUFBSS9GLENBQUMsQ0FBQ2lDLFNBQUYsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDakNzSyxNQUFBQSxjQUFjLEdBQUdFLFdBQWpCO0FBQ0gsS0FGTSxNQUVBLElBQUl6TSxDQUFDLENBQUNpQyxTQUFGLEtBQWdCLEtBQXBCLEVBQTJCO0FBQzlCc0ssTUFBQUEsY0FBYyxHQUFHakQsSUFBSSxDQUFDcUQsR0FBTCxDQUFTNUcsV0FBVCxFQUFzQjBHLFdBQXRCLENBQWpCO0FBQ0g7O0FBRUQsUUFBS3pNLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsSUFDRGxDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsQ0FBcUI0RixNQURwQixJQUVEOUgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUUsVUFBVixLQUF5QixJQUY3QixFQUVtQztBQUUvQm9LLE1BQUFBLGdCQUFnQixHQUFHLElBQW5COztBQUVBLFdBQUtELFVBQUwsSUFBbUJyTSxDQUFDLENBQUNnRixXQUFyQixFQUFrQztBQUM5QixZQUFJaEYsQ0FBQyxDQUFDZ0YsV0FBRixDQUFjNEgsY0FBZCxDQUE2QlAsVUFBN0IsQ0FBSixFQUE4QztBQUMxQyxjQUFJck0sQ0FBQyxDQUFDa0csZ0JBQUYsQ0FBbUJyRSxXQUFuQixLQUFtQyxLQUF2QyxFQUE4QztBQUMxQyxnQkFBSTBLLGNBQWMsR0FBR3ZNLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBY3FILFVBQWQsQ0FBckIsRUFBZ0Q7QUFDNUNDLGNBQUFBLGdCQUFnQixHQUFHdE0sQ0FBQyxDQUFDZ0YsV0FBRixDQUFjcUgsVUFBZCxDQUFuQjtBQUNIO0FBQ0osV0FKRCxNQUlPO0FBQ0gsZ0JBQUlFLGNBQWMsR0FBR3ZNLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBY3FILFVBQWQsQ0FBckIsRUFBZ0Q7QUFDNUNDLGNBQUFBLGdCQUFnQixHQUFHdE0sQ0FBQyxDQUFDZ0YsV0FBRixDQUFjcUgsVUFBZCxDQUFuQjtBQUNIO0FBQ0o7QUFDSjtBQUNKOztBQUVELFVBQUlDLGdCQUFnQixLQUFLLElBQXpCLEVBQStCO0FBQzNCLFlBQUl0TSxDQUFDLENBQUM2RSxnQkFBRixLQUF1QixJQUEzQixFQUFpQztBQUM3QixjQUFJeUgsZ0JBQWdCLEtBQUt0TSxDQUFDLENBQUM2RSxnQkFBdkIsSUFBMkN1SCxXQUEvQyxFQUE0RDtBQUN4RHBNLFlBQUFBLENBQUMsQ0FBQzZFLGdCQUFGLEdBQ0l5SCxnQkFESjs7QUFFQSxnQkFBSXRNLENBQUMsQ0FBQ2lGLGtCQUFGLENBQXFCcUgsZ0JBQXJCLE1BQTJDLFNBQS9DLEVBQTBEO0FBQ3REdE0sY0FBQUEsQ0FBQyxDQUFDNk0sT0FBRixDQUFVUCxnQkFBVjtBQUNILGFBRkQsTUFFTztBQUNIdE0sY0FBQUEsQ0FBQyxDQUFDdkMsT0FBRixHQUFZSCxDQUFDLENBQUNrQixNQUFGLENBQVMsRUFBVCxFQUFhd0IsQ0FBQyxDQUFDa0csZ0JBQWYsRUFDUmxHLENBQUMsQ0FBQ2lGLGtCQUFGLENBQ0lxSCxnQkFESixDQURRLENBQVo7O0FBR0Esa0JBQUlILE9BQU8sS0FBSyxJQUFoQixFQUFzQjtBQUNsQm5NLGdCQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVa0UsWUFBM0I7QUFDSDs7QUFDRDNCLGNBQUFBLENBQUMsQ0FBQzhNLE9BQUYsQ0FBVVgsT0FBVjtBQUNIOztBQUNESyxZQUFBQSxpQkFBaUIsR0FBR0YsZ0JBQXBCO0FBQ0g7QUFDSixTQWpCRCxNQWlCTztBQUNIdE0sVUFBQUEsQ0FBQyxDQUFDNkUsZ0JBQUYsR0FBcUJ5SCxnQkFBckI7O0FBQ0EsY0FBSXRNLENBQUMsQ0FBQ2lGLGtCQUFGLENBQXFCcUgsZ0JBQXJCLE1BQTJDLFNBQS9DLEVBQTBEO0FBQ3REdE0sWUFBQUEsQ0FBQyxDQUFDNk0sT0FBRixDQUFVUCxnQkFBVjtBQUNILFdBRkQsTUFFTztBQUNIdE0sWUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixHQUFZSCxDQUFDLENBQUNrQixNQUFGLENBQVMsRUFBVCxFQUFhd0IsQ0FBQyxDQUFDa0csZ0JBQWYsRUFDUmxHLENBQUMsQ0FBQ2lGLGtCQUFGLENBQ0lxSCxnQkFESixDQURRLENBQVo7O0FBR0EsZ0JBQUlILE9BQU8sS0FBSyxJQUFoQixFQUFzQjtBQUNsQm5NLGNBQUFBLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUN2QyxPQUFGLENBQVVrRSxZQUEzQjtBQUNIOztBQUNEM0IsWUFBQUEsQ0FBQyxDQUFDOE0sT0FBRixDQUFVWCxPQUFWO0FBQ0g7O0FBQ0RLLFVBQUFBLGlCQUFpQixHQUFHRixnQkFBcEI7QUFDSDtBQUNKLE9BakNELE1BaUNPO0FBQ0gsWUFBSXRNLENBQUMsQ0FBQzZFLGdCQUFGLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCN0UsVUFBQUEsQ0FBQyxDQUFDNkUsZ0JBQUYsR0FBcUIsSUFBckI7QUFDQTdFLFVBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsR0FBWXVDLENBQUMsQ0FBQ2tHLGdCQUFkOztBQUNBLGNBQUlpRyxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDbEJuTSxZQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVa0UsWUFBM0I7QUFDSDs7QUFDRDNCLFVBQUFBLENBQUMsQ0FBQzhNLE9BQUYsQ0FBVVgsT0FBVjs7QUFDQUssVUFBQUEsaUJBQWlCLEdBQUdGLGdCQUFwQjtBQUNIO0FBQ0osT0E3RDhCLENBK0QvQjs7O0FBQ0EsVUFBSSxDQUFDSCxPQUFELElBQVlLLGlCQUFpQixLQUFLLEtBQXRDLEVBQThDO0FBQzFDeE0sUUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixZQUFsQixFQUFnQyxDQUFDL00sQ0FBRCxFQUFJd00saUJBQUosQ0FBaEM7QUFDSDtBQUNKO0FBRUosR0F0RkQ7O0FBd0ZBNU0sRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQlYsV0FBaEIsR0FBOEIsVUFBU3NHLEtBQVQsRUFBZ0JDLFdBQWhCLEVBQTZCO0FBRXZELFFBQUlqTixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lrTixPQUFPLEdBQUc1UCxDQUFDLENBQUMwUCxLQUFLLENBQUNHLGFBQVAsQ0FEZjtBQUFBLFFBRUlDLFdBRko7QUFBQSxRQUVpQjdJLFdBRmpCO0FBQUEsUUFFOEI4SSxZQUY5QixDQUZ1RCxDQU12RDs7O0FBQ0EsUUFBR0gsT0FBTyxDQUFDSSxFQUFSLENBQVcsR0FBWCxDQUFILEVBQW9CO0FBQ2hCTixNQUFBQSxLQUFLLENBQUNPLGNBQU47QUFDSCxLQVRzRCxDQVd2RDs7O0FBQ0EsUUFBRyxDQUFDTCxPQUFPLENBQUNJLEVBQVIsQ0FBVyxJQUFYLENBQUosRUFBc0I7QUFDbEJKLE1BQUFBLE9BQU8sR0FBR0EsT0FBTyxDQUFDOU4sT0FBUixDQUFnQixJQUFoQixDQUFWO0FBQ0g7O0FBRURpTyxJQUFBQSxZQUFZLEdBQUlyTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUF6QixLQUE0QyxDQUE1RDtBQUNBNEssSUFBQUEsV0FBVyxHQUFHQyxZQUFZLEdBQUcsQ0FBSCxHQUFPLENBQUNyTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUMwRCxZQUFsQixJQUFrQzFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQTdFOztBQUVBLFlBQVF3SyxLQUFLLENBQUMvRyxJQUFOLENBQVd1SCxPQUFuQjtBQUVJLFdBQUssVUFBTDtBQUNJakosUUFBQUEsV0FBVyxHQUFHNkksV0FBVyxLQUFLLENBQWhCLEdBQW9CcE4sQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBOUIsR0FBK0N4QyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCNkssV0FBdEY7O0FBQ0EsWUFBSXBOLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTdCLEVBQTJDO0FBQ3ZDdkMsVUFBQUEsQ0FBQyxDQUFDaUssWUFBRixDQUFlakssQ0FBQyxDQUFDMEQsWUFBRixHQUFpQmEsV0FBaEMsRUFBNkMsS0FBN0MsRUFBb0QwSSxXQUFwRDtBQUNIOztBQUNEOztBQUVKLFdBQUssTUFBTDtBQUNJMUksUUFBQUEsV0FBVyxHQUFHNkksV0FBVyxLQUFLLENBQWhCLEdBQW9CcE4sQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBOUIsR0FBK0M0SyxXQUE3RDs7QUFDQSxZQUFJcE4sQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBN0IsRUFBMkM7QUFDdkN2QyxVQUFBQSxDQUFDLENBQUNpSyxZQUFGLENBQWVqSyxDQUFDLENBQUMwRCxZQUFGLEdBQWlCYSxXQUFoQyxFQUE2QyxLQUE3QyxFQUFvRDBJLFdBQXBEO0FBQ0g7O0FBQ0Q7O0FBRUosV0FBSyxPQUFMO0FBQ0ksWUFBSXRGLEtBQUssR0FBR3FGLEtBQUssQ0FBQy9HLElBQU4sQ0FBVzBCLEtBQVgsS0FBcUIsQ0FBckIsR0FBeUIsQ0FBekIsR0FDUnFGLEtBQUssQ0FBQy9HLElBQU4sQ0FBVzBCLEtBQVgsSUFBb0J1RixPQUFPLENBQUN2RixLQUFSLEtBQWtCM0gsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FEcEQ7O0FBR0F4QyxRQUFBQSxDQUFDLENBQUNpSyxZQUFGLENBQWVqSyxDQUFDLENBQUN5TixjQUFGLENBQWlCOUYsS0FBakIsQ0FBZixFQUF3QyxLQUF4QyxFQUErQ3NGLFdBQS9DOztBQUNBQyxRQUFBQSxPQUFPLENBQUM5RSxRQUFSLEdBQW1CMkUsT0FBbkIsQ0FBMkIsT0FBM0I7QUFDQTs7QUFFSjtBQUNJO0FBekJSO0FBNEJILEdBL0NEOztBQWlEQW5OLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JxRyxjQUFoQixHQUFpQyxVQUFTOUYsS0FBVCxFQUFnQjtBQUU3QyxRQUFJM0gsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJME4sVUFESjtBQUFBLFFBQ2dCQyxhQURoQjs7QUFHQUQsSUFBQUEsVUFBVSxHQUFHMU4sQ0FBQyxDQUFDNE4sbUJBQUYsRUFBYjtBQUNBRCxJQUFBQSxhQUFhLEdBQUcsQ0FBaEI7O0FBQ0EsUUFBSWhHLEtBQUssR0FBRytGLFVBQVUsQ0FBQ0EsVUFBVSxDQUFDNUYsTUFBWCxHQUFvQixDQUFyQixDQUF0QixFQUErQztBQUMzQ0gsTUFBQUEsS0FBSyxHQUFHK0YsVUFBVSxDQUFDQSxVQUFVLENBQUM1RixNQUFYLEdBQW9CLENBQXJCLENBQWxCO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsV0FBSyxJQUFJK0YsQ0FBVCxJQUFjSCxVQUFkLEVBQTBCO0FBQ3RCLFlBQUkvRixLQUFLLEdBQUcrRixVQUFVLENBQUNHLENBQUQsQ0FBdEIsRUFBMkI7QUFDdkJsRyxVQUFBQSxLQUFLLEdBQUdnRyxhQUFSO0FBQ0E7QUFDSDs7QUFDREEsUUFBQUEsYUFBYSxHQUFHRCxVQUFVLENBQUNHLENBQUQsQ0FBMUI7QUFDSDtBQUNKOztBQUVELFdBQU9sRyxLQUFQO0FBQ0gsR0FwQkQ7O0FBc0JBL0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjBHLGFBQWhCLEdBQWdDLFlBQVc7QUFFdkMsUUFBSTlOLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBELElBQVYsSUFBa0JuQixDQUFDLENBQUM0RCxLQUFGLEtBQVksSUFBbEMsRUFBd0M7QUFFcEN0RyxNQUFBQSxDQUFDLENBQUMsSUFBRCxFQUFPMEMsQ0FBQyxDQUFDNEQsS0FBVCxDQUFELENBQ0ttSyxHQURMLENBQ1MsYUFEVCxFQUN3Qi9OLENBQUMsQ0FBQzBHLFdBRDFCLEVBRUtxSCxHQUZMLENBRVMsa0JBRlQsRUFFNkJ6USxDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNnTyxTQUFWLEVBQXFCaE8sQ0FBckIsRUFBd0IsSUFBeEIsQ0FGN0IsRUFHSytOLEdBSEwsQ0FHUyxrQkFIVCxFQUc2QnpRLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixLQUF4QixDQUg3QjtBQUtIOztBQUVEQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSSxHQUFWLENBQWMsd0JBQWQ7O0FBRUEsUUFBSS9OLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZDLE1BQVYsS0FBcUIsSUFBckIsSUFBNkJOLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTFELEVBQXdFO0FBQ3BFdkMsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixJQUFnQmpFLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYThKLEdBQWIsQ0FBaUIsYUFBakIsRUFBZ0MvTixDQUFDLENBQUMwRyxXQUFsQyxDQUFoQjtBQUNBMUcsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixJQUFnQmhFLENBQUMsQ0FBQ2dFLFVBQUYsQ0FBYStKLEdBQWIsQ0FBaUIsYUFBakIsRUFBZ0MvTixDQUFDLENBQUMwRyxXQUFsQyxDQUFoQjtBQUNIOztBQUVEMUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRc0osR0FBUixDQUFZLGtDQUFaLEVBQWdEL04sQ0FBQyxDQUFDOEcsWUFBbEQ7O0FBQ0E5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVFzSixHQUFSLENBQVksaUNBQVosRUFBK0MvTixDQUFDLENBQUM4RyxZQUFqRDs7QUFDQTlHLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXNKLEdBQVIsQ0FBWSw4QkFBWixFQUE0Qy9OLENBQUMsQ0FBQzhHLFlBQTlDOztBQUNBOUcsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRc0osR0FBUixDQUFZLG9DQUFaLEVBQWtEL04sQ0FBQyxDQUFDOEcsWUFBcEQ7O0FBRUE5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVFzSixHQUFSLENBQVksYUFBWixFQUEyQi9OLENBQUMsQ0FBQzJHLFlBQTdCOztBQUVBckosSUFBQUEsQ0FBQyxDQUFDNkksUUFBRCxDQUFELENBQVk0SCxHQUFaLENBQWdCL04sQ0FBQyxDQUFDOEYsZ0JBQWxCLEVBQW9DOUYsQ0FBQyxDQUFDaU8sVUFBdEM7O0FBRUFqTyxJQUFBQSxDQUFDLENBQUNrTyxrQkFBRjs7QUFFQSxRQUFJbE8sQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUMsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ0YsTUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRc0osR0FBUixDQUFZLGVBQVosRUFBNkIvTixDQUFDLENBQUNnSCxVQUEvQjtBQUNIOztBQUVELFFBQUloSCxDQUFDLENBQUN2QyxPQUFGLENBQVVnRSxhQUFWLEtBQTRCLElBQWhDLEVBQXNDO0FBQ2xDbkUsTUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDb0UsV0FBSCxDQUFELENBQWlCZ0UsUUFBakIsR0FBNEIyRixHQUE1QixDQUFnQyxhQUFoQyxFQUErQy9OLENBQUMsQ0FBQzRHLGFBQWpEO0FBQ0g7O0FBRUR0SixJQUFBQSxDQUFDLENBQUN1QyxNQUFELENBQUQsQ0FBVWtPLEdBQVYsQ0FBYyxtQ0FBbUMvTixDQUFDLENBQUNGLFdBQW5ELEVBQWdFRSxDQUFDLENBQUNtTyxpQkFBbEU7QUFFQTdRLElBQUFBLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVa08sR0FBVixDQUFjLHdCQUF3Qi9OLENBQUMsQ0FBQ0YsV0FBeEMsRUFBcURFLENBQUMsQ0FBQ29PLE1BQXZEO0FBRUE5USxJQUFBQSxDQUFDLENBQUMsbUJBQUQsRUFBc0IwQyxDQUFDLENBQUNvRSxXQUF4QixDQUFELENBQXNDMkosR0FBdEMsQ0FBMEMsV0FBMUMsRUFBdUQvTixDQUFDLENBQUN1TixjQUF6RDtBQUVBalEsSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFELENBQVVrTyxHQUFWLENBQWMsc0JBQXNCL04sQ0FBQyxDQUFDRixXQUF0QyxFQUFtREUsQ0FBQyxDQUFDNkcsV0FBckQ7QUFDQXZKLElBQUFBLENBQUMsQ0FBQzZJLFFBQUQsQ0FBRCxDQUFZNEgsR0FBWixDQUFnQix1QkFBdUIvTixDQUFDLENBQUNGLFdBQXpDLEVBQXNERSxDQUFDLENBQUM2RyxXQUF4RDtBQUVILEdBaEREOztBQWtEQWpILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I4RyxrQkFBaEIsR0FBcUMsWUFBVztBQUU1QyxRQUFJbE8sQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXNKLEdBQVIsQ0FBWSxrQkFBWixFQUFnQ3pRLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixJQUF4QixDQUFoQzs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRc0osR0FBUixDQUFZLGtCQUFaLEVBQWdDelEsQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDZ08sU0FBVixFQUFxQmhPLENBQXJCLEVBQXdCLEtBQXhCLENBQWhDO0FBRUgsR0FQRDs7QUFTQUosRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmlILFdBQWhCLEdBQThCLFlBQVc7QUFFckMsUUFBSXJPLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFBYzBMLGNBQWQ7O0FBRUEsUUFBRzFMLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBFLElBQVYsR0FBaUIsQ0FBcEIsRUFBdUI7QUFDbkJ1SixNQUFBQSxjQUFjLEdBQUcxTCxDQUFDLENBQUNxRSxPQUFGLENBQVUrRCxRQUFWLEdBQXFCQSxRQUFyQixFQUFqQjtBQUNBc0QsTUFBQUEsY0FBYyxDQUFDbEIsVUFBZixDQUEwQixPQUExQjs7QUFDQXhLLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXVHLEtBQVYsR0FBa0IzRCxNQUFsQixDQUF5Qm9ELGNBQXpCO0FBQ0g7QUFFSixHQVZEOztBQVlBOUwsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQlQsWUFBaEIsR0FBK0IsVUFBU3FHLEtBQVQsRUFBZ0I7QUFFM0MsUUFBSWhOLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3lGLFdBQUYsS0FBa0IsS0FBdEIsRUFBNkI7QUFDekJ1SCxNQUFBQSxLQUFLLENBQUNzQix3QkFBTjtBQUNBdEIsTUFBQUEsS0FBSyxDQUFDdUIsZUFBTjtBQUNBdkIsTUFBQUEsS0FBSyxDQUFDTyxjQUFOO0FBQ0g7QUFFSixHQVZEOztBQVlBM04sRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQm9ILE9BQWhCLEdBQTBCLFVBQVMxQixPQUFULEVBQWtCO0FBRXhDLFFBQUk5TSxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDd0csYUFBRjs7QUFFQXhHLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsR0FBZ0IsRUFBaEI7O0FBRUExRSxJQUFBQSxDQUFDLENBQUM4TixhQUFGOztBQUVBeFEsSUFBQUEsQ0FBQyxDQUFDLGVBQUQsRUFBa0IwQyxDQUFDLENBQUMwRixPQUFwQixDQUFELENBQThCMkMsTUFBOUI7O0FBRUEsUUFBSXJJLENBQUMsQ0FBQzRELEtBQU4sRUFBYTtBQUNUNUQsTUFBQUEsQ0FBQyxDQUFDNEQsS0FBRixDQUFRNkssTUFBUjtBQUNIOztBQUdELFFBQUt6TyxDQUFDLENBQUNpRSxVQUFGLElBQWdCakUsQ0FBQyxDQUFDaUUsVUFBRixDQUFhNkQsTUFBbEMsRUFBMkM7QUFFdkM5SCxNQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQ0tzRyxXQURMLENBQ2lCLHlDQURqQixFQUVLQyxVQUZMLENBRWdCLG9DQUZoQixFQUdLekwsR0FITCxDQUdTLFNBSFQsRUFHbUIsRUFIbkI7O0FBS0EsVUFBS2lCLENBQUMsQ0FBQ2lILFFBQUYsQ0FBV3dELElBQVgsQ0FBaUJ6SyxDQUFDLENBQUN2QyxPQUFGLENBQVUrQyxTQUEzQixDQUFMLEVBQTZDO0FBQ3pDUixRQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWF3SyxNQUFiO0FBQ0g7QUFDSjs7QUFFRCxRQUFLek8sQ0FBQyxDQUFDZ0UsVUFBRixJQUFnQmhFLENBQUMsQ0FBQ2dFLFVBQUYsQ0FBYThELE1BQWxDLEVBQTJDO0FBRXZDOUgsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUNLdUcsV0FETCxDQUNpQix5Q0FEakIsRUFFS0MsVUFGTCxDQUVnQixvQ0FGaEIsRUFHS3pMLEdBSEwsQ0FHUyxTQUhULEVBR21CLEVBSG5COztBQUtBLFVBQUtpQixDQUFDLENBQUNpSCxRQUFGLENBQVd3RCxJQUFYLENBQWlCekssQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0QsU0FBM0IsQ0FBTCxFQUE2QztBQUN6Q1QsUUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFheUssTUFBYjtBQUNIO0FBRUo7O0FBR0QsUUFBSXpPLENBQUMsQ0FBQ3FFLE9BQU4sRUFBZTtBQUVYckUsTUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUNLa0csV0FETCxDQUNpQixtRUFEakIsRUFFS0MsVUFGTCxDQUVnQixhQUZoQixFQUdLQSxVQUhMLENBR2dCLGtCQUhoQixFQUlLNUwsSUFKTCxDQUlVLFlBQVU7QUFDWnRCLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlLLElBQVIsQ0FBYSxPQUFiLEVBQXNCakssQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMkksSUFBUixDQUFhLGlCQUFiLENBQXRCO0FBQ0gsT0FOTDs7QUFRQWpHLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsS0FBSzNLLE9BQUwsQ0FBYTRFLEtBQXBDLEVBQTJDZ0csTUFBM0M7O0FBRUFySSxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNpRSxNQUFkOztBQUVBckksTUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRNEQsTUFBUjs7QUFFQXJJLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTRDLE1BQVYsQ0FBaUJ0SSxDQUFDLENBQUNxRSxPQUFuQjtBQUNIOztBQUVEckUsSUFBQUEsQ0FBQyxDQUFDcU8sV0FBRjs7QUFFQXJPLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTZFLFdBQVYsQ0FBc0IsY0FBdEI7O0FBQ0F2SyxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVU2RSxXQUFWLENBQXNCLG1CQUF0Qjs7QUFDQXZLLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTZFLFdBQVYsQ0FBc0IsY0FBdEI7O0FBRUF2SyxJQUFBQSxDQUFDLENBQUM0RSxTQUFGLEdBQWMsSUFBZDs7QUFFQSxRQUFHLENBQUNrSSxPQUFKLEVBQWE7QUFDVDlNLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsU0FBbEIsRUFBNkIsQ0FBQy9NLENBQUQsQ0FBN0I7QUFDSDtBQUVKLEdBMUVEOztBQTRFQUosRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQndDLGlCQUFoQixHQUFvQyxVQUFTdkgsS0FBVCxFQUFnQjtBQUVoRCxRQUFJckMsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJa0ssVUFBVSxHQUFHLEVBRGpCOztBQUdBQSxJQUFBQSxVQUFVLENBQUNsSyxDQUFDLENBQUM2RixjQUFILENBQVYsR0FBK0IsRUFBL0I7O0FBRUEsUUFBSTdGLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsS0FBdkIsRUFBOEI7QUFDMUJ4QixNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCbUwsVUFBbEI7QUFDSCxLQUZELE1BRU87QUFDSGxLLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYTVGLEtBQWIsRUFBb0J0RCxHQUFwQixDQUF3Qm1MLFVBQXhCO0FBQ0g7QUFFSixHQWJEOztBQWVBdEssRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnNILFNBQWhCLEdBQTRCLFVBQVNDLFVBQVQsRUFBcUI3RixRQUFyQixFQUErQjtBQUV2RCxRQUFJOUksQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDa0YsY0FBRixLQUFxQixLQUF6QixFQUFnQztBQUU1QmxGLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYTBHLFVBQWIsRUFBeUI1UCxHQUF6QixDQUE2QjtBQUN6Qm9FLFFBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGO0FBRE8sT0FBN0I7O0FBSUFuRCxNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVU0RCxFQUFWLENBQWEwRyxVQUFiLEVBQXlCaEcsT0FBekIsQ0FBaUM7QUFDN0JpRyxRQUFBQSxPQUFPLEVBQUU7QUFEb0IsT0FBakMsRUFFRzVPLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVUcsS0FGYixFQUVvQm9DLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZELE1BRjlCLEVBRXNDd0gsUUFGdEM7QUFJSCxLQVZELE1BVU87QUFFSDlJLE1BQUFBLENBQUMsQ0FBQzBKLGVBQUYsQ0FBa0JpRixVQUFsQjs7QUFFQTNPLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYTBHLFVBQWIsRUFBeUI1UCxHQUF6QixDQUE2QjtBQUN6QjZQLFFBQUFBLE9BQU8sRUFBRSxDQURnQjtBQUV6QnpMLFFBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGO0FBRk8sT0FBN0I7O0FBS0EsVUFBSTJGLFFBQUosRUFBYztBQUNWYSxRQUFBQSxVQUFVLENBQUMsWUFBVztBQUVsQjNKLFVBQUFBLENBQUMsQ0FBQzRKLGlCQUFGLENBQW9CK0UsVUFBcEI7O0FBRUE3RixVQUFBQSxRQUFRLENBQUNXLElBQVQ7QUFDSCxTQUxTLEVBS1B6SixDQUFDLENBQUN2QyxPQUFGLENBQVVHLEtBTEgsQ0FBVjtBQU1IO0FBRUo7QUFFSixHQWxDRDs7QUFvQ0FnQyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCeUgsWUFBaEIsR0FBK0IsVUFBU0YsVUFBVCxFQUFxQjtBQUVoRCxRQUFJM08sQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSUEsQ0FBQyxDQUFDa0YsY0FBRixLQUFxQixLQUF6QixFQUFnQztBQUU1QmxGLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYTBHLFVBQWIsRUFBeUJoRyxPQUF6QixDQUFpQztBQUM3QmlHLFFBQUFBLE9BQU8sRUFBRSxDQURvQjtBQUU3QnpMLFFBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUI7QUFGRSxPQUFqQyxFQUdHbkQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVRyxLQUhiLEVBR29Cb0MsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNkQsTUFIOUI7QUFLSCxLQVBELE1BT087QUFFSHRCLE1BQUFBLENBQUMsQ0FBQzBKLGVBQUYsQ0FBa0JpRixVQUFsQjs7QUFFQTNPLE1BQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYTBHLFVBQWIsRUFBeUI1UCxHQUF6QixDQUE2QjtBQUN6QjZQLFFBQUFBLE9BQU8sRUFBRSxDQURnQjtBQUV6QnpMLFFBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUI7QUFGRixPQUE3QjtBQUtIO0FBRUosR0F0QkQ7O0FBd0JBdkQsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjBILFlBQWhCLEdBQStCbFAsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjJILFdBQWhCLEdBQThCLFVBQVNDLE1BQVQsRUFBaUI7QUFFMUUsUUFBSWhQLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlnUCxNQUFNLEtBQUssSUFBZixFQUFxQjtBQUVqQmhQLE1BQUFBLENBQUMsQ0FBQzJGLFlBQUYsR0FBaUIzRixDQUFDLENBQUNxRSxPQUFuQjs7QUFFQXJFLE1BQUFBLENBQUMsQ0FBQzZILE1BQUY7O0FBRUE3SCxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLEtBQUszSyxPQUFMLENBQWE0RSxLQUFwQyxFQUEyQ2dHLE1BQTNDOztBQUVBckksTUFBQUEsQ0FBQyxDQUFDMkYsWUFBRixDQUFlcUosTUFBZixDQUFzQkEsTUFBdEIsRUFBOEJqSCxRQUE5QixDQUF1Qy9ILENBQUMsQ0FBQ29FLFdBQXpDOztBQUVBcEUsTUFBQUEsQ0FBQyxDQUFDdUksTUFBRjtBQUVIO0FBRUosR0FsQkQ7O0FBb0JBM0ksRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjZILFlBQWhCLEdBQStCLFlBQVc7QUFFdEMsUUFBSWpQLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQ0txSSxHQURMLENBQ1Msd0JBRFQsRUFFSzdPLEVBRkwsQ0FFUSx3QkFGUixFQUdRLHFCQUhSLEVBRytCLFVBQVM4TixLQUFULEVBQWdCO0FBRTNDQSxNQUFBQSxLQUFLLENBQUNzQix3QkFBTjtBQUNBLFVBQUlZLEdBQUcsR0FBRzVSLENBQUMsQ0FBQyxJQUFELENBQVg7QUFFQXFNLE1BQUFBLFVBQVUsQ0FBQyxZQUFXO0FBRWxCLFlBQUkzSixDQUFDLENBQUN2QyxPQUFGLENBQVVzRSxZQUFkLEVBQTZCO0FBQ3pCL0IsVUFBQUEsQ0FBQyxDQUFDbUYsUUFBRixHQUFhK0osR0FBRyxDQUFDNUIsRUFBSixDQUFPLFFBQVAsQ0FBYjs7QUFDQXROLFVBQUFBLENBQUMsQ0FBQ3NHLFFBQUY7QUFDSDtBQUVKLE9BUFMsRUFPUCxDQVBPLENBQVY7QUFTSCxLQWpCRDtBQWtCSCxHQXRCRDs7QUF3QkExRyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCK0gsVUFBaEIsR0FBNkJ2UCxLQUFLLENBQUN3SCxTQUFOLENBQWdCZ0ksaUJBQWhCLEdBQW9DLFlBQVc7QUFFeEUsUUFBSXBQLENBQUMsR0FBRyxJQUFSOztBQUNBLFdBQU9BLENBQUMsQ0FBQzBELFlBQVQ7QUFFSCxHQUxEOztBQU9BOUQsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnlELFdBQWhCLEdBQThCLFlBQVc7QUFFckMsUUFBSTdLLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlxUCxVQUFVLEdBQUcsQ0FBakI7QUFDQSxRQUFJQyxPQUFPLEdBQUcsQ0FBZDtBQUNBLFFBQUlDLFFBQVEsR0FBRyxDQUFmOztBQUVBLFFBQUl2UCxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCLGFBQU8yTixVQUFVLEdBQUdyUCxDQUFDLENBQUNrRSxVQUF0QixFQUFrQztBQUM5QixVQUFFcUwsUUFBRjtBQUNBRixRQUFBQSxVQUFVLEdBQUdDLE9BQU8sR0FBR3RQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQWpDO0FBQ0E4TSxRQUFBQSxPQUFPLElBQUl0UCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFWLElBQTRCeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBdEMsR0FBcUR2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUEvRCxHQUFnRnhDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXJHO0FBQ0g7QUFDSixLQU5ELE1BTU8sSUFBSXZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBN0IsRUFBbUM7QUFDdEMyTyxNQUFBQSxRQUFRLEdBQUd2UCxDQUFDLENBQUNrRSxVQUFiO0FBQ0gsS0FGTSxNQUVBLElBQUcsQ0FBQ2xFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThDLFFBQWQsRUFBd0I7QUFDM0JnUCxNQUFBQSxRQUFRLEdBQUcsSUFBSWpHLElBQUksQ0FBQ0MsSUFBTCxDQUFVLENBQUN2SixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExQixJQUEwQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQTlELENBQWY7QUFDSCxLQUZNLE1BRUQ7QUFDRixhQUFPNk0sVUFBVSxHQUFHclAsQ0FBQyxDQUFDa0UsVUFBdEIsRUFBa0M7QUFDOUIsVUFBRXFMLFFBQUY7QUFDQUYsUUFBQUEsVUFBVSxHQUFHQyxPQUFPLEdBQUd0UCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFqQztBQUNBOE0sUUFBQUEsT0FBTyxJQUFJdFAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBVixJQUE0QnhDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXRDLEdBQXFEdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBL0QsR0FBZ0Z4QyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFyRztBQUNIO0FBQ0o7O0FBRUQsV0FBT2dOLFFBQVEsR0FBRyxDQUFsQjtBQUVILEdBNUJEOztBQThCQTNQLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JvSSxPQUFoQixHQUEwQixVQUFTYixVQUFULEVBQXFCO0FBRTNDLFFBQUkzTyxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0k2SSxVQURKO0FBQUEsUUFFSTRHLGNBRko7QUFBQSxRQUdJQyxjQUFjLEdBQUcsQ0FIckI7QUFBQSxRQUlJQyxXQUpKOztBQU1BM1AsSUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixHQUFnQixDQUFoQjtBQUNBa0wsSUFBQUEsY0FBYyxHQUFHelAsQ0FBQyxDQUFDcUUsT0FBRixDQUFVeUcsS0FBVixHQUFrQnBDLFdBQWxCLENBQThCLElBQTlCLENBQWpCOztBQUVBLFFBQUkxSSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCLFVBQUkxQixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE3QixFQUEyQztBQUN2Q3ZDLFFBQUFBLENBQUMsQ0FBQ3VFLFdBQUYsR0FBaUJ2RSxDQUFDLENBQUNtRSxVQUFGLEdBQWVuRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExQixHQUEwQyxDQUFDLENBQTNEO0FBQ0FtTixRQUFBQSxjQUFjLEdBQUlELGNBQWMsR0FBR3pQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTVCLEdBQTRDLENBQUMsQ0FBOUQ7QUFDSDs7QUFDRCxVQUFJdkMsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBekIsS0FBNEMsQ0FBaEQsRUFBbUQ7QUFDL0MsWUFBSW1NLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXZCLEdBQXdDeEMsQ0FBQyxDQUFDa0UsVUFBMUMsSUFBd0RsRSxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFyRixFQUFtRztBQUMvRixjQUFJb00sVUFBVSxHQUFHM08sQ0FBQyxDQUFDa0UsVUFBbkIsRUFBK0I7QUFDM0JsRSxZQUFBQSxDQUFDLENBQUN1RSxXQUFGLEdBQWlCLENBQUN2RSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLElBQTBCb00sVUFBVSxHQUFHM08sQ0FBQyxDQUFDa0UsVUFBekMsQ0FBRCxJQUF5RGxFLENBQUMsQ0FBQ21FLFVBQTVELEdBQTBFLENBQUMsQ0FBM0Y7QUFDQXVMLFlBQUFBLGNBQWMsR0FBSSxDQUFDMVAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixJQUEwQm9NLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBQXpDLENBQUQsSUFBeUR1TCxjQUExRCxHQUE0RSxDQUFDLENBQTlGO0FBQ0gsV0FIRCxNQUdPO0FBQ0h6UCxZQUFBQSxDQUFDLENBQUN1RSxXQUFGLEdBQWtCdkUsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBMUIsR0FBNEN4QyxDQUFDLENBQUNtRSxVQUEvQyxHQUE2RCxDQUFDLENBQTlFO0FBQ0F1TCxZQUFBQSxjQUFjLEdBQUsxUCxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUExQixHQUE0Q2lOLGNBQTdDLEdBQStELENBQUMsQ0FBakY7QUFDSDtBQUNKO0FBQ0o7QUFDSixLQWhCRCxNQWdCTztBQUNILFVBQUlkLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXZCLEdBQXNDdkMsQ0FBQyxDQUFDa0UsVUFBNUMsRUFBd0Q7QUFDcERsRSxRQUFBQSxDQUFDLENBQUN1RSxXQUFGLEdBQWdCLENBQUVvSyxVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF4QixHQUF3Q3ZDLENBQUMsQ0FBQ2tFLFVBQTNDLElBQXlEbEUsQ0FBQyxDQUFDbUUsVUFBM0U7QUFDQXVMLFFBQUFBLGNBQWMsR0FBRyxDQUFFZixVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF4QixHQUF3Q3ZDLENBQUMsQ0FBQ2tFLFVBQTNDLElBQXlEdUwsY0FBMUU7QUFDSDtBQUNKOztBQUVELFFBQUl6UCxDQUFDLENBQUNrRSxVQUFGLElBQWdCbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBOUIsRUFBNEM7QUFDeEN2QyxNQUFBQSxDQUFDLENBQUN1RSxXQUFGLEdBQWdCLENBQWhCO0FBQ0FtTCxNQUFBQSxjQUFjLEdBQUcsQ0FBakI7QUFDSDs7QUFFRCxRQUFJMVAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUF6QixJQUFpQ1osQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixJQUE1RCxFQUFrRTtBQUM5RDFCLE1BQUFBLENBQUMsQ0FBQ3VFLFdBQUYsSUFBaUJ2RSxDQUFDLENBQUNtRSxVQUFGLEdBQWVtRixJQUFJLENBQUNzRyxLQUFMLENBQVc1UCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCLENBQXBDLENBQWYsR0FBd0R2QyxDQUFDLENBQUNtRSxVQUEzRTtBQUNILEtBRkQsTUFFTyxJQUFJbkUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUE3QixFQUFtQztBQUN0Q1osTUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixHQUFnQixDQUFoQjtBQUNBdkUsTUFBQUEsQ0FBQyxDQUFDdUUsV0FBRixJQUFpQnZFLENBQUMsQ0FBQ21FLFVBQUYsR0FBZW1GLElBQUksQ0FBQ3NHLEtBQUwsQ0FBVzVQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBcEMsQ0FBaEM7QUFDSDs7QUFFRCxRQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVdUYsUUFBVixLQUF1QixLQUEzQixFQUFrQztBQUM5QjZGLE1BQUFBLFVBQVUsR0FBSzhGLFVBQVUsR0FBRzNPLENBQUMsQ0FBQ21FLFVBQWhCLEdBQThCLENBQUMsQ0FBaEMsR0FBcUNuRSxDQUFDLENBQUN1RSxXQUFwRDtBQUNILEtBRkQsTUFFTztBQUNIc0UsTUFBQUEsVUFBVSxHQUFLOEYsVUFBVSxHQUFHYyxjQUFkLEdBQWdDLENBQUMsQ0FBbEMsR0FBdUNDLGNBQXBEO0FBQ0g7O0FBRUQsUUFBSTFQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXNGLGFBQVYsS0FBNEIsSUFBaEMsRUFBc0M7QUFFbEMsVUFBSS9DLENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExQixJQUEwQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsS0FBckUsRUFBNEU7QUFDeEVpTyxRQUFBQSxXQUFXLEdBQUczUCxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLGNBQXZCLEVBQXVDSCxFQUF2QyxDQUEwQzBHLFVBQTFDLENBQWQ7QUFDSCxPQUZELE1BRU87QUFDSGdCLFFBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsY0FBdkIsRUFBdUNILEVBQXZDLENBQTBDMEcsVUFBVSxHQUFHM08sQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBakUsQ0FBZDtBQUNIOztBQUVELFVBQUl2QyxDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLElBQXRCLEVBQTRCO0FBQ3hCLFlBQUl1TixXQUFXLENBQUMsQ0FBRCxDQUFmLEVBQW9CO0FBQ2hCOUcsVUFBQUEsVUFBVSxHQUFHLENBQUM3SSxDQUFDLENBQUNvRSxXQUFGLENBQWN0RyxLQUFkLEtBQXdCNlIsV0FBVyxDQUFDLENBQUQsQ0FBWCxDQUFlRSxVQUF2QyxHQUFvREYsV0FBVyxDQUFDN1IsS0FBWixFQUFyRCxJQUE0RSxDQUFDLENBQTFGO0FBQ0gsU0FGRCxNQUVPO0FBQ0grSyxVQUFBQSxVQUFVLEdBQUksQ0FBZDtBQUNIO0FBQ0osT0FORCxNQU1PO0FBQ0hBLFFBQUFBLFVBQVUsR0FBRzhHLFdBQVcsQ0FBQyxDQUFELENBQVgsR0FBaUJBLFdBQVcsQ0FBQyxDQUFELENBQVgsQ0FBZUUsVUFBZixHQUE0QixDQUFDLENBQTlDLEdBQWtELENBQS9EO0FBQ0g7O0FBRUQsVUFBSTdQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBN0IsRUFBbUM7QUFDL0IsWUFBSVosQ0FBQyxDQUFDa0UsVUFBRixJQUFnQmxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTFCLElBQTBDdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixLQUFyRSxFQUE0RTtBQUN4RWlPLFVBQUFBLFdBQVcsR0FBRzNQLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsY0FBdkIsRUFBdUNILEVBQXZDLENBQTBDMEcsVUFBMUMsQ0FBZDtBQUNILFNBRkQsTUFFTztBQUNIZ0IsVUFBQUEsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixjQUF2QixFQUF1Q0gsRUFBdkMsQ0FBMEMwRyxVQUFVLEdBQUczTyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF2QixHQUFzQyxDQUFoRixDQUFkO0FBQ0g7O0FBRUQsWUFBSXZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEIsY0FBSXVOLFdBQVcsQ0FBQyxDQUFELENBQWYsRUFBb0I7QUFDaEI5RyxZQUFBQSxVQUFVLEdBQUcsQ0FBQzdJLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3RHLEtBQWQsS0FBd0I2UixXQUFXLENBQUMsQ0FBRCxDQUFYLENBQWVFLFVBQXZDLEdBQW9ERixXQUFXLENBQUM3UixLQUFaLEVBQXJELElBQTRFLENBQUMsQ0FBMUY7QUFDSCxXQUZELE1BRU87QUFDSCtLLFlBQUFBLFVBQVUsR0FBSSxDQUFkO0FBQ0g7QUFDSixTQU5ELE1BTU87QUFDSEEsVUFBQUEsVUFBVSxHQUFHOEcsV0FBVyxDQUFDLENBQUQsQ0FBWCxHQUFpQkEsV0FBVyxDQUFDLENBQUQsQ0FBWCxDQUFlRSxVQUFmLEdBQTRCLENBQUMsQ0FBOUMsR0FBa0QsQ0FBL0Q7QUFDSDs7QUFFRGhILFFBQUFBLFVBQVUsSUFBSSxDQUFDN0ksQ0FBQyxDQUFDeUUsS0FBRixDQUFRM0csS0FBUixLQUFrQjZSLFdBQVcsQ0FBQ0csVUFBWixFQUFuQixJQUErQyxDQUE3RDtBQUNIO0FBQ0o7O0FBRUQsV0FBT2pILFVBQVA7QUFFSCxHQTdGRDs7QUErRkFqSixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCMkksU0FBaEIsR0FBNEJuUSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNEksY0FBaEIsR0FBaUMsVUFBU0MsTUFBVCxFQUFpQjtBQUUxRSxRQUFJalEsQ0FBQyxHQUFHLElBQVI7O0FBRUEsV0FBT0EsQ0FBQyxDQUFDdkMsT0FBRixDQUFVd1MsTUFBVixDQUFQO0FBRUgsR0FORDs7QUFRQXJRLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J3RyxtQkFBaEIsR0FBc0MsWUFBVztBQUU3QyxRQUFJNU4sQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJcVAsVUFBVSxHQUFHLENBRGpCO0FBQUEsUUFFSUMsT0FBTyxHQUFHLENBRmQ7QUFBQSxRQUdJWSxPQUFPLEdBQUcsRUFIZDtBQUFBLFFBSUlDLEdBSko7O0FBTUEsUUFBSW5RLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUJ5TyxNQUFBQSxHQUFHLEdBQUduUSxDQUFDLENBQUNrRSxVQUFSO0FBQ0gsS0FGRCxNQUVPO0FBQ0htTCxNQUFBQSxVQUFVLEdBQUdyUCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFWLEdBQTJCLENBQUMsQ0FBekM7QUFDQThNLE1BQUFBLE9BQU8sR0FBR3RQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQVYsR0FBMkIsQ0FBQyxDQUF0QztBQUNBMk4sTUFBQUEsR0FBRyxHQUFHblEsQ0FBQyxDQUFDa0UsVUFBRixHQUFlLENBQXJCO0FBQ0g7O0FBRUQsV0FBT21MLFVBQVUsR0FBR2MsR0FBcEIsRUFBeUI7QUFDckJELE1BQUFBLE9BQU8sQ0FBQ0UsSUFBUixDQUFhZixVQUFiO0FBQ0FBLE1BQUFBLFVBQVUsR0FBR0MsT0FBTyxHQUFHdFAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBakM7QUFDQThNLE1BQUFBLE9BQU8sSUFBSXRQLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQVYsSUFBNEJ4QyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF0QyxHQUFxRHZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQS9ELEdBQWdGeEMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBckc7QUFDSDs7QUFFRCxXQUFPMk4sT0FBUDtBQUVILEdBeEJEOztBQTBCQXRRLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JpSixRQUFoQixHQUEyQixZQUFXO0FBRWxDLFdBQU8sSUFBUDtBQUVILEdBSkQ7O0FBTUF6USxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCa0osYUFBaEIsR0FBZ0MsWUFBVztBQUV2QyxRQUFJdFEsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJdVEsZUFESjtBQUFBLFFBQ3FCQyxXQURyQjtBQUFBLFFBQ2tDQyxZQURsQzs7QUFHQUEsSUFBQUEsWUFBWSxHQUFHelEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUF6QixHQUFnQ1osQ0FBQyxDQUFDbUUsVUFBRixHQUFlbUYsSUFBSSxDQUFDc0csS0FBTCxDQUFXNVAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUFwQyxDQUEvQyxHQUF3RixDQUF2Rzs7QUFFQSxRQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUYsWUFBVixLQUEyQixJQUEvQixFQUFxQztBQUNqQzFDLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2tELElBQWQsQ0FBbUIsY0FBbkIsRUFBbUMxSSxJQUFuQyxDQUF3QyxVQUFTK0ksS0FBVCxFQUFnQnRGLEtBQWhCLEVBQXVCO0FBQzNELFlBQUlBLEtBQUssQ0FBQ3dOLFVBQU4sR0FBbUJZLFlBQW5CLEdBQW1DblQsQ0FBQyxDQUFDK0UsS0FBRCxDQUFELENBQVN5TixVQUFULEtBQXdCLENBQTNELEdBQWlFOVAsQ0FBQyxDQUFDd0UsU0FBRixHQUFjLENBQUMsQ0FBcEYsRUFBd0Y7QUFDcEZnTSxVQUFBQSxXQUFXLEdBQUduTyxLQUFkO0FBQ0EsaUJBQU8sS0FBUDtBQUNIO0FBQ0osT0FMRDs7QUFPQWtPLE1BQUFBLGVBQWUsR0FBR2pILElBQUksQ0FBQ29ILEdBQUwsQ0FBU3BULENBQUMsQ0FBQ2tULFdBQUQsQ0FBRCxDQUFlakosSUFBZixDQUFvQixrQkFBcEIsSUFBMEN2SCxDQUFDLENBQUMwRCxZQUFyRCxLQUFzRSxDQUF4RjtBQUVBLGFBQU82TSxlQUFQO0FBRUgsS0FaRCxNQVlPO0FBQ0gsYUFBT3ZRLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQWpCO0FBQ0g7QUFFSixHQXZCRDs7QUF5QkE1QyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCdUosSUFBaEIsR0FBdUIvUSxLQUFLLENBQUN3SCxTQUFOLENBQWdCd0osU0FBaEIsR0FBNEIsVUFBU3ZPLEtBQVQsRUFBZ0I0SyxXQUFoQixFQUE2QjtBQUU1RSxRQUFJak4sQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzBHLFdBQUYsQ0FBYztBQUNWVCxNQUFBQSxJQUFJLEVBQUU7QUFDRnVILFFBQUFBLE9BQU8sRUFBRSxPQURQO0FBRUY3RixRQUFBQSxLQUFLLEVBQUVrSixRQUFRLENBQUN4TyxLQUFEO0FBRmI7QUFESSxLQUFkLEVBS0c0SyxXQUxIO0FBT0gsR0FYRDs7QUFhQXJOLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JELElBQWhCLEdBQXVCLFVBQVMySixRQUFULEVBQW1CO0FBRXRDLFFBQUk5USxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJLENBQUMxQyxDQUFDLENBQUMwQyxDQUFDLENBQUMwRixPQUFILENBQUQsQ0FBYXFMLFFBQWIsQ0FBc0IsbUJBQXRCLENBQUwsRUFBaUQ7QUFFN0N6VCxNQUFBQSxDQUFDLENBQUMwQyxDQUFDLENBQUMwRixPQUFILENBQUQsQ0FBYWhILFFBQWIsQ0FBc0IsbUJBQXRCOztBQUVBc0IsTUFBQUEsQ0FBQyxDQUFDb0wsU0FBRjs7QUFDQXBMLE1BQUFBLENBQUMsQ0FBQytLLFFBQUY7O0FBQ0EvSyxNQUFBQSxDQUFDLENBQUNnUixRQUFGOztBQUNBaFIsTUFBQUEsQ0FBQyxDQUFDaVIsU0FBRjs7QUFDQWpSLE1BQUFBLENBQUMsQ0FBQ2tSLFVBQUY7O0FBQ0FsUixNQUFBQSxDQUFDLENBQUNtUixnQkFBRjs7QUFDQW5SLE1BQUFBLENBQUMsQ0FBQ29SLFlBQUY7O0FBQ0FwUixNQUFBQSxDQUFDLENBQUNrTCxVQUFGOztBQUNBbEwsTUFBQUEsQ0FBQyxDQUFDa00sZUFBRixDQUFrQixJQUFsQjs7QUFDQWxNLE1BQUFBLENBQUMsQ0FBQ2lQLFlBQUY7QUFFSDs7QUFFRCxRQUFJNkIsUUFBSixFQUFjO0FBQ1Y5USxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLE1BQWxCLEVBQTBCLENBQUMvTSxDQUFELENBQTFCO0FBQ0g7O0FBRUQsUUFBSUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUMsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ0YsTUFBQUEsQ0FBQyxDQUFDcVIsT0FBRjtBQUNIOztBQUVELFFBQUtyUixDQUFDLENBQUN2QyxPQUFGLENBQVVpRCxRQUFmLEVBQTBCO0FBRXRCVixNQUFBQSxDQUFDLENBQUNzRixNQUFGLEdBQVcsS0FBWDs7QUFDQXRGLE1BQUFBLENBQUMsQ0FBQ3NHLFFBQUY7QUFFSDtBQUVKLEdBcENEOztBQXNDQTFHLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JpSyxPQUFoQixHQUEwQixZQUFXO0FBQ2pDLFFBQUlyUixDQUFDLEdBQUcsSUFBUjs7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDcUUsT0FBRixDQUFVcUcsR0FBVixDQUFjMUssQ0FBQyxDQUFDb0UsV0FBRixDQUFja0QsSUFBZCxDQUFtQixlQUFuQixDQUFkLEVBQW1EQyxJQUFuRCxDQUF3RDtBQUNwRCxxQkFBZSxNQURxQztBQUVwRCxrQkFBWTtBQUZ3QyxLQUF4RCxFQUdHRCxJQUhILENBR1EsMEJBSFIsRUFHb0NDLElBSHBDLENBR3lDO0FBQ3JDLGtCQUFZO0FBRHlCLEtBSHpDOztBQU9BdkgsSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjbUQsSUFBZCxDQUFtQixNQUFuQixFQUEyQixTQUEzQjs7QUFFQXZILElBQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlGLEdBQVYsQ0FBYzlKLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2tELElBQWQsQ0FBbUIsZUFBbkIsQ0FBZCxFQUFtRDFJLElBQW5ELENBQXdELFVBQVNxQyxDQUFULEVBQVk7QUFDaEUzRCxNQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpSyxJQUFSLENBQWE7QUFDVCxnQkFBUSxRQURDO0FBRVQsNEJBQW9CLGdCQUFnQnZILENBQUMsQ0FBQ0YsV0FBbEIsR0FBZ0NtQixDQUFoQyxHQUFvQztBQUYvQyxPQUFiO0FBSUgsS0FMRDs7QUFPQSxRQUFJakIsQ0FBQyxDQUFDNEQsS0FBRixLQUFZLElBQWhCLEVBQXNCO0FBQ2xCNUQsTUFBQUEsQ0FBQyxDQUFDNEQsS0FBRixDQUFRMkQsSUFBUixDQUFhLE1BQWIsRUFBcUIsU0FBckIsRUFBZ0NELElBQWhDLENBQXFDLElBQXJDLEVBQTJDMUksSUFBM0MsQ0FBZ0QsVUFBU3FDLENBQVQsRUFBWTtBQUN4RDNELFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlLLElBQVIsQ0FBYTtBQUNULGtCQUFRLGNBREM7QUFFVCwyQkFBaUIsT0FGUjtBQUdULDJCQUFpQixlQUFldkgsQ0FBQyxDQUFDRixXQUFqQixHQUErQm1CLENBQS9CLEdBQW1DLEVBSDNDO0FBSVQsZ0JBQU0sZ0JBQWdCakIsQ0FBQyxDQUFDRixXQUFsQixHQUFnQ21CLENBQWhDLEdBQW9DO0FBSmpDLFNBQWI7QUFNSCxPQVBELEVBUUs2SixLQVJMLEdBUWF2RCxJQVJiLENBUWtCLGVBUmxCLEVBUW1DLE1BUm5DLEVBUTJDK0osR0FSM0MsR0FTS2hLLElBVEwsQ0FTVSxRQVRWLEVBU29CQyxJQVRwQixDQVN5QixNQVR6QixFQVNpQyxRQVRqQyxFQVMyQytKLEdBVDNDLEdBVUtsUyxPQVZMLENBVWEsS0FWYixFQVVvQm1JLElBVnBCLENBVXlCLE1BVnpCLEVBVWlDLFNBVmpDO0FBV0g7O0FBQ0R2SCxJQUFBQSxDQUFDLENBQUNxSCxXQUFGO0FBRUgsR0FqQ0Q7O0FBbUNBekgsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQm1LLGVBQWhCLEdBQWtDLFlBQVc7QUFFekMsUUFBSXZSLENBQUMsR0FBRyxJQUFSOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTZDLE1BQVYsS0FBcUIsSUFBckIsSUFBNkJOLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTFELEVBQXdFO0FBQ3BFdkMsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUNJOEosR0FESixDQUNRLGFBRFIsRUFFSTdPLEVBRkosQ0FFTyxhQUZQLEVBRXNCO0FBQ2RzTyxRQUFBQSxPQUFPLEVBQUU7QUFESyxPQUZ0QixFQUlNeE4sQ0FBQyxDQUFDMEcsV0FKUjs7QUFLQTFHLE1BQUFBLENBQUMsQ0FBQ2dFLFVBQUYsQ0FDSStKLEdBREosQ0FDUSxhQURSLEVBRUk3TyxFQUZKLENBRU8sYUFGUCxFQUVzQjtBQUNkc08sUUFBQUEsT0FBTyxFQUFFO0FBREssT0FGdEIsRUFJTXhOLENBQUMsQ0FBQzBHLFdBSlI7QUFLSDtBQUVKLEdBakJEOztBQW1CQTlHLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JvSyxhQUFoQixHQUFnQyxZQUFXO0FBRXZDLFFBQUl4UixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVUwRCxJQUFWLEtBQW1CLElBQW5CLElBQTJCbkIsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBeEQsRUFBc0U7QUFDbEVqRixNQUFBQSxDQUFDLENBQUMsSUFBRCxFQUFPMEMsQ0FBQyxDQUFDNEQsS0FBVCxDQUFELENBQWlCMUUsRUFBakIsQ0FBb0IsYUFBcEIsRUFBbUM7QUFDL0JzTyxRQUFBQSxPQUFPLEVBQUU7QUFEc0IsT0FBbkMsRUFFR3hOLENBQUMsQ0FBQzBHLFdBRkw7QUFHSDs7QUFFRCxRQUFLMUcsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEQsSUFBVixLQUFtQixJQUFuQixJQUEyQm5CLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVFLGdCQUFWLEtBQStCLElBQS9ELEVBQXNFO0FBRWxFMUUsTUFBQUEsQ0FBQyxDQUFDLElBQUQsRUFBTzBDLENBQUMsQ0FBQzRELEtBQVQsQ0FBRCxDQUNLMUUsRUFETCxDQUNRLGtCQURSLEVBQzRCNUIsQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDZ08sU0FBVixFQUFxQmhPLENBQXJCLEVBQXdCLElBQXhCLENBRDVCLEVBRUtkLEVBRkwsQ0FFUSxrQkFGUixFQUU0QjVCLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixLQUF4QixDQUY1QjtBQUlIO0FBRUosR0FsQkQ7O0FBb0JBSixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCcUssZUFBaEIsR0FBa0MsWUFBVztBQUV6QyxRQUFJelIsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBS0EsQ0FBQyxDQUFDdkMsT0FBRixDQUFVcUUsWUFBZixFQUE4QjtBQUUxQjlCLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXZGLEVBQVIsQ0FBVyxrQkFBWCxFQUErQjVCLENBQUMsQ0FBQ2lKLEtBQUYsQ0FBUXZHLENBQUMsQ0FBQ2dPLFNBQVYsRUFBcUJoTyxDQUFyQixFQUF3QixJQUF4QixDQUEvQjs7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRdkYsRUFBUixDQUFXLGtCQUFYLEVBQStCNUIsQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDZ08sU0FBVixFQUFxQmhPLENBQXJCLEVBQXdCLEtBQXhCLENBQS9CO0FBRUg7QUFFSixHQVhEOztBQWFBSixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCK0osZ0JBQWhCLEdBQW1DLFlBQVc7QUFFMUMsUUFBSW5SLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUN1UixlQUFGOztBQUVBdlIsSUFBQUEsQ0FBQyxDQUFDd1IsYUFBRjs7QUFDQXhSLElBQUFBLENBQUMsQ0FBQ3lSLGVBQUY7O0FBRUF6UixJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsa0NBQVgsRUFBK0M7QUFDM0N3UyxNQUFBQSxNQUFNLEVBQUU7QUFEbUMsS0FBL0MsRUFFRzFSLENBQUMsQ0FBQzhHLFlBRkw7O0FBR0E5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsaUNBQVgsRUFBOEM7QUFDMUN3UyxNQUFBQSxNQUFNLEVBQUU7QUFEa0MsS0FBOUMsRUFFRzFSLENBQUMsQ0FBQzhHLFlBRkw7O0FBR0E5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsOEJBQVgsRUFBMkM7QUFDdkN3UyxNQUFBQSxNQUFNLEVBQUU7QUFEK0IsS0FBM0MsRUFFRzFSLENBQUMsQ0FBQzhHLFlBRkw7O0FBR0E5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsb0NBQVgsRUFBaUQ7QUFDN0N3UyxNQUFBQSxNQUFNLEVBQUU7QUFEcUMsS0FBakQsRUFFRzFSLENBQUMsQ0FBQzhHLFlBRkw7O0FBSUE5RyxJQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVF2RixFQUFSLENBQVcsYUFBWCxFQUEwQmMsQ0FBQyxDQUFDMkcsWUFBNUI7O0FBRUFySixJQUFBQSxDQUFDLENBQUM2SSxRQUFELENBQUQsQ0FBWWpILEVBQVosQ0FBZWMsQ0FBQyxDQUFDOEYsZ0JBQWpCLEVBQW1DeEksQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDaU8sVUFBVixFQUFzQmpPLENBQXRCLENBQW5DOztBQUVBLFFBQUlBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlDLGFBQVYsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbENGLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUXZGLEVBQVIsQ0FBVyxlQUFYLEVBQTRCYyxDQUFDLENBQUNnSCxVQUE5QjtBQUNIOztBQUVELFFBQUloSCxDQUFDLENBQUN2QyxPQUFGLENBQVVnRSxhQUFWLEtBQTRCLElBQWhDLEVBQXNDO0FBQ2xDbkUsTUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDb0UsV0FBSCxDQUFELENBQWlCZ0UsUUFBakIsR0FBNEJsSixFQUE1QixDQUErQixhQUEvQixFQUE4Q2MsQ0FBQyxDQUFDNEcsYUFBaEQ7QUFDSDs7QUFFRHRKLElBQUFBLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVWCxFQUFWLENBQWEsbUNBQW1DYyxDQUFDLENBQUNGLFdBQWxELEVBQStEeEMsQ0FBQyxDQUFDaUosS0FBRixDQUFRdkcsQ0FBQyxDQUFDbU8saUJBQVYsRUFBNkJuTyxDQUE3QixDQUEvRDtBQUVBMUMsSUFBQUEsQ0FBQyxDQUFDdUMsTUFBRCxDQUFELENBQVVYLEVBQVYsQ0FBYSx3QkFBd0JjLENBQUMsQ0FBQ0YsV0FBdkMsRUFBb0R4QyxDQUFDLENBQUNpSixLQUFGLENBQVF2RyxDQUFDLENBQUNvTyxNQUFWLEVBQWtCcE8sQ0FBbEIsQ0FBcEQ7QUFFQTFDLElBQUFBLENBQUMsQ0FBQyxtQkFBRCxFQUFzQjBDLENBQUMsQ0FBQ29FLFdBQXhCLENBQUQsQ0FBc0NsRixFQUF0QyxDQUF5QyxXQUF6QyxFQUFzRGMsQ0FBQyxDQUFDdU4sY0FBeEQ7QUFFQWpRLElBQUFBLENBQUMsQ0FBQ3VDLE1BQUQsQ0FBRCxDQUFVWCxFQUFWLENBQWEsc0JBQXNCYyxDQUFDLENBQUNGLFdBQXJDLEVBQWtERSxDQUFDLENBQUM2RyxXQUFwRDtBQUNBdkosSUFBQUEsQ0FBQyxDQUFDNkksUUFBRCxDQUFELENBQVlqSCxFQUFaLENBQWUsdUJBQXVCYyxDQUFDLENBQUNGLFdBQXhDLEVBQXFERSxDQUFDLENBQUM2RyxXQUF2RDtBQUVILEdBM0NEOztBQTZDQWpILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J1SyxNQUFoQixHQUF5QixZQUFXO0FBRWhDLFFBQUkzUixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVU2QyxNQUFWLEtBQXFCLElBQXJCLElBQTZCTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExRCxFQUF3RTtBQUVwRXZDLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYTJOLElBQWI7O0FBQ0E1UixNQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWE0TixJQUFiO0FBRUg7O0FBRUQsUUFBSTVSLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBELElBQVYsS0FBbUIsSUFBbkIsSUFBMkJuQixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF4RCxFQUFzRTtBQUVsRXZDLE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsQ0FBUWdPLElBQVI7QUFFSDtBQUVKLEdBakJEOztBQW1CQWhTLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JKLFVBQWhCLEdBQTZCLFVBQVNnRyxLQUFULEVBQWdCO0FBRXpDLFFBQUloTixDQUFDLEdBQUcsSUFBUixDQUZ5QyxDQUd4Qzs7O0FBQ0QsUUFBRyxDQUFDZ04sS0FBSyxDQUFDakQsTUFBTixDQUFhOEgsT0FBYixDQUFxQkMsS0FBckIsQ0FBMkIsdUJBQTNCLENBQUosRUFBeUQ7QUFDckQsVUFBSTlFLEtBQUssQ0FBQytFLE9BQU4sS0FBa0IsRUFBbEIsSUFBd0IvUixDQUFDLENBQUN2QyxPQUFGLENBQVV5QyxhQUFWLEtBQTRCLElBQXhELEVBQThEO0FBQzFERixRQUFBQSxDQUFDLENBQUMwRyxXQUFGLENBQWM7QUFDVlQsVUFBQUEsSUFBSSxFQUFFO0FBQ0Z1SCxZQUFBQSxPQUFPLEVBQUV4TixDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLElBQWxCLEdBQXlCLE1BQXpCLEdBQW1DO0FBRDFDO0FBREksU0FBZDtBQUtILE9BTkQsTUFNTyxJQUFJNEssS0FBSyxDQUFDK0UsT0FBTixLQUFrQixFQUFsQixJQUF3Qi9SLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlDLGFBQVYsS0FBNEIsSUFBeEQsRUFBOEQ7QUFDakVGLFFBQUFBLENBQUMsQ0FBQzBHLFdBQUYsQ0FBYztBQUNWVCxVQUFBQSxJQUFJLEVBQUU7QUFDRnVILFlBQUFBLE9BQU8sRUFBRXhOLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBbEIsR0FBeUIsVUFBekIsR0FBc0M7QUFEN0M7QUFESSxTQUFkO0FBS0g7QUFDSjtBQUVKLEdBcEJEOztBQXNCQXhDLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J4RixRQUFoQixHQUEyQixZQUFXO0FBRWxDLFFBQUk1QixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0lnUyxTQURKO0FBQUEsUUFDZUMsVUFEZjtBQUFBLFFBQzJCQyxVQUQzQjtBQUFBLFFBQ3VDQyxRQUR2Qzs7QUFHQSxhQUFTQyxVQUFULENBQW9CQyxXQUFwQixFQUFpQztBQUU3Qi9VLE1BQUFBLENBQUMsQ0FBQyxnQkFBRCxFQUFtQitVLFdBQW5CLENBQUQsQ0FBaUN6VCxJQUFqQyxDQUFzQyxZQUFXO0FBRTdDLFlBQUkwVCxLQUFLLEdBQUdoVixDQUFDLENBQUMsSUFBRCxDQUFiO0FBQUEsWUFDSWlWLFdBQVcsR0FBR2pWLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlLLElBQVIsQ0FBYSxXQUFiLENBRGxCO0FBQUEsWUFFSWlMLFdBQVcsR0FBR3JNLFFBQVEsQ0FBQzBGLGFBQVQsQ0FBdUIsS0FBdkIsQ0FGbEI7O0FBSUEyRyxRQUFBQSxXQUFXLENBQUNDLE1BQVosR0FBcUIsWUFBVztBQUU1QkgsVUFBQUEsS0FBSyxDQUNBM0osT0FETCxDQUNhO0FBQUVpRyxZQUFBQSxPQUFPLEVBQUU7QUFBWCxXQURiLEVBQzZCLEdBRDdCLEVBQ2tDLFlBQVc7QUFDckMwRCxZQUFBQSxLQUFLLENBQ0EvSyxJQURMLENBQ1UsS0FEVixFQUNpQmdMLFdBRGpCLEVBRUs1SixPQUZMLENBRWE7QUFBRWlHLGNBQUFBLE9BQU8sRUFBRTtBQUFYLGFBRmIsRUFFNkIsR0FGN0IsRUFFa0MsWUFBVztBQUNyQzBELGNBQUFBLEtBQUssQ0FDQTlILFVBREwsQ0FDZ0IsV0FEaEIsRUFFS0QsV0FGTCxDQUVpQixlQUZqQjtBQUdILGFBTkw7O0FBT0F2SyxZQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLFlBQWxCLEVBQWdDLENBQUMvTSxDQUFELEVBQUlzUyxLQUFKLEVBQVdDLFdBQVgsQ0FBaEM7QUFDSCxXQVZMO0FBWUgsU0FkRDs7QUFnQkFDLFFBQUFBLFdBQVcsQ0FBQ0UsT0FBWixHQUFzQixZQUFXO0FBRTdCSixVQUFBQSxLQUFLLENBQ0E5SCxVQURMLENBQ2lCLFdBRGpCLEVBRUtELFdBRkwsQ0FFa0IsZUFGbEIsRUFHSzdMLFFBSEwsQ0FHZSxzQkFIZjs7QUFLQXNCLFVBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsZUFBbEIsRUFBbUMsQ0FBRS9NLENBQUYsRUFBS3NTLEtBQUwsRUFBWUMsV0FBWixDQUFuQztBQUVILFNBVEQ7O0FBV0FDLFFBQUFBLFdBQVcsQ0FBQ0csR0FBWixHQUFrQkosV0FBbEI7QUFFSCxPQW5DRDtBQXFDSDs7QUFFRCxRQUFJdlMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUE3QixFQUFtQztBQUMvQixVQUFJWixDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLElBQTNCLEVBQWlDO0FBQzdCd1EsUUFBQUEsVUFBVSxHQUFHbFMsQ0FBQyxDQUFDMEQsWUFBRixJQUFrQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBekIsR0FBNkIsQ0FBL0MsQ0FBYjtBQUNBNFAsUUFBQUEsUUFBUSxHQUFHRCxVQUFVLEdBQUdsUyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF2QixHQUFzQyxDQUFqRDtBQUNILE9BSEQsTUFHTztBQUNIMlAsUUFBQUEsVUFBVSxHQUFHNUksSUFBSSxDQUFDNkcsR0FBTCxDQUFTLENBQVQsRUFBWW5RLENBQUMsQ0FBQzBELFlBQUYsSUFBa0IxRCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCLENBQXpCLEdBQTZCLENBQS9DLENBQVosQ0FBYjtBQUNBNFAsUUFBQUEsUUFBUSxHQUFHLEtBQUtuUyxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEdBQXlCLENBQXpCLEdBQTZCLENBQWxDLElBQXVDdkMsQ0FBQyxDQUFDMEQsWUFBcEQ7QUFDSDtBQUNKLEtBUkQsTUFRTztBQUNId08sTUFBQUEsVUFBVSxHQUFHbFMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixHQUFxQjFCLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUJ2QyxDQUFDLENBQUMwRCxZQUFoRCxHQUErRDFELENBQUMsQ0FBQzBELFlBQTlFO0FBQ0F5TyxNQUFBQSxRQUFRLEdBQUc3SSxJQUFJLENBQUNDLElBQUwsQ0FBVTJJLFVBQVUsR0FBR2xTLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQWpDLENBQVg7O0FBQ0EsVUFBSXZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsSUFBdkIsRUFBNkI7QUFDekIsWUFBSTBRLFVBQVUsR0FBRyxDQUFqQixFQUFvQkEsVUFBVTtBQUM5QixZQUFJQyxRQUFRLElBQUluUyxDQUFDLENBQUNrRSxVQUFsQixFQUE4QmlPLFFBQVE7QUFDekM7QUFDSjs7QUFFREgsSUFBQUEsU0FBUyxHQUFHaFMsQ0FBQyxDQUFDMEYsT0FBRixDQUFVNEIsSUFBVixDQUFlLGNBQWYsRUFBK0JzTCxLQUEvQixDQUFxQ1YsVUFBckMsRUFBaURDLFFBQWpELENBQVo7QUFDQUMsSUFBQUEsVUFBVSxDQUFDSixTQUFELENBQVY7O0FBRUEsUUFBSWhTLENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE5QixFQUE0QztBQUN4QzBQLE1BQUFBLFVBQVUsR0FBR2pTLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTRCLElBQVYsQ0FBZSxjQUFmLENBQWI7QUFDQThLLE1BQUFBLFVBQVUsQ0FBQ0gsVUFBRCxDQUFWO0FBQ0gsS0FIRCxNQUlBLElBQUlqUyxDQUFDLENBQUMwRCxZQUFGLElBQWtCMUQsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBL0MsRUFBNkQ7QUFDekQwUCxNQUFBQSxVQUFVLEdBQUdqUyxDQUFDLENBQUMwRixPQUFGLENBQVU0QixJQUFWLENBQWUsZUFBZixFQUFnQ3NMLEtBQWhDLENBQXNDLENBQXRDLEVBQXlDNVMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBbkQsQ0FBYjtBQUNBNlAsTUFBQUEsVUFBVSxDQUFDSCxVQUFELENBQVY7QUFDSCxLQUhELE1BR08sSUFBSWpTLENBQUMsQ0FBQzBELFlBQUYsS0FBbUIsQ0FBdkIsRUFBMEI7QUFDN0J1TyxNQUFBQSxVQUFVLEdBQUdqUyxDQUFDLENBQUMwRixPQUFGLENBQVU0QixJQUFWLENBQWUsZUFBZixFQUFnQ3NMLEtBQWhDLENBQXNDNVMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUFDLENBQWhFLENBQWI7QUFDQTZQLE1BQUFBLFVBQVUsQ0FBQ0gsVUFBRCxDQUFWO0FBQ0g7QUFFSixHQTlFRDs7QUFnRkFyUyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCOEosVUFBaEIsR0FBNkIsWUFBVztBQUVwQyxRQUFJbFIsQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzZHLFdBQUY7O0FBRUE3RyxJQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCO0FBQ2Q2UCxNQUFBQSxPQUFPLEVBQUU7QUFESyxLQUFsQjs7QUFJQTVPLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVTZFLFdBQVYsQ0FBc0IsZUFBdEI7O0FBRUF2SyxJQUFBQSxDQUFDLENBQUMyUixNQUFGOztBQUVBLFFBQUkzUixDQUFDLENBQUN2QyxPQUFGLENBQVVtRSxRQUFWLEtBQXVCLGFBQTNCLEVBQTBDO0FBQ3RDNUIsTUFBQUEsQ0FBQyxDQUFDNlMsbUJBQUY7QUFDSDtBQUVKLEdBbEJEOztBQW9CQWpULEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IwTCxJQUFoQixHQUF1QmxULEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IyTCxTQUFoQixHQUE0QixZQUFXO0FBRTFELFFBQUkvUyxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDMEcsV0FBRixDQUFjO0FBQ1ZULE1BQUFBLElBQUksRUFBRTtBQUNGdUgsUUFBQUEsT0FBTyxFQUFFO0FBRFA7QUFESSxLQUFkO0FBTUgsR0FWRDs7QUFZQTVOLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IrRyxpQkFBaEIsR0FBb0MsWUFBVztBQUUzQyxRQUFJbk8sQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQ2tNLGVBQUY7O0FBQ0FsTSxJQUFBQSxDQUFDLENBQUM2RyxXQUFGO0FBRUgsR0FQRDs7QUFTQWpILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I0TCxLQUFoQixHQUF3QnBULEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I2TCxVQUFoQixHQUE2QixZQUFXO0FBRTVELFFBQUlqVCxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDd0csYUFBRjs7QUFDQXhHLElBQUFBLENBQUMsQ0FBQ3NGLE1BQUYsR0FBVyxJQUFYO0FBRUgsR0FQRDs7QUFTQTFGLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I4TCxJQUFoQixHQUF1QnRULEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IrTCxTQUFoQixHQUE0QixZQUFXO0FBRTFELFFBQUluVCxDQUFDLEdBQUcsSUFBUjs7QUFFQUEsSUFBQUEsQ0FBQyxDQUFDc0csUUFBRjs7QUFDQXRHLElBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlELFFBQVYsR0FBcUIsSUFBckI7QUFDQVYsSUFBQUEsQ0FBQyxDQUFDc0YsTUFBRixHQUFXLEtBQVg7QUFDQXRGLElBQUFBLENBQUMsQ0FBQ21GLFFBQUYsR0FBYSxLQUFiO0FBQ0FuRixJQUFBQSxDQUFDLENBQUNvRixXQUFGLEdBQWdCLEtBQWhCO0FBRUgsR0FWRDs7QUFZQXhGLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JnTSxTQUFoQixHQUE0QixVQUFTekwsS0FBVCxFQUFnQjtBQUV4QyxRQUFJM0gsQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSSxDQUFDQSxDQUFDLENBQUM0RSxTQUFQLEVBQW1CO0FBRWY1RSxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLGFBQWxCLEVBQWlDLENBQUMvTSxDQUFELEVBQUkySCxLQUFKLENBQWpDOztBQUVBM0gsTUFBQUEsQ0FBQyxDQUFDcUQsU0FBRixHQUFjLEtBQWQ7O0FBRUFyRCxNQUFBQSxDQUFDLENBQUM2RyxXQUFGOztBQUVBN0csTUFBQUEsQ0FBQyxDQUFDd0UsU0FBRixHQUFjLElBQWQ7O0FBRUEsVUFBS3hFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlELFFBQWYsRUFBMEI7QUFDdEJWLFFBQUFBLENBQUMsQ0FBQ3NHLFFBQUY7QUFDSDs7QUFFRCxVQUFJdEcsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUMsYUFBVixLQUE0QixJQUFoQyxFQUFzQztBQUNsQ0YsUUFBQUEsQ0FBQyxDQUFDcVIsT0FBRjtBQUNIO0FBRUo7QUFFSixHQXhCRDs7QUEwQkF6UixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCaU0sSUFBaEIsR0FBdUJ6VCxLQUFLLENBQUN3SCxTQUFOLENBQWdCa00sU0FBaEIsR0FBNEIsWUFBVztBQUUxRCxRQUFJdFQsQ0FBQyxHQUFHLElBQVI7O0FBRUFBLElBQUFBLENBQUMsQ0FBQzBHLFdBQUYsQ0FBYztBQUNWVCxNQUFBQSxJQUFJLEVBQUU7QUFDRnVILFFBQUFBLE9BQU8sRUFBRTtBQURQO0FBREksS0FBZDtBQU1ILEdBVkQ7O0FBWUE1TixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCbUcsY0FBaEIsR0FBaUMsVUFBU1AsS0FBVCxFQUFnQjtBQUU3Q0EsSUFBQUEsS0FBSyxDQUFDTyxjQUFOO0FBRUgsR0FKRDs7QUFNQTNOLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J5TCxtQkFBaEIsR0FBc0MsVUFBVVUsUUFBVixFQUFxQjtBQUV2REEsSUFBQUEsUUFBUSxHQUFHQSxRQUFRLElBQUksQ0FBdkI7O0FBRUEsUUFBSXZULENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXdULFdBQVcsR0FBR2xXLENBQUMsQ0FBRSxnQkFBRixFQUFvQjBDLENBQUMsQ0FBQzBGLE9BQXRCLENBRG5CO0FBQUEsUUFFSTRNLEtBRko7QUFBQSxRQUdJQyxXQUhKO0FBQUEsUUFJSUMsV0FKSjs7QUFNQSxRQUFLZ0IsV0FBVyxDQUFDMUwsTUFBakIsRUFBMEI7QUFFdEJ3SyxNQUFBQSxLQUFLLEdBQUdrQixXQUFXLENBQUMxSSxLQUFaLEVBQVI7QUFDQXlILE1BQUFBLFdBQVcsR0FBR0QsS0FBSyxDQUFDL0ssSUFBTixDQUFXLFdBQVgsQ0FBZDtBQUNBaUwsTUFBQUEsV0FBVyxHQUFHck0sUUFBUSxDQUFDMEYsYUFBVCxDQUF1QixLQUF2QixDQUFkOztBQUVBMkcsTUFBQUEsV0FBVyxDQUFDQyxNQUFaLEdBQXFCLFlBQVc7QUFFNUJILFFBQUFBLEtBQUssQ0FDQS9LLElBREwsQ0FDVyxLQURYLEVBQ2tCZ0wsV0FEbEIsRUFFSy9ILFVBRkwsQ0FFZ0IsV0FGaEIsRUFHS0QsV0FITCxDQUdpQixlQUhqQjs7QUFLQSxZQUFLdkssQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEMsY0FBVixLQUE2QixJQUFsQyxFQUF5QztBQUNyQ0gsVUFBQUEsQ0FBQyxDQUFDNkcsV0FBRjtBQUNIOztBQUVEN0csUUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixZQUFsQixFQUFnQyxDQUFFL00sQ0FBRixFQUFLc1MsS0FBTCxFQUFZQyxXQUFaLENBQWhDOztBQUNBdlMsUUFBQUEsQ0FBQyxDQUFDNlMsbUJBQUY7QUFFSCxPQWREOztBQWdCQUwsTUFBQUEsV0FBVyxDQUFDRSxPQUFaLEdBQXNCLFlBQVc7QUFFN0IsWUFBS2EsUUFBUSxHQUFHLENBQWhCLEVBQW9CO0FBRWhCOzs7OztBQUtBNUosVUFBQUEsVUFBVSxDQUFFLFlBQVc7QUFDbkIzSixZQUFBQSxDQUFDLENBQUM2UyxtQkFBRixDQUF1QlUsUUFBUSxHQUFHLENBQWxDO0FBQ0gsV0FGUyxFQUVQLEdBRk8sQ0FBVjtBQUlILFNBWEQsTUFXTztBQUVIakIsVUFBQUEsS0FBSyxDQUNBOUgsVUFETCxDQUNpQixXQURqQixFQUVLRCxXQUZMLENBRWtCLGVBRmxCLEVBR0s3TCxRQUhMLENBR2Usc0JBSGY7O0FBS0FzQixVQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLGVBQWxCLEVBQW1DLENBQUUvTSxDQUFGLEVBQUtzUyxLQUFMLEVBQVlDLFdBQVosQ0FBbkM7O0FBRUF2UyxVQUFBQSxDQUFDLENBQUM2UyxtQkFBRjtBQUVIO0FBRUosT0ExQkQ7O0FBNEJBTCxNQUFBQSxXQUFXLENBQUNHLEdBQVosR0FBa0JKLFdBQWxCO0FBRUgsS0FwREQsTUFvRE87QUFFSHZTLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsaUJBQWxCLEVBQXFDLENBQUUvTSxDQUFGLENBQXJDO0FBRUg7QUFFSixHQXBFRDs7QUFzRUFKLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IwRixPQUFoQixHQUEwQixVQUFVMkcsWUFBVixFQUF5QjtBQUUvQyxRQUFJelQsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUFjMEQsWUFBZDtBQUFBLFFBQTRCZ1EsZ0JBQTVCOztBQUVBQSxJQUFBQSxnQkFBZ0IsR0FBRzFULENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTVDLENBSitDLENBTS9DO0FBQ0E7O0FBQ0EsUUFBSSxDQUFDdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBWCxJQUF5QjFCLENBQUMsQ0FBQzBELFlBQUYsR0FBaUJnUSxnQkFBOUMsRUFBa0U7QUFDOUQxVCxNQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCZ1EsZ0JBQWpCO0FBQ0gsS0FWOEMsQ0FZL0M7OztBQUNBLFFBQUsxVCxDQUFDLENBQUNrRSxVQUFGLElBQWdCbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBL0IsRUFBOEM7QUFDMUN2QyxNQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCLENBQWpCO0FBRUg7O0FBRURBLElBQUFBLFlBQVksR0FBRzFELENBQUMsQ0FBQzBELFlBQWpCOztBQUVBMUQsSUFBQUEsQ0FBQyxDQUFDd08sT0FBRixDQUFVLElBQVY7O0FBRUFsUixJQUFBQSxDQUFDLENBQUNrQixNQUFGLENBQVN3QixDQUFULEVBQVlBLENBQUMsQ0FBQ29ELFFBQWQsRUFBd0I7QUFBRU0sTUFBQUEsWUFBWSxFQUFFQTtBQUFoQixLQUF4Qjs7QUFFQTFELElBQUFBLENBQUMsQ0FBQ21ILElBQUY7O0FBRUEsUUFBSSxDQUFDc00sWUFBTCxFQUFvQjtBQUVoQnpULE1BQUFBLENBQUMsQ0FBQzBHLFdBQUYsQ0FBYztBQUNWVCxRQUFBQSxJQUFJLEVBQUU7QUFDRnVILFVBQUFBLE9BQU8sRUFBRSxPQURQO0FBRUY3RixVQUFBQSxLQUFLLEVBQUVqRTtBQUZMO0FBREksT0FBZCxFQUtHLEtBTEg7QUFPSDtBQUVKLEdBckNEOztBQXVDQTlELEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JGLG1CQUFoQixHQUFzQyxZQUFXO0FBRTdDLFFBQUlsSCxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQWNxTSxVQUFkO0FBQUEsUUFBMEJzSCxpQkFBMUI7QUFBQSxRQUE2Q0MsQ0FBN0M7QUFBQSxRQUNJQyxrQkFBa0IsR0FBRzdULENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsSUFBd0IsSUFEakQ7O0FBR0EsUUFBSzVFLENBQUMsQ0FBQ3dXLElBQUYsQ0FBT0Qsa0JBQVAsTUFBK0IsT0FBL0IsSUFBMENBLGtCQUFrQixDQUFDL0wsTUFBbEUsRUFBMkU7QUFFdkU5SCxNQUFBQSxDQUFDLENBQUNpQyxTQUFGLEdBQWNqQyxDQUFDLENBQUN2QyxPQUFGLENBQVV3RSxTQUFWLElBQXVCLFFBQXJDOztBQUVBLFdBQU1vSyxVQUFOLElBQW9Cd0gsa0JBQXBCLEVBQXlDO0FBRXJDRCxRQUFBQSxDQUFDLEdBQUc1VCxDQUFDLENBQUNnRixXQUFGLENBQWM4QyxNQUFkLEdBQXFCLENBQXpCO0FBQ0E2TCxRQUFBQSxpQkFBaUIsR0FBR0Usa0JBQWtCLENBQUN4SCxVQUFELENBQWxCLENBQStCQSxVQUFuRDs7QUFFQSxZQUFJd0gsa0JBQWtCLENBQUNqSCxjQUFuQixDQUFrQ1AsVUFBbEMsQ0FBSixFQUFtRDtBQUUvQztBQUNBO0FBQ0EsaUJBQU91SCxDQUFDLElBQUksQ0FBWixFQUFnQjtBQUNaLGdCQUFJNVQsQ0FBQyxDQUFDZ0YsV0FBRixDQUFjNE8sQ0FBZCxLQUFvQjVULENBQUMsQ0FBQ2dGLFdBQUYsQ0FBYzRPLENBQWQsTUFBcUJELGlCQUE3QyxFQUFpRTtBQUM3RDNULGNBQUFBLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBYytPLE1BQWQsQ0FBcUJILENBQXJCLEVBQXVCLENBQXZCO0FBQ0g7O0FBQ0RBLFlBQUFBLENBQUM7QUFDSjs7QUFFRDVULFVBQUFBLENBQUMsQ0FBQ2dGLFdBQUYsQ0FBY29MLElBQWQsQ0FBbUJ1RCxpQkFBbkI7O0FBQ0EzVCxVQUFBQSxDQUFDLENBQUNpRixrQkFBRixDQUFxQjBPLGlCQUFyQixJQUEwQ0Usa0JBQWtCLENBQUN4SCxVQUFELENBQWxCLENBQStCOU4sUUFBekU7QUFFSDtBQUVKOztBQUVEeUIsTUFBQUEsQ0FBQyxDQUFDZ0YsV0FBRixDQUFjZ1AsSUFBZCxDQUFtQixVQUFTM0ksQ0FBVCxFQUFZQyxDQUFaLEVBQWU7QUFDOUIsZUFBU3RMLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW9FLFdBQVosR0FBNEJ3SixDQUFDLEdBQUNDLENBQTlCLEdBQWtDQSxDQUFDLEdBQUNELENBQTNDO0FBQ0gsT0FGRDtBQUlIO0FBRUosR0F0Q0Q7O0FBd0NBekwsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQm1CLE1BQWhCLEdBQXlCLFlBQVc7QUFFaEMsUUFBSXZJLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLEdBQ0lyRSxDQUFDLENBQUNvRSxXQUFGLENBQ0tnRSxRQURMLENBQ2NwSSxDQUFDLENBQUN2QyxPQUFGLENBQVU0RSxLQUR4QixFQUVLM0QsUUFGTCxDQUVjLGFBRmQsQ0FESjtBQUtBc0IsSUFBQUEsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDcUUsT0FBRixDQUFVeUQsTUFBekI7O0FBRUEsUUFBSTlILENBQUMsQ0FBQzBELFlBQUYsSUFBa0IxRCxDQUFDLENBQUNrRSxVQUFwQixJQUFrQ2xFLENBQUMsQ0FBQzBELFlBQUYsS0FBbUIsQ0FBekQsRUFBNEQ7QUFDeEQxRCxNQUFBQSxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQTVDO0FBQ0g7O0FBRUQsUUFBSXhDLENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE5QixFQUE0QztBQUN4Q3ZDLE1BQUFBLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIsQ0FBakI7QUFDSDs7QUFFRDFELElBQUFBLENBQUMsQ0FBQ2tILG1CQUFGOztBQUVBbEgsSUFBQUEsQ0FBQyxDQUFDZ1IsUUFBRjs7QUFDQWhSLElBQUFBLENBQUMsQ0FBQ2lMLGFBQUY7O0FBQ0FqTCxJQUFBQSxDQUFDLENBQUNzSyxXQUFGOztBQUNBdEssSUFBQUEsQ0FBQyxDQUFDb1IsWUFBRjs7QUFDQXBSLElBQUFBLENBQUMsQ0FBQ3VSLGVBQUY7O0FBQ0F2UixJQUFBQSxDQUFDLENBQUMySyxTQUFGOztBQUNBM0ssSUFBQUEsQ0FBQyxDQUFDa0wsVUFBRjs7QUFDQWxMLElBQUFBLENBQUMsQ0FBQ3dSLGFBQUY7O0FBQ0F4UixJQUFBQSxDQUFDLENBQUNrTyxrQkFBRjs7QUFDQWxPLElBQUFBLENBQUMsQ0FBQ3lSLGVBQUY7O0FBRUF6UixJQUFBQSxDQUFDLENBQUNrTSxlQUFGLENBQWtCLEtBQWxCLEVBQXlCLElBQXpCOztBQUVBLFFBQUlsTSxDQUFDLENBQUN2QyxPQUFGLENBQVVnRSxhQUFWLEtBQTRCLElBQWhDLEVBQXNDO0FBQ2xDbkUsTUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDb0UsV0FBSCxDQUFELENBQWlCZ0UsUUFBakIsR0FBNEJsSixFQUE1QixDQUErQixhQUEvQixFQUE4Q2MsQ0FBQyxDQUFDNEcsYUFBaEQ7QUFDSDs7QUFFRDVHLElBQUFBLENBQUMsQ0FBQ21MLGVBQUYsQ0FBa0IsT0FBT25MLENBQUMsQ0FBQzBELFlBQVQsS0FBMEIsUUFBMUIsR0FBcUMxRCxDQUFDLENBQUMwRCxZQUF2QyxHQUFzRCxDQUF4RTs7QUFFQTFELElBQUFBLENBQUMsQ0FBQzZHLFdBQUY7O0FBQ0E3RyxJQUFBQSxDQUFDLENBQUNpUCxZQUFGOztBQUVBalAsSUFBQUEsQ0FBQyxDQUFDc0YsTUFBRixHQUFXLENBQUN0RixDQUFDLENBQUN2QyxPQUFGLENBQVVpRCxRQUF0Qjs7QUFDQVYsSUFBQUEsQ0FBQyxDQUFDc0csUUFBRjs7QUFFQXRHLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVXFILE9BQVYsQ0FBa0IsUUFBbEIsRUFBNEIsQ0FBQy9NLENBQUQsQ0FBNUI7QUFFSCxHQWhERDs7QUFrREFKLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JnSCxNQUFoQixHQUF5QixZQUFXO0FBRWhDLFFBQUlwTyxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJMUMsQ0FBQyxDQUFDdUMsTUFBRCxDQUFELENBQVUvQixLQUFWLE9BQXNCa0MsQ0FBQyxDQUFDK0YsV0FBNUIsRUFBeUM7QUFDckNrTyxNQUFBQSxZQUFZLENBQUNqVSxDQUFDLENBQUNrVSxXQUFILENBQVo7QUFDQWxVLE1BQUFBLENBQUMsQ0FBQ2tVLFdBQUYsR0FBZ0JyVSxNQUFNLENBQUM4SixVQUFQLENBQWtCLFlBQVc7QUFDekMzSixRQUFBQSxDQUFDLENBQUMrRixXQUFGLEdBQWdCekksQ0FBQyxDQUFDdUMsTUFBRCxDQUFELENBQVUvQixLQUFWLEVBQWhCOztBQUNBa0MsUUFBQUEsQ0FBQyxDQUFDa00sZUFBRjs7QUFDQSxZQUFJLENBQUNsTSxDQUFDLENBQUM0RSxTQUFQLEVBQW1CO0FBQUU1RSxVQUFBQSxDQUFDLENBQUM2RyxXQUFGO0FBQWtCO0FBQzFDLE9BSmUsRUFJYixFQUphLENBQWhCO0FBS0g7QUFDSixHQVpEOztBQWNBakgsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQitNLFdBQWhCLEdBQThCdlUsS0FBSyxDQUFDd0gsU0FBTixDQUFnQmdOLFdBQWhCLEdBQThCLFVBQVN6TSxLQUFULEVBQWdCME0sWUFBaEIsRUFBOEJDLFNBQTlCLEVBQXlDO0FBRWpHLFFBQUl0VSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJLE9BQU8ySCxLQUFQLEtBQWtCLFNBQXRCLEVBQWlDO0FBQzdCME0sTUFBQUEsWUFBWSxHQUFHMU0sS0FBZjtBQUNBQSxNQUFBQSxLQUFLLEdBQUcwTSxZQUFZLEtBQUssSUFBakIsR0FBd0IsQ0FBeEIsR0FBNEJyVSxDQUFDLENBQUNrRSxVQUFGLEdBQWUsQ0FBbkQ7QUFDSCxLQUhELE1BR087QUFDSHlELE1BQUFBLEtBQUssR0FBRzBNLFlBQVksS0FBSyxJQUFqQixHQUF3QixFQUFFMU0sS0FBMUIsR0FBa0NBLEtBQTFDO0FBQ0g7O0FBRUQsUUFBSTNILENBQUMsQ0FBQ2tFLFVBQUYsR0FBZSxDQUFmLElBQW9CeUQsS0FBSyxHQUFHLENBQTVCLElBQWlDQSxLQUFLLEdBQUczSCxDQUFDLENBQUNrRSxVQUFGLEdBQWUsQ0FBNUQsRUFBK0Q7QUFDM0QsYUFBTyxLQUFQO0FBQ0g7O0FBRURsRSxJQUFBQSxDQUFDLENBQUM2SCxNQUFGOztBQUVBLFFBQUl5TSxTQUFTLEtBQUssSUFBbEIsRUFBd0I7QUFDcEJ0VSxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLEdBQXlCcUcsTUFBekI7QUFDSCxLQUZELE1BRU87QUFDSHpPLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsS0FBSzNLLE9BQUwsQ0FBYTRFLEtBQXBDLEVBQTJDNEYsRUFBM0MsQ0FBOENOLEtBQTlDLEVBQXFEOEcsTUFBckQ7QUFDSDs7QUFFRHpPLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUYsR0FBWXJFLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsS0FBSzNLLE9BQUwsQ0FBYTRFLEtBQXBDLENBQVo7O0FBRUFyQyxJQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNnRSxRQUFkLENBQXVCLEtBQUszSyxPQUFMLENBQWE0RSxLQUFwQyxFQUEyQ2dHLE1BQTNDOztBQUVBckksSUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFja0UsTUFBZCxDQUFxQnRJLENBQUMsQ0FBQ3FFLE9BQXZCOztBQUVBckUsSUFBQUEsQ0FBQyxDQUFDMkYsWUFBRixHQUFpQjNGLENBQUMsQ0FBQ3FFLE9BQW5COztBQUVBckUsSUFBQUEsQ0FBQyxDQUFDdUksTUFBRjtBQUVILEdBakNEOztBQW1DQTNJLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JtTixNQUFoQixHQUF5QixVQUFTQyxRQUFULEVBQW1CO0FBRXhDLFFBQUl4VSxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l5VSxhQUFhLEdBQUcsRUFEcEI7QUFBQSxRQUVJQyxDQUZKO0FBQUEsUUFFT0MsQ0FGUDs7QUFJQSxRQUFJM1UsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkUsR0FBVixLQUFrQixJQUF0QixFQUE0QjtBQUN4Qm9TLE1BQUFBLFFBQVEsR0FBRyxDQUFDQSxRQUFaO0FBQ0g7O0FBQ0RFLElBQUFBLENBQUMsR0FBRzFVLENBQUMsQ0FBQ3VGLFlBQUYsSUFBa0IsTUFBbEIsR0FBMkIrRCxJQUFJLENBQUNDLElBQUwsQ0FBVWlMLFFBQVYsSUFBc0IsSUFBakQsR0FBd0QsS0FBNUQ7QUFDQUcsSUFBQUEsQ0FBQyxHQUFHM1UsQ0FBQyxDQUFDdUYsWUFBRixJQUFrQixLQUFsQixHQUEwQitELElBQUksQ0FBQ0MsSUFBTCxDQUFVaUwsUUFBVixJQUFzQixJQUFoRCxHQUF1RCxLQUEzRDtBQUVBQyxJQUFBQSxhQUFhLENBQUN6VSxDQUFDLENBQUN1RixZQUFILENBQWIsR0FBZ0NpUCxRQUFoQzs7QUFFQSxRQUFJeFUsQ0FBQyxDQUFDMkUsaUJBQUYsS0FBd0IsS0FBNUIsRUFBbUM7QUFDL0IzRSxNQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNyRixHQUFkLENBQWtCMFYsYUFBbEI7QUFDSCxLQUZELE1BRU87QUFDSEEsTUFBQUEsYUFBYSxHQUFHLEVBQWhCOztBQUNBLFVBQUl6VSxDQUFDLENBQUNrRixjQUFGLEtBQXFCLEtBQXpCLEVBQWdDO0FBQzVCdVAsUUFBQUEsYUFBYSxDQUFDelUsQ0FBQyxDQUFDOEUsUUFBSCxDQUFiLEdBQTRCLGVBQWU0UCxDQUFmLEdBQW1CLElBQW5CLEdBQTBCQyxDQUExQixHQUE4QixHQUExRDs7QUFDQTNVLFFBQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3JGLEdBQWQsQ0FBa0IwVixhQUFsQjtBQUNILE9BSEQsTUFHTztBQUNIQSxRQUFBQSxhQUFhLENBQUN6VSxDQUFDLENBQUM4RSxRQUFILENBQWIsR0FBNEIsaUJBQWlCNFAsQ0FBakIsR0FBcUIsSUFBckIsR0FBNEJDLENBQTVCLEdBQWdDLFFBQTVEOztBQUNBM1UsUUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckYsR0FBZCxDQUFrQjBWLGFBQWxCO0FBQ0g7QUFDSjtBQUVKLEdBM0JEOztBQTZCQTdVLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0J3TixhQUFoQixHQUFnQyxZQUFXO0FBRXZDLFFBQUk1VSxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVV1RixRQUFWLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCLFVBQUloRCxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQTdCLEVBQW1DO0FBQy9CWixRQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVExRixHQUFSLENBQVk7QUFDUjhWLFVBQUFBLE9BQU8sRUFBRyxTQUFTN1UsQ0FBQyxDQUFDdkMsT0FBRixDQUFVb0Q7QUFEckIsU0FBWjtBQUdIO0FBQ0osS0FORCxNQU1PO0FBQ0hiLE1BQUFBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTFHLE1BQVIsQ0FBZWlDLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVXlHLEtBQVYsR0FBa0JwQyxXQUFsQixDQUE4QixJQUE5QixJQUFzQzFJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQS9EOztBQUNBLFVBQUl2QyxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQTdCLEVBQW1DO0FBQy9CWixRQUFBQSxDQUFDLENBQUN5RSxLQUFGLENBQVExRixHQUFSLENBQVk7QUFDUjhWLFVBQUFBLE9BQU8sRUFBRzdVLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW9ELGFBQVYsR0FBMEI7QUFENUIsU0FBWjtBQUdIO0FBQ0o7O0FBRURiLElBQUFBLENBQUMsQ0FBQzZELFNBQUYsR0FBYzdELENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTNHLEtBQVIsRUFBZDtBQUNBa0MsSUFBQUEsQ0FBQyxDQUFDOEQsVUFBRixHQUFlOUQsQ0FBQyxDQUFDeUUsS0FBRixDQUFRMUcsTUFBUixFQUFmOztBQUdBLFFBQUlpQyxDQUFDLENBQUN2QyxPQUFGLENBQVV1RixRQUFWLEtBQXVCLEtBQXZCLElBQWdDaEQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVc0YsYUFBVixLQUE0QixLQUFoRSxFQUF1RTtBQUNuRS9DLE1BQUFBLENBQUMsQ0FBQ21FLFVBQUYsR0FBZW1GLElBQUksQ0FBQ0MsSUFBTCxDQUFVdkosQ0FBQyxDQUFDNkQsU0FBRixHQUFjN0QsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBbEMsQ0FBZjs7QUFDQXZDLE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY3RHLEtBQWQsQ0FBb0J3TCxJQUFJLENBQUNDLElBQUwsQ0FBV3ZKLENBQUMsQ0FBQ21FLFVBQUYsR0FBZW5FLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsY0FBdkIsRUFBdUNOLE1BQWpFLENBQXBCO0FBRUgsS0FKRCxNQUlPLElBQUk5SCxDQUFDLENBQUN2QyxPQUFGLENBQVVzRixhQUFWLEtBQTRCLElBQWhDLEVBQXNDO0FBQ3pDL0MsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjdEcsS0FBZCxDQUFvQixPQUFPa0MsQ0FBQyxDQUFDa0UsVUFBN0I7QUFDSCxLQUZNLE1BRUE7QUFDSGxFLE1BQUFBLENBQUMsQ0FBQ21FLFVBQUYsR0FBZW1GLElBQUksQ0FBQ0MsSUFBTCxDQUFVdkosQ0FBQyxDQUFDNkQsU0FBWixDQUFmOztBQUNBN0QsTUFBQUEsQ0FBQyxDQUFDb0UsV0FBRixDQUFjckcsTUFBZCxDQUFxQnVMLElBQUksQ0FBQ0MsSUFBTCxDQUFXdkosQ0FBQyxDQUFDcUUsT0FBRixDQUFVeUcsS0FBVixHQUFrQnBDLFdBQWxCLENBQThCLElBQTlCLElBQXNDMUksQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixjQUF2QixFQUF1Q04sTUFBeEYsQ0FBckI7QUFDSDs7QUFFRCxRQUFJZ04sTUFBTSxHQUFHOVUsQ0FBQyxDQUFDcUUsT0FBRixDQUFVeUcsS0FBVixHQUFrQmdGLFVBQWxCLENBQTZCLElBQTdCLElBQXFDOVAsQ0FBQyxDQUFDcUUsT0FBRixDQUFVeUcsS0FBVixHQUFrQmhOLEtBQWxCLEVBQWxEOztBQUNBLFFBQUlrQyxDQUFDLENBQUN2QyxPQUFGLENBQVVzRixhQUFWLEtBQTRCLEtBQWhDLEVBQXVDL0MsQ0FBQyxDQUFDb0UsV0FBRixDQUFjZ0UsUUFBZCxDQUF1QixjQUF2QixFQUF1Q3RLLEtBQXZDLENBQTZDa0MsQ0FBQyxDQUFDbUUsVUFBRixHQUFlMlEsTUFBNUQ7QUFFMUMsR0FyQ0Q7O0FBdUNBbFYsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjJOLE9BQWhCLEdBQTBCLFlBQVc7QUFFakMsUUFBSS9VLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSTZJLFVBREo7O0FBR0E3SSxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQVV6RixJQUFWLENBQWUsVUFBUytJLEtBQVQsRUFBZ0I1SCxPQUFoQixFQUF5QjtBQUNwQzhJLE1BQUFBLFVBQVUsR0FBSTdJLENBQUMsQ0FBQ21FLFVBQUYsR0FBZXdELEtBQWhCLEdBQXlCLENBQUMsQ0FBdkM7O0FBQ0EsVUFBSTNILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEI5RSxRQUFBQSxDQUFDLENBQUN5QyxPQUFELENBQUQsQ0FBV2hCLEdBQVgsQ0FBZTtBQUNYeVYsVUFBQUEsUUFBUSxFQUFFLFVBREM7QUFFWFEsVUFBQUEsS0FBSyxFQUFFbk0sVUFGSTtBQUdYSSxVQUFBQSxHQUFHLEVBQUUsQ0FITTtBQUlYOUYsVUFBQUEsTUFBTSxFQUFFbkQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEYsTUFBVixHQUFtQixDQUpoQjtBQUtYeUwsVUFBQUEsT0FBTyxFQUFFO0FBTEUsU0FBZjtBQU9ILE9BUkQsTUFRTztBQUNIdFIsUUFBQUEsQ0FBQyxDQUFDeUMsT0FBRCxDQUFELENBQVdoQixHQUFYLENBQWU7QUFDWHlWLFVBQUFBLFFBQVEsRUFBRSxVQURDO0FBRVh4TCxVQUFBQSxJQUFJLEVBQUVILFVBRks7QUFHWEksVUFBQUEsR0FBRyxFQUFFLENBSE07QUFJWDlGLFVBQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUIsQ0FKaEI7QUFLWHlMLFVBQUFBLE9BQU8sRUFBRTtBQUxFLFNBQWY7QUFPSDtBQUNKLEtBbkJEOztBQXFCQTVPLElBQUFBLENBQUMsQ0FBQ3FFLE9BQUYsQ0FBVTRELEVBQVYsQ0FBYWpJLENBQUMsQ0FBQzBELFlBQWYsRUFBNkIzRSxHQUE3QixDQUFpQztBQUM3Qm9FLE1BQUFBLE1BQU0sRUFBRW5ELENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUIsQ0FERTtBQUU3QnlMLE1BQUFBLE9BQU8sRUFBRTtBQUZvQixLQUFqQztBQUtILEdBL0JEOztBQWlDQWhQLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I2TixTQUFoQixHQUE0QixZQUFXO0FBRW5DLFFBQUlqVixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUFWLEtBQTJCLENBQTNCLElBQWdDdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEMsY0FBVixLQUE2QixJQUE3RCxJQUFxRUgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVdUYsUUFBVixLQUF1QixLQUFoRyxFQUF1RztBQUNuRyxVQUFJeUYsWUFBWSxHQUFHekksQ0FBQyxDQUFDcUUsT0FBRixDQUFVNEQsRUFBVixDQUFhakksQ0FBQyxDQUFDMEQsWUFBZixFQUE2QmdGLFdBQTdCLENBQXlDLElBQXpDLENBQW5COztBQUNBMUksTUFBQUEsQ0FBQyxDQUFDeUUsS0FBRixDQUFRMUYsR0FBUixDQUFZLFFBQVosRUFBc0IwSixZQUF0QjtBQUNIO0FBRUosR0FURDs7QUFXQTdJLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I4TixTQUFoQixHQUNBdFYsS0FBSyxDQUFDd0gsU0FBTixDQUFnQitOLGNBQWhCLEdBQWlDLFlBQVc7QUFFeEM7Ozs7Ozs7Ozs7OztBQWFBLFFBQUluVixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQWM0VCxDQUFkO0FBQUEsUUFBaUJ3QixJQUFqQjtBQUFBLFFBQXVCbkYsTUFBdkI7QUFBQSxRQUErQm9GLEtBQS9CO0FBQUEsUUFBc0N2SSxPQUFPLEdBQUcsS0FBaEQ7QUFBQSxRQUF1RGdILElBQXZEOztBQUVBLFFBQUl4VyxDQUFDLENBQUN3VyxJQUFGLENBQVF3QixTQUFTLENBQUMsQ0FBRCxDQUFqQixNQUEyQixRQUEvQixFQUEwQztBQUV0Q3JGLE1BQUFBLE1BQU0sR0FBSXFGLFNBQVMsQ0FBQyxDQUFELENBQW5CO0FBQ0F4SSxNQUFBQSxPQUFPLEdBQUd3SSxTQUFTLENBQUMsQ0FBRCxDQUFuQjtBQUNBeEIsTUFBQUEsSUFBSSxHQUFHLFVBQVA7QUFFSCxLQU5ELE1BTU8sSUFBS3hXLENBQUMsQ0FBQ3dXLElBQUYsQ0FBUXdCLFNBQVMsQ0FBQyxDQUFELENBQWpCLE1BQTJCLFFBQWhDLEVBQTJDO0FBRTlDckYsTUFBQUEsTUFBTSxHQUFJcUYsU0FBUyxDQUFDLENBQUQsQ0FBbkI7QUFDQUQsTUFBQUEsS0FBSyxHQUFHQyxTQUFTLENBQUMsQ0FBRCxDQUFqQjtBQUNBeEksTUFBQUEsT0FBTyxHQUFHd0ksU0FBUyxDQUFDLENBQUQsQ0FBbkI7O0FBRUEsVUFBS0EsU0FBUyxDQUFDLENBQUQsQ0FBVCxLQUFpQixZQUFqQixJQUFpQ2hZLENBQUMsQ0FBQ3dXLElBQUYsQ0FBUXdCLFNBQVMsQ0FBQyxDQUFELENBQWpCLE1BQTJCLE9BQWpFLEVBQTJFO0FBRXZFeEIsUUFBQUEsSUFBSSxHQUFHLFlBQVA7QUFFSCxPQUpELE1BSU8sSUFBSyxPQUFPd0IsU0FBUyxDQUFDLENBQUQsQ0FBaEIsS0FBd0IsV0FBN0IsRUFBMkM7QUFFOUN4QixRQUFBQSxJQUFJLEdBQUcsUUFBUDtBQUVIO0FBRUo7O0FBRUQsUUFBS0EsSUFBSSxLQUFLLFFBQWQsRUFBeUI7QUFFckI5VCxNQUFBQSxDQUFDLENBQUN2QyxPQUFGLENBQVV3UyxNQUFWLElBQW9Cb0YsS0FBcEI7QUFHSCxLQUxELE1BS08sSUFBS3ZCLElBQUksS0FBSyxVQUFkLEVBQTJCO0FBRTlCeFcsTUFBQUEsQ0FBQyxDQUFDc0IsSUFBRixDQUFRcVIsTUFBUixFQUFpQixVQUFVc0YsR0FBVixFQUFlQyxHQUFmLEVBQXFCO0FBRWxDeFYsUUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOFgsR0FBVixJQUFpQkMsR0FBakI7QUFFSCxPQUpEO0FBT0gsS0FUTSxNQVNBLElBQUsxQixJQUFJLEtBQUssWUFBZCxFQUE2QjtBQUVoQyxXQUFNc0IsSUFBTixJQUFjQyxLQUFkLEVBQXNCO0FBRWxCLFlBQUkvWCxDQUFDLENBQUN3VyxJQUFGLENBQVE5VCxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFsQixNQUFtQyxPQUF2QyxFQUFpRDtBQUU3Q2xDLFVBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsR0FBdUIsQ0FBRW1ULEtBQUssQ0FBQ0QsSUFBRCxDQUFQLENBQXZCO0FBRUgsU0FKRCxNQUlPO0FBRUh4QixVQUFBQSxDQUFDLEdBQUc1VCxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLENBQXFCNEYsTUFBckIsR0FBNEIsQ0FBaEMsQ0FGRyxDQUlIOztBQUNBLGlCQUFPOEwsQ0FBQyxJQUFJLENBQVosRUFBZ0I7QUFFWixnQkFBSTVULENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXlFLFVBQVYsQ0FBcUIwUixDQUFyQixFQUF3QnZILFVBQXhCLEtBQXVDZ0osS0FBSyxDQUFDRCxJQUFELENBQUwsQ0FBWS9JLFVBQXZELEVBQW9FO0FBRWhFck0sY0FBQUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUUsVUFBVixDQUFxQjZSLE1BQXJCLENBQTRCSCxDQUE1QixFQUE4QixDQUE5QjtBQUVIOztBQUVEQSxZQUFBQSxDQUFDO0FBRUo7O0FBRUQ1VCxVQUFBQSxDQUFDLENBQUN2QyxPQUFGLENBQVV5RSxVQUFWLENBQXFCa08sSUFBckIsQ0FBMkJpRixLQUFLLENBQUNELElBQUQsQ0FBaEM7QUFFSDtBQUVKO0FBRUo7O0FBRUQsUUFBS3RJLE9BQUwsRUFBZTtBQUVYOU0sTUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjs7QUFDQTdILE1BQUFBLENBQUMsQ0FBQ3VJLE1BQUY7QUFFSDtBQUVKLEdBaEdEOztBQWtHQTNJLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JQLFdBQWhCLEdBQThCLFlBQVc7QUFFckMsUUFBSTdHLENBQUMsR0FBRyxJQUFSOztBQUVBQSxJQUFBQSxDQUFDLENBQUM0VSxhQUFGOztBQUVBNVUsSUFBQUEsQ0FBQyxDQUFDaVYsU0FBRjs7QUFFQSxRQUFJalYsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixLQUF2QixFQUE4QjtBQUMxQnhCLE1BQUFBLENBQUMsQ0FBQ3VVLE1BQUYsQ0FBU3ZVLENBQUMsQ0FBQ3dQLE9BQUYsQ0FBVXhQLENBQUMsQ0FBQzBELFlBQVosQ0FBVDtBQUNILEtBRkQsTUFFTztBQUNIMUQsTUFBQUEsQ0FBQyxDQUFDK1UsT0FBRjtBQUNIOztBQUVEL1UsSUFBQUEsQ0FBQyxDQUFDMEYsT0FBRixDQUFVcUgsT0FBVixDQUFrQixhQUFsQixFQUFpQyxDQUFDL00sQ0FBRCxDQUFqQztBQUVILEdBaEJEOztBQWtCQUosRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjRKLFFBQWhCLEdBQTJCLFlBQVc7QUFFbEMsUUFBSWhSLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXlWLFNBQVMsR0FBR3RQLFFBQVEsQ0FBQ3VQLElBQVQsQ0FBY0MsS0FEOUI7O0FBR0EzVixJQUFBQSxDQUFDLENBQUN1RixZQUFGLEdBQWlCdkYsQ0FBQyxDQUFDdkMsT0FBRixDQUFVdUYsUUFBVixLQUF1QixJQUF2QixHQUE4QixLQUE5QixHQUFzQyxNQUF2RDs7QUFFQSxRQUFJaEQsQ0FBQyxDQUFDdUYsWUFBRixLQUFtQixLQUF2QixFQUE4QjtBQUMxQnZGLE1BQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVWhILFFBQVYsQ0FBbUIsZ0JBQW5CO0FBQ0gsS0FGRCxNQUVPO0FBQ0hzQixNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVU2RSxXQUFWLENBQXNCLGdCQUF0QjtBQUNIOztBQUVELFFBQUlrTCxTQUFTLENBQUNHLGdCQUFWLEtBQStCQyxTQUEvQixJQUNBSixTQUFTLENBQUNLLGFBQVYsS0FBNEJELFNBRDVCLElBRUFKLFNBQVMsQ0FBQ00sWUFBVixLQUEyQkYsU0FGL0IsRUFFMEM7QUFDdEMsVUFBSTdWLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW9GLE1BQVYsS0FBcUIsSUFBekIsRUFBK0I7QUFDM0I3QyxRQUFBQSxDQUFDLENBQUNrRixjQUFGLEdBQW1CLElBQW5CO0FBQ0g7QUFDSjs7QUFFRCxRQUFLbEYsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBZixFQUFzQjtBQUNsQixVQUFLLE9BQU94QixDQUFDLENBQUN2QyxPQUFGLENBQVUwRixNQUFqQixLQUE0QixRQUFqQyxFQUE0QztBQUN4QyxZQUFJbkQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEYsTUFBVixHQUFtQixDQUF2QixFQUEyQjtBQUN2Qm5ELFVBQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBGLE1BQVYsR0FBbUIsQ0FBbkI7QUFDSDtBQUNKLE9BSkQsTUFJTztBQUNIbkQsUUFBQUEsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMEYsTUFBVixHQUFtQm5ELENBQUMsQ0FBQ3RDLFFBQUYsQ0FBV3lGLE1BQTlCO0FBQ0g7QUFDSjs7QUFFRCxRQUFJc1MsU0FBUyxDQUFDTyxVQUFWLEtBQXlCSCxTQUE3QixFQUF3QztBQUNwQzdWLE1BQUFBLENBQUMsQ0FBQzhFLFFBQUYsR0FBYSxZQUFiO0FBQ0E5RSxNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLGNBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLGFBQW5CO0FBQ0EsVUFBSTRQLFNBQVMsQ0FBQ1EsbUJBQVYsS0FBa0NKLFNBQWxDLElBQStDSixTQUFTLENBQUNTLGlCQUFWLEtBQWdDTCxTQUFuRixFQUE4RjdWLENBQUMsQ0FBQzhFLFFBQUYsR0FBYSxLQUFiO0FBQ2pHOztBQUNELFFBQUkyUSxTQUFTLENBQUNVLFlBQVYsS0FBMkJOLFNBQS9CLEVBQTBDO0FBQ3RDN1YsTUFBQUEsQ0FBQyxDQUFDOEUsUUFBRixHQUFhLGNBQWI7QUFDQTlFLE1BQUFBLENBQUMsQ0FBQzRGLGFBQUYsR0FBa0IsZ0JBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLGVBQW5CO0FBQ0EsVUFBSTRQLFNBQVMsQ0FBQ1EsbUJBQVYsS0FBa0NKLFNBQWxDLElBQStDSixTQUFTLENBQUNXLGNBQVYsS0FBNkJQLFNBQWhGLEVBQTJGN1YsQ0FBQyxDQUFDOEUsUUFBRixHQUFhLEtBQWI7QUFDOUY7O0FBQ0QsUUFBSTJRLFNBQVMsQ0FBQ1ksZUFBVixLQUE4QlIsU0FBbEMsRUFBNkM7QUFDekM3VixNQUFBQSxDQUFDLENBQUM4RSxRQUFGLEdBQWEsaUJBQWI7QUFDQTlFLE1BQUFBLENBQUMsQ0FBQzRGLGFBQUYsR0FBa0IsbUJBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLGtCQUFuQjtBQUNBLFVBQUk0UCxTQUFTLENBQUNRLG1CQUFWLEtBQWtDSixTQUFsQyxJQUErQ0osU0FBUyxDQUFDUyxpQkFBVixLQUFnQ0wsU0FBbkYsRUFBOEY3VixDQUFDLENBQUM4RSxRQUFGLEdBQWEsS0FBYjtBQUNqRzs7QUFDRCxRQUFJMlEsU0FBUyxDQUFDYSxXQUFWLEtBQTBCVCxTQUE5QixFQUF5QztBQUNyQzdWLE1BQUFBLENBQUMsQ0FBQzhFLFFBQUYsR0FBYSxhQUFiO0FBQ0E5RSxNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLGVBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLGNBQW5CO0FBQ0EsVUFBSTRQLFNBQVMsQ0FBQ2EsV0FBVixLQUEwQlQsU0FBOUIsRUFBeUM3VixDQUFDLENBQUM4RSxRQUFGLEdBQWEsS0FBYjtBQUM1Qzs7QUFDRCxRQUFJMlEsU0FBUyxDQUFDYyxTQUFWLEtBQXdCVixTQUF4QixJQUFxQzdWLENBQUMsQ0FBQzhFLFFBQUYsS0FBZSxLQUF4RCxFQUErRDtBQUMzRDlFLE1BQUFBLENBQUMsQ0FBQzhFLFFBQUYsR0FBYSxXQUFiO0FBQ0E5RSxNQUFBQSxDQUFDLENBQUM0RixhQUFGLEdBQWtCLFdBQWxCO0FBQ0E1RixNQUFBQSxDQUFDLENBQUM2RixjQUFGLEdBQW1CLFlBQW5CO0FBQ0g7O0FBQ0Q3RixJQUFBQSxDQUFDLENBQUMyRSxpQkFBRixHQUFzQjNFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXFGLFlBQVYsSUFBMkI5QyxDQUFDLENBQUM4RSxRQUFGLEtBQWUsSUFBZixJQUF1QjlFLENBQUMsQ0FBQzhFLFFBQUYsS0FBZSxLQUF2RjtBQUNILEdBN0REOztBQWdFQWxGLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0IrRCxlQUFoQixHQUFrQyxVQUFTeEQsS0FBVCxFQUFnQjtBQUU5QyxRQUFJM0gsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJeVEsWUFESjtBQUFBLFFBQ2tCK0YsU0FEbEI7QUFBQSxRQUM2QnBKLFdBRDdCO0FBQUEsUUFDMENxSixTQUQxQzs7QUFHQUQsSUFBQUEsU0FBUyxHQUFHeFcsQ0FBQyxDQUFDMEYsT0FBRixDQUNQNEIsSUFETyxDQUNGLGNBREUsRUFFUGlELFdBRk8sQ0FFSyx5Q0FGTCxFQUdQaEQsSUFITyxDQUdGLGFBSEUsRUFHYSxNQUhiLENBQVo7O0FBS0F2SCxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQ0s0RCxFQURMLENBQ1FOLEtBRFIsRUFFS2pKLFFBRkwsQ0FFYyxlQUZkOztBQUlBLFFBQUlzQixDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLElBQTdCLEVBQW1DO0FBRS9CNlAsTUFBQUEsWUFBWSxHQUFHbkgsSUFBSSxDQUFDc0csS0FBTCxDQUFXNVAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUFwQyxDQUFmOztBQUVBLFVBQUl2QyxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLElBQTNCLEVBQWlDO0FBRTdCLFlBQUlpRyxLQUFLLElBQUk4SSxZQUFULElBQXlCOUksS0FBSyxJQUFLM0gsQ0FBQyxDQUFDa0UsVUFBRixHQUFlLENBQWhCLEdBQXFCdU0sWUFBM0QsRUFBeUU7QUFFckV6USxVQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQ0t1TyxLQURMLENBQ1dqTCxLQUFLLEdBQUc4SSxZQURuQixFQUNpQzlJLEtBQUssR0FBRzhJLFlBQVIsR0FBdUIsQ0FEeEQsRUFFSy9SLFFBRkwsQ0FFYyxjQUZkLEVBR0s2SSxJQUhMLENBR1UsYUFIVixFQUd5QixPQUh6QjtBQUtILFNBUEQsTUFPTztBQUVINkYsVUFBQUEsV0FBVyxHQUFHcE4sQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5Qm9GLEtBQXZDO0FBQ0E2TyxVQUFBQSxTQUFTLENBQ0o1RCxLQURMLENBQ1d4RixXQUFXLEdBQUdxRCxZQUFkLEdBQTZCLENBRHhDLEVBQzJDckQsV0FBVyxHQUFHcUQsWUFBZCxHQUE2QixDQUR4RSxFQUVLL1IsUUFGTCxDQUVjLGNBRmQsRUFHSzZJLElBSEwsQ0FHVSxhQUhWLEVBR3lCLE9BSHpCO0FBS0g7O0FBRUQsWUFBSUksS0FBSyxLQUFLLENBQWQsRUFBaUI7QUFFYjZPLFVBQUFBLFNBQVMsQ0FDSnZPLEVBREwsQ0FDUXVPLFNBQVMsQ0FBQzFPLE1BQVYsR0FBbUIsQ0FBbkIsR0FBdUI5SCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUR6QyxFQUVLN0QsUUFGTCxDQUVjLGNBRmQ7QUFJSCxTQU5ELE1BTU8sSUFBSWlKLEtBQUssS0FBSzNILENBQUMsQ0FBQ2tFLFVBQUYsR0FBZSxDQUE3QixFQUFnQztBQUVuQ3NTLFVBQUFBLFNBQVMsQ0FDSnZPLEVBREwsQ0FDUWpJLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBRGxCLEVBRUs3RCxRQUZMLENBRWMsY0FGZDtBQUlIO0FBRUo7O0FBRURzQixNQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQ0s0RCxFQURMLENBQ1FOLEtBRFIsRUFFS2pKLFFBRkwsQ0FFYyxjQUZkO0FBSUgsS0EzQ0QsTUEyQ087QUFFSCxVQUFJaUosS0FBSyxJQUFJLENBQVQsSUFBY0EsS0FBSyxJQUFLM0gsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBckQsRUFBb0U7QUFFaEV2QyxRQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQ0t1TyxLQURMLENBQ1dqTCxLQURYLEVBQ2tCQSxLQUFLLEdBQUczSCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQURwQyxFQUVLN0QsUUFGTCxDQUVjLGNBRmQsRUFHSzZJLElBSEwsQ0FHVSxhQUhWLEVBR3lCLE9BSHpCO0FBS0gsT0FQRCxNQU9PLElBQUlpUCxTQUFTLENBQUMxTyxNQUFWLElBQW9COUgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBbEMsRUFBZ0Q7QUFFbkRpVSxRQUFBQSxTQUFTLENBQ0o5WCxRQURMLENBQ2MsY0FEZCxFQUVLNkksSUFGTCxDQUVVLGFBRlYsRUFFeUIsT0FGekI7QUFJSCxPQU5NLE1BTUE7QUFFSGtQLFFBQUFBLFNBQVMsR0FBR3pXLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQXJDO0FBQ0E2SyxRQUFBQSxXQUFXLEdBQUdwTixDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLElBQXZCLEdBQThCMUIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5Qm9GLEtBQXZELEdBQStEQSxLQUE3RTs7QUFFQSxZQUFJM0gsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixJQUEwQnZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXBDLElBQXVEeEMsQ0FBQyxDQUFDa0UsVUFBRixHQUFleUQsS0FBaEIsR0FBeUIzSCxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE3RixFQUEyRztBQUV2R2lVLFVBQUFBLFNBQVMsQ0FDSjVELEtBREwsQ0FDV3hGLFdBQVcsSUFBSXBOLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUJrVSxTQUE3QixDQUR0QixFQUMrRHJKLFdBQVcsR0FBR3FKLFNBRDdFLEVBRUsvWCxRQUZMLENBRWMsY0FGZCxFQUdLNkksSUFITCxDQUdVLGFBSFYsRUFHeUIsT0FIekI7QUFLSCxTQVBELE1BT087QUFFSGlQLFVBQUFBLFNBQVMsQ0FDSjVELEtBREwsQ0FDV3hGLFdBRFgsRUFDd0JBLFdBQVcsR0FBR3BOLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBRGhELEVBRUs3RCxRQUZMLENBRWMsY0FGZCxFQUdLNkksSUFITCxDQUdVLGFBSFYsRUFHeUIsT0FIekI7QUFLSDtBQUVKO0FBRUo7O0FBRUQsUUFBSXZILENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1FLFFBQVYsS0FBdUIsVUFBM0IsRUFBdUM7QUFDbkM1QixNQUFBQSxDQUFDLENBQUM0QixRQUFGO0FBQ0g7QUFFSixHQXJHRDs7QUF1R0FoQyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNkQsYUFBaEIsR0FBZ0MsWUFBVztBQUV2QyxRQUFJakwsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJaUIsQ0FESjtBQUFBLFFBQ08wTixVQURQO0FBQUEsUUFDbUIrSCxhQURuQjs7QUFHQSxRQUFJMVcsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixJQUF2QixFQUE2QjtBQUN6QnhCLE1BQUFBLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsR0FBdUIsS0FBdkI7QUFDSDs7QUFFRCxRQUFJWixDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLElBQXZCLElBQStCMUIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixLQUF0RCxFQUE2RDtBQUV6RG1OLE1BQUFBLFVBQVUsR0FBRyxJQUFiOztBQUVBLFVBQUkzTyxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE3QixFQUEyQztBQUV2QyxZQUFJdkMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixJQUE3QixFQUFtQztBQUMvQjhWLFVBQUFBLGFBQWEsR0FBRzFXLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQVYsR0FBeUIsQ0FBekM7QUFDSCxTQUZELE1BRU87QUFDSG1VLFVBQUFBLGFBQWEsR0FBRzFXLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTFCO0FBQ0g7O0FBRUQsYUFBS3RCLENBQUMsR0FBR2pCLENBQUMsQ0FBQ2tFLFVBQVgsRUFBdUJqRCxDQUFDLEdBQUlqQixDQUFDLENBQUNrRSxVQUFGLEdBQ3BCd1MsYUFEUixFQUN3QnpWLENBQUMsSUFBSSxDQUQ3QixFQUNnQztBQUM1QjBOLFVBQUFBLFVBQVUsR0FBRzFOLENBQUMsR0FBRyxDQUFqQjtBQUNBM0QsVUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDcUUsT0FBRixDQUFVc0ssVUFBVixDQUFELENBQUQsQ0FBeUJnSSxLQUF6QixDQUErQixJQUEvQixFQUFxQ3BQLElBQXJDLENBQTBDLElBQTFDLEVBQWdELEVBQWhELEVBQ0tBLElBREwsQ0FDVSxrQkFEVixFQUM4Qm9ILFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBRDdDLEVBRUtpRSxTQUZMLENBRWVuSSxDQUFDLENBQUNvRSxXQUZqQixFQUU4QjFGLFFBRjlCLENBRXVDLGNBRnZDO0FBR0g7O0FBQ0QsYUFBS3VDLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR3lWLGFBQWhCLEVBQStCelYsQ0FBQyxJQUFJLENBQXBDLEVBQXVDO0FBQ25DME4sVUFBQUEsVUFBVSxHQUFHMU4sQ0FBYjtBQUNBM0QsVUFBQUEsQ0FBQyxDQUFDMEMsQ0FBQyxDQUFDcUUsT0FBRixDQUFVc0ssVUFBVixDQUFELENBQUQsQ0FBeUJnSSxLQUF6QixDQUErQixJQUEvQixFQUFxQ3BQLElBQXJDLENBQTBDLElBQTFDLEVBQWdELEVBQWhELEVBQ0tBLElBREwsQ0FDVSxrQkFEVixFQUM4Qm9ILFVBQVUsR0FBRzNPLENBQUMsQ0FBQ2tFLFVBRDdDLEVBRUs2RCxRQUZMLENBRWMvSCxDQUFDLENBQUNvRSxXQUZoQixFQUU2QjFGLFFBRjdCLENBRXNDLGNBRnRDO0FBR0g7O0FBQ0RzQixRQUFBQSxDQUFDLENBQUNvRSxXQUFGLENBQWNrRCxJQUFkLENBQW1CLGVBQW5CLEVBQW9DQSxJQUFwQyxDQUF5QyxNQUF6QyxFQUFpRDFJLElBQWpELENBQXNELFlBQVc7QUFDN0R0QixVQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpSyxJQUFSLENBQWEsSUFBYixFQUFtQixFQUFuQjtBQUNILFNBRkQ7QUFJSDtBQUVKO0FBRUosR0ExQ0Q7O0FBNENBM0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjRHLFNBQWhCLEdBQTRCLFVBQVU0SSxNQUFWLEVBQW1CO0FBRTNDLFFBQUk1VyxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJLENBQUM0VyxNQUFMLEVBQWM7QUFDVjVXLE1BQUFBLENBQUMsQ0FBQ3NHLFFBQUY7QUFDSDs7QUFDRHRHLElBQUFBLENBQUMsQ0FBQ29GLFdBQUYsR0FBZ0J3UixNQUFoQjtBQUVILEdBVEQ7O0FBV0FoWCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCUixhQUFoQixHQUFnQyxVQUFTb0csS0FBVCxFQUFnQjtBQUU1QyxRQUFJaE4sQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBSTZXLGFBQWEsR0FDYnZaLENBQUMsQ0FBQzBQLEtBQUssQ0FBQ2pELE1BQVAsQ0FBRCxDQUFnQnVELEVBQWhCLENBQW1CLGNBQW5CLElBQ0loUSxDQUFDLENBQUMwUCxLQUFLLENBQUNqRCxNQUFQLENBREwsR0FFSXpNLENBQUMsQ0FBQzBQLEtBQUssQ0FBQ2pELE1BQVAsQ0FBRCxDQUFnQitNLE9BQWhCLENBQXdCLGNBQXhCLENBSFI7QUFLQSxRQUFJblAsS0FBSyxHQUFHa0osUUFBUSxDQUFDZ0csYUFBYSxDQUFDdFAsSUFBZCxDQUFtQixrQkFBbkIsQ0FBRCxDQUFwQjtBQUVBLFFBQUksQ0FBQ0ksS0FBTCxFQUFZQSxLQUFLLEdBQUcsQ0FBUjs7QUFFWixRQUFJM0gsQ0FBQyxDQUFDa0UsVUFBRixJQUFnQmxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQTlCLEVBQTRDO0FBRXhDdkMsTUFBQUEsQ0FBQyxDQUFDbUwsZUFBRixDQUFrQnhELEtBQWxCOztBQUNBM0gsTUFBQUEsQ0FBQyxDQUFDTyxRQUFGLENBQVdvSCxLQUFYOztBQUNBO0FBRUg7O0FBRUQzSCxJQUFBQSxDQUFDLENBQUNpSyxZQUFGLENBQWV0QyxLQUFmO0FBRUgsR0F2QkQ7O0FBeUJBL0gsRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQjZDLFlBQWhCLEdBQStCLFVBQVN0QyxLQUFULEVBQWdCb1AsSUFBaEIsRUFBc0I5SixXQUF0QixFQUFtQztBQUU5RCxRQUFJMEMsV0FBSjtBQUFBLFFBQWlCcUgsU0FBakI7QUFBQSxRQUE0QkMsUUFBNUI7QUFBQSxRQUFzQ0MsU0FBdEM7QUFBQSxRQUFpRHJPLFVBQVUsR0FBRyxJQUE5RDtBQUFBLFFBQ0k3SSxDQUFDLEdBQUcsSUFEUjtBQUFBLFFBQ2NtWCxTQURkOztBQUdBSixJQUFBQSxJQUFJLEdBQUdBLElBQUksSUFBSSxLQUFmOztBQUVBLFFBQUkvVyxDQUFDLENBQUNxRCxTQUFGLEtBQWdCLElBQWhCLElBQXdCckQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVeUYsY0FBVixLQUE2QixJQUF6RCxFQUErRDtBQUMzRDtBQUNIOztBQUVELFFBQUlsRCxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLElBQW5CLElBQTJCeEIsQ0FBQyxDQUFDMEQsWUFBRixLQUFtQmlFLEtBQWxELEVBQXlEO0FBQ3JEO0FBQ0g7O0FBRUQsUUFBSTNILENBQUMsQ0FBQ2tFLFVBQUYsSUFBZ0JsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUE5QixFQUE0QztBQUN4QztBQUNIOztBQUVELFFBQUl3VSxJQUFJLEtBQUssS0FBYixFQUFvQjtBQUNoQi9XLE1BQUFBLENBQUMsQ0FBQ08sUUFBRixDQUFXb0gsS0FBWDtBQUNIOztBQUVEZ0ksSUFBQUEsV0FBVyxHQUFHaEksS0FBZDtBQUNBa0IsSUFBQUEsVUFBVSxHQUFHN0ksQ0FBQyxDQUFDd1AsT0FBRixDQUFVRyxXQUFWLENBQWI7QUFDQXVILElBQUFBLFNBQVMsR0FBR2xYLENBQUMsQ0FBQ3dQLE9BQUYsQ0FBVXhQLENBQUMsQ0FBQzBELFlBQVosQ0FBWjtBQUVBMUQsSUFBQUEsQ0FBQyxDQUFDeUQsV0FBRixHQUFnQnpELENBQUMsQ0FBQ3dFLFNBQUYsS0FBZ0IsSUFBaEIsR0FBdUIwUyxTQUF2QixHQUFtQ2xYLENBQUMsQ0FBQ3dFLFNBQXJEOztBQUVBLFFBQUl4RSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLEtBQXZCLElBQWdDMUIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVbUQsVUFBVixLQUF5QixLQUF6RCxLQUFtRStHLEtBQUssR0FBRyxDQUFSLElBQWFBLEtBQUssR0FBRzNILENBQUMsQ0FBQzZLLFdBQUYsS0FBa0I3SyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFwSCxDQUFKLEVBQXlJO0FBQ3JJLFVBQUl4QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCbU8sUUFBQUEsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDMEQsWUFBaEI7O0FBQ0EsWUFBSXVKLFdBQVcsS0FBSyxJQUFwQixFQUEwQjtBQUN0QmpOLFVBQUFBLENBQUMsQ0FBQzRJLFlBQUYsQ0FBZXNPLFNBQWYsRUFBMEIsWUFBVztBQUNqQ2xYLFlBQUFBLENBQUMsQ0FBQ29ULFNBQUYsQ0FBWXpELFdBQVo7QUFDSCxXQUZEO0FBR0gsU0FKRCxNQUlPO0FBQ0gzUCxVQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVl6RCxXQUFaO0FBQ0g7QUFDSjs7QUFDRDtBQUNILEtBWkQsTUFZTyxJQUFJM1AsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUUsUUFBVixLQUF1QixLQUF2QixJQUFnQzFCLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBekQsS0FBa0UrRyxLQUFLLEdBQUcsQ0FBUixJQUFhQSxLQUFLLEdBQUkzSCxDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVUrRSxjQUFqSCxDQUFKLEVBQXVJO0FBQzFJLFVBQUl4QyxDQUFDLENBQUN2QyxPQUFGLENBQVUrRCxJQUFWLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCbU8sUUFBQUEsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDMEQsWUFBaEI7O0FBQ0EsWUFBSXVKLFdBQVcsS0FBSyxJQUFwQixFQUEwQjtBQUN0QmpOLFVBQUFBLENBQUMsQ0FBQzRJLFlBQUYsQ0FBZXNPLFNBQWYsRUFBMEIsWUFBVztBQUNqQ2xYLFlBQUFBLENBQUMsQ0FBQ29ULFNBQUYsQ0FBWXpELFdBQVo7QUFDSCxXQUZEO0FBR0gsU0FKRCxNQUlPO0FBQ0gzUCxVQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVl6RCxXQUFaO0FBQ0g7QUFDSjs7QUFDRDtBQUNIOztBQUVELFFBQUszUCxDQUFDLENBQUN2QyxPQUFGLENBQVVpRCxRQUFmLEVBQTBCO0FBQ3RCMEosTUFBQUEsYUFBYSxDQUFDcEssQ0FBQyxDQUFDdUQsYUFBSCxDQUFiO0FBQ0g7O0FBRUQsUUFBSW9NLFdBQVcsR0FBRyxDQUFsQixFQUFxQjtBQUNqQixVQUFJM1AsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBekIsS0FBNEMsQ0FBaEQsRUFBbUQ7QUFDL0N3VSxRQUFBQSxTQUFTLEdBQUdoWCxDQUFDLENBQUNrRSxVQUFGLEdBQWdCbEUsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBckQ7QUFDSCxPQUZELE1BRU87QUFDSHdVLFFBQUFBLFNBQVMsR0FBR2hYLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZXlMLFdBQTNCO0FBQ0g7QUFDSixLQU5ELE1BTU8sSUFBSUEsV0FBVyxJQUFJM1AsQ0FBQyxDQUFDa0UsVUFBckIsRUFBaUM7QUFDcEMsVUFBSWxFLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStFLGNBQXpCLEtBQTRDLENBQWhELEVBQW1EO0FBQy9Dd1UsUUFBQUEsU0FBUyxHQUFHLENBQVo7QUFDSCxPQUZELE1BRU87QUFDSEEsUUFBQUEsU0FBUyxHQUFHckgsV0FBVyxHQUFHM1AsQ0FBQyxDQUFDa0UsVUFBNUI7QUFDSDtBQUNKLEtBTk0sTUFNQTtBQUNIOFMsTUFBQUEsU0FBUyxHQUFHckgsV0FBWjtBQUNIOztBQUVEM1AsSUFBQUEsQ0FBQyxDQUFDcUQsU0FBRixHQUFjLElBQWQ7O0FBRUFyRCxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLGNBQWxCLEVBQWtDLENBQUMvTSxDQUFELEVBQUlBLENBQUMsQ0FBQzBELFlBQU4sRUFBb0JzVCxTQUFwQixDQUFsQzs7QUFFQUMsSUFBQUEsUUFBUSxHQUFHalgsQ0FBQyxDQUFDMEQsWUFBYjtBQUNBMUQsSUFBQUEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQnNULFNBQWpCOztBQUVBaFgsSUFBQUEsQ0FBQyxDQUFDbUwsZUFBRixDQUFrQm5MLENBQUMsQ0FBQzBELFlBQXBCOztBQUVBLFFBQUsxRCxDQUFDLENBQUN2QyxPQUFGLENBQVU4QyxRQUFmLEVBQTBCO0FBRXRCNFcsTUFBQUEsU0FBUyxHQUFHblgsQ0FBQyxDQUFDNkosWUFBRixFQUFaO0FBQ0FzTixNQUFBQSxTQUFTLEdBQUdBLFNBQVMsQ0FBQ25OLEtBQVYsQ0FBZ0IsVUFBaEIsQ0FBWjs7QUFFQSxVQUFLbU4sU0FBUyxDQUFDalQsVUFBVixJQUF3QmlULFNBQVMsQ0FBQzFaLE9BQVYsQ0FBa0I4RSxZQUEvQyxFQUE4RDtBQUMxRDRVLFFBQUFBLFNBQVMsQ0FBQ2hNLGVBQVYsQ0FBMEJuTCxDQUFDLENBQUMwRCxZQUE1QjtBQUNIO0FBRUo7O0FBRUQxRCxJQUFBQSxDQUFDLENBQUNrTCxVQUFGOztBQUNBbEwsSUFBQUEsQ0FBQyxDQUFDb1IsWUFBRjs7QUFFQSxRQUFJcFIsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0QsSUFBVixLQUFtQixJQUF2QixFQUE2QjtBQUN6QixVQUFJeUwsV0FBVyxLQUFLLElBQXBCLEVBQTBCO0FBRXRCak4sUUFBQUEsQ0FBQyxDQUFDNk8sWUFBRixDQUFlb0ksUUFBZjs7QUFFQWpYLFFBQUFBLENBQUMsQ0FBQzBPLFNBQUYsQ0FBWXNJLFNBQVosRUFBdUIsWUFBVztBQUM5QmhYLFVBQUFBLENBQUMsQ0FBQ29ULFNBQUYsQ0FBWTRELFNBQVo7QUFDSCxTQUZEO0FBSUgsT0FSRCxNQVFPO0FBQ0hoWCxRQUFBQSxDQUFDLENBQUNvVCxTQUFGLENBQVk0RCxTQUFaO0FBQ0g7O0FBQ0RoWCxNQUFBQSxDQUFDLENBQUN3SSxhQUFGOztBQUNBO0FBQ0g7O0FBRUQsUUFBSXlFLFdBQVcsS0FBSyxJQUFwQixFQUEwQjtBQUN0QmpOLE1BQUFBLENBQUMsQ0FBQzRJLFlBQUYsQ0FBZUMsVUFBZixFQUEyQixZQUFXO0FBQ2xDN0ksUUFBQUEsQ0FBQyxDQUFDb1QsU0FBRixDQUFZNEQsU0FBWjtBQUNILE9BRkQ7QUFHSCxLQUpELE1BSU87QUFDSGhYLE1BQUFBLENBQUMsQ0FBQ29ULFNBQUYsQ0FBWTRELFNBQVo7QUFDSDtBQUVKLEdBMUhEOztBQTRIQXBYLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I2SixTQUFoQixHQUE0QixZQUFXO0FBRW5DLFFBQUlqUixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUN2QyxPQUFGLENBQVU2QyxNQUFWLEtBQXFCLElBQXJCLElBQTZCTixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUExRCxFQUF3RTtBQUVwRXZDLE1BQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYW1ULElBQWI7O0FBQ0FwWCxNQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWFvVCxJQUFiO0FBRUg7O0FBRUQsUUFBSXBYLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTBELElBQVYsS0FBbUIsSUFBbkIsSUFBMkJuQixDQUFDLENBQUNrRSxVQUFGLEdBQWVsRSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RSxZQUF4RCxFQUFzRTtBQUVsRXZDLE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsQ0FBUXdULElBQVI7QUFFSDs7QUFFRHBYLElBQUFBLENBQUMsQ0FBQzBGLE9BQUYsQ0FBVWhILFFBQVYsQ0FBbUIsZUFBbkI7QUFFSCxHQW5CRDs7QUFxQkFrQixFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCaVEsY0FBaEIsR0FBaUMsWUFBVztBQUV4QyxRQUFJQyxLQUFKO0FBQUEsUUFBV0MsS0FBWDtBQUFBLFFBQWtCQyxDQUFsQjtBQUFBLFFBQXFCQyxVQUFyQjtBQUFBLFFBQWlDelgsQ0FBQyxHQUFHLElBQXJDOztBQUVBc1gsSUFBQUEsS0FBSyxHQUFHdFgsQ0FBQyxDQUFDMEUsV0FBRixDQUFjZ1QsTUFBZCxHQUF1QjFYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2lULElBQTdDO0FBQ0FKLElBQUFBLEtBQUssR0FBR3ZYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2tULE1BQWQsR0FBdUI1WCxDQUFDLENBQUMwRSxXQUFGLENBQWNtVCxJQUE3QztBQUNBTCxJQUFBQSxDQUFDLEdBQUdsTyxJQUFJLENBQUN3TyxLQUFMLENBQVdQLEtBQVgsRUFBa0JELEtBQWxCLENBQUo7QUFFQUcsSUFBQUEsVUFBVSxHQUFHbk8sSUFBSSxDQUFDeU8sS0FBTCxDQUFXUCxDQUFDLEdBQUcsR0FBSixHQUFVbE8sSUFBSSxDQUFDME8sRUFBMUIsQ0FBYjs7QUFDQSxRQUFJUCxVQUFVLEdBQUcsQ0FBakIsRUFBb0I7QUFDaEJBLE1BQUFBLFVBQVUsR0FBRyxNQUFNbk8sSUFBSSxDQUFDb0gsR0FBTCxDQUFTK0csVUFBVCxDQUFuQjtBQUNIOztBQUVELFFBQUtBLFVBQVUsSUFBSSxFQUFmLElBQXVCQSxVQUFVLElBQUksQ0FBekMsRUFBNkM7QUFDekMsYUFBUXpYLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVTJFLEdBQVYsS0FBa0IsS0FBbEIsR0FBMEIsTUFBMUIsR0FBbUMsT0FBM0M7QUFDSDs7QUFDRCxRQUFLcVYsVUFBVSxJQUFJLEdBQWYsSUFBd0JBLFVBQVUsSUFBSSxHQUExQyxFQUFnRDtBQUM1QyxhQUFRelgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVMkUsR0FBVixLQUFrQixLQUFsQixHQUEwQixNQUExQixHQUFtQyxPQUEzQztBQUNIOztBQUNELFFBQUtxVixVQUFVLElBQUksR0FBZixJQUF3QkEsVUFBVSxJQUFJLEdBQTFDLEVBQWdEO0FBQzVDLGFBQVF6WCxDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLEtBQWxCLEdBQTBCLE9BQTFCLEdBQW9DLE1BQTVDO0FBQ0g7O0FBQ0QsUUFBSXBDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXdGLGVBQVYsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcEMsVUFBS3dVLFVBQVUsSUFBSSxFQUFmLElBQXVCQSxVQUFVLElBQUksR0FBekMsRUFBK0M7QUFDM0MsZUFBTyxNQUFQO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsZUFBTyxJQUFQO0FBQ0g7QUFDSjs7QUFFRCxXQUFPLFVBQVA7QUFFSCxHQWhDRDs7QUFrQ0E3WCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNlEsUUFBaEIsR0FBMkIsVUFBU2pMLEtBQVQsRUFBZ0I7QUFFdkMsUUFBSWhOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSWtFLFVBREo7QUFBQSxRQUVJUCxTQUZKOztBQUlBM0QsSUFBQUEsQ0FBQyxDQUFDc0QsUUFBRixHQUFhLEtBQWI7QUFDQXRELElBQUFBLENBQUMsQ0FBQ29GLFdBQUYsR0FBZ0IsS0FBaEI7QUFDQXBGLElBQUFBLENBQUMsQ0FBQ3lGLFdBQUYsR0FBa0J6RixDQUFDLENBQUMwRSxXQUFGLENBQWN3VCxXQUFkLEdBQTRCLEVBQTlCLEdBQXFDLEtBQXJDLEdBQTZDLElBQTdEOztBQUVBLFFBQUtsWSxDQUFDLENBQUMwRSxXQUFGLENBQWNpVCxJQUFkLEtBQXVCOUIsU0FBNUIsRUFBd0M7QUFDcEMsYUFBTyxLQUFQO0FBQ0g7O0FBRUQsUUFBSzdWLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3lULE9BQWQsS0FBMEIsSUFBL0IsRUFBc0M7QUFDbENuWSxNQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLE1BQWxCLEVBQTBCLENBQUMvTSxDQUFELEVBQUlBLENBQUMsQ0FBQ3FYLGNBQUYsRUFBSixDQUExQjtBQUNIOztBQUVELFFBQUtyWCxDQUFDLENBQUMwRSxXQUFGLENBQWN3VCxXQUFkLElBQTZCbFksQ0FBQyxDQUFDMEUsV0FBRixDQUFjMFQsUUFBaEQsRUFBMkQ7QUFFdkR6VSxNQUFBQSxTQUFTLEdBQUczRCxDQUFDLENBQUNxWCxjQUFGLEVBQVo7O0FBRUEsY0FBUzFULFNBQVQ7QUFFSSxhQUFLLE1BQUw7QUFDQSxhQUFLLE1BQUw7QUFFSU8sVUFBQUEsVUFBVSxHQUNObEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUYsWUFBVixHQUNJMUMsQ0FBQyxDQUFDeU4sY0FBRixDQUFrQnpOLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUNzUSxhQUFGLEVBQW5DLENBREosR0FFSXRRLENBQUMsQ0FBQzBELFlBQUYsR0FBaUIxRCxDQUFDLENBQUNzUSxhQUFGLEVBSHpCO0FBS0F0USxVQUFBQSxDQUFDLENBQUN3RCxnQkFBRixHQUFxQixDQUFyQjtBQUVBOztBQUVKLGFBQUssT0FBTDtBQUNBLGFBQUssSUFBTDtBQUVJVSxVQUFBQSxVQUFVLEdBQ05sRSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRixZQUFWLEdBQ0kxQyxDQUFDLENBQUN5TixjQUFGLENBQWtCek4sQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3NRLGFBQUYsRUFBbkMsQ0FESixHQUVJdFEsQ0FBQyxDQUFDMEQsWUFBRixHQUFpQjFELENBQUMsQ0FBQ3NRLGFBQUYsRUFIekI7QUFLQXRRLFVBQUFBLENBQUMsQ0FBQ3dELGdCQUFGLEdBQXFCLENBQXJCO0FBRUE7O0FBRUo7QUExQko7O0FBK0JBLFVBQUlHLFNBQVMsSUFBSSxVQUFqQixFQUE4QjtBQUUxQjNELFFBQUFBLENBQUMsQ0FBQ2lLLFlBQUYsQ0FBZ0IvRixVQUFoQjs7QUFDQWxFLFFBQUFBLENBQUMsQ0FBQzBFLFdBQUYsR0FBZ0IsRUFBaEI7O0FBQ0ExRSxRQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLE9BQWxCLEVBQTJCLENBQUMvTSxDQUFELEVBQUkyRCxTQUFKLENBQTNCO0FBRUg7QUFFSixLQTNDRCxNQTJDTztBQUVILFVBQUszRCxDQUFDLENBQUMwRSxXQUFGLENBQWNnVCxNQUFkLEtBQXlCMVgsQ0FBQyxDQUFDMEUsV0FBRixDQUFjaVQsSUFBNUMsRUFBbUQ7QUFFL0MzWCxRQUFBQSxDQUFDLENBQUNpSyxZQUFGLENBQWdCakssQ0FBQyxDQUFDMEQsWUFBbEI7O0FBQ0ExRCxRQUFBQSxDQUFDLENBQUMwRSxXQUFGLEdBQWdCLEVBQWhCO0FBRUg7QUFFSjtBQUVKLEdBeEVEOztBQTBFQTlFLEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JOLFlBQWhCLEdBQStCLFVBQVNrRyxLQUFULEVBQWdCO0FBRTNDLFFBQUloTixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFLQSxDQUFDLENBQUN2QyxPQUFGLENBQVVnRixLQUFWLEtBQW9CLEtBQXJCLElBQWdDLGdCQUFnQjBELFFBQWhCLElBQTRCbkcsQ0FBQyxDQUFDdkMsT0FBRixDQUFVZ0YsS0FBVixLQUFvQixLQUFwRixFQUE0RjtBQUN4RjtBQUNILEtBRkQsTUFFTyxJQUFJekMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVNEQsU0FBVixLQUF3QixLQUF4QixJQUFpQzJMLEtBQUssQ0FBQzhHLElBQU4sQ0FBV3VFLE9BQVgsQ0FBbUIsT0FBbkIsTUFBZ0MsQ0FBQyxDQUF0RSxFQUF5RTtBQUM1RTtBQUNIOztBQUVEclksSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjNFQsV0FBZCxHQUE0QnRMLEtBQUssQ0FBQ3VMLGFBQU4sSUFBdUJ2TCxLQUFLLENBQUN1TCxhQUFOLENBQW9CQyxPQUFwQixLQUFnQzNDLFNBQXZELEdBQ3hCN0ksS0FBSyxDQUFDdUwsYUFBTixDQUFvQkMsT0FBcEIsQ0FBNEIxUSxNQURKLEdBQ2EsQ0FEekM7QUFHQTlILElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBYzBULFFBQWQsR0FBeUJwWSxDQUFDLENBQUM2RCxTQUFGLEdBQWM3RCxDQUFDLENBQUN2QyxPQUFGLENBQ2xDbUYsY0FETDs7QUFHQSxRQUFJNUMsQ0FBQyxDQUFDdkMsT0FBRixDQUFVd0YsZUFBVixLQUE4QixJQUFsQyxFQUF3QztBQUNwQ2pELE1BQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBYzBULFFBQWQsR0FBeUJwWSxDQUFDLENBQUM4RCxVQUFGLEdBQWU5RCxDQUFDLENBQUN2QyxPQUFGLENBQ25DbUYsY0FETDtBQUVIOztBQUVELFlBQVFvSyxLQUFLLENBQUMvRyxJQUFOLENBQVd5TCxNQUFuQjtBQUVJLFdBQUssT0FBTDtBQUNJMVIsUUFBQUEsQ0FBQyxDQUFDeVksVUFBRixDQUFhekwsS0FBYjs7QUFDQTs7QUFFSixXQUFLLE1BQUw7QUFDSWhOLFFBQUFBLENBQUMsQ0FBQzBZLFNBQUYsQ0FBWTFMLEtBQVo7O0FBQ0E7O0FBRUosV0FBSyxLQUFMO0FBQ0loTixRQUFBQSxDQUFDLENBQUNpWSxRQUFGLENBQVdqTCxLQUFYOztBQUNBO0FBWlI7QUFnQkgsR0FyQ0Q7O0FBdUNBcE4sRUFBQUEsS0FBSyxDQUFDd0gsU0FBTixDQUFnQnNSLFNBQWhCLEdBQTRCLFVBQVMxTCxLQUFULEVBQWdCO0FBRXhDLFFBQUloTixDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0kyWSxVQUFVLEdBQUcsS0FEakI7QUFBQSxRQUVJQyxPQUZKO0FBQUEsUUFFYXZCLGNBRmI7QUFBQSxRQUU2QmEsV0FGN0I7QUFBQSxRQUUwQ1csY0FGMUM7QUFBQSxRQUUwREwsT0FGMUQ7O0FBSUFBLElBQUFBLE9BQU8sR0FBR3hMLEtBQUssQ0FBQ3VMLGFBQU4sS0FBd0IxQyxTQUF4QixHQUFvQzdJLEtBQUssQ0FBQ3VMLGFBQU4sQ0FBb0JDLE9BQXhELEdBQWtFLElBQTVFOztBQUVBLFFBQUksQ0FBQ3hZLENBQUMsQ0FBQ3NELFFBQUgsSUFBZWtWLE9BQU8sSUFBSUEsT0FBTyxDQUFDMVEsTUFBUixLQUFtQixDQUFqRCxFQUFvRDtBQUNoRCxhQUFPLEtBQVA7QUFDSDs7QUFFRDhRLElBQUFBLE9BQU8sR0FBRzVZLENBQUMsQ0FBQ3dQLE9BQUYsQ0FBVXhQLENBQUMsQ0FBQzBELFlBQVosQ0FBVjtBQUVBMUQsSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjaVQsSUFBZCxHQUFxQmEsT0FBTyxLQUFLM0MsU0FBWixHQUF3QjJDLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV00sS0FBbkMsR0FBMkM5TCxLQUFLLENBQUMrTCxPQUF0RTtBQUNBL1ksSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjbVQsSUFBZCxHQUFxQlcsT0FBTyxLQUFLM0MsU0FBWixHQUF3QjJDLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV1EsS0FBbkMsR0FBMkNoTSxLQUFLLENBQUNpTSxPQUF0RTtBQUVBalosSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjd1QsV0FBZCxHQUE0QjVPLElBQUksQ0FBQ3lPLEtBQUwsQ0FBV3pPLElBQUksQ0FBQzRQLElBQUwsQ0FDbkM1UCxJQUFJLENBQUM2UCxHQUFMLENBQVNuWixDQUFDLENBQUMwRSxXQUFGLENBQWNpVCxJQUFkLEdBQXFCM1gsQ0FBQyxDQUFDMEUsV0FBRixDQUFjZ1QsTUFBNUMsRUFBb0QsQ0FBcEQsQ0FEbUMsQ0FBWCxDQUE1Qjs7QUFHQSxRQUFJMVgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVd0YsZUFBVixLQUE4QixJQUFsQyxFQUF3QztBQUNwQ2pELE1BQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3dULFdBQWQsR0FBNEI1TyxJQUFJLENBQUN5TyxLQUFMLENBQVd6TyxJQUFJLENBQUM0UCxJQUFMLENBQ25DNVAsSUFBSSxDQUFDNlAsR0FBTCxDQUFTblosQ0FBQyxDQUFDMEUsV0FBRixDQUFjbVQsSUFBZCxHQUFxQjdYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2tULE1BQTVDLEVBQW9ELENBQXBELENBRG1DLENBQVgsQ0FBNUI7QUFFSDs7QUFFRFAsSUFBQUEsY0FBYyxHQUFHclgsQ0FBQyxDQUFDcVgsY0FBRixFQUFqQjs7QUFFQSxRQUFJQSxjQUFjLEtBQUssVUFBdkIsRUFBbUM7QUFDL0I7QUFDSDs7QUFFRCxRQUFJckssS0FBSyxDQUFDdUwsYUFBTixLQUF3QjFDLFNBQXhCLElBQXFDN1YsQ0FBQyxDQUFDMEUsV0FBRixDQUFjd1QsV0FBZCxHQUE0QixDQUFyRSxFQUF3RTtBQUNwRWxMLE1BQUFBLEtBQUssQ0FBQ08sY0FBTjtBQUNIOztBQUVEc0wsSUFBQUEsY0FBYyxHQUFHLENBQUM3WSxDQUFDLENBQUN2QyxPQUFGLENBQVUyRSxHQUFWLEtBQWtCLEtBQWxCLEdBQTBCLENBQTFCLEdBQThCLENBQUMsQ0FBaEMsS0FBc0NwQyxDQUFDLENBQUMwRSxXQUFGLENBQWNpVCxJQUFkLEdBQXFCM1gsQ0FBQyxDQUFDMEUsV0FBRixDQUFjZ1QsTUFBbkMsR0FBNEMsQ0FBNUMsR0FBZ0QsQ0FBQyxDQUF2RixDQUFqQjs7QUFDQSxRQUFJMVgsQ0FBQyxDQUFDdkMsT0FBRixDQUFVd0YsZUFBVixLQUE4QixJQUFsQyxFQUF3QztBQUNwQzRWLE1BQUFBLGNBQWMsR0FBRzdZLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY21ULElBQWQsR0FBcUI3WCxDQUFDLENBQUMwRSxXQUFGLENBQWNrVCxNQUFuQyxHQUE0QyxDQUE1QyxHQUFnRCxDQUFDLENBQWxFO0FBQ0g7O0FBR0RNLElBQUFBLFdBQVcsR0FBR2xZLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3dULFdBQTVCO0FBRUFsWSxJQUFBQSxDQUFDLENBQUMwRSxXQUFGLENBQWN5VCxPQUFkLEdBQXdCLEtBQXhCOztBQUVBLFFBQUluWSxDQUFDLENBQUN2QyxPQUFGLENBQVVpRSxRQUFWLEtBQXVCLEtBQTNCLEVBQWtDO0FBQzlCLFVBQUsxQixDQUFDLENBQUMwRCxZQUFGLEtBQW1CLENBQW5CLElBQXdCMlQsY0FBYyxLQUFLLE9BQTVDLElBQXlEclgsQ0FBQyxDQUFDMEQsWUFBRixJQUFrQjFELENBQUMsQ0FBQzZLLFdBQUYsRUFBbEIsSUFBcUN3TSxjQUFjLEtBQUssTUFBckgsRUFBOEg7QUFDMUhhLFFBQUFBLFdBQVcsR0FBR2xZLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY3dULFdBQWQsR0FBNEJsWSxDQUFDLENBQUN2QyxPQUFGLENBQVU4RCxZQUFwRDtBQUNBdkIsUUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjeVQsT0FBZCxHQUF3QixJQUF4QjtBQUNIO0FBQ0o7O0FBRUQsUUFBSW5ZLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVXVGLFFBQVYsS0FBdUIsS0FBM0IsRUFBa0M7QUFDOUJoRCxNQUFBQSxDQUFDLENBQUN3RSxTQUFGLEdBQWNvVSxPQUFPLEdBQUdWLFdBQVcsR0FBR1csY0FBdEM7QUFDSCxLQUZELE1BRU87QUFDSDdZLE1BQUFBLENBQUMsQ0FBQ3dFLFNBQUYsR0FBY29VLE9BQU8sR0FBSVYsV0FBVyxJQUFJbFksQ0FBQyxDQUFDeUUsS0FBRixDQUFRMUcsTUFBUixLQUFtQmlDLENBQUMsQ0FBQzZELFNBQXpCLENBQVosR0FBbURnVixjQUEzRTtBQUNIOztBQUNELFFBQUk3WSxDQUFDLENBQUN2QyxPQUFGLENBQVV3RixlQUFWLEtBQThCLElBQWxDLEVBQXdDO0FBQ3BDakQsTUFBQUEsQ0FBQyxDQUFDd0UsU0FBRixHQUFjb1UsT0FBTyxHQUFHVixXQUFXLEdBQUdXLGNBQXRDO0FBQ0g7O0FBRUQsUUFBSTdZLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVStELElBQVYsS0FBbUIsSUFBbkIsSUFBMkJ4QixDQUFDLENBQUN2QyxPQUFGLENBQVVrRixTQUFWLEtBQXdCLEtBQXZELEVBQThEO0FBQzFELGFBQU8sS0FBUDtBQUNIOztBQUVELFFBQUkzQyxDQUFDLENBQUNxRCxTQUFGLEtBQWdCLElBQXBCLEVBQTBCO0FBQ3RCckQsTUFBQUEsQ0FBQyxDQUFDd0UsU0FBRixHQUFjLElBQWQ7QUFDQSxhQUFPLEtBQVA7QUFDSDs7QUFFRHhFLElBQUFBLENBQUMsQ0FBQ3VVLE1BQUYsQ0FBU3ZVLENBQUMsQ0FBQ3dFLFNBQVg7QUFFSCxHQXhFRDs7QUEwRUE1RSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCcVIsVUFBaEIsR0FBNkIsVUFBU3pMLEtBQVQsRUFBZ0I7QUFFekMsUUFBSWhOLENBQUMsR0FBRyxJQUFSO0FBQUEsUUFDSXdZLE9BREo7O0FBR0F4WSxJQUFBQSxDQUFDLENBQUNvRixXQUFGLEdBQWdCLElBQWhCOztBQUVBLFFBQUlwRixDQUFDLENBQUMwRSxXQUFGLENBQWM0VCxXQUFkLEtBQThCLENBQTlCLElBQW1DdFksQ0FBQyxDQUFDa0UsVUFBRixJQUFnQmxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBQWpFLEVBQStFO0FBQzNFdkMsTUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixHQUFnQixFQUFoQjtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFFBQUlzSSxLQUFLLENBQUN1TCxhQUFOLEtBQXdCMUMsU0FBeEIsSUFBcUM3SSxLQUFLLENBQUN1TCxhQUFOLENBQW9CQyxPQUFwQixLQUFnQzNDLFNBQXpFLEVBQW9GO0FBQ2hGMkMsTUFBQUEsT0FBTyxHQUFHeEwsS0FBSyxDQUFDdUwsYUFBTixDQUFvQkMsT0FBcEIsQ0FBNEIsQ0FBNUIsQ0FBVjtBQUNIOztBQUVEeFksSUFBQUEsQ0FBQyxDQUFDMEUsV0FBRixDQUFjZ1QsTUFBZCxHQUF1QjFYLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2lULElBQWQsR0FBcUJhLE9BQU8sS0FBSzNDLFNBQVosR0FBd0IyQyxPQUFPLENBQUNNLEtBQWhDLEdBQXdDOUwsS0FBSyxDQUFDK0wsT0FBMUY7QUFDQS9ZLElBQUFBLENBQUMsQ0FBQzBFLFdBQUYsQ0FBY2tULE1BQWQsR0FBdUI1WCxDQUFDLENBQUMwRSxXQUFGLENBQWNtVCxJQUFkLEdBQXFCVyxPQUFPLEtBQUszQyxTQUFaLEdBQXdCMkMsT0FBTyxDQUFDUSxLQUFoQyxHQUF3Q2hNLEtBQUssQ0FBQ2lNLE9BQTFGO0FBRUFqWixJQUFBQSxDQUFDLENBQUNzRCxRQUFGLEdBQWEsSUFBYjtBQUVILEdBckJEOztBQXVCQTFELEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JnUyxjQUFoQixHQUFpQ3haLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0JpUyxhQUFoQixHQUFnQyxZQUFXO0FBRXhFLFFBQUlyWixDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUMyRixZQUFGLEtBQW1CLElBQXZCLEVBQTZCO0FBRXpCM0YsTUFBQUEsQ0FBQyxDQUFDNkgsTUFBRjs7QUFFQTdILE1BQUFBLENBQUMsQ0FBQ29FLFdBQUYsQ0FBY2dFLFFBQWQsQ0FBdUIsS0FBSzNLLE9BQUwsQ0FBYTRFLEtBQXBDLEVBQTJDZ0csTUFBM0M7O0FBRUFySSxNQUFBQSxDQUFDLENBQUMyRixZQUFGLENBQWVvQyxRQUFmLENBQXdCL0gsQ0FBQyxDQUFDb0UsV0FBMUI7O0FBRUFwRSxNQUFBQSxDQUFDLENBQUN1SSxNQUFGO0FBRUg7QUFFSixHQWhCRDs7QUFrQkEzSSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCUyxNQUFoQixHQUF5QixZQUFXO0FBRWhDLFFBQUk3SCxDQUFDLEdBQUcsSUFBUjs7QUFFQTFDLElBQUFBLENBQUMsQ0FBQyxlQUFELEVBQWtCMEMsQ0FBQyxDQUFDMEYsT0FBcEIsQ0FBRCxDQUE4QitJLE1BQTlCOztBQUVBLFFBQUl6TyxDQUFDLENBQUM0RCxLQUFOLEVBQWE7QUFDVDVELE1BQUFBLENBQUMsQ0FBQzRELEtBQUYsQ0FBUTZLLE1BQVI7QUFDSDs7QUFFRCxRQUFJek8sQ0FBQyxDQUFDaUUsVUFBRixJQUFnQmpFLENBQUMsQ0FBQ2lILFFBQUYsQ0FBV3dELElBQVgsQ0FBZ0J6SyxDQUFDLENBQUN2QyxPQUFGLENBQVUrQyxTQUExQixDQUFwQixFQUEwRDtBQUN0RFIsTUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFhd0ssTUFBYjtBQUNIOztBQUVELFFBQUl6TyxDQUFDLENBQUNnRSxVQUFGLElBQWdCaEUsQ0FBQyxDQUFDaUgsUUFBRixDQUFXd0QsSUFBWCxDQUFnQnpLLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWdELFNBQTFCLENBQXBCLEVBQTBEO0FBQ3REVCxNQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWF5SyxNQUFiO0FBQ0g7O0FBRUR6TyxJQUFBQSxDQUFDLENBQUNxRSxPQUFGLENBQ0trRyxXQURMLENBQ2lCLHNEQURqQixFQUVLaEQsSUFGTCxDQUVVLGFBRlYsRUFFeUIsTUFGekIsRUFHS3hJLEdBSEwsQ0FHUyxPQUhULEVBR2tCLEVBSGxCO0FBS0gsR0F2QkQ7O0FBeUJBYSxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCeUYsT0FBaEIsR0FBMEIsVUFBU3lNLGNBQVQsRUFBeUI7QUFFL0MsUUFBSXRaLENBQUMsR0FBRyxJQUFSOztBQUNBQSxJQUFBQSxDQUFDLENBQUMwRixPQUFGLENBQVVxSCxPQUFWLENBQWtCLFNBQWxCLEVBQTZCLENBQUMvTSxDQUFELEVBQUlzWixjQUFKLENBQTdCOztBQUNBdFosSUFBQUEsQ0FBQyxDQUFDd08sT0FBRjtBQUVILEdBTkQ7O0FBUUE1TyxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCZ0ssWUFBaEIsR0FBK0IsWUFBVztBQUV0QyxRQUFJcFIsQ0FBQyxHQUFHLElBQVI7QUFBQSxRQUNJeVEsWUFESjs7QUFHQUEsSUFBQUEsWUFBWSxHQUFHbkgsSUFBSSxDQUFDc0csS0FBTCxDQUFXNVAsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBVixHQUF5QixDQUFwQyxDQUFmOztBQUVBLFFBQUt2QyxDQUFDLENBQUN2QyxPQUFGLENBQVU2QyxNQUFWLEtBQXFCLElBQXJCLElBQ0ROLENBQUMsQ0FBQ2tFLFVBQUYsR0FBZWxFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVThFLFlBRHhCLElBRUQsQ0FBQ3ZDLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVWlFLFFBRmYsRUFFMEI7QUFFdEIxQixNQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWFzRyxXQUFiLENBQXlCLGdCQUF6QixFQUEyQ2hELElBQTNDLENBQWdELGVBQWhELEVBQWlFLE9BQWpFOztBQUNBdkgsTUFBQUEsQ0FBQyxDQUFDZ0UsVUFBRixDQUFhdUcsV0FBYixDQUF5QixnQkFBekIsRUFBMkNoRCxJQUEzQyxDQUFnRCxlQUFoRCxFQUFpRSxPQUFqRTs7QUFFQSxVQUFJdkgsQ0FBQyxDQUFDMEQsWUFBRixLQUFtQixDQUF2QixFQUEwQjtBQUV0QjFELFFBQUFBLENBQUMsQ0FBQ2lFLFVBQUYsQ0FBYXZGLFFBQWIsQ0FBc0IsZ0JBQXRCLEVBQXdDNkksSUFBeEMsQ0FBNkMsZUFBN0MsRUFBOEQsTUFBOUQ7O0FBQ0F2SCxRQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWF1RyxXQUFiLENBQXlCLGdCQUF6QixFQUEyQ2hELElBQTNDLENBQWdELGVBQWhELEVBQWlFLE9BQWpFO0FBRUgsT0FMRCxNQUtPLElBQUl2SCxDQUFDLENBQUMwRCxZQUFGLElBQWtCMUQsQ0FBQyxDQUFDa0UsVUFBRixHQUFlbEUsQ0FBQyxDQUFDdkMsT0FBRixDQUFVOEUsWUFBM0MsSUFBMkR2QyxDQUFDLENBQUN2QyxPQUFGLENBQVVtRCxVQUFWLEtBQXlCLEtBQXhGLEVBQStGO0FBRWxHWixRQUFBQSxDQUFDLENBQUNnRSxVQUFGLENBQWF0RixRQUFiLENBQXNCLGdCQUF0QixFQUF3QzZJLElBQXhDLENBQTZDLGVBQTdDLEVBQThELE1BQTlEOztBQUNBdkgsUUFBQUEsQ0FBQyxDQUFDaUUsVUFBRixDQUFhc0csV0FBYixDQUF5QixnQkFBekIsRUFBMkNoRCxJQUEzQyxDQUFnRCxlQUFoRCxFQUFpRSxPQUFqRTtBQUVILE9BTE0sTUFLQSxJQUFJdkgsQ0FBQyxDQUFDMEQsWUFBRixJQUFrQjFELENBQUMsQ0FBQ2tFLFVBQUYsR0FBZSxDQUFqQyxJQUFzQ2xFLENBQUMsQ0FBQ3ZDLE9BQUYsQ0FBVW1ELFVBQVYsS0FBeUIsSUFBbkUsRUFBeUU7QUFFNUVaLFFBQUFBLENBQUMsQ0FBQ2dFLFVBQUYsQ0FBYXRGLFFBQWIsQ0FBc0IsZ0JBQXRCLEVBQXdDNkksSUFBeEMsQ0FBNkMsZUFBN0MsRUFBOEQsTUFBOUQ7O0FBQ0F2SCxRQUFBQSxDQUFDLENBQUNpRSxVQUFGLENBQWFzRyxXQUFiLENBQXlCLGdCQUF6QixFQUEyQ2hELElBQTNDLENBQWdELGVBQWhELEVBQWlFLE9BQWpFO0FBRUg7QUFFSjtBQUVKLEdBakNEOztBQW1DQTNILEVBQUFBLEtBQUssQ0FBQ3dILFNBQU4sQ0FBZ0I4RCxVQUFoQixHQUE2QixZQUFXO0FBRXBDLFFBQUlsTCxDQUFDLEdBQUcsSUFBUjs7QUFFQSxRQUFJQSxDQUFDLENBQUM0RCxLQUFGLEtBQVksSUFBaEIsRUFBc0I7QUFFbEI1RCxNQUFBQSxDQUFDLENBQUM0RCxLQUFGLENBQ0swRCxJQURMLENBQ1UsSUFEVixFQUVLaUQsV0FGTCxDQUVpQixjQUZqQixFQUdLaEQsSUFITCxDQUdVLGFBSFYsRUFHeUIsTUFIekI7O0FBS0F2SCxNQUFBQSxDQUFDLENBQUM0RCxLQUFGLENBQ0swRCxJQURMLENBQ1UsSUFEVixFQUVLVyxFQUZMLENBRVFxQixJQUFJLENBQUNzRyxLQUFMLENBQVc1UCxDQUFDLENBQUMwRCxZQUFGLEdBQWlCMUQsQ0FBQyxDQUFDdkMsT0FBRixDQUFVK0UsY0FBdEMsQ0FGUixFQUdLOUQsUUFITCxDQUdjLGNBSGQsRUFJSzZJLElBSkwsQ0FJVSxhQUpWLEVBSXlCLE9BSnpCO0FBTUg7QUFFSixHQW5CRDs7QUFxQkEzSCxFQUFBQSxLQUFLLENBQUN3SCxTQUFOLENBQWdCNkcsVUFBaEIsR0FBNkIsWUFBVztBQUVwQyxRQUFJak8sQ0FBQyxHQUFHLElBQVI7O0FBRUEsUUFBS0EsQ0FBQyxDQUFDdkMsT0FBRixDQUFVaUQsUUFBZixFQUEwQjtBQUV0QixVQUFLeUYsUUFBUSxDQUFDbkcsQ0FBQyxDQUFDcUYsTUFBSCxDQUFiLEVBQTBCO0FBRXRCckYsUUFBQUEsQ0FBQyxDQUFDb0YsV0FBRixHQUFnQixJQUFoQjtBQUVILE9BSkQsTUFJTztBQUVIcEYsUUFBQUEsQ0FBQyxDQUFDb0YsV0FBRixHQUFnQixLQUFoQjtBQUVIO0FBRUo7QUFFSixHQWxCRDs7QUFvQkE5SCxFQUFBQSxDQUFDLENBQUNDLEVBQUYsQ0FBS3lNLEtBQUwsR0FBYSxZQUFXO0FBQ3BCLFFBQUloSyxDQUFDLEdBQUcsSUFBUjtBQUFBLFFBQ0l1VixHQUFHLEdBQUdELFNBQVMsQ0FBQyxDQUFELENBRG5CO0FBQUEsUUFFSWlFLElBQUksR0FBR0MsS0FBSyxDQUFDcFMsU0FBTixDQUFnQndMLEtBQWhCLENBQXNCbkosSUFBdEIsQ0FBMkI2TCxTQUEzQixFQUFzQyxDQUF0QyxDQUZYO0FBQUEsUUFHSTFCLENBQUMsR0FBRzVULENBQUMsQ0FBQzhILE1BSFY7QUFBQSxRQUlJN0csQ0FKSjtBQUFBLFFBS0l3WSxHQUxKOztBQU1BLFNBQUt4WSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUcyUyxDQUFoQixFQUFtQjNTLENBQUMsRUFBcEIsRUFBd0I7QUFDcEIsVUFBSSxPQUFPc1UsR0FBUCxJQUFjLFFBQWQsSUFBMEIsT0FBT0EsR0FBUCxJQUFjLFdBQTVDLEVBQ0l2VixDQUFDLENBQUNpQixDQUFELENBQUQsQ0FBSytJLEtBQUwsR0FBYSxJQUFJcEssS0FBSixDQUFVSSxDQUFDLENBQUNpQixDQUFELENBQVgsRUFBZ0JzVSxHQUFoQixDQUFiLENBREosS0FHSWtFLEdBQUcsR0FBR3paLENBQUMsQ0FBQ2lCLENBQUQsQ0FBRCxDQUFLK0ksS0FBTCxDQUFXdUwsR0FBWCxFQUFnQm1FLEtBQWhCLENBQXNCMVosQ0FBQyxDQUFDaUIsQ0FBRCxDQUFELENBQUsrSSxLQUEzQixFQUFrQ3VQLElBQWxDLENBQU47QUFDSixVQUFJLE9BQU9FLEdBQVAsSUFBYyxXQUFsQixFQUErQixPQUFPQSxHQUFQO0FBQ2xDOztBQUNELFdBQU96WixDQUFQO0FBQ0gsR0FmRDtBQWlCSCxDQTF6RkEsQ0FBRDtDQ2pCQTtFQ0FBO0dDQUE7SUNBQTtLSkFBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigkKXtcblxuLypcbiAgT3B0aW9uczpcblxuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcbiAgUmVxdWlyZWRcbiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cbiAgLS0gU2xpZGUgaW4gU2lkZSA9IGRlZmF1bHQgbGVmdFxuICAgIC0tIFBvc3NpYmxlIG9wdGlvbnMgXCJ0b3BcIiwgXCJsZWZ0XCIsIFwicmlnaHRcIiwgXCJib3R0b21cIlxuXG4gIC0tIFNsaWRlIGluIFNwZWVkID0gZGVmYXVsdCA0MDBcbiAgICAtLSBPcHRpb25zIGFueSBtaWxpc2Vjb25kc1xuXG4gIC0tIE9wdGlvbmFsIE9wZW4vQ2xvc2UgQnV0dG9uID0gZGVmYXVsdCBDcmVhdGUgb24gZm9yIHRoZW1cbiAgICAtLSBBbnkgb2JqZWN0IHdpdGggYSBkaXYgb3IgY2xhc3NcblxuICAtLSBXaWR0aCBvZiBNZW51ID0gZGVmYXVsdCAxMDAlXG4gICAgLS0gQW55IG51bWJlclxuXG4jIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuICBOb24gRXNzZW50YWlsc1xuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblxuICAtLSBGaXhlZCBvciBTdGF0aWMgPSBEZWZhdWx0IEZpeGVkXG4gICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG5cbiAgLS0gQW5pbWF0aW9uXG4gICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG5cbiAgLS0gT3B0aW9uYWwgQnV0dG9uIFN0eWxpbmdcbiAgICAtLSBOb24gZXNzZW50aWFsIGZvciBub3dcblxuXG5cbiovXG5cbiAgJC5mbi5tb2JpbGVfbWVudSA9IGZ1bmN0aW9uICggb3B0aW9ucyApIHtcblxuICAgIC8vIERlZmluZSBEZWZhdWx0IHNldHRpbmdzOlxuICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgIHNpZGU6ICdsZWZ0JyxcbiAgICAgIHNwZWVkOiAnMC41cycsXG4gICAgICB0b2dnbGVyOiAnLm1lbnUtdG9nZ2xlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIGJ1dHRvblN0eWxlczogdHJ1ZSxcbiAgICB9O1xuXG4gICAgdmFyIGluaXRhbHMgPSB7XG4gICAgICB0b2dnbGVCdXR0b246ICc8ZGl2IGNsYXNzPVwibWVudS10b2dnbGVcIj48L2Rpdj4nLFxuICAgICAgbWVudVdyYXA6ICc8ZGl2IGNsYXNzPVwib2ZmY2FudmFzLW1lbnUtd3JhcFwiPjwvZGl2PicsXG4gICAgfTtcblxuICAgIHZhciAkdG9nZ2xlcjtcbiAgICB2YXIgb2ZmY2FudmFzO1xuXG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICAgIHZhciBzZXR0aW5ncyA9ICQuZXh0ZW5kKCB7fSwgaW5pdGFscywgZGVmYXVsdHMsIG9wdGlvbnMgKTtcblxuICAgIGlmKHNldHRpbmdzLnRvZ2dsZXIgPT0gZGVmYXVsdHMudG9nZ2xlcil7XG4gICAgICAkdGhpcy5iZWZvcmUoc2V0dGluZ3MudG9nZ2xlQnV0dG9uKTtcbiAgICB9XG5cbiAgICAvKiBEZXRlcm1pbmUgaWYgeW91IHdhbnQgdG9nZ2xlciBTdHlsZXMgKi9cbiAgICAkdG9nZ2xlciA9ICQoc2V0dGluZ3MudG9nZ2xlcik7XG4gICAgaWYoc2V0dGluZ3MuYnV0dG9uU3R5bGVzKXtcbiAgICAgICR0b2dnbGVyLmFkZENsYXNzKCdvZmZjYW52YXMtdG9nZ2xlJyk7XG4gICAgfVxuXG4gICAgc3dpdGNoICggc2V0dGluZ3Muc2lkZSApIHtcbiAgICAgIGNhc2UgXCJsZWZ0XCI6XG4gICAgICAgIG9mZmNhbnZhcyA9ICd0cmFuc2xhdGVYKC0xMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcInRvcFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWSgtMTAwJSknO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJyaWdodFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWCgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcImJvdHRvbVwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWSgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIHZhciBzdHlsZXMgPSB7XG4gICAgICAndHJhbnNmb3JtJyA6IG9mZmNhbnZhcyxcbiAgICAgICd0cmFuc2l0aW9uJyA6IHNldHRpbmdzLnNwZWVkLFxuICAgICAgaGVpZ2h0IDogc2V0dGluZ3MuaGVpZ2h0LFxuICAgICAgd2lkdGggOiBzZXR0aW5ncy53aWR0aCxcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpIHtcblxuICAgICAgICAkdGhpcy5hZGRDbGFzcygnb2ZmY2FudmFzLW1lbnUnKS53cmFwKHNldHRpbmdzLm1lbnVXcmFwKTtcbiAgICAgICAgJHRoaXMucGFyZW50KCkuY3NzKCBzdHlsZXMgKTtcbiAgICAgICAgY29uc29sZS5sb2coJHRvZ2dsZXIpO1xuICAgICAgICAkdG9nZ2xlci5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdvcGVuL2Nsb3NlJyk7XG5cbiAgICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdvZmZjYW52YXMtYWN0aXZlJyk7XG4gICAgICAgICAgY29uc29sZS5sb2coJHRoaXMpO1xuICAgICAgICAgICR0aGlzLmNsb3Nlc3QoJy5vZmZjYW52YXMtbWVudS13cmFwJykudG9nZ2xlQ2xhc3MoJ29mZmNhbnZhcy1vcGVuJyk7XG5cbiAgICAgICAgfSk7XG5cblxuICAgIH0pO1xuICB9XG5cblxuICAkKCcjbW9iaWxlX21lbnUnKS5tb2JpbGVfbWVudSh7XG4gICAgc2lkZTogJ2JvdHRvbScsXG4gIH0pO1xuXG5cbn0pKGpRdWVyeSk7XG4iLCIiLCIiLCIiLCIiLCIiXX0=
