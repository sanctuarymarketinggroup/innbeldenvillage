<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

$class = 'default';


get_header(); 
//TO DO add in stuff from Theme options
$settings = get_setting( 'schema-logo' );
$array = array();
$categories = get_the_category(get_the_id());
foreach ($categories as $cat ){
	$array[] = $cat->name;
}
?>
<script type="application/ld+json">
{ 
	"@context": "http://schema.org", 
	"@type": "BlogPosting",
	"headline": "<?php echo get_the_title(); ?>",
	"name": "<?php echo get_the_title(); ?>",
	<?php if( has_post_thumbnail() ) { ?>
	"image": "<?php echo get_the_post_thumbnail_url(); ?>",
	<?php } ?>
	"keywords": "<?php echo implode(', ', $array); ?>", 
	"articleSection": "<?php echo implode(', ', $array); ?>", 
	"wordcount": "<?php echo word_count();?>",
	"url": "<?php echo get_permalink(); ?>",
	"mainEntityOfPage": "<?php echo get_permalink(); ?>",
	"datePublished": "<?php echo get_the_date(); ?>",
	"dateCreated": "<?php echo get_the_date(); ?>",
	"isFamilyFriendly": "Yes",
	"dateModified": "<?php echo get_the_modified_date('F d, Y');?>",
	"description": <?php echo json_encode(get_the_excerpt());?>,
	"articleBody": <?php echo json_encode(get_the_content()); ?>,
  "author": {
    "@type": "Person",
    "name": "<?php echo get_the_author(); ?>"
	},
	"publisher": {
		"@type": "Organization",
		"name": "<?php echo get_bloginfo('sitename'); ?>",
		"logo": {
			"@type": "ImageObject",
			"url": "<?php echo $settings['url']; ?>"
		}
	}
 }
</script>

<div id="post" class="template <?php echo $class . '-template'; ?>" role="main">
	<?php while ( have_posts() ) { the_post(); ?>
		<article class="entry" id="post-<?php the_ID(); ?>">
			<header class="page-header entry-header">
				<?php $featured = get_featured_image(); ?>
				<?php if( $featured !== null ){ ?> 
					<img class="entry-image lazyload" data-src="<?php echo $featured; ?>" />
				<?php } ?>
				<?php echo show_template('components/page-title', array('class' => 'entry-title')); ?>
				<?php //smg_entry_meta(); ?>
			</header>
			<main class="page-body entry-body">
				<?php do_action('before_entry_body'); ?>
				<?php echo show_template('components/page-content', array('class' => 'entry-content')); ?>
				<?php do_action('after_entry_body'); ?>
			</main>
			<footer class="page-footer entry-footer">
				<?php // if(is_single()) comments_template(); ?>
			</footer>
		</article>
		<aside class="sidebar">
                			<?php get_sidebar(); ?>
                		</aside>
	<?php } ?>
</div>

<?php get_footer();