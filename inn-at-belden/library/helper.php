<?php
/**
 * Website Specific Functions
 * 
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

function smg_get_image( $id, $size = 'full', $icon = false, $args = '', $return = 'full' ) {
  $image = wp_get_attachment_image_src( $id, $size, $icon );
  if( strpos( $image[0], '.svg' ) ) {
    return get_svg( $image[0] );
  }
  if( 'src' === $return ) {
    return $image[0];
  }
  return wp_get_attachment_image( $id, $size, $icon, $args );
}

function get_the_post_logo( $post_id, $size, $icon = false, $args = '' ) {
  $id = get_post_meta( $post_id, '_featured_logo', true );
  return smg_get_image( $id, $size, $icon, $args );
}

function fl_dependency_field( $dependency, $array, $field, $value ) {
  if( 'true' == $dependency ) {
    $array[$field] = $value;
  }
  return $array;
}

function smg_get_style_field( $value, $null, $field, $extra = '' ){
  return ($null !== $value) ? $field . ':' . $value . $extra . ';' : '';
}


/**
 * incvar
 * 
 * @since 1.0.0
 */
if (!function_exists('incvar')) {
  function incvar($var) {
    if (isset($var) && !empty($var)) {
      return $var;
    }
    return null;
  }
}


/**
 * get_incclass
 * 
 * @since 1.0.0
 */
if (!function_exists('get_incclass')) {
  function get_incclass($class, $default) {
    if (null == incvar($class)) {
      return $default;
    }
    return "$default $class";
  }
}


/**
 * smg_social_bar
 * 
 * @since 1.0.0
 */
if (!function_exists('smg_social_bar')) {
  function smg_social_bar() {
    echo show_template('cp-social-bar');
  }
}


/**
 * Most Raw form of the Functions
 */
function get_img_lazy( $src, $args = array(
  'height' => '',
  'width' => '',
  'title' => '',
  'alt' => ''
) ) {
  return '<img class="lazyload" src="' . get_image_placeholder_url() . '" data-src="' . $src . '" height="' . $args['height'] . '" width="' . $args['width'] . '" alt="' . $args['alt'] . '" title="' . $args['title'] . '" />';
}

function get_img( $src, $args = array(
  'height' => '',
  'width' => '',
  'title' => '',
  'alt' => ''
) ) {
  return '<img src="' . $src . '" height="' . $args['height'] . '" width="' . $args['width'] . '" alt="' . $args['alt'] . '" title="' . $args['title'] . '" />';
}
function get_svg_lazy( $src ) {
  return '<svg class="lazyload" data-url="' . $src . '"></svg>';
}
function get_featured_image($size = 'full'){
  return get_the_post_thumbnail_url(get_the_ID(), $size) ? get_the_post_thumbnail_url(get_the_ID(), $size) : null;
}


/**
 * Get Theme Images/Svgs
 */
function get_theme_img( $src, $args = array() ) {
  return get_img_lazy( get_theme_img_url($src), $args );
}
function get_theme_svg( $src ) {
  return get_svg_lazy( get_theme_svg_url($src) );
}


/**
 * Get Theme Src
 */
function get_theme_img_url( $src ) {
  return THEME_ROOT . '/assets/images/' . $src;
}
function get_image_placeholder_url() {
  return THEME_ROOT . '/assets/images/placeholder.gif';
}
function get_theme_svg_url( $src ) {
  return THEME_ROOT . '/assets/images/svg/' . $src . '.svg';
}


/**
 * 
 */
function sample_image() {
  ?><img src="<?php echo THEME_ROOT; ?>/assets/images/sample-image.png" alt="Sample Placeholder"><?php
}
