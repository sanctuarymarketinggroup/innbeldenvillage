<?php

/**
 * Get Main Settings of the Site
 */
function get_setting( $pri, $sec = null, $tir = null ) {
    $vars = array(
        'sitename'     => get_bloginfo( 'name' ),
        'header-logo'  => array(
            'url'    => THEME_ROOT . '/assets/images/svg/logo.svg',
            'width'  => '',
            'height' => '',
            'alt'    => get_bloginfo( 'name' ),
            'title'  => get_bloginfo( 'name' ) . ' Logo',
        ),
        'schema-logo'  => array(
            'url'    => THEME_ROOT . '/assets/images/logo-iabv.png',
            'width'  => '',
            'height' => '',
            'alt'    => get_bloginfo( 'name' ),
            'title'  => get_bloginfo( 'name' ) . ' Logo',
        ),
        'address'      => array(
            'street'  => '3927 38th St NW,',
            'city'    => 'Canton',
            'state'   => 'OH',
            'zipcode' => '44718',
            'full'    => '3927 38th St NW, Canton, OH 44718'
        ),
        'phone'        => array(
            'primary'   => array(
                'text' => '(330) 493-0096',
                'link' => 'tel:+1-330-493-0096'
            ),
        ),

        'footer-logos' => array(
            "logo1" => array(
                'url' => 'footer-logo1.jpg',
                'link' => 'http://www.ohioassistedliving.org/'
            ),
            "logo2" => array(
                'url' => 'footer-logo2.jpg',

                'link' => 'https://www.cantonchamber.org/'
            ),
            "logo3" => array(
                'url' => 'footer-logo3.jpg',
                'link' => 'https://www.odh.ohio.gov/'
            ),
            "logo4" => array(
                'url' => 'footer-logo4.jpg',
                'link' => 'http://www.services4aging.org/'
            ),
            "logo5" => array(
                'url' => 'footer-logo5.jpg', 
                'link' => 'https://www.alz.org/'
            ),
            "logo6" => array(
                'url' => 'footer-logo6.jpg',
                'link' => 'http://portal.hud.gov/hudportal/HUD?src=/program_offices/fair_housing_equal_opp'
            ),
            "logo7" => array(
                'url' => 'footer-logo7.jpg',
                'link' => 'http://cathedraloflife.org/'
            ),
        ),
        'facebook' => array(
            'url' => 'https://www.facebook.com/pages/The-Inn-at-Belden-Village/662715313783570'
        ),
        'google_map' => 'https://www.google.com/maps/place/The+Inn+At+Belden+Village/@40.8427311,-81.4210644,846m/data=!3m2!1e3!4b1!4m5!3m4!1s0x8836d12844efb8ff:0x1efb37f0059bc6ef!8m2!3d40.8427311!4d-81.4188757?hl=en'
    );
    if ( $tir ) {
        return $vars[ $pri ][ $sec ][ $tir ];
    }
    if ( $sec ) {
        return $vars[ $pri ][ $sec ];
    }
    if ( isset( $vars[ $pri ] ) && $pri ) {
        return $vars[ $pri ];
    }

    return;
}

/**
 * Get Full Address
 */
function get_full_address() {
    $address = get_setting( 'address' );
    $street  = $address['street'];
    $city    = $address['city'];
    $state   = $address['state'];
    $zip     = $address['zip'];

    return "$street, $city, $state $zip";
}

/**
 * Enable Gravity Forms Hidden Titles
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


