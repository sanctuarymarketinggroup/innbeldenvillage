<?php
/**
 * Author: SMG Dev Team
 * URL: https://sanctuarymg.com/
 *
 * SanctuaryMG functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */


/** Inital Functions */
require_once( 'library/init.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Add functions to be used throughout theme */
require_once( 'library/helper.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Register all navigation menus */
require_once( 'library/post-types-taxonomies.php' );

/** Theme Specific Functions */
require_once( 'library/shortcodes.php' );

/** Theme Options Functions */
require_once( 'library/theme-options.php' );

/** Theme Support Functions */
require_once( 'library/theme-support.php' );

/** Beaver Builder Modules Functions */
//require_once( 'library/beaver-builder/init.php' );

/** Modules Functions */
require_once( 'modules/init.php' );

/** Defines */
define('SMG_THEME', ABSPATH . '/wp-content/themes/inn-at-belden');
define('HOME_URL', home_url());
define('THEME_ROOT', get_stylesheet_directory_uri());

/* ------------------------------------
   Block: disable Gutenberg
------------------------------------- */

add_filter('use_block_editor_for_post_type', '__return_false');

/* ------------------------------------
   Block: Add "Read More"
------------------------------------- */

function add_read_more($text = '', $link = '#', $link_text = 'Learn More') {
    return $text . " <a target='_blank' href=". $link ." >$link_text</a>";
}

/* ------------------------------------
   Block: Check the title size
------------------------------------- */

function checkTitleSize($title) {
    if(strlen($title) > 57) {
        return substr($title, 0, 49) . '...';
    }

    return $title;
}

