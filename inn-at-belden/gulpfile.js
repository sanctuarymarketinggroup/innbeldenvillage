/*jslint node: true */
"use strict";

// Variables
const gulp = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const argv = require("yargs").argv;
const exec = require("child_process").exec;
const browserSync = require("browser-sync").create();
const $ = {
  postcss: require("gulp-postcss"),
  autoprefixer: require("autoprefixer"),
  cssnano: require("cssnano"),
  babel: require("gulp-babel"),
  if: require("gulp-if"),
  notify: require("gulp-notify"),
  jshint: require("gulp-jshint"),
  sourcemaps: require("gulp-sourcemaps"),
  uglify: require("gulp-uglify")
};

const isProduction = argv.production; // -- production flag
const isStaging = argv.staging; // -- staging flag
const isDevelopment = argv.development; // -- development flag

const env = getEnv();

let remote = {};

if (isDevelopment) {
  remote = {
    username: "theinnatbeldenvillage",
    ip: "35.225.33.64",
    port: 20179,
    theme: "inn-at-belden",
    method: "ssh"
  };
}
if (isProduction) {
  remote = {
    username: "",
    ip: "",
    port: "",
    theme: "",
    method: ""
  };
}
if (isStaging) {
  remote = {
    username: "",
    ip: "",
    port: "",
    theme: "",
    method: ""
  };
}

let reload = false;
// let file = "";
// let file2 = "";
let file = {
  relative: "",
  absolute: "",
  type: ""
};

// Functions
function getEnv() {
  if (isProduction) {
    return "production";
  }
  if (isStaging) {
    return "staging";
  }
  if (isDevelopment) {
    return "development";
  }
  return null;
}

function css() {
  reload = false;
  // file = "assets/dist/app.css";
  var plugins = [
    $.autoprefixer({
      browsers: ["last 2 versions"]
    })
  ];
  if (isProduction) {
    plugins.push($.cssnano());
  }
  return gulp
    .src("assets/scss/app.scss")
    .pipe($.sourcemaps.init())
    .pipe(sass())
    .on(
      "error",
      $.notify.onError({
        message: "<%= error.message %>",
        title: "Sass Error"
      })
    )
    .pipe($.postcss(plugins))
    .pipe($.if(!isProduction, $.sourcemaps.write(".")))
    .pipe(gulp.dest("./assets/dist"));
  // .pipe(browserSync.stream());
}

function js() {
  reload = true;
  // file = "assets/dist/app.js";
  return gulp
    .src("assets/javascript/**/*.js", { sourcemaps: true }) // ["assets/javascript/plugins/*.js", "assets/javascript/scripts/*.js"]
    .pipe(
      $.babel({
        presets: ["@babel/env"]
      })
    )
    .pipe(concat("app.js"))
    .pipe($.if(isProduction, $.uglify()))
    .pipe(gulp.dest("./assets/dist", { sourcemaps: true }));
}

function lint() {
  return gulp
    .src("assets/javascript/**/*.js")
    .pipe($.if(isProduction, $.jshint()))
    .pipe(
      $.if(
        isProduction,
        $.notify(function(file) {
          if (file.jshint === undefined) {
            return false;
          }
          if (file.jshint.success) {
            return false;
          }

          var errors = file.jshint.results
            .map(function(data) {
              if (data.error) {
                return (
                  "(" +
                  data.error.line +
                  ":" +
                  data.error.character +
                  ") " +
                  data.error.reason
                );
              }
            })
            .join("\n");
          return (
            file.relative +
            " (" +
            file.jshint.results.length +
            " errors)\n" +
            errors
          );
        })
      )
    );
}

function php(done) {
  reload = true;
  done();
}

function upload_files(done) {
  if (env === null) {
    done();
    return;
  }
  if (remote.method === "ftp") {
    upload_ftp(function() {
      done();
    });
  } else if (remote.method === "ssh") {
    upload_ssh(function() {
      done();
    });
  } else {
    done();
  }
}

function upload_ftp(done) {
  console.log("Push Files FTP");
  exec("node node_scripts/ftp.js --file=" + file.relative, function(
    err,
    stdout,
    stderr
  ) {
    if (err != null && stdout.length > 0) {
      console.log(err);
    }
    if (stdout != null && stdout.length > 0) {
      console.log(stdout.trim());
    }
    // if (stderr != null) {
    //   console.log(stderr);
    // }
    if (reload === true) {
      // browserSync.reload();
      reload = false;
    }
    done();
  });
}

function upload_ssh(done) {
  console.log("Push Files SSH");
  console.log("File Type: " + file.type);
  let localFile = file.absolute;
  let remotePath = "~/public/wp-content/themes/" + remote.theme + "/";
  let remoteFile = remotePath + file.relative;

  if (file.type == "css" || file.type == "js") {
    localFile = __dirname + "/assets/dist/";
    remoteFile = remotePath + "assets/";
  }

  exec(
    "scp -r" +
      " " +
      "-P " +
      remote.port +
      " " +
      localFile +
      " " +
      remote.username +
      "@" +
      remote.ip +
      ":" +
      remoteFile,
    function(err, stdout, stderr) {
      if (err != null && stdout.length > 0) {
        console.log(err);
      }
      if (stdout != null && stdout.length > 0) {
        console.log(stdout.trim());
      }
      console.log(localFile + " uploaded!");
      console.log(
        "scp -r" +
          " " +
          "-P " +
          remote.port +
          " " +
          localFile +
          " " +
          remote.username +
          "@" +
          remote.ip +
          ":" +
          remoteFile
      );
      done();
    }
  );
}

function getFile(event) {
  file.relative = require("path").relative(__dirname, event);
  file.absolute = __dirname + "/" + event;
  file.type = getFileType(event);
}

function getFileType(file) {
  if (file.includes(".js")) {
    return "js";
  }
  if (file.includes(".scss")) {
    return "css";
  }
  if (file.includes(".php")) {
    return "php";
  }
  return null;
}

function watchFiles() {
  gulp
    .watch("assets/scss/**/*.scss", gulp.series(css, upload_files))
    .on("change", getFile);
  gulp
    .watch("assets/javascript/**/*.js", gulp.series(js, upload_files))
    .on("change", getFile);
  gulp.watch("**/*.php", gulp.series(php, upload_files)).on("change", getFile);
}

exports.watch = gulp.parallel(watchFiles);
exports.default = gulp.parallel(css, gulp.series(lint, js));
